<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsForStageChangeInTableWorkflowStageActionInterviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            $table->integer('if_pass')->default(0);
            $table->integer('if_fail')->default(0);
            $table->integer('if_canceled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            //
        });
    }
}
