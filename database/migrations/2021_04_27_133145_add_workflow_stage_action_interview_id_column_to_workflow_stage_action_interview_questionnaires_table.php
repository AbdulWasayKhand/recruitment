<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkflowStageActionInterviewIdColumnToWorkflowStageActionInterviewQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_interview_questionnaires', function (Blueprint $table) {
            $table->integer('workflow_stage_action_interview_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_interview_questionnaires', function (Blueprint $table) {
            $table->dropColumn('workflow_stage_action_interview_id');
        });
    }
}
