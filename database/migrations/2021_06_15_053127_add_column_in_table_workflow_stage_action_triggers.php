<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInTableWorkflowStageActionTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_triggers', function (Blueprint $table) {
            $table->integer('workflow_id')->after('action_id')->default(0);
            $table->integer('stage_id')->after('workflow_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_triggers', function (Blueprint $table) {
            $table->dropColumn('workflow_id');
            $table->dropColumn('stage_id');
        });
    }
}
