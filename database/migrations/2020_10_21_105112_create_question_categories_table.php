<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_categories', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedInteger('company_id')->default(null);
            $table->unsignedInteger('facebook_id')->nullable();
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->integer('category_id')->after('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_categories');
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
