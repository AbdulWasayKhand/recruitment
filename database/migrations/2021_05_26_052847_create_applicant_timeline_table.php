<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_timeline', function (Blueprint $table) {
            $table->id();
            $table->integer('applicant_id')->nullable();
            $table->integer('entity_id')->nullable();
            $table->integer('user_id')->nullable()->default(0);
            $table->enum('entity', ["interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail","status","stage","notes","questionnaires","hire","reject","attachment_photo","attachment_resume","rating"]);
            $table->text('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_timeline');
    }
}
