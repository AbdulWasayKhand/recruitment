<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableApplicationLabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_labels', function (Blueprint $table) {
            $table->increments("id");
            $table->text('label_value');
            $table->text('label_color');
            $table->integer('company_id');
            $table->timestamp('created_at', $precision = 0);
            $table->timestamp('updated_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_labels');
    }
}
