<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCloneGoogleEventColumnToCustomEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_events', function (Blueprint $table) {
            $table->boolean('clone_google_event')->default(0)->after('block_complete_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_events', function (Blueprint $table) {
            $table->dropColumn('clone_google_event');
        });
    }
}
