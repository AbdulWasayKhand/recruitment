<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('application_id');
            $table->integer('schedule_id');
            $table->string('eid', 200);
            $table->string('cid', 200);
            $table->enum('type', ['calendar_event', 'schedule', 'reschedule', 'canceled']);
            $table->integer('user_id');
            $table->timestamp('created_at', $precision = 0);
            $table->timestamp('updated_at', $precision = 0);
            $table->timestamp('interview_start', $precision = 0);
            $table->timestamp('interview_end', $precision = 0);
            $table->longText('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_logs');
    }
}
