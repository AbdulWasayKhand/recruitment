<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSmsRemindersColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("sms_auto_reminder_hour")->default('disable')->after("remember_token");
            $table->string("sms_auto_reminder_day")->default('disable')->after("sms_auto_reminder_hour");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('sms_auto_reminder_hour');
            $table->dropColumn('sms_auto_reminder_day');
        });
    }
}
