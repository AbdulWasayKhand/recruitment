<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterApplicantTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE applicant_timeline CHANGE COLUMN entity entity ENUM("interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail","status","stage","notes","questionnaires","hire","reject","attachment_photo","attachment_resume","rating","label")');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('ALTER TABLE applicant_timeline CHANGE COLUMN entity entity ENUM("interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail","status","stage","notes","questionnaires","hire","reject","attachment_photo","attachment_resume","rating")');
    }
}
