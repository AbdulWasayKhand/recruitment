<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToWorkflowStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stages', function (Blueprint $table) {
            $table->enum('type', ['applied','hired','rejected','custom'])->default("custom")->after('workflow_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stages', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}