<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowStageActionTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_stage_action_tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('action_id')->nullable();
            $table->integer('stage_id')->nullable();
            $table->integer('workflow_id')->nullable();
            $table->string('title');
            $table->text('description');
            $table->integer('assign_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_stage_action_tasks');
    }
}
