<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInTableWorkflowStageActionTriggersTwo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_triggers', function (Blueprint $table) {
            $table->integer('to_stage')->after('trigger_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_triggers', function (Blueprint $table) {
            $table->dropColumn('to_stage')->after('trigger_id');
        });
    }
}
