<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLabelsTransitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('label_transitions', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('label_id');
            $table->integer('application_id');
            $table->timestamp('created_at', $precision = 0);
            $table->timestamp('updated_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('label_transitions');
    }
}
