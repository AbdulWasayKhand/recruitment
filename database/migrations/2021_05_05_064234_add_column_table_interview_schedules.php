<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableInterviewSchedules extends Migration
{

    public function up()
    {
        Schema::table('interview_schedules', function (Blueprint $table) {
            $table->integer('stage_id')->after('job_application_id');
        });
        DB::statement("ALTER TABLE interview_schedules CHANGE COLUMN status status ENUM('pass', 'failed', 'pending', 'canceled', 'rejected', 'hired', 'pending') DEFAULT 'pending'");
    }

    public function down()
    {
        Schema::table('interview_schedules', function (Blueprint $table) {
            // $table->integer('stage_id')->after('job_application_id');
        });
    }
}
