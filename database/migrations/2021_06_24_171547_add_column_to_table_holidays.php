<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTableHolidays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holidays', function (Blueprint $table) {
            $table->text('eid')->after('clone_google_event');
        });
        Schema::table('custom_events', function (Blueprint $table) {
            $table->text('eid')->after('clone_google_event');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays', function (Blueprint $table) {
            $table->dropColum('eid');
        });
        Schema::table('custom_events', function (Blueprint $table) {
            $table->dropColum('eid');
        });
    }
}
