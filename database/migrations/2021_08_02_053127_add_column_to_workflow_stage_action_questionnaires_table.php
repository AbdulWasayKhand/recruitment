<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToWorkflowStageActionQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_questionnaires', function (Blueprint $table) {
            $table->enum('questionnaire_type', ['internal', 'external'])->default('external')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_questionnaires', function (Blueprint $table) {
            $table->dropColumn('questionnaire_type');
        });
    }
}