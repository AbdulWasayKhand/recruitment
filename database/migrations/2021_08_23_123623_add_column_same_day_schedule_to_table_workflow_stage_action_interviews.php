<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSameDayScheduleToTableWorkflowStageActionInterviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
                $table->integer('same_day_schedule')->after('duration')->default(0);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            $table->dropColumn('same_day_schedule');
        });
    }
}
