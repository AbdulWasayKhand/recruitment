<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class AlterWorkflowStageActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE workflow_stage_actions CHANGE COLUMN type type ENUM("email","interview","questionnaire","document","assessment","sms","task","change_stage") NULL DEFAULT "email"');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('ALTER TABLE workflow_stage_actions CHANGE COLUMN type type ENUM("email","interview","questionnaire","document","assessment","sms","task") NULL DEFAULT "email"');
    }
}
