<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowStageActionSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_stage_action_sms', function (Blueprint $table) {
            $table->id();
            $table->integer('action_id')->nullable();
            $table->integer('stage_id')->nullable();
            $table->integer('workflow_id')->nullable();
            $table->integer('sms_template_id')->nullable();
            $table->enum('to', ["hiring_manager","candidate","hiring_team"])->default("candidate");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_stage_action_sms');
    }
}
