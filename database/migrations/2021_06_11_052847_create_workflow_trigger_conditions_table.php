<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowTriggerConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return voidConditions     */
    public function up()
    {
        Schema::create('workflow_trigger_conditions', function (Blueprint $table) {
            $table->id();
            $table->integer('workflow_trigger_id')->nullable();
            $table->enum('metric', ["qualification"])->nullable()->default("qualification");
            $table->string('operator');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_trigger_conditions');
    }
}
