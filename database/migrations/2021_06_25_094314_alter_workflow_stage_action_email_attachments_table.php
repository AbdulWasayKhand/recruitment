<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterWorkflowStageActionEmailAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_email_attachments', function (Blueprint $table) {
            $table->text('title')->nullable()->change();
            $table->dropColumn('no_of_attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_email_attachments', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->integer('no_of_attachments')->nullable();
        });
    }
}
