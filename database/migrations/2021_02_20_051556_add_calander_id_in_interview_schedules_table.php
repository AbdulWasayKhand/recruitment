<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCalanderIdInInterviewSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interview_schedules', function (Blueprint $table) {
            Schema::table('interview_schedules', function (Blueprint $table) {
                $table->text("calendar_id")->after("id");
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interview_schedules', function (Blueprint $table) {
            Schema::table('interview_schedules', function (Blueprint $table) {
                $table->dropColumn("calendar_id");
            });
        });
    }
}
