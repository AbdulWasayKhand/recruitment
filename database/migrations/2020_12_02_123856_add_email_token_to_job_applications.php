<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailTokenToJobApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_applications', function (Blueprint $table) {
            $table->enum('source',['website','facebook','widget','bot'])->default('website')->after('column_priority');
            $table->float('eligibility')->nullable()->after('column_priority');
            $table->float('eligibility_percentage')->default('50')->after('column_priority');
            $table->string('email_token')->nullable()->after('column_priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_applications', function (Blueprint $table) {
            $table->dropColumn('email_token');
        });
    }
}
