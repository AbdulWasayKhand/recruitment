<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApplyEnumOptionToSourceToJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE job_applications CHANGE COLUMN source source ENUM('website', 'facebook', 'widget', 'other', 'career_plug', 'jazzhr', 'craigslist', 'referral', 'apply') DEFAULT 'website'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE job_applications CHANGE COLUMN source source ENUM('website', 'facebook', 'widget', 'other', 'career_plug', 'jazzhr', 'craigslist', 'referral') DEFAULT 'website'");
    }
}