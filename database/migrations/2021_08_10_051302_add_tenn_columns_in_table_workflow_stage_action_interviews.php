<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTennColumnsInTableWorkflowStageActionInterviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            $table->integer('email_invite')->default(0);
            $table->integer('email_schedule')->default(0);
            $table->integer('email_reschedule')->default(0);
            $table->integer('email_cancelled')->default(0);
            $table->integer('email_reminder')->default(0);
            $table->integer('sms_invite')->default(0);
            $table->integer('sms_schedule')->default(0);
            $table->integer('sms_reschedule')->default(0);
            $table->integer('sms_cancelled')->default(0);
            $table->integer('sms_reminder')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            //
        });
    }
}
