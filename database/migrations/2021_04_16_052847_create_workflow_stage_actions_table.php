<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowStageActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_stage_actions', function (Blueprint $table) {
            $table->id();
            $table->integer('stage_id')->nullable();
            $table->integer('workflow_id')->nullable();
            $table->enum('type', ["email","interview","questionnaire","document","assessment","sms","task"])->nullable()->default("email");
            $table->enum('tigger', ["enter","exit"])->nullable()->default("enter");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_stage_actions');
    }
}
