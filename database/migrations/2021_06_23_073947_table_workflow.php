<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            $table->integer('available_days')->after('workflow_id');
            $table->integer('timeslot')->after('available_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_interviews', function (Blueprint $table) {
            $table->dropColumn('available_days');
            $table->dropColumn('timeslot');
        });
    }
}
