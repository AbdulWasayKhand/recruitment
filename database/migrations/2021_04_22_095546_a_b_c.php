<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ABC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_stage_action_emails', function (Blueprint $table) {
            $table->enum('to', array('hiring_manager' , 'candidate' , 'hiring_team' , 'custom'))->default('hiring_manager')->after('from');
            $table->text('custom_emails')->after('to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_stage_action_emails', function (Blueprint $table) {
            $table->dropColumn('to');
            $table->dropColumn('custom_emails');
        });
    }
}
