<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class AddEnumOptionToApplicantTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE applicant_timeline CHANGE COLUMN entity entity ENUM("interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail","status","stage","notes","questionnaires","hire","reject","attachment_photo","attachment_resume","rating","label","documents")');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('ALTER TABLE applicant_timeline CHANGE COLUMN entity entity ENUM("interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail","status","stage","notes","questionnaires","hire","reject","attachment_photo","attachment_resume","rating","label")');
    }
}
