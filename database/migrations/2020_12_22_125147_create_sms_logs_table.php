<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_logs', function (Blueprint $table) {
            $table->id();   
            $table->unsignedInteger('company_id')->nullable();
            $table->foreign('company_id')->nullable()
                    ->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->enum('type', ['reminder', 'normal'])->default('normal')->index();
            $table->string('number');
            $table->text('message');
            $table->enum('status', ['pending', 'sent', 'bounce'])->default('pending')->index();
            $table->timestamp('send_at');

            $table->string('reference_name')->nullable()->index();
            $table->unsignedInteger('reference_id')->nullable()->index();
            $table->longText('provider_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_logs');
    }
}
