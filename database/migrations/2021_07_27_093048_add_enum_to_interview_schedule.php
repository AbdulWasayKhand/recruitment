<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnumToInterviewSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interview_schedules', function (Blueprint $table) {
            \DB::statement('ALTER TABLE interview_schedules CHANGE COLUMN user_accept_status user_accept_status ENUM("accept", "refuse", "waiting", "done")');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interview_schedule', function (Blueprint $table) {
            //
        });
    }
}
