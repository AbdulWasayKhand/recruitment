<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_history', function (Blueprint $table) {
            $table->increments("id");
            $table->text('current_value')->nullable();
            $table->text('previous_value')->nullable();
            $table->text('status_type')->nullable();
            $table->integer('applicant_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->timestamp('created_at', $precision = 0);
            $table->timestamp('updated_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_history');
    }
}
