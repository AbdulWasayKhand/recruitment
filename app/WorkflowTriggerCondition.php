<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class WorkflowTriggerCondition extends Model
{
    protected $fillable = ['workflow_trigger_id','metric','operator','value'];
}