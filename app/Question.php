<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Question extends Model
{
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('questions.company_id', user()->company_id);
            }
        });
    }

    public function jobs()
    {
        $this->belongsToMany(Job::class, 'job_questions');
    }

    public function answers()
    {
        return $this->hasMany(JobApplicationAnswer::class);
    }

    public function applicantAnswer($applicant_id)
    {
        $answers = $this->answers->where('job_application_id',$applicant_id);
        return $answers->count() ? $answers->first() : new JobApplication;
    }

    public function getQuestionAttribute($value)
    {
        return $value;
    }
    
    public function custom_fields(){
        return $this->hasMany('App\CustomFieldValue');
    }

    public function category() {
        return $this->belongsTo('App\QuestionCategory');
    }

    public function is_qualifiable($question_id=null)
    {
        $is_qualifiable = false;
        $question = $question_id ? Question::find($question_id) : $this;
        if($question && $question->custom_fields->first()) {
            $custom_question = $question->custom_fields->first();
            $question_value  = json_decode($custom_question->question_value, 1);
            $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
            if(is_array($question_qualifiable) && count($question_qualifiable)) {
                return true;
            }
        }
        return $is_qualifiable;
    }
}
