<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class InterviewSchedule extends Model
{
    protected $dates = ['schedule_date','created_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check()) {
                $builder->where('interview_schedules.company_id', user()->company_id);
            }
        });
    }

    // Relation with job application
    public function jobApplication(){
        return $this->belongsTo(JobApplication::class)->withDefault();
    }

    // Relation with location
    public function location(){
        return $this->belongsTo(JobLocation::class,'location_id')->withDefault();
    }

    // Relation with user
    public function user(){
        return $this->belongsTo(User::class);
    }

    // Relation with user
    public function comments(){
        return $this->hasMany(ScheduleComments::class);
    }

    // Relation with user
    public function employee(){
        return $this->hasMany(InterviewScheduleEmployee::class);
    }

    public function employees(){
        return $this->belongsToMany(User::class, InterviewScheduleEmployee::class);
    }

    public function actions(){
        return $this->belongsTo(WorkflowStageActionInterview::class, 'stage_id', 'stage_id')->withDefault();
    }
    
    public function employeeData($userId){
        return InterviewScheduleEmployee::where('user_id', $userId)->where('interview_schedule_id', $this->id)->first();
    }
    public function comment(){
        return $this->belongsTo('\App\ScheduleComments', 'interview_schedule_id', 'id' )->withDefault();
    }

    public function hasAccepted($userId){
        return $this->employee->where('user_id',$userId)->where('user_accept_status','accept')->first() ?? new InterviewScheduleEmployee;
    }

    public function accepter(){
        $accepter = $this->employee->where('user_accept_status','accept')->first() ?? new InterviewScheduleEmployee;
        return $accepter->user;
    }

    public function stage()
    {
        return $this->belongsTo(WorkflowStage::class)->withDefault();
    }
}
