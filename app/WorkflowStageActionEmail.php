<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionEmail extends Model
{
    function template(){
        return $this->belongsTo(EmailTemplate::class, 'email_template_id')->withDefault();
    }
}
