<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CustomEvent extends Model
{
    protected $fillable = ['user_id','title','date','time_range','description','block_complete_day','delete_google_event','clone_google_event'];
}