<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageAction extends Model
{
    protected $fillable = ['stage_id','workflow_id','type','tigger'];
    
    function emails(){
        return $this->hasMany(WorkflowStageActionEmail::class, 'action_id');
    }

    function change_stage(){
        return $this->hasOne('\App\WorkflowStageActionChangeStage', 'action_id','id')->withDefault();
    }

    function trigger(){
        return $this->hasOne('\App\WorkflowStageActionTrigger', 'action_id','id')->withDefault();
    }
}
