<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\InterviewSchedule;
use App\Option;
use App\Company;
use App\JobApplication;
use App\User;
use Google_Client; 
use Google_Service_Calendar;
use App\calendar_log;
use App\Notifications\ScheduleInterview;
use App\Notifications\CandidateScheduleInterview;
use Illuminate\Support\Facades\Notification;
use App\Helper\TwilioHelper as Twilio;
use Illuminate\Notifications\Messages\MailMessage;

class GetGCNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gc:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Google Calendar Notifications for users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // if((int)date("i") % 5 == 0){
            $tokens = Option::where("option_name", "employee_refresh_token")->whereNotNull('option_value')->get();
            foreach($tokens as $token){
                $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
                $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
                $redirectUri = url('oauth2callback');
                $client = new Google_Client();
                $client->setClientId($clientID);
                $client->setClientSecret($clientSecret);
                $client->setRedirectUri($redirectUri);    
                $client->refreshToken($token->option_value);
                $clint_json = $client->getAccessToken();
                if($clint_json){
                    $service = new Google_Service_Calendar($client);
                    $calendarId = 'primary';
                    $optParams = array(
                    'maxResults' => 250,
                    'orderBy' => 'startTime',
                    'singleEvents' => true,
                    'timeMin' => date('c'),
                    );
                    $results = $service->events->listEvents($calendarId, $optParams);
                    $events = $results->getItems();
                    $html = "";
                    $ar = [];
                    if (!empty($events)) { 
                        foreach ($events as $event) {
                            $start = $event->start->dateTime;
                            if (empty($start)) {
                                $start = $event->start->date;
                            }
                            $ev = $event->getSummary();
                            $c = calendar_log::where('eid', '=', $event->id)->first();
                            $start = $event->start->dateTime != "" ? $event->start->dateTime : $event->start->date . "T00:00:00";
                            $end = $event->end->dateTime != "" ? $event->end->dateTime : $event->start->date . "T00:00:00";
                            $date = $event->start->date == null ? explode("T", $event->start->dateTime)[0] : $event->start->date;
                            $UTC_start = explode("-", explode("+", explode("T", $start)[1])[0])[0];
                            $GMT_start = new \DateTime(date("Y-m-d h:i A", strtotime($date . "T" . $UTC_start)), new \DateTimeZone("GMT"));
                            $GMT_start = $GMT_start->format("h:i A");
                            $UTC_end = explode("-",explode("+", explode("T", $end)[1])[0])[0];
                            $GMT_end = new \DateTime(date("Y-m-d h:i A", strtotime($date . "T" . $UTC_end)), new \DateTimeZone("GMT"));
                            $GMT_end = $GMT_end->format("h:i A");
                            $i = InterviewSchedule::where("google_event_id", '=', $event->id)->first();
                            if($i){
                                $change = false;
                                $day = explode("T", $i->toArray()["schedule_date"]);
                                $slot = explode(" - ", $i->slot_timing);
                
                                if(date("Y-m-d", strtotime($i->schedule_date)) != date("Y-m-d", strtotime($date))){
                                    $i->schedule_date = date("Y-m-d H:i:s", strtotime($date ." " . $GMT_start));
                                    $change = true;
                                }
                                if(date("h:i A", strtotime($day[0] . " " . $slot[0])) != $GMT_start){
                                    $slot[0] =  $GMT_start;
                                    $change = true;
                                }
                                if(date("h:i A", strtotime($day[0] . " " . $slot[1])) != $GMT_end){
                                    $slot[1] = $GMT_end;
                                    $change = true;
                                }
                                $i->slot_timing = implode(" - ", $slot);
                                if($change){
                                    $i->save();
                                    $user = User::find($token->user_id);
                                    try {
                                        if (Option::getOption('reschedule_auto_email_enabled', $token->company_id)) {
                                            // echo "<br>Employee:";
                                            Notification::send($user, new ScheduleInterview($i->jobApplication, $i, true));           
                                        }
                                    } catch(\Exception $e) {}
                                    try {
                                        if (Option::getOption('reschedule_auto_email_enabled', $token->company_id)) {
                                            // echo "<br>Candidate:";
                                            Notification::send($i->jobApplication, new CandidateScheduleInterview($i->jobApplication, $i, true));      
                                        }
                                    } catch(\Exception $e) {}
                                        $this->smsSetting = SmsSetting::first();
                                        $name = Option::getOption('company_email_sender_name', $i->jobApplication->company_id);
                                        $from = Option::getOption('company_sender_email', $i->jobApplication->company_id);
                                        $this->setMailConfigs($name, $from);
                                        $this->setSmsConfigs();
                                        $multiEmail = Option::getOption("company_multi_email",$i->jobApplication->company_id);
                                        if($multiEmail){
                                            $multiEmail = explode(",", $multiEmail);
                                        }else{
                                            $multiEmail = [];
                                        }
                                        $subject = "Interview Rescheduled";
                                        $line1 = "Your interview with {$i->jobApplication->full_name} for the job {$i->jobApplication->job->title} has been rescheduled to ".
                                        $i->schedule_date->format('D, M d, Y') . " at {$i->interviewSchedule->slot_timing}.";
                                        (new MailMessage)
                                        ->subject($subject)
                                        ->cc($multiEmail)
                                        ->markdown('email.schedule-interview-received', compact('greet', 'url', 'line1', 'reSchedule'));


                                    if(Option::getOption('reschedule_auto_sms_enabled', $i->jobApplication->company_id)){
                                        $c_token = JobApplication::find($i->jobApplication->id);
                                        $c_token = $c_token ? $c_token->email_token : '';
                                        $interview_url = route('candidate.schedule-inteview') . "?c={$i->jobApplication->company_id}&app={$i->jobApplication->id}&t={$c_token}";
                                        $has_message = Option::getOption('sms_reschedule_interview_text', $i->jobApplication->company_id);
                                        if($i->jobApplication->job->workflow_id != 0){
                                            $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$i->jobApplication->workflow_stage_id)->first();
                                            if($interview_action->sms_schedule != 0){
                                                $has_message = \App\SmsTemplate::find($interview_action->sms_schedule)->content;
                                            }
                                        }
                                        if($has_message){
                                            $message = strtr($has_message, [
                                                '[applicant_name]' => $i->jobApplication->full_name,
                                                '[applicant_email]'=> $i->jobApplication->email,
                                                '[company_name]' => $i->jobApplication->company->company_name,
                                                '[interview_url]' => $interview_url,
                                                '[job_title]' => "{$i->jobApplication->job->title}",
                                                '[schedule_date]' => $i->schedule_date->format('M d, Y'),
                                                '[schedule_time]' => "{$i->slot_timing}",
                                                '[employee_name]'   => $user->name,
                                            ]);
                                        }
                                        $from = Option::getOption('twilio_from_number', $i->jobApplication->company_id);
                                        $from = $from ?? config('twilio-notification-channel.from');
                                        // echo "<br>to: ".$i->jobApplication->phone;
                                        // echo "<br>Message: ".$message;
                                        // echo "<br>from: ".$from;
                                        Twilio::set($i->jobApplication->phone, $message,$from)->send();
                                    }
                                }
                            }
                            // print_r($event);
                        }
                    }
                }
            }
        // }
        return 0;
    }
}
