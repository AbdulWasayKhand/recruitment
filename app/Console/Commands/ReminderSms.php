<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;
use App\SmsLog;
use App\Company;
use App\User;
use App\Helper\TwilioHelper as Twilio;
use Exception;
use Illuminate\Support\Facades\Log;

class ReminderSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send sms to qualified pending applications ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            
            $now = now('GMT');
            $messages = SmsLog::where('send_at', '<=', $now)
                                ->where('message_type', 'outbound')
                                ->where('status', 'pending')->get();

            foreach ($messages as $message) {
                $finally = true;
                $from = Option::getOption('twilio_from_number', $message->company_id, config('twilio-notification-channel.from'));
                if($message->reference_name == 'admin-alert'){
                    $user = User::where("company_id", '=', $message->company_id)->first()->bussness_hours_only;
                    $timings_ = json_decode(Option::where('option_name', '=','company_timings')->where('company_id', '=', $message->company_id)->get()->pluck('option_value')[0], 1);
                    if($user){
                        if(!is_null($timings_['checkin']) && !is_null($timings_['checkout'])){
                            $_timezone = Company::find($message->company_id)->timezone ?? "GMT";
                            $_date = new \DateTime("now", new \DateTimeZone($_timezone));
                            $_now = strtotime($_date->format('H:s'));
                            $_from = strtotime(date("Y-m-d") . " " . $timings_['checkin']);
                            $_to = strtotime(date("Y-m-d") . " " . $timings_['checkout']);
                            if($_now > $_from && $_now < $_to){
                            }else{
                                $finally = false;
                            }
                        }
                        if(isset($timings_['saturday'])){
                            if(date('l') == "Saturday"){
                                $finally = false;
                            }
                        }
                        if(isset($timings_['sunday'])){
                            if(date('l') == "Sunday"){
                                $finally = false;
                            }
                        }
                    }           
                }
                if($finally){
                    if(is_numeric($message->number)){
                        $response = Twilio::set($message->number, $message->message, $from)
                        ->send();
                        $message->provider_response = $response['message'];
                        $message->send_at = $now->toDateTimeString();
                        if ($response['status']) {
                            $message->status = 'sent';
                        } else {
                            $message->status = 'bounce';
                        }
                    }else{
                        $message->status = 'bounce';
                    }
                    $message->save();
                }

            }
        } catch(Exception $e) {
            Log::info('ReminderSms::error');
            Log::error($e->getMessage());
        }
        return 0;
    }
}
