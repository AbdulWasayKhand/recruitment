<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helper\FBApi;
use Illuminate\Support\Arr;
use Carbon\Carbon;

use App\Job;
use App\Company;
use App\JobApplication;
use App\JobApplicationAnswer;
use App\Option;
use App\Question;
use App\ApplicationStatus;
use App\FacebookLead;
use App\FacebookLeadData;

class GetFacebookLead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook:leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Facebook Leads for Companies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $leads = FacebookLead::with('company.facebook')->where('enabled', 1)->get();
        foreach($leads as $lead) {
            if(is_null($lead->company->facebook))
                continue;
            
            $company_id = $lead->company_id;
            $lead_id = $lead->lead_id;
            $access_token = $lead->company->facebook->access_token;
            $last_page = $lead->last_page;
            $job = Job::where('fb_lead_id', $lead_id)->first();

            if(is_null($job))
                continue;

            $api = new FBApi($access_token);
            $fbleads = $api->getLeadApplicants($lead_id, $last_page);
            $applicants = [];
            while (true) {
                if(count($fbleads['data']) == 0)
                    break;
                
                foreach (Arr::get($fbleads, 'data', []) as $d1) {
                    $data = [
                        'fb_id' => $d1['id'], 
                        'created_at' => \Carbon\Carbon::parse($d1['created_time'])
                                            ->setTimeZone('Asia/Karachi')
                                            ->format('Y-m-d H:i:s'),
                    ];
                    foreach (Arr::get($d1, 'field_data', []) as $field_data) {
                        $data[$field_data['name']] = implode(', ',$field_data['values']);
                    }
                    $applicants[] = $data;
                }

                if(isset($fbleads['paging']['next']) && !empty($fbleads['paging']['next'])) {
                    $fbleads = $api->getLeadApplicants($lead_id, $fbleads['paging']['cursors']['after']);
                    continue;
                }

                $lead->last_page = Arr::get($fbleads, 'paging.cursors.after');
                $lead->save();
                
                break;
            }

            $applicationStatus = ApplicationStatus::where('company_id', $company_id)->firstOrFail();
            $applicantsData = [];

            foreach ($applicants as $applicant) {
                if(Arr::get($applicant, 'fb_id', false)
                    && !FacebookLeadData::where('fb_id', Arr::get($applicant, 'fb_id'))
                                        ->where('fb_lead_id', $lead_id)->exists()) {
                                            
                    $db_applicant = JobApplication::insertGetId([
                        'full_name' => Arr::get($applicant, 'full_name', ''),
                        'email' => Arr::get($applicant, 'email', ''),
                        'phone' => Arr::get($applicant, 'phone_number', ''),
                        'job_id' => $job->id,
                        'company_id' => $job->company_id,
                        'status_id' => $applicationStatus->id,
                        'source' => 'facebook',
                        'created_at' => Arr::get($applicant, 'created_at', now()->toDateTimeString()),
                    ]);
                    
                    $job_questions = Arr::except($applicant, ['full_name', 'email', 'phone_number', 'fb_id', 'created_at']);

                    FacebookLeadData::insert([
                        'fb_id' => Arr::get($applicant, 'fb_id', '0'),
                        'fb_lead_id' => $job->fb_lead_id,
                        'company_id' => $job->company_id,
                        'job_application_id' => $db_applicant,
                        'data' => json_encode($job_questions),
                    ]);
                    
                    $totalScore = 0;
                    $totalCount = 0;
                    foreach ($job_questions as $_question => $_answer) {
                        $q = str_replace('_', ' ', $_question);
                        $answer_value = str_replace('_', ' ', $_answer);

                        $question = Question::with('custom_fields')
                                        ->where('company_id', $job->company_id)
                                        ->where('question', 'LIKE', '%'.$q.'%')
                                        ->first();
                                        
                        if($question) {
                            $answer = new JobApplicationAnswer();
                            $answer->job_application_id = $db_applicant;
                            $answer->job_id = $job->id;
                            $answer->question_id = $question->id;
                            $answer->company_id = $job->company_id;
                            $answer->answer = $answer_value;
                            
                            if($question && $question->custom_fields->count() > 0) {
                                $custom_question = $question->custom_fields[0];
                                $question_value  = json_decode($custom_question->question_value, 1);
                                $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                                if(is_array($question_qualifiable)) {
                                    $answer->score = 0;
                                    $_total = count($question_qualifiable);
                                    $totalCount += 1;
                                    foreach($question_qualifiable as $q) {
                                        if(in_array(strtolower($question_value[$q]), (array)$answer_value)) {
                                            $answer->score += 1;
                                            $totalScore += (1 / $_total);
                                        }
                                    }
                                }
                            }
                
                            $answer->save();
                        }
                    }
                    
                    if($totalCount > 0) {
                        $jobApplication = JobApplication::find($db_applicant);
                        $jobApplication->eligibility = ($totalScore / ($totalCount > 0 ? $totalCount : 1)) * 100;
                        $jobApplication->save();
                    }

                    $this->info("{$job->title} --> {$totalScore} / {$totalCount}");
                }
            }

            $this->info("Total Applicants: " . count($applicants));
        }
        return 0;
    }
}
