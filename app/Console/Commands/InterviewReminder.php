<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;
use App\Company;
use App\ApplicationStatus;
use App\JobApplication;
use App\InterviewSchedule;

class InterviewReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:interview-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies Applicant if scheduled for interview before given hour(s) or day(s)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $interviews = InterviewSchedule::with('jobApplication')
                        ->where('status', 'pending')
                        ->get();
        $companies = Company::all()->pluck('id');
        $options   = Option::select('option_name', 'option_value', 'company_id')->whereIn('company_id', $companies)
                            ->whereIn('option_name', ['qualify_auto_email_enabled', 'qualify_auto_sms_enabled', 'sms_auto_reminder_hour', 'sms_auto_reminder_day'])
                            ->get();
        
        $this->info($options->toJson());
        foreach ($interviews as $interview) {
            
            // $this->info('interview '. $interview->toJson());
        }

        return 0;
    }
}
