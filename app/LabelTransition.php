<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabelTransition extends Model
{
    public function jobApplication(){
        return $this->belongsTo(JobApplication::class)->withDefault();
    }
    public function applicationLabel(){
        return $this->belongsToMany(ApplicationLabel::class);
    }
    public function label(){
        return $this->belongsTo('App\ApplicationLabel','label_id')->withDefault();
    }
}
