<?php

namespace App\Http\Requests\Questionnaire;

use App\Http\Requests\CoreRequest;
use App\Question;

class SaveAnswersRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if(!empty($this->get('answer')))
        {
            foreach($this->get('answer') as $key => $value){

                $answer = Question::where('id', $key)->first();
                if($answer->required == 'yes')
                    $rules["answer.{$key}"] = 'required';
            }
        }
        return $rules;
    }

    public function messages()
    {
        $custom_messages = [];
        if(is_array($this->rules()) && count($this->rules())){
            $search = preg_grep ('/^answer.(\w+)/i', array_keys($this->rules()) );
            if(count($search)){
                foreach ($search as $key => $value) {
                    $custom_messages[$value.'.required'] = 'This field is required.';
                }            
            }
        }
        return $custom_messages;            
    }
}