<?php

namespace App\Http\Requests\Questionnaire;

use App\Http\Requests\CoreRequest;

class UpdateRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules["name"] = "required";
        if(!$this->has('questions') || $this->has_questions===null || $this->has_questions===0){
            $rules['has_questions'] = 'required';
        }
        if($this->has('questions') && count($this->questions)<1){
            $rules['has_questions'] = 'required';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'has_questions.required' => 'Please select atleast 1 question.'
        ];
    }
}
