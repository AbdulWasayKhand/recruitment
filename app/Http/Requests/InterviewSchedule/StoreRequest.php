<?php

namespace App\Http\Requests\InterviewSchedule;

use App\Http\Requests\CoreRequest;

class StoreRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "candidates.0"    => "required",
            // "employees.0"      => "required",
            "scheduleDate"    => "required",
            "slot_timing"    => "required",
        ];
    }

    public function messages()
    {
        $custom_messages = [
            // 'employees.*' => 'Please select at-least one employee.',
            'candidates.*' => 'Please select at-least one candidate.'
        ];
        // Employees
        $search = preg_grep ('/^employees.(\w+)/i', array_keys($this->rules()) );
        if(count($search)){
            foreach ($search as $key => $value) {
                $custom_messages[$value.'.required'] = 'Please select at-least one employee.';
            }            
        }
        // Candidates
        $search = preg_grep ('/^candidates.(\w+)/i', array_keys($this->rules()) );
        if(count($search)){
            foreach ($search as $key => $value) {
                $custom_messages[$value.'.required'] = 'Please select at-least one candidate.';
            }            
        }
        return $custom_messages;

        
    }
}
