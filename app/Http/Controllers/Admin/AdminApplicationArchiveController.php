<?php

namespace App\Http\Controllers\Admin;

use App\Exports\JobApplicationArchiveExport;
use App\Helper\Reply;
use App\JobApplication;
use App\JobApplicationAnswer;
use App\Skill;
use App\Job;
use App\Company;
use App\ApplicationLabel;
use App\InterviewSchedule;
use App\ApplicationStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;

class AdminApplicationArchiveController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.archivedApplications';
        $this->pageIcon = 'icon-drawer';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(!$this->user->cans('view_job_applications'), 403);

        return view('admin.applications-archive.index', $this->data);
    }

    public function data(Request $request)
    {
        abort_if(!$this->user->cans('view_job_applications'), 403);

        $jobApplications = JobApplication::onlyTrashed();

        // Filter by skills
        if ($request->has('skill') && $request->skill !== null) {
            $skill = Skill::select('id', 'name')->where('name', 'LIKE', '%'.strtolower($request->skill).'%')->first();

            if ($skill) {
                $jobApplications = $jobApplications->whereJsonContains('skills', (string) $skill->id)->get();
            }
            else {
                $jobApplications = collect([]);
            }
        }
        else {
            $jobApplications = $jobApplications->get();
        }

        return DataTables::of($jobApplications)
            ->editColumn('full_name', function ($row) {
                return '<a href="javascript:;" class="show-detail" data-widget="control-sidebar" data-slide="true" data-row-id="' . $row->id . '">' . ucwords($row->full_name) . '</a>';
            })
            ->editColumn('title', function ($row) {
                return ucfirst($row->job->title);
            })
            ->editColumn('location', function ($row) {
                if(!$row->job->location)
                    return '';
                return ucwords($row->job->location->location);
            })
            ->rawColumns(['action', 'full_name'])
            ->addIndexColumn()
            ->make(true);
    }

    public function destroy($id)
    {
        abort_if(!$this->user->cans('delete_job_applications'), 403);

        $jobApplication = JobApplication::findOrFail($id);

        if ($jobApplication->photo) {
            Storage::delete('candidate-photos/'.$jobApplication->photo);
        }

        $history = \App\ApplicantHistory::where('applicant_id', '=', $jobApplication->id)->get();
        $history->delete();
        $jobApplication->delete();
        // JobApplication::destroy($id);
        return Reply::success(__('messages.recordDeleted'));
    }

    public function show($id)
    {
        $this->application = JobApplication::with(['schedule','notes','onboard', 'status', 'schedule.employee', 'schedule.comments.user'])->withTrashed()->find($id);

        $this->skills = Skill::select('id', 'name')->get();

        $this->answers = JobApplicationAnswer::with(['question'])
            ->where('job_id', $this->application->job_id)
            ->where('job_application_id', $this->application->id)
            ->get();


        $view = view('admin.applications-archive.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function adminShow(Request $request,$id)
    {
        $this->applications = JobApplication::withTrashed()->get();
        $this->application = JobApplication::withTrashed()->with(['schedule', 'status', 'schedule.employee', 'schedule.comments.user', 'facebook_data'])->find($id);
        $this->timezone = Company::where('id', $this->user->company_id)->select('id', 'timezone')->first()->timezone;
        $this->labels = ApplicationLabel::where("company_id", '=', $this->application->company_id)->get();
        $this->skills = Skill::select('id', 'name')->get();
        $this->interviews = InterviewSchedule::where('job_application_id', '=', $this->application->id )->get();
        $this->answers = JobApplicationAnswer::with(['question'])
            ->where('job_id', $this->application->job_id ?? 0)
            ->where('job_application_id', $this->application->id ?? 0)
            ->whereNull('questionnaire_id')
            ->get();
        $this->statuses = ApplicationStatus::where('company_id', $request->user()->company_id)->get();
        $this->stages = $this->application->job->workflow->stages;
        $this->has_interview_invited = $this->application->is_manual_informed;
        $this->questionnaire = $this->application->job->questionnaire;
        
        $this->before = null;
        $this->after = null;
        foreach($this->applications as $key => $value){
            if($this->application->id == $value->id){
                if($key != 0){
                    $this->before = $this->applications[$key - 1];
                }
                if($key != (sizeof($this->applications) - 1)){
                    $this->after = $this->applications[$key + 1];
                }
            }
        }
        $this->attachments = [
            'Resume' => $this->application->resume_url,
            'Photo' => ($this->application->photo_url  != 'http://localhost/recruitment/public/avatar.png' ? $this->application->photo_url : null) 
        ];
        $this->all_stages = \App\WorkflowStage::where('workflow_id',$this->application->job->workflow_id)->get();
        $ids = [];
        foreach($this->all_stages as $stage){
            $ids[] = $stage;
            if($stage->id == $this->application->workflow_stage_id)
                break;
        }
        
        $all_questionnaire = array();
        foreach($ids as $stage){
            foreach(\App\WorkflowStageActionQuestionnaire::where('stage_id', '=', $stage->id ?? 0)->get() as $act){
                
                $ques = \App\Questionnaire::find($act->questionnaire_id);
                $all_questionnaire[$act->questionnaire_id] = [
                    "id" => $act->questionnaire_id, 
                    "name" => $ques->name ?? "", 
                    "is_external" => ($act->questionnaire_type == 'external'),
                    "is_filled" => ($act->questionnaire_type == 'internal') && count($this->application->getStageQuestionnaires('internal')),
                    "created_at" => Carbon::parse($ques->created_at ?? "")->timezone($this->application->company->timezone)->format('M d, Y, h:i A') ];
            }
        }
        $this->previous_questionnaire = $all_questionnaire;
        $view = view('admin.job-applications.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function export($skill)
    {
        $filters = [
            'skill' => $skill
        ];

        $data = [
            'company' => $this->companyName
        ];

        return Excel::download(new JobApplicationArchiveExport($filters, $data), __('modules.applicationArchive.exportFileName').'.xlsx', ExcelExcel::XLSX);
    }
}
