<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\StoreJobCategory;
use App\JobCategory;
use App\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Workflow;
use App\WorkflowStage;
use App\EmailTemplate;
use App\SmsTemplate;
use App\WorkflowStageAction;
use App\WorkflowStageActionEmail;
use App\WorkflowStageActionEmailAttachment;
use App\WorkflowStageActionSms;
use Yajra\DataTables\Facades\DataTables;
use App\Questionnaire;
use App\WorkflowStageActionQuestionnaire;
use App\WorkflowStageActionInterview;
use App\WorkflowStageActionInterviewQuestionnaire;
use App\WorkflowStageActionChangeStage;
use App\WorkflowTrigger;
use App\WorkflowStageActionTrigger;

class AdminWorkFlowController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.workflows';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->workflows = Workflow::all();
        return view('admin.workflow.index', $this->data);
    }

    public function data() {
//        abort_if(! $this->user->cans('view_category'), 403);
        $sources[] = [

        ];
        return DataTables::of($sources)
        ->make(true);
    }

    public function deleteWorkflow(Request $request){
        $wf = Workflow::find($request->wf_id);
        $wf->delete();
        return $request;
    }

    public function deleteInterview(Request $request){
        if($request->has('interview_id') && $request->interview_id){
            $workflow_interview = WorkflowStageActionInterview::find($request->interview_id);
            // Delete Interview Questionnaires
            $workflow_interview_questionnaires = WorkflowStageActionInterviewQuestionnaire::whereIn('workflow_stage_action_interview_id',$workflow_interview->pluck('id'))
                                                ->where('action_id',$workflow_interview->action_id);
            if($workflow_interview_questionnaires->count()){
                $workflow_interview_questionnaires->delete();
            }
            // Delete Interviews Actions
            if($workflow_interview){
                WorkflowStageAction::find($workflow_interview->action_id)->delete();
            }
            // Delete Interviews
            if($workflow_interview){
                $workflow_interview->delete();
            }
        }
        return $request;
    }

    public function emailAction(Request $request){
        $data = $request->all();
        $action_email = WorkflowStageActionEmail::findOrNew($request->action_id);
        // Save Update Action
        $action = WorkflowStageAction::findOrNew($action_email->action_id);
        $data['type'] = 'email';
        $action->fill($data);
        $action->save();
        // Save Update Email Action
        $action_email->action_id = $action->id;
        $action_email->stage_id = $request->stage_id;
        $action_email->workflow_id = $request->workflow_id;
        $action_email->email_template_id = $request->template_id;
        $action_email->to = $request->to;
        $action_email->from = $request->from;
        $action_email->custom_emails = $request->custom_email;
        $action_email->save();
        return $request;
    }
    
    public function smsAction(Request $request){
        $data = $request->all();
        $action_sms = WorkflowStageActionSms::findOrNew($request->action_id);
        // Save Update Action
        $action = WorkflowStageAction::findOrNew($action_sms->action_id);
        $data['type'] = 'sms';
        $action->fill($data);
        $action->save();
        // Save Update Sms Action
        $action_sms->action_id = $action->id;
        $action_sms->stage_id = $request->stage_id;
        $action_sms->workflow_id = $request->workflow_id;
        $action_sms->sms_template_id = $request->template_id;
        $action_sms->to = $request->to;
        $action_sms->save();
        return $request;
    }
    
    public function deleteSmsAction(Request $request){
        $action = WorkflowStageActionSms::find($request->action_id);
        $action->delete();
        return $request;
    }

    public function addQuestionnaire(Request $request){
        $data = $request->all();
        $action_questionnaire = WorkflowStageActionQuestionnaire::findOrNew($request->action_id);
        // Save Update Action
        $action = WorkflowStageAction::findOrNew($action_questionnaire->action_id);
        $data['type'] = 'questionnaire';
        $action->fill($data);
        $action->save();
        // Save Update Questionnaire Action
        $action_questionnaire->action_id = $action->id;
        $action_questionnaire->questionnaire_type = $request->type;
        $action_questionnaire->email_template_id = $request->template_id;
        $action_questionnaire->questionnaire_id = $request->questionnaire;
        $action_questionnaire->workflow_id = $request->workflow_id;
        $action_questionnaire->stage_id = $request->stage_id;
        $action_questionnaire->save();
        return $request;
    }
    
    public function saveInterview(Request $request){
        $data = $request->all();
        $alerts = json_decode($request->alerts,1);
        foreach($alerts as $key=>$value){
            $data[$key] = $value;
        }
        if(!isset($request->employees)){
            $data['employees'] = null;
        }
        // dd(!isset($request->interview_invite));
        if(!isset($request->interview_invite)){
            $data['interview_invite'] = 0;
        }else{
            $data['interview_invite'] = 1;
        }
        $data['show_location']  = $request->has('show_locations');
        $data['same_day_schedule']  = $request->has('same_day_schedule');
        $action_interview = WorkflowStageActionInterview::findOrNew($request->id);
        // Save Update Action
        $action = WorkflowStageAction::findOrNew($action_interview->action_id);
        $data['type'] = 'interview';
        $action->fill($data);
        $action->save();
        
        // Save Update Interview Action
        $data['employees'] = json_encode($data['employees']);
        $action_interview->fill($data);
        $action_interview->action_id = $action->id;
        $action_interview->save();
        $var =  [];
        WorkflowStageActionInterviewQuestionnaire::where('workflow_stage_action_interview_id', '=', $action_interview->id)->delete();
        // Save Update Interview Action
        // return ;
        if($request->has('internal_questionnaire') && count($request->internal_questionnaire)){
            foreach ($request->internal_questionnaire as $questionnaire_id) {
                $action_interview_questionnaire = WorkflowStageActionInterviewQuestionnaire::where([
                    'workflow_stage_action_interview_id'=>$action_interview->id,
                    'questionnaire_id'=>$questionnaire_id
                ]);
                $action_interview_questionnaire = $action_interview_questionnaire->first() ?? new WorkflowStageActionInterviewQuestionnaire;
                $action_interview_questionnaire->workflow_stage_action_interview_id = $action_interview->id;
                $action_interview_questionnaire->action_id = $action->id;
                $action_interview_questionnaire->questionnaire_id = $questionnaire_id;
                $action_interview_questionnaire->save();

                $var[] = $action_interview_questionnaire->id;
            }
            
        }
        return $request;
    }

    public function deleteQuestionnaire(Request $request){
        $action = WorkflowStageActionQuestionnaire::find($request->action_id);
        $action->delete();
        return $request;
    }

    public function deleteEmailAction(Request $request){
        $action = WorkflowStageActionEmail::find($request->action_id);
        $action->delete();
        return $request;
    }

    public function updateWorkflowName(Request $request){
        $redirect = false;
        $return = $request->old;
        $update = Workflow::findOrNew($request->wf_id);
        $update->name = $request->new;
        if(empty($update->id)){
            $last_workflow_id = Workflow::latest()->first();
            $workflow_id = $last_workflow_id ? $last_workflow_id->id + 1 : 1;
            $update->name = "Workflow{$workflow_id}";
            $redirect = true;
        }
        $update->company_id = $this->user->company_id;
        $update->save();
        if($redirect){
            // Save Default Stages
            $update->saveDefaultStages();
            return redirect("admin/workflow/workflow?id={$update->id}");
        }
        return response()->json($update);
    }

    public function saveStage(Request $request){    
        if($request->stage_name != ''){
            $stage = ($request->has('stage_id') && $request->stage_id) ? WorkflowStage::find($request->stage_id) : new WorkflowStage();
            $workflow = \App\Workflow::find($request->workflow_id);
            $last_stage = $workflow->stages->where('type','custom')->last();
            $stage->workflow_id = $request->workflow_id;
            $stage->title = $request->stage_name;
            $stage->color = $request->stage_color;
            if(empty($stage->id)){
                $stage->position = $last_stage ? $last_stage->position + 1 : 1;
            }
            $stage->save();
            if($request->ajax()){
                return ["stage_id" => $stage->id, "title" => $request->stage_name, 'color' => $request->stage_color, 'workflow_id' =>  $request->workflow_id];
            }
            return redirect()->back();
        }
        return [];
    }

    public function deleteStage(Request $request){
        $stage =  WorkflowStage::find($request->sid);
        $app_app = \App\JobApplication::where('workflow_stage_id', '=', $request->sid);
        $app_app->update(['workflow_stage_id'=>0]);
        $stage->delete();
        return $request;
    }

    public function workflow(Request $request)
    {
        $this->email_templates = EmailTemplate::all();
        $this->sms_templates = SmsTemplate::all();
        $this->questionnaires = Questionnaire::all();
        $this->workflow = Workflow::findOrNew($request->id);
        $this->from = ["hiring_manager" => "Admin", 'company' => 'Company',  "engyj" => "Engyj Recruit"];
        $this->to = ["hiring_manager" => "Admin",  'candidate' =>  'Candidate',  'hiring_team' => 'Hiring Team' ,  'custom' => 'Custom Emails'];
        $this->stages = WorkflowStage::where('workflow_id',$this->workflow->id)->where(function($q){
            $q->where('position','>',-1)->orWhereNull('position');
        })->orderBy('position','asc')->get();
        $this->stages = $this->stages->merge( WorkflowStage::where('workflow_id',$this->workflow->id)->where('position','<',0)->get() );
        $this->triggers = WorkflowTrigger::orderBy('id','desc')->get();
        return view('admin.workflow.create', $this->data);
    }

    public function createStage()
    {
        return view('admin.workflow.create-stage', $this->data);
    }

    public function changeStageOrder(Request $request)
    {
        $current = \App\WorkflowStage::find($request->sid);
        $stages = $current->workflow->orderdstages;
        // return ['stages'=>$stages,'current'=>$current,'top'=>$top,'bottom'=>$bottom];
        switch($request->do){
            case 'top':
                $go = false;
                for(( $i = $stages->count() - 1 ); $i >= 0; $i--){
                    $test[] = $i;
                    if($stages[$i]->id == $current->id){
                        $go = true;
                    }
                    if($go && isset($stages[$i-1])){
                        $temp = $current->position;
                        $terget = $stages[$i-1];
                        $current->position = $terget->position;
                        $current->update();
                        $terget->position = $temp;
                        $terget->update();
                    }
                }
                return 'TOP';
            break;
            case 'above':
                foreach($stages as $key=>$stage){
                    if($stage->id == $current->id){
                        $temp = $current->position;
                        $terget = $stages[$key-1];
                        $current->position = $terget->position;
                        $current->update();
                        $terget->position = $temp;
                        $terget->update();
                        break;
                    }
                }
                return 'ABOVE';
            break;
            case 'down':
                foreach($stages as $key=>$stage){
                    if($stage->id == $current->id){
                        $temp = $current->position;
                        $terget = $stages[$key+1];
                        $current->position = $terget->position;
                        $current->update();
                        $terget->position = $temp;
                        $terget->update();
                        break;
                    }
                }
                return 'DOWN';
            break;
            case 'bottom':
                $go = false;
                foreach($stages as $key=>$stage){
                    if($stage->id == $current->id){
                        $go = true;
                    }
                    if($go && isset($stages[$key+1])){
                        $temp = $current->position;
                        $terget = $stages[$key+1];
                        $current->position = $terget->position;
                        $current->update();
                        $terget->position = $temp;
                        $terget->update();
                    }
                }
                return 'BOTTOM';
            break;
        }
    }

    public function changeStage(Request $request)
    {
        // Save Action
        $workflow_stage_action =  WorkflowStageActionTrigger::findOrNew($request->workflow_stage_action_id);
        $workflow_stage_action->action_id = $request->workflow_stage_action_id;
        $workflow_stage_action->stage_id = $request->stage_id;
        $workflow_stage_action->workflow_id = $request->workflow_id;
        $workflow_stage_action->trigger_id = $request->trigger_id;
        $workflow_stage_action->to_stage = $request->to_stage;
        $workflow_stage_action->save();
        return redirect()->back();
    }

    public function emailAttachment(Request $request)
    {
        // Save Action
        $workflow_stage_action =  WorkflowStageActionEmailAttachment::findOrNew($request->workflow_stage_action_email_attachment_id);
        $workflow_stage_action->title = implode(',',$request->doc_titles);
        $workflow_stage_action->fill($request->all());
        $workflow_stage_action->save();
        return redirect()->back();
    }

    public function deleteChangeStageAction(Request $request){
        $action = WorkflowStageActionTrigger::find($request->action_id);
        $action->delete();
        return $request;
    }

    public function deleteEmailAttachmentAction(Request $request){
        $action = WorkflowStageActionEmailAttachment::find($request->action_id);
        $action->delete();
        return $request;
    }
}
