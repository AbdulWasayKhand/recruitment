<?php

namespace App\Http\Controllers\Admin;

use App\ApplicationStatus;
use App\InterviewSchedule;
use App\Job;
use App\JobApplication;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Company;
use App\JobLocation;
use App\User;
use App\Helper\TwilioHelper as Twilio;
use App\Option;
use App\SmsLog;
use Illuminate\Support\Arr;

class AdminDashboardController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'icon-speedometer';
        $this->pageTitle = 'menu.dashboard';
    }

    public function index(Request $request){   
        $this->totalOpenings = Job::activeJobsCount();
        $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
        $appStatuses['interview'] = isset($appStatuses['interview']) ? $appStatuses['interview'] : null;
        $appStatuses['rejected'] = isset($appStatuses['rejected']) ? $appStatuses['rejected'] : null;
        $appStatuses['applied'] = isset($appStatuses['applied']) ? $appStatuses['applied'] : null;
        $appStatuses['phone screen'] = isset($appStatuses['phone screen']) ? $appStatuses['phone screen'] : null;
        $appStatuses['hired'] = isset($appStatuses['hired']) ? $appStatuses['hired'] : null;
        $appStatuses['No Show'] = isset($appStatuses['No Show']) ? $appStatuses['No Show'] : null;
        $this->appStatuses = $appStatuses;
        $allApplications = JobApplication::join('application_status', 'application_status.id', '=', 'job_applications.status_id')->get();
        
        $this->totalRejected = $allApplications->filter(function ($value, $key) use($appStatuses)  {
            return $value->status_id  == $appStatuses['rejected'];
        })->count();

        $this->newApplications = $allApplications->filter(function ($value, $key) use($appStatuses)  {
            return $value->status_id  == $appStatuses['applied'];
        })->count();

        $this->shortlisted = $allApplications->filter(function ($value, $key) use($appStatuses)  {
            return $value->status_id  == $appStatuses['phone screen'] || $value->status_id == $appStatuses['interview'];
        })->count();
        
        $this->interviews_count = $allApplications->filter(function ($value, $key) use($appStatuses) {
            return $value->status_id == $appStatuses['interview'];
        })->count();
        
        $allJobs = Job::select("*")
        ->leftJoin('job_applications', 'job_applications.job_id', '=', 'jobs.id')
        ->get();
        $qualified = [];
        foreach($allJobs as $job){
            if($job->eligibility >= $job->eligibility_percentage){
                $qualified[$job->job_id][] = $job;
            }
        }
        $currentDate = Carbon::now(company()->timezone)->format('Y-m-d');

        $this->totalTodayInterview = InterviewSchedule::where(DB::raw('DATE(`schedule_date`)'),  "$currentDate")
            ->count();
        $this->todoItemsView = $this->generateTodoView();

        // For Upcoming Interviews
        // Get All schedules
        $this->schedules = InterviewSchedule::
            select('interview_schedules.id', 'job_applications.full_name','slot_timing',  'job_applications.full_name', 'job_application_id', 'schedule_date', 'interview_schedules.status',  'jobs.title', 'job_applications.eligibility as qualified', 'job_applications.created_at as applied')
            ->leftJoin('job_applications', 'job_applications.id', '=', 'interview_schedules.job_application_id')
            ->leftJoin('jobs', 'jobs.id', '=', 'job_applications.job_id')
            ->with(['employees', 'jobApplication:id,job_id,full_name', 'jobApplication.job:id,title'])
            ->where('interview_schedules.status', 'pending')
            // ->where('interview_schedules.user_accept_status', 'accept')
            ->orderBy('schedule_date')
            ->get();

        $this->jobs = Job::select("jobs.id", "title", "location as point", "start_date", "end_date", "companies.company_name")
            ->leftJoin('companies', 'companies.id', '=', 'jobs.company_id')
            ->leftJoin('job_locations', 'job_locations.id', '=', 'jobs.location_id')
            ->leftJoin('job_applications', 'job_applications.job_id', '=', 'jobs.id')
            ->where("jobs.status", "active")
            ->groupBy("jobs.id")
            ->get();

        $this->jobs = Job::withCount(['applications as inteviewed' => function($query) use($appStatuses) {
                            $query->where('status_id', $appStatuses['interview']);
                        }, 'applications as applied' => function($query) use($appStatuses)  {
                            $query->where('status_id', $appStatuses['applied']);
                        }, 'applications as hired' => function($query) use($appStatuses)  {
                            $query->where('status_id', $appStatuses['hired']);
                        }, 'applications as rejected' => function($query) use($appStatuses)  {
                            $query->where('status_id', $appStatuses['rejected']);
                        }, 'applications as qualified' => function($query) {
                            $query->where('eligibility', '>=', 'jobs.eligibility_percentage');
                        }, 'applications as unqualified' => function($query) {
                            $query->where('eligibility', '<', 'jobs.eligibility_percentage');
                        }])
                        ->where('status', 'active')
                        ->get();

        $this->qualified = $qualified;
        // Filter upcoming schedule
        $upComingSchedules = $this->schedules->filter(function ($value, $key)use($currentDate) {
            return $value->schedule_date >= $currentDate;
        });

        $upcomingData = [];

        // Set array for upcoming schedule
        foreach($upComingSchedules as $upComingSchedule) {
            $dt = $upComingSchedule->schedule_date->setTimezone(company()->timezone ?? 'GMT')->format('Y-m-d');
            $upcomingData[$dt][] = $upComingSchedule;
        }

        $this->upComingSchedules = $upcomingData;
        $from = Option::getOption('twilio_from_number', $this->global->id);
        $twilio_number = $from ?? config('twilio-notification-channel.from');

        $this->messages = Twilio::set(NULL, NULL, $twilio_number)
                          ->read();
        
        $to_numbers = $this->messages->pluck('to');
        $applications   = JobApplication::select('id', 'full_name', 'phone')
                            ->where('company_id', $this->user->company_id)
                            ->whereIn('phone', $to_numbers)->get();
        
        $this->messages = $this->messages->map(function($item) use($applications) {
            $application = $applications->first(function($app) use($item) { return $app->phone == $item['to']; });
            $item = Arr::only($item, ['from', 'body', 'dateCreated']);
            $item['date_created'] = Carbon::parse(Arr::get($item, 'dateCreated'));
            unset($item['dateCreated']);
            if($application) {
                $application = $application->toArray();
                $application = array_merge($application ?? [], $item);
                $application['full_name'] = $this->user->name;
                $application = Arr::only($application, ['id', 'full_name', 'phone', 'body', 'date_created']);
                $application['is_sender'] = 1;
            }
            return $application;
        });

        $this->messages = $this->messages->filter(function($item) {
            return $item != NULL;
        })->values();

        $sms_logs = SmsLog::whereIn('number', $to_numbers)
                            ->select('job_applications.id', 'job_applications.full_name', 'sms_logs.number as phone', 'sms_logs.message as body', 'sms_logs.send_at as date_created')
                            ->leftJoin('job_applications', 'job_applications.id', '=', 'sms_logs.reference_id')
                            ->where('sms_logs.message_type', 'inbound')
                            ->where('sms_logs.company_id', $this->user->company_id)
                            ->whereNotNull('job_applications.id')
                            ->status('sent')
                            ->get()
                            ->map(function($log) { $log->date_created = new Carbon($log->date_created); return $log->toArray(); });
        
        $this->messages = $this->messages->merge($sms_logs)->sortByDesc(function($message) { return $message['date_created']->timestamp; })->values();

        return view('admin.dashboard.index', $this->data);
    }

    function get_engyj_data(){
        if($_POST){
            $start_date = $_POST['start_date'];
            $end_date   = $_POST['end_date'];
            
            $total_jobs_openings = Job::activeJobs();
            $total_jobs_openings = $total_jobs_openings->filter(function ($item) use ($start_date, $end_date) {
                return (data_get($item, 'created_at') >= $start_date && data_get($item, 'created_at') <= $end_date);
            })->count();
           
            $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
            $all_applicants = JobApplication::all();
            if(strtotime(explode(" " , $end_date)[0]) == strtotime(date('Y-m-d'))){
                $interview_schedules = InterviewSchedule::whereDate('schedule_date','>=',$start_date)
                                        ->groupBy('job_application_id')->get();

            }else{
                $interview_schedules = InterviewSchedule::whereDate('schedule_date','>=',$start_date)
                ->whereDate('schedule_date','<=',$end_date)
                ->groupBy('job_application_id')->get();
            }
 
            $all_applicants = $all_applicants->filter(function ($item) use ($start_date, $end_date) {
                return (data_get($item, 'created_at') >= $start_date && data_get($item, 'created_at') <= $end_date);
            });
            // $interview_schedules;
            $total_interviewed = $interview_schedules->filter(function ($value) {
                return !empty($value->jobApplication->id);
            })->count();

            $total_offered = $all_applicants->filter(function ($value, $key) use($appStatuses)  {
                if($value->job->workflow_id==0){
                    return $value->status_id  == $appStatuses['rejected'];
                }else{
                    return $value->hiring_status  == 'rejected';
                }
            })->count();

            $total_hired = $all_applicants->filter(function ($value, $key) use($appStatuses) {
                if($value->job->workflow_id==0){
                    return $value->status_id  == $appStatuses['hired'];
                }else{
                    return $value->hiring_status  == 'hired';
                }
            })->count();


            $total_applications = count($all_applicants);
            $total_qualified    = $all_applicants->filter(function ($value, $key) use($appStatuses)  {
                return $value->job->eligibility_percentage <= $value->eligibility;
            })->count();
            $total_unqualified = $total_applications-$total_qualified;

            $chart_qual_unqual = [
                'qualified'   => $total_qualified,
                'qual_per'    => $total_qualified > 0 ? number_format(($total_qualified / $total_applications) * 100, 2) : 0,
                'unqualified' => $total_unqualified,
                'unqual_per'  => $total_unqualified > 0 ? number_format(($total_unqualified / $total_applications) * 100, 2) : 0
            ]; 

            $all_applicats   = json_decode($all_applicants);

            $day_applicant = []; $total_fb = 0; $total_widget = 0; $total_indeed = 0;
            $tot_app_daywise = [];
            foreach($all_applicats as $applicant){
                if(isset($day_applicant[date('l', strtotime($applicant->created_at))])){
                    $day_applicant[date('l', strtotime($applicant->created_at))][] = $applicant;
                }else{
                    $day_applicant[date('l', strtotime($applicant->created_at))][] = $applicant;
                }

                if($applicant->source == 'facebook'){
                    $total_fb++;
                }

                if($applicant->source == 'widget'){
                    $total_widget++;
                }

                if($applicant->source == 'indeed'){
                    $total_indeed++;
                }
            }

            $chart_app_sources = [
                'facebook_total' => $total_fb,
                'facebook_per'   => $total_fb      > 0 ? number_format(($total_fb / $total_applications) * 100, 2)     : 0,
                'widget_total'   => $total_widget,
                'widget_per'     => $total_widget  > 0 ? number_format(($total_widget / $total_applications) * 100, 2) : 0,
                'indeed_total'   => $total_indeed,
                'indeed_per'     => $total_indeed  > 0 ? number_format(($total_indeed / $total_applications) * 100, 2) : 0
            ]; 

            
            $applicants_chart = [];
            $qualified_day = [];
            $unqualified_day = [];
            foreach(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] as $key => $day){
                if(isset($day_applicant[$day]) && !empty($day_applicant[$day])){
                    $applicants_chart['days'][$key]     = $day;   
                    $applicants_chart['days_val'][$key] = count($day_applicant[$day]);
                }else{
                    $applicants_chart['days'][$key]     = $day;   
                    $applicants_chart['days_val'][$key] = 0;

                }
            }
            $weekdays = [
                'Monday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Monday';
                            }), 
                'Tuesday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Tuesday';
                            }), 
                'Wednesday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Wednesday';
                            }), 
                'Thursday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Thursday';
                            }), 
                'Friday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Friday';
                            }), 
                'Saturday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Saturday';
                            }), 
                'Sunday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Sunday';
                            })
            ];

            $daywise = [];
            $qualified_bar_chart_daywise = [];
            $i = 0;
            foreach($weekdays as $key => $source){
                $total = $source->count();
                $unqualified = $source->filter(function ($value, $key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();
                $daywise[] = [
                    'day' => $key, 
                    'applied' => $source->count(), 
                    'unqualified' => $unqualified, 
                    'qualified' => $source->count() - $unqualified
                ];
                $qualified_bar_chart_daywise['data']['qualified'][] = $source->count() - $unqualified;
                $qualified_bar_chart_daywise['data']['unqualified'][] = $unqualified;
                $qualified_bar_chart_daywise['labels'][$i] = [$key];
                $i++;
            }
            $stats = [
                //'total_jobs_openings' => $total_jobs_openings,
                'total_applications'            => $total_applications,
                'total_qualified'               => $total_qualified,
                'total_interviewed'             => $total_interviewed,
                'total_offered'                 => $total_offered,
                'total_hired'                   => $total_hired,
                'total-qualified-percentage'    => $total_applications ? '(' . number_format((float)($total_qualified /  $total_applications * 100) , 2, '.', '') . '%)' : '(0%)',
                'total-rejected-percentage'     => $total_applications ? '(' . number_format((float)($total_offered /  $total_applications * 100) , 2, '.', '') . '%)' : '(0%)',
                'total-hired-percentage'        => $total_applications ? '(' . number_format((float)($total_hired /  $total_applications * 100) , 2, '.', '') . '%)' : '(0%)',
                'total-interview-percentage'    => $total_applications ? '(' . number_format((float)($total_interviewed /  $total_applications * 100) , 2, '.', '') . '%)' : '(0%)',
                
            ];
            $daywise = [];
            $qualified_bar_chart_daywise = [];
            $i = 0;
            foreach($weekdays as $key => $source){
                $total = $source->count();
                $unqualified = $source->filter(function ($value, $key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();
                $daywise[] = [
                    'day' => $key, 
                    'applied' => $source->count(), 
                    'unqualified' => $unqualified, 
                    'qualified' => $source->count() - $unqualified
                ];
                $qualified_bar_chart_daywise['data']['qualified'][] = $source->count() - $unqualified;
                $qualified_bar_chart_daywise['data']['unqualified'][] = $unqualified;
                $qualified_bar_chart_daywise['labels'][$i] = [$key];
                $i++;
            }


            echo json_encode( [$applicants_chart, $chart_qual_unqual, $chart_app_sources, $stats, $qualified_bar_chart_daywise] );
        } //post
    } //get_engyj_data
}