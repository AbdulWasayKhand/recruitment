<?php

namespace App\Http\Controllers\Admin;

use App\ApplicationStatus;
use App\Helper\Reply;

use App\Http\Requests\InterviewSchedule\StoreRequest;
use App\Http\Requests\InterviewSchedule\UpdateRequest;
use App\InterviewSchedule;
use App\InterviewScheduleEmployee;
use App\JobCategory;
use App\Job;
use App\JobApplication;
use App\Notifications\SmsTwilio;
use App\Notifications\CandidateNotify;
use App\Notifications\CandidateReminder;
use App\Notifications\CandidateScheduleInterview;
use App\Notifications\EmployeeResponse;
use App\Notifications\ScheduleInterview;
use App\Notifications\ScheduleInterviewStatus;
use App\Notifications\ScheduleStatusCandidate;
use App\Notifications\AcceptInterviewRequest;
use App\ScheduleComments;
use App\User;
use App\LanguageSetting;
use App\Company;
use App\Option;
use App\SmsLog;
use App\calendar_log;
use App\ApplicationSetting;
use App\ThemeSetting;
use App\JobLocation;
use App\CustomEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Yajra\DataTables\Facades\DataTables;
use App\Notifications\CandidateRejected;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Helper\TwilioHelper as Twilio;
use Illuminate\Support\Facades\Log;
use Google_Client; 
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use App\ApplicantHistory;
use \App\Http\Controllers\Admin\AdminJobApplicationController;

class InterviewScheduleController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.interviewCalendar';
        $this->pageIcon = 'icon-calender';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        abort_if(! $this->user->cans('view_schedule'), 403);
        $googleSchedules = (($this->user->GC_channel_id ?? '') != '') ?  getGCEvents($this->user) : $this->getExternalGEvents($this->user);
        foreach($googleSchedules as $key=>$gSlots){
            $start =Carbon::createFromFormat('Y-m-d h:i A', $googleSchedules[$key]["start"]);
            $start->format("Y-m-d h:i A");
            $start->setTimezone("GMT");
            $end =Carbon::createFromFormat('Y-m-d h:i A', $googleSchedules[$key]["end"]);
            $end->format("Y-m-d h:i A");
            $end->setTimezone("GMT");
            $googleSchedules[$key]["start"] = $start;
            $googleSchedules[$key]["end"] = $end;
        }
        $this->googleSchedules = $googleSchedules;
        $currentDate = Carbon::now($this->global->timezone); // Current Date

        // Get All schedules
        $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
        $this->schedules = InterviewSchedule::
            select('interview_schedules.id', 'interview_schedules.job_application_id', 'interview_schedules.schedule_date', 'interview_schedules.status', 'interview_schedules.slot_timing')
            ->leftJoin('job_applications','interview_schedules.job_application_id','job_applications.id')
            ->with(['employees', 'jobApplication:id,job_id,full_name,status_id', 'jobApplication.job:id,title'])
            ->where('interview_schedules.status', 'pending')
            ->where('job_applications.status_id','!=', $appStatuses['hired'])
            ->where('job_applications.status_id','!=', $appStatuses['rejected'])
            ->orderBy('schedule_date')
            ->get();
        
        $this->schedules = InterviewSchedule::where('status', '=', 'pending')
                                            ->orderBy('schedule_date')
                                            ->get();

        // Filter upcoming schedule
        $upComingSchedules = $this->schedules->filter(function ($value, $key)use($currentDate) {
            return ( $value->schedule_date >= $currentDate->format('Y-m-d') && !in_array($value->user_accept_status,['waiting','']) && !empty($value->jobApplication->id));
        });

        $upcomingData = [];
        // Set array for upcoming schedule
        foreach($upComingSchedules as $upComingSchedule){
            $dt = $upComingSchedule->schedule_date->timezone($this->global->timezone)->format('Y-m-d');
            $upcomingData[$dt][] = $upComingSchedule;

        }

        $this->upComingSchedules = $upcomingData;
        if($request->ajax()){
            $viewData = view('admin.interview-schedule.upcoming-schedule', $this->data)->render();
            return Reply::dataOnly(['data' => $viewData, 'scheduleData' => $this->schedules]);
        }

        $this->today = $currentDate->format('c');
        $this->timezone = $this->global->timezone ?: 'GMT';

        $this->optionEmp = $this->user->GC_refresh_token; //Option::getOption('employee_refresh_token', $this->user->company_id, $this->user->id);
        $holidays = \App\Holiday::where('company_id','=', company()->id)->get();
        $user = $this->user;
        if($this->user->GC_refresh_token != ''){
            foreach($holidays as $holiday){
                $eids = json_decode($holiday->eid, 1);
                if(!in_array($user->id, array_keys($eids ?? []))){
                    // $from  = \Carbon\Carbon::createFromFormat('m-d-Y',$holiday->from)->timezone($user->company->timezone)->format('Y-m-d');
                    // $to = \Carbon\Carbon::createFromFormat('m-d-Y',$holiday->to)->timezone($user->company->timezone)->format('Y-m-d');
                    $eids[$user->id] = createGoogleEvent($holiday->title, $holiday->description, $holiday->from. "T00:00:00",  $holiday->to. "T23:59:00",  $user);
                    $holiday->eid = json_encode($eids);
                    $holiday->save();
                }
            }
        }

        $this->is_user_accepter = \App\InterviewScheduleEmployee::whereIn('interview_schedule_id',upComingScheduleRequests()->pluck('id'))->where('user_id',$this->user->id)->count();
        $this->userId = $this->user->id;
        $this->schedule_date = $this->user->getTimeSlotDate();
        $this->custom_events = \App\CustomEvent::where('user_id',$this->user->id)->get();
        $this->holidays = \App\Holiday::where('company_id',$this->user->company_id)->get();
        $this->workflows = \App\Workflow::all();
        /* Default applicants*/
        $interviewStatus = ApplicationStatus::where('company_id', $request->user()->company_id)->get()->filter(function($s) {
            return $s->status == 'interview';
        })->values()->first();
        $default_applicants = \App\JobApplication::where('status_id','<',$interviewStatus->id ?? 0)
            ->whereNotIn('id',$this->schedules->pluck('job_application_id')->toArray())
            ->pluck('id')
            ->toArray();
        /* end Default applicants*/
        /* Workflow applicants*/
        $workflow_applicants = JobApplication::all()->filter(function($applicant){
            return $applicant->workflow_stage->interview_actions->count() > 0;
        })->pluck('id')->toArray();
        /* end Workflow applicants*/
        $this->applicants = JobApplication::whereIn('id',array_merge($default_applicants,$workflow_applicants))->get();
        return view('admin.interview-schedule.index', $this->data);
    }


    /**
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function create(Request $request){
        abort_if(! $this->user->cans('add_schedule'), 403);
        $this->candidates = JobApplication::all();
        $this->candidates = $this->candidates->filter(function($applicant){
            if($applicant->workflow_stage_id ==0){
                return $applicant->schedule->id == null;
            }
            else{
                return ( $applicant->workflow_stage->workflow_actions('interview')->count() > 0 && $applicant->schedule->status != 'pending' );
            }
        });
        $this->users = User::with('company')->company()->get();
        $this->scheduleDate = $request->date;
        $this->selected_schedule_date = ($request->has('is_clicked_by_button') && $request->is_clicked_by_button) ? $this->user->getTimeSlotDate() : '';        
        return view('admin.interview-schedule.create', $this->data)->render();
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function table(Request $request){
        abort_if(! $this->user->cans('add_schedule'), 403);
        $this->candidates = JobApplication::all();
        $this->users = User::with('company')->company()->get();
        return view('admin.interview-schedule.table', $this->data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function data(Request $request){
        abort_if(! $this->user->cans('view_schedule'), 403);

        $shedule = InterviewSchedule::select('interview_schedules.id','job_applications.full_name','interview_schedules.status', 'interview_schedules.schedule_date')
            ->leftjoin('job_applications', 'job_applications.id', 'interview_schedules.job_application_id');
        // Filter by status
        if($request->status != 'all' && $request->status != ''){
            $shedule = $shedule->where('interview_schedules.status', $request->status);
        }

        // Filter By candidate
        if($request->applicationID != 'all' && $request->applicationID != ''){
            $shedule = $shedule->where('job_applications.id', $request->applicationID);
        }

        // Filter by StartDate
        if($request->startDate !== null && $request->startDate != 'null'){
            $shedule = $shedule->where(DB::raw('DATE(interview_schedules.`schedule_date`)'), '>=', "$request->startDate");
        }

        // Filter by EndDate
        if($request->endDate !== null && $request->endDate != 'null'){
            $shedule = $shedule->where(DB::raw('DATE(interview_schedules.`schedule_date`)'), '<=', "$request->endDate");
        }

        return DataTables::of($shedule)
            ->addColumn('action', function ($row) {
                $action = '';


                if( $this->user->cans('view_schedule')){
                    $action.= '<a href="javascript:;" data-row-id="' . $row->id . '" class="btn btn-info btn-circle view-data"
                      data-toggle="tooltip" data-original-title="'.__('app.view').'"><i class="fa fa-search" aria-hidden="true"></i></a>';
                }
                if( $this->user->cans('edit_schedule')){
                    $action.= '<a href="javascript:;" style="margin-left:4px" data-row-id="' . $row->id . '" class="btn btn-primary btn-circle edit-data"
                      data-toggle="tooltip" data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }

                if( $this->user->cans('delete_schedule')){
                    $action.= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
                return $action;
            })
            ->addColumn('checkbox', function ($row) {
                return '
                    <div class="checkbox form-check">
                        <input id="' . $row->id . '" type="checkbox" name="id[]" class="form-check-input" value="' . $row->id . '" >
                        <label for="' . $row->id . '"></label>
                    </div>
                ';
            })
            ->editColumn('full_name', function ($row) {
                return ucwords($row->full_name);
            })
            ->editColumn('schedule_date', function ($row) {
                return Carbon::parse($row->schedule_date)->setTimezone($this->user->company->timezone)->format('d F, Y H:i a');
            })
            ->editColumn('status', function ($row) {
                if($row->status == 'pending'){
                    return '<label class="badge bg-warning">'.__('app.pending').'</label>';
                }
                if($row->status == 'hired'){
                    return '<label class="badge bg-success">'.__('app.hired').'</label>';
                }
                if($row->status == 'canceled'){
                    return '<label class="badge bg-danger">'.__('app.canceled').'</label>';
                }
                if($row->status == 'rejected'){
                    return '<label class="badge bg-danger">'.__('app.rejected').'</label>';
                }
            })
            ->rawColumns(['action', 'status', 'full_name', 'checkbox'])
            ->make(true);
    }

    /**
     * @param $id
     * @return string
     * @throws \Throwable
     */
    public function edit($id){
        abort_if(! $this->user->cans('edit_schedule'), 403);
        
        $this->candidates = JobApplication::all();
        $this->users = User::with('company')->company()->get();
        $this->schedule = InterviewSchedule::with(['jobApplication', 'user'])->find($id);
        $this->comment = ScheduleComments::where('interview_schedule_id', $this->schedule->id)
                                            ->where('user_id', $this->user->id)->first();
        $this->employeeList = json_encode($this->schedule->employee->pluck('user_id')->toArray());

        
        return view('admin.interview-schedule.edit', $this->data)->render();
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request){
        $scheduleTime = Str::before($request->slot_timing, ' - ');
        $scheduleTime = date('H:i', strtotime($scheduleTime));
        $dateTime =  $request->scheduleDate.' '. ($scheduleTime ?? '00:00');
        $dateTime = Carbon::createFromFormat('Y-m-d H:i', $dateTime, company()->timezone ?? 'GMT');
        $dateTime->setTimezone('GMT');

        foreach ($request->candidates as $candidateId) {
            $application = JobApplication::find($candidateId);
            if(!$request->has('employees') && $request->from == 'admin'){
                $request->merge([
                    'employees' => [($application->schedule->user_accept_status == 'accept') ? $application->schedule->employee[0]->user->id : User::where('company_id', $this->user->company_id)->withRole('admin')->first()->id]
                ]);
            }
            $current_user = User::find($request->employees[0]);
            abort_if(! $current_user->cans('add_schedule'), 403);
            $company = Company::find($current_user->company_id);
            $company_settings = Option::getAll($company->id);
            
            $go = true;
            $auto_accept = false;
            if($application->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id', '=', $application->workflow_stage_id)->first();
                $go = $interview_action->show_location;
                $total_users = [];
                $auto_accept_user = $current_user;
                if($interview_action->interview_invite){
                   $panal = $application->workflow_stage->workflow_actions('interview')[0]->interview_panel();
                   foreach($panal as $interviewer){
                       if(isset($interviewer->profile_schedule['days'][strtolower($dateTime->format('l'))]['active'])){
                           $total_users[] = $interviewer;
                       }
                   }
                   if($interview_action->interview_invite){
                       $auto_accept = $panal->count() == 1 || count($total_users) == 1;
                       $auto_accept_user = (($panal->count() == 1) ? $panal : ((count($total_users) == 1) ? $total_users[0] : new \App\User));
                       if(get_class($auto_accept_user) == 'Illuminate\Database\Eloquent\Collection'){
                            $auto_accept_user = $auto_accept_user->first();
                        }
                    }
                }
            }
            $interviewSchedule = new InterviewSchedule();
            $interviewSchedule->location_id = $go ? ($request->location_id ?? str_replace('"]', "", str_replace('["',"", $application->job->location_id))) : 0;
            $interviewSchedule->company_id = $current_user->company_id;
            $interviewSchedule->job_application_id = $candidateId;
            $interviewSchedule->schedule_date = $dateTime;
            $interviewSchedule->slot_timing = $request->slot_timing;
            $interviewSchedule->save();
            // Update Schedule Status
            $status = ApplicationStatus::where('company_id', $current_user->company_id)
                            ->where('status', 'interview')
                            ->pluck('id');
            
            $jobApplication = $interviewSchedule->jobApplication;
            if($jobApplication->job->workflow_id==0){
                $jobApplication->status_id = $status[0] ?? 3;
                $applicantHistory = new ApplicantHistory();
                $applicantHistory->current_value = $status[0] ?? 3;
                $applicantHistory->previous_value = $application->status_id;
                $applicantHistory->status_type = 'application';
                $applicantHistory->company_id = $application->company_id;
                $applicantHistory->applicant_id = $application->id;
                $applicantHistory->save();
                $jobApplication->save();
            }

            if($request->comment){
                $scheduleComment = [
                    'interview_schedule_id' => $interviewSchedule->id,
                    'user_id' => $current_user->id,
                    'company_id' => $current_user->company_id,
                    'comment' => $request->comment
                ];

                $interviewSchedule->comments()->create($scheduleComment);
            }

            if(!empty($request->employees) && $request->from != 'admin'){
                $employees = $request->employees;
                if($jobApplication->job->workflow_id){
                    $valid_users = [];
                    $time_slot = $jobApplication->schedule->slot_timing;
                    $duration = $application->workflow_stage->interview_action->timeslot;
                    $employees = [];
                    $panal = $jobApplication->schedule->user_accept_status == 'accept' ? [$jobApplication->schedule->employee[0]->user] : $jobApplication->workflow_stage->workflow_actions('interview')[0]->interview_panel();
                    foreach($panal as $interviewer){
                        if(in_array($time_slot, $this->getTimeSlots($interviewer, $request->scheduleDate, $duration, $interviewer->company->timezone, $interviewer->profile_schedule, (($interviewer->GC_channel_id ?? '') != '') ?  getGCEvents($interviewer) : $this->getExternalGEvents($interviewer)))){
                            $valid_users[] = $interviewer;
                            $employees[] = $interviewer->id;
                        }
                    }
                    $interviewSchedule->employees()->attach($employees, ['company_id' => $current_user->company_id]);
                    $user_emails = [];
                    foreach($valid_users as $vuser){
                        $employees[] = $vuser->id;
                        $user_emails[] = $vuser->email;
                        /* Send Email to all employees */
                        $interviewEmp = $interviewSchedule->employee->where('user_id',$vuser->id)->first() ?? new InterviewScheduleEmployee;
                        $path = url("interview-schedule/response/" .$interviewEmp->id. "/accept?direct=1&user_id={$vuser->id}&schedule_id={$interviewSchedule->id}");
                        $logo = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
                        $url = " <a href='". $path ."'>Click here to Accept the invite</a> ";
                        $subject = Option::getOption('schedule_request_subject', $jobApplication->company_id) ?? 'New Schedule Request';
                        $mesg = 'Dear [employee_name],<br><br> You have got a new schedule request of [applicant_name] for [job_title] on [schedule_date] [schedule_time] <br><br>[accept_url]<br><br>Thanks';
                        $mesg = Option::getOption('schedule_request_text', $jobApplication->company_id) ?? $mesg;
                        if($jobApplication->job->workflow_id != 0){
                            $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                            $subject = strtr($subject ?? '', [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                            $mesg = strtr($mesg, [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                        }else{
                            $subject = strtr($subject ?? '', [
                                '[interview_session_title]' => '',
                            ]);
                            $mesg = strtr($mesg, [
                                '[interview_session_title]' => '',
                            ]);
                        };
                        $subject = strtr($subject ?? '', [
                            '[company_name]' => $jobApplication->company->company_name,
                        ]);
                        
                        $mesg = strtr($mesg, [
                            '[applicant_name]'  => $jobApplication->full_name ?? '[applicant_name]',
                            '[applicant_email]'  => $jobApplication->email ?? '[applicant_email]',
                            '[employee_name]'   => $vuser->name ?? '[employee_name]',
                            '[company_name]'    => $jobApplication->job->company->company_name ?? '[company_name]',
                            '[schedule_date]'   => $jobApplication->schedule->count() ? ($jobApplication->schedule->status == 'pending' ? $jobApplication->schedule->schedule_date->format('M d, Y') : '[schedule_date]') : '[schedule_date]',
                            '[schedule_time]'   => $jobApplication->schedule->count() ? ($jobApplication->schedule->status == 'pending' ? $jobApplication->schedule->slot_timing : '[schedule_time]') : '[schedule_date]',
                            '[job_title]'       => $jobApplication->job->title ?? '[job_title]',
                            '[accept_url]'      => $url ?? '[accept_url]'
                            ]);
                            $data = array(
                                'from' => ['name' => 'WAZiE Recruit', 'email' => 'info@engyj.com'],
                                'to' => $vuser->email,
                                'message' =>  $logo . $mesg,
                                'subject' => $subject
                            );
                            if(Option::getOption('schedule_request_auto_email_enabled', $jobApplication->company_id) && !$auto_accept){
                                \Illuminate\Support\Facades\Mail::send(new \App\Mail\NewUserNotification($data));
                            }
                        /* End Send Email to all employees */
                        /* Send Schedule request Text Message */
                        if(Option::getOption('schedule_request_auto_sms_enabled', $jobApplication->company_id)){
                            $mesg = 'Dear [employee_name], You have got a new schedule request of [applicant_name] for [job_title] on [schedule_date] [schedule_time] [accept_url] Thanks';
                            $mesg = Option::getOption('sms_schedule_request_text', $jobApplication->company_id) ?? $mesg;
                            $mesg = strtr($mesg, [
                                '[applicant_name]'  => $jobApplication->full_name ?? '[applicant_name]',
                                '[applicant_email]' => $jobApplication->email ?? '[applicant_email]',
                                '[employee_name]'   => $vuser->name ?? '[employee_name]',
                                '[company_name]'    => $jobApplication->job->company->company_name ?? '[company_name]',
                                '[schedule_date]'   => $jobApplication->schedule->count() ? ($jobApplication->schedule->status == 'pending' ? $jobApplication->schedule->schedule_date->format('M d, Y') : '[schedule_date]') : '[schedule_date]',
                                '[schedule_time]'   => $jobApplication->schedule->count() ? ($jobApplication->schedule->status == 'pending' ? $jobApplication->schedule->slot_timing : '[schedule_time]') : '[schedule_date]',
                                '[job_title]'       => $jobApplication->job->title ?? '[job_title]',
                                '[accept_url]'      => $path ?? '[accept_url]'
                                ]);
                                if($jobApplication->job->workflow_id != 0){
                                    $mesg = strtr($mesg, [
                                        '[interview_session_title]' => $interview_action->session_title,
                                    ]); 
                                }else{
                                    $mesg = strtr($mesg, [
                                        '[interview_session_title]' => '',
                                    ]);
                                }
                            if($vuser->can_text && !$auto_accept){
                                    $to = "+1" . $vuser->mobile;
                                    $log = new \App\SmsLog;
                                    $log->company_id = $jobApplication->company_id;
                                    $log->message_type = 'outbound';
                                    $log->type = 'normal';
                                    $log->number = str_replace(' ', '',  $to);
                                    $log->message = $mesg;
                                    $log->status = 'pending';
                                    $log->send_at = date('Y-m-d h:i:s');
                                    $log->reference_name = 'admin-alert';
                                    $log->reference_id = $jobApplication->id;
                                    $log->save();
                            }
                        }
                        /* End Send Schedule request Text Message */
                    }//end foreach 
                }else{
                    $employees = $request->employees;
                }
                
                foreach ($employees as $emp) {
                    Option::setOption('employee_inteview_slot_timing_'.($request->scheduleDate), $request->slot_timing, $current_user->company_id, $emp);
                }
                if($jobApplication->job->workflow_id == 0){
                    if(in_array($current_user->id, $request->employees)) {
                        $interviewSchedule->employees()->attach($request->employees, ['company_id' => $current_user->company_id, 'user_accept_status' => 'accept']);
                        $interviewSchedule->user_accept_status = 'accept';
                        $employees = Arr::except($request->employees, $current_user->id);
                    }
                }
                
                if($jobApplication->job->workflow_id == 0){
                    try {
                    // Mail to employee for inform interview schedule
                    Notification::send($interviewSchedule->employees[0], new ScheduleInterview($jobApplication, $interviewSchedule,false,($request->comment ?? '')));
                    } catch(\Exception $e) {}
                }
                    $message = "You have been assigned to interview candidate {$jobApplication->full_name} for {$jobApplication->job->title} on " .
                    $interviewSchedule->schedule_date->timezone($jobApplication->company->timezone)->format('D, M d, Y') . 
                    " at {$interviewSchedule->slot_timing}." . 
                    ($interviewSchedule->location->location_name ? "\nLocation: {$interviewSchedule->location->location_name}" : "");
                    $reciever = $interviewSchedule->employees[0];
                    if($jobApplication->job->workflow_id == 0){
                        if($reciever->can_text){
                            $to = "+1" . $reciever->mobile;
                            $log = new \App\SmsLog();
                            $log->company_id = $jobApplication->company_id;
                            $log->message_type = 'outbound';
                            $log->type = 'reminder';
                            $log->number = str_replace(' ', '',  $to);
                            $log->message = $message;
                            $log->status = 'pending';
                            $log->send_at = date('Y-m-d h:i:s');
                            $log->reference_name = 'admin-alert';
                            $log->reference_id = $jobApplication->id;
                            $log->save();
                        }
                    }
            }

            
            try {
                $users = User::frontAllAdmins($current_user->company_id);
                $settings = ApplicationSetting::where('company_id', $jobApplication->company_id)->first();

                $reSchedule = ($request->has('reschedule') && $request->get('reschedule')) ? true : false;
                if(array_key_exists($status[0], $settings->mail_setting ?? []) && 
                    Arr::get($settings->mail_setting, $status[0].'.status')) {
                    // mail to candidate for inform interview schedule
                    if ($reSchedule && Option::getOption('reschedule_auto_email_enabled', $jobApplication->company_id)) {
                        Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule,$reSchedule));
                    }
                    else{
                        if (Option::getOption('schedule_auto_email_enabled', $jobApplication->company_id)) {
                            if($jobApplication->job->workflow_id == 0){
                                Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule,$reSchedule));
                            }
                        }
                    }
                }
                $interview_url = route('candidate.schedule-inteview') . 
                        "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";

                if(array_key_exists($status[0], $settings->sms_setting ?? []) && Arr::get($settings->sms_setting, $status[0].'.status')) {
                    $interview_url = route('candidate.schedule-inteview') . 
                            "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
                    $message = '';
                    $extraMaterial = '';
                    if($current_user->company_id == 11){
                        $linkMe = "https://us02web.zoom.us/j/87390114590";
                        $extraMaterial = "On that Day and Time Please copy and paste this link into a browser: \n" . $linkMe . "\n";  
                    }
                    $message .= "Dear {$jobApplication->full_name}, your video interview has been scheduled with {$users[0]->name} at {$jobApplication->job->company->company_name} on " . $interviewSchedule->schedule_date->format('M d, Y') . " from {$interviewSchedule->slot_timing}\n" . $extraMaterial;
                    $message .= "To Reschedule or Cancel click here:\n" . $interview_url;
                    $go = false;
                    $has_message = Option::getOption('sms_schedule_interview_text', $jobApplication->company_id);
                    if($jobApplication->job->workflow_id != 0){
                        $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                        if($interview_action->sms_schedule != 0){
                            $has_message = \App\SmsTemplate::find($interview_action->sms_schedule)->content;
                            $go = true;
                        }else{
                            if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                                $go = true;
                            }
                        }
                    }else{
                        if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                            $go = true;
                        }
                    }
                    if($go){
                        if($has_message){
                            $message = strtr($has_message, [
                                '[applicant_name]' => $jobApplication->full_name,
                                '[applicant_email]' => $jobApplication->email,
                                '[company_name]' => $company->company_name,
                                '[interview_url]' => $interview_url,
                                '[schedule_date]' => $interviewSchedule->schedule_date->timezone($company->timezone)->format('M d, Y'),
                                '[schedule_time]' => "{$interviewSchedule->slot_timing}",
                                '[job_title]'     => $jobApplication->job->title,
                                '[job_location]'  => "{$interviewSchedule->location->location_name}",
                                '[job_location_address]' => "{$interviewSchedule->location->location}",
                                '[employee_name]' => $users[0]->name,
                            ]);
                            if($jobApplication->job->workflow_id != 0){
                                $message = strtr($message ?? '', [
                                    '[interview_session_title]' => $interview_action->session_title,
                                ]);
                            }else{
                                $message = strtr($message ?? '', [
                                    '[interview_session_title]' => '',
                                ]);
                            }
                        }
                        if($jobApplication->job->workflow_id == 0){
                            $from = Option::getOption('twilio_from_number', $jobApplication->company_id);
                            $from = $from ?? config('twilio-notification-channel.from');
                            $log = new \App\SmsLog();
                            $log->company_id = $jobApplication->company_id;
                            $log->message_type = 'outbound';
                            $log->type = 'normal';
                            $log->number = str_replace(' ', '',  $jobApplication->phone);
                            $log->message = $message;
                            $log->status = 'pending';
                            $log->send_at = date('Y-m-d h:i:s');
                            $log->reference_name = 'applicant-alert';
                            $log->reference_id = $jobApplication->id;
                            $log->save();
                        }
                    }
                }

                
            } catch(\Exception $e) {
                Log::debug($e->getMessage());
            }
            if($jobApplication->job->workflow_id == 0){
                sendReminder($jobApplication, $users[0], $interviewSchedule, $dateTime);
            }
        }

        $redirect_to = route('jobs.jobOpenings', $company->career_page_link);
        if($request->from == 'api' && ($thank_you_page = Arr::get($company_settings, 'thanks_page_url'))) {
            $redirect_to = $thank_you_page;
        }

        $interviewSchedule->stage_id = $jobApplication->workflow_stage_id;
        $interviewSchedule->save();
        // Save Timeline
        
        if($jobApplication->job->workflow_id == 0){
            $interviewSchedule->google_event_id = $this->sendGEvent($request, $current_user, $company, $jobApplication, $interviewSchedule);
            $interviewSchedule->save();
        }else{
            if($interview_action->interview_invite && !$auto_accept  && $interviewSchedule->employees->count() == 1){
                $auto_accept = $interviewSchedule->employees->count();
                $auto_accept_user = $interviewSchedule->employees->first();
            }
        }

        if($auto_accept || $request->from == 'admin'){
                if($request->from == 'admin'){
                    // if($this->user->id == $current_user->id){
                    //     $interviewSchedule->google_event_id = $this->sendGEvent($request, $this->user, $company, $jobApplication, $interviewSchedule);
                    //     $interviewSchedule->save();
                    // }
                    $interviewSchedule->user_accept_status	 = ( $this->user->id == $current_user->id ) ? 'accept' : 'waiting' ;
                    $interviewSchedule->save();
                    $scheduleEmployee = new \App\InterviewScheduleEmployee;
                    $scheduleEmployee->company_id = $jobApplication->company_id;
                    $scheduleEmployee->interview_schedule_id = $interviewSchedule->id;
                    $scheduleEmployee->user_id = $current_user->id;
                    $scheduleEmployee->user_accept_status	 = ( $this->user->id == $current_user->id ) ? 'accept' : 'waiting' ;
                    $scheduleEmployee->save();
                    if($jobApplication->job->workflow_id){
                        $name = $this->user->name;
                        saveTimeline([
                            'applicant_id' =>   $jobApplication->id,
                            'entity'       =>   'interview_schedule',
                            'entity_id'    =>   $interviewSchedule->id,
                            'user_id'      =>   $current_user->id ?? 0,
                            'comments'     =>   "Interview Scheduled by {$name}" . (($this->user->id == $current_user->id) ? "" : (" for <b>" . $current_user->name . "</b>")) . " on <i>" .\Carbon\Carbon::parse($interviewSchedule->schedule_date)->timezone($jobApplication->company->timezone)->format('M d, Y') . " ({$interviewSchedule->slot_timing})</i>"
                        ]);
                    }
                }else{
                    $scheduleEmployee = InterviewScheduleEmployee::where('interview_schedule_id', '=', $interviewSchedule->id)->where('user_id', '=', $auto_accept_user->id)->first();                
                    $id = $scheduleEmployee->id;
                    $users = User::allAdmins(); // Get All admins for mail
                    // $jobApplication = \App\JobApplication::find($scheduleEmployee->schedule->job_application_id);
                    $company_id = $jobApplication->company_id;
                    $company = Company::find($company_id);
                    $status = ApplicationStatus::where('company_id', $company_id)
                            ->where('status', 'interview')
                            ->pluck('id');
                    $settings = ApplicationSetting::where('company_id', $company_id)->first();
            
                    $scheduleEmployee->user_accept_status = 'accept';
                    $scheduleEmployee->schedule->user_accept_status = 'accept';
                    $scheduleEmployee->save();
                    $scheduleEmployee->schedule->save();
                    if($jobApplication->job->workflow_id){
                        if(true){
                            /* Mail and sms to employee for inform interview schedule If Hiring Manager accept */
                            try {
                                Notification::send($scheduleEmployee->user, new ScheduleInterview($jobApplication, $scheduleEmployee->schedule,false,(\App\ScheduleComments::where('interview_schedule_id', '=', $scheduleEmployee->schedule->id)->first()->comment ?? '')));
                            } catch(\Exception $e) {}
                            if($scheduleEmployee->user->can_text){
                                    $message = "You have been assigned to interview candidate {$jobApplication->full_name} for {$jobApplication->job->title} on " .
                                    $scheduleEmployee->schedule->schedule_date->timezone($jobApplication->company->timezone)->format('D, M d, Y') . 
                                    " at {$scheduleEmployee->schedule->slot_timing}." . 
                                    ($scheduleEmployee->schedule->location->location_name ? "\nLocation: {$scheduleEmployee->schedule->location->location_name}" : "");
                                    $to = "+1" . $scheduleEmployee->user->mobile;
                                    $log = new \App\SmsLog;
                                    $log->company_id = $jobApplication->company_id;
                                    $log->message_type = 'outbound';
                                    $log->type = 'reminder';
                                    $log->number = str_replace(' ', '',  $to);
                                    $log->message = $message;
                                    $log->status = 'pending';
                                    $log->send_at = date('Y-m-d h:i:s');
                                    $log->reference_name = 'admin-alert';
                                    $log->reference_id = $jobApplication->id;
                                    $log->save();
                            }
                            /* End Mail and SMS to employee for inform interview schedule If Hiring Manager accept */
                            /* Mail and sms to all other employees for inform accepted and interview scheduled */
                            $otherScheduleEmployees = \App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $scheduleEmployee->interview_schedule_id)
                                                ->where('id','!=', $id)
                                                ->get();
                            foreach ($otherScheduleEmployees as $otherScheduleEmployee) {
                                try {
                                    Notification::send($otherScheduleEmployee->user, new AcceptInterviewRequest($jobApplication, $otherScheduleEmployee->schedule,$scheduleEmployee->user, true));
                                } catch(\Exception $e) {}
                                if($otherScheduleEmployee->user->can_text){
                                        $message = "Dear {$otherScheduleEmployee->user->name}, The interview of {$jobApplication->full_name} for the position of {$jobApplication->job->title} has been Accepted by {$scheduleEmployee->user->name}";
                                        $subject = "Interview Auto Accepted by {$scheduleEmployee->user->name}" . 
                                        ($otherScheduleEmployee->schedule->location->location_name ? "\nLocation: {$otherScheduleEmployee->schedule->location->location_name}" : "");
                                        $to = "+1" . $otherScheduleEmployee->user->mobile;
                                        $log = new \App\SmsLog;
                                        $log->company_id = $jobApplication->company_id;
                                        $log->message_type = 'outbound';
                                        $log->type = 'reminder';
                                        $log->number = str_replace(' ', '',  $to);
                                        $log->message = $message;
                                        $log->status = 'pending';
                                        $log->send_at = date('Y-m-d h:i:s');
                                        $log->reference_name = 'admin-alert';
                                        $log->reference_id = $jobApplication->id;
                                        $log->save();
                                }
                            }
                            /* End Mail and sms to all other employees for inform accepted and interview scheduled */
                        }
                    }
                    if($jobApplication->job->workflow_id != 0){
                        $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $jobApplication->workflow_stage_id)->first();
                    }
                    \App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $scheduleEmployee->interview_schedule_id)->where('id','!=', $id)->delete();
                    try {
                        if($jobApplication->job->workflow_id != 0){
                            if($interview_action->email_schedule){
                                Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                            }else{
                                if (Option::getOption('schedule_auto_email_enabled', $jobApplication->company_id)) {
                                    Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                                }
                            }
                        }else{
                            if (Option::getOption('schedule_auto_email_enabled', $jobApplication->company_id)) {
                                Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                            }
                        }
                    } catch(\Exception $e) {}
            
                    /* Send Sms to candidate */
                    if($jobApplication->job->workflow_id){
                        if(array_key_exists($status[0], $settings->sms_setting ?? []) && Arr::get($settings->sms_setting, $status[0].'.status')) {
                            $interview_url = route('candidate.schedule-inteview') . 
                                    "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
                            $message = '';
                            $extraMaterial = '';
                            if(!empty($current_user) && $current_user->company_id == 11){
                                $linkMe = "https://us02web.zoom.us/j/87390114590";
                                $extraMaterial = "On that Day and Time Please copy and paste this link into a browser: \n" . $linkMe . "\n";  
                            }
                            $message .= "Dear {$jobApplication->full_name}, your video interview has been scheduled with {$scheduleEmployee->user->name} at {$jobApplication->job->company->company_name} on " . $scheduleEmployee->schedule->schedule_date->format('M d, Y') . " from {$scheduleEmployee->schedule->slot_timing}\n" . $extraMaterial;
                            $message .= "To Reschedule or Cancel click here:\n" . $interview_url;
                            $go = false;
                            $has_message = Option::getOption('sms_schedule_interview_text', $jobApplication->company_id);
                            if($jobApplication->job->workflow_id != 0){
                                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                                if($interview_action->sms_schedule != 0){
                                    $has_message = \App\SmsTemplate::find($interview_action->sms_schedule)->content;
                                    $go = true;
                                }else{
                                    if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                                        $go = true;
                                    }
                                }
                            }else{
                                if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                                    $go = true;
                                }
                            }
                            if($go){     
                                if($has_message){
                                    $message = strtr($has_message, [
                                        '[applicant_name]' => $jobApplication->full_name,
                                        '[applicant_email]' => $jobApplication->email,
                                        '[company_name]' => $company->company_name,
                                        '[interview_url]' => $interview_url,
                                        '[schedule_date]' => $scheduleEmployee->schedule->schedule_date->timezone($company->timezone)->format('M d, Y'),
                                        '[schedule_time]' => "{$scheduleEmployee->schedule->slot_timing}",
                                        '[job_title]'     => $jobApplication->job->title,
                                        '[job_location]'  => "{$scheduleEmployee->schedule->location->location_name}",
                                        '[job_location_address]' => "{$scheduleEmployee->schedule->location->location}",
                                        '[employee_name]' => $scheduleEmployee->user->name,
                                    ]);
                                    if($jobApplication->job->workflow_id != 0){
                                        $message = strtr($message ?? '', [
                                            '[interview_session_title]' => $interview_action->session_title,
                                        ]);
                                    }else{
                                        $message = strtr($message ?? '', [
                                            '[interview_session_title]' => '',
                                        ]);
                                    }
                                }
                                    $from = Option::getOption('twilio_from_number', $jobApplication->company_id);
                                    $from = $from ?? config('twilio-notification-channel.from');
                                    $log = new \App\SmsLog;
                                    $log->company_id = $jobApplication->company_id;
                                    $log->message_type = 'outbound';
                                    $log->type = 'normal';
                                    $log->number = str_replace(' ', '',  $jobApplication->phone);
                                    $log->message = $message;
                                    $log->status = 'pending';
                                    $log->send_at = date('Y-m-d h:i:s');
                                    $log->reference_name = 'applicant-alert';
                                    $log->reference_id = $jobApplication->id;
                                    $log->save();
                            }
                        }
                    }
                }

        }else{
            $name = ($request->from == 'api') ? $jobApplication->full_name : $this->user->name;
            saveTimeline([
                'applicant_id' =>   $jobApplication->id,
                'entity'       =>   'interview_schedule',
                'entity_id'    =>   $interviewSchedule->id,
                'user_id'      =>   $this->user->id ?? 0,
                'comments'     =>   "Interview Scheduled by {$name} on <i>" .\Carbon\Carbon::parse($interviewSchedule->schedule_date)->timezone($jobApplication->company->timezone)->format('M d, Y') . " ({$interviewSchedule->slot_timing})</i>"
            ]);
        }
        if($auto_accept){
            $interviewSchedule->google_event_id = $this->sendGEvent($request, $auto_accept_user, $company, $application, $interviewSchedule);
            $interviewSchedule->save();
            sendReminder($jobApplication, $auto_accept_user, $interviewSchedule, $dateTime);
        }
        return Reply::redirect($redirect_to, __('menu.interviewSchedule').' '.__('messages.createdSuccessfully'));
    }

    public function changeStatus(Request $request){

        if($request->from != 'api') {
            abort_if(! $this->user->cans('add_schedule'), 403);
        }
        if($request->status != 'pending'){
            $user_id = InterviewScheduleEmployee::where("interview_schedule_id",'=',  $request->id)->first()->user_id;
            $user = User::find($user_id);
            $company_id = $user->company_id;
            if($user->GC_refresh_token != ''){
                $this->cancelGEvent($request->id, $company_id, $user->id);
            }   
        }
        $this->commonChangeStatusFunction($request->id, $request);

        return Reply::success(__('messages.interviewScheduleStatus'));
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return array
     */
    public function update(UpdateRequest $request, $id){
        if ($request->from == 'api') {
            $request->employees = $request->employee;
            $this->user = User::find($request->employees[0]);
        }

        abort_if(! $this->user->cans('add_schedule'), 403);
 
        $company = Company::find($this->user->company_id);
        $company_settings = Option::getAll($company->id);

        $scheduleTime = Str::before($request->slot_timing, ' - ');
        $scheduleTime = date('H:i', strtotime($scheduleTime));
        $dateTime =  $request->scheduleDate . ' ' . ($scheduleTime ?? '00:00');

        $dateTime = Carbon::createFromFormat('Y-m-d H:i', $dateTime, $company->timezone ?? 'GMT');
        $dateTime->setTimezone('GMT');

        // Update interview Schedule
        $interviewSchedule = InterviewSchedule::select('id', 'job_application_id', 'schedule_date', 'status', 'company_id','stage_id')
                            ->with([
                                'jobApplication:id,full_name,email,phone,job_id,status_id,company_id,workflow_stage_id',
                                'employees',
                                'comments'
                            ])
                            ->where('id', $id)->first();
        $go = true;
        if($interviewSchedule->jobApplication->job->workflow_id != 0){
            $go = \App\WorkflowStageActionInterview::where('stage_id', '=', $interviewSchedule->jobApplication->workflow_stage_id)->first()->show_location;
        }
        $slot_date = $interviewSchedule->schedule_date->format('Y-m-d');
        $interviewSchedule->location_id = $go ? ($request->location_id ?? str_replace('"]', "", str_replace('["',"", $interviewSchedule->jobApplication->job->location_id))) : 0;
        $interviewSchedule->slot_timing = $request->slot_timing;
        $interviewSchedule->schedule_date = $dateTime;
        $interviewSchedule->stage_id = $interviewSchedule->stage_id;
        $interviewSchedule->save();
        
        if($request->comment){
            $scheduleComment = [
                'comment' => $request->comment
            ];

            $interviewSchedule->comments()->updateOrCreate([
                'interview_schedule_id' => $interviewSchedule->id,
                'user_id' => $this->user->id
            ], $scheduleComment);
        }

        $jobApplication = $interviewSchedule->jobApplication;
        if(!empty($request->employee)){
            $employees = [];

            foreach ($request->employee as $employee) {
                $employees = Arr::add($employees, $employee, ['company_id' => $this->user->company_id]);

                Option::destroyOption('employee_inteview_slot_timing_' . $slot_date, $this->user->company_id, $employee);
                Option::setOption('employee_inteview_slot_timing_' . $request->scheduleDate, $request->slot_timing, $this->user->company_id, $employee);
            }
            
            $interviewSchedule->employees()->sync($employees);
            try {
                // Mail to employee for inform interview schedule
                Notification::send($interviewSchedule->employees[0], new ScheduleInterview($jobApplication, $interviewSchedule, true,($request->comment ?? '')));
            } catch(\Exception $e) {}
                        $message = "Your interview with the candidate {$jobApplication->full_name} for {$jobApplication->job->title} has been rescheduled to " .
                        $interviewSchedule->schedule_date->timezone($jobApplication->company->timezone)->format('D, M d, Y') . 
                        " at {$interviewSchedule->slot_timing}." . 
                        ($interviewSchedule->location->location_name ? "\nLocation: {$interviewSchedule->location->location_name}" : "");
                        $reciever = $interviewSchedule->employees[0];
                            if($reciever->can_text){
                                $to = "+1" . $reciever->mobile;
                                $log = new \App\SmsLog();
                                $log->company_id = $jobApplication->company_id;
                                $log->message_type = 'outbound';
                                $log->type = 'reminder';
                                $log->number = str_replace(' ', '',  $to);
                                $log->message = $message;
                                $log->status = 'pending';
                                $log->send_at = date('Y-m-d h:i:s');
                                $log->reference_name = 'admin-alert';
                                $log->reference_id = $jobApplication->id;
                                $log->save();
                        }
        }
        $reSchedule = ($request->has('reschedule') && $request->get('reschedule')) ? true : false;
        try {
            $users = User::frontAllAdmins($this->user->company_id);
            $status = ApplicationStatus::where('company_id', $this->user->company_id)
                ->where('status', 'interview')
                ->pluck('id');
                
            $settings = ApplicationSetting::where('company_id', $jobApplication->company_id)->first();
            if($jobApplication->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $jobApplication->workflow_stage_id)->first();
            }
            if (
                array_key_exists($status[0], $settings->mail_setting ?? []) &&
                Arr::get($settings->mail_setting, $status[0] . '.status')
            ) {
                if($jobApplication->job->workflow_id == 0){
                    if (Option::getOption('reschedule_auto_email_enabled', $jobApplication->company_id)) {
                        Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule,"true"));
                    }
                }else{
                    if($interview_action->email_reschedule){
                        Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule,"true"));
                        
                    }else{
                        if (Option::getOption('reschedule_auto_email_enabled', $jobApplication->company_id)) {
                            Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule,"true"));
                        }
                    }
                }

            }
            $token = JobApplication::find($jobApplication->id);
            $token = $token ? $token->email_token : '';
            $interview_url = route('candidate.schedule-inteview') . 
                    "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$token}";
            if (array_key_exists($status[0], $settings->sms_setting ?? []) && Arr::get($settings->sms_setting, $status[0] . '.status')) {
                $message = '';
                $message .= "Dear {$jobApplication->full_name}, Your phone interview for the {$jobApplication->job->title} position at {$jobApplication->job->company->company_name} has been rescheduled to " . $interviewSchedule->schedule_date->format('M d, Y') . " from {$interviewSchedule->slot_timing}". ($interviewSchedule->location->location_name ? "\nLocation: {$interviewSchedule->location->location_name}" : "");
                $go = false;
                $has_message = Option::getOption('sms_reschedule_interview_text', $jobApplication->company_id);
                if($jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                    if($interview_action->sms_reschedule != 0){
                        $has_message = \App\SmsTemplate::find($interview_action->sms_reschedule)->content;
                        $go = true;
                    }else{
                        if(Option::getOption('reschedule_auto_sms_enabled', $jobApplication->company_id)){
                            $go = true;
                        }
                    }
                }else{
                    if(Option::getOption('reschedule_auto_sms_enabled', $jobApplication->company_id)){
                        $go = true;
                    }
                }
                if($go){
                    if($has_message){
                        $message = strtr($has_message, [
                            '[applicant_name]' => $jobApplication->full_name,
                            '[applicant_email]' => $jobApplication->email,
                            '[company_name]' => $company->company_name,
                            '[interview_url]' => $interview_url,
                            '[job_title]' => "{$jobApplication->job->title}",
                            '[schedule_date]' => $interviewSchedule->schedule_date->timezone($company->timezone)->format('M d, Y'),
                            '[schedule_time]' => "{$interviewSchedule->slot_timing}",
                            '[employee_name]'   => $users[0]->name,
                            '[job_location]' => "{$interviewSchedule->location->location_name}",
                            '[job_location_address]' => "{$interviewSchedule->location->location}",
                        ]);
                        if($jobApplication->job->workflow_id != 0){
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                        }else{
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => '',
                            ]);
                        }
                    }
                    $from = Option::getOption('twilio_from_number', $jobApplication->company_id);
                    $from = $from ?? config('twilio-notification-channel.from');
                    $log = new \App\SmsLog();
                    $log->company_id = $jobApplication->company_id;
                    $log->message_type = 'outbound';
                    $log->type = 'normal';
                    $log->number = str_replace(' ', '',  $jobApplication->phone);
                    $log->message = $message;
                    $log->status = 'pending';
                    $log->send_at = date('Y-m-d h:i:s');
                    $log->reference_name = 'applicant-alert';
                    $log->reference_id = $jobApplication->id;
                    $log->save();
                }
            }
            sendReminder($jobApplication, $users[0], $interviewSchedule, $dateTime);

        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }

        $redirect_to = route('admin.interview-schedule.index');
        if($request->from == 'api') {
            $redirect_to = route('jobs.jobOpenings', $company->career_page_link);
            if ($thank_you_page = Arr::get($company_settings, 'thanks_page_url')) {
                $redirect_to = $thank_you_page;
            }
        }
        $this->cancelGEvent($jobApplication->schedule->id, $this->user->company_id, $this->user->id);

        $interviewSchedule->stage_id = $jobApplication->workflow_stage_id;
        $interviewSchedule->save();
        // Save Timeline
        $name = ($request->has('from') && $request->from == 'api') ? $jobApplication->full_name : $this->user->name;
        saveTimeline([
            'applicant_id' =>   $jobApplication->id,
            'entity'       =>   'interview_reschedule',
            'entity_id'    =>   $interviewSchedule->id,
            'user_id'      =>   $this->user->id ?? 0,
            'comments'     =>   "Interview Re-scheduled by {$name} on <i>" .\Carbon\Carbon::parse($interviewSchedule->schedule_date)->timezone($jobApplication->company->timezone)->format('M d, Y') . " ({$interviewSchedule->slot_timing})</i>"
        ]);
        $interviewSchedule->google_event_id = $this->sendGEvent($request, $this->user , $company, $jobApplication, $interviewSchedule);
        $interviewSchedule->save();
        return Reply::redirect($redirect_to, __('menu.interviewSchedule').' '.__('messages.updatedSuccessfully'));
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        abort_if(! $this->user->cans('delete_schedule'), 403);

        $interview = InterviewSchedule::with('jobApplication')->findOrFail($id);
        Option::destroyOption('sent_manual_interview_invite', $interview->company_id, $interview->jobApplication->id);
        SmsLog::ref('application', $interview->jobApplication->id)->delete();
        Option::destroyOption('employee_inteview_slot_timing_'.$interview->schedule_date->format('Y-m-d'), $this->user->company_id);

        InterviewSchedule::destroy($id);
        return Reply::success(__('messages.recordDeleted'));
    }

    /**
     * @param $id
     * @return string
     * @throws \Throwable
     */
    public function show(Request $request, $id)
    {
        abort_if(! $this->user->cans('view_schedule'), 403);
        $this->schedule = InterviewSchedule::with(['jobApplication', 'user'])->find($id);
        $this->currentDateTimestamp = Carbon::now(company()->timezone)->timestamp;
        $this->tableData = null;

        if($request->has('table')){
            $this->tableData = 'yes';
        }

        $this->questionnaires = \App\Questionnaire::where('company_id', '=', $this->user->company_id)->get();
        $actions = \App\WorkflowStageActionInterview::where('stage_id', '=', $this->schedule->jobApplication->workflow_stage_id)->get() ;
        $questionnaire = [];
        if($actions->count()){
            $questions = \App\WorkflowStageActionInterviewQuestionnaire::where('workflow_stage_action_interview_id', '=', $actions[0]->id)->get()->pluck('questionnaire_id');
            foreach($questions as $id){
                $questionnaire[] = \App\Questionnaire::find($id);
            }
            
        }
        $this->questionnaires =  $questionnaire;
        return view('admin.interview-schedule.show', $this->data)->render();
        
    }

    // notify and reminder to candidate on interview schedule
    public function notify($id, $type)
    {

        $jobApplication = JobApplication::find($id);

        if ($type == 'notify') {
            if ($jobApplication->status->status == 'hired') {
                // mail to candidate for hiring notify
                try{
                // Notification::send($jobApplication, new CandidateNotify());
                } catch(\Exception $e) {}
                return Reply::success(__('messages.notificationForHire'));
            }
            
            if ($jobApplication->status->status == 'rejected') {
                try{
                // mail to candidate for hiring notify
                // Notification::send($jobApplication, new CandidateRejected());
                } catch(\Exception $e) {}
                return Reply::success(__('messages.notificationForReject'));
            }
            
        } else {
            try{
            // mail to candidate for interview reminder
            Notification::send($jobApplication, new CandidateReminder( $jobApplication->schedule));
            } catch(\Exception $e) {}
            return Reply::success(__('messages.notificationForReminder'));
        }

    }

    // Employee response on interview schedule
    public function employeeResponse(Request $request,$id, $res){
        $scheduleEmployee = InterviewScheduleEmployee::find($id);
        if(empty($scheduleEmployee)){
            $user = User::find($request->user_id);
            $schedule = InterviewScheduleEmployee::where('interview_schedule_id',$request->schedule_id)->first();
            return redirect('schedule-request-thankyou-page/' . $user->company->career_page_link . "?already_accepted={$schedule->user->name}");
        }
        if($scheduleEmployee && InterviewScheduleEmployee::where('interview_schedule_id',$request->schedule_id)->where('user_accept_status','accept')->count()){
            $user = User::find($request->user_id);
            return redirect('schedule-request-thankyou-page/' . $user->company->career_page_link);
        }
        $users = User::allAdmins(); // Get All admins for mail
        $jobApplication = \App\JobApplication::find($scheduleEmployee->schedule->job_application_id);
        $company_id = ($request->has('direct') || empty($this->user)) ? $jobApplication->company_id : $this->user->company_id;
        $company = Company::find($company_id);
        $status = ApplicationStatus::where('company_id', $company_id)
                ->where('status', 'interview')
                ->pluck('id');
        $settings = ApplicationSetting::where('company_id', $company_id)->first();
        $type = 'refused';

        if($res == 'accept'){  
            $type = 'accepted';
            $scheduleEmployee->schedule->google_event_id = $this->sendGEvent($request, $scheduleEmployee->user, $company, $jobApplication,$scheduleEmployee->schedule);
            $scheduleEmployee->schedule->save(); 
        }

        $scheduleEmployee->user_accept_status = $res;
        $scheduleEmployee->schedule->user_accept_status = $res;
        $scheduleEmployee->save();
        $scheduleEmployee->schedule->save();

        if($jobApplication->job->workflow_id){
            if($res == 'accept'){
                /* Mail and sms to employee for inform interview schedule If Hiring Manager accept */
                try {
                    Notification::send($scheduleEmployee->user, new ScheduleInterview($jobApplication, $scheduleEmployee->schedule,false,(\App\ScheduleComments::where('interview_schedule_id', '=', $scheduleEmployee->schedule->id)->first()->comment ?? '')));
                } catch(\Exception $e) {}
                if($scheduleEmployee->user->can_text){
                        $message = "You have been assigned to interview candidate {$jobApplication->full_name} for {$jobApplication->job->title} on " .
                        $scheduleEmployee->schedule->schedule_date->timezone($jobApplication->company->timezone)->format('D, M d, Y') . 
                        " at {$scheduleEmployee->schedule->slot_timing}." . 
                        ($scheduleEmployee->schedule->location->location_name ? "\nLocation: {$scheduleEmployee->schedule->location->location_name}" : "");
                        $to = "+1" . $scheduleEmployee->user->mobile;
                        $log = new \App\SmsLog;
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'reminder';
                        $log->number = str_replace(' ', '',  $to);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'admin-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                }
                /* End Mail and SMS to employee for inform interview schedule If Hiring Manager accept */
                /* Mail and sms to all other employees for inform accepted and interview scheduled */
                $otherScheduleEmployees = \App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $scheduleEmployee->interview_schedule_id)
                                    ->where('id','!=', $id)
                                    ->get();
                foreach ($otherScheduleEmployees as $otherScheduleEmployee) {
                    try {
                        Notification::send($otherScheduleEmployee->user, new AcceptInterviewRequest($jobApplication, $otherScheduleEmployee->schedule,$scheduleEmployee->user));
                    } catch(\Exception $e) {}
                    if($otherScheduleEmployee->user->can_text){
                            $message = "Dear {$otherScheduleEmployee->user->name}, The interview of {$jobApplication->full_name} for the position of {$jobApplication->job->title} has been Accepted by {$scheduleEmployee->user->name}";
                            $subject = "Interview Accepted by {$scheduleEmployee->user->name}" . 
                            ($otherScheduleEmployee->schedule->location->location_name ? "\nLocation: {$otherScheduleEmployee->schedule->location->location_name}" : "");
                            $to = "+1" . $otherScheduleEmployee->user->mobile;
                            $log = new \App\SmsLog;
                            $log->company_id = $jobApplication->company_id;
                            $log->message_type = 'outbound';
                            $log->type = 'reminder';
                            $log->number = str_replace(' ', '',  $to);
                            $log->message = $message;
                            $log->status = 'pending';
                            $log->send_at = date('Y-m-d h:i:s');
                            $log->reference_name = 'admin-alert';
                            $log->reference_id = $jobApplication->id;
                            $log->save();
                    }
                }
                /* End Mail and sms to all other employees for inform accepted and interview scheduled */
            }
        }
        if($jobApplication->job->workflow_id != 0){
            $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $jobApplication->workflow_stage_id)->first();
        }
        \App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $scheduleEmployee->interview_schedule_id)->where('id','!=', $id)->delete();
        try {
            if($jobApplication->job->workflow_id != 0){
                if($interview_action->email_schedule){
                    Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                }else{
                    if (Option::getOption('schedule_auto_email_enabled', $jobApplication->company_id)) {
                        Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                    }
                }
            }else{
                if (Option::getOption('schedule_auto_email_enabled', $jobApplication->company_id)) {
                    Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $scheduleEmployee->schedule, false));
                }
            }
        } catch(\Exception $e) {}

        /* Send Sms to candidate */
        if($jobApplication->job->workflow_id){
            if(array_key_exists($status[0], $settings->sms_setting ?? []) && Arr::get($settings->sms_setting, $status[0].'.status')) {
                $interview_url = route('candidate.schedule-inteview') . 
                        "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
                $message = '';
                $extraMaterial = '';
                if(!empty($this->user) && $this->user->company_id == 11){
                    $linkMe = "https://us02web.zoom.us/j/87390114590";
                    $extraMaterial = "On that Day and Time Please copy and paste this link into a browser: \n" . $linkMe . "\n";  
                }
                $message .= "Dear {$jobApplication->full_name}, your video interview has been scheduled with {$scheduleEmployee->user->name} at {$jobApplication->job->company->company_name} on " . $scheduleEmployee->schedule->schedule_date->format('M d, Y') . " from {$scheduleEmployee->schedule->slot_timing}\n" . $extraMaterial;
                $message .= "To Reschedule or Cancel click here:\n" . $interview_url;
                $go = false;
                $has_message = Option::getOption('sms_schedule_interview_text', $jobApplication->company_id);
                if($jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                    if($interview_action->sms_schedule != 0){
                        $has_message = \App\SmsTemplate::find($interview_action->sms_schedule)->content;
                        $go = true;
                    }else{
                        if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                            $go = true;
                        }
                    }
                }else{
                    if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                        $go = true;
                    }
                }
                if($go){     
                    if($has_message){
                        $message = strtr($has_message, [
                            '[applicant_name]' => $jobApplication->full_name,
                            '[applicant_email]'=> $jobApplication->email,
                            '[company_name]' => $company->company_name,
                            '[interview_url]' => $interview_url,
                            '[schedule_date]' => $scheduleEmployee->schedule->schedule_date->timezone($company->timezone)->format('M d, Y'),
                            '[schedule_time]' => "{$scheduleEmployee->schedule->slot_timing}",
                            '[job_title]'     => $jobApplication->job->title,
                            '[job_location]'  => "{$scheduleEmployee->schedule->location->location_name}",
                            '[job_location_address]' => "{$scheduleEmployee->schedule->location->location}",
                            '[employee_name]' => $scheduleEmployee->user->name,
                        ]);
                        if($jobApplication->job->workflow_id != 0){
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                        }else{
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => '',
                            ]);
                        }
                    }
                        $from = Option::getOption('twilio_from_number', $jobApplication->company_id);
                        $from = $from ?? config('twilio-notification-channel.from');
                        $log = new \App\SmsLog;
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                }
            }
            sendReminder($jobApplication, $scheduleEmployee->user, $scheduleEmployee->schedule, $scheduleEmployee->schedule->schedule_date);
        }
        /* End Send Sms to candidate */
        if($request->has('direct')){
            return redirect('schedule-request-thankyou-page/' . $jobApplication->company->career_page_link);
        }
        return Reply::success(__('messages.responseAppliedSuccess'));

    }

    public function changeStatusMultiple(Request $request){
        abort_if(! $this->user->cans('edit_schedule'), 403);
        foreach($request->id as $ids)
        {
            $this->commonChangeStatusFunction($ids, $request);
        }

        return Reply::success(__('messages.interviewScheduleStatus'));
    }

    public function commonChangeStatusFunction($id, $request)
    {
        // store Schedule
        $interviewSchedule = InterviewSchedule::select('id', 'job_application_id', 'status', 'schedule_date', 'company_id', 'stage_id')
        ->with([
            'jobApplication:id,company_id,full_name,email,phone,job_id,status_id,workflow_stage_id',
            'employees',
            'jobApplication.job.company',
            ])
            ->where('id', $id)->first();
            $interviewSchedule->status = $request->status;
                if($request->status == 'canceled') {
                    $interviewSchedule->user_accept_status = 'refuse';
                }
        $application = $interviewSchedule->jobApplication;
        $go = false;
        if(in_array($request->status,['canceled','pass','fail'])){
            $name = ($request->has('from') && $request->from == 'api') ? $interviewSchedule->jobApplication->full_name : $this->user->name;
            saveTimeline([
                'applicant_id' =>   $interviewSchedule->jobApplication->id,
                'entity'       =>   'interview_'.$request->status,
                'entity_id'    =>   $interviewSchedule->id,
                'user_id'      =>   $this->user->id ?? 0,
                'comments'     =>   "{$name} ". ucFirst($request->status) . ($request->status != 'canceled' ? 'ed' : '') . ($name != $interviewSchedule->jobApplication->full_name ? " {$interviewSchedule->jobApplication->full_name}'s" : "") . " interview"
            ]);
            $interviewSchedule->user_accept_status = 'done';
            $interview_action = $interviewSchedule->stage->interview_action;
            $if = 'if_'.$request->status;
            session()->put('previous_stage_session_title', $interviewSchedule->stage->interview_action->session_title);
            session()->put('has_triggered', $interview_action->$if);
            if($interview_action->$if != 0){
                $go = true;
            }
        }
                $interviewSchedule->save();
                
                $status = ApplicationStatus::company($application->company_id)->select('id', 'status');
                
                if(in_array($request->status, ['canceled'])){
                    $applicationStatus = $status->status('applied');
                }
                if(in_array($request->status, ['rejected'])){
                    $applicationStatus = $status->status('rejected');
                }
                if($request->status === 'hired'){
                    $applicationStatus = $status->status('hired');
                }
                if ($request->status === 'pending') {
                    $applicationStatus = $status->status('interview');
                }
                if($interviewSchedule->jobApplication->job->workflow_id == 0){
                    $applicantHistory = new ApplicantHistory();
                    $applicantHistory->current_value = $applicationStatus->id;
                    $applicantHistory->previous_value = $application->status_id;
                    $applicantHistory->status_type = 'application';
                    $applicantHistory->company_id = $application->company_id;
                    $applicantHistory->applicant_id = $application->id;
                    $applicantHistory->save();
                    $application->status_id = $applicationStatus->id;
                }
        
        $application->email_token = Str::random(42);
        $application->save();
        if($go){
            $applicantHistory = new ApplicantHistory();
            $applicantHistory->current_value = $interview_action->$if;
            $applicantHistory->previous_value = $interviewSchedule->stage_id;
            $applicantHistory->status_type = 'application';
            $applicantHistory->company_id = $application->company_id;
            $applicantHistory->applicant_id = $application->id;
            $applicantHistory->save();
            $application->workflow_stage_id = $interview_action->$if;
            if(in_array($interview_action->if($request->status)->type, ['hired', 'rejected'])){
                $application->hiring_status = $interview_action->if($request->status)->type;
            }
            $application->save();
            // Save Timeline
            saveTimeline([
                'applicant_id' =>   $application->id,
                'entity'       =>   'stage',
                'entity_id'    =>   $interview_action->$if,
                'user_id'      =>   $this->user->id ?? 0,
                'comments'     =>   "Stage Moved to {$applicantHistory->stage->title} by Trigger (".ucfirst($request->status).")"
            ]);
            $changeStage = new AdminJobApplicationController();
            $changeStage->performStageAction($application);
        }
        $employees = $interviewSchedule->employees;
        $admins = User::allAdmins($interviewSchedule->company_id, $interviewSchedule->company_id);
        if($request->from == 'api') {
            $this->user = User::find($admins[0]->id);
            Option::destroyOption('sent_manual_interview_invite', $interviewSchedule->company_id, $application->id);
        }
        
        $users = $employees->merge($admins);
        if($request->status != 'pending') {
            if(!is_null($interviewSchedule->schedule_date)) {
                Option::destroyOption('employee_inteview_slot_timing_'.$interviewSchedule->schedule_date->format('Y-m-d'), $this->user->company_id);
            }
        }
        
        SmsLog::ref('application', $application->id)->status('pending')->delete();
        
        if($users){
            // Mail to employee for inform interview schedule
            try{
                Notification::send($users[0], new ScheduleInterviewStatus($application, $request->status == 'canceled'));
            } catch(\Exception $e) {}
        }

        /* If Status is rejected */
        if($request->status==='rejected'){
            $is_allowed = Option::getOption('rejected_auto_email_enabled', $application->company_id);
            if($is_allowed && $request->mailToCandidate ==  'yes'){
                Notification::send($interviewSchedule->jobApplication, new ScheduleStatusCandidate($interviewSchedule->jobApplication, $interviewSchedule));
            }
            // Send sms notification to admin on update status
            $this->sendStatusChangeSms($request->status,$application,$applicationStatus);
            return;
        }
        /* end If Status is rejected */
        /* If Status is hired */
        if($request->status==='hired'){
            $is_hired_allowed = Option::getOption('hired_auto_email_enabled', $application->company_id);
            if($is_hired_allowed && $request->mailToCandidate ==  'yes'){
                Notification::send($interviewSchedule->jobApplication, new ScheduleStatusCandidate($interviewSchedule->jobApplication, $interviewSchedule));
            }
            // Send sms notification to admin on update status
            $this->sendStatusChangeSms($request->status,$application,$applicationStatus);
            return;
        }
        /* end If Status is hired */

        if(in_array($request->status, ['canceled'])) {
            $reciever = User::where('company_id', '=', $interviewSchedule->jobApplication->company_id)->first();
            if($reciever->can_text){
                $log = new SmsLog();
                $log->company_id = $interviewSchedule->jobApplication->company_id;
                $log->message_type = 'outbound';
                $log->type = 'reminder';
                $log->number = "+1" . $reciever->mobile;
                $log->message = "Your interview with {$interviewSchedule->jobApplication->full_name} has been cancelled";
                $log->status = 'pending';
                $log->send_at = date('Y-m-d h:i:s');
                $log->reference_name = 'admin-canceled-alert';
                $log->reference_id = $interviewSchedule->jobApplication->id;
                $log->save();
            }

            $interview_url = route('candidate.schedule-inteview') . 
                    "?c={$interviewSchedule->jobApplication->company_id}&app={$interviewSchedule->jobApplication->id}&t={$interviewSchedule->jobApplication->email_token}";

            $message = "Dear {$application->full_name},\nYour phone interview at {$application->job->company->company_name} has been cancelled.";
            $go = false;
            $has_message = Option::getOption('sms_cancel_interview_text', $interviewSchedule->jobApplication->company_id);
            if($interviewSchedule->jobApplication->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$interviewSchedule->jobApplication->job->workflow_id)->first() ?? new \App\WorkflowStageActionInterview;
                if($interview_action->sms_cancelled != 0){
                    // $has_message = \App\SmsTemplate::find($interview_action->sms_cancelled)->content;
                    // $go = true;
                }
                else{
                    if(Option::getOption('cancel_auto_sms_enabled', $interviewSchedule->jobApplication->company_id)){
                        $go = true;
                    }
                }
            }else{
                if(Option::getOption('cancel_auto_sms_enabled', $interviewSchedule->jobApplication->company_id)){
                    $go = true;
                }
            }
            if($go){
                if($has_message){
                    $message = strtr($has_message, [
                        '[applicant_name]' => $interviewSchedule->jobApplication->full_name,
                        '[applicant_email]' => $interviewSchedule->jobApplication->email,
                        '[company_name]' => $interviewSchedule->jobApplication->company->company_name,
                        '[status_name]' => "cancelled",
                        '[job_title]'       => $interviewSchedule->jobApplication->job->title,
                        '[interview_url]'   => $interview_url,
                        '[employee_name]'   => $users[0]->name,
                        '[schedule_date]'   => $interviewSchedule->schedule_date->format('M d, Y'),
                        '[schedule_time]'   => $interviewSchedule->slot_timing,
                    ]);
                    if($interviewSchedule->jobApplication->job->workflow_id != 0){
                        $message = strtr($message ?? '', [
                            '[interview_session_title]' => $interview_action->session_title,
                        ]);
                    }else{
                        $message = strtr($message ?? '', [
                            '[interview_session_title]' => '',
                        ]);
                    }
                }
                $from = Option::getOption('twilio_from_number', $interviewSchedule->jobApplication->company_id);
                $from = $from ?? config('twilio-notification-channel.from');
    
                $log = new \App\SmsLog();
                $log->company_id = $interviewSchedule->jobApplication->company_id;
                $log->message_type = 'outbound';
                $log->type = 'normal';
                $log->number = str_replace(' ', '',  $interviewSchedule->jobApplication->phone);
                $log->message = $message;
                $log->status = 'pending';
                $log->send_at = date('Y-m-d h:i:s');
                $log->reference_name = 'applicant-canceled-alert';
                $log->reference_id = $interviewSchedule->jobApplication->id;
                $log->save();
            }
            if($interviewSchedule->jobApplication->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $interviewSchedule->jobApplication->workflow_stage_id)->first();
            }
            try {
                if($interviewSchedule->jobApplication->job->workflow_id != 0){
                    if($interview_action->email_cancelled){
                        Notification::send($interviewSchedule->jobApplication, new ScheduleStatusCandidate($interviewSchedule->jobApplication, $interviewSchedule, true));
                    }else{
                        if (Option::getOption('cancel_auto_email_enabled', $interviewSchedule->jobApplication->company_id) && $request->mailToCandidate ==  'yes') {
                            Notification::send($interviewSchedule->jobApplication, new ScheduleStatusCandidate($interviewSchedule->jobApplication, $interviewSchedule, true));
                        }
                    }
                }else{
                    if (Option::getOption('cancel_auto_email_enabled', $interviewSchedule->jobApplication->company_id) && $request->mailToCandidate ==  'yes') {
                        Notification::send($interviewSchedule->jobApplication, new ScheduleStatusCandidate($interviewSchedule->jobApplication, $interviewSchedule, true));
                    }
                }
            } catch(\Exception $e) {}
        
        }
        if(in_array($request->status, ['canceled'])){
            $allPending = \App\SmsLog::where('reference_id', $interviewSchedule->jobApplication->id)->where('status','pending')->get();
            if($allPending->count()){
                foreach($allPending as $pending){
                    if(in_array($pending->reference_name, ['applicant-reminder-alert', 'admin-reminder-alert'])){
                        $pending->status = 'canceled';
                        $pending->save();
                    }
                }
            }
        }

        return;
    }

    public function getTiming(Request $request)
    {
        if($request->has('app') && $request->get('app')){
            $application = JobApplication::findOrNew($request->get('app'));
        }
        elseif($request->has('candidates') && count($request->candidates)==1){
            $application = JobApplication::findOrNew($request->candidates[0] ?? '');            
        }
        else{
            $application = new JobApplication;
        }
        $user = User::find($request->employees)->first();
        $duration = ($application->job->workflow_id) ? ( $application->workflow_stage->interview_action->timeslot == 0 ? 30 :  $application->workflow_stage->interview_action->timeslot ) : Arr::get($user->profile_schedule, 'global.interview_slot', '30');
        $googleSlots = (($user->GC_channel_id ?? '') != '') ?  getGCEvents($user) : $this->getExternalGEvents($user);
        if($application->job->workflow_id){
            $stage = $application->workflow_stage;
            $multislots = [];
            $i = 0;
            if(!isset($stage->workflow_actions('interview')[0])){
                return Reply::error('This stage has no schedule');
            }
            $panal = $application->schedule->user_accept_status == 'accept' ? [$application->schedule->employee[0]->user] : $stage->workflow_actions('interview')[0]->interview_panel();
            foreach($panal as $interviewer){
                $multislots = array_merge($multislots, $this->getTimeSlots($interviewer, $request->date, $duration, $interviewer->company->timezone, $interviewer->profile_schedule, ((($interviewer->GC_channel_id ?? '') != '') ?  getGCEvents($interviewer) : $this->getExternalGEvents($interviewer))));                
            }
            $all_slots = array_unique($multislots);
            usort($all_slots,function ($a, $b)
            {
                $a = strtotime(explode(' - ',$a)[0]);
                $b = strtotime(explode(' - ',$b)[0]);
                if ($a == $b) {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            });
        }else{
            $all_slots = $this->getTimeSlots($user, $request->date, $duration, $user->company->timezone, $user->profile_schedule, $googleSlots);
        }

        return Reply::dataOnly([
            'data' => $all_slots
        ]);
    }

    function getTimeSlots($user, $current_date, $duration, $timezone, $schedule, $googleSchedules, $jobApplication = null ){
        $event_slots = array();
        foreach($googleSchedules as $gEvent){
            $date = explode(" ", $gEvent["start"]);
            $start = $date[1] . " " . $date[2];
            $pre_end = explode(" ", $gEvent["end"]);
            $end = $pre_end[1] . " " . $pre_end[2];
            if($pre_end[0] == $date[0]){
                $event_slots[$date[0]][] = ['start' => $start, 'end' => $end];
            }else{
                $st_date = Carbon::createFromFormat('Y-m-d', $date[0]);
                $ed_date = Carbon::createFromFormat('Y-m-d', $pre_end[0]);
                for($i = 0; $i <= $st_date->diffInDays($ed_date); $i++){
                    $dat = Carbon::createFromFormat('Y-m-d', $pre_end[0])->subDays($i)->format('Y-m-d');
                    $event_slots[$dat][] = ['start'=>'12:00 AM', 'end' => '11:59 PM'];
                }
            }
        }
        $employees = $user;
        
        $day = Carbon::createFromFormat('Y-m-d H:i', ($current_date . ' 00:00'), $timezone);
        $day = strtolower($day->format('l'));

        $time_ins = $time_outs = $slots = $break_times = [];
        $time_slots = [];

                $time_in = Arr::get($schedule, 'global.time_in', '09:00');
                $time_out = Arr::get($schedule, 'global.time_out', '18:00');
                $interview_slot = $duration;
                 
                $break_start = Arr::get($schedule, 'global.break_start', '13:00');
                $break_end = Arr::get($schedule, 'global.break_end', '14:00');
                $exclude_lunch = Arr::get($schedule, 'global.exclude_lunch', 0);

                $available_anytime = Arr::get($schedule, 'global.available_anytime', 0);
                if(!$available_anytime) {
                    $active = Arr::get($schedule, 'days.'. $day .'.active', 0);
                    $time_in = Arr::get($schedule, 'days.'. $day .'.time_in', '09:00');
                    $time_out = Arr::get($schedule, 'days.'. $day .'.time_out', '18:00');
                    $day_break_start = Arr::get($schedule, 'days.'. $day .'.break_start', '12:00');
                    $day_break_end = Arr::get($schedule, 'days.'. $day .'.break_end', '13:00');
                    $day_active_break = Arr::get($schedule, 'days.'. $day .'.active_break', 0);
                    if(!$active) {
                        $time_in = $time_out = '';
                    }
                }

                if (!empty($time_in) && !empty($time_out)) {
                    $time_ins[] = date('Y-m-d H:i', strtotime($time_in));
                    $time_outs[] = date('Y-m-d H:i', strtotime($time_out));

                    if($exclude_lunch) {
                        $break_times = [date('Y-m-d H:i', strtotime($break_start)), date('Y-m-d H:i', strtotime($break_end))];
                    }
                    if($day_active_break){
                        $break_times = [date('Y-m-d H:i', strtotime($day_break_start)), date('Y-m-d H:i', strtotime($day_break_end))];
                    }
                }

            if(count($time_ins) == 0) {
                Reply::error('The day is already occupied!');
                return [];
            }

            if( in_array($current_date,blockHolidays([$current_date],$employees->pluck('company_id')->toArray())) ) {
                Reply::error('This day is Holiday!');
                return [];
            }

            $block_custom_event_times = blockTimeSlots($current_date,$time_slots,$interview_slot,$employees->pluck('id')->toArray());
            if($employees->count() && count( blockCompleteDay([$current_date],$employees->pluck('id')->toArray()) ) && count($block_custom_event_times) == 0 ) {
                Reply::error('The day is occupied by custom event!');
                return [];
            }

            $time_start = max($time_ins);
            $time_end = min($time_outs);
            if($time_start > $time_end) {
                $time_start = date('Y-m-d H:i', strtotime('-1 day', strtotime($time_start)));
            }
            $shift_start_constant = $shift_start = date('h:i A', strtotime($time_ins[0]));
            $shift_end =  date('h:i A', strtotime($time_outs[0]));
            while (strtotime($shift_start) < strtotime($shift_end)) {
                    $time = new \DateTime($shift_start);
                    $time->add(new \DateInterval('PT' . $duration . 'M'));
                    $slot_end_time = $time->format('h:i A');
                    if(strtotime($slot_end_time) <= strtotime($shift_end) && strtotime($slot_end_time) >= strtotime($shift_start_constant)){
                        $time_slots[] = $shift_start . " - " . $slot_end_time;
                    }else{
                        break;
                    }
                    $shift_start = $slot_end_time;
            }
            if($day_active_break) {
                $break_gap = date('h:i A', strtotime($break_times[0])) . " - " .  date('h:i A', strtotime($break_times[1]));
                $time_slots = $this->filterSlots($time_slots, $break_gap);
            }
            if(!empty($event_slots)) {
                if(isset($event_slots[$current_date])){
                    foreach($event_slots[$current_date] as $key=>$gce_date){
                        $google_slot_gap = $event_slots[$current_date][$key]['start'] . " - " .  $event_slots[$current_date][$key]['end'];
                        if($event_slots[$current_date][$key]['start'] == '12:00 AM' && $event_slots[$current_date][$key]['end'] == '11:59 PM'){
                            $google_slot_gap =  $user->block_all_day_event ? $event_slots[$current_date][$key]['start'] . " - " .  $event_slots[$current_date][$key]['end'] : ' - ';
                        }
                        $time_slots = $this->filterSlots($time_slots, $google_slot_gap);
                    }
                }
            }

            $from = date('Y-m-d', strtotime($current_date. ' + 1 day'));
            $to = date('Y-m-d', strtotime($current_date. ' - 1 day'));
            $used_slots = InterviewScheduleEmployee::leftJoin("interview_schedules", 'interview_schedules.id', 'interview_schedule_employees.interview_schedule_id')
            ->where('interview_schedule_employees.user_id', $user->id)
            ->where('interview_schedules.status', '=', 'pending')
            ->whereBetween('interview_schedules.schedule_date',[$to, $from])
            ->get();
            
            if($used_slots->count() != 0){
                foreach($used_slots as $used_slot){
                    $dt = Carbon::parse($used_slot->schedule_date)->timezone($timezone)->format('Y-m-d h:i A');
                    $dt = explode(" ", $dt);
                    $date = $dt[0];
                    $start = $dt[1];
                    if($current_date == $date){
                        $ts = explode(" - ", $used_slot->slot_timing)[0];
                        foreach($time_slots as $key=>$slot){
                            
                            if($ts == explode(" - ", $slot)[0]){
                                unset($time_slots[$key]);
                                
                            }
                        } 
                    } 
                }
            }
            $remove = [];

        if(count($block_custom_event_times)){
            $remove = $block_custom_event_times;
        }
        /* Same Day Interview Schedule */
        $today_time_in = Arr::get($schedule, 'days.'. strtolower(now()->format('l')) .'.time_in');
        $jobApplication = $jobApplication ?? new JobApplication;
        if($jobApplication->workflow_stage->interview_action->same_day_schedule && array_key_exists(strtolower(now()->format('l')),Arr::get($schedule, 'days'))){
            $start = Carbon::parse($time_in)->format('g:i A');
            $remove = createSlots($start, now($timezone)->format('h:i A'), $interview_slot);
        }
        /* End Same Day Interview Schedule */
        return array_values(array_diff($time_slots, $remove));
    }

    function filterSlots($time_slots, $gap){
        $remove = [];
        foreach($time_slots as $k=>$v){  
            if( $this->isOverlaping( $v, $gap) ){
                $remove[] = $v;
            }
        }
        return array_values(array_diff($time_slots, $remove));
    }

    function isOverlaping($rang_1, $rang_2){
        $rang_1 = explode(' - ', $rang_1);
        $R1_start = strtotime($rang_1[0]);
        $R1_end = strtotime($rang_1[1]);
        $rang_2 = explode(' - ', $rang_2);
        $R2_start = strtotime($rang_2[0]);
        $R2_end = strtotime($rang_2[1]);
        if( ($R1_start >= $R2_start && $R1_start < $R2_end ) || ( $R1_end > $R2_start &&  $R1_end <= $R2_start) ){
            return true;
        }
        if( ($R2_start >= $R1_start && $R2_start < $R1_end ) || ( $R2_end > $R1_start &&  $R2_end <= $R1_start) ){
            return true;
        }
        return false;
    }

    private function getAllSlots($c, $date, $employees, $event_slots, $application){
        $employee = User::select('id', 'company_id', 'time_in', 'time_out', 'slot', 'profile_schedule')->find($employees);
        $company   = Company::find($employee->company_id);
        
        $day = Carbon::createFromFormat('Y-m-d H:i', ($date . ' 00:00'), $company->timezone);
        $day = strtolower($day->format('l'));

        $time_ins = $time_outs = $slots = $break_times = [];
        $time_slots = [];
            $schedule = $employee->profile_schedule;

            $time_in = Arr::get($schedule, 'global.time_in', '09:00');
            $time_out = Arr::get($schedule, 'global.time_out', '18:00');
            $interview_slot = ($application->job->workflow_id) ? ( $application->workflow_stage->interview_action->timeslot == 0 ? Arr::get($schedule, 'global.interview_slot', '30') :  $application->workflow_stage->interview_action->timeslot ) : Arr::get($schedule, 'global.interview_slot', '30');

            $break_start = Arr::get($schedule, 'global.break_start', '13:00');
            $break_end = Arr::get($schedule, 'global.break_end', '14:00');
            $exclude_lunch = Arr::get($schedule, 'global.exclude_lunch', 0);

            $available_anytime = Arr::get($schedule, 'global.available_anytime', 0);
            if(!$available_anytime) {
                $active = Arr::get($schedule, 'days.'. $day .'.active', 0);
                $time_in = Arr::get($schedule, 'days.'. $day .'.time_in', '13:00');
                $time_out = Arr::get($schedule, 'days.'. $day .'.time_out', '18:00');

                if(!$active) {
                    $time_in = $time_out = '';
                }
            }

            if (!empty($time_in) && !empty($time_out)) {
                $time_ins[] = date('Y-m-d H:i', strtotime($time_in));
                $time_outs[] = date('Y-m-d H:i', strtotime($time_out));

                if($exclude_lunch) {
                    $break_times = [date('Y-m-d H:i', strtotime($break_start)), date('Y-m-d H:i', strtotime($break_end))];
                }
            }
            if(empty($time_ins)){
                return [];
            }
            $time_start = max($time_ins);
            $time_end = min($time_outs);
            while ($time_start < $time_end) {
                $continue = false;
                if($exclude_lunch) {
                    if($time_start >= $break_times[0] && $time_start < $break_times[1]) {
                        $continue = true;
                    }
                }
                $temp_time = date('Y-m-d H:i', strtotime("+{$interview_slot} minutes", strtotime($time_start)));
                $time_slot = date('h:i A', strtotime($time_start)) . " - " . date('h:i A', strtotime($temp_time));
                if(!$continue)
                    $time_slots[] = $time_slot;
                $time_start = $temp_time;
            }
            $used_slots = InterviewSchedule::where("company_id", $c)->whereDate("schedule_date", $date)->pluck("slot_timing");
            $used_slots = $used_slots->count() ? $used_slots->toArray() : [];
            $final_time_slots = [];
            if(empty($used_slots)){
                $final_time_slots = $time_slots;
            }else{
                foreach($time_slots as $slot){
                    if(!in_array($slot, $used_slots)){
                        $final_time_slots[] = $slot;
                    } 
                }
            }
            $remove = [];
            if($event_slots){
                foreach($event_slots as $eSlot){
                    foreach($time_slots as $k=>$v){
                        $v = explode(" - ", $v);
                        if( (strtotime($v[0]) >= strtotime($eSlot['start']) && strtotime($v[0]) < strtotime($eSlot['end'])) || (strtotime($v[1]) >= strtotime($eSlot['start']) && strtotime($v[1]) < strtotime($eSlot['end'])) ){
                            $remove[] = implode(" - ", $v);
                        }
                    }
                }
            }
        return array_values(array_diff($final_time_slots, $remove));
    }

    public function candidateScheduleInterview(Request $request)
    {

        abort_if(!$request->filled('c', 'app', 't'), 403);
        $this->application = JobApplication::with('job.company', 'schedule.employee', 'status')
                                ->where('email_token', $request->t)
                                ->find($request->app);
        if($this->application == null){
            return redirect('schedule-not-found/' . \App\Company::find($request->c)->career_page_link);
        }
        $this->timezone = $this->application->job->company->timezone ?? 'GMT';
        if(!$request->has('loadMainView') && !$request->has('locationIdForCompany')){
            return view('interview-schedule', $this->data);
        }
        $vu = User::where('company_id', $request->c)->first();
        $this->googleSchedules = (($vu->GC_channel_id ?? '') != '') ?  getGCEvents($vu) : $this->getExternalGEvents($vu);
        $user = User::where('company_id', $request->c)->first();
        if(is_null($this->application)) {
            $company = Company::find($request->c);
            return redirect()->route('jobs.jobOpenings', $company->career_page_link);
        }
        $thankyou = Option::getOption('thanks_page_url', $request->c);
        if($this->application->schedule && $this->application->schedule->status == 'canceled') {
            $company = Company::find($request->c);
        }
        
        $currentDate = now($this->application->job->company->timezone); // Current Date

        $this->employee = $this->application->schedule->user_accept_status == 'accept' ? $this->application->schedule->employee[0]->user : User::where('company_id', $request->c)->withRole('admin')->first();
        $schedule = $this->employee->profile_schedule;
        $available_weaks = Arr::get($schedule, 'global.available_weaks');
        $available_weaks = ($available_weaks==1) ? 7 : 14;
        $available_weaks = ($this->application->job->workflow_id) ? $this->application->workflow_stage->interview_action->available_days : $available_weaks;
        $days = [];
        foreach (range($this->application->workflow_stage->interview_action->same_day_schedule ? 0 : 1, $available_weaks) as $d) {
            $currentTime = $currentDate->copy()->addDays($d);
            $days[] = $currentDate->copy()->addDays($d)->format('Y-m-d');
            if(!Arr::get($user->profile_schedule, 'days.' . strtolower($currentTime->format('l')) . '.active')) {
                $occupied[] = $currentTime->format('Y-m-d');
            }
        }
        
        $occupied = [];
        $user = User::where("company_id", $request->c)->first();
        $duration = ($this->application->job->workflow_id) ? ( $this->application->workflow_stage->interview_action->timeslot == 0 ? 30 :  $this->application->workflow_stage->interview_action->timeslot ) : Arr::get($user->profile_schedule, 'global.interview_slot', '30');

        foreach (range(1, $available_weaks) as $d) {
            $currentTime = $currentDate->copy()->addDays($d);
            if($this->application->job->workflow_id == 0){
                if(!Arr::get($user->profile_schedule, 'days.' . strtolower($currentTime->format('l')) . '.active')) {
                    $occupied[] = $currentTime->format('Y-m-d');
                }
                $g_slots = $this->getTimeSlots($user, $currentTime->format('Y-m-d'), $duration, $user->company->timezone, $user->profile_schedule, $this->googleSchedules = (($user->GC_channel_id ?? '') != '') ?  getGCEvents($user) : $this->getExternalGEvents($user));
                if(empty($g_slots)){
                    $occupied[] = $currentTime->format('Y-m-d');
                }
            }else{
                if(!isset( $this->application->workflow_stage->workflow_actions('interview')[0])){
                    return redirect('schedule-not-found/' . $user->company->career_page_link);
                }
                if( $this->application->workflow_stage->workflow_actions('interview')[0]->interview_panel()->count() == 1){
                    $user = $this->application->workflow_stage->workflow_actions('interview')[0]->interview_panel()[0];
                    $g_slots = $this->getTimeSlots($user, $currentTime->format('Y-m-d'), $duration, $user->company->timezone, $user->profile_schedule, $this->googleSchedules = (($user->GC_channel_id ?? '') != '') ?  getGCEvents($user) : $this->getExternalGEvents($user));
                    if(empty($g_slots)){
                        $occupied[] = $currentTime->format('Y-m-d');
                    }
                }
            }
        }

        // If Holidays
        $holidays = blockHolidays($occupied,[$request->c]);

        $this->today = $currentDate->format('c');
        $this->timezone = $this->application->job->company->timezone ?: 'GMT';
        $this->days = $days;
        $this->occupied = array_merge($occupied, blockCompleteDay([],[$this->employee->id]) );
        $this->career_link = $thankyou ? $thankyou : route('jobs.jobOpenings', $this->application->job->company->career_page_link);
        $stage = $this->application->workflow_stage;
        $all_days = [];
        if($this->application->job->workflow_id){
            $available_days_name = [];
            $available_days = [];
            $occupied_days = [];
            $panal = $this->application->schedule->user_accept_status == 'accept' ? [$this->application->schedule->employee[0]->user] : $stage->workflow_actions('interview')[0]->interview_panel();
            foreach($panal as $interviewer){
                if($interviewer->profile_schedule['days'] ?? null){
                    foreach($interviewer->profile_schedule['days'] as $day=>$stats){
                        if(isset($stats['active'])){
                            $available_days_name[ucfirst($day)] = ucfirst($day);
                        }
                    }
                }
            }
            foreach($days as $date){
                if(in_array(Carbon::parse($date)->format("l"), $available_days_name)){
                    $available_days[] = $date;
                    if(in_array($date,$holidays)){
                        $all_days[] = ['date'=>$date, 'occupied'=>true,'holiday'=>'Holiday'];
                    }
                    else{
                        $all_days[] = ['date'=>$date, 'occupied'=>false];
                    }
                }else{
                    $occupied_days[] = $date;
                    $all_days[] = ['date'=>$date, 'occupied'=>true];
                }
            }
            
            $this->days = $available_days;
            $this->occupied = $occupied_days;

        }else{
            foreach(range(1, $available_weaks) as $d){
                $date = $currentDate->copy()->addDays($d)->format('Y-m-d');
                if(!Arr::get($user->profile_schedule, 'days.' . strtolower($currentDate->copy()->addDays($d)->format('l')) . '.active') || in_array($date, $this->occupied)){
                    $all_days[] = ['date'=>$date, 'occupied'=> true ];
                }else{
                    $all_days[] = ['date'=>$date, 'occupied'=> false];
                }
            }
        }
        $this->forCompany = false;
        $this->forCompany_reschedule = 0;
        $this->show_location = true;
        if($this->application->job->workflow_id != 0){
            $this->show_location = \App\WorkflowStageActionInterview::where('stage_id', '=', $this->application->workflow_stage_id)->first()->show_location;
            $interview = \App\InterviewSchedule::where('stage_id' , '=', $this->application->workflow_stage_id)->where('job_application_id' , '=', $this->application->id)->where('status', 'pending')->get();

            $this->forCompany_reschedule = $interview->count();
            if($_SERVER['SERVER_NAME'] == 'stage.engyj.com'){
                $this->forCompany = $this->application->company_id == 33 && $this->show_location;
                $searchArray = [
                    28 => ['Friday', 'Monday'],
                    29 => ['Tuesday', 'Thursday']
                ];
            }elseif($_SERVER['SERVER_NAME'] == 'recruit.engyj.com'){
                $this->forCompany = ( $this->application->company_id == 22 || $this->application->company_id == 44 ) && $this->show_location;
                $searchArray = [
                    54 => ['Friday', 'Monday'],
                    55 => ['Tuesday', 'Thursday'],
                    45 => ['Friday', 'Monday'],
                    46 => ['Tuesday', 'Thursday']
                ];
            }

            if($request->has('locationIdForCompany')){
                foreach($all_days as $key=>$days){
                    if(in_array(Carbon::parse($days['date'])->format('l'), $searchArray[($request->locationIdForCompany == 0 ? $interview[0]->location_id : $request->locationIdForCompany)])){
                        $all_days[$key]['occupied'] = true;
                    }
                }
                $this->all_days = $all_days;    
                return view('admin.interview-schedule.calander', $this->data)->render();
            }
        }
        $this->all_days = $all_days;
        return view('admin.interview-schedule.main-view', $this->data)->render();
    }

    private function getExternalGEvents($user){
        if( $user->GC_refresh_token != ''){
            $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
            $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
            $redirectUri = url('oauth2callback');
            $client = new Google_Client();
            $client->setClientId($clientID);
            $client->setClientSecret($clientSecret);
            $client->setRedirectUri($redirectUri);    
            $client->refreshToken($user->GC_refresh_token);
            $clint_json = $client->getAccessToken();
            if($clint_json){
                $service = new Google_Service_Calendar($client);
                $calendarId = 'primary';
                $optParams = array(
                'maxResults' => 250,
                'orderBy' => 'startTime',
                'singleEvents' => true,
                'timeZone' => 'GMT',
                'timeMin' => date('c'),
                );
                $results = $service->events->listEvents($calendarId, $optParams);
                $events = $results->getItems();
                $ar = [];
                if (!empty($events)) { 
                    foreach ($events as $event) {
                        $start = $event->start->dateTime;
                        if (empty($start)) {
                            $start = $event->start->date;
                        }
                        $ev = $event->getSummary();
                        $c = calendar_log::where('eid', '=', $event->id)->first();
                        if($event->start->date && $event->end->date){
                            $actual_end_day = Carbon::createFromFormat('Y-m-d', $event->end->date)->subDays('1')->format('Y-m-d');
                            $start = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ',  $event->start->date .'T00:00:00Z')));
                            $end = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ', $actual_end_day .'T23:59:00Z')));
    
                        }else{
                            $start = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ',  $event->start->dateTime)));
                            $end = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ', $event->end->dateTime)));
    
                        }
                        $hours = (abs(strtotime($start) - strtotime($end))/(60*60));
                        $start->setTimezone($user->company->timezone);
                        $end->setTimezone($user->company->timezone);

                        $comp_date = str_replace("Z", '',str_replace("T", ' ', $event->start->dateTime));
                        $i = InterviewSchedule::where("google_event_id", '=', $event->id)->first();
                        if($i){
                            $change = false;
                            $i_date = Carbon::createFromFormat('Y-m-d H:i:s', $i->schedule_date);
                            $i_date->setTimezone($user->company->timezone);
                            $day = explode("T", $i->toArray()["schedule_date"]);
                            $slot = explode(" - ", $i->slot_timing);
                            if(date("Y-m-d H:i:s", strtotime($i_date)) != date("Y-m-d H:i:s", strtotime($comp_date))){
                                $i->schedule_date = date("Y-m-d H:i:s", strtotime($comp_date));
                                $change = true;
                            }

                            if(date("h:i A", strtotime($day[0] . " " . $slot[0])) != date("h:i A", strtotime($start))){
                                $slot[0] =  date("h:i A", strtotime($start));
                                $change = true;
                            }

                            if(date("h:i A", strtotime($day[0] . " " . $slot[1])) != date("h:i A", strtotime($end))){
                                $slot[1] = date("h:i A", strtotime($end));
                                $change = true;
                            }
                            
                            $i->slot_timing = implode(" - ", $slot);
                            if($change){
                                $i->save();
                            }
                        
                        }
                        if($c === null){
                            if(round($hours) >= 24){
                                $start->setTimezone('GMT');
                                $end->setTimezone('GMT');
                            }
                            $ar[] = array('id'=> "google__" . $event->id, 'title'=> 'Google Event', 'start'=> $start->format("Y-m-d h:i A"), 'end'=> $end->format("Y-m-d h:i A"));                            
                        }
    
                    }
                }
                return $ar;
            }
            return [];
        }
        return [];
    }

    private function sendGEvent(Request $request, User $user, Company $company, JobApplication $jobApplication, InterviewSchedule $interviewSchedule){
        $calendar_id = "";
        $s_date = explode(" " , $interviewSchedule->schedule_date)[0];
        $g_slot = explode(' - ', $interviewSchedule->slot_timing);

        $scheduleTime = $g_slot[0];
        $scheduleTime = date('H:i', strtotime($scheduleTime));
        $dateTime =  $s_date . ' ' . ($scheduleTime ?? '00:00');

        $dateTime = Carbon::createFromFormat('Y-m-d H:i', $dateTime, $company->timezone ?? 'GMT');
        $dateTime->setTimezone('GMT');

        $scheduleTimeEnd = $g_slot[1];
        $scheduleTimeEnd = date('H:i', strtotime($scheduleTimeEnd));
        $dateTimeEnd =  $s_date . ' ' . ($scheduleTimeEnd ?? '00:00');

        $dateTimeEnd = Carbon::createFromFormat('Y-m-d H:i', $dateTimeEnd, $company->timezone ?? 'GMT');
        $dateTimeEnd->setTimezone('GMT');
        $g_start = str_replace(" ", "T",$dateTime); 
        $g_end = str_replace(" ", "T", $dateTimeEnd);

        if( $user->GC_refresh_token != ''){
            $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
            $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
            $redirectUri = url('oauth2callback');
            $client = new Google_Client();
            $client->setClientId($clientID);
            $client->setClientSecret($clientSecret);
            $client->setRedirectUri($redirectUri);    
            $client->refreshToken($user->GC_refresh_token);
            $clint_json = $client->getAccessToken();
            if($clint_json){
                $service = new Google_Service_Calendar($client);
                $event = new Google_Service_Calendar_Event(array(
                'summary' => 'Interview for ' . $jobApplication->job->title,
                'location' => "",
                'description' => $jobApplication->comment,
                'start' => array(
                'dateTime' => $g_start, 
                'timeZone' => 'GMT',
                ),
                'end' => array(
                'dateTime' => $g_end, 
                'timeZone' => 'GMT',
                ),
                'attendees' => array(
                array('email' => $jobApplication->email),
                ),
                'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 24 * 60),
                    array('method' => 'popup', 'minutes' => 10),
                ),
                ),
            ));
            $calendarId = 'primary';
            $event = $service->events->insert($calendarId, $event);
            $calander = new calendar_log();
            $calander->company_id = $user->company_id;
            $calander->application_id = $jobApplication->id;
            $calander->schedule_id = $interviewSchedule->id;
            $calander->user_id = $user->id;
            $calendar_id = $event->id;
            $calander->eid = $calendar_id;
            $calander->cid = explode("?eid=", $event->htmlLink)[1];
            $calander->created_at = $event->created;
            $calander->interview_start = $g_start;
            $calander->interview_end = $g_end;
            $calander->save();              
            }
        }
        return $calendar_id;
    }

    private function cancelGEvent($id, $cid, $uid = 0){
        $schadule = calendar_log::where('schedule_id', $id)->orderBy('id', 'desc')->first();
        if(!empty($schadule)){
            try{
                $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
                $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
                $redirectUri = url('oauth2callback');
                $client = new Google_Client();
                $client->setClientId($clientID);
                $client->setClientSecret($clientSecret);
                $client->setRedirectUri($redirectUri);    
                $client->refreshToken(\App\User::find($uid)->GC_refresh_token);
                $clint_json = $client->getAccessToken();
                if($clint_json){
                    $service = new Google_Service_Calendar($client);
                    $check = $service->events->get('primary', $schadule->eid);
                    if($check->status != 'cancelled'){
                        $calendarId = 'primary';
                        $event = $service->events->delete($calendarId, $schadule->eid);
                    }
                }
            }catch(\Exception $e) {}
        }
    }

    public function sendStatusChangeSms($status,$jobApplication,$applicationStatus)
    {
        if($jobApplication && $status){
            $interview_url = route('candidate.schedule-inteview')."?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
            $message = __('email.ScheduleStatusCandidate.hasBeen') . ' - ' . "**{$status}**";
            $employees = User::frontAllAdmins(company()->id);
            switch ($status) {
                // If Status Hired
                case 'hired':
                    $is_allowed = Option::getOption('hired_auto_sms_enabled', company()->id);
                    if($is_allowed){
                        $has_dynamic = Option::getOption('sms_hired_applicant_text', company()->id);
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'=> $jobApplication->full_name,
                                '[applicant_email]'=> $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $interview_url,
                                '[job_title]'     => $jobApplication->job->title
                            ]);
                        }
                        $from = Option::getOption('twilio_from_number', company()->id);
                        $from = $from ?? config('twilio-notification-channel.from');
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
                // If Status Rejected
                case 'rejected':
                    $is_allowed = Option::getOption('rejected_auto_sms_enabled', company()->id);
                    if($is_allowed){
                        $has_dynamic = Option::getOption('sms_rejected_applicant_text', company()->id);
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'=> $jobApplication->full_name,
                                '[applicant_email]'=> $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $interview_url,
                                '[job_title]'     => $jobApplication->job->title
                                ]);
                        }
                        $from = Option::getOption('twilio_from_number', company()->id);
                        $from = $from ?? config('twilio-notification-channel.from');
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
            }
        }
    }
    
    public function interviewCanceled($applicant_id)
    {
        $this->pageTitle = 'Interview Canceled';
        $jobApplication = JobApplication::find($applicant_id);
        if($jobApplication->schedule){
            $this->company = Company::withoutGlobalScope('company')->find($jobApplication->company->id);
            $this->companyName = $this->companyName ?? $this->company->company_name;
            $this->languageSettings = $this->languageSettings ?? LanguageSetting::where('status', 'enabled')->orderBy('language_name')->get();
            $this->jobs = Job::frontActiveJobs($jobApplication->company->id);
            $this->locations = JobLocation::withoutGlobalScope('company')->where('company_id', $jobApplication->company->id)->get();
            $this->categories = JobCategory::withoutGlobalScope('company')->where('company_id', $jobApplication->company->id)->get();
            $this->frontTheme = ThemeSetting::where('company_id', $jobApplication->company->id)->first();
            return view('front.interview-canceled', $this->data);
        }
        return redirect()->route('jobs.jobOpenings', $jobApplication->career_page_link);
    }
}
