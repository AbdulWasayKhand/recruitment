<?php

namespace App\Http\Controllers\Admin;

use App\ApplicationSetting;
use App\ApplicationStatus;
use App\Helper\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Option;
use App\Company;
use App\ApplicationLabel;

class ApplicationSettingsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.applicationFormSettings';
        $this->pageIcon = 'icon-settings';
        $this->setting = ApplicationSetting::first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(! $this->user->cans('manage_settings'), 403);
        $this->setting = ApplicationSetting::where('company_id', user()->company_id)->first();

        $this->timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        if(!$this->setting){
            abort(404);
        }
        $this->company = Company::find($this->user->company_id);
        $this->statuses = ApplicationStatus::all();
        $this->labels = ApplicationLabel::where('company_id', '=', $this->user->company_id)->get();
        $this->statuses = ApplicationStatus::where('status','!=','rejected')
                            ->where('status','!=','hired')
                            ->where('status','!=','no show')
                            ->get();
        return view('admin.application-setting.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $labels = json_decode($request->labels);
        if(count($labels)){
            foreach($labels as $label){
                $checkLabel = ApplicationLabel::where('label_value', '=', $label->value)
                                                ->where('label_color', '=', $label->color)
                                                ->where('company_id', '=', $this->user->company_id)
                                                ->count();
                if($checkLabel == 0){
                    $newlabel = new ApplicationLabel();
                    $newlabel->label_value = preg_replace('#<[^>]+>#', '',$label->value);
                    $newlabel->label_color = $label->color;
                    $newlabel->company_id = $this->user->company_id;
                    $newlabel->save();
                }
            }
        }
        abort_if(! $this->user->cans('manage_settings'), 403);
        $this->setting = ApplicationSetting::where('company_id', user()->company_id)->first();

        $mailSetting = [];
        foreach ($this->setting->mail_setting as $id => $setting) {
            $setting['status'] = false;
            $status_id = Arr::get($request->mail_setting, $setting['name'], $id);
            if(isset($request->mail_setting[$setting['name']])) {
                $setting['status'] = true;
            }
            $mailSetting = Arr::add($mailSetting, $status_id, $setting);
        }
        
        $smsSetting = [];
        foreach ($this->setting->mail_setting as $id => $setting) {
            $setting['status'] = false;
            $status_id = Arr::get($request->sms_setting, $setting['name'], $id);
            if(isset($request->sms_setting[$setting['name']])) {
                $setting['status'] = true;
            }
            $smsSetting = Arr::add($smsSetting, $status_id, $setting);
        }

        $this->setting->sms_setting = $smsSetting;
        $this->setting->mail_setting = $mailSetting;
        
        $this->setting->save();

        $defaultOptions = Option::defaultCompanyOptions();
        if($defaultOptions) {
            if(array_key_exists('twilio_from_number', $defaultOptions) && \App\Option::getOption('twilio_from_number',$this->user->company_id)){
                unset($defaultOptions['twilio_from_number']); 
            }
            foreach ($defaultOptions as $name => $default) {
                if($request->has('company_settings.'.$name)) {
                    Option::setOption($name, $request->company_settings[$name], $this->user->company_id);
                } else {
                    Option::setOption($name, $default, $this->user->company_id);
                }
            }
        }

        return Reply::success(__('messages.applicationSetting.settingUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
