<?php

namespace App\Http\Controllers\Admin;

use App\ApplicationSetting;
use App\ApplicationStatus;
use App\Option;
use App\SmsLog;
use App\Company;
use App\Exports\JobApplicationExport;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\InterviewSchedule\StoreRequest;
use App\Http\Requests\StoreJobApplication;
use App\Http\Requests\UpdateJobApplication;
use App\InterviewSchedule;
use App\Job;
use App\JobApplication;
use App\JobApplicationAnswer;
use App\JobLocation;
use App\Notifications\CandidateScheduleInterview;
use App\Notifications\CandidateStatusChange;
use App\Notifications\ScheduleInterview;
use App\Mail\NewUserNotification;
use App\Skill;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\Notifications\SmsTwilio;
use App\Helper\TwilioHelper as Twilio;
use App\Mail\MailQualification;
use Illuminate\Support\Facades\Mail;
use App\ApplicantHistory;
use App\ApplicationLabel;
use App\Workflow;
use App\WorkflowStage;
use App\Notifications\ScheduleInterviewStatus;
use App\Notifications\ScheduleStatusCandidate;

class AdminJobApplicationController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.jobApplications';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(!$this->user->cans('view_job_applications'), 403);
        $date = Carbon::now(company()->timezone);
        $startDate = $date->subDays(30)->format('Y-m-d');
        $endDate = Carbon::now(company()->timezone)->format('Y-m-d');
        $this->locations = JobLocation::all();
        $this->jobs = Job::where('status', 'active')->get();
        $this->default_workflow_view_job = Option::getOption('default_workflow_view_job',company()->id);

        $this->workflows = Workflow::all();
        $this->boardColumns = ApplicationStatus::with(['applications' => function ($q) use ($startDate, $endDate, $request) {

            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $q = $q->where(DB::raw('DATE(job_applications.`created_at`)'), '>=', $request->startDate);
            } else {
                $q = $q->where(DB::raw('DATE(job_applications.`created_at`)'), '>=', $startDate);
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $q = $q->where(DB::raw('DATE(job_applications.`created_at`)'), '<=', $request->endDate);
            } else {
                $q = $q->where(DB::raw('DATE(job_applications.`created_at`)'), '<=', $endDate);
            }

            // Filter By jobs
            if ($request->jobs != 'all' && $request->jobs != '') {
                $q = $q->where('job_applications.job_id', $request->jobs);
            }
            // Filter By jobs
            if ($request->search != null && $request->search != '' && $request->search != 'undefined') {
                $q = $q->where('full_name', 'LIKE', '%' . $request->search . '%');
            }

            if( session()->has('workflow_dropdown_job_id') && session()->get('workflow_dropdown_job_id') ){
                $q = $q->where('job_id',session()->get('workflow_dropdown_job_id'));
            }
        }, 'applications.schedule'])->orderBy('position')->get();

        $boardStracture = [];
        foreach ($this->boardColumns as $key => $column) {
            $boardStracture[$column->id] = [];
            foreach ($column->applications as $application) {
                $boardStracture[$column->id][] = $application->id;
            }
        }
        $this->boardStracture =  json_encode($boardStracture);
        $this->currentDate = Carbon::now(company()->timezone)->timestamp;
        
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        
        if ($request->ajax()) {
            $this->message_body = Arr::get(Option::getAll(company()->id), 'qualified_text');
            $this->sms_body = Arr::get(Option::getAll(company()->id), 'sms_qualified_text');    
            $view = view('admin.job-applications.board-data', $this->data)->render();
            return Reply::dataOnly(['view' => $view]);
        }

        $this->mailSetting = ApplicationSetting::select('id', 'mail_setting')->first();

        return view('admin.job-applications.board', $this->data);
    }

    public function create()
    {
        abort_if(!$this->user->cans('add_job_applications'), 403);

        $candidateCount = JobApplication::count();
            if($candidateCount > 0 && $this->activePackage->package->no_of_candidate_access !== null && $this->activePackage->package->no_of_candidate_access <= $candidateCount) {
            $this->message = __('modules.subscription.candidatePurchase');
            return view('admin.subscription.ask_purchase', $this->data);
        }
        $this->jobs = Job::activeJobs();
        $this->gender = [
            'male' => __('modules.front.male'),
            'female' => __('modules.front.female'),
            'others' => __('modules.front.others')
        ];
        return view('admin.job-applications.create', $this->data);
    }

    /**
     * @param $jobID
     * @return mixed
     * @throws \Throwable
     */
    public function jobQuestion($jobID, $applicationId = null)
    {
        $this->job = Job::findOrFail($jobID);
        $this->jobQuestion = $this->job->questions()->with([
            'answers' => function ($query) use ($jobID, $applicationId){
                $query->where(['job_application_id' => $applicationId, 'job_id' => $jobID]);
            }
        ])->get();
        $this->gender = [
            'male' => __('modules.front.male'),
            'female' => __('modules.front.female'),
            'others' => __('modules.front.others')
        ];
        $this->applicationId = $applicationId;
        $view = view('admin.job-applications.job-question', $this->data)->render();

        $options = ['job' => $this->job, 'gender' => $this->gender];
        $sections = ['section_visibility' => $this->job->section_visibility];

        if ($applicationId) {
            $application = JobApplication::select('id', 'gender', 'dob', 'country', 'state', 'city')->where('id', $applicationId)->first();

            $options = Arr::add($options, 'application', $application);
            $sections = Arr::add($sections, 'application', $application);
        }
        $requiredColumnsView = view('admin.job-applications.required-columns', $options)->render();
        $requiredSectionsView = view('admin.job-applications.required-sections', $sections)->render();
        
        $count = count($this->jobQuestion);
        
        $data = ['status' => 'success', 'view' => $view, 'requiredColumnsView' => $requiredColumnsView, 'requiredSectionsView' => $requiredSectionsView, 'count' => $count];
        
        if ($applicationId) {
            $data = Arr::add($data, 'application', $application);
        }

        return Reply::dataOnly($data);
    }


    public function edit($id)
    {
        abort_if(!$this->user->cans('edit_job_applications'), 403);

        $this->statuses = ApplicationStatus::all();
        $this->application = JobApplication::find($id);
        $this->jobQuestion = $this->application->job->questions;
        $this->jobs = Job::activeJobs();

        return view('admin.job-applications.edit', $this->data);
    }

    public function data(Request $request)
    {
        abort_if(!$this->user->cans('view_job_applications'), 403);

        $jobApplications = JobApplication::select('id', 'job_id', 'eligibility', 'status_id', 'workflow_stage_id', 'email', 'full_name', 'skills', 'source', 'created_at', 'company_id','referer_url','hiring_status','widget_host')
            ->with([
                'job:id,location_id,title,eligibility_percentage,workflow_id',
                'job.skills',
                'job.location:id,location',
                'status:id,status'
            ]);

        if($request->filled('job_id')) {
            $jobApplications = $jobApplications->where('job_id', $request->job_id);
        }
        
        // Filter by status
        if ($request->status != 'all' && $request->status != '') {
            $jobApplications = $jobApplications->where('status_id', $request->status);
        }

        // Filter By jobs
        if ($request->jobs != 'all' && $request->jobs != '') {
            $jobApplications = $jobApplications->where('job_id', $request->jobs);
        }

        // Filter By skills
        if ($request->skill != 'all' && $request->skill != '') {
            foreach (explode(',', $request->skill) as $key => $skill) {
                if ($key == 0) {
                    $jobApplications = $jobApplications->whereJsonContains('skills', $skill);
                }
                else {
                    $jobApplications = $jobApplications->orWhereJsonContains('skills', $skill);
                }
            }
        }
        
        // Filter by location
        if ($request->location != 'all' && $request->location != '') {
            $jobApplications = $jobApplications->whereHas('job.location', function ($query) use ($request)
            {
                $query->where('location', $request->location);
            });
        }

        // Filter by StartDate
        if ($request->startDate != null && $request->startDate != '') {
            $jobApplications = $jobApplications->whereDate('created_at', '>=', $request->startDate);
        }

        // Filter by EndDate
        if ($request->endDate != null && $request->endDate != '') {
            $jobApplications = $jobApplications->whereDate('created_at', '<=', $request->endDate);
        }
        $jobApplications = $jobApplications->get();
        if ($request->qualification != 'all' && $request->qualification != '') {
            $jobApplications = $jobApplications->filter(function($item) use($request) {
                return $request->qualification == 'qualified' ?
                            $item->eligibility >= $item->job->eligibility_percentage :
                            $item->eligibility < $item->job->eligibility_percentage;

            })->values()->all();
        }

        return DataTables::of($jobApplications)
            ->addColumn('selectColumn', function($row) {
                $selectRow = '<div class="checkbox form-check">
                                <input id="' . $row->id . '" type="checkbox" data-workflow="' . $row->job->workflow_id .'" data-job="' . ($this->user->job_wise ? $row->job->id : 0) .'" name="id[]" class="form-check-input multi-check" value="' . $row->id . '" >
                                <label for="' . $row->id . '"></label>
                            </div>';
                return $selectRow;
            })
            ->addColumn('action', function ($row) {
                $action = '';

                if ($this->user->cans('view_job_applications')) {
                $action .= '<a href="javascript:;" class="btn btn-circle-transparent text-success btn-circle show-document" data-toggle="tooltip" data-row-id="' . $row->id . '" data-modal-name="' . '\\' . get_class($row) . '" data-original-title="' . __('modules.jobApplication.viewDocuments') . '"><i class="fa fa-file" aria-hidden="true"></i></a>&nbsp;';
                }
                $action .= '<a href="javascript:;" class="btn text-info btn-circle-transparent btn-circle show-detail" data-widget="control-sidebar" data-slide="true" data-row-id="' . $row->id . '" data-original-title="' . __('modules.jobApplication.viewProfile') . '" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true"></i></a>';

                if ($this->user->cans('edit_job_applications')) {
                    $action .= ' <a href="' . route('admin.job-applications.edit', [$row->id]) . '" class="btn-circle-transparent btn text-primary btn-circle"
                      data-toggle="tooltip" data-original-title="' . __('app.edit') . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }

                if ($this->user->cans('delete_job_applications')) {
                    // $action .= ' <a href="javascript:;" class="btn text-danger btn-circle-transparent btn-circle sa-params"
                    //   data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="' . __('app.delete') . '"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    $action .= ' <a href="javascript:;" class="btn text-danger btn-circle-transparent btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="' . __('modules.jobApplication.archive') . '"><i class="fa fa-archive" aria-hidden="true"></i></a>';
                }
                return $action;
            })
            ->editColumn('full_name', function ($row) {
                $emailCount = JobApplication::where('email', $row->email)->count();
                $str =  '<a href="javascript:;" class="show-detail" data-widget="control-sidebar" data-slide="true" data-row-id="' . $row->id . '">' . ucwords($row->full_name) . '</a>';
                $auto_informed = JobApplication::where("id", $row->id)->first();
                $schedule = InterviewSchedule::where("job_application_id", $auto_informed->id);
                if($row->status->status == 'hired'){
                    $str .= "<span style='color:#000066' class=\"d-block\">Hired</span>";
                    // if($row->status){
                    //     if($row->status->status == 'applied' || $row->status->status == "phone screen"){
                        //     }
                        // }    
                }elseif($row->status->status == 'rejected'){
                    $str .= "<span class=\"d-block text-danger\">Rejected</span>";
                }elseif($auto_informed->is_manual_informed && $schedule->count() == 0){
                        // if($row->status->status == 'applied' || $row->status->status == "phone screen"){
                    $str .= "<span class=\"text-success d-block\">Invited</span>";
                            // }
                }elseif($auto_informed->is_auto_informed && $schedule->count() == 0){
                    $str .= "<span class=\"text-success d-block\">Invited</span>";
                }

                // If Workflow
                if($row->job->workflow_id != 0){
                    switch ($row->hiring_status) {
                        case 'hired':
                            $str .= "<span style='color:#000066' class=\"d-block\">Hired</span>";
                        break;
                        case 'rejected':
                            $str .= "<span class=\"text-danger d-block\">Rejected</span>";
                        break;
                    }
                }

                if($emailCount != 1){
                    $min = JobApplication::where('email', $row->email)->select(DB::raw('MIN(id) as min'))->groupByRaw('job_id')
                    ->pluck('min')->toArray(); 
                        if(!in_array($row->id, $min)){
                            $str .= "<span style='color:#dc3545' class=\"d-block\"><i class='fas fa-exclamation-triangle'></i> Applied Before</span>";
                        }
                }
                return $str;
            })
            ->editColumn('title', function ($row) {
                return ucfirst($row->job->title). "<br/><small><i>" . ($row->job->workflow->name == '' ? 'Default' : $row->job->workflow->name) . '</i></small>' ;
            })
            ->editColumn('applicant_name', function ($row) {
                return ucfirst($row->applicant_name);
            })
            ->editColumn('source', function ($row) {
                $url = $row->referer_url != '' || $row->referer_url != null ? $row->referer_url : '#';
                $title = $row->source;
                $title = strpos($url, '#') !== false ? "Direct" : (($row->source=='referral') ? 'Referral' : 'Other');
                $title = strpos($url, 'facebook') !== false ? "Facebook" : $title;
                $title = strpos($url, 'google') !== false ? "Google" : $title;
                $title = strpos($url, 'linkedin') !== false ? "Linkedin" : $title;
                $title = strpos($url, 'indeed') !== false ? "Indeed" : $title;
                $title = strpos($url, 'recruit') !== false ? "Direct" : $title;
                if(strpos($url, 'mycna')){
                    return '<span title="'.$url.'">MyCNA</span>';
                }
                if(strpos($url, 'careerplug')){
                    return '<span title="'.$url.'">Career Plug</span>';
                }
                if(strpos($url, 'jazzhr')){
                    return '<span title="'.$url.'">JazzHR</span>';
                }
                if(strpos($url, 'craigslist')){
                    return '<span title="'.$url.'">Craigslist</span>';
                }
                if(strpos($url, 'apply')){
                    return '<span title="'.$url.'">Apply</span>';
                }
                return '<span title="'.$url.'">'.ucwords($title).'</span>';
            })
            ->editColumn('date', function ($row) {
                return $row->created_at->format('M d, Y');
            })
            ->editColumn('location', function ($row) {
                $location = $row->schedule->location->location_name ?? "N/A";
                $location = ($location == "N/A") ? (($row->job->locations()->count() == 1) ? $row->job->locations()->first()->location_name : "N/A") : $location;
                return ucwords($location);
            })
            ->editColumn('job_title', function ($row) {
                return ucwords($row->job_title);
            })
            ->addColumn('qualification', function($row) {
                $qualifiable = \DB::table('job_questions')->leftJoin('custom_field_values', "job_questions.question_id", '=', 'custom_field_values.question_id')->where('job_id','=', $row->job->id)->whereNotNull('question_qualifiable')->count();
                if(is_numeric($row->eligibility)) {
                    $html = '';
                    if($row->eligibility >= $row->job->eligibility_percentage) {
                        $elg = number_format($row->eligibility, 1);
                        return 'QUALIFIED (' . ($elg <= 100 ? $elg : '100.0') . '%)';
                    }
                    if($qualifiable == 0){
                        return 'N/A';
                    }
                    return 'UNQUALIFIED (' . (number_format(100-$row->eligibility, 1) . '%)');
                }
                return 'N/A';
            })
            ->editColumn('referer_url', function ($row) {
                return $row->referer_url;
            })
            ->editColumn('widget_host', function ($row) {
                return $row->widget_host ?? 'N/A';
            })
            ->editColumn('workflow_stage', function ($row) {
                if($row->workflow_stage_id){
                    $i_can_accept = in_array($this->user->id, $row->schedule->employees->pluck('id')->toArray());
                    return $row->interview->schedule_date && $row->workflow_stage_id == $row->interview->stage_id ? ucwords($row->workflow_stage->title) . ( $row->interview->user_accept_status == 'done' ?  "<br><i class='fa fa-calendar'></i>&nbsp;<span style='font-size:12px;'>".Carbon::parse($row->interview->schedule_date)->format('M d, Y')."</span><br><small " . ($row->interview->status == 'pass' ? "style='color:green'" : ($row->interview->status == 'fail' ? "style='color:red'" : '' )) . "><i>".ucwords($row->interview->status).'</i></small>' : ("<br>" .  ($row->interview->user_accept_status ? "<i class='fa fa-calendar'></i>&nbsp;<span style='font-size:12px;'>".Carbon::parse($row->interview->schedule_date)->format('M d, Y')."</span>" : "<i><small ". ($i_can_accept ? "style='color:green;'" : "style='color:#ffc107;'") . ">Interview Requested</small></i>"))) : ucwords($row->workflow_stage->title) . "<br>" . ($row->workflow_stage->interview_action->id ? "<i><small style='color:#4caf50;'>Invited</small></i>" : '');
                }
                else if($row->status_id){
                    return ($row->status->status=='interview' && $row->interview->schedule_date) ? ucwords($row->status->status)."<br><i class='fa fa-calendar'></i>&nbsp;<span style='font-size:12px;'>".Carbon::parse($row->interview->schedule_date)->format('M d, Y').'</span>' : ucwords($row->status->status);
                }
                else{
                    return '';
                }
            })->rawColumns(['action', 'title' ,'full_name', 'selectColumn', 'source','status','workflow_stage'])
            ->addIndexColumn()
            ->make(true);
    }

    public function createSchedule(Request $request, $id)
    {
        abort_if(!$this->user->cans('add_schedule'), 403);
        $this->candidates = JobApplication::all();
        $this->users = User::with('company')->company()->get();
        $this->scheduleDate = $request->date;
        $this->currentApplicant = JobApplication::findOrFail($id);
        return view('admin.job-applications.interview-create', $this->data)->render();
    }

    public function storeSchedule(StoreRequest $request)
    {
        abort_if(!$this->user->cans('add_schedule'), 403);
        
        $company = Company::find($this->user->company_id);
        $scheduleTime = Str::before($request->slot_timing, ' - ');
        $scheduleTime = date('H:i', strtotime($scheduleTime));
        $dateTime =  $request->scheduleDate.' '. ($scheduleTime ?? '00:00');
        $dateTime = Carbon::createFromFormat('Y-m-d H:i', $dateTime, $company->timezone ?? 'GMT');
        $dateTime->setTimezone('GMT');
        
        $application = JobApplication::find($request->candidates[0]);
        // if($application->schedule && $application->schedule->status == 'canceled') {
        //     // $application->schedule()->delete();
        // }
        
        // store Schedule
        $interviewSchedule = new InterviewSchedule();
        $interviewSchedule->job_application_id = $request->candidates[0];
        $interviewSchedule->schedule_date = $dateTime;
        $interviewSchedule->slot_timing = $request->slot_timing;
        $interviewSchedule->save();

        // Update Schedule Status
        $status = ApplicationStatus::where('status', 'interview')->first();
        $jobApplication = $interviewSchedule->jobApplication;

        $jobApplication->status_id = $status->id;

        $jobApplication->save();

        if($request->comment){
            $scheduleComment = [
                'interview_schedule_id' => $interviewSchedule->id,
                'user_id' => $this->user->id,
                'comment' => $request->comment
            ];

            $interviewSchedule->comments()->create($scheduleComment);
        }

        if (!empty($request->employees)) {
            $employees = $request->employees;
            foreach ($employees as $emp) {
                Option::setOption('employee_inteview_slot_timing_'.($request->scheduleDate), $request->slot_timing, $this->user->company_id, $emp);
            }
            if(in_array($this->user->id, $request->employees)) {
                $interviewSchedule->employees()->attach($request->employees, ['company_id' => $this->user->company_id, 'user_accept_status' => 'accept']);

                $employees = Arr::except($request->employees, $this->user->id);
            }

            $interviewSchedule->employees()->attach($employees, ['company_id' => $this->user->company_id]);
            
            try {
                // Mail to employee for inform interview schedule
                Notification::send($interviewSchedule->employees, new ScheduleInterview($jobApplication, $interviewSchedule, false, ($request->comment ?? '')));
                $message = "You have been assigned to interview applicant {$jobApplication->full_name} for {$jobApplication->job->title} on ".
                            $interviewSchedule->schedule_date->timezone($company->timezone)->format('D, M d, Y')." at {$interviewSchedule->slot_timing}.";
                Notification::send($interviewSchedule->employees, new SmsTwilio($message, $company->twilio_number ?? NULL));
            } catch(\Exception $e) {}
        }

        try {
            $users = User::frontAllAdmins($this->user->company_id);
            $settings = ApplicationSetting::where('company_id', $jobApplication->company_id)->first();
            $company_settings = Option::getAll($jobApplication->company_id);

            if(array_key_exists($status->id, $settings->mail_setting ?? []) && 
                \Arr::get($settings->mail_setting, $status->id.'.status')) {
                // mail to candidate for inform interview schedule
                Notification::send($jobApplication, new CandidateScheduleInterview($jobApplication, $interviewSchedule));
            }

            if(array_key_exists($status->id, $settings->sms_setting ?? []) && \Arr::get($settings->sms_setting, $status->id.'.status')) {
                $message = '';
                $message .= "Dear {$jobApplication->full_name}, your interview has been scheduled with {$users[0]->name} at {$jobApplication->job->company->company_name} on " . $interviewSchedule->timezone($company->timezone)->schedule_date->format('M d, Y') . " from {$interviewSchedule->slot_timing}";

                $interview_url = route('candidate.schedule-inteview') .
                "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
                $go = false;
                $has_message = Option::getOption('sms_schedule_interview_text', $jobApplication->company_id);
                if($jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                    if($interview_action->sms_schedule != 0){
                        $has_message = \App\SmsTemplate::find($interview_action->sms_schedule)->content;
                        $go = true;
                    }else{
                        if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                            $go = true;
                        }
                    }
                }else{
                    if(Option::getOption('schedule_auto_sms_enabled', $jobApplication->company_id)){
                        $go = true;
                    }
                }
                if($go){                    
                    if($has_message){
                        $message = strtr($has_message, [
                            '[applicant_name]'  => $jobApplication->full_name,
                            '[applicant_email]' => $jobApplication->email,
                            '[company_name]'    => $jobApplication->job->company->company_name,
                            '[job_title]'       => $jobApplication->job->title,
                            '[interview_url]'   => $interview_url,
                            '[employee_name]'   => $users[0]->name,
                            '[schedule_date]'   => $interviewSchedule->schedule_date->format('M d, Y'),
                            '[schedule_time]'   => $interviewSchedule->slot_timing,
                        ]);
                        if($jobApplication->job->workflow_id != 0){
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                        }else{
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => '',
                            ]);
                        }
                    }
                    $from = Option::getOption('twilio_from_number', $jobApplication->company_id);
                    $from = $from ?? config('twilio-notification-channel.from');
    
                    // Twilio::set($jobApplication->phone, $message,$from)->send(); 

                    $log = new \App\SmsLog();
                    $log->company_id = $jobApplication->company_id;
                    $log->message_type = 'outbound';
                    $log->type = 'normal';
                    $log->number = str_replace(' ', '',  $jobApplication->phone);
                    $log->message = $message;
                    $log->status = 'pending';
                    $log->send_at = date('Y-m-d h:i:s');
                    $log->reference_name = 'applicant-alert';
                    $log->reference_id = $jobApplication->id;
                    $log->save(); 
                }
            }
            
            sendReminder($jobApplication, $users[0], $interviewSchedule, $dateTime);

        } catch(\Exception $e) {
            \Log::debug($e->getMessage());
        }

        return Reply::redirect(route('admin.interview-schedule.index'), __('menu.interviewSchedule') . ' ' . __('messages.createdSuccessfully'));
    }
    public function getQna(Request $request){
        $applicant = JobApplication::find($request->id);
        $this->answers = JobApplicationAnswer::with(['question'])
            ->where('job_id', $applicant->job_id ?? 0)
            ->where('job_application_id', $applicant->id ?? 0)
            ->whereNull('questionnaire_id')
            ->get();
        return view('admin.job-applications.qna', $this->data);
    }

    public function store(StoreJobApplication $request)
    {
        abort_if(!$this->user->cans('add_job_applications'), 403);
        $applicationStatus = ApplicationStatus::first();

        $jobApplication = new JobApplication();
        $jobApplication->full_name = $request->full_name;
        $jobApplication->job_id = $request->job_id;
        $jobApplication->status_id = $applicationStatus->id;
        $jobApplication->email = $request->email;
        $phone = str_replace([' ', '-'], '', $request->phone);
        $phone = Str::startsWith($phone, '+1') ? $phone : ('+1' . $phone);
        $jobApplication->phone = $phone;
        $jobApplication->cover_letter = $request->cover_letter;
        $jobApplication->column_priority = 0;

        if ($request->has('gender')) {
            $jobApplication->gender = $request->gender;
        }
        if ($request->has('dob')) {
            $jobApplication->dob = $request->dob;
        }
        if ($request->has('country')) {
            $countriesArray = json_decode(file_get_contents(public_path('country-state-city/countries.json')), true)['countries'];
            $statesArray = json_decode(file_get_contents(public_path('country-state-city/states.json')), true)['states'];

            $jobApplication->country = $this->getName($countriesArray, $request->country);
            $jobApplication->state = $this->getName($statesArray, $request->state);
            $jobApplication->city = $request->city;
        }

        if ($request->hasFile('photo')) {
            $jobApplication->photo = Files::upload($request->photo,'candidate-photos');
        }
        $jobApplication->save();

        $applicantHistory = new ApplicantHistory();
        $applicantHistory->current_value = $jobApplication->status->id;
        $applicantHistory->status_type = 'application';
        $applicantHistory->company_id = $jobApplication->company_id;
        $applicantHistory->applicant_id = $jobApplication->id;
        $applicantHistory->save();

        if ($request->hasFile('resume')) {
            $hashname = Files::upload($request->resume, 'documents/'.$jobApplication->id, null, null, false);
            $jobApplication->documents()->create([
                'name' => 'Resume',
                'hashname' => $hashname
            ]);
        }

        // Job Application Answer save
        if (isset($request->answer) && !empty($request->answer)) {
                foreach ($request->answer as $key => $value) {
                    $answer = new JobApplicationAnswer();
                    $answer->job_application_id = $jobApplication->id;
                    $answer->job_id = $request->job_id;
                    $answer->question_id = $key;
                    $answer->answer = implode(', ', (array)$value);
                    $answer->save();
                }
            }

        return Reply::redirect(route('admin.job-applications.index'), __('menu.jobApplications') . ' ' . __('messages.createdSuccessfully'));
    }

    public function update(UpdateJobApplication $request, $id)
    {
        abort_if(!$this->user->cans('edit_job_applications'), 403);

        $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;

        $jobApplication = JobApplication::with(['documents'])->findOrFail($id);
        $jobApplication->full_name = $request->full_name;
        $jobApplication->job_id = $request->job_id;
        $jobApplication->status_id = $request->status_id;
        $jobApplication->email = $request->email;
        $jobApplication->phone = $request->phone;
        $jobApplication->cover_letter = $request->cover_letter;

        if ($request->has('gender')) {
            $jobApplication->gender = $request->gender;
        }
        if ($request->has('dob')) {
            $jobApplication->dob = $request->dob;
        }
        if ($request->has('country')) {
            $countriesArray = json_decode(file_get_contents(public_path('country-state-city/countries.json')), true)['countries'];
            $statesArray = json_decode(file_get_contents(public_path('country-state-city/states.json')), true)['states'];

            $jobApplication->country = $this->getName($countriesArray, $request->country);
            $jobApplication->state = $this->getName($statesArray, $request->state);
            $jobApplication->city = $request->city;
        }

        if ($request->hasFile('photo')) {
            $jobApplication->photo = Files::upload($request->photo,'candidate-photos');
        }

        $isStatusDirty = $jobApplication->isDirty('status_id');
        $jobApplication->onStatusChange();
        $jobApplication->save();

        if ($request->hasFile('resume')) {
            $hashname = Files::upload($request->resume, 'documents/'.$jobApplication->id, null, null, false);
            $jobApplication->documents()->updateOrCreate([
                'documentable_type' => JobApplication::class,
                'documentable_id' => $jobApplication->id,
                'name' => 'Resume'
            ],
            [
                'hashname' => $hashname
            ]);
        }
        // Job Application Answer save
        if (isset($request->answer) && count($request->answer) > 0) {
            foreach ($request->answer as $key => $value) {
                JobApplicationAnswer::updateOrCreate([
                    'job_application_id' => $jobApplication->id,
                    'job_id' => $jobApplication->job_id,
                    'question_id' => $key
                ], ['answer' => implode(', ', (array)$value)]);
            }
        }

        \Log::info('MailSetting::' . json_encode($mailSetting));
        if (isset($mailSetting[$request->status_id]) && $mailSetting[$request->status_id]['status'] && $isStatusDirty) {
                Notification::send($jobApplication, new CandidateStatusChange($jobApplication));
        }

        return Reply::redirect(route('admin.job-applications.table'), __('menu.jobApplications') . ' ' . __('messages.updatedSuccessfully'));
    }

    public function destroy($id)
    {
        abort_if(!$this->user->cans('delete_job_applications'), 403);
        $history = \App\ApplicantHistory::where('applicant_id', '=', $id);
        if($history->count()){
            $history->delete();
        }
        $application = JobApplication::findOrFail($id);
        $application->delete();


        return Reply::success(__('messages.applicationArchivedSuccessfully'));
        abort_if(!$this->user->cans('delete_job_applications'), 403);

        $jobApplication = JobApplication::findOrFail($id);
        SmsLog::where('reference_id', $id)->delete();
        $jobApplication->facebook_data()->delete();
        if ($jobApplication->photo) {
            Storage::delete('candidate-photos/'.$jobApplication->photo);
        }

        $jobApplication->forceDelete();

        return Reply::success(__('messages.recordDeleted'));
    }

    public function show(Request $request, $id)
    {
        $this->applications = JobApplication::all();
        $this->application = JobApplication::with(['schedule', 'status', 'schedule.employee', 'schedule.comments.user', 'facebook_data'])->find($id);
        $this->timezone = Company::where('id', $this->user->company_id)->select('id', 'timezone')->first()->timezone;
        $this->labels = ApplicationLabel::where("company_id", '=', $this->application->company_id)->get();
        $this->skills = Skill::select('id', 'name')->get();
        $this->interviews = InterviewSchedule::where('job_application_id', '=', $this->application->id )->get();
        $this->answers = JobApplicationAnswer::with(['question'])
            ->where('job_id', $this->application->job_id ?? 0)
            ->where('job_application_id', $this->application->id ?? 0)
            ->whereNull('questionnaire_id')
            ->get();
        $this->statuses = ApplicationStatus::where('company_id', $request->user()->company_id)->get();
        $this->stages = WorkflowStage::where('workflow_id',$this->application->job->workflow->id)->where(function($q){
            $q->where('position','>',-1)->orWhereNull('position');
        })->orderBy('position','asc')->get();
        $this->stages = $this->stages->merge( WorkflowStage::where('workflow_id',$this->application->job->workflow->id)
                        ->where('position','<',0)->get() );
        $this->has_interview_invited = $this->application->is_manual_informed;
        $this->questionnaire = $this->application->job->questionnaire;
        
        $this->before = null;
        $this->after = null;
        foreach($this->applications as $key => $value){
            if($this->application->id == $value->id){
                if($key != 0){
                    $this->before = $this->applications[$key - 1];
                }
                if($key != (sizeof($this->applications) - 1)){
                    $this->after = $this->applications[$key + 1];
                }
            }
        }
        $this->attachments = [
            'Resume' => $this->application->resume_url,
            'Photo' => ($this->application->photo_url  != 'http://localhost/recruitment/public/avatar.png' ? $this->application->photo_url : null)
        ];
        $this->all_stages = \App\WorkflowStage::where('workflow_id',$this->application->job->workflow_id)->orderBy('position')->get();
        $ids = [];
        foreach($this->all_stages as $stage){
            $ids[] = $stage;
            if($stage->id == $this->application->workflow_stage_id)
                break;
        }
        
        // $all_questionnaires_ids = array();
        // foreach(\App\WorkflowStageActionInterviewQuestionnaire::where('action_id', '=',(\App\WorkflowStageActionInterview::where('stage_id', '=', $stage->id)->first()->action_id))->get() as $act){
        //     $ques = \App\Questionnaire::find($act->questionnaire_id);
        //     $all_questionnaires_ids[] = ["id" => $act->questionnaire_id, "name" => $ques->name, "created_at" => Carbon::parse($ques->created_at)->timezone($this->application->company->timezone)->format('M d, Y, h:i A') ];
        // }
        // return $ids;
        $all_questionnaire = array();
        
        // foreach($ids as $stage){
        //     foreach(\App\WorkflowStageActionQuestionnaire::where('stage_id', '=', $stage->id ?? 0)->get() as $act){                
        //         $ques = \App\Questionnaire::find($act->questionnaire_id);
        //         $all_questionnaire[$act->questionnaire_id] = [
        //             "id" => $act->questionnaire_id, 
        //             "name" => $ques->name ?? "", 
        //             "is_external" => ($act->questionnaire_type == 'external'),
        //             "is_filled" => ($act->questionnaire_type == 'internal') && count($this->application->getStageQuestionnaires('internal')),
        //             "created_at" => Carbon::parse($ques->created_at ?? "")->timezone($this->application->company->timezone)->format('M d, Y, h:i A') ];
        //     }
        // }
        
        foreach(\App\WorkflowStageActionQuestionnaire::where('workflow_id', $this->application->job->workflow_id)->get() as $act){
            $ques = \App\Questionnaire::find($act->questionnaire_id);
            if(\App\JobApplicationAnswer::where('job_application_id', $this->application->id)->where('questionnaire_id', $act->questionnaire_id)->get()->count() || $act->questionnaire_type == 'internal'){
                $all_questionnaire[$act->questionnaire_id] = [
                    "id" => $act->questionnaire_id, 
                    "name" => $ques->name ?? "", 
                    "is_external" => ($act->questionnaire_type == 'external'),
                    "is_filled" => ($act->questionnaire_type == 'internal') && count($this->application->getStageQuestionnaires('internal')),
                    "created_at" => Carbon::parse($ques->created_at ?? "")->timezone($this->application->company->timezone)->format('M d, Y, h:i A') 
                ];
            }
        }
        $this->previous_questionnaire = $all_questionnaire;
        $this->message_body = Arr::get(\App\Option::getAll(company()->id), 'qualified_text');
        $this->sms_body = Arr::get(\App\Option::getAll(company()->id), 'sms_qualified_text');
        $view = view('admin.job-applications.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function updateIndex(Request $request)
    {
        $jobApplication = JobApplication::findOrFail($request->draggedTaskId);
        if($jobApplication->job->workflow_id){
            // If Interview Scheduled already
            if($jobApplication->workflow_stage->workflow_actions('interview')->count()){
                $interview = \App\InterviewSchedule::where('stage_id' , '=', $jobApplication->workflow_stage_id)->where('job_application_id' , '=', $jobApplication->id)->get();
                if(!($interview->count())){
                    return Reply::error('Interview not Scheduled by candidate.');
                }else{
                    if($jobApplication->schedule->status == 'pending'){
                        return Reply::error('Please complete Interview to move stage.');
                    }
                }
            }
            // if($jobApplication->schedule->status == 'pending'){
            //     return Reply::error('Please complete Interview to move stage.');
            // }
            // else{
            //     // If Interview not Scheduled and moving to other stage
            //     if($jobApplication->workflow_stage->workflow_actions('interview')->count() && $jobApplication->interviews()->where('status','!=','pending')->count()===0){
            //         return Reply::error('Interview not Scheduled by candidate.');
            //     }
            // }
        }
        $applicantStatuses = ApplicationStatus::get()->pluck('id','status');
        $taskIds = $request->applicationIds;
        $boardColumnIds = $request->boardColumnIds;
        $priorities = $request->prioritys;
        $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;

        $date = Carbon::now(company()->timezone);
        $startDate = $request->startDate ?: $date->subDays(30)->format('Y-m-d');
        $endDate = $request->endDate ?: $date->format('Y-m-d');
        
        if ($request->has('applicationIds')) {
            foreach ($taskIds as $key => $taskId) {
                if (!is_null($taskId)) {

                    $task = JobApplication::find($taskId);
                    $task->column_priority = $priorities[$key];
                    if($task->job->workflow_id==0){
                        $task->status_id = $boardColumnIds[$key];
                        $task->onStatusChange();
                    }
                    else{
                        $task->workflow_stage_id = $boardColumnIds[$key];
                    }
                    $task->save();
                    if($task->job->workflow_id!=0 && $task->id == $request->draggedTaskId){
                        $this->performStageAction($task);
                    }

                }
            }
            
            // Send notification to candidate on update status
            if($jobApplication->job->workflow_id==0){
                if( (isset($mailSetting[$boardColumnIds[0]]) && $mailSetting[$boardColumnIds[0]]['status'] || ( ($applicantStatuses['rejected']==$boardColumnIds[0] || $applicantStatuses['hired']==$boardColumnIds[0] || $applicantStatuses['interview']==$boardColumnIds[0])) ) && $request->draggedTaskId != 0){
                    $status = null;
                    // If Status Rejected
                    if($applicantStatuses['rejected']==$boardColumnIds[0]){
                        $status = 'rejected';
                    }
                    // If Status Hired
                    if($applicantStatuses['hired']==$boardColumnIds[0]){
                        $status = 'hired';
                    }
                    // If Status Interview
                    if($applicantStatuses['interview']==$boardColumnIds[0]){
                        $status = 'interview';
                    }
                    if($request->draggedTaskId != 0 && $request->draggingTaskId == 0){
                        try{
                        Notification::send($jobApplication, new CandidateStatusChange($jobApplication,$status));
                        } catch(\Exception $e) {}    
                    }
                }
                
                // If Status No Show
                if($applicantStatuses['no show']==$boardColumnIds[0]){
                    $status = 'no show';
                }
                // Send sms notification to applicant on update status
                if($request->draggedTaskId != 0 && $request->draggingTaskId == 0){
                    $this->sendStatusChangeSms($boardColumnIds[0],$jobApplication,$applicantStatuses);
                }
            }
        }

        $columnCountByIds = ApplicationStatus::select('id', 'color')
            ->withCount([
                'applications as status_count' => function ($query) use($startDate, $endDate, $request) {
                    $query->withoutTrashed()->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
                    if ($request->jobs != 'all' && $request->jobs != '') {
                        $query->where('job_id', $request->jobs);
                    }
                    if ($request->search != '') {
                        $query->where('full_name', 'LIKE', '%' . $request->search . '%');
                    }
                }
            ])
            ->get()
            ->toArray();
        // if($jobApplication->job->workflow_id!=0){
        //     $this->performStageAction($jobApplication);
        // }
        return Reply::dataOnly(['status' => 'success','columnCountByIds' => $columnCountByIds]);
    }

    public function table($jobId = null)
    {
        abort_if(!$this->user->cans('view_job_applications'), 403);

        $this->job_id = (request()->has('job_id') && request()->get('job_id')) ? request()->get('job_id') : $jobId;
        $this->boardColumns = ApplicationStatus::all();
        $this->locations = JobLocation::all();
        $this->jobs = Job::where('status', 'active')->get();
        $this->skills = Skill::all();
        $this->message_body = Arr::get(Option::getAll(company()->id), 'qualified_text');
        $this->sms_body = Arr::get(Option::getAll(company()->id), 'sms_qualified_text');
        $this->workflows = Workflow::all();
        return view('admin.job-applications.index', $this->data);
    }

    public function ratingSave(Request $request, $id)
    {
        abort_if(!$this->user->cans('edit_job_applications'), 403);

        $application = JobApplication::withTrashed()->findOrFail($id);
        $application->rating = $request->rating;
        $application->save();
        // Save Timeline
        saveTimeline([
            'applicant_id' =>   $application->id,
            'entity'       =>   'rating',
            'entity_id'    =>   $application->id,
            'user_id'      =>   $this->user->id,
            'comments'     =>   $request->rating == 0 ? "{$this->user->name} removed stars from {$application->full_name}" : "{$this->user->name} gave {$request->rating} " . ($request->rating == 1 ? "star" :  "stars") . " to {$application->full_name}"
        ]);
        return Reply::success(__('messages.updatedSuccessfully'));
    }

    // Job Applications data Export
    public function export($status, $startDate, $endDate, $jobs,$qualification)
    {
        $filters = [
            'status' => $status,
            'jobs' => $jobs,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'qualification' => $qualification
        ];

        $data = [
            'company' => $this->companyName
        ];

        return Excel::download(new JobApplicationExport($filters, $data), 'job-applications.xlsx', ExcelExcel::XLSX);
    }


    public function viewDetails(Request $request) {
        $candidateCount = JobApplication::count();
        if($candidateCount > 0 && !is_null($this->activePackage->package->no_of_candidate_access) && $this->activePackage->package->no_of_candidate_access <= $candidateCount) {
            $this->message = __('modules.subscription.candidatePurchase');
            return Reply::error(__('modules.subscription.candidatePurchase'));
        }

        $application = JobApplication::withTrashed()->findOrFail($request->id);
        $application->is_viewed = 1;
        $application->save();

        return Reply::dataOnly([
            'status' => 'success',
            'email' => $application->email,
            'phone' => $application->phone,
            'gender' => ucfirst($application->gender),
            'dob' => $application->dob ? $application->dob->format('M d, Y') : '',
            'country' => $application->country,
            'state' => $application->state,
            'city' => $application->city,
            'resume' => $application->resume_url ?: ''
        ]);
    }

    public function getName($arr, $id)
    {
        $result = array_filter($arr, function ($value) use ($id) {
            return $value['id'] == $id;
        });
        return current($result)['name'];
    }

    public function archiveJobApplication(Request $request, JobApplication $application)
    {
        abort_if(!$this->user->cans('delete_job_applications'), 403);
        $history = \App\ApplicantHistory::where('applicant_id', '=', $application->id);
        if($history->count()){
            $history->delete();
        }
        $application->delete();


        return Reply::success(__('messages.applicationArchivedSuccessfully'));
    }

    public function unarchiveJobApplication(Request $request, $application_id)
    {
        abort_if(!$this->user->cans('delete_job_applications'), 403);

        $application = JobApplication::select('id', 'deleted_at')->withTrashed()->where('id', $application_id)->first();
        
        $application->restore();

        return Reply::success(__('messages.applicationUnarchivedSuccessfully'));
    }

    public function sendInterviewInvitation(Request $request)
    {
        abort_if(!$this->user->cans('edit_job_applications'), 403);
        $jobApplication = JobApplication::with('job', 'schedule')->find($request->applicationId);
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
        // ->line($firstLine)
        $users = User::frontAllAdmins($jobApplication->job->company_id);
        $this->company_settings = Option::getAll($jobApplication->job->company_id);
        $url = route('candidate.schedule-inteview') . 
                    "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
        
        if(!is_null($jobApplication->schedule)) {
            if($jobApplication->schedule->status == 'canceled' || $jobApplication->schedule->status == 'rejected'){
                $jobApplication->schedule()->delete();
            }
        }
        
        // Send Email
        $email_text = rawurldecode(rawurldecode($request->emailMessage));
        if(!empty($email_text)) {
            $email_text = replace_template($email_text, [
                '[applicant_name]'  => $jobApplication->full_name,
                '[applicant_email]'  => $jobApplication->email,
                '[job_title]'       => $jobApplication->job->title,
                '[company_name]'    => $jobApplication->job->company->company_name,
                '[employee_name]'   => $users[0]->name,
                '[interview_url]'   => $url,
                '[schedule_date]'   => '',
                '[schedule_time]'   => ''
            ]);
            try {
                Mail::send(new MailQualification($jobApplication, 'qualified', $email_text));
            }catch(\Exception $e) {}
        }

        // Send SMS
        $sms_text = rawurldecode(rawurldecode($request->smsMassage)); 
        $debug[] = $sms_text;
        if(!empty($sms_text)) {
            $message = replace_template($sms_text, [
                '[applicant_name]'  => $jobApplication->full_name,
                '[applicant_email]'  => $jobApplication->email,
                '[job_title]'       => $jobApplication->job->title,
                '[company_name]'    => $jobApplication->job->company->company_name,
                '[employee_name]'   => $users[0]->name,
                '[interview_url]'   => $url,
                '[schedule_date]'   => '',
                '[schedule_time]'   => ''
            ]);
        } else {
            $message = "";
            $message .= "Dear {$jobApplication->full_name},\n";
            $message .= "This is {$users[0]->name} from {$this->global->company_name}\n";
            $message .= "Thankyou for submitting your application. We are delighted and look forward to meetimg you. Please schedule your interview by clicking the link below:\n";
            
            $message .= "\n" . $url;
        }

        $debug[] = $message;
        $send_at = Carbon::now();

        $jobApplication->is_manual_informed =1 ;
        $jobApplication->save();
        SmsLog::reminder($jobApplication->phone, $message, $send_at, $jobApplication->job->company_id);

        return Reply::success(__('messages.applicationInterviewInvitedSuccessfully'));
    }

    public function addSkills(Request $request, $applicationId)
    {
        abort_if(!$this->user->cans('edit_job_applications'), 403);

        $application = JobApplication::withTrashed()->findOrFail($applicationId);

        $application->skills = $request->skills;
        $application->save();

        return Reply::success(__('messages.skillsSavedSuccessfully'));
    }

    public function updateStatus(Request $request)
    {
        $applicantStatuses = ApplicationStatus::get()->pluck('id','status');
        $applicant_id = $request->applicant_id;
        $status_id    = $request->status_id;

        $application = JobApplication::findOrFail($applicant_id);

        $applicantHistory = new ApplicantHistory();
        $applicantHistory->current_value = $status_id;
        $applicantHistory->previous_value = $application->status->id;
        $applicantHistory->status_type = 'application';
        $applicantHistory->company_id = $application->company_id;
        $applicantHistory->applicant_id = $application->id;
        $applicantHistory->save();

        $application->status_id = $status_id;
        $application->onStatusChange();
        $application->save();        
        $isStatusDirty = $application->isDirty('status_id');
        $isStatusDirty = true;
        $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;
        
        /* If Status is rejected */
        $is_allowed = Option::getOption('rejected_auto_email_enabled', $application->company_id);
        if($applicantStatuses['rejected']==$status_id && $is_allowed!='enable'){
            return Reply::success("Applicant's status updated!");
        }
        /* end If Status is rejected */
        /* If Status is hired */
        $is_allowed = Option::getOption('hired_auto_email_enabled', $application->company_id);
        if($applicantStatuses['hired']==$status_id && $is_allowed!='enable'){
            return Reply::success("Applicant's status updated!");
        }
        /* end If Status is rejected */
        /* If Status is interview */
        if($applicantStatuses['interview']==$status_id && (!isset($mailSetting[$applicantStatuses['interview']]) || $mailSetting[$applicantStatuses['interview']]['status']===false) ){
            return Reply::success("Applicant's status updated!");
        }
        /* end If Status is interview */
        
        \Log::info('SEND_EMAIL >> ' . (isset($mailSetting[$status_id]) ? json_encode($mailSetting[$status_id]) : 'NOT_FOUND') . ' <> ' . $isStatusDirty);
        if ( (isset($mailSetting[$status_id]) && $mailSetting[$status_id]['status']) || ($applicantStatuses['rejected']==$status_id || $applicantStatuses['hired']==$status_id || $applicantStatuses['interview']==$status_id || ($applicantStatuses['no show'] ?? '#')==$status_id) && $isStatusDirty ) {
            $status = null;
            if($applicantStatuses['rejected']==$status_id){
                $status = 'rejected';
            }
            if($applicantStatuses['hired']==$status_id){
                $status = 'hired';
            }
            if($applicantStatuses['interview']==$status_id){
                $status = 'interview';
            }
            if(($applicantStatuses['no show'] ?? '#')==$status_id){
                $status = 'no show';
            }
            if($_SERVER['HTTP_HOST'] != 'localhost'){
                Notification::send($application, new CandidateStatusChange($application, $status));
            }
        }
        // Send sms notification to applicant on update status
        $this->sendStatusChangeSms($status_id,$application,$applicantStatuses);
        return Reply::success("Applicant's status updated!");
    }
    public function multiUpdateStage(Request $request)
    {
        $flag = false;
        $applicantStatuses = ApplicationStatus::get()->pluck('id','status');
        foreach($request->applicant_ids as $applicant_id){
            $workflow_stage_id    = $request->workflow_stage_id;
            
            $application = JobApplication::findOrFail($applicant_id);

            if($application->job->workflow_id == 0){
                
                $status_id    = $request->workflow_stage_id;
                
                $applicantHistory = new ApplicantHistory();
                $applicantHistory->current_value = $status_id;
                $applicantHistory->previous_value = $application->status->id;
                $applicantHistory->status_type = 'application';
                $applicantHistory->company_id = $application->company_id;
                $applicantHistory->applicant_id = $application->id;
                $applicantHistory->save();
        
                $application->status_id = $status_id;
                $application->onStatusChange();
                $application->save();        
                $isStatusDirty = $application->isDirty('status_id');
                $isStatusDirty = true;
                $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;

                $is_allowed = Option::getOption('rejected_auto_email_enabled', $application->company_id);
                if($applicantStatuses['rejected']==$status_id && $is_allowed!='enable'){
                    // return Reply::success("Applicant's status updated!");
                }

                $is_allowed = Option::getOption('hired_auto_email_enabled', $application->company_id);
                if($applicantStatuses['hired']==$status_id && $is_allowed!='enable'){
                    // return Reply::success("Applicant's status updated!");
                }
                
                if($applicantStatuses['interview']==$status_id && (!isset($mailSetting[$applicantStatuses['interview']]) || $mailSetting[$applicantStatuses['interview']]['status']===false) ){
                    // return Reply::success("Applicant's status updated!");
                }
                                
                \Log::info('SEND_EMAIL >> ' . (isset($mailSetting[$status_id]) ? json_encode($mailSetting[$status_id]) : 'NOT_FOUND') . ' <> ' . $isStatusDirty);
                if ( (isset($mailSetting[$status_id]) && $mailSetting[$status_id]['status']) || ($applicantStatuses['rejected']==$status_id || $applicantStatuses['hired']==$status_id || $applicantStatuses['interview']==$status_id || ($applicantStatuses['no show'] ?? '#')==$status_id) && $isStatusDirty ) {
                    $status = null;
                    if($applicantStatuses['rejected']==$status_id){
                        $status = 'rejected';
                    }
                    if($applicantStatuses['hired']==$status_id){
                        $status = 'hired';
                    }
                    if($applicantStatuses['interview']==$status_id){
                        $status = 'interview';
                    }
                    if(($applicantStatuses['no show'] ?? '#')==$status_id){
                        $status = 'no show';
                    }
                    if($_SERVER['HTTP_HOST'] != 'localhost'){
                        Notification::send($application, new CandidateStatusChange($application, $status));
                    }
                }
                
                $this->sendStatusChangeSms($status_id,$application,$applicantStatuses);
                // return Reply::success("AAAApplicant's status updated!");
            }else{
                $go = ((isset($application->schedule->status)) ? ( ($application->schedule->status == 'pending') ? false : true )  : true);
    
                if($go){
                    $applicantHistory = new ApplicantHistory();
                    $applicantHistory->current_value = $workflow_stage_id;
                    $applicantHistory->previous_value = $application->workflow_stage_id;
                    $applicantHistory->status_type = 'application';
                    $applicantHistory->company_id = $application->company_id;
                    $applicantHistory->applicant_id = $application->id;
                    $applicantHistory->save();
        
                    $application->workflow_stage_id = $workflow_stage_id;
                    $application->save();
                    // Save Timeline
                    saveTimeline([
                        'applicant_id' =>   $application->id,
                        'entity'       =>   'stage',
                        'entity_id'    =>   $workflow_stage_id,
                        'user_id'      =>   $this->user->id ?? 0,
                        'comments'     =>   "Stage Moved to {$application->workflow_stage->title} by {$this->user->name}"
                    ]);
                    $this->performStageAction($application);
                    // $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;
                }else{
                    $flag = true;
                }
            }
        }
        if($flag){
            return Reply::error('Few of the applicant can not change stage due to scheduled interview');
        }else{
            return Reply::success("All applicant's " . ($application->job->workflow_id == 0 ? 'status' : 'workflow stage') . " updated!");
        }
    }
    public function updateStage(Request $request)
    {
        $applicant_id = $request->applicant_id;
        $workflow_stage_id    = $request->workflow_stage_id;

        $application = JobApplication::findOrFail($applicant_id);
        // If Interview Scheduled already
        if($application->workflow_stage->workflow_actions('interview')->count()){
            $interview = \App\InterviewSchedule::where('stage_id' , '=', $application->workflow_stage_id)->where('job_application_id' , '=', $application->id)->get();
            if(!($interview->count())){
                return Reply::error('Interview not Scheduled by candidate.');
            }else{
                if($application->schedule->status == 'pending'){
                    return Reply::error('Please complete Interview to move stage.');
                }
            }
        }
        
        $go = ((isset($application->schedule->status)) ? ( ($application->schedule->status == 'pending') ? false : true )  : true);

        if($go){
            $applicantHistory = new ApplicantHistory();
            $applicantHistory->current_value = $workflow_stage_id;
            $applicantHistory->previous_value = $application->workflow_stage_id;
            $applicantHistory->status_type = 'application';
            $applicantHistory->company_id = $application->company_id;
            $applicantHistory->applicant_id = $application->id;
            $applicantHistory->save();

            $application->workflow_stage_id = $workflow_stage_id;
            if($application->hiring_status != 'none'){
                $application->hiring_status = 'none';
            }
            if(in_array($applicantHistory->stage->type,['applied','hired','rejected'])){
                $application->hiring_status = $applicantHistory->stage->type;
            }
            $application->save();
            // Save Timeline
            saveTimeline([
                'applicant_id' =>   $application->id,
                'entity'       =>   'stage',
                'entity_id'    =>   $workflow_stage_id,
                'user_id'      =>   $this->user->id ?? 0,
                'comments'     =>   "Stage Moved to {$applicantHistory->stage->title} by {$this->user->name}"
            ]);
            $this->performStageAction($application);
            $mailSetting = ApplicationSetting::select('id', 'mail_setting')->first()->mail_setting;
            return Reply::success("Applicant's workflow stage updated!");
        }
    }

    public function updateHiringStatus(Request $request)
    {
        $applicantStatuses = ApplicationStatus::get()->pluck('id','status');
        $applicant_id = $request->applicant_id;
        $status    = $request->hiring_status;
        if($request->has('hiring_status') && $request->hiring_status){
            $application = JobApplication::findOrFail($applicant_id);
            // If Interview Scheduled already
            if($application->workflow_stage->workflow_actions('interview')->count()){
                $interview = \App\InterviewSchedule::where('stage_id' , '=', $application->workflow_stage_id)->where('job_application_id' , '=', $application->id)->get();
                if(!($interview->count())){
                    return Reply::error('Interview not Scheduled by candidate.');
                }else{
                    if($application->schedule->status == 'pending'){
                        return Reply::error('Please complete Interview to move stage.');
                    }
                }
            }
            $application->hiring_status = $status;
            if($default_stage = $application->job->workflow->stages->where('type',$status)->first()){
                $application->workflow_stage_id = $default_stage->id;
            }
            $application->save();

            // Save Timeline
            saveTimeline([
                'applicant_id' =>   $application->id,
                'entity'       =>   $application->hiring_status=='hired' ? 'hire' : 'reject',
                'entity_id'    =>   $application->id,
                'user_id'      =>   $this->user->id ?? 0,
                'comments'     =>   "Applicant ". ucFirst($status) ." by {$this->user->name}"
            ]);

            /* Mail to Employee */
            $employee = User::allAdmins($application->schedule->company_id, $application->schedule->company_id)->first();
            if( $employee && !in_array($status,['rejected','hired']) ){
                try{
                    Notification::send($employee, new ScheduleInterviewStatus($application));
                } catch(\Exception $e) {}
            }
            /* End Mail to Employee */
            $interviewController = new \App\Http\Controllers\Admin\InterviewScheduleController;
            // mail to candidate for inform interview schedule status
                /* If Status is rejected */
                if($status==='rejected'){
                    // $is_allowed = Option::getOption('rejected_auto_email_enabled', $application->company_id);
                    // if($is_allowed && $request->mailToCandidate){
                    //     try {
                    //         Notification::send($application, new ScheduleStatusCandidate($application, $application->schedule));
                    //     } catch(\Exception $e) {}
                    // }
                    // // Send sms notification to admin on update 
                    // $interviewController->sendStatusChangeSms($status,$application,$applicantStatuses);
                    $this->performStageAction($application);
                    return Reply::success("Applicant's hiring status updated!");
                }
                /* end If Status is rejected */
                /* If Status is hired */
                if($status==='hired'){
                    // $is_hired_allowed = Option::getOption('hired_auto_email_enabled', $application->company_id);
                    // if($is_hired_allowed && $request->mailToCandidate){
                    //     try {
                    //         Notification::send($application, new ScheduleStatusCandidate($application, $application->schedule));
                    //     } catch(\Exception $e) {}
                    // }
                    // // Send sms notification to admin on update status
                    // $interviewController->sendStatusChangeSms($status,$application,$applicantStatuses);
                    $this->performStageAction($application);
                    return Reply::success("Applicant's hiring status updated!");
                }
                /* end If Status is hired */
        }
        return Reply::error("Oops! Something went wrong.");
    }

    public function performStageAction($application, $new = false){
        
        $workflow_id = $application->job->workflow_id ?? \App\Workflow::where('company_id', '=', $application->company_id)->first()->id;
        $workflow_stage_id = $application->workflow_stage_id ?? \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->first();
        $employee = \App\User::where('company_id' , '=' , $application->company_id)->first();
        $company = \App\Company::find($application->company_id);
        $email_actions = \App\WorkflowStageActionEmail::where('stage_id', '=', $workflow_stage_id)->get();
        $sms_action = \App\WorkflowStageActionSms::where('stage_id', '=', $workflow_stage_id)->get();
        $questionnaires_action = \App\WorkflowStageActionQuestionnaire::where('stage_id', '=', $workflow_stage_id)->where('questionnaire_type', 'external')->get();
        $interview_action = \App\WorkflowStageActionInterview::where('stage_id', '=', $workflow_stage_id)->get();
        $trigger_action = \App\WorkflowStageActionTrigger::where('stage_id', '=', $workflow_stage_id)->get();
        $application->workflow_stage_id = $workflow_stage_id;
        $email_attachment_action = \App\WorkflowStageActionEmailAttachment::where('stage_id', '=', $workflow_stage_id)->get();
        $application->save();
        $company_settings = Option::getAll($application->company_id);
        // $logo = '<table border="0" width="100"><tr><td style="text-align:center;"><a class="topbar-brand" href="' . url('/') . '"><img style="max-width:40%;" src="' .$company->logo_url . '" class="logo-inverse" alt="home" /></a></td></tr><table><br /><br />';
        $logo = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
        
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            return $workflow_stage_id;
        }

        $token = $application ? $application->email_token : '';
        $mainurl = route('candidate.schedule-inteview') . 
                "?c={$application->company_id}&app={$application->id}&t={$token}";
        
        // Email Attachment Template
        foreach ($email_attachment_action as $attachment) {
            $path = url('email-attachment'). '/' .$application->id.'.'.$attachment->id;
            $url = " <a href='". $path ."'>Upload your documents</a> ";
            $froms = [
                'hiring_manager' => ['name' => $employee->name, 'email' => $employee->email], 
                'company' => ['name' => $company->company_name, 'email' => $company->company_email],
                'engyj' => ['name' => 'WAZiE Recruit', 'email' => 'info@engyj.com']
            ];
            $tos['candidate'] = [$application->email];
            $pre_message = strtr($attachment->template->content ?? '', [
                '[applicant_name]'  => $application->full_name ?? '[applicant_name]',
                '[applicant_email]' => $application->email ?? '[applicant_email]',
                '[employee_name]'   => $employee->name ?? '[employee_name]',
                '[company_name]'    => $application->job->company->company_name ?? '[company_name]',
                '[interview_url]'   => $mainurl ?? '[interview_url]',
                '[schedule_date]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->schedule_date->format('M d, Y') : '[schedule_date]' : '[schedule_date]',
                '[schedule_time]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->slot_timing : '[schedule_time]' : '[schedule_date]',
                '[job_title]'       => $application->job->title ?? '[job_title]',
                '[qualification]'   => ucfirst(strtolower(explode(' ', $application->qualified)[0])),
                '[applied_note]'    => $application->applied_before ? '<br><strong>Note:</strong> <span style="color:red;">This applicant has applied before.</span>' : '',
                '[job_location]'    => '',
                '[job_location_address]'=> '',
            ]);
            $data = array(
                'from' => $froms[$attachment->from],
                'to' => $tos['candidate'],
                'message' => $logo . $pre_message . $url,
                'subject' => $attachment->template->subject
            );
            Mail::send(new NewUserNotification($data));
        }
        $from_email_name = Option::getOption('company_email_sender_name', $application->company_id);
        $from_email_from = Option::getOption('company_sender_email', $application->company_id);

        foreach($interview_action as $interview){
            $go = true;
            $sms_go = true;
            if($interview->email_invite == 0){
                if(Arr::get($company_settings, 'qualify_auto_email_enabled') == 'enable') {
                    $subject = \App\Option::getOption('qualified_subject', $application->company_id);
                    $message = \App\Option::getOption('qualified_text', $application->company_id);
                }else{
                    $go = false;
                }
            }else{
                $interview_email_template = \App\EmailTemplate::find($interview->email_invite);
                $subject = $interview_email_template->subject;
                $message = $interview_email_template->content;
            }
            if($interview->sms_invite == 0){
                
                if(Arr::get($company_settings, 'qualify_auto_sms_enabled') == 'enable') {
                    $sms_text = Arr::get($company_settings, 'sms_qualified_text');
                }else{
                    $sms_go = false;
                }
                
            }else{
                $interview_sms_template = \App\SmsTemplate::find($interview->sms_invite);
                $sms_text = $interview_sms_template->content;
            }
            $url = " <a href='" . $mainurl  . "'>Schedule Interview</a> ";
            $pre_subject = strtr($subject ?? '', ['[interview_session_title]' => $interview->session_title ?? '','[company_name]' => $application->company->company_name]);
            $pre_message = strtr($message ?? '', [
                '[applicant_name]'  => $application->full_name ?? '[applicant_name]',
                '[applicant_email]' => $application->email ?? '[applicant_email]',
                '[employee_name]'   => $employee->name ?? '[employee_name]',
                '[company_name]'    => $application->job->company->company_name ?? '[company_name]',
                '[interview_url]'   => $mainurl ?? '[interview_url]',
                '[interview_session_title]' => $interview->session_title ?? '',
                '[schedule_date]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->schedule_date->format('M d, Y') : '[schedule_date]' : '[schedule_date]',
                '[schedule_time]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->slot_timing : '[schedule_time]' : '[schedule_date]',
                '[job_title]'       => $application->job->title ?? '[job_title]',
                '[job_location]'    => '',
                '[job_location_address]'=> '',
            ]);
            
            $data = array(
                'from' => ['name' => $from_email_name, 'email' => $from_email_from],
                'to' => [$application->email],
                'message' => $logo .  $pre_message . $url,
                'subject' =>  $pre_subject
            );
            if($sms_go) {
                if(!empty($sms_text)) {
                    $message =  strtr($sms_text ?? '', [
                        '[applicant_name]'  => $application->full_name,
                        '[applicant_email]' => $application->email,
                        '[job_title]'       => $application->job->title,
                        '[company_name]'    => $application->job->company->company_name,
                        '[employee_name]'   => $employee->name,
                        '[interview_url]'   => $mainurl,
                        '[schedule_date]'   => '',
                        '[schedule_time]'   => '',
                        '[interview_session_title]' => $interview->session_title ?? '',
                        '[job_location]'    => '',
                        '[job_location_address]'=> ''
                    ]);
                } else {
                    $message = "";
                    $message .= "Dear {$application->full_name},\n";
                    $message .= "This is {$employee->name} from {$company->company_name}\n";
                    $message .= "Thankyou for submitting your application. We are delighted and look forward to meetimg you. Please schedule your interview by clicking the link below:\n";
                    
                    $message .= "\n" . $mainurl;
                }
                $log = new \App\SmsLog();
                $log->company_id = $application->company_id;
                $log->message_type = 'outbound';
                $log->type = 'normal';
                $log->number = str_replace(' ', '',  $application->phone);
                $log->message = $message;
                $log->status = 'pending';
                $log->send_at = date('Y-m-d h:i:s');
                $log->reference_name = 'applicant-alert';
                $log->reference_id = $application->id;
                $log->save();
            }
            if($go){
                Mail::send(new NewUserNotification($data));
            }
            
        }

        foreach($questionnaires_action as $question){
            $url = " <a href='" . url('/-') . "/" . $application->id . "." . $question->questionnaire_id   . "'>" .$question->questionnaire->name. "</a> ";
            $pre_message = strtr($question->template->content ?? '', [
                '[applicant_name]'  => $application->full_name ?? '[applicant_name]',
                '[applicant_email]' => $application->email ?? '[applicant_email]',
                '[employee_name]'   => $employee->name ?? '[employee_name]',
                '[company_name]'    => $application->job->company->company_name ?? '[company_name]',
                '[interview_url]'   => $mainurl ?? '[interview_url]',
                '[schedule_date]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->schedule_date->format('M d, Y') : '[schedule_date]' : '[schedule_date]',
                '[schedule_time]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->slot_timing : '[schedule_time]' : '[schedule_date]',
                '[job_title]'       => $application->job->title ?? '[job_title]',
                '[qualification]'   => ucfirst(strtolower(explode(' ', $application->qualified)[0])),
                '[applied_note]'    => $application->applied_before ? 'Note: This applicant has applied before.' : '',
                '[job_location]'    => '',
                '[job_location_address]'=> '',
            ]);
            $data = array(
                'from' => ['name' => $company->company_name, 'email' => $company->company_email],
                'to' => [$application->email],
                'message' =>  $logo .  $pre_message . $url,
                'subject' => $question->template->subject
            );
            Mail::send(new NewUserNotification($data));
        }


        foreach($email_actions as $email){
            $froms = [
                'hiring_manager' => ['name' => $employee->name, 'email' => $employee->email], 
                'company' => ['name' => $company->company_name, 'email' => $company->company_email],
                'engyj' => ['name' => 'WAZiE Recruit', 'email' => 'info@engyj.com']
            ];
            if($email->custom){
                while(strpos(' ', $email->custom) >= 0){
                    $email->custom = str_replace(' ', '', $email->custom);
                }
            }
            $custom = $email->custom_emails != null ? explode(',',$email->custom_emails) : [];
            foreach($custom as $k => $v){
                $custom[$k] = str_replace(" ", '', $custom[$k]);
            }

            $tos = [
                'hiring_manager' => [$employee->email], 
                'candidate' => [$application->email], 
                'hiring_team' => \App\User::where('company_id', '=', $application->company_id)->pluck('email')->toArray(), 
                'custom' =>  $custom
            ];
            $pre_message = strtr($email->template->content ?? '', [
                '[applicant_name]'  => $application->full_name ?? '[applicant_name]',
                '[applicant_email]' => $application->email ?? '[applicant_email]',
                '[employee_name]'   => $employee->name ?? '[employee_name]',
                '[company_name]'    => $application->job->company->company_name ?? '[company_name]',
                '[interview_url]'   => $mainurl ?? '[interview_url]',
                '[schedule_date]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->schedule_date->format('M d, Y') : '[schedule_date]' : '[schedule_date]',
                '[schedule_time]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->slot_timing : '[schedule_time]' : '[schedule_date]',
                '[job_title]'       => $application->job->title ?? '[job_title]',
                '[qualification]'   => ucfirst(strtolower(explode(' ', $application->qualified)[0])),
                '[applied_note]'    => $application->applied_before ? '<br><strong>Note:</strong> <span style="color:red;">This applicant has applied before.</span>' : '',
                '[job_location]'    => ($email->to == 'hiring_manager' && $application->schedule->location->location_name) ? "Location: {$application->schedule->location->location_name}" : '[job_location]',
                '[job_location_address]'=> ($email->to == 'hiring_manager' && $application->schedule->location->location) ? "{$application->schedule->location->location_name}" : '[job_location_address]',
            ]);
            $data = array(
                'from' => $froms[$email->from],
                'to' => $tos[$email->to],
                'message' => $logo .  $pre_message,
                'subject' => $email->template->subject
            );
            Mail::send(new NewUserNotification($data));
        }

        foreach($sms_action as $sms){
            $tos = [
                'hiring_manager' => [$employee->calling_code . "" . $employee->mobile], 
                'candidate' => [$application->phone],
                'hiring_team' => \App\User::select(DB::raw('CONCAT(calling_code,mobile) AS mobile'))->where('company_id', '=', $application->company_id)->pluck('mobile')->toArray()
            ];
            $pre_message = strtr($sms->template->content ?? '', [
                '[applicant_name]'  => $application->full_name ?? '[applicant_name]',
                '[applicant_email]' => $application->email ?? '[applicant_email]',
                '[employee_name]'   => $employee->name ?? '[employee_name]',
                '[company_name]'    => $application->job->company->company_name ?? '[company_name]',
                '[interview_url]'   => $mainurl ?? '[interview_url]',
                '[schedule_date]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->schedule_date->format('M d, Y') : '[schedule_date]' : '[schedule_date]',
                '[schedule_time]'   => $application->schedule->count() ? $application->schedule->status == 'pending' ? $application->schedule->slot_timing : '[schedule_time]' : '[schedule_date]',
                '[job_title]'       => $application->job->title ?? '[job_title]',
                '[qualification]'   => ucfirst(strtolower(explode(' ', $application->qualified)[0])),
                '[applied_note]'    => $application->applied_before ? 'Note: This applicant has applied before.' : '',
                '[job_location]'    => ($sms->to == 'hiring_manager' && $application->schedule->location->location_name) ? "Location: {$application->schedule->location->location_name}" : '[job_location]',
                '[job_location_address]'=> ($sms->to == 'hiring_manager' && $application->schedule->location->location) ? "{$application->schedule->location->location_name}" : '[job_location_address]',
            ]);
            foreach($tos[$sms->to] as $to){
                $log = new SmsLog();
                $log->company_id = $application->company_id;
                $log->message_type = 'outbound';
                $log->type = 'normal';
                $log->number = $to;
                $log->message = $pre_message;
                $log->status = 'pending';
                $log->send_at = date('Y-m-d h:i:s');
                $log->reference_name = 'workflow';
                $log->save();
            }
        }
        foreach($trigger_action as $trigger){
            $scope = $trigger->trigger->scope;
            $conditions = $trigger->trigger->conditions;
            $finally = [];
            foreach($conditions as $condition){
                switch($condition->metric){
                    case 'qualification' :
                        $finally[] = getTriggerOperator($condition->operator, $application->eligibility ?? 0, $condition->value);
                    break;
                }
            }
            if($scope == 'all'){
                $super_finally = !in_array(false, $finally);
            }else{
                $super_finally = in_array(true, $finally);
            }
            if($super_finally){
                //Here we go ....
                $application->workflow_stage_id = $trigger->to_stage;
                $application->save();
                if(in_array($trigger->stage->type, ['hired', 'rejected'])){
                    $application->hiring_status = $trigger->stage->type;
                    $application->save();
                }
                $this->performStageAction($application);
                saveTimeline([
                    'applicant_id' =>   $application->id,
                    'entity'       =>   'stage',
                    'entity_id'    =>   $trigger->to_stage,
                    'comments'     =>   "Stage Moved to " . (\App\WorkflowStage::find($trigger->to_stage)->title) . " by Trigger ({$trigger->trigger->name})"
                ]);
            }
        }

        if($new){
            $applicantHistory = new ApplicantHistory();
            $applicantHistory->current_value = $workflow_stage_id;
            $applicantHistory->status_type = 'application';
            $applicantHistory->company_id = $application->company_id;
            $applicantHistory->applicant_id = $application->id;
            $applicantHistory->save();
        }
        return $workflow_stage_id;
    }
    public function multiDelete(Request $request)
    {
        $data = $request->input('id');
        $return = [];
        
        foreach ($data as $id) {
            $return[] = $this->destroy($id);
        }

        return $return[0];
    }

    public function multiArchive(Request $request)
    {
        $data = $request->input('id');
        $apps = JobApplication::whereIn('id',$data);
        return ($apps->count() && $apps->delete()) ? Reply::success(__('messages.applicationsArchivedSuccessfully')) : false;
    }

    public function sendStatusChangeSms($status_id,$jobApplication,$applicantStatuses)
    {
        $url = route('candidate.schedule-inteview')."?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
        $status_by_id = array_flip($applicantStatuses->toArray());
        if($jobApplication && isset($status_by_id[$status_id]) && $status_by_id[$status_id]){
            $message = __('email.candidateStatusUpdate.text').' - ' . ucwords($jobApplication->job->title).__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($status_by_id[$status_id]).__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($status_by_id[$status_id]).__('email.thankyouNote');
            $employees = User::frontAllAdmins(company()->id);
            $from = Option::getOption('twilio_from_number', company()->id);
            $from = $from ?? config('twilio-notification-channel.from');
            $status = false;
            switch ($status_by_id[$status_id]) {
                // If Status Interview
                case 'interview':
                    $settings = ApplicationSetting::where('company_id', company()->id)->first();
                    $sms_setting = ($settings && $settings->sms_setting) ? $settings->sms_setting : [];
                    if(array_key_exists($status_id,$sms_setting) && count($sms_setting[$status_id]) && $sms_setting[$status_id]['status']){
                        $has_dynamic = Option::getOption('sms_qualified_text', company()->id);
                        if($jobApplication->job->workflow_id != 0){
                            $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                        }
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'=> $jobApplication->full_name,
                                '[applicant_email]'=> $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $url,
                            ]);
                            if($jobApplication->job->workflow_id != 0){
                                $message = strtr($message ?? '', [
                                    '[interview_session_title]' =>  $interview_action->sesstion_title,
                                ]);
                            }else{
                                $message = strtr($message ?? '', [
                                    '[interview_session_title]' => '',
                                ]);
                            }
                        }
                        // Twilio::set($jobApplication->phone, $message,$from)->send();
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
                // If Status Hired
                case 'hired':
                    $is_allowed = Option::getOption('hired_auto_sms_enabled', company()->id);
                    if($is_allowed){
                        $has_dynamic = Option::getOption('sms_hired_applicant_text', company()->id);
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'  => $jobApplication->full_name,
                                '[applicant_email]' => $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $url,
                                '[job_title]'     => $jobApplication->job->title
                            ]);
                        }
                        // Twilio::set($jobApplication->phone, $message,$from)->send();
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
                // If Status No Show
                case 'no show':
                    $is_allowed = Option::getOption('no_show_auto_sms_enabled', company()->id);
                    if($is_allowed){
                        $has_dynamic = Option::getOption('sms_no_show_applicant_text', company()->id);
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'=> $jobApplication->full_name,
                                '[applicant_email]'=> $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $url,
                                '[job_title]'     => $jobApplication->job->title
                            ]);
                        }
                        // Twilio::set($jobApplication->phone, $message,$from)->send();
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
                // If Status Rejected
                case 'rejected':
                    $is_allowed = Option::getOption('rejected_auto_sms_enabled', company()->id);
                    if($is_allowed){
                        $has_dynamic = Option::getOption('sms_rejected_applicant_text', company()->id);
                        if($has_dynamic){
                            $message = strtr($has_dynamic, [
                                '[applicant_name]'=> $jobApplication->full_name,
                                '[applicant_email]'=> $jobApplication->email,
                                '[company_name]'  => $jobApplication->company->company_name,
                                '[employee_name]' => $employees[0]->name,
                                '[interview_url]' => $url,
                                '[job_title]'     => $jobApplication->job->title
                            ]);
                        }
                        // Twilio::set($jobApplication->phone, $message,$from)->send();
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                    }
                break;
            }
        }
    }
}
