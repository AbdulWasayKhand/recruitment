<?php

namespace App\Http\Controllers\Admin;

use App\JobDescriptionTemplate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Helper\Reply;
use Illuminate\Support\Arr;

class AdminJobDescriptionTemplateController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle ='menu.jobTemplates';
        $this->pageIcon = 'icon-badge';
    }

    public function index()
    {
        // $this->templates = JobDescriptionTemplate::select('id', 'title')->where('company_id', $this->user->company_id)->get();
        return view('admin.job-templates.index', $this->data);
    }
    
    public function create()
    {
        
        return view('admin.job-templates.create', $this->data);
    }

    public function show($id)
    {
        $template = JobDescriptionTemplate::select('title', 'content', 'meta_data')->findOrFail($id);

        return $template;
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string',
            'job_description' => 'required',
            'job_requirement' => 'required',
        ]);

        $template = JobDescriptionTemplate::create([
            'title' => $validated['title'],
            'company_id' => $this->user->company_id,
            'content' => Arr::only($validated, ['job_description', 'job_requirement'])
        ]);

        return Reply::redirect(route('admin.job-templates.index'), __('menu.jobTemplates') . ' ' . __('messages.jobTemplateAdded'));
    }

    public function update(Request $request, $id) {
        $validated = $request->validate([
            'title' => 'required|string',
            'job_description' => 'required',
            'job_requirement' => 'required',
        ]);
        
        $template = JobDescriptionTemplate::findOrFail($id);
        $template->title = $validated['title'];
        $template->content = [
            'job_description' => $validated['job_description'],
            'job_requirement' => $validated['job_requirement'],
        ];

        $template->save();

        return Reply::redirect(route('admin.job-templates.index'), __('menu.jobTemplates') . ' ' . __('messages.updatedSuccessfully'));
    }

    public function destroy($id) {
        abort_if(!$this->user->cans('delete_jobs'), 403);

        JobDescriptionTemplate::destroy($id);
        return Reply::success(__('messages.recordDeleted'));
    }
    
    public function edit($id) {
        $this->template = JobDescriptionTemplate::whereId($id)->where('company_id', $this->user->company_id)->first();

        return view('admin.job-templates.edit', $this->data);
    }

    public function searchTemplates(Request $request)
    {
        $response = ['results' => []];
        $search = $request->q;
        $response['results'] = JobDescriptionTemplate::select('id', 'title')
                            ->where('company_id', $this->user->company_id)
                            ->where('title', 'LIKE', "%$search%")
                            ->get();

        return $response;
    }

    public function data()
    {
        abort_if(!$this->user->cans('view_jobs'), 403);

        $templates = JobDescriptionTemplate::where('company_id', $this->user->company_id);
        $templates->select('id', 'title');
        $templates->get();


        return DataTables::of($templates)
            ->addColumn('action', function ($row) {
                $action = '';

                if ($this->user->cans('edit_jobs')) {
                    $action .= '<a href="' . route('admin.job-templates.edit', [$row->id]) . '" class="btn btn-primary btn-circle"
                      data-toggle="tooltip" data-original-title="' . __('app.edit') . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }
                if ($this->user->cans('delete_jobs')) {
                    $action .= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="' . __('app.delete') . '"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
                return $action;
            })
            ->editColumn('title', function ($row) {
                return ucfirst($row->title);
            })
            ->addIndexColumn()
            ->make(true);
    }
}
