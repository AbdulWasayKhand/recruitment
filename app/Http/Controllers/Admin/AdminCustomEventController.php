<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\CustomEvent;
use DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AdminCustomEventController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle ='menu.customEvents';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.custom-event.index', $this->data);
    }

    /**
     * Show.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->id  = $id;
        $this->event = CustomEvent::findOrNew($id);
        return view('admin.custom-event.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->event = new CustomEvent;
        return view('admin.custom-event.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $user = \App\User::where('company_id', '=', $this->user->company_id)->first();
        $id = $request->id;
        $day  = Carbon::createFromFormat('m-d-Y',$request->date)->timezone($this->user->company->timezone)->format('Y-m-d');
        $event = CustomEvent::findOrNew($id);
        $event->fill($request->all());
        $event->date = $day;
        $event->user_id = $this->user->id;
        $event->block_complete_day = $request->has('block_complete_day') ? $request->block_complete_day : 0;
        $event->delete_google_event = $request->has('delete_google_event') ? $request->delete_google_event : 0;
        $event->clone_google_event = $request->has('clone_google_event') ? $request->clone_google_event : 0;
        if($request->has('time_from')){
            $event->time_range = $request->time_from.' - '.$request->time_to;
            $fromTime = date('H:i', strtotime($request->time_from)) . ":00";
            $toTime = date('H:i', strtotime($request->time_to)) . ":00";
        }
        if($event->clone_google_event){
            $from =  $day . 'T' . ($fromTime ?? '00:00:00');
            $to =  $day . 'T' . ($toTime ?? '23:59:00');
            if($event->eid != ''){
                cancleGC($user, $event->eid);
            }
            $event->eid = createGoogleEvent($event->title, $event->description,  $from,  $to,  $user );
            if($event->delete_google_event){
                cancleGC($user, $event->eid);
                $event->eid = '';
            }
        }
        $event->save();
        $msg = $id ? __('modules.customEvent.updateMsg') : __('modules.customEvent.createMsg');
        return Reply::redirect(route('admin.custom-event.index'), $msg);
    }

    public function edit($id)
    {
        $this->id = $id;
        $this->event = CustomEvent::findOrNew($id);
        return view('admin.custom-event.edit', $this->data);
    }

    public function data() {
        $questionnaire = CustomEvent::all();
        return DataTables::of($questionnaire)
            ->addColumn('selectColumn', function($row) {
                $selectRow = '<div class="checkbox form-check">
                                <input id="' . $row->id . '" type="checkbox" name="id[]" class="form-check-input multi-check" value="' . $row->id . '" >
                                <label for="' . $row->id . '"></label>
                            </div>';
                return $selectRow;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                    $edit = route('admin.custom-event.edit', [$row->id]);
                    $action.= '<a href="javascript:openInModal(\'' .$edit. '\');" class="btn btn-primary btn-circle"
                      data-toggle="tooltip" data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    $action.= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
                return $action;
            })
            ->editColumn('title', function ($row) {
                return ucfirst($row->title);
            })
            ->editColumn('date', function ($row) {
                return Carbon::parse($row->date)->format('M d, Y');
            })
            ->editColumn('time_range', function ($row) {
                return $row->time_range;
            })
            ->rawColumns(['action', 'selectColumn'])
           ->make(true);
    }

    public function destroy($id)
    {
        
        $event = CustomEvent::find($id)->first();
        if($event->eid != ''){
            $user = \App\User::where('company_id', '=', $this->user->company_id)->first();
            cancleGC($user, $event->eid);
        }
        CustomEvent::destroy($id);
        return Reply::success(__('messages.eventDeleted'));
    }

    public function multiDelete(Request $request)
    {
        $data = $request->input('id');
        $return = [];
        
        foreach ($data as $id) {
            $return[] = $this->destroy($id);
        }
        return $return[0];
    }
}