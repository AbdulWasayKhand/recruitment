<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\UpdateProfile;
use App\Option;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class AdminProfileController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.myProfile';
        $this->pageIcon = 'ti-user';
    }

    public function index()
    {
        $this->calling_codes = $this->getCallingCodes();
        $this->timezone = get_timezone_list(company()->timezone) . "(" . company()->timezone . ")";
        $this->profile_online_schedule = 1;
        $this->profile_schedule = $this->user->profile_schedule;
        $this->optionEmp = $this->user->GC_refresh_token; //Option::getOption('employee_refresh_token', $this->user->company_id, $this->user->id);
        $this->userId = $this->user->id;
        return view('admin.profile.index', $this->data);
    }

    public function profileSchedule()
    {
        $this->timezone = get_timezone_list(company()->timezone) . "(" . company()->timezone . ")";
        $this->profile_online_schedule = Option::getOption('profile_online_schedule', $this->user->company_id, $this->user->id) == 1 ? 1 : 0;
        $this->profile_schedule = $this->user->profile_schedule;
        
        return view('admin.profile.schedule', $this->data);
    }

    public function profileScheduleSave(Request $request)
    {
        $data = $request->except('_token');

        $profile_online_schedule = 0;
        if(isset($data['profile_online_schedule'])){
            $profile_online_schedule = $data['profile_online_schedule'];
        }

        Option::setOption('profile_online_schedule', $profile_online_schedule, $this->user->company_id, $this->user->id);
        if($profile_online_schedule == 1){
            User::where('id',$this->user->id)->update(["profile_schedule" => $data]);
        }

        return Reply::success("Profile schedule updated!");
    }

    public function update(UpdateProfile $request)
    {
        $user = User::find($this->user->id);
        $user->can_text = $request->can_text ?? 0;
        $user->bussness_hours_only = $request->bussness_hours_only ?? 0;
        $user->sms_auto_reminder_hour = $request->sms_auto_reminder_hour ?? 'disable';
        $user->sms_auto_reminder_day = $request->sms_auto_reminder_day ?? 'disable';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile_schedule = $request->schedule;
        $user->job_wise = $request->job_wise;

        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }

        if ($request->has('mobile')) {
            if ($user->mobile !== $request->mobile || $user->calling_code !== $request->calling_code) {
                $user->mobile_verified = 0;
            }

            $user->mobile = $request->mobile;
            $user->calling_code = $request->calling_code;
        }

        if ($request->hasFile('image')) {
            $user->image = Files::upload($request->image, 'profile');
        }
        $user->time_in = $request->time_in;
        $user->time_out = $request->time_out;
        $user->slot = $request->slot;
        $user->block_all_day_event = $request->has('block_all_day_event') ? 1 : 0;
        $user->save();

        return Reply::redirect(route('admin.profile.index'), __('menu.myProfile') . ' ' . __('messages.updatedSuccessfully'));
    }
}
