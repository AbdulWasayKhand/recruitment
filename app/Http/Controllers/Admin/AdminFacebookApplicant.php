<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Job;
use App\Company;
use App\JobApplication;
use App\ApplicationStatus;
use App\FacebookPage;
use App\FacebookLead;
use App\FacebookLeadData;
use App\Helper\FBApi;
use Auth;
use App\Helper\Reply;
use App\JobApplicationAnswer;
use App\Question;

class AdminFacebookApplicant extends AdminBaseController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.facebookApplicants';
        $this->pageIcon = 'icon-settings';
    }

    public function index(Request $request)
    {
        $api = (new FBApi())->currentUser();
        $api_leads = $api->getLeads()['data'];
        
        $leads = FacebookLead::select('lead_id as id', 'name', 'enabled')->where('company_id', $request->user()->company_id)->get()->toArray();
        if(count($leads) < count($api_leads)){
            //  Need To be update;
            $local_leads = FacebookLead::all()->pluck('lead_id')->toArray();
            $insertData = [];
            foreach($api_leads as $cumming_lead){
                if(!in_array($cumming_lead["id"] , $local_leads)){
                    $insertData[] = [
                        'company_id' => company()->id,
                        'lead_id'    => $cumming_lead['id'],
                        'name'       => $cumming_lead['name'],
                        'enabled'    => intval(Arr::get($cumming_lead, 'enabled', 0)),
                        'created_at' => now()->toDateTimeString(),
                    ];
                }
            }
            FacebookLead::insert($insertData);
            sleep(5);
        }
        if(count($leads) > 0) {
            $this->leads = $leads;
        } else {
            $this->leads = Arr::get($api->getLeads(), 'data', []);
        }

        // return $leads;
        
        return view('admin.facebook-applicants.index', $this->data);
    }
    
    public function show(Request $request, $id)
    {
        $api = (new FBApi())->currentUser();
        $this->applicants = $api->getLeadApplicants($id);
        
        return view('admin.facebook-applicants.show', $this->data);
    }

    public function saveLead(Request $request)
    {
        $data = $request->except('_token');
        $data = array_values($data['leads']);
        // $data = array_filter($data, function($arr) { return isset($arr['id']); });
        
        $user = $request->user();
        $company_id = $user->company_id;
        FacebookLead::where('company_id', $company_id)->delete();

        $insertData = [];
        foreach ($data as $d) {
            $insertData[] = [
                'company_id' => $company_id,
                'lead_id'    => $d['lead_id'],
                'name'       => $d['name'],
                'enabled'    => intval(Arr::get($d, 'enabled', 0)),
                'created_at' => now()->toDateTimeString(),
            ];
        }

        FacebookLead::insert($insertData);

        return back()->with('status', 'Leads Updated');
    }

    public function show_date(Request $request)
    {
        $lead_id = $request->lead_id;
        $api = (new FBApi())->currentUser();

        $app = $api->getLeadApplicants($lead_id);
        if($data = Arr::get($app, 'data', null) && count($app['data']) > 0) {
            return ['date' => \Carbon\Carbon::parse($app['data'][0]['created_time'])
                                    ->setTimeZone('Asia/Karachi')
                                    ->format('Y-m-d H:i:s')];
        }

        return ['date' => false, 'response' => $app];
    }

    public function syncFacebookApplicants(Request $request)
    {
        $job_id = $request->job_id;
        abort_if(!is_numeric($job_id), 404);

        $job = Job::findOrFail($job_id);
        
        if(is_null($job->fb_lead_id)) {
            return abort(403);
        }
        $debug = [];
        $api = (new FBApi())->currentUser();
        $applicants = [];
        $app = $api->getLeadApplicants($job->fb_lead_id);
        while (true) {
            foreach (Arr::get($app, 'data', []) as $d1) {
                $data = [
                    'fb_id' => $d1['id'], 
                    'created_at' => \Carbon\Carbon::parse($d1['created_time'])
                                        ->setTimeZone('Asia/Karachi')
                                        ->format('Y-m-d H:i:s'),
                ];
                foreach (Arr::get($d1, 'field_data', []) as $field_data) {
                    $data[$field_data['name']] = implode(', ',$field_data['values']);
                }
                $applicants[] = $data;
            }

            if(isset($app['paging']['next']) && !empty($app['paging']['next'])) {
                $app = $api->getLeadApplicants($job->fb_lead_id, $app['paging']['cursors']['after']);
                continue;
            }
            
            $lead = FacebookLead::where('lead_id', $job->fb_lead_id)->first();
            $lead->last_page = Arr::get($app, 'paging.cursors.after');
            $lead->save();
            
            break;
        }

        $applicationStatus = ApplicationStatus::where('company_id', $job->company_id)->firstOrFail();

        $applicantsData = [];
        foreach ($applicants as $applicant) {
            if(Arr::get($applicant, 'fb_id', false) 
                && !FacebookLeadData::where('fb_id', Arr::get($applicant, 'fb_id'))
                                    ->where('fb_lead_id', $job->fb_lead_id)->exists()) {
                $db_applicant = JobApplication::insertGetId([
                    'full_name' => Arr::get($applicant, 'full_name', ''),
                    'email' => Arr::get($applicant, 'email', ''),
                    'phone' => Arr::get($applicant, 'phone_number', ''),
                    'job_id' => $job->id,
                    'company_id' => $job->company_id,
                    'status_id' => $applicationStatus->id,
                    'source' => 'facebook',
                    'created_at' => Arr::get($applicant, 'created_at', now()->toDateTimeString()),
                ]);
                
                $job_questions = Arr::except($applicant, ['full_name', 'email', 'phone_number', 'fb_id', 'created_at']);

                FacebookLeadData::insert([
                    'fb_id' => Arr::get($applicant, 'fb_id', '0'),
                    'fb_lead_id' => $job->fb_lead_id,
                    'company_id' => $job->company_id,
                    'job_application_id' => $db_applicant,
                    'data' => json_encode($job_questions),
                ]);

                $totalScore = 0;
                $totalCount = 0;
                foreach ($job_questions as $_question => $_answer) {
                    $q = str_replace('_', ' ', $_question);
                    $answer_value = str_replace('_', ' ', $_answer);

                    $question = Question::with('custom_fields')
                                    ->where('company_id', $job->company_id)
                                    ->where('question', 'LIKE', '%'.$q.'%')
                                    ->first();
                    $debug[] = [$q, $question];
                    if($question) {
                        $answer = new JobApplicationAnswer();
                        $answer->job_application_id = $db_applicant;
                        $answer->job_id = $job->id;
                        $answer->question_id = $question->id;
                        $answer->company_id = $job->company_id;
                        $answer->answer = $answer_value;
                        
                        if($question && $question->custom_fields->count() > 0) {
                            $custom_question = $question->custom_fields[0];
                            $question_value  = json_decode($custom_question->question_value, 1);
                            $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                            if(is_array($question_qualifiable)) {
                                $answer->score = 0;
                                $_total = count($question_qualifiable);
                                $totalCount += 1;
                                foreach($question_qualifiable as $q) {
                                    if(in_array(strtolower($question_value[$q]), (array)$answer_value)) {
                                        $answer->score += 1;
                                        $totalScore += (1 / $_total);
                                    }
                                }
                            }
                        }
            
                        $answer->save();
                    }
                }
                
                if($totalCount > 0) {
                    $jobApplication = JobApplication::find($db_applicant);
                    $jobApplication->eligibility = ($totalScore / ($totalCount > 0 ? $totalCount : 1)) * 100;
                    $jobApplication->save();
                }

            }
        }
        
        return [
            'status' => 'success',
            'message' => 'Data sync successfully!',
            'debug' => $debug,
        ];
    }

}
