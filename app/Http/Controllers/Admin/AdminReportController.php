<?php

namespace App\Http\Controllers\Admin;

use App\ApplicationStatus;
use App\InterviewSchedule;
use App\Job;
use App\JobApplication;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Option;
use App\Budget;
use Illuminate\Support\Arr;
use Yajra\DataTables\Facades\DataTables;
use App\ApplicantHistory;

class AdminReportController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'icon-speedometer';
        $this->pageTitle = 'menu.reports';
    }

    public function index(){
        $this->jobs = Job::all()->count();
        $this->openJobs = Job::where('status', 'active')->count();
        $this->jobs_dropdown = Job::where('status', 'active')->pluck('title','id');
        return view('admin.reports.index', $this->data);
    }
    public function funnel_data(Request $request){
        $start_date = $request->start_date;
        $end_date   = $request->end_date;
        $workflow_id = \App\Job::find($request->job_id)->workflow_id;

        $status = ApplicationStatus::get()->pluck('id', 'status');
        $applicants = JobApplication::whereDate('created_at','>=',$start_date)->whereDate('created_at','<=', $end_date)->where('job_id', '=', $request->job_id)->get();
        $total_unqualified = $applicants->filter(function ($value, $key)  { return $value->eligibility  < $value->job->eligibility_percentage; })->count();
        $total_qualified = $applicants->count() - $total_unqualified;
        $type = 'Funnel';
        $desc = 'Drop Offs at different stages of the hiring process';
        $data = [
            ['title' => 'Applied', 'total' => $applicants->count(), "previous" => 0,'extra' => 0], 
            ['title' => 'Unqualified', 'total' => $total_unqualified , "previous" => 0, 'extra' => 1], 
        ];
        if($workflow_id != 0){
            $type = 'Stage';
            $desc = 'Currently in the process at different stages of the hiring process';
            $stage_titles = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->orderBy('position')->pluck('id','title');
            $pre_stages = [];
            foreach($stage_titles as $key=>$stage){
                $totals = 0;
                $previous = 0;
                foreach($applicants as $applicant){

                    // $previous += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)->where('previous_value', '=', $stage)->get()->groupBy('applicant_id')->count();
                    $previous += \App\ApplicantTimeline::where('applicant_id', '=', $applicant->id)->where('entity', '=', 'stage')->where('entity_id', '=', $stage)->get()->groupBy('applicant_id')->count();
                    if($stage == $applicant->workflow_stage_id){
                        $totals++;
                    }
                }
                $pre_stages[] = ['title' => $key, 'total' => $totals, 'previous' => $previous, 'extra' => 1];
            }
            foreach($pre_stages as $k=>$v){
                $data[] = $v;
            }
        }else{
            $interview = 0;
            foreach($applicants as $applicant){
                $interview += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)
                                                    ->where('current_value', '=', $status['interview'])
                                                    ->groupBy('applicant_id')
                                                    ->get()->count();
            }
            $interview_previous = $applicants->where('status_id', '=',  $status['interview'])->count();

            $phone_screen = 0;
            foreach($applicants as $applicant){
                $phone_screen += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)->where('current_value', '=', $status['phone screen'])->get()->count();
            }
            $phone_screen_previous = $applicants->where('status_id', '=',  $status['phone screen'])->count();

            $background = $applicants->where('status_id', '=',  $status['hired'])->count();

    
            foreach([
            ['title' => 'Phone Screen', 'total' => $phone_screen_previous, "previous" => $phone_screen, 'extra' => 1], 
            ['title' => 'Interview', 'total' => $interview_previous, "previous" => $interview ,'extra' => 1],
            ['title' => 'Hired', 'total' => $background, "previous" => 0, 'extra' => 0], 
            ] as $k=>$v){
                $data[] = $v;
            }
        }
        return ['request'=>$request, 'data'=>$data, 'qualified' => $total_qualified, 'type' => $type, 'desc' => $desc];
    }

    public function funnel_data_two(Request $request){
        $start_date = $request->start_date;
        $end_date   = $request->end_date;
        $workflow_id = \App\Job::find($request->job_id)->workflow_id;

        $status = ApplicationStatus::get()->pluck('id', 'status');
        $applicants = JobApplication::whereDate('created_at','>=',$start_date)->whereDate('created_at','<=', $end_date)->where('job_id', '=', $request->job_id)->get();
        $total_unqualified = $applicants->filter(function ($value, $key)  { return $value->eligibility  < $value->job->eligibility_percentage; })->count();
        $total_qualified = $applicants->count() - $total_unqualified;
        $data = [
            ['title' => 'Applied', 'total' => $applicants->count(), "previous" => 0,'extra' => 0], 
            ['title' => 'Unqualified', 'total' => $total_unqualified , "previous" => 0, 'extra' => 1], 
        ];
        if($workflow_id != 0){
            $stage_titles = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->orderBy('position')->pluck('id','title');
            $pre_stages = [];
            foreach($stage_titles as $key=>$stage){
                $totals = 0;
                $previous = 0;
                foreach($applicants as $applicant){

                    // $previous += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)->where('previous_value', '=', $stage)->get()->groupBy('applicant_id')->count();
                    $previous += \App\ApplicantTimeline::where('applicant_id', '=', $applicant->id)->where('entity', '=', 'stage')->where('entity_id', '=', $stage)->get()->groupBy('applicant_id')->count();
                    if($stage == $applicant->workflow_stage_id && $applicant->hiring_status == 'rejected'){
                        $totals++;
                    }
                }
                $pre_stages[] = ['title' => $key, 'total' => $totals, 'previous' => $previous, 'extra' => 1];
            }
            foreach($pre_stages as $k=>$v){
                $data[] = $v;
            }
        }else{
            $interview = 0;
            foreach($applicants as $applicant){
                $interview += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)
                                                    ->where('current_value', '=', $status['interview'])
                                                    ->groupBy('applicant_id')
                                                    ->get()->count();
            }
            $interview_previous = $applicants->where('status_id', '=',  $status['interview'])->count();

            $phone_screen = 0;
            foreach($applicants as $applicant){
                $phone_screen += \App\ApplicantHistory::where('applicant_id', '=', $applicant->id)->where('current_value', '=', $status['phone screen'])->get()->count();
            }
            $phone_screen_previous = $applicants->where('status_id', '=',  $status['phone screen'])->count();

            $background = $applicants->where('status_id', '=',  $status['hired'])->count();

    
            foreach([
            ['title' => 'Phone Screen', 'total' => $phone_screen_previous, "previous" => $phone_screen, 'extra' => 1], 
            ['title' => 'Interview', 'total' => $interview_previous, "previous" => $interview ,'extra' => 1],
            ['title' => 'Hired', 'total' => $background, "previous" => 0, 'extra' => 0], 
            ] as $k=>$v){
                $data[] = $v;
            }
        }
        return ['request'=>$request, 'data'=>$data, 'qualified' => $total_qualified];
    }
    public function stage_data(Request $request){
            $workflow_id = \App\Job::find($request->job_id)->workflow_id;
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
            $stage_titles = ['Total Applied','Applied', 'Phone Screen', 'Interviewed', 'Hired', 'Rejected'];
            $colors = ['#007bff','#d2d6de','#3c8dbc','#28a745','#dc3545'];
            $first_column = "Total Applied";
            foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
                if($key != 'Others' &&  $key != 'Direct'){
                    $sources[] = [
                        'source' => $key,
                        'Total Applied' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                        'Applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['applied'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'like', '%'. $keywords.'%')->count(),
                        'Phone Screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                        ->whereDate('updated_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['phone screen'])
                                        ->where('job_id', '=', $request->job_id)
                                        ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                        'Interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                        ->whereDate('updated_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['interview'])
                                        ->where('job_id', '=', $request->job_id)
                                        ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                                        'Hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                        ->whereDate('updated_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['hired'])
                                        ->where('job_id', '=', $request->job_id)
                                        ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                        'Rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['rejected'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                    ];
                }else if($key == 'Others'){
                    $sources[] = [
                        'Total Applied' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                        'source' => $key,
                        'Applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['applied'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                        'Phone Screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['phone screen'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                        'Interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['interview'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                        'Hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['hired'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                        'Rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['rejected'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')->count(),
                    ];
                }else if($key == 'Direct'){
                    $sources[] = [
                        'source' => $key,
                        'Total Applied' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('job_id', '=', $request->job_id)
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                        'Applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['applied'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                        'Phone Screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['phone screen'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                        'Interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('status_id', $appStatuses['interview'])
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                        'Hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['hired'])
                                    ->where('job_id', '=', $request->job_id)
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                        'Rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('status_id', $appStatuses['rejected'])
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->count(),
                    ];
                }
            }

            if($workflow_id != 0){
                $stage_titles = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('title');
                $stage_id = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('id', 'title');
                $colors = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('color');

                $all_applicant = \App\JobApplication::where('job_id','=',$request->job_id)->get();
                foreach($all_applicant as $key=>$app){
                    if(!in_array($app->workflow_stage_id, $stage_id->toArray())){
                        $all_applicant[$key]->workflow_stage_id = $stage_id->first();
                    }
                }
                $app = [];
                foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
                    foreach($stage_id as $stage=>$sid){
                        foreach($all_applicant as $applicant){
                            if($applicant->workflow_stage_id == $sid){
                                if(gettype(strpos($applicant->referer_url, $keywords)) == 'integer'){
                                    $app[$key][$applicant->id] = $applicant;
                                }else if($applicant->referer_url == null){
                                    $app['Direct'][$applicant->id] = $applicant;
                                }else if(gettype(strpos($applicant->referer_url, 'facebook')) != 'integer' && gettype(strpos($applicant->referer_url, 'google')) != 'integer' && gettype(strpos($applicant->referer_url, 'indeed')) != 'integer' && gettype(strpos($applicant->referer_url, 'linkedin')) != 'integer'){
                                    $app['Others'][$applicant->id] = $applicant;
                                }
                            }
                        }
                    }  
                }
                if(isset($app['Others'])){
                    foreach($app['Others'] as $k=>$v){
                        if(isset($app['Direct'])){
                            foreach($app['Direct'] as $k2=>$v2){
                                if($k == $k2){
                                    unset($app['Others'][$k]);
                                }
                            }
                        }
                    }
                }
                $sources = [];
                foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
                    $arr_1 = ['source' => $key];
                    $counter = 0;
                    foreach($stage_id as $stage=>$id){
                        $arr_1 =  array_merge($arr_1, [ $stage  => 0]);
                        if(isset($app[$key])){
                            foreach($app[$key] as $appli){
                                if($appli->workflow_stage_id == $id){
                                    $arr_1[$stage] += 1;
                                }
                            }
                        }
                        if($counter == 0){
                            $first_column = $stage;
                        }
                        $counter++;
                    }
                    $sources[] = $arr_1;
                }
            }
            $data = [];
            for($i = 0 ; $i <  6 ; $i++ ){
                foreach($stage_titles as $k=>$v){
                    $data[$k] = $sources[$i][$v] ?? 0;  
                };
                break;
            }
            for($i = 1 ; $i <  6 ; $i++ ){
                foreach($stage_titles as $k=>$v){
                    $data[$k] += $sources[$i][$v] ?? 0;  
                };
            }
            $sorter = array();
            foreach ($sources as $key => $row)
            {
                $sorter[$key] = $row[$first_column];
            }
            array_multisort($sorter, SORT_DESC, $sources);
        return [
            'data'      => $data,
            'stages'    => $stage_titles,
            'sources'   => $sources,
            'colors'    => $colors
        ];
    }
    public function get_engyj_data_for_dashboard(Request $request){
            if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
                
                $workflow_id = isset($request->job_id) ? \App\Job::find($request->job_id)->workflow_id : 0;
    
                if($workflow_id != 0){
                    $stage_titles = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('title');
                }
                
                $start_date = $request->start_date;
                $end_date   = $request->end_date;
                
                $jobs = Job::whereDate('created_at', '>=',$start_date)
                                ->whereDate('created_at', '<=',$end_date)
                                ->where('status','active');
                $jobs_added = $jobs->count();
                $active_jobs_dropdown['jobs_dropdown'] = Job::where('status','active')
                                                        ->pluck('title','id');
                $jobs_closed = $jobs->whereDate('start_date', '>=',$start_date)
                                ->whereDate('end_date', '<=',$end_date)
                                ->where('status','inactive')->count();
    
                $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
                $all_applicants = JobApplication::whereDate('created_at','>=',$start_date)->whereDate('created_at','<=', $end_date);
                $applicants_count = $all_applicants->count();
                $sum = JobApplication::leftJoin('jobs','job_applications.job_id','jobs.id')
                        ->whereDate('job_applications.created_at','>=',$start_date)
                        ->whereDate('job_applications.created_at','<=', $end_date)
                        ->where('job_applications.eligibility', '>=','jobs.eligibility_percentage' )
                        ->select(DB::raw('SUM(job_applications.eligibility) as sum'));
                $sum = $sum->count() ?  $sum->first()->sum : 0;
                $qualified_avg = $applicants_count ? $sum/$applicants_count : 0;
                $qualified_avg = $qualified_avg ? number_format($qualified_avg, 1) : 0;
    
                $total_hired = JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id',$appStatuses['hired'])
                                    ->count();
    
                $total_applications = $all_applicants->count();
                $applicants = JobApplication::whereDate('created_at','>=',$start_date)
                                            ->whereDate('created_at','<=', $end_date)
                                            
                                            ->get();
                $total_unqualified = $applicants->filter(function ($value, $key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();
                $total_qualified    = $total_applications - $total_unqualified;
    
    
                $chart_qual_unqual = [
                    'qualified'    => $total_qualified,
                    'qual_per'    => $total_qualified > 0 ? number_format(($total_qualified / $total_applications) * 100, 2) : 0,
                    'unqualified' => $total_unqualified,
                    'unqual_per'  => $total_unqualified > 0 ? number_format(($total_unqualified / $total_applications) * 100, 2) : 0
                ]; 
    
                $all_applicants   = json_decode($all_applicants->get());
    
                /*Source Donat Chart*/
                $total = [
                    'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%facebook%')
                                ->count(), 
                    'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%google%')
                                ->count(),
                    'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%indeed%')
                                ->count(),
                    'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%linkedin%')
                                ->count(), 
                    'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(), 
                    'MyCNA' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%mycna%');
                                })
                                ->count(), 
                    'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')
                                ->count(),
                ];
    
    
                $chart_app_sources = [
                    'facebook_total' => $total['Facebook'],
                    'facebook_per'   => $total['Facebook'] > 0 ? number_format(($total['Facebook'] / $total_applications) * 100, 2)     : 0,
                    'google_total'   => $total['Google'],
                    'google_per'     => $total['Google']  > 0 ? number_format(($total['Google'] / $total_applications) * 100, 2) : 0,
                    'indeed_total'   => $total['Indeed'],
                    'indeed_per'     => $total['Indeed']  > 0 ? number_format(($total['Indeed'] / $total_applications) * 100, 2) : 0,
                    'linkedin_total'   => $total['Linkedin'],
                    'linkedin_per'     => $total['Linkedin']  > 0 ? number_format(($total['Linkedin'] / $total_applications) * 100, 2) : 0,
                    'mycna_total'   => $total['MyCNA'],
                    'mycna_per'     => $total['MyCNA']  > 0 ? number_format(($total['MyCNA'] / $total_applications) * 100, 2) : 0,
                    'others_total'   => $total['Others'],
                    'others_per'     => $total['Others']  > 0 ? number_format(($total['Others'] / $total_applications) * 100, 2) : 0,
                    'direct_total'   => $total['Direct'],
                    'direct_per'     => $total['Direct']  > 0 ? number_format(($total['Direct'] / $total_applications) * 100, 2) : 0
                ]; 

                $bar_chart_app_sources['labels'] = ['Facebook','Google','Indeed','Linkedin','MyCNA','Others','Direct'];
                $bar_chart_app_sources['colors'] = ['#2777f2','#ea4f35', '#073a9b', '#3178b5', '#4db1f8', '#00a65a', 'rgb(117 117 117)'];
                $bar_chart_app_sources['data'] = [
                    $total['Facebook'],
                    $total['Google'],
                    $total['Indeed'],
                    $total['Linkedin'],
                    $total['MyCNA'],
                    $total['Others'],
                    $total['Direct']
                ]; 
                
                $labels = ['Facebook', 'Widget', 'Indeed', 'Website'];
    
                $applicants_chart['labels'][0] = ['Facebook'];
                $applicants_chart['labels'][1] = ['Google'];
                $applicants_chart['labels'][2] = ['Indeed'];
                $applicants_chart['labels'][3] = ['Linkedin'];
                $applicants_chart['labels'][4] = ['MyCNA'];
                $applicants_chart['labels'][5] = ['Others'];
                $applicants_chart['labels'][6] = ['Direct'];
    
                $applicants_chart['data'][0] = $total['Facebook'];
                $applicants_chart['data'][1] = $total['Google'];
                $applicants_chart['data'][2] = $total['Indeed'];
                $applicants_chart['data'][3] = $total['Linkedin'];
                $applicants_chart['data'][4] = $total['MyCNA'];
                $applicants_chart['data'][5] = $total['Others'];
                $applicants_chart['data'][6] = $total['Direct'];
    
                /*End Source Donat Chart*/
    
                /*Qualified Unqualified BAR CHART*/
                // Sources
                $total = [
                    'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%facebook%')
                                
                                ->get(), 
                    'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%google%')
                                
                                ->get(),
                    'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%indeed%')
                                
                                ->get(),
                    'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('referer_url', 'like', '%linkedin%')
                                
                                ->get(), 
                    'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->get(), 
                    'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')
                                ->get(),
                ];
    
                $sources = [];
                $qualified_bar_chart = [];
                foreach($total as $key => $source){
                    $total = $source->count();
                    $unqualified = $source->filter(function ($value, $key)  {
                        return $value->eligibility  < $value->job->eligibility_percentage;
                    })->count();
                    $sources[] = [
                        'source' => $key, 
                        'applied' => $source->count(), 
                        'unqualified' => $unqualified, 
                        'qualified' => $source->count() - $unqualified
                    ];
                    $qualified_bar_chart['data']['qualified'][] = $source->count() - $unqualified;
                    $qualified_bar_chart['data']['unqualified'][] = $unqualified;
                }
    
                $qualified_bar_chart['labels'][0] = ['Facebook'];
                $qualified_bar_chart['labels'][1] = ['Google'];
                $qualified_bar_chart['labels'][2] = ['Indeed'];
                $qualified_bar_chart['labels'][3] = ['Linkedin'];
                $qualified_bar_chart['labels'][4] = ['Direct'];
                $qualified_bar_chart['labels'][5] = ['Others'];
    
    
                // Weekdays
                $weekdays = [
                    'Monday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Monday';
                                }), 
                    'Tuesday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Tuesday';
                                }), 
                    'Wednesday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Wednesday';
                                }), 
                    'Thursday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Thursday';
                                }), 
                    'Friday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Friday';
                                }), 
                    'Saturday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Saturday';
                                }), 
                    'Sunday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Sunday';
                                })
                ];
    
                $daywise = [];
                $qualified_bar_chart_daywise = [];
                $i = 0;
                foreach($weekdays as $key => $source){
                    $total = $source->count();
                    $unqualified = $source->filter(function ($value, $key)  {
                        return $value->eligibility  < $value->job->eligibility_percentage;
                    })->count();
                    $daywise[] = [
                        'day' => $key, 
                        'applied' => $source->count(), 
                        'unqualified' => $unqualified, 
                        'qualified' => $source->count() - $unqualified
                    ];
                    $qualified_bar_chart_daywise['data']['qualified'][] = $source->count() - $unqualified;
                    $qualified_bar_chart_daywise['data']['unqualified'][] = $unqualified;
                    $qualified_bar_chart_daywise['labels'][$i] = [$key];
                    $i++;
                }
    
                // foreach($labels as $key => $source){
                //     $qualified_bar_chart['labels'][$key] = $source;   
                //     $qualified_bar_chart['data']['qualified'][$key] = $this->totalQualified($start_date,$end_date,$source);
                //     $all = JobApplication::whereDate('created_at','>=',$start_date)
                //             ->whereDate('created_at','<=', $end_date)
                //             ->where('source',$source)
                //             ->count();
                //     $qualified_bar_chart['data']['unqualified'][$key] = $all-$this->totalQualified($start_date,$end_date,$source);
                // }            
    
                /*End Qualified Unqualified BAR CHART*/
    
                /*Source Status Wise Donat Chart*/
                $status_chart = [];
                $status_labels = ['Applied', 'Phone Screen', 'Interview', 'Hired','Rejected'];
                foreach($status_labels as $key => $status){
                    $status_id = isset($appStatuses[strtolower($status)]) ? $appStatuses[strtolower($status)] : 0;
                    $status_chart['data'][$key] = JobApplication::whereDate('created_at','>=',$start_date)
                        ->whereDate('created_at','<=', $end_date)
                        ->where('status_id', $status_id)
                        ->count();
                    $status_chart['labels'][$key] = ($key==2) ? $status.'ed' : $status;   
                }
                    $statuses = []; 
                    $total_applied     = JobApplication::whereDate('created_at','>=',$start_date)
                                        ->whereDate('created_at','<=', $end_date)
                                        ->where('status_id',$appStatuses['applied'])
                                        ->count();
                    $total_phonescreen = JobApplication::whereDate('created_at','>=',$start_date)
                                        ->whereDate('created_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['phone screen'])
                                        ->count();
                    $total_interviewed = JobApplication::whereDate('created_at','>=',$start_date)
                                        ->whereDate('created_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['interview'])
                                        ->count();
                    $total_rejected = JobApplication::whereDate('created_at','>=',$start_date)
                                        ->whereDate('created_at','<=', $end_date)
                                        ->where('status_id', $appStatuses['rejected'])
                                        ->count();
                    $total_hired = $total_hired;
                    // $total_offered = 0;
                    if($total_applications == 0){
                        $total_applicant_two = 1;
                    }else{
                        $total_applicant_two = $total_applications;
                    }
                    $chart_app_statuses = [
                        'applied_total' => $total_applied,
                        'applied_per'   => ($total_applied > 0) ? number_format(($total_applied / $total_applicant_two) * 100, 2) : 0,
                        'phonescreen_total'   => $total_phonescreen,
                        'phonescreen_per'     => $total_phonescreen  > 0 ? number_format(($total_phonescreen / $total_applicant_two) * 100, 2) : 0,
                        'interviewed_total'   => $total_interviewed,
                        'interviewed_per'     => $total_interviewed  > 0 ? number_format(($total_interviewed / $total_applicant_two) * 100, 2) : 0,
                        'hired_total'   => $total_hired,
                        'hired_per'     => $total_hired  > 0 ? number_format(($total_hired / $total_applicant_two) * 100, 2) : 0,
                        // 'offered_total'   => $total_offered,
                        // 'offered_per'     => $total_offered  > 0 ? number_format(($total_offered / $total_applicant_two) * 100, 2) : 0,
                        'rejected_total'   => $total_rejected,
                        'rejected_per'     => $total_rejected  > 0 ? number_format(($total_rejected / $total_applicant_two) * 100, 2) : 0,
                    ]; 
                $status_chart = $status_chart+$chart_app_statuses;
                /*End Source Status Wise Donat Chart*/
                /*Source Status Wise BAR CHART*/
                $statuswise_bar_chart = $status_chart;
                /*End Source Status Wise BAR CHART*/
    
                $stats = [
                    'jobs_added'          => $jobs_added,
                    'jobs_closed'         => $jobs_closed,
                    'total_applications'  => $total_applications,
                    'total_qualified'     => $total_qualified,
                    'total_unqualified'   => $total_unqualified,
                    'total_hired'         => $total_hired,
                    'total_rejected'      => $total_rejected,
                    'qualified_avg'       => $qualified_avg,
                    'applicants_count'    => $applicants_count,
                    'workflow_id'         => $workflow_id,
                    'stage_titles'              => $stages ?? 0,
                    // 'sum_of_all_applicants_eligibility' => $sum,
                    // 'average' => '$sum/$applicants_count = '.($sum/$applicants_count)
                ];
    
                return response()->json([
                    $applicants_chart, $chart_qual_unqual, $chart_app_sources,$qualified_bar_chart,$status_chart,$statuswise_bar_chart, $stats,$active_jobs_dropdown,$qualified_bar_chart_daywise, $bar_chart_app_sources
                ]);
            }
        
    }
    public function get_engyj_data(Request $request){
        $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            
            $workflow_id = \App\Job::find($request->job_id)->workflow_id;

            if($workflow_id != 0){
                $stage_titles = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('title');
            }
            
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            
            $jobs = Job::whereDate('created_at', '>=',$start_date)
                            ->whereDate('created_at', '<=',$end_date)
                            ->where('status','active');
            $jobs_added = $jobs->count();
            $active_jobs_dropdown['jobs_dropdown'] = Job::where('status','active')
                                                    ->pluck('title','id');
            $jobs_closed = $jobs->whereDate('start_date', '>=',$start_date)
                            ->whereDate('end_date', '<=',$end_date)
                            ->where('status','inactive')->count();

            $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
            $all_applicants = JobApplication::whereDate('created_at','>=',$start_date)->whereDate('created_at','<=', $end_date)->where('job_id', '=', $request->job_id);
            $applicants_count = $all_applicants->count();
            $sum = JobApplication::leftJoin('jobs','job_applications.job_id','jobs.id')
                    ->whereDate('job_applications.created_at','>=',$start_date)
                    ->whereDate('job_applications.created_at','<=', $end_date)
                    ->where('job_applications.eligibility', '>=','jobs.eligibility_percentage' )
                    ->select(DB::raw('SUM(job_applications.eligibility) as sum'));
            $sum = $sum->count() ?  $sum->first()->sum : 0;
            $qualified_avg = $applicants_count ? $sum/$applicants_count : 0;
            $qualified_avg = $qualified_avg ? number_format($qualified_avg, 1) : 0;

            // $total_hired = JobApplication::whereDate('updated_at','>=',$start_date)
            //                     ->whereDate('updated_at','<=', $end_date)
            //                     ->where('status_id',$appStatuses['hired'])
            //                     ->where('job_id', '=', $request->job_id)
            //                     ->count();
            $all_app = JobApplication::all();
            $all_app =  $all_app->filter(function ($item) use ($start_date, $end_date, $request) {
                return (data_get($item, 'updated_at') >= $start_date && data_get($item, 'updated_at') <= $end_date) && $item->job->id == $request->job_id ;
            });         

            $total_applications = $all_applicants->count();
            $applicants = JobApplication::whereDate('created_at','>=',$start_date)
                                        ->whereDate('created_at','<=', $end_date)
                                        ->where('job_id', '=', $request->job_id)
                                        ->get();
            $total_unqualified = $applicants->filter(function ($value, $key)  {
                return $value->eligibility  < $value->job->eligibility_percentage;
            })->count();
            $total_qualified    = $total_applications - $total_unqualified;


            $chart_qual_unqual = [
                'qualified'    => $total_qualified,
                'qual_per'    => $total_qualified > 0 ? number_format(($total_qualified / $total_applications) * 100, 2) : 0,
                'unqualified' => $total_unqualified,
                'unqual_per'  => $total_unqualified > 0 ? number_format(($total_unqualified / $total_applications) * 100, 2) : 0
            ]; 

            $all_applicants   = json_decode($all_applicants->get());

            /*Source Donat Chart*/
            $total = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)->count(),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)->count(),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)->count(),
            ];


            $chart_app_sources = [
                'facebook_total' => $total['Facebook'],
                'facebook_per'   => $total['Facebook'] > 0 ? number_format(($total['Facebook'] / $total_applications) * 100, 2)     : 0,
                'google_total'   => $total['Google'],
                'google_per'     => $total['Google']  > 0 ? number_format(($total['Google'] / $total_applications) * 100, 2) : 0,
                'indeed_total'   => $total['Indeed'],
                'indeed_per'     => $total['Indeed']  > 0 ? number_format(($total['Indeed'] / $total_applications) * 100, 2) : 0,
                'linkedin_total'   => $total['Linkedin'],
                'linkedin_per'     => $total['Linkedin']  > 0 ? number_format(($total['Linkedin'] / $total_applications) * 100, 2) : 0,
                'others_total'   => $total['Others'],
                'others_per'     => $total['Others']  > 0 ? number_format(($total['Others'] / $total_applications) * 100, 2) : 0,
                'direct_total'   => $total['Direct'],
                'direct_per'     => $total['Direct']  > 0 ? number_format(($total['Direct'] / $total_applications) * 100, 2) : 0
            ]; 
            
            $labels = ['Facebook', 'Widget', 'Indeed', 'Website'];

            $applicants_chart['labels'][0] = ['Facebook'];
            $applicants_chart['labels'][1] = ['Google'];
            $applicants_chart['labels'][2] = ['Indeed'];
            $applicants_chart['labels'][3] = ['Linkedin'];
            $applicants_chart['labels'][4] = ['Others'];
            $applicants_chart['labels'][5] = ['Direct'];

            $applicants_chart['data'][0] = $total['Facebook'];
            $applicants_chart['data'][1] = $total['Google'];
            $applicants_chart['data'][2] = $total['Indeed'];
            $applicants_chart['data'][3] = $total['Linkedin'];
            $applicants_chart['data'][4] = $total['Others'];
            $applicants_chart['data'][5] = $total['Direct'];

            /*End Source Donat Chart*/

            /*Qualified Unqualified BAR CHART*/
            // Sources
            $total = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->get(), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->get(),
            ];

            $sources = [];
            $qualified_bar_chart = [];
            $qualified_bar_chart_HRS = [];
            foreach($total as $key => $source){
                $total = $source->count();
                $unqualified = $source->filter(function ($value, $key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();
                
                $sources[] = [
                    'source' => $key, 
                    'applied' => $source->count(), 
                    'unqualified' => $unqualified, 
                    'qualified' => $source->count() - $unqualified
                ];
                $qualified_bar_chart['data']['qualified'][] = $source->count() - $unqualified;
                $qualified_bar_chart['data']['unqualified'][] = $unqualified;
            }

            $qualified_bar_chart['labels'][0] = ['Facebook'];
            $qualified_bar_chart['labels'][1] = ['Google'];
            $qualified_bar_chart['labels'][2] = ['Indeed'];
            $qualified_bar_chart['labels'][3] = ['Linkedin'];
            $qualified_bar_chart['labels'][4] = ['Direct'];
            $qualified_bar_chart['labels'][5] = ['Others'];
            $total_hire = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
            ];
            $total_rejected = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
            ];
            foreach(['Facebook', 'Google', 'Indeed', 'Linkedin', 'Direct', 'Others'] as $key){
                $qualified_bar_chart_HRS['data']['hired'][] = $total_hire[$key]->count();
                $qualified_bar_chart_HRS['data']['rejected'][] = $total_rejected[$key]->count(); 
            }


            // Weekdays
            $weekdays = [
                'Monday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Monday';
                            }), 
                'Tuesday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Tuesday';
                            }), 
                'Wednesday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Wednesday';
                            }), 
                'Thursday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Thursday';
                            }), 
                'Friday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Friday';
                            }), 
                'Saturday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Saturday';
                            }), 
                'Sunday' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                return date('l', strtotime($value->created_at)) == 'Sunday';
                            })
            ];

            $daywise = [];
            $qualified_bar_chart_daywise = [];
            $qualified_bar_chart_daywise_HRS = [];
            $i = 0;
            foreach($weekdays as $key => $source){
                $total = $source->count();
                $unqualified = $source->filter(function ($value) use($key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();

                $hired = JobApplication::whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=', $end_date)
                ->where('job_id', '=', $request->job_id)
                ->get()
                ->filter(function ($value) use($key){
                    if($value->lastStatus('hired')->updated_at)
                    return date('l', strtotime($value->lastStatus('hired')->updated_at)) == $key;

                })->count();
                $rejected = JobApplication::whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=', $end_date)
                ->where('job_id', '=', $request->job_id)
                ->get()
                ->filter(function ($value) use($key){
                    if($value->lastStatus('rejected')->updated_at)
                    return date('l', strtotime($value->lastStatus('rejected')->updated_at)) == $key;
                })->count();
                $qualified_bar_chart_daywise_HRS['data']['hired'][] = $hired ?? 0;
                $qualified_bar_chart_daywise_HRS['data']['rejected'][] = $rejected ?? 0;
                $daywise[] = [
                    'day' => $key, 
                    'applied' => $source->count(), 
                    'unqualified' => $unqualified, 
                    'qualified' => $source->count() - $unqualified
                ];
                $qualified_bar_chart_daywise['data']['qualified'][] = $source->count() - $unqualified;
                $qualified_bar_chart_daywise['data']['unqualified'][] = $unqualified;
                $qualified_bar_chart_daywise['labels'][$i] = [$key];
                $i++;
            }
            /*End Qualified Unqualified BAR CHART*/

            /*Source Status Wise Donat Chart*/
            $status_chart = [];
            $status_labels = ['Applied', 'Phone Screen', 'Interview', 'Hired','Rejected'];
            foreach($status_labels as $key => $status){
                $status_id = isset($appStatuses[strtolower($status)]) ? $appStatuses[strtolower($status)] : 0;
                $status_chart['data'][$key] = JobApplication::whereDate('created_at','>=',$start_date)
                    ->whereDate('created_at','<=', $end_date)
                    ->where('status_id', $status_id)
                    ->count();
                $status_chart['labels'][$key] = ($key==2) ? $status.'ed' : $status;   
            }
                $statuses = []; 
                $total_applied     = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where('status_id',$appStatuses['applied'])
                                    ->count();
                $total_phonescreen = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['phone screen'])
                                    ->count();
                $total_interviewed = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['interview'])
                                    ->count();
                $total_rejected = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['rejected'])
                                    ->count();
                
                $total_rejected = $all_app->filter(function ($value, $key) use($appStatuses, $request)  {
                    if($value->job->id == $request->job_id){
                        if($value->job->workflow_id==0){
                            return $value->status_id  == $appStatuses['rejected'];
                        }else{
                            return $value->hiring_status  == 'rejected';
                        }
                    }
                })->count();
                $total_hired = $all_app->filter(function ($value, $key) use($appStatuses, $request) {
                    if($value->job->id == $request->job_id){
                        if($value->job->workflow_id==0){
                            return $value->status_id  == $appStatuses['hired'];
                        }else{
                            
                            return $value->hiring_status  == 'hired';
                        }
                    }
                })->count();
                // $total_offered = 0;
                if($total_applications == 0){
                    $total_applicant_two = 1;
                }else{
                    $total_applicant_two = $total_applications;
                }
                $chart_app_statuses = [
                    'applied_total' => $total_applied,
                    'applied_per'   => ($total_applied > 0) ? number_format(($total_applied / $total_applicant_two) * 100, 2) : 0,
                    'phonescreen_total'   => $total_phonescreen,
                    'phonescreen_per'     => $total_phonescreen  > 0 ? number_format(($total_phonescreen / $total_applicant_two) * 100, 2) : 0,
                    'interviewed_total'   => $total_interviewed,
                    'interviewed_per'     => $total_interviewed  > 0 ? number_format(($total_interviewed / $total_applicant_two) * 100, 2) : 0,
                    'hired_total'   => $total_hired,
                    'hired_per'     => $total_hired  > 0 ? number_format(($total_hired / $total_applicant_two) * 100, 2) : 0,
                    // 'offered_total'   => $total_offered,
                    // 'offered_per'     => $total_offered  > 0 ? number_format(($total_offered / $total_applicant_two) * 100, 2) : 0,
                    'rejected_total'   => $total_rejected,
                    'rejected_per'     => $total_rejected  > 0 ? number_format(($total_rejected / $total_applicant_two) * 100, 2) : 0,
                ]; 
            $status_chart = $status_chart+$chart_app_statuses;
            /*End Source Status Wise Donat Chart*/
            /*Source Status Wise BAR CHART*/
            $statuswise_bar_chart = $status_chart;
            /*End Source Status Wise BAR CHART*/

            $stats = [
                'jobs_added'          => $jobs_added,
                'jobs_closed'         => $jobs_closed,
                'total_applications'  => $total_applications,
                'total_qualified'     => $total_qualified,
                'total_unqualified'   => $total_unqualified,
                'total_hired'         => $total_hired,
                'total_rejected'      => $total_rejected,
                'qualified_avg'       => $qualified_avg,
                'applicants_count'    => $applicants_count,
                'workflow_id'         => $workflow_id,
                'total-unqualified-percentage'    => '(' . ($total_unqualified != 0 ? number_format((float)($total_unqualified  /  $total_applications * 100) , 2, '.', '') : "0.00" ) . '%)',
                'total-qualified-percentage'    => '(' . ($total_applications != 0 ? number_format((float)($total_qualified /  $total_applications * 100) , 2, '.', '') : "0.00" ) . '%)',
                'total-rejected-percentage'    => '(' . ($total_applications != 0 ? number_format((float)($total_rejected /  $total_applications * 100) , 2, '.', '') : "0.00" ) . '%)',
                'total-hired-percentage'    => '(' . ($total_applications != 0 ? number_format((float)($total_hired /  $total_applications * 100) , 2, '.', '') : "0.00" ) . '%)',
                'total-interview-percentage'    => '(' . ($total_applications != 0 ? number_format((float)($total_interviewed /  $total_applications * 100) , 2, '.', '') : "0.00" ) . '%)',
                'stage_titles'              => $stages ?? 0,
                // 'sum_of_all_applicants_eligibility' => $sum,
                // 'average' => '$sum/$applicants_count = '.($sum/$applicants_count)
            ];

            $interviews = JobApplication::whereDate('created_at','>=',$start_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('job_id', '=', $request->job_id)->get();

            $pass = 0;
            $fail = 0;
            $cancel = 0;
            foreach($interviews as $apps){
                $interview = \App\InterviewSchedule::where('job_application_id', '=', $apps->id)->get();
                foreach($interview as $inter){
                    if($inter->status == 'pass'){
                        $pass++;
                    }
                    if($inter->status == 'fail'){
                        $fail++;
                    }
                    if($inter->status == 'canceled'){
                        $cancel++;
                    }
                }
            }

            
            return response()->json([
                $applicants_chart, 
                $chart_qual_unqual, 
                $chart_app_sources,
                $qualified_bar_chart,
                $status_chart,
                $statuswise_bar_chart, 
                $stats,
                $active_jobs_dropdown,
                $qualified_bar_chart_daywise,
                $qualified_bar_chart_HRS,
                $qualified_bar_chart_daywise_HRS,
                [$pass, $fail, $cancel]

            ]);
        }
    }
    public function qualifiedData(Request $request){
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            // Daywise 
            if($request->has('daywise') && $request->daywise){
                // Weekdays
                $weekdays = [
                    'Monday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Monday';
                                }), 
                    'Tuesday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Tuesday';
                                }), 
                    'Wednesday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Wednesday';
                                }), 
                    'Thursday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Thursday';
                                }), 
                    'Friday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Friday';
                                }), 
                    'Saturday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Saturday';
                                }), 
                    'Sunday' => JobApplication::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('job_id', '=', $request->job_id)->get()->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Sunday';
                                })
                ];
                $daywise = [];
                $qualified_bar_chart_daywise = [];
                $i = 0;
                foreach($weekdays as $key => $source){
                    $total = $source->count();
                    $unqualified = $source->filter(function ($value, $key)  {
                        return $value->eligibility  < $value->job->eligibility_percentage;
                    })->count();
                    $daywise[] = [
                        'day' => $key, 
                        'applied' => $source->count(), 
                        'unqualified' => $unqualified, 
                        'qualified' => $source->count() - $unqualified
                    ];
                }
                return DataTables::of($daywise)
                ->make(true);
            }
        
            $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
            $interviewed_app_ids = InterviewSchedule::where('status','pending')->pluck('job_application_id');
            $total = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get(), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })->get(), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)
                            ->get(),
            ];
            $sources = [];
            foreach($total as $key => $source){
                $total = $source->count();
                $unqualified = $source->filter(function ($value, $key)  {
                    return $value->eligibility  < $value->job->eligibility_percentage;
                })->count();
                $sources[] = [
                    'source' => $key, 
                    'applied' => $source->count(), 
                    'qualified' => $source->count() - $unqualified,
                    'unqualified' => $unqualified
                ];  
            }
            return DataTables::of($sources)
                ->make(true);
        }
    }


    public function HRSData(Request $request){
        $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            $all_applications = JobApplication::whereDate('created_at','>=',$start_date)
            ->whereDate('created_at','<=', $end_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('job_id', '=', $request->job_id)->get();

            // Daywise 
            if($request->has('daywise') && $request->daywise){
                // Weekdays
                $weekdays = [
                    'Monday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Monday';
                                }), 
                    'Tuesday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Tuesday';
                                }), 
                    'Wednesday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Wednesday';
                                }), 
                    'Thursday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Thursday';
                                }), 
                    'Friday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Friday';
                                }), 
                    'Saturday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Saturday';
                                }), 
                    'Sunday' => $all_applications->filter(function ($value, $key) {
                                    return date('l', strtotime($value->created_at)) == 'Sunday';
                                })
                ];
                $daywise = [];
                $qualified_bar_chart_daywise = [];
                $i = 0;
                foreach($weekdays as $key => $source){

                $hired = JobApplication::whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=', $end_date)
                ->where('job_id', '=', $request->job_id)
                ->get()
                ->filter(function ($value) use($key){
                    if($value->lastStatus('hired')->updated_at)
                    return date('l', strtotime($value->lastStatus('hired')->updated_at)) == $key;

                })->count();
                $rejected = JobApplication::whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=', $end_date)
                ->where('job_id', '=', $request->job_id)
                ->get()
                ->filter(function ($value) use($key){
                    if($value->lastStatus('rejected')->updated_at)
                    return date('l', strtotime($value->lastStatus('rejected')->updated_at)) == $key;
                })->count();
                    $total = $source->count();
                    $daywise[] = [
                        'day' => $key, 
                        'applied' => $source->count(), 
                        'hired' => $hired ?? 0, 
                        'rejected' => $rejected ?? 0
                    ];
                }
                return DataTables::of($daywise)
                ->make(true);
            }
        
            
            $interviewed_app_ids = InterviewSchedule::where('status','pending')->pluck('job_application_id');
            $total_hire = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                
                                if($value->lastStatus('hired')->id)
                                return $value;
                            }),
            ];
            $total_rejected = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $request->job_id)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)
                            ->get()
                            ->filter(function ($value){
                                if($value->lastStatus('rejected')->id)
                                return $value;
                            }),
            ];
            $sources = [];
            foreach(['Facebook', 'Google', 'Indeed', 'Linkedin', 'Direct', 'Others'] as $key){
                $sources[] = [
                    'source' => $key, 
                    'hired' => $total_hire[$key]->count(),
                    'rejected' => $total_rejected[$key]->count()
                ];  
            }
            return DataTables::of($sources)
                ->make(true);
        }
    }




    public function sourcesData(Request $request)
    {
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
            $interviewed_app_ids = InterviewSchedule::where('status','pending')->pluck('job_application_id');
            $total = [
                'Facebook' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%facebook%')
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Google' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%google%')
                            ->where('job_id', '=', $request->job_id)->count(),
                'Indeed' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%indeed%')
                            ->where('job_id', '=', $request->job_id)->count(),
                'Linkedin' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'like', '%linkedin%')
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Direct' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where(function ($query){
                                $query->where('referer_url', 'like', '%recruit%')
                                ->orWhereNull('referer_url');
                            })
                            ->where('job_id', '=', $request->job_id)->count(), 
                'Others' => JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('referer_url', 'not like', '%recruit%')
                            ->where('referer_url', 'not like', '%linkedin%')
                            ->where('referer_url', 'not like', '%indeed%')
                            ->where('referer_url', 'not like', '%google%')
                            ->where('referer_url', 'not like', '%facebook%')
                            ->WhereNotNull('referer_url')
                            ->where('job_id', '=', $request->job_id)->count(),
            ];
            $sources = [];
            foreach($total as $key => $source){
                $sources[] = [
                    'source' => $key, 
                    'applied' => $source, 
                    'unqualified' => $this->totalQualified($start_date,$end_date,strToLower($key),true), 
                    'qualified' => $this->totalQualified($start_date,$end_date,strToLower($key))
                ];  
            }
            return DataTables::of($sources)
                ->make(true);
        }
    }

    public function statusData(Request $request)
    {
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            $appStatuses = ApplicationStatus::get()->pluck('id', 'status');
            $interviewed_app_ids = InterviewSchedule::where('status','pending')->pluck('job_application_id');
        $sources = [];
        foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
            if($key != 'Others' &&  $key != 'Direct'){
                $sources[] = [
                    'source' => $key,
                    'applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['applied'])
                                ->where('referer_url', 'like', '%'. $keywords.'%')->count(),
                    'phone_screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['phone screen'])
                                    ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                    'interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                    ->whereDate('updated_at','<=', $end_date)
                                    ->where('status_id', $appStatuses['interview'])
                                    ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                    'hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['hired'])
                                ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                    'rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['rejected'])
                                ->where('referer_url', 'like', '%'. $keywords .'%')->count(),
                ];
            }else if($key == 'Others'){
                $sources[] = [
                    'source' => $key,
                    'applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['applied'])
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')->count(),
                    'phone_screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['phone screen'])
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')->count(),
                    'interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['interview'])
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')->count(),
                    'hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['hired'])
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')->count(),
                    'rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['rejected'])
                                ->where('referer_url', 'not like', '%recruit%')
                                ->where('referer_url', 'not like', '%linkedin%')
                                ->where('referer_url', 'not like', '%indeed%')
                                ->where('referer_url', 'not like', '%google%')
                                ->where('referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('referer_url')->count(),
                ];
            }else if($key == 'Direct'){
                $sources[] = [
                    'source' => $key,
                    'applied'=> JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['applied'])
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(),
                    'phone_screen' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['phone screen'])
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(),
                    'interviewed' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['interview'])
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(),
                    'hired' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['hired'])
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(),
                    'rejected' => JobApplication::whereDate('updated_at','>=',$start_date)
                                ->whereDate('updated_at','<=', $end_date)
                                ->where('status_id', $appStatuses['rejected'])
                                ->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                })
                                ->count(),
                ];
            }         
        }

            return DataTables::of($sources)
                ->make(true);
        }
    }
    public function getRejectedData(Request $request){
        
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $status = ApplicationStatus::get()->pluck('id', 'status');

        $all_rej = ApplicantHistory::whereDate('updated_at','>=',$start_date)
                ->whereDate('updated_at','<=', $end_date)
                ->where('current_value', '=', ($status['rejected'] ?? 0))
                ->get();
        
        $applicants = JobApplication::whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=', $end_date)->get();
        $interview = $all_rej->filter(function ($value, $key) use($status)  {
            return $value->previous_value  == ($status['interview'] ?? 0);
        })->count();
        // UnRejected
        $ur_interview = ApplicantHistory::whereDate('created_at','>=',$start_date)
                        ->whereDate('created_at','<=', $end_date)
                        ->where('current_value', ($status['interview'] ?? 0))
                        ->pluck('applicant_id','id')
                        ->toArray();
        $ur_interview = count(array_unique($ur_interview));
        
        $phone_screen = $all_rej->filter(function ($value, $key) use($status)  {
            return $value->previous_value  == ($status['phone screen'] ?? 0);
        })->count();
        // UnRejected
        $ur_phone_screen = ApplicantHistory::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('current_value', ($status['phone screen'] ?? 0))
                            ->pluck('applicant_id','id')
                            ->toArray();
        $ur_phone_screen = count(array_unique($ur_phone_screen));
    
        $applied = $all_rej->filter(function ($value, $key) use($status)  {
            return $value->previous_value  == ($status['applied'] ?? 0);
        })->count();

        $background_check = $all_rej->filter(function ($value, $key) use($status)  {
            return $value->previous_value  == ($status['Background Check'] ?? 0);
        })->count();
        // UnRejected
        $ur_background_check = ApplicantHistory::whereDate('created_at','>=',$start_date)
                                ->whereDate('created_at','<=', $end_date)
                                ->where('current_value', ($status['Background Check'] ?? 0))
                                ->pluck('applicant_id','id')
                                ->toArray();
        $ur_background_check = count(array_unique($ur_background_check));
    
        $total_applicants =  $applicants->count();

        $unqualified = $applicants->filter(function ($value, $key) use($status)  {
            return $value->eligibility  < $value->job->eligibility_percentage;
        })->count();

        $total = $total_applicants;
        $unqualified_percentage = $total ? ($unqualified *100/$total) : 0;
        $phone_screen_percentage = $ur_phone_screen ? ($phone_screen *100/$ur_phone_screen) : 0;
        $interview_percentage = $ur_interview ? ($interview *100/$ur_interview) : 0;
        $background_check_percentage = $ur_background_check ? ($background_check *100/$ur_background_check) : 0;
        
        /* UnRejected Percentages */
        $qualified_percentage = $total ? (($total-$unqualified) *100/$total) : 0;
        $ur_phone_screen_percentage = $total ? ($ur_phone_screen *100/$total) : 0;
        $ur_interview_percentage = $total ? ($ur_interview *100/$total) : 0;
        $ur_background_check_percentage = $total ? ($ur_background_check *100/$total) : 0;
        /* End UnRejected Percentages */
        return [
            ['total' =>  $total, 'key' => 'applied', 'value' => $total, 'key2' => 'applied', 'value2' => $total],
            ['total' =>  $total, 'key' => 'unqualified', 'value'  => $unqualified, 'percentage'  => number_format($unqualified_percentage,2), 'key2' => 'qualified', 'value2' => ($total-$unqualified), 'percentage2'  => number_format($qualified_percentage,2)],
            ['total' =>  $total, 'key' => 'phone_screen', 'value'  => $phone_screen, 'percentage'  => number_format($phone_screen_percentage,2), 'key2' => 'phone_screen', 'value2' => $ur_phone_screen, 'percentage2'  => number_format($ur_phone_screen_percentage,2)],
            ['total' =>  $total, 'key' => 'interview', 'value'  => $interview, 'percentage'  => number_format($interview_percentage,2), 'key2' => 'interview', 'value2' => $ur_interview, 'percentage2'  => number_format($ur_interview_percentage,2)],
            ['total' =>  $total, 'key' => 'background_check', 'value'  => $background_check, 'percentage'  => number_format($background_check_percentage,2), 'key2' => 'background_check', 'value2' => $ur_background_check, 'percentage2'  => number_format($ur_background_check_percentage,2)]
        ];
    }
    public function totalQualified($start_date,$end_date,$source=null,$need_unqualified=null)
    {
        $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
        $applications = JobApplication::leftJoin('jobs','job_applications.job_id','jobs.id')
                        ->whereDate('job_applications.created_at','>=',$start_date)
                        ->whereDate('job_applications.created_at','<=', $end_date);
        if($need_unqualified){
            $applications = $applications->where('job_applications.eligibility','<=','jobs.eligibility_percentage');
        }
        else{
            $applications = $applications->where('job_applications.eligibility','>','jobs.eligibility_percentage');
        }
        if($source){
            if($source=='direct'){ 
                $applications = $applications->where(function ($query){
                                    $query->where('referer_url', 'like', '%recruit%')
                                    ->orWhereNull('referer_url');
                                });
             }
             elseif($source=='others'){
                $applications = $applications
                                ->where('job_applications.referer_url', 'not like', '%recruit%')
                                ->where('job_applications.referer_url', 'not like', '%linkedin%')
                                ->where('job_applications.referer_url', 'not like', '%indeed%')
                                ->where('job_applications.referer_url', 'not like', '%google%')
                                ->where('job_applications.referer_url', 'not like', '%facebook%')
                                ->WhereNotNull('job_applications.referer_url');
            }
            else{
                $applications = $applications->where('job_applications.referer_url', 'LIKE', '%'.$source.'%');
            }
        }
        return $applications->count();
    }

    public function budgetTable(Request $request)
    {
        if(($request->has('start_date') && $request->start_date) && ($request->has('end_date') && $request->end_date)){
            $appStatuses    = ApplicationStatus::get()->pluck('id', 'status');
            $start_date = $request->start_date;
            $end_date   = $request->end_date;
            $earlier = new \DateTime($request->start_date);
            $later = new \DateTime($request->end_date);
            $diff = $later->diff($earlier)->format("%a");
            $diff = $diff == 0 ? 1 : $diff;
            $data = [];
            
            /*get no of days between dates*/
            $from = Carbon::parse($start_date);
            $end = Carbon::parse($end_date);
            $get_no_days = $from->diffInDays($end);
            /*end get no of days between dates*/
            
            $job = Job::find($request->job_id);
            $budget = $job->budget ?? new Budget;
            $facebook = $budget->facebook ?? 0.00;
            $indeed = $budget->indeed ?? 0.00;
            $google = $budget->google ?? 0.00;
            $linkedin = $budget->linkedin ?? 0.00;
            $direct = $budget->direct ?? 0.00;
            $totals = 0.00;
            $id = $request->job_id;
            $total_other = JobApplication::whereDate('created_at','>=',$start_date)
            ->whereDate('created_at','<=', $end_date)
                        ->where('job_id', '=', $id)
                        ->where('referer_url', 'not like', '%recruit%')
                        ->where('referer_url', 'not like', '%linkedin%')
                        ->where('referer_url', 'not like', '%indeed%')
                        ->where('referer_url', 'not like', '%google%')
                        ->where('referer_url', 'not like', '%facebook%')
                        ->WhereNotNull('referer_url')->count();
            $total_direct = JobApplication::whereDate('created_at','>=',$start_date)
                            ->whereDate('created_at','<=', $end_date)
                            ->where('job_id', '=', $id)
                            ->where(function ($query)USE($id){
                                $query->where('referer_url', 'like', '%recruit%');
                                $query->orWhereNull('referer_url');
                            })
                            ->count();
            $total_direct = $total_direct + $total_other;
            $total_hired_others = $total_hired = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where(function ($query){
                                        $query->where('referer_url', 'like', '%recruit%')
                                        ->orWhereNull('referer_url');
                                    })
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('status_id', $appStatuses['hired'])->count();
            $total_hired_direct = $total_hired = JobApplication::whereDate('created_at','>=',$start_date)
                                    ->whereDate('created_at','<=', $end_date)
                                    ->where('job_id', '=', $id)
                                    ->where('referer_url', 'not like', '%recruit%')
                                    ->where('referer_url', 'not like', '%linkedin%')
                                    ->where('referer_url', 'not like', '%indeed%')
                                    ->where('referer_url', 'not like', '%google%')
                                    ->where('referer_url', 'not like', '%facebook%')
                                    ->WhereNotNull('referer_url')
                                    ->where('job_id', '=', $request->job_id)
                                    ->where('status_id', $appStatuses['hired'])->count();
            $total_hired_direct = $total_hired_direct + $total_hired_others;
            $dig = 2;
            $t_budget = 0.00;
            $t_applicants = 0;
            $t_hired = 0.00;
            foreach(['facebook',  'indeed', 'google', 'linkedin', 'direct'] as $kwd){
                $total_hired = JobApplication::whereDate('created_at','>=',$start_date)
                                                ->whereDate('created_at','<=', $end_date)
                                                ->where('referer_url', 'like','%'.$kwd.'%')
                                                ->where('job_id', '=', $request->job_id)
                                                ->where('status_id', $appStatuses['hired'])->count();
                $total = JobApplication::whereDate('created_at','>=',$start_date)
                        ->whereDate('created_at','<=', $end_date)
                        ->where('job_id', '=', $request->job_id)
                        ->where('referer_url', 'like', '%'. $kwd .'%')->count(); 
                $total_app = $kwd == 'direct' ? $total_direct : $total;
                $total_budget = $diff * $$kwd;            
                $total_hired = $kwd == 'direct' ? $total_hired_direct : $total_hired;
                $data[$kwd]['source'] = $kwd == 'direct' ? "Direct/Others" : ucFirst($kwd);
                $data[$kwd]['total_days'] = number_format((float)$total_budget, $dig, '.', '') ;
                $data[$kwd]['monthly_budget'] = number_format((float)$$kwd, $dig, '.', '');
                $data[$kwd]['total_applicants'] =  $total_app;
                $data[$kwd]['cost_as_per_applicant'] = $total_app != 0 ? number_format((float)$total_budget/$total_app, $dig, '.', '') : '-';
                $data[$kwd]['total_hred'] = $total_hired;
                $data[$kwd]['cost_as_per_hired'] = $total_hired != 0 ? number_format((float)$total_budget/$total_hired, $dig, '.', '') : '-';
                $t_budget += $data[$kwd]['monthly_budget'];
                $t_applicants += $data[$kwd]['total_applicants'];
                $t_hired += $data[$kwd]['total_hred'];
            }

            $data['z']['source'] = "Totals";
            $data['z']['total_days'] = number_format((float)$diff * $t_budget, $dig, '.', '');
            $data['z']['monthly_budget'] = number_format((float)$t_budget, $dig, '.', '');
            $data['z']['total_applicants'] =  $t_applicants;
            $data['z']['cost_as_per_applicant'] = $t_applicants != 0 ? number_format((float)($diff * $t_budget)/$t_applicants, $dig, '.', '') : number_format((float)0, $dig, '.', '');
            $data['z']['total_hred'] = $t_hired;
            $data['z']['cost_as_per_hired'] = $t_hired != 0 ?  number_format((float)($diff * $t_budget)/$t_hired, $dig, '.', '') : number_format((float)0, $dig, '.', '');
            return DataTables::of($data)
                ->make(true);
        }
    }
}