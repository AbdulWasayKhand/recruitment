<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\SmsLog;
use App\User;
use App\Helper\TwilioHelper as Twilio;
use App\Option;
use App\JobApplication;

class AdminSmsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'icon-speedometer';
        $this->pageTitle = 'menu.smsMessages';
    }

    public function index(Request $request)
    {
        $from = Option::getOption('twilio_from_number', $this->user->company_id);
        $twilio_number = $from ?? config('twilio-notification-channel.from');
        $this->messages = Twilio::set(NULL, NULL, $twilio_number)
            ->read();

        $to_numbers = $this->messages->pluck('to');
        // $applicants = JobApplication::select('id', 'full_name', 'phone')
        //     ->with('sms_logs')
        //     ->where('company_id', $this->user->company_id)
        //     ->whereIn('phone', $to_numbers)->orderBy('id', 'DESC')->get();
        $this->twilio_number = $twilio_number;
        $this->call_forward_number = Option::getOption('call_forward_number', $this->user->company_id);
        $this->applicants = JobApplication::orderBy('id','desc')->get();
        return view('admin.messages.index', $this->data);
    }

    public function getMessages(Request $request)
    {
        $applicant_id = $request->applicant_id;
        $applicant = JobApplication::with('job:id,title,eligibility_percentage')
                                ->select('id', 'job_id', 'company_id', 'full_name', 'email', 'phone', 'eligibility', 'eligibility_percentage', 'source', 'created_at', 'referer_url')
                                ->find($applicant_id);
        
        $this->applicant = $applicant;
        $this->messages = SmsLog::where('number', 'like', '%'.$applicant->phone.'%')->where('company_id', '=', $applicant->company_id)->where('status', '!=', 'pending')->get();
        return [
            'status' => 'success',
            'data' => view('admin.messages.thread-single', $this->data)->render(),
        ];
    }
    
    public function sendMessage(Request $request)
    {
        // test
        $applicant_id = $request->applicant_id;
        $applicant = JobApplication::find($applicant_id);
        $phone = $request->phone;
        $message = $request->message;
        $from = Option::getOption('twilio_from_number', $applicant->company_id);
        $twilio_number = $from ?? config('twilio-notification-channel.from');

        // $response = Twilio::set($phone, $message,$twilio_number)->send();
        $log = new \App\SmsLog();
        $log->company_id = $applicant->company_id;
        $log->message_type = 'outbound';
        $log->type = 'normal';
        $log->number = str_replace(' ', '',  $applicant->phone);
        $log->message = $message;
        $log->status = 'pending';
        $log->send_at = date('Y-m-d h:i:s');
        $log->reference_name = 'applicant-alert';
        $log->reference_id = $applicant->id;
        $log->save();
        return [
            'status' => 'success',
            'data' => [
                'time' => now($this->global->timezone)->format('h:i')
            ],
        ];
    }

    public function saveCallForwardNumber(Request $request)
    {
        if($request->has('call_forward_number') && $request->call_forward_number){
            $option = Option::where([
                'option_name'=>'call_forward_number',
                'company_id' =>$this->user->company_id
            ]);
            $option = $option->count() ? $option->first() : new Option;
            $option->option_name = 'call_forward_number';
            $option->company_id = $this->user->company_id;
            $option->user_id = $this->user->id;
            $option->option_value = $request->call_forward_number;
            $option->save();
            return $option;
        }
    }
}
