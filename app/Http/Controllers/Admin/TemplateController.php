<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\EmailTemplate;
use App\SmsTemplate;

class TemplateController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Template';
        $this->pageIcon = 'icon-grid';
    }

    public function index(){
        $this->all_sms = SmsTemplate::orderBy('name')->get();
        $this->all_emails = EmailTemplate::orderBy('name')->get();
        return view('admin.template.index', $this->data);
    }

    public function saveEmail(Request $request){
        $email = new EmailTemplate();
        $email->name = $request->title;
        $email->company_id = $request->cid;
        $email->content = $request->content;
        $email->subject = $request->subject;
        $email->save();
        return $request;
    }

    public function saveSms(Request $request){
        $sms = new SmsTemplate();
        $sms->name = $request->title;
        $sms->company_id = $request->cid;
        $sms->content = $request->content;
        $sms->save();
        return $request;
    }

    public function deleteEmail(Request $request){
        $email = EmailTemplate::find($request->id);
        $email->delete();
        return $request;
    }

    public function deleteSms(Request $request){
        $sms = SmsTemplate::find($request->id);
        $sms->delete();
        return $request;
    }

    public function editEmail(Request $request){
        $email = EmailTemplate::find($request->tid);
        $email->name = $request->title;
        $email->company_id = $request->cid;
        $email->content = $request->content;
        $email->subject = $request->subject;
        $email->save();
        return $request;
    }

    public function editSms(Request $request){
        $sms = SmsTemplate::find($request->tid);
        $sms->name = $request->title;
        $sms->company_id = $request->cid;
        $sms->content = $request->content;
        $sms->save();
        return $request;
    }
}
