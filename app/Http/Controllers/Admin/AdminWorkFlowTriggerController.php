<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helper\Reply;
use Yajra\DataTables\Facades\DataTables;
use App\WorkflowTrigger;
use App\WorkflowTriggerCondition;

class AdminWorkFlowTriggerController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.triggers';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->workflow_triggers = WorkflowTrigger::where('company_id','=', $this->user->company_id)->get();
        return view('admin.workflow.triggers.index', $this->data);
    }

    /**
     * Create form of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->workflow_trigger = new WorkflowTrigger;
        return view('admin.workflow.triggers.create', $this->data);
    }

    public function edit($id)
    {
        $this->id = $id;
        $this->workflow_trigger = WorkflowTrigger::find($id);
        $this->conditions = $this->workflow_trigger->conditions;
        return view('admin.workflow.triggers.edit', $this->data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        // Save Trigger
        $trigger = WorkflowTrigger::findOrNew($request->id);
        $trigger->fill($data);
        $trigger->company_id = $this->user->company_id;
        $trigger->save();
        // Save Trigger Conditions
        if($request->has('conditions') && count($request->conditions)){
            foreach ($request->conditions as $condition) {
                $trigger_condition = WorkflowTriggerCondition::findOrNew($condition['id']);
                $trigger_condition->fill($condition);
                $trigger_condition->workflow_trigger_id = $trigger->id;
                $trigger_condition->save();
            }
        }
        $msg = $request->has('id') ? __('modules.workflow.trigger.updatedMsg') : __('modules.workflow.trigger.createdMsg');
        return Reply::success($msg)+['trigger'=>$trigger];
    }

    public function destroy($id)
    {
        $trigger = WorkflowTrigger::find($id);
        $trigger->conditions()->delete();
        $trigger->destroy($id);
        return Reply::success(__('modules.workflow.trigger.deletedMsg'));
    }

    public function deleteCondition(Request $request)
    {
        $cond = WorkflowTriggerCondition::find($request->id);
        $cond->delete();
        return Reply::success(__('modules.workflow.trigger.deletedMsg'));
    }
}
