<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Holiday;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use \Carbon\Carbon;

class AdminHolidayController extends AdminBaseController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle ='menu.holidays';
        $this->pageIcon = 'icon-grid';
    }
    
    
    public function edit($id)
    {
        $this->holiday = Holiday::find($id);
        return view('admin.holidays.edit', $this->data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.holidays.index', $this->data);
    }
    
    public function show($id)
    {
        $this->holiday = Holiday::find($id);
        return view('admin.holidays.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->questions = Holiday::all();
        return view('admin.holidays.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from  = Carbon::createFromFormat('m-d-Y',$request->from)->timezone($this->user->company->timezone)->format('Y-m-d');
        $to = Carbon::createFromFormat('m-d-Y',$request->to)->timezone($this->user->company->timezone)->format('Y-m-d');
        $data = $request->all();
        $holiday = Holiday::findOrNew($request->id);
        $holiday->fill($data);
        $holiday->from = Carbon::createFromFormat('m-d-Y',$request->from)->timezone($this->user->company->timezone)->format('Y-m-d');
        $holiday->to = ($request->has('to') && $request->to) ? Carbon::createFromFormat('m-d-Y',$request->to)->timezone($this->user->company->timezone)->format('Y-m-d') : $holiday->from;
        $holiday->company_id = $this->user->company_id;
        $holiday->delete_google_event = $request->has('delete_google_event') ? $request->delete_google_event : 0;
        $holiday->clone_google_event = $request->has('clone_google_event') ? $request->clone_google_event : 0;
        $holiday->save();
        if($holiday->clone_google_event){
            if($holiday->eid != ''){
                foreach(json_decode($holiday->eid, 1) as $user_id=>$eid){
                    cancleGC(\App\User::find($user_id), $eid);
                }
                // inactiveEvent($holiday->eid);
            }
            $users = \App\User::where('company_id', '=', $this->user->company_id)->where('GC_refresh_token', '!=', '')->get();
            $user_gc_info_array = [];
            foreach($users as $user){
                $user_gc_info_array[$user->id] = createGoogleEvent($holiday->title, $holiday->description, $from. "T00:00:00",  $to. "T23:59:00",  $user);
            }
            $holiday->eid = json_encode($user_gc_info_array);
            // $holiday->eid = createGoogleEvent($holiday->title, $holiday->description,  $from. "T00:00:00",  $to. "T23:59:00",  $this->user );
            if($holiday->delete_google_event){
                foreach(json_decode($holiday->eid, 1) as $user_id=>$eid){
                    cancleGC(\App\User::find($user_id), $eid);
                }
                $holiday->eid = '';
                // inactiveEvent($holiday->eid);
            }
        }
        $holiday->save();
        $msg = ($request->has('id') && $request->id) ? __('modules.holiday.updateMsg') : __('modules.holiday.createMsg');
        return Reply::redirect(url('admin/holiday'), $msg);
    }

    public function data() {
        $holiday = Holiday::all();
        return DataTables::of($holiday)
            ->addColumn('selectColumn', function($row) {
                $selectRow = '<div class="checkbox form-check">
                                <input id="' . $row->id . '" type="checkbox" name="id[]" class="form-check-input multi-check" value="' . $row->id . '" >
                                <label for="' . $row->id . '"></label>
                            </div>';
                return $selectRow;
            })
            ->addColumn('action', function ($row) {
                $action = '';
                    $edit = route('admin.holiday.edit', [$row->id]);
                    $action.= '<a href="javascript:openInModal(\'' .$edit. '\');" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    $action.= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
                return $action;
            })
            ->editColumn('title', function ($row) {
                return ucfirst($row->title);
            })
            ->editColumn('from', function ($row) {
                return Carbon::parse($row->from)->format('M d, Y');
            })
            ->editColumn('to', function ($row) {
                return Carbon::parse($row->to)->format('M d, Y');
            })
            ->editColumn('description', function ($row) {
                return $row->description;
            })
            ->rawColumns(['action', 'selectColumn'])
            ->make(true);
    }

    public function delete($id)
    {
        $eid = Holiday::find($id)->eid;
        if($eid != ''){
            foreach(json_decode($eid, 1) as $user_id=>$eid){
                cancleGC(\App\User::find($user_id), $eid);
            }
        }
        Holiday::destroy($id);
        return Reply::success(__('messages.holidayDeleted'));
    }

    public function multiDelete(Request $request)
    {
        $data = $request->input('id');
        $return = [];
        foreach ($data as $id) {
            $return[] = $this->delete($id);
        }
        return $return[0];
    }
}
