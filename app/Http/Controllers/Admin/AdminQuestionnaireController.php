<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Questionnaire\StoreRequest;
use App\Http\Requests\Questionnaire\UpdateRequest;
use App\Http\Requests\Questionnaire\SaveAnswersRequest;
use App\Question;
use App\Questionnaire;
use App\CustomFieldValue;
use App\JobApplication;
use App\JobApplicationAnswer;
use App\Job;
use DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class AdminQuestionnaireController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle ='menu.questionnaires';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        abort_if(! $this->user->cans('view_questionnaire'), 403);

        return view('admin.questionnaire.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(! $this->user->cans('add_questionnaire'), 403);
        $this->questions = Question::all();
        return view('admin.questionnaire.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();
        abort_if(! $this->user->cans('add_questionnaire'), 403);
        $questionnaire = new Questionnaire;
        $data['custom_field_value_ids'] = json_encode($request->questions);
        $questionnaire->fill($data);
        $questionnaire->company_id = $this->user->company_id;
        $questionnaire->save();
        return Reply::redirect(route('admin.questionnaire.index'), __('menu.questionnaire').' '.__('messages.createdSuccessfully'));
    }

    public function edit($id)
    {
        abort_if(! $this->user->cans('edit_questionnaire'), 403);
        $this->questionnaire = Questionnaire::find($id);
        // dd($this->questionnaire->custom_field_values);
        $this->selected_questions = $this->questionnaire->custom_field_values->count() ? 
          $this->questionnaire->custom_field_values->pluck('id')->toArray() :
          [];
        $this->questions = Question::all();
        return view('admin.questionnaire.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        abort_if(! $this->user->cans('edit_questionnaire'), 403);
        $data = $request->all();
        $questionnaire = Questionnaire::find($id);
        $data['custom_field_value_ids'] = json_encode($request->questions);
        $questionnaire->fill($data);
        $questionnaire->company_id = $this->user->company_id;
        $questionnaire->save();
        return Reply::redirect(route('admin.questionnaire.index'), __('menu.questionnaire').' '.__('messages.updatedSuccessfully'));
    }

    public function data() {
        abort_if(! $this->user->cans('view_questionnaire'), 403);

        $questionnaire = Questionnaire::all();
        return DataTables::of($questionnaire)
            ->addColumn('action', function ($row) {
                $action = '';

                if( $this->user->cans('edit_questionnaire')){
                    $action.= '<a href="' . route('admin.questionnaire.edit', [$row->id]) . '" class="btn btn-primary btn-circle"
                      data-toggle="tooltip" data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }

                if( $this->user->cans('delete_questionnaire')){
                    $action.= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
                return $action;
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function getAnswers(Request $request,$id)
    {
        $applicant_id = ($request->has('applicant_id') && $request->get('applicant_id')) ? $request->get('applicant_id') : null;
        $this->questions = Question::all();
        $questionnaire = Questionnaire::find($id);
        $this->custom_fields = $questionnaire->custom_field_values;
        $this->questionnaire_id = $id;
        $this->questionnaire = $questionnaire;
        $this->jobApplication = JobApplication::find($applicant_id);
        $answers = JobApplicationAnswer::where([
            'job_application_id'=>$applicant_id,
            'job_id'=>$this->jobApplication->job_id,
            'questionnaire_id' => $id
        ]);
        $this->editable = isset($request->editable);
        $this->answers = $answers->count() ? $answers->pluck('answer','question_id')->toArray() : [];
        return view('admin.questionnaire.answers', $this->data);
    }

    public function saveApplicantAnswers(SaveAnswersRequest $request)
    {
        if($request->has('questionnaire_id') && $request->questionnaire_id){
            $questionnaire = Questionnaire::find($request->questionnaire_id);
            $questions = $questionnaire->questions();
            $fields = $questionnaire->custom_field_values->first();
            $job = Job::find($request->job_id);
            $jobApplication = JobApplication::find($request->job_application_id);
            $user_qualified = 'qualified';
            $printData = [];
            if ($request->has('answer') && count($request->answer)) {
                $totalScore = 0;
                $totalCount = 0;
                foreach ($request->answer as $key => $answer_value) {
                    $value = implode(', ', (array)$answer_value);
                    $answer = JobApplicationAnswer::firstOrNew([
                        'job_application_id'=>$request->job_application_id,
                        'job_id'=>$request->job_id,
                        'questionnaire_id'=>$request->questionnaire_id,
                        'question_id'=>$key
                        ]);
                        $answer->job_application_id = $request->job_application_id;
                        $answer->job_id = $request->job_id;
                        $answer->question_id = $key;
                        $answer->company_id = $job->company_id;
                        $answer->answer = $value;
                        // if(!$answer->id){
                            $answer->questionnaire_id = $request->questionnaire_id;
                        // }
                        $question = Question::with('custom_fields')->find($key);
                        if($question && $question->custom_fields->count() > 0) {
                            $custom_question = $question->custom_fields[0];
                            $question_value  = json_decode($custom_question->question_value, 1);
                            $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                            if(is_array($question_qualifiable)) {
                                // $answer->score = 0;
                                $_total = count($question_qualifiable);
                                $_total = (($custom_question->question_type=='checkbox') && $_total>0) ? 1 : $_total;
                                $totalCount += 1;
                                foreach($question_qualifiable as $q) {
                                    if(isset($question_value[$q]) && in_array($question_value[$q], (array)$answer_value)) {
                                        // $answer->score += 1;
                                        $totalScore += (1 / $_total);
                                    }
                                }
                            }
                        }
                        $answer->save();
                        $printData[$answer->question_id]['question'] = $answer->question->question;
                        $printData[$answer->question_id]['answer'] = $answer->answer;
                }
                // if($totalScore){
                //     $jobApplication->eligibility = ($totalScore / ($totalCount > 0 ? $totalCount : 1)) * 100;
                // }
                $jobApplication->save();
                if($jobApplication->eligibility < $job->eligibility_percentage) {
                    $user_qualified = 'unqualified';
                }
                // Save Timeline
                saveTimeline([
                    'applicant_id' =>   $jobApplication->id,
                    'entity'       =>   'questionnaires',
                    'entity_id'    =>   $request->questionnaire_id,
                    'user_id'      =>   $this->user->id ?? 0,
                    'comments'     =>   "<b>Internal Questionnaire:</b> <i>(" . \App\Questionnaire::find($request->questionnaire_id)->name . ")</i> filled by {$this->user->name}"
                ]);
            }
            $data = [];
            foreach ($questions as $question) {
                $data[$question->id]['question'] = $question->question;
                $data[$question->id]['answer'] = array_key_exists($question->id,$printData) ? $printData[$question->id]['answer'] : '';
            }
            return Reply::dataOnly(['status' => 'success', 'msg' => __('modules.front.applySuccessMsg'),'printData'=>$data]);
        }
    }

    public function destroy($id)
    {
        abort_if(! $this->user->cans('delete_questionnaire'), 403);

        Questionnaire::destroy($id);
        return Reply::success(__('messages.questionnaireDeleted'));
    }
}
