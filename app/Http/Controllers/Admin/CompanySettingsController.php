<?php

namespace App\Http\Controllers\Admin;

use App\CompanySetting;
use App\Helper\Files;
use App\Helper\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\ApplicationSetting;
use App\Option;
use App\Holiday;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class CompanySettingsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.companySettings';
        $this->pageIcon = 'icon-settings';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(! $this->user->cans('manage_settings'), 403);

        $this->timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        $setting = Company::findOrFail(company()->id);

        if(!$setting){
            abort(404);
        }
        $this->engyj_api_token = Option::getOption('engyj_api_token', $this->user->company_id) ?: Str::random(14);
        $this->company_timings = json_decode(Option::getOption('company_timings', $this->user->company_id), 1);
        $this->appSetting = ApplicationSetting::where('company_id', $this->user->company_id)->first() ?? new ApplicationSetting;
        $this->holidays = $setting->holidays;
        $this->call_forward_number = Option::getOption('call_forward_number', $this->user->company_id);
        $this->twilio_from_number = Option::getOption('twilio_from_number', $this->user->company_id);
        return view('admin.settings.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_if(! $this->user->cans('manage_settings'), 403);

        $setting = Company::findOrFail($id);
        $setting->company_name      = $request->input('company_name');
        $setting->company_email     = $request->input('company_email');
        $setting->company_phone     = $request->input('company_phone');
        $setting->website           = $request->input('website');
        $setting->address           = $request->input('address');
        $setting->timezone          = $request->input('timezone');
        $setting->locale            = $request->input('locale');
        $setting->job_opening_text  = $request->input('job_opening_text');
        $setting->job_opening_title = $request->input('job_opening_title');
        $setting->career_page_link  = $request->input('slug');
        $setting->career_page_link  = $request->input('slug');
        Option::setOption('call_forward_number', $request->input('call_forward_number'), $this->user->company_id);
        Option::setOption('twilio_from_number',$request->input('twilio_from_number'), $this->user->company_id);
        Option::setOption('engyj_api_token', $request->engyj_api_token, $id);

        Option::setOption('company_timings', $request->company_timings, $id);
        if ($request->hasFile('logo')) {
            $setting->logo = Files::upload($request->logo,'company-logo');
        }  

        if ($request->hasFile('login_background')) {
            $setting->login_background = Files::upload($request->login_background,'login-background-image');
        }

        $setting->save();

        // Save Application setting Terms conditions
        $appSetting = ApplicationSetting::where('company_id', $this->user->company_id)->first();
        $appSetting->legal_term = $request->input('legal_term');
        $appSetting->save();
        // Save Holidays
        if($request->has('holidays') && count($request->holidays)){
            foreach ($request->holidays as $data) {
                $holiday = Holiday::findOrNew($data['id']);
                $holiday->fill($data);
                $holiday->date = \Carbon\Carbon::createFromFormat('m-d-Y',$data['date'])->timezone($this->user->company->timezone)->format('Y-m-d');
                $holiday->company_id = $setting->id;
                $holiday->save();
            }
        }
        
        return Reply::redirect(route('admin.settings.index'));
    }

    /**
     * Remove the holiday setting.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function deleteHoliday(Request $request)
    {
        if($request->has('id') && $request->id){
            Holiday::destroy($request->id);
        }
        return Reply::success('assas');
    }
}
