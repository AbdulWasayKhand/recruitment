<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Question\StoreRequest;
use App\Http\Requests\Question\UpdateRequest;
use App\Question;
use App\QuestionCategory;
use App\CustomFieldValue;
use DB;
use Yajra\DataTables\Facades\DataTables;

class AdminQuestionController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.customQuestions';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(! $this->user->cans('view_question'), 403);
        return view('admin.question.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(! $this->user->cans('add_question'), 403);

        $this->categories = QuestionCategory::where('company_id', $this->user->company_id)->orWhere('company_id', 0)->get();
        return view('admin.question.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $question_value = [];
        abort_if(! $this->user->cans('add_question'), 403);
        $question = new Question();
        $question->question = $request->question;
        $question->category_id = $request->category_id ?: '0';
        $question->required = $request->required;
        $question->sort_order = $request->sort_order;
        $question->save();
        $question_id = $question->id;

        $question_type = $request->question_type;
        if($question_type == 'dropdown'){
            $question_value = $request->dropdown ?? [];
        }else if($question_type == 'radio'){
            $question_value = $request->radio ?? [];
        }else if($question_type == 'checkbox'){
            $question_value = $request->checkbox ?? [];
        }

        foreach($question_value as $key=>$value){
            if($value == null || $value === 'null'){
                unset($question_value[$key]);
            }
        }
        $question_qualifiable = null;
        if($request->qualify_on == 'yes') {
            $question_qualifiable = json_encode($request->qualifiable ?: []);
        }

        $question_values = DB::connection()->getPdo()->quote(json_encode($question_value));

        DB::insert("INSERT INTO `custom_field_values`(`question_id`, `question_type`, `question_value`, `question_qualifiable`) VALUES ($question_id,'$question_type',$question_values, '$question_qualifiable')");

        return Reply::redirect(route('admin.questions.index'), __('menu.question').' '.__('messages.createdSuccessfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(! $this->user->cans('edit_question'), 403);
        $this->categories = QuestionCategory::where('company_id', $this->user->company_id)->orWhere('company_id', 0)->get();
        $this->question = Question::with('custom_fields')->find($id);
        return view('admin.question.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        abort_if(! $this->user->cans('edit_question'), 403);
        $question = Question::find($id);
        $question->question = $request->question;
        $question->category_id = $request->category_id;
        $question->required = $request->required;
        $question->sort_order = $request->sort_order;
        $question->save();
        $question_type = $request->question_type;
        $question_value = $request->$question_type;
        // if($question_type == 'dropdown'){
        //     $question_value = $request->dropdown;
        // }else if($question_type == 'radio'){
        //     $question_value = $request->radio;
        // }else if($question_type == 'checkbox'){
        //     $question_value = $request->checkbox;
        // }else if($request->question_type == 'text'){
        //     $request->question_type;
        // }

        $question_values = json_encode($question_value);

        $question_qualifiable = null;
        if($request->qualify_on == 'yes') {
            $question_qualifiable = json_encode($request->qualifiable ?: []);
        }

        CustomFieldValue::where('question_id', $question->id)->update([
            'question_type' => $question_type,
            'question_value' => $question_values,
            'question_qualifiable' => $question_qualifiable,
        ]);
        
        return Reply::redirect(route('admin.questions.index'), __('menu.question').' '.__('messages.updatedSuccessfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if(! $this->user->cans('delete_question'), 403);

        Question::destroy($id);
        return Reply::success(__('messages.questionDeleted'));
    }

    public function data() {
        abort_if(! $this->user->cans('view_question'), 403);

        $questions = Question::all()->sortByDesc("id");

        return DataTables::of($questions)
            ->addColumn('action', function ($row) {
                $action = '';

                if( $this->user->cans('edit_question')){
                    $action.= '<a href="' . route('admin.questions.edit', [$row->id]) . '" class="btn btn-primary btn-circle"
                      data-toggle="tooltip" data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }

                if( $this->user->cans('delete_question')){
                    $action.= ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
                return $action;
            })
            ->editColumn('qualifiable', function ($row) {
                return $row->is_qualifiable() ? 'Yes' : 'No';
            })
            ->editColumn('required', function ($row) {
                return ucfirst($row->required);
            })
            ->editColumn('requ', function ($row) {
                return ucfirst($row->question);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function addCategory()
    {
        if(!request('name'))
            return Reply::error('Category name required');
        if(QuestionCategory::where('company_id', $this->user->company_id)->whereName(request('name'))->exists())
            return Reply::error('Category with name already exist');

        $category = new QuestionCategory;
        $category->name = request('name');
        $category->company_id = $this->user->company_id;
        $category->save();

        return $category;
    }

}
