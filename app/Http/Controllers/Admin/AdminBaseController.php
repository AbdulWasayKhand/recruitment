<?php

namespace App\Http\Controllers\Admin;

use App\LanguageSetting;
use App\LinkedInSetting;
use App\StickyNote;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\ThemeSetting;
use App\Company;
use App\User;
use App\Option;
use App\CompanyPackage;
use Illuminate\Support\Facades\DB;
use App\GlobalSetting;
use Illuminate\Support\Facades\Route;

class AdminBaseController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[ $name ]);
    }

    /**
     * UserBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // Inject currently logged in user object into every view of user dashboard
//        $this->emailSetting = EmailNotificationSetting::all();
        $this->middleware(function ($request, $next) {

            $this->user = auth()->user();
            /* Give Permission Questionnaire */
            if(auth()->check() && auth()->user()->role->role->name=='admin'){
                $role_id = auth()->user()->role->role->id;
                $module = \App\Module::firstOrCreate(['module_name'=>'questionnaire']);
                $permission = \App\Permission::firstOrCreate([
                    'module_id'=>$module->id,
                    'name'=>'view_questionnaire',
                    'display_name'=>'View Questionnaire'
                ]);
                $permission_role = \App\PermissionRole::firstOrCreate([
                    'permission_id'=>$permission->id,
                    'role_id'=> $role_id
                ]);
                $permission = \App\Permission::firstOrCreate([
                    'module_id'=>$module->id,
                    'name'=>'add_questionnaire',
                    'display_name'=>'Add Questionnaire'
                ]);
                $permission_role = \App\PermissionRole::firstOrCreate([
                    'permission_id'=>$permission->id,
                    'role_id'=> $role_id
                ]);
                $permission = \App\Permission::firstOrCreate([
                    'module_id'=>$module->id,
                    'name'=>'edit_questionnaire',
                    'display_name'=>'Edit Questionnaire'
                ]);
                $permission_role = \App\PermissionRole::firstOrCreate([
                    'permission_id'=>$permission->id,
                    'role_id'=> $role_id
                ]);
                $permission = \App\Permission::firstOrCreate([
                    'module_id'=>$module->id,
                    'name'=>'delete_questionnaire',
                    'display_name'=>'Delete Questionnaire'
                ]);
                $permission_role = \App\PermissionRole::firstOrCreate([
                    'permission_id'=>$permission->id,
                    'role_id'=> $role_id
                ]);
            }
            /* end */
            // $multiEmail = Option::getOption("company_multi_email",$this->user->company_id ?? 0);
            // if($multiEmail){
            //     $this->user->email = $multiEmail;
            // }
            if ($this->user && $this->user->roles->count() > 0) {
                $this->todoItems = $this->user->todoItems()->groupBy('status', 'position')->get();
            }
            $this->superSettings = GlobalSetting::with('currency')->first();
            if($this->user) {
                $this->getPermissions = User::with('roles.permissions.permission')->find($this->user->id);
                $userPermissions = array();
                foreach ($this->getPermissions->roles[0]->permissions as $key => $value) {
                    $userPermissions[] = $value->permission->name;
                }
                $this->userPermissions = $userPermissions;
    
                $this->global = Company::findOrFail($this->user->company_id);
                $this->adminTheme = ThemeSetting::where('company_id', '=', $this->global->id)->first();
                $this->companyName = $this->global->company_name;
    
                $this->languageSettings = LanguageSetting::where('status', 'enabled')->orderBy('language_name')->get();
                $this->activePackage = CompanyPackage::with('package')->where('company_id', $this->user->company_id)
                    ->where('status', 'active')
                    ->where(function($query){
                        $query->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'));
                        $query->orWhereNull('end_date');
                    })
                    ->first();
        
                App::setLocale($this->global->locale);
                Carbon::setLocale($this->global->locale);
                setlocale(LC_TIME,$this->global->locale.'_'.strtoupper($this->global->locale));

                $this->stickyNotes = StickyNote::where('user_id', $this->user->id)
                    ->orderBy('updated_at', 'desc')
                    ->get();
                
                $this->company_settings = Option::getAll($this->user->company_id);
            }

            $this->engyj_baseurl = ( env('HTTP_HOST') == 'localhost' ) ? "http://local.engyj.com/" : "https://app.engyj.com/";
            $this->linkedinGlobal = LinkedInSetting::first();
            return $next($request);
        });
    }

    public function generateTodoView()
    {
        $pendingTodos = $this->user->todoItems()->status('pending')->orderBy('position', 'DESC')->limit(5)->get();
        $completedTodos = $this->user->todoItems()->status('completed')->orderBy('position', 'DESC')->limit(5)->get();

        $view = view('sections.todo_items_list', compact('pendingTodos', 'completedTodos'))->render();

        return $view;
    }
}
