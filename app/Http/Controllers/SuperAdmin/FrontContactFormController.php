<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\ContactForm;
use DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class FrontContactFormController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle ='menu.contactForms';
        $this->pageIcon = 'icon-grid';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('super-admin.contact-form.index', $this->data);
    }

    public function tableData(Request $request) {
        $contacts = ContactForm::all();
        return DataTables::of($contacts)
            ->addColumn('action', function ($row) {
                return ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                data-toggle="tooltip" data-row-id="' . $row->id . '" data-original-title="' . __('app.delete') . '"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->addColumn('company_name', function ($row) {
                return ucfirst($row->company->company_name);
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            ->editColumn('email', function ($row) {
                return ucfirst($row->email);
            })
            ->editColumn('message', function ($row) {
                return ucfirst($row->message);
            })
            ->rawColumns(['action','company_name'])
            ->addIndexColumn()
            ->make(true);
    }

    public function destroy($id)
    {
        $form = ContactForm::find($id)->delete();
        return Reply::success(__('messages.contactFormDeletedSuccessfully'));
    }
}