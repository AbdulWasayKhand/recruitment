<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\User;
use App\FacebookPage;
use App\Helper\Reply;
use App\Helper\FBApi;

class FacebookSettingController extends SuperAdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'menu.facebookSettings';
        $this->pageIcon = 'icon-settings';
    }
    
    public function index()
    {
        $this->fbdata = json_decode(User::find(1)->facebook_data?:'{}', 1);
        $this->pages = FacebookPage::all();
        return view('super-admin.facebook-settings.edit', $this->data);
    }

    public function update(Request $request, $id = NULL)
    {
        $data = $request->data;
        $data['start_date'] = now();
        $update = User::find(1);
        $update->facebook_data = json_encode($data);
        $update->save();

        return back()->with('status', 'Token Updated');
    }

    public function updateFacebookPages()
    {
        $fb_data = json_decode(User::find(1)->facebook_data?:'{}', 1);
        $token = $fb_data['access_token'];
        
        $api = (new FBApi())->setUserId($fb_data['user_id'])->setAccessToken($token);
        $pages = $api->getPages();
        while (true) {
            foreach ($pages['data'] as $page) {
                $dbpage = FacebookPage::where('facebook_id', $page['id'])->first();
                if(!is_null($dbpage)) {
                    $dbpage->update([
                        'facebook_id' => $page['id'],
                        'name' => $page['name'],
                        'access_token' => $page['access_token'],
                    ]);
                } else {
                    FacebookPage::insert([
                        'facebook_id' => $page['id'],
                        'name' => $page['name'],
                        'access_token' => $page['access_token'],
                    ]);
                }
            }

            if(isset($pages['paging']['next']) && !empty($pages['paging']['next'])) {
                $pages = $api->getPages($pages['paging']['cursors']['after']);
                continue;
            }
            
            break;
        }

        return back()->with('status', 'Pages Updated');
    }

}
