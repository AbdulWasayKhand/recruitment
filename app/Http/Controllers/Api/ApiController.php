<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Job;
use App\Option;
use App\JobApplication;
use App\User;

class ApiController extends Controller
{
    function validateEmail(Request $request){
        return User::where('email',$request->email)->exists();
    }

    function getApplicants(Request $request){
        if(isset($request->token) && isset($request->job_id)) {
            $job = Job::findOrFail($request->job_id);
            $token = Option::getOption('engyj_api_token', $job->company_id);
            if($token === $request->token) {
                $results = JobApplication::where('job_id', $request->job_id)->get();
                if($results->isEmpty()){
                    return [
                        "status"=>"failed", 
                        "error"=>"fields miss-match", 
                        "message"=> "api_token or job_id does not metch"
                    ];
                }
    
                return [
                    'status' => 'success',
                    'count' => $results->count(),
                    'data' => $results,
                ];
            }

            return [
                'status' => 'failed',
                'error' => 'auth error',
            ];
        }

        return [
            "status" => "failed",
            "error" => "fields missing", 
            "message" => "This API required fields are missing."
        ];
    }

    function getJobList(Request $request) {
        if(isset($request->token)) {
            $token = Option::getOption('engyj_api_token', $request->company_id);
            $company_id = Option::where('option_name', 'engyj_api_token')
                                    ->where('option_value', $request->token)
                                    ->first()->company_id ?? NULL;
            
            if($company_id) {
                $results = Job::select('jobs.id', 'jobs.title')
                    ->where('company_id', $company_id)
                    ->where("status", "active")
                    ->get();
                
                return [
                    'status' => 'success',
                    'count' => $results->count(),
                    'data' => $results,
                ];
            }

            return [
                'status' => 'failed',
                'error' => 'auth error',
            ];
        }

        return [
            "status" => "failed",
            "error" => "fields missing",
            "message" => "This API required fields are missing."
        ];
    }
} 