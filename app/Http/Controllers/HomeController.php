<?php

namespace App\Http\Controllers;

use App\JobApplication;
use App\Option;
use App\SmsLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Company;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AdminSMSEmail;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function twilioSmsReply(Request $request)
    {
        if($request->SmsStatus == 'received' || $request->SmsStatus == 'receiving') {

            $from = $request->To;
            $from_phone = $request->From;
            // If candidate not exist
            if(JobApplication::where('phone', $from_phone)->count()==0){
                $response = Http::get('https://app.engyj.com/index.php/subscriber_text_reply', [
                    'from' => $request->From,
                    'to' => $request->To,
                    'message' => $request->Body,
                ]);
            }
            else{
                $company_id = Option::where('option_name', 'twilio_from_number')
                ->where('option_value', $from)
                ->first()->company_id ?? NULL;  
                
                if(!is_null($company_id)) {
                    $contact =  User::where('company_id', $company_id)->first()->mobile;
                    $user =  User::where('company_id', $company_id)->first();
                    $to = $request->From;
                    $applicant = JobApplication::where('phone', $to)->where('company_id', $company_id)->first();
                    $AccountSid = $request->AccountSid;
                    $sid = $request->SmsMessageSid;
                    $body = $request->Body;

                    if(!$request->filled('To', 'From', 'AccountSid')) {
                        Log::info('SMS_TWILIO_RESPONSE_INVALID_REQUEST::' . json_encode($request->all()));
                        return ['status' => 'error'];
                    }

                    SmsLog::create([
                    'company_id' => $company_id,
                    'number'  => $to,
                    'message' => $body,
                    'message_type' => 'inbound',
                    'type'    => 'normal',
                    'status'  => 'sent',
                    'send_at' => now('GMT')->toDateTimeString(),
                    'reference_id' => $applicant->id ?? NULL,
                    'reference_name' => 'application',
                    'provider_response' => "[Twilio.Api.V2010.MessageInstance accountSid={$AccountSid} sid={$sid}]",
                    ]);
                    if($contact){
                        SmsLog::create([
                            'company_id' => $company_id,
                            'number'  => $contact,
                            'message' => $body,
                            'message_type' => 'outbound',
                            'type'    => 'normal',
                            'status'  => 'pending',
                            'send_at' => now('GMT')->toDateTimeString(),
                            'reference_id' => $applicant->id ?? NULL,
                            'reference_name' => 'application',
                            'provider_response' => NULL,
                        ]);

                        try{
                            $message = "Hello, <br><br>You just got a <b>text message</b> sent to your WAZiE Recruit Phone Number <br><br><b>" . $applicant->full_name . " said: </b><br>\"" . $body . "\" <br> Their cell phone number is <br><b>" . $to . "</b> <br>you can reply to this text message on WAZiE Recruit Text Messages";
                            Notification::send($user, new AdminSMSEmail($applicant, $message));
                        } catch(\Exception $e) {}
                    }
                    return ['status' => 'success'];
                }
                Log::debug(['status' => 'no_company', 'from' => $request->From, 'to' => $request->To, 'request' => $request->all()]);
                return;
            }//else of non candidate exist
        }
        Log::info('TWILIO_ENDPOINT::' . json_encode($request->only('From', 'To', 'SmsMessageSid', 'Body', 'MessageStatus')));
    }
}
