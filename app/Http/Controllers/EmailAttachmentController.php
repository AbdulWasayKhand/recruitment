<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Reply;
use App\WorkflowStageActionEmailAttachment;
use App\JobApplication;
    use App\Helper\Files;

class EmailAttachmentController extends Controller
{
    public function index($data)
    {
        if($data){
            $data = explode(".", $data);
            $jobApplication = \App\JobApplication::find($data[0]);
            if($jobApplication){
                $employee = \App\User::where('company_id' , '=' , $jobApplication->company_id)->first();
                $action = WorkflowStageActionEmailAttachment::findOrNew($data[1]);
                $template = \App\EmailTemplate::findOrNew($action->email_template_id);
                $template->content = strtr($template->content ?? '', [
                    '[applicant_name]'  => $jobApplication->full_name ?? '[applicant_name]',
                    '[applicant_email]'  => $jobApplication->email ?? '[applicant_email]',
                    '[employee_name]'   => $employee->name ?? '[employee_name]',
                    '[company_name]'    => $jobApplication->job->company->company_name ?? '[company_name]',
                    '[interview_url]'   => '[interview_url]',
                    '[schedule_date]'   => '[schedule_date]',
                    '[schedule_time]'   => '[schedule_time]',
                    '[job_title]'       => $jobApplication->job->title ?? '[job_title]',
                    '[qualification]'   => ucfirst(strtolower(explode(' ', $jobApplication->qualified)[0])),
                    '[applied_note]'    => $jobApplication->applied_before ? '<br><strong>Note:</strong> <span style="color:red;">This applicant has applied before.</span>' : ''
                ]);
                $viewData = [
                    'jobApplication' => $jobApplication,
                    'action' => $action,
                    'template' => $template 
                ];
                return view('email-attachments', $viewData);
            }
            $thanks_page = \App\Option::getOption('thanks_page_url', \App\JobApplication::onlyTrashed()->find($data[0])->company_id);
            return redirect($thanks_page);
        }
    }

    public function store(Request $request)
    {
        if($request->has('applicant_id') && $request->has('attachments')){
            $names = [];
            foreach($request->attachments as $data) {
                $names[] = $data['name'];
            }
            foreach($request->attachments as $data) {
                $applicant = JobApplication::with('attachments')->find($request->applicant_id);
                $attachment = $applicant->attachments()->findOrNew($data['id']);
                $hashname = isset($data['file']) ? Files::upload($data['file'], 'documents/'.$request->applicant_id, null, null, false) : $attachment->hashname;
                $attachment->fill([
                    'name' => $data['name'],
                    'hashname' => $hashname,
                    'action_id' => $request->action_id,
                    'documentable_type' => 'App\JobApplication',
                    'company_id' => $applicant->company_id,
                ]);
                $attachment->save();
            }
            // Save Resume Attachment Timeline
            saveTimeline([
                'applicant_id' =>   $applicant->id,
                'entity'       =>   'documents',
                'entity_id'    =>   $attachment->id,
                'comments'     =>   "Documents sent by {$applicant->full_name}"
            ]);
            $thank_you_page = \App\Option::getOption('thanks_page_url', $attachment->company_id);
            if($thank_you_page){
                return Reply::redirect($thank_you_page);
            }
        }
    }
}