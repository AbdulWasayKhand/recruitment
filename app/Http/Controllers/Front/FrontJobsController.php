<?php

namespace App\Http\Controllers\Front;

use App\ApplicationSetting;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\FrontJobApplication;
use App\Question;
use App\Job;
use App\JobApplication;
use App\JobApplicationAnswer;
use App\JobCategory;
use App\JobLocation;
use App\LinkedInSetting;
use App\Notifications\NewJobApplication;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Notification;
use App\Company;
use App\ApplicationStatus;
use App\CompanyPackage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReceivedApplication;
use App\ThemeSetting;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use App\Helper\TwilioHelper as Twilio;
use App\Option;
use App\SmsLog;
use App\Notifications\AdminCandidateNotification;
use App\ApplicantHistory;
use \App\Http\Controllers\Admin\AdminJobApplicationController;

class FrontJobsController extends FrontBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('modules.front.jobOpenings');

        $linkedinSetting = LinkedInSetting::where('status', 'enable')->first();
        /*dd($linkedinSetting);*/
        if ($linkedinSetting) {
            Config::set('services.linkedin.client_id', $linkedinSetting->client_id);
            Config::set('services.linkedin.client_secret', $linkedinSetting->client_secret);
            Config::set('services.linkedin.redirect', $linkedinSetting->callback_url);
        }
    }

    public function jobOpenings($slug)
    {
        $company = Company::withoutGlobalScope('company')->where('career_page_link', $slug)->first();

        $activePackage = CompanyPackage::where('company_id', $company->id)
            ->where('status', 'active')
            ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->first();

        if (!$activePackage) {
            return abort(404);
        }

        $this->jobs = Job::frontActiveJobs($company->id);
        $this->locations = JobLocation::withoutGlobalScope('company')->where('company_id', $company->id)->get();
        $this->categories = JobCategory::withoutGlobalScope('company')->where('company_id', $company->id)->get();

        $this->company = $this->global = $company;
        $this->companyName = $this->global->company_name;
        $this->frontTheme = ThemeSetting::where('company_id', $this->company->id)->first();
        App::setLocale($this->global->locale);
        Carbon::setLocale($this->global->locale);
        setlocale(LC_TIME, $this->global->locale.'_'.strtoupper($this->global->locale));

        return view('front.job-openings', $this->data);
    }

    public function scheduleNotFound(Request $request, $slug)
    {
        $company = Company::withoutGlobalScope('company')->where('career_page_link', $slug)->first();

        $this->jobs = Job::frontActiveJobs($company->id);
        $this->locations = JobLocation::withoutGlobalScope('company')->where('company_id', $company->id)->get();
        $this->categories = JobCategory::withoutGlobalScope('company')->where('company_id', $company->id)->get();

        $this->company = $this->global = $company;
        $this->companyName = $this->global->company_name;
        $this->frontTheme = ThemeSetting::where('company_id', $this->company->id)->first();
        $this->pageTitle = 'Sorry No Schedule Found.';
        return view('front.schedule-not-found', $this->data);
    }

    public function scheduleRequestThankyou(Request $request, $slug)
    {
        $company = Company::withoutGlobalScope('company')->where('career_page_link', $slug)->first();

        $activePackage = CompanyPackage::where('company_id', $company->id)
            ->where('status', 'active')
            ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->first();

        $this->jobs = Job::frontActiveJobs($company->id);
        $this->locations = JobLocation::withoutGlobalScope('company')->where('company_id', $company->id)->get();
        $this->categories = JobCategory::withoutGlobalScope('company')->where('company_id', $company->id)->get();

        $this->company = $this->global = $company;
        $this->companyName = $this->global->company_name;
        $this->frontTheme = ThemeSetting::where('company_id', $this->company->id)->first();
        $this->pageTitle = 'Thank you';
        App::setLocale($this->global->locale);
        Carbon::setLocale($this->global->locale);
        setlocale(LC_TIME, $this->global->locale.'_'.strtoupper($this->global->locale));
        if($request->has('already_accepted')){
            return view('front.schedule-requested-already',$this->data);
        }
        return view('front.schedule-request-thankyou-page', $this->data);
    }

    public function jobDetail($slug)
    {
        if(request('widget')){
            header('Access-Control-Allow-Origin: *');
        }
        $this->job = Job::where('slug', $slug)
            ->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>=', Carbon::now())
            ->where('status', 'active')
            ->firstOrFail();
        
        $this->linkedinGlobal = LinkedInSetting::first();
        Session::put('lastPageUrl', $slug);

        $this->company = $this->global = $this->job->company;

        $activePackage = CompanyPackage::where('company_id', $this->company->id)
            ->whereDate('end_date', '>=', Carbon::now())
            ->where('status', 'active')
            ->first();
        
        if (!$activePackage) {
            // return abort(404);
        }

        $this->companyName = $this->global->company_name;
        $this->frontTheme = ThemeSetting::where('company_id', $this->company->id)->first();
        App::setLocale($this->global->locale);
        Carbon::setLocale($this->global->locale);
        setlocale(LC_TIME, $this->global->locale.'_'.strtoupper($this->global->locale));

        $this->pageTitle = $this->job->title . ' - ' . $this->companyName;
        $this->metaTitle = $this->job->meta_details['title'];
        $this->metaDescription = $this->job->meta_details['description'];
        $this->metaImage = $this->job->company->logo_url;
        $this->pageUrl = request()->url();

        return view('front.job-detail', $this->data);
    }

    public function callback($provider, Request $request)
    {
        if ($request->error) {
            $this->errorCode = $request->error;
            $this->error = $request->error_description;
            return view('errors.linkedin', $this->data);
        }
        $this->user = Socialite::driver($provider)->stateless()->user();
//        dd($this->user);
        $this->lastPageUrl = Session::get('lastPageUrl');
        Session::put('accessToken', $this->user->token);
        Session::put('expiresIn', $this->user->expiresIn);
        return redirect()->route('jobs.jobApply', $this->lastPageUrl);

    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function jobApply($slug)
    {
        $this->job = Job::where('slug', $slug)
            ->where(DB::raw('DATE(start_date)'), '<=', DB::raw('CURDATE()'))
            ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->firstOrFail();

        $this->accessToken = Session::get('accessToken');
        if ($this->accessToken) {
            $this->user = Socialite::driver('linkedin')->userFromToken($this->accessToken);
        } else {
            $this->user = [];
        }
        $this->job = Job::where('slug', $slug)->first();
      
        $this->company = $this->global = $this->job->company;
      
        $activePackage = CompanyPackage::where('company_id', $this->company->id)
            ->where('status', 'active')
            ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->first();

        if (!$activePackage) {
            // return abort(404);
        }
        
        $this->jobQuestion = $this->job->questions->filter(function($item) {
            return $item->category == null;
        });
        $groupQuestions = $this->job->questions->filter(function($item) {
            return $item->category != null;
        })->values();
        
        foreach ($groupQuestions as $key => $value) {
            $groupQuestions[$key]->category_name = $value->category->name;
        }

        $this->groupQuestions = $groupQuestions->groupBy('category_name', true);
        
        $this->companyName = $this->global->company_name;
        $this->frontTheme = ThemeSetting::where('company_id', $this->job->company_id)->first();
        App::setLocale($this->global->locale);
        Carbon::setLocale($this->global->locale);
        setlocale(LC_TIME, $this->global->locale.'_'.strtoupper($this->global->locale));
        $this->applicationSetting = DB::table('application_settings')->where('company_id', $this->company->id)->first();
        $this->pageTitle = $this->job->title . ' - ' . $this->companyName;
        if(isset($_REQUEST['jobView'])){
            return view('apply-job', $this->data);
        }
        return view('front.job-apply', $this->data);
    }

    public function saveApplication(FrontJobApplication $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Accept, Content-Type');

        $job = Job::findOrFail($request->job_id);
        $activePackage = CompanyPackage::where('company_id', $job->company_id)
            ->where('status', 'active')
            ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->first();
        
        if (!$activePackage) {
            // return abort(404);
        }

        $applicationStatus = ApplicationStatus::where('company_id', $job->company_id)->firstOrFail();

        $jobApplication = new JobApplication();
        $jobApplication->full_name = $request->full_name;
        $jobApplication->job_id = $request->job_id;
        $jobApplication->company_id = $job->company_id;
        if($job->workflow_id==0){
            $jobApplication->status_id = $applicationStatus->id;
        }
        else{
            $jobApplication->workflow_stage_id = $request->stage_id != null ? $request->stage_id : $job->workflow->applied_stage->id;
        }
        $jobApplication->email = $request->email;
        $phone = str_replace([' ', '-'], '', $request->phone);
        $phone = Str::startsWith($phone, '+1') ? $phone : ('+1' . $phone);
        $jobApplication->phone = $phone;
        $jobApplication->email_token = Str::random(42);
        
        if ($request->has('gender')) {
            $jobApplication->gender = $request->gender;
        }
        if ($request->has('dob')) {
            $jobApplication->dob = $request->dob;
        }
        if ($request->has('country')) {
            $countriesArray = json_decode(file_get_contents(public_path('country-state-city/countries.json')), true)['countries'];
            $statesArray = json_decode(file_get_contents(public_path('country-state-city/states.json')), true)['states'];
            
            $jobApplication->country = $this->getName($countriesArray, $request->country);
            $jobApplication->state = $this->getName($statesArray, $request->state);
            $jobApplication->city = $request->city;
        }
        
        $jobApplication->cover_letter = $request->cover_letter;
        $jobApplication->column_priority = 0;
        
        if ($request->hasFile('photo')) {
            $jobApplication->photo = Files::upload($request->photo,'candidate-photos');
        }
        $jobApplication->save();
        if($job->workflow_id && in_array($jobApplication->workflow_stage->type,['hired','rejected'])){
            $jobApplication->hiring_status = $jobApplication->workflow_stage->type;
            $jobApplication->save();
        }
        saveTimeline([
            'applicant_id' =>   $jobApplication->id,
            'entity'       =>   'stage',
            'entity_id'    =>   $jobApplication->workflow_stage_id,
            'comments'     =>   "Applicant applied and moved to stage {$jobApplication->workflow_stage->title}"
        ]);
        
        // Save Photo Attachment Timeline
        if ($request->hasFile('photo')) {
            saveTimeline([
                'applicant_id' =>   $jobApplication->id,
                'entity'       =>   'attachment_photo',
                'entity_id'    =>   $jobApplication->id,
                'comments'     =>   "Photo Attached by {$jobApplication->full_name}"
            ]);
        }
        $applicantHistory = new ApplicantHistory();
        $applicantHistory->current_value = $jobApplication->status->id;
        $applicantHistory->status_type = 'application';
        $applicantHistory->company_id = $jobApplication->company_id;
        $applicantHistory->applicant_id = $jobApplication->id;
        $applicantHistory->save();

        if ($request->hasFile('resume')) {
            $hashname = Files::upload($request->resume, 'documents/'.$jobApplication->id, null, null, false);
            $attached = $jobApplication->documents()->create([
                'company_id' => $job->company_id,
                'name' => 'Resume',
                'hashname' => $hashname
            ]);
            // Save Resume Attachment Timeline
            saveTimeline([
                'applicant_id' =>   $jobApplication->id,
                'entity'       =>   'attachment_resume',
                'entity_id'    =>   $jobApplication->id,
                'comments'     =>   "Resume Attached by {$jobApplication->full_name}"
            ]);
        }

        $users = User::frontAllAdmins($job->company_id);
        $linkedin = false;
        if ($request->linkedinPhoto) {
            $contents = file_get_contents($request->linkedinPhoto);
            $getfilename =  str_replace(' ', '_', $request->full_name);
            $filename = $jobApplication->id.$getfilename.'.png';
            Storage::put('candidate-photos/'.$filename, $contents);
            $jobApplication = JobApplication::find($jobApplication->id);
            $jobApplication->photo = $filename;
            $jobApplication->save();
        }

        if($request->has('apply_type')){
            $linkedin = true;
        }
        
        // Save referer url if apply from admin side
        if($request->has('referer_url') && $request->referer_url){
            $jobApplication->referer_url = $request->referer_url;
            switch ($request->referer_url) {
                case "https://mycna.ca/":
                    $jobApplication->source = 'MyCNA';
                    break;
                case "other":
                        $jobApplication->source = 'other';
                        $jobApplication->referer_url = ($request->has('source') && $request->source) ? $request->source : '';
                    break;
                    case "referral":
                        $jobApplication->source = 'referral';
                        $jobApplication->referer_url = ($request->has('source') && $request->source) ? $request->source : '';
                    break;
                case "https://www.careerplug.com/":
                    $jobApplication->source = 'career_plug';
                    break;
                case "https://www.jazzhr.com/":
                    $jobApplication->source = 'jazzhr';
                    break;
                case "https://www.craigslist.org/":
                    $jobApplication->source = 'craigslist';
                    break;
            }
        }

        $user_qualified = 'qualified';
        if (!empty($request->answer)) {
            $totalScore = 0;
            $totalCount = 0;
            foreach ($request->answer as $key => $answer_value) {
                $value = implode(', ', (array)$answer_value);
                $answer = new JobApplicationAnswer();
                $answer->job_application_id = $jobApplication->id;
                $answer->job_id = $request->job_id;
                $answer->question_id = $key;
                $answer->company_id = $job->company_id;
                $answer->answer = $value;
                $question = Question::with('custom_fields')->find($key);
                if($question && $question->custom_fields->count() > 0) {
                    $custom_question = $question->custom_fields[0];
                    $question_value  = json_decode($custom_question->question_value, 1);
                    $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                    if(is_array($question_qualifiable)) {
                        $answer->score = 0;
                        $_total = count($question_qualifiable);
                        $_total = ($_total>0) ? 1 : $_total;
                        $totalCount += 1;
                        foreach($question_qualifiable as $q) {
                            if(isset($question_value[$q]) && in_array($question_value[$q], (array)$answer_value)) {
                                $answer->score += 1;
                                $totalScore += (1 / $_total);
                            }
                        }
                    }
                }

                $answer->save();
            }

            $jobApplication->eligibility = ($totalScore / ($totalCount > 0 ? $totalCount : 1)) * 100;
            $jobApplication->save();
            if($jobApplication->eligibility < $job->eligibility_percentage) {
                $user_qualified = 'unqualified';
            }
        }

        $reciver = User::where('company_id', '=', $jobApplication->company_id)->first();
        if($reciver->cant_text){
            $to = "+1" . $reciver->mobile;
            $message = "You have received a new job application from {$jobApplication->full_name} for the " . ucwords($jobApplication->job->title) . " position.";
            SmsLog::reminder($to, $message, date('Y-m-d h:i:s'), $jobApplication->company_id, 'admin-alert', $jobApplication->id);
        }
        $stage = new AdminJobApplicationController();
        // $stage->performStageAction($jobApplication);
        $stage->performStageAction($jobApplication, true);
        // $jobApplication->save();
        $debug = [];
        // if($job->workflow_id==0){
            $debug = $this->actionAfterApply($job, $jobApplication, $applicationStatus, $user_qualified);
        // }

        // If Trigger Exists then triggout
        // if(isset($stage_id)){
        //     $jobApplication->workflow_stage_id = $stage_id;
        //     $jobApplication->save();
        // }

        return Reply::dataOnly(['status' => 'success', 'msg' => __('modules.front.applySuccessMsg'), 'debug' => $debug]);
    }

    public function jobApplyCareer(Request $request)
    {
        $job = Job::findOrFail($request->job_id);

        $applicationStatus = ApplicationStatus::where('company_id', $job->company_id)->firstOrFail();

        $jobApplication = new JobApplication();
        $jobApplication->full_name = $request->full_name;
        $jobApplication->job_id = $request->job_id;
        $jobApplication->company_id = $job->company_id;
        $jobApplication->status_id = $applicationStatus->id;
        $jobApplication->email = $request->email;
        $phone = str_replace([' ', '-'], '', $request->phone);
        $phone = Str::startsWith($phone, '+1') ? $phone : ('+1' . $phone);
        $jobApplication->phone = $phone;
        $jobApplication->email_token = Str::random(42);
        $jobApplication->source = 'widget';
        
        if ($request->has('gender')) {
            $jobApplication->gender = $request->gender;
        }
        if ($request->has('dob')) {
            $jobApplication->dob = $request->dob;
        }
        if ($request->has('country')) {
            $countriesArray = json_decode(file_get_contents(public_path('country-state-city/countries.json')), true)['countries'];
            $statesArray = json_decode(file_get_contents(public_path('country-state-city/states.json')), true)['states'];

            $jobApplication->country = $this->getName($countriesArray, $request->country);
            $jobApplication->state = $this->getName($statesArray, $request->state);
            $jobApplication->city = $request->city;
        }

        $jobApplication->cover_letter = $request->cover_letter;
        $jobApplication->column_priority = 0;

        if ($request->hasFile('photo')) {
            $jobApplication->photo = Files::upload($request->photo,'candidate-photos');
        }
        if($request->has('referer_url') && $request->referer_url){
            $jobApplication->referer_url = $request->referer_url;
        }
        $jobApplication->save();

        $applicantHistory = new ApplicantHistory();
        $applicantHistory->current_value = $jobApplication->status->id;
        $applicantHistory->status_type = 'application';
        $applicantHistory->company_id = $jobApplication->company_id;
        $applicantHistory->applicant_id = $jobApplication->id;
        $applicantHistory->save();

        if ($request->hasFile('resume')) {
            $hashname = Files::upload($request->resume, 'documents/'.$jobApplication->id, null, null, false);
            $jobApplication->documents()->create([
                'company_id' => $job->company_id,
                'name' => 'Resume',
                'hashname' => $hashname
            ]);
        }

        $users = User::frontAllAdmins($job->company_id);
        $linkedin = false;
        if ($request->linkedinPhoto) {
            $contents = file_get_contents($request->linkedinPhoto);
            $getfilename =  str_replace(' ', '_', $request->full_name);
            $filename = $jobApplication->id.$getfilename.'.png';
            Storage::put('candidate-photos/'.$filename, $contents);
            $jobApplication = JobApplication::find($jobApplication->id);
            $jobApplication->photo = $filename;
            $jobApplication->save();
        }

        if($request->has('apply_type')){
            $linkedin = true;
        }
        $user_qualified = 'qualified';
        if (!empty($request->answer)) {
            $totalScore = 0;
            $totalCount = 0;
            foreach ($request->answer as $key => $answer_value) {
                $value = implode(', ', (array)$answer_value);
                $answer = new JobApplicationAnswer();
                $answer->job_application_id = $jobApplication->id;
                $answer->job_id = $request->job_id;
                $answer->question_id = $key;
                $answer->company_id = $job->company_id;
                $answer->answer = $value;
                $question = Question::with('custom_fields')->find($key);
                if($question && $question->custom_fields->count() > 0) {
                    $custom_question = $question->custom_fields[0];
                    $question_value  = json_decode($custom_question->question_value, 1);
                    $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                    if(is_array($question_qualifiable)) {
                        $answer->score = 0;
                        $_total = count($question_qualifiable);
                        $_total = ($_total>0) ? 1 : $_total;
                        $totalCount += 1;
                        foreach($question_qualifiable as $q) {
                            if(isset($question_value[$q]) && in_array($question_value[$q], (array)$answer_value)) {
                                $answer->score += 1;
                                $totalScore += (1 / $_total);
                            }
                        }
                    }
                }

                $answer->save();
            }

            $jobApplication->eligibility = ($totalScore / $totalCount) * 100;
            $jobApplication->save();

            if($jobApplication->eligibility < $job->eligibility_percentage) {
                $user_qualified = 'unqualified';
            }
        }
        return Reply::dataOnly(['status' => 'success', 'msg' => __('modules.front.applySuccessMsg'), 'debug' => $debug]);
    }

    public function jobApplyCaregiver(Request $request)
    {
        $user_info = $request->user ?: [];
        $questions = $request->messages;
        
        $job = Job::findOrFail($request->job_id);

        $applicationStatus = ApplicationStatus::where('company_id', $job->company_id)->firstOrFail();

        if(empty(Arr::get($user_info, 'name', ''))) {
            foreach ($questions as $answer) {
                $question = Arr::get($answer, 'question');
                $input_type = Arr::get($answer, 'input_type');
                if( is_numeric(strpos($input_type, 'name')) ) {
                    $name = strtolower(Arr::get($answer, 'value'));
                    $keywords = [
                        'name',
                        'is',
                        'im',
                        'am',
                        'i am',
                        'called',
                        'i\'m',
                        'hello'
                    ];
                    foreach($keywords as $word){
                        $name = explode(' '.$word.' ',$name)[1] ?? $name;   // LIKE
                    }
                    $name = explode(' ', $name);
                    foreach($name as $key=>$value){
                        $name[$key] = ucfirst($value);
                    }
                    $user_info['name']  = implode(" ", $name);
                } else
                if( is_numeric(strpos($input_type, 'email')) ) {
                    $user_info['email'] = Arr::get($answer, 'value');
                } else
                if( is_numeric(strpos($input_type, 'phone')) ) {
                    $user_info['phone'] = Arr::get($answer, 'value');
                }
            }
        }

        $jobApplication = new JobApplication();
        $jobApplication->full_name = Arr::get($user_info, 'name');
        $jobApplication->job_id = $request->job_id;
        $jobApplication->company_id = $job->company_id;
        if($job->workflow_id==0){
            $jobApplication->status_id = $applicationStatus->id;
        }
        else{
            $jobApplication->workflow_stage_id = $job->workflow->applied_stage->id;
        }
        $jobApplication->email = Arr::get($user_info, 'email');
        $phone = str_replace([' ', '-', '(', ')'], '', Arr::get($user_info, 'phone'));
        $phone = Str::startsWith($phone, '+1') ? $phone : ('+1' . $phone);
        $jobApplication->phone = $phone;
        $jobApplication->email_token = Str::random(42);
        $jobApplication->source = 'widget';
        
        $jobApplication->cover_letter = '';
        $jobApplication->column_priority = 0;
        if($request->has('referer_url') && $request->referer_url){
            $jobApplication->referer_url = $request->referer_url;
        }
        // Save Widget
        if($request->has('site_id') && $request->get('site_id')){
            $response = \Illuminate\Support\Facades\Http::get("https://app.engyj.com/chat/index.php?d=visitors&c=chat&m=get_site_name&site_id={$request->get('site_id')}");
            $jobApplication->widget_host = $response->body();
        }
        $jobApplication->save();

        $applicantHistory = new ApplicantHistory();
        $applicantHistory->current_value = $jobApplication->status->id;
        $applicantHistory->status_type = 'application';
        $applicantHistory->company_id = $jobApplication->company_id;
        $applicantHistory->applicant_id = $jobApplication->id;
        $applicantHistory->save();

        $user_qualified = 'qualified';
        
        $debug = [];
        $totalScore = 0;
        $totalCount = 0;
        foreach ($questions as $answer) {
            $q = Arr::get($answer, 'question');
            $answer_value = Arr::get($answer, 'value');

            $question = Question::with('custom_fields')
                            ->where('company_id', $job->company_id)
                            ->where('question', 'LIKE', '%'.$q.'%')
                            ->first();
                            
            if($question) {
                $answer = new JobApplicationAnswer();
                $answer->job_application_id = $jobApplication->id;
                $answer->job_id = $request->job_id;
                $answer->question_id = $question->id;
                $answer->company_id = $job->company_id;
                $answer->answer = $answer_value;
    
                if($question && $question->custom_fields->count() > 0) {
                    $custom_question = $question->custom_fields[0];
                    $question_value  = json_decode($custom_question->question_value, 1);
                    $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                    if(is_array($question_qualifiable)) {
                        $answer->score = 0;
                        $_total = count($question_qualifiable);
                        $_total = ($_total>0) ? 1 : $_total;
                        $totalCount += 1;
                        foreach($question_qualifiable as $q) {
                            if(isset($question_value[$q]) && in_array($question_value[$q], (array)$answer_value)) {
                                $answer->score += 1;
                                $totalScore += (1 / $_total);
                            }
                        }
                    }
                }
    
                $answer->save();
            }
        }

        $jobApplication->eligibility = ($totalScore / ($totalCount > 0 ? $totalCount : 1)) * 100;
        $jobApplication->save();

        if($jobApplication->eligibility < $job->eligibility_percentage) {
            $user_qualified = 'unqualified';
        }
        $reciever = User::where('company_id', '=', $jobApplication->company_id)->first();
        if($reciever->can_text){
            $to = "+1" . $reciever->mobile;
            $message = "You have received a new job application from {$jobApplication->full_name} for the " . ucwords($jobApplication->job->title) . " position.";
            SmsLog::reminder($to, $message, date('Y-m-d h:i:s'), $jobApplication->company_id, 'admin-alert', $jobApplication->id);
        }
        
        $stage = new AdminJobApplicationController();
    //    $stage->performStageAction($jobApplication);
        $stage->performStageAction($jobApplication, true);
        // $jobApplication->save();
        $debug = [];
        $get_url = false;
        if($job->workflow_id==0){
            $debug = $this->actionAfterApply($job, $jobApplication, $applicationStatus, $user_qualified);
        }else{
            $interview = \App\WorkflowStageActionInterview::where('stage_id', '=', $jobApplication->workflow_stage_id)->get();
            $get_url = $interview->count(); 
        }
        $url = ($user_qualified == 'qualified') || $get_url ? url("https://recruit.engyj.com/schedule-interview?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}") : null;
        return Reply::dataOnly(['status' => 'success', 'msg' => __('modules.front.applySuccessMsg'), 'debug' => $debug,'qualification'=>$jobApplication->qualified,'interview_link'=>$url]);
    }

    public function actionAfterApply($job, $jobApplication, $applicationStatus, $user_qualified)
    {
        $this->company_settings = Option::getAll($job->company_id);
        $debug[] = $this->company_settings;
        try {
            $users = User::frontAllAdmins($job->company_id);
            $settings = ApplicationSetting::where('company_id', $job->company_id)->first();
            
            // Notification::send($users, new NewJobApplication($jobApplication, $linkedin));
            if(array_key_exists($applicationStatus->id, $settings->mail_setting ?? []) && Arr::get($settings->mail_setting, $applicationStatus->id.'.status')) {
                // Mail::send(new ReceivedApplication($jobApplication));
                $subject =  Option::getOption('applicant_recieved_subject', $jobApplication->company_id);
                $mesg = Option::getOption('applicant_recieved_text', $jobApplication->company_id);
                if($jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                    $mesg = strtr($mesg, [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                }else{
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => '',
                    ]);
                    $mesg = strtr($mesg, [
                        '[interview_session_title]' => '',
                    ]);
                };
                $subject = strtr($subject ?? '', [
                    '[company_name]' => $jobApplication->company->company_name,
                ]);
                if($job->workflow_id == 0 && $mesg != ''){
                    $pre_message = strtr($mesg ?? '', [
                        '[applicant_name]'  => $jobApplication->full_name ?? '[applicant_name]',
                        '[applicant_email]'  => $jobApplication->email ?? '[applicant_email]',
                        '[employee_name]'   => $users[0]->name ?? '[employee_name]',
                        '[company_name]'    => $job->company->company_name ?? '[company_name]',
                        '[job_title]'       => $job->title ?? '[job_title]'
                    ]);
                    $data = array(
                        'from' => [
                                    'name' => Option::getOption('company_email_sender_name', $jobApplication->company_id), 
                                    'email' => Option::getOption('company_sender_email', $jobApplication->company_id)
                                ],
                        'to' => [$jobApplication->email],
                        'message' => $pre_message ,
                        'subject' => $subject
                    );
                    Mail::send(new \App\Mail\NewUserNotification($data));
                }
            }

            if(array_key_exists($applicationStatus->id, $settings->sms_setting ?? []) && Arr::get($settings->sms_setting, $applicationStatus->id.'.status')) {
                $mesg = Option::getOption('applicant_recieved_sms', $jobApplication->company_id);
                if($job->workflow_id == 0 && $mesg != ''){
                    $mesg = strtr($mesg ?? '', [
                        '[applicant_name]'  => $jobApplication->full_name ?? '[applicant_name]',
                        '[applicant_email]'  => $jobApplication->email ?? '[applicant_email]',
                        '[employee_name]'   => $users[0]->name ?? '[employee_name]',
                        '[company_name]'    => $job->company->company_name ?? '[company_name]',
                        '[job_title]'       => $job->title ?? '[job_title]'
                    ]);
                    $log = new \App\SmsLog();
                    $log->company_id = $jobApplication->company_id;
                    $log->message_type = 'outbound';
                    $log->type = 'normal';
                    $log->number = str_replace(' ', '',  $jobApplication->phone);
                    $log->message = $mesg;
                    $log->status = 'pending';
                    $log->send_at = date('Y-m-d h:i:s');
                    $log->reference_name = 'applicant-alert';
                    $log->reference_id = $jobApplication->id;
                    $log->save();   
                }
            }
            if($jobApplication->job->workflow_id == 0){
                try{
                    Notification::send($users[0], new AdminCandidateNotification($jobApplication, $user_qualified));
                } catch(\Exception $e) {}
            }

            if($user_qualified == 'qualified' && $jobApplication->job->workflow_id == 0) {
                $debug[] = $user_qualified;
                $url = route('candidate.schedule-inteview') . 
                        "?c={$job->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
                if(Arr::get($this->company_settings, 'qualify_auto_email_enabled') == 'enable') {
                    $email_text = Arr::get($this->company_settings, 'qualified_text', null);
                    if(!empty($email_text)) {
                        $email_text = replace_template($email_text, [
                            '[applicant_name]' => $jobApplication->full_name,
                            '[applicant_email]' => $jobApplication->email,
                            '[job_title]'       => $jobApplication->job->title,
                            '[company_name]'    => $jobApplication->job->company->company_name,
                            '[employee_name]'   => $users[0]->name,
                            '[interview_url]'   => $url,
                            '[schedule_date]'   => '',
                            '[schedule_time]'   => ''
                        ]);
                    }
                    Mail::send(new \App\Mail\MailQualification($jobApplication, $user_qualified, $email_text));
                    $informed = JobApplication::find($jobApplication->id);
                    $informed->is_auto_informed = 1;
                    $informed->save();
                }
                
                if(Arr::get($this->company_settings, 'qualify_auto_sms_enabled') == 'enable') {
                    $sms_text = Arr::get($this->company_settings, 'sms_qualified_text');
                    if($jobApplication->job->workflow_id != 0){
                        $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
                    }

                    $debug[] = $sms_text;
                    if(!empty($sms_text)) {
                        $message = replace_template($sms_text, [
                            '[applicant_name]' => $jobApplication->full_name,
                            '[applicant_email]' => $jobApplication->email,
                            '[job_title]'       => $jobApplication->job->title,
                            '[company_name]'    => $jobApplication->job->company->company_name,
                            '[employee_name]'   => $users[0]->name,
                            '[interview_url]'   => $url,
                            '[schedule_date]'   => '',
                            '[schedule_time]'   => '',
                        ]);
                        if($jobApplication->job->workflow_id != 0){
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => $interview_action->session_title,
                            ]);
                        }else{
                            $message = strtr($message ?? '', [
                                '[interview_session_title]' => '',
                            ]);
                        }
                    } else {
                        $message = "";
                        $message .= "Dear {$jobApplication->full_name},\n";
                        $message .= "This is {$users[0]->name} from {$this->global->company_name}\n";
                        $message .= "Thankyou for submitting your application. We are delighted and look forward to meetimg you. Please schedule your interview by clicking the link below:\n";
                        
                        $message .= "\n" . $url;
                    }
                    
                    $debug[] = $message;
                    $delay = Arr::get($this->company_settings, 'sms_time_delay', Arr::get(Option::defaultCompanyOptions(), 'sms_time_delay', 1));
                    $send_at = Carbon::now()->addMinutes($delay);
                    // if($delay==0){
                        $from = Option::getOption('twilio_from_number', $job->company_id, config('twilio-notification-channel.from'));
                        // Twilio::set($jobApplication->phone, $message, $from)->send();
                        $log = new \App\SmsLog();
                        $log->company_id = $jobApplication->company_id;
                        $log->message_type = 'outbound';
                        $log->type = 'normal';
                        $log->number = str_replace(' ', '',  $jobApplication->phone);
                        $log->message = $message;
                        $log->status = 'pending';
                        $log->send_at = date('Y-m-d h:i:s');
                        $log->reference_name = 'applicant-alert';
                        $log->reference_id = $jobApplication->id;
                        $log->save();
                        // SmsLog::reminder($jobApplication->phone, $message, $send_at, $job->company_id,null,null,'sent');
                    // }
                    // else{
                    //     SmsLog::reminder($jobApplication->phone, $message, $send_at, $job->company_id,null,null,'pending');
                    // }
                    $informed = JobApplication::find($jobApplication->id);
                    $informed->is_auto_informed = 1;
                    $informed->save();
                }
            }
        } catch(\Exception $e) { $debug[] = $e->getMessage() . ' Line: ' . $e->getLine(); }

        return $debug;
    }

    public function apiGetJob(Request $request, $id)
    {
        $job = Job::where('id', $id)
            // ->where(DB::raw('DATE(start_date)'), '<=', DB::raw('CURDATE()'))
            // ->where(DB::raw('DATE(end_date)'), '>=', DB::raw('CURDATE()'))
            ->with('questions')
            ->where('status', 'active')
            ->firstOrFail();
        
        if($request->js_form) {
            foreach($job->questions as $question) {
                echo "api_data.append('answer[{$question->id}]', '') // {$question->question} <br>";
            }

            exit();
        }
        
        return $job;
    }

    public function apiApplyJob(Request $request)
    {
        
    }

    public function fetchCountryState(Request $request)
    {
        $responseArr = [];

        $response = [
            "status" => "success", 
            "tp" => 1,
            "msg" => "Countries fetched successfully."
        ];

        switch ($request->type) {
            case 'getCountries':
                $countriesArray = json_decode(file_get_contents(public_path('country-state-city/countries.json')), true)['countries'];

                foreach ($countriesArray as $country) {
                    $responseArr = Arr::add($responseArr, $country['id'], $country['name']);
                }
            break;
            case 'getStates':
                $statesArray = json_decode(file_get_contents(public_path('country-state-city/states.json')), true)['states'];
                $countryId = $request->countryId;

                $filteredStates = array_filter($statesArray, function ($value) use ($countryId) {
                    return $value['country_id'] == $countryId;
                });

                foreach ($filteredStates as $state) {
                    $responseArr = Arr::add($responseArr, $state['id'], $state['name']);
                }
            break;
        }
        $response = Arr::add($response, "result", $responseArr);                

        return response()->json($response);
    }

    public function getName($arr, $id)
    {
        $result = array_filter($arr, function ($value) use ($id) {
            return $value['id'] == $id;
        });
        return current($result)['name'];
    }
}
