<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Trebol\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    public function scopeCompany($query, $company_id = null)
    {
        if(auth()->check())
            return $query->where('users.company_id', auth()->user()->company_id);
        else if(!is_null($company_id))
            return $query->where('users.company_id', $company_id);
        else
            return $query;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends =[
        'profile_image_url', 'mobile_with_code', 'formatted_mobile'
    ];

    protected $casts = [
        'profile_schedule' => 'array',
    ];

    protected $attributes = [
        'profile_schedule' => '{}'
    ];

    public function getProfileImageUrlAttribute(){
        if (is_null($this->image)) {
            return asset('avatar.png');
        }
        return asset_url('profile/'.$this->image);
    }

    public function role() {
        return $this->hasOne(RoleUser::class, 'user_id');
    }

    public function todoItems()
    {
        return $this->hasMany(TodoItem::class);
    }

    public static function allAdmins($exceptId = NULL, $forCompany = NULL)
    {
        $users = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.calling_code', 'users.mobile', 'users.mobile_verified', 'users.created_at')
            ->where('roles.name', 'admin')
            ->company($forCompany);

        if(!is_null($exceptId)){
            $users->where('users.id', '<>', $exceptId);
        }

        return $users->get();
    }

    public static function frontAllAdmins($companyId)
    {
        return User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'can_text', 'users.calling_code', 'users.mobile', 'users.mobile_verified', 'users.created_at')
            ->where('roles.name', 'admin')
            ->where('users.company_id', $companyId)
            ->get();
    }

    public function getMobileWithCodeAttribute()
    {
        if($this->can_text){
            return substr($this->calling_code, 1).$this->mobile;
        }else{
            return null;
        }
        
    }

    public function getFormattedMobileAttribute()
    {   
        if(!$this->can_text){
            return null;
        }
        if (!$this->calling_code) {
            return $this->mobile;
        }
        return $this->calling_code.'-'.$this->mobile;
    }

    public function routeNotificationForNexmo($notification)
    {
        if(!$this->can_text){
            return null;
        }
        return $this->mobile_with_code;
    }

    public function routeNotificationForTwilio()
    {
        if(!$this->can_text){
            return null;
        }
        return $this->mobile_with_code; // '+12258008941';
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function getTimeSlotDate()
    {
        for ($i = 1; $i <= 7; $i++) {
            $corbon = \Carbon\Carbon::now()->addDays($i);
            $get_future_dates[strtolower($corbon->format('l'))] = $corbon->format('Y-m-d');
        }
        $schedule_date = \Carbon\Carbon::now()->addDays(1)->format('Y-m-d');
        $profile_schedule = $this->profile_schedule;
        if($profile_schedule && is_array($profile_schedule) && count($profile_schedule) && $availability = $profile_schedule['global']['available_anytime']==0 && isset($profile_schedule['days']) && $profile_schedule['days']){
            foreach ($profile_schedule['days'] as $day => $value) {
                if(isset($value['active'])){
                    $schedule_date = $get_future_dates[strtolower($day)];
                    break;
                }
            }
        }
        return $schedule_date;
    }
}
