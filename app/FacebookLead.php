<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookLead extends Model
{
    public $fillable = [
        'company_id', 'lead_id', 'name', 'enabled',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    

}
