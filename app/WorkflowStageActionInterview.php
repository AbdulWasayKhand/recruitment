<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionInterview extends Model
{
    protected $fillable = [
        'action_id',
        'workflow_id',
        'stage_id',
        'session_title',
        'ask_candidate_to_schedule',
        'duration',
        'timeslot',
        'available_days',
        'employees',
        'interview_invite',
        'show_location',
        'email_invite', 
        'email_schedule', 
        'email_reschedule', 
        'email_cancelled', 
        'email_reminder',
        'sms_invite', 
        'sms_schedule', 
        'sms_reschedule', 
        'sms_cancelled', 
        'sms_reminder',
        'if_pass',
        'if_fail',
        'if_canceled',
        'same_day_schedule',
    ];
    public function interview_panel(){
        return $this->employees != 'null' && $this->employees != null ? \App\User::find(explode(',',str_replace(' ', '', str_replace('"','', str_replace(']', '', str_replace('[','',$this->employees)))))) : $this->workflow->company->users;
    }
    function workflow(){
        return $this->belongsTo('\App\Workflow', 'workflow_id');
    }
    function questionnaires(){
        return $this->hasMany(WorkflowStageActionInterviewQuestionnaire::class, 'action_id', 'action_id');
    }

    function if($status){
        return WorkflowStage::findOrNew($this->{"if_$status"});
    }
    // function template(){
    //     return $this->belongsTo(EmailTemplate::class, 'email_template_id');
    // }
    function sms_invite_template(){
        return $this->belongsTo(SmsTemplate::class, 'sms_invite');
    }
    function email_invite_template(){
        return $this->belongsTo(EmailTemplate::class, 'email_invite');
    }

}
