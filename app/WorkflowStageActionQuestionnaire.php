<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionQuestionnaire extends Model
{
    function questionnaire(){
        return $this->belongsTo(Questionnaire::class, 'questionnaire_id')->withDefault();
    }
    function template(){
        return $this->belongsTo(EmailTemplate::class, 'email_template_id');
    }
}
