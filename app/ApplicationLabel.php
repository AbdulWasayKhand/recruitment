<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationLabel extends Model
{
    public function labelTransition(){
        return $this->belongsToMany(LabelTransition::class);
    }
}
