<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Workflow extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check()) {
                $builder->where('workflows.company_id', user()->company_id);
            }
        });
    }

    function company(){
        return $this->belongsTo('\App\Company', 'company_id');
    }

    public function stages()
    {
        return $this->hasMany(WorkflowStage::class, 'workflow_id')->orderBy('position')->with('applications');
    }

    public function applied_stage()
    {
        return $this->hasOne(WorkflowStage::class, 'workflow_id')->where('type','applied')->withDefault();
    }

    public function hired_stage()
    {
        return $this->hasOne(WorkflowStage::class, 'workflow_id')->where('type','hired')->withDefault();
    }

    public function rejected_stage()
    {
        return $this->hasOne(WorkflowStage::class, 'workflow_id')->where('type','rejected')->withDefault();
    }

    public function orderdstages()
    {
        return $this->hasMany(WorkflowStage::class, 'workflow_id')->orderBy('position');
    }

    public function orderdStagesTwo(){
        $this->stages = \App\WorkflowStage::where('workflow_id',$this->id)->where(function($q){
            $q->where('position','>',-1)->orWhereNull('position');
        })->orderBy('position','asc')->get();
        return $this->stages = $this->stages->merge( \App\WorkflowStage::where('workflow_id',$this->id)->where('position','<',0)->get() );
    }

    public function saveDefaultStages()
    {
        foreach(['applied','hired','rejected'] as $title){
            $is_exist = WorkflowStage::where('workflow_id',$this->id)->where('title',$title);
            if($is_exist->count() == 0){
                switch ($title) {
                    case 'hired':
                        $position = -2;
                        $color    = 'green';
                    break;
                    case 'rejected':
                        $position = -1;
                        $color    = 'red';
                    break;
                    default:
                        $position = 0;
                        $color    = 'blue';
                    break;
                }
                WorkflowStage::create([
                    'workflow_id' => $this->id,
                    'title'       => ucFirst($title),
                    'color'       => $color,
                    'type'        => $title,
                    'position'    => $position,
                ]);
            }
        }
    }
}