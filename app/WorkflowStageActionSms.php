<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionSms extends Model
{
    protected $table ='workflow_stage_action_sms';
    function template(){
        return $this->belongsTo(SmsTemplate::class, 'sms_template_id')->withDefault();
    }
}
