<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionTrigger extends Model
{
    protected $fillable = ['action_id','trigger_id'];

    function trigger(){
        return $this->belongsTo('\App\WorkflowTrigger', 'trigger_id')->withDefault();
    }
    function stage() {
        return $this->belongsTo(\App\WorkflowStage::class, 'to_stage')->withDefault();
    }
    function conditions() {
        return $this->belongsTo(\App\WorkflowTriggerCondition::class, 'workflow_trigger_id')->withDefault();
    }
}