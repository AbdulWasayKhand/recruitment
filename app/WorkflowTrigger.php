<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class WorkflowTrigger extends Model
{
    protected $fillable = ['name','scope'];

    public function conditions() {
        return $this->hasMany('App\WorkflowTriggerCondition','workflow_trigger_id','id')->orderBy('id','desc');
    }
}