<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantHistory extends Model
{
    protected $table = 'applicant_history';
    public function company()
    {
        return $this->belongsTo(Company::class)->withDefault();
    }
    public function jobApplication(){
        return $this->belongsTo(JobApplication::class)->withDefault();
    }
    public function stage(){
        return $this->belongsTo(WorkflowStage::class,'current_value','id')->withDefault();
    }
}
