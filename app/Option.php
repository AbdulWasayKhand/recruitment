<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Option extends Model
{
    
    protected $fillable = [
        'option_name', 'option_value', 'company_id', 'user_id', 
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getOption($name, $company_id = null, $default = null, $user_id = null)
    {
        $query = static::where('option_name', $name)
                    ->where('company_id', $company_id);

        if(!is_null($user_id))
            $query = $query->where('user_id', $user_id);
        
        return Arr::get($query->first(), 'option_value') ?: ($default ?: Arr::get(self::defaultCompanyOptions(), $name));
    }

    public static function getAll($company_id, $user_id = null, $names = null, $default = [])
    {
        $result = static::where('company_id', $company_id);
        if(is_array($names)) {
            $result = $result->where('option_name', 'IN', $names);
        }
        $result = $result->where('user_id', $user_id);
        $result = $result->get()->pluck('option_value', 'option_name');

        if($result->count() > 0) {
            foreach (self::defaultCompanyOptions() as $key => $value) {
                if(!$result->has($key)) {
                    $result->put($key, $value);
                    continue;
                }
                if($result->has($key) && $result->get($key)=='') {
                    $result->put($key, $value);
                    continue;
                }
            }
            return $result;
        }

        return $default;
    }

    public static function setOption($name, $value, $company_id, $user_id = null)
    {
        $first = static::where('option_name', $name)
                    ->where('company_id', $company_id);

        if(!is_null($user_id))
            $first = $first->where('user_id', $user_id);

        if($first->exists()) {
            $first = $first->first();
            $query = static::where('option_name', $name)
                        ->where('company_id', $company_id);
            if(!is_null($user_id)) {
                $query = $query->where('user_id', $user_id);
            }
            return $query->update(['option_value' => ((is_array($value) || is_object($value)) ? json_encode($value) : $value)]);
        }
        
        return static::create([
            'option_name' => $name,
            'option_value' => ((is_array($value) || is_object($value)) ? json_encode($value) : $value),
            'company_id' => $company_id,
            'user_id' => $user_id,
        ]);
    }

    public static function destroyOption($name, $company_id, $user_id = null)
    {
        $query = static::where('option_name', $name)->where('company_id', $company_id);
        if(!is_null($user_id)) {
            $query = $query->where('user_id', $user_id);
        }

        $query->delete();
    }

    public static function defaultCompanyOptions()
    {
        return [
            'qualified_subject' => __('email.applicationQualification.subject'),
            'qualified_text' => "Dear [applicant_name],<br>This is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:<br>[interview_url]",
            'schedule_request_subject' => __('email.scheduleRequest.subject'),
            'schedule_request_text' => "Dear [employee_name], <br>You have got a new schedule request of [applicant_name] for [job_title] on [schedule_date] [schedule_time] <br>Please click here to accept [accept_url]",
            'interview_schedule_subject' => __('email.interviewSchedule.subject'),
            'interview_schedule_text' => "Dear [applicant_name],<br>This is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:<br>[interview_url]",
            'interview_reschedule_subject' => __('email.interviewReSchedule.subject'),
            'interview_reschedule_text' => "Dear [applicant_name],<br>This is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:<br>[interview_url]",
            'canceled_interview_subject' => __('email.applicationQualification.subject'),
            'canceled_interview_text' => "Dear [applicant_name],<br>This is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:<br>[interview_url]",
            'rejected_applicant_subject' => __('email.candidateRejected.subject'),
            'rejected_applicant_text' => "Hello [applicant_name],<br>We appreciate the time and effort you have taken in applying. We regret to inform you that your current qualifications do not match those needed for our current openings. We will retain your resume and if a position opens that closely matches your skill set, we will contact you. Thank you once again for your interest in employment at Home Care Assistance.",
            'hired_applicant_subject' => __('email.candidateHired.subject'),
            'hired_applicant_text' => "Hello [applicant_name],<br>We are delighted to inform you that you have met all requirements to be considered for the role.",
            'no_show_applicant_subject' => __('email.candidateNoShow.subject'),
            'no_show_applicant_text' => "Hello [applicant_name],<br>We are not pleased that you did not show up for the interview.",
            // 'status_change_subject' => __('email.candidateStatusUpdate.subject'),
            // 'status_change_text' => "Dear [applicant_name],<br>This is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:<br>[interview_url]",
            'sms_schedule_request_text' => "Dear [employee_name], You have got a new schedule request of [applicant_name] for [job_title] on [schedule_date] [schedule_time] Please click here to accept [accept_url]",
            'sms_qualified_text' => "Dear [applicant_name],\nThis is Silvia from [company_name].  We are delighted that you have applied for the caregiver position.  We look forward to meeting you. Kindly schedule your interview by clicking on my calendar below:\n[interview_url]",
            'sms_schedule_interview_text' => "Dear [applicant_name], your phone interview has been scheduled with [employee_name] at [company_name] on [schedule_date] from [schedule_time].",
            'sms_reschedule_interview_text' => "Dear [applicant_name], Your phone interview for the [job_title] position at [company_name] has been rescheduled to [schedule_date] from [schedule_time].",
            'sms_cancel_interview_text' => "Dear [applicant_name],Your phone interview at [company_name] has been [status_name].",
            'sms_rejected_applicant_text' => "Hello [applicant_name],We appreciate the time and effort you have taken in applying. We regret to inform you that your current qualifications do not match those needed for our current openings. We will retain your resume and if a position opens that closely matches your skill set, we will contact you. Thank you once again for your interest in employment at Home Care Assistance.",
            'sms_hired_applicant_text' => "Hello [applicant_name], We are delighted to inform you that you have met all requirements to be considered for the role.",
            'sms_no_show_applicant_text' => "Hello [applicant_name], We are not pleased that you did not show up for the interview.",
            // 'sms_status_change_text' => "Dear [applicant_name],Your phone interview at [company_name] has been [status_name].",
            'schedule_request_auto_email_enabled' => '0',
            'qualify_auto_email_enabled' => '0',
            'schedule_auto_email_enabled' => '0',
            'reschedule_auto_email_enabled' => '0',
            'cancel_auto_email_enabled' => '0',
            'rejected_auto_email_enabled' => '0',
            'hired_auto_email_enabled' => '0',
            'no_show_auto_email_enabled' => '0',
            // 'status_change_auto_email_enabled' => '0',
            'schedule_request_auto_sms_enabled' => '0',
            'qualify_auto_sms_enabled' => '0',
            'schedule_auto_sms_enabled' => '0',
            'reschedule_auto_sms_enabled' => '0',
            'cancel_auto_sms_enabled' => '0',
            'rejected_auto_sms_enabled' => '0',
            'hired_auto_sms_enabled' => '0',
            'no_show_auto_sms_enabled' => '0',
            // 'status_change_auto_sms_enabled' => '0',
            'sms_auto_reminder_hour' => 'disable',
            'sms_auto_reminder_day' => 'disable',
            'sms_reminder_text' => "Dear [applicant_name], this is a reminder for your interview with [employee_name] at [company_name] on [schedule_date] from [schedule_time]",
            'sms_time_delay' => 1,
            'thanks_page_url' => '',
            'company_email_sender_name' => 'Recruitment',
            'company_sender_email' => 'info@engyj.com',
            'company_multi_email' => implode(",",User::where('company_id', company()->id ?? 0)->pluck('email')->toArray()),
            'profile_online_schedule' => 1,
            'twilio_from_number' => config('twilio-notification-channel.from'),
            'applicant_recieved_subject' => '',
            'applicant_recieved_text' => '',
            'applicant_recieved_sms' => '',
        ];
    }

}
