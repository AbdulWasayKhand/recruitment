<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDescriptionTemplate extends Model
{
    protected $casts = [
        'meta_data' => 'array',
        'content' => 'array',
    ];

    protected $attributes = [
        'meta_data' => '{}',
        'content' => '{}',
    ];

    protected $fillable = [
        'company_id', 'title', 'content', 'meta_data'
    ];

    
}
