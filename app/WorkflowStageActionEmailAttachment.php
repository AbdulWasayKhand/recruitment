<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionEmailAttachment extends Model
{
    protected $fillable = ['action_id','stage_id','workflow_id','email_template_id','from','title','no_of_attachments'];

    function template(){
        return $this->belongsTo(EmailTemplate::class, 'email_template_id')->withDefault();
    }

    function attachments(){
        return $this->hasMany('\App\Document', 'action_id','id');
    }

    function applicantAttachmentByFileName($applicant_id,$name){
        return $this->attachments->where('documentable_id',$applicant_id)->where('name',$name)->first() ?? new \App\Document;
    }

    public function getTitleAttribute($value)
    {
        return $value ? explode(',',$value) : [];
    }
}