<?php

namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\JobApplication;

class Job extends Model
{
    use Sluggable;

    protected $dates = ['end_date', 'start_date'];

    protected $casts = [
        'required_columns' => 'array',
        'meta_details' => 'array',
        'section_visibility' => 'array'
    ];

    protected $appends = [
        'active'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('jobs.company_id', user()->company_id);
            }
        });
    }
    public function workflowapplications()
    {
        return $this->hasMany(JobApplication::class)->orderBy('id', 'desc');
    }

    public function applications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function category()
    {
        return $this->belongsTo(JobCategory::class, 'category_id');
    }

    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class, 'questionnaire_id')->withDefault();
    }

    public function location()
    {
        return $this->belongsTo(JobLocation::class, 'location_id')->withDefault();
    }

    public function locations()
    {
        if(strpos($this->location_id, '[') !== false){
            $location_ids = explode(',', str_replace('"', '', str_replace(']', '', str_replace('[', '', $this->location_id))));
            $locations = \App\JobLocation::whereIn('id',$location_ids)->get();
        }
        else{
            $locations = \App\JobLocation::whereIn('id',[$this->location_id])->get();
        }
        return $locations;
    }

    public function skills()
    {
        return $this->hasMany(JobSkill::class, 'job_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['title', 'location.location']
            ]
        ];
    }

    public static function activeJobs()
    {
        return Job::where('status', 'active')
            ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'))
            ->get();
    }

    public static function frontActiveJobs($companyId)
    {
        return Job::withoutGlobalScope('company')
            ->where('status', 'active')
            ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'))
            ->where('company_id', $companyId)
            ->get();
    }

    public static function activeJobsCount()
    {
        return Job::where('status', 'active')
            ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'))
            ->count();
    }

    public function getActiveAttribute()
    {
        return $this->status === 'active' && $this->start_date->lessThanOrEqualTo(Carbon::now()) && $this->end_date->greaterThanOrEqualTo(Carbon::now());
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'job_questions')->with('custom_fields', 'category')->orderBy('questions.sort_order','asc');
    }

    public function budget()
    {
        return $this->hasOne(Budget::class)->withDefault();
    }

    public function workflow()
    {
        return $this->belongsTo(Workflow::class, 'workflow_id')->withDefault();
    }

    public function getDefaultWorkflowViewJobAttribute()
    {
        $option = Option::where('option_name', 'default_workflow_view_job')
                        ->where('option_value', $this->id)
                        ->where('company_id', $this->company_id)->first() ?? new Option;
        return $option->option_value;
    }

    public function getApplicants($start_date,$end_date,$flag, $job_id)
    {
        $qry = JobApplication::where('job_id', '=', $job_id)->get();
        $qry =  $qry->filter(function ($value, $key) use($start_date,$end_date) {
                return $value->job->start_date >= $start_date && $value->job->end_date <= $end_date;
        });
        $qry =  $qry->filter(function ($value, $key) use($flag) {
                    if($flag == 'qualified'){
                        return $value->eligibility >= $value->job->eligibility_percentage;
                    }
                    return $value->eligibility < $value->job->eligibility_percentage;
                });
        return $qry->count();
    }
}
