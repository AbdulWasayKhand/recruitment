<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model
{
    protected $fillable = [
        'company_id', 'message_type', 'type', 'number', 'message', 'status', 'send_at', 'reference_name', 'reference_id', 'provider_response',
    ];

    protected $casts = [
        'send_at' => 'datetime'
    ];

    public static function reminder($number, $message, $send_at, $company_id, $ref_name = null, $ref_id = null,$status='pending')
    {
        return self::create([
            'company_id' => $company_id,
            'number'  => $number,
            'message' => $message,
            'type'    => 'reminder',
            'status'  => $status,
            'send_at' => $send_at,
            'reference_name' => $ref_name,
            'reference_id' => $ref_id,
        ]);
    }

    public function scopeRef($query, $ref, $id)
    {
        return $query->where('reference_name', $ref)->where('reference_id', $id);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

}
