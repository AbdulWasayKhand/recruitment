<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookPage extends Model
{
    
    public $fillable = [
        'company_id', 'facebook_id', 'name', 'access_token',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

}
