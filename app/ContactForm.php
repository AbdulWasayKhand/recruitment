<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ContactForm extends Model
{
    protected $fillable = ['company_id','name','email','message'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('contact_forms.company_id', user()->company_id);
            }
        });
    }

    public function company(){
        return $this->belongsTo('App\Company')->withDefault();
    }
}
