<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GcUpdate extends Model
{
    protected $fillable = ['company_id', 'user_id', 'refe_id', 'ref_type', 'eid', 'state'];

    public function referance()
    {
        switch ($this->ref_type) {
            case 'holiday':
                return $this->belongsTo('\App\Holiday', 'refe_id','id')->withDefault();
            break;
            case 'application':
                return $this->belongsTo('\App\JobApplication', 'refe_id','id')->withDefault();
            break;
        }
    }
}
