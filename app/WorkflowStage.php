<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class WorkflowStage extends Model
{
    protected $fillable = ['position','workflow_id','title','color','type'];
    public function applications()
    {
        return $this->hasMany(JobApplication::class, 'workflow_stage_id');
    }

    public function workflow()
    {
        return $this->belongsTo(Workflow::class, 'workflow_id');
    }

    public function interview_actions()
    {
        return $this->hasMany(WorkflowStageAction::class, 'stage_id')->where('type','interview');
    }

    public function interview_action()
    {
        return $this->hasOne(WorkflowStageActionInterview::class, 'stage_id', 'id')->withDefault();
    }

    public function email_actions()
    {
        return $this->hasMany(WorkflowStageAction::class, 'stage_id')->where('type','email');
    }

    public function sms_actions()
    {
        return $this->hasMany(WorkflowStageAction::class, 'stage_id')->where('type','sms');
    }
    
    function change_stage_action(){
        return $this->hasMany(WorkflowStageAction::class, 'stage_id')->where('type','change_stage')->first() ?? new WorkflowStageAction;
    }

    function email_attachments(){
        return $this->hasMany('\App\WorkflowStageActionEmailAttachment', 'stage_id', 'id');
    }

    public function children(){
        return workflowStage::where('after_stage',$this->id)->get();
    }

    public function workflow_actions($action){
        switch($action){
            case 'interview' :
                return WorkflowStageActionInterview::where('stage_id','=', $this->id)->get();
            break;
            case 'email' :
                return WorkflowStageActionEmail::where('stage_id','=', $this->id)->get();
            break;
            case 'sms' :
                return WorkflowStageActionSms::where('stage_id','=', $this->id)->get();
            break;
            case 'document' :
                return WorkflowStageActionEmailAttachment::where('stage_id','=', $this->id)->get();
            break;
            case 'trigger' :
                return WorkflowStageActionTrigger::where('stage_id','=', $this->id)->get();
            break;
            case 'questionnaire' :
                return WorkflowStageActionQuestionnaire::where('stage_id','=', $this->id)->get();
            break;
        }
    }
}