<?php
namespace App\Helper;

use Auth;
use App\User;
use App\Company;

class FBApi
{
    public $endpoint = 'https://graph.facebook.com/';
    public $version = 'v8.0';

    public $user_id;
    public $page_id;
    public $access_token;
    protected $lead_id;

    public function __construct($accessToken = NULL) {
        if(!is_null($accessToken)) {
            $this->access_token = $accessToken;
        }

    }
    

    public function getLeads()
    {
        return $this->request("/{$this->page_id}/leadgen_forms");
    }

    public function getLeadApplicants($lead_id, $after = null)
    {
        return $this->request("/{$lead_id}/leads", ['after' => $after]);
    }

    public function getPages($after = null)
    {
        return $this->request("/{$this->user_id}/accounts", ['fields' => 'id,name,category,access_token', 'after' => $after]);
    }

    public function currentUser()
    {
        $company_id = Auth::user()->company_id ?? null;
        $company = Company::with('facebook')->findOrFail($company_id);
        
        abort_if(is_null($company->facebook), 404);

        $this->page_id = $company->facebook->facebook_id;
        $this->access_token = $company->facebook->access_token;

        return $this;
    }

    public function setLeadId($lead_id)
    {
        $this->lead_id = $lead_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function setAccessToken($accessToken)
    {
        $this->access_token = $accessToken;

        return $this;
    }

    public function request($path, $params = array())
    {
        $url  = $this->endpoint . $this->version . '/';
        $url .= $path;
        $params['access_token'] = $this->access_token;
        $url .= '?' . http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $response = json_decode($server_output, true);
        curl_close($ch);

        return $response;
    }

}
