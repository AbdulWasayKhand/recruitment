<?php
namespace App\Helper;

use App\Traits\SmsSettings;
use Twilio\Rest\Client;
use Illuminate\Support\Arr;


/**
 * Twilio SMS Helper
 */
class TwilioHelper
{
    use SmsSettings;
    protected $to;
    protected $from;
    protected $message;

    function __construct($to, $message, $from = null) {
        $this->to = $to;
        $this->from = $from;
        $this->message = $message;

        $this->setSmsConfigs();
    }

    public static function set($to, $message, $from = null)
    {
        return new TwilioHelper($to, $message, $from);
    }

    public function send()
    {

        if(is_null($this->to) || is_null($this->message) || $_SERVER['SERVER_NAME'] == 'localhost') {
            return [$this->to, $this->message];
        }

        $from = config('twilio-notification-channel.from', '+19733545682');
        if(!is_null($this->from)) {
            $from = $this->from;
        }
        
        $status = true;
        try {
            $twilio = new Client(env('TWILIO_ACCOUNT_SID', 'AC1e5f0f52f4b59c4d66318462966514fd'), env('TWILIO_AUTH_TOKEN', '3552d1a7fe28bacc315ecef36ada0003'));
            $send = $twilio->messages->create($this->to, [
                'body' => $this->message,
                'from' => $from,
            ]);
        } catch (\Exception $e) {
            $status = false;
            $send = substr($e->getMessage(), 0, 100);
        }

        return ['message' => $send, 'status' => $status];
    }

    public function read(array $options = array())
    {
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            return collect([]);
        }
        $from = config('twilio-notification-channel.from', '+19733545682');
        if (!is_null($this->from)) {
            $from = $this->from;
        }
        $options = [];
        $options['from'] = $from;
        
        $to = Arr::get($options, 'to');
        $limit = Arr::get($options, 'limit', 20);
        if(!is_null($to)) {
            $options['to'] = $to;
        }
        $twilio = new Client(config('twilio-notification-channel.account_sid', 'AC1e5f0f52f4b59c4d66318462966514fd'), config('twilio-notification-channel.auth_token', '3552d1a7fe28bacc315ecef36ada0003'));
        $_messages = $twilio->messages
                        ->read(
                          $options,
                          $limit
                        );
        $messages = [];
        foreach ($_messages as $record) {
            $messages[] = $record->toArray();
        }
        
        return collect($messages);
    }

    public function fetch($sid)
    {
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            return [];
        }
        $status = false;
        try {
            $twilio = new Client(env('TWILIO_ACCOUNT_SID', 'AC1e5f0f52f4b59c4d66318462966514fd'), env('TWILIO_AUTH_TOKEN', '3552d1a7fe28bacc315ecef36ada0003'));
            $send = $twilio->messages($sid)->fetch();

            return $send->toArray();
        } catch (\Exception $e) {
            $status = false;
        }

        return $send;
    }
}
