<?php
use App\Company;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Carbon\Carbon;

if (!function_exists('superAdmin')) {
    function superAdmin()
    {
        return auth()->user();
    }
}
if (!function_exists('company')) {
    function company()
    {
        if(auth()->user()) {
            $company = Company::find(auth()->user()->company_id);
            return $company;
        }

        return null;
    }
}

if (!function_exists('user')) {
    function user()
    {
        if(auth()->check()) {
            return auth()->user();
        }
        // return null;
    }
}

if (!function_exists('asset_url')) {

    // @codingStandardsIgnoreLine
    function asset_url($path)
    {
        $path = 'user-uploads/' . $path;
        $storageUrl = $path;

        if (!Str::startsWith($storageUrl, 'http')) {
            return url($storageUrl);
        }

        return $storageUrl;

    }

}
if (!function_exists('check_migrate_status')) {

    function check_migrate_status()
    {
        if (!session()->has('check_migrate_status')) {

            $status = Artisan::call('migrate:check');

            if ($status && !request()->ajax()) {
                Artisan::call('migrate', array('--force' => true)); //migrate database
                Artisan::call('optimize:clear');
            }
            session(['check_migrate_status' => true]);
        }

        return session('check_migrate_status');
    }
}

if (!function_exists('module_enabled')) {
    function module_enabled($moduleName)
    {
        return \Nwidart\Modules\Facades\Module::collections()->has($moduleName);
    }
}

if (!function_exists('getDomainSpecificUrl')) {
    function getDomainSpecificUrl($url, $company = false)
    {
        if (module_enabled('Subdomain')) {
            // If company specific

            if ($company) {
                $url = str_replace(request()->getHost(), $company->sub_domain, $url);
                $url = str_replace('www.', '', $url);
                // Replace https to http for sub-domain to
                return $url = str_replace('https', 'http', $url);
            }

            // If there is no company and url has login means
            // New superadmin is created
            return $url = str_replace('login', 'super-admin-login', $url);
        }

        return $url;
    }
}

if (!function_exists('get_domain')) {

    function get_domain($host=false)
    {
        if(!$host){
            $host = $_SERVER['SERVER_NAME'];
        }
        $myhost = strtolower(trim($host));
        $count = substr_count($myhost, '.');
        if ($count === 2) {
            if (strlen(explode('.', $myhost)[1]) >= 3) $myhost = explode('.', $myhost, 2)[1];
        } else if ($count > 2) {
            $myhost = get_domain(explode('.', $myhost, 2)[1]);
        }
        return $myhost;
    }
}

if (!function_exists('recruit_plugins')) {

    function recruit_plugins()
    {

        if (!session()->has('recruit_plugins')) {
            $plugins = \Nwidart\Modules\Facades\Module::allEnabled();
            // dd(array_keys($plugins));

            // foreach ($plugins as $plugin) {
            //     Artisan::call('module:migrate', array($plugin, '--force' => true));
            // }

            session(['recruit_plugins' => array_keys($plugins)]);
        }
        return session('recruit_plugins');
    }
}

if (!function_exists('replace_template')) {
    function replace_template($content, $placeholders)
    {
        return strtr($content, $placeholders);
    }
}

function saveTimeline($data){
    $applicantTimeline = new \App\ApplicantTimeline;
    $applicantTimeline->fill($data);
    $applicantTimeline->save();
    return $applicantTimeline;
}

function get_timezone_list($time_zone='') {
    $timezoneList = array(
        'Pacific/Tarawa' => 'Tarawa ',
        'Pacific/Auckland' => 'New Zealand Time ',
        'Pacific/Norfolk' => 'Norfolk Island (Austl.) ',
        'Pacific/Noumea' => 'Noumea, New Caledonia ',
        'Australia/Sydney' => 'Australian Eastern Time (Sydney) ',
        'Australia/Brisbane' => 'Australian Eastern Time (Queensland) ',
        'Australia/Adelaide' => 'Australian Central Time (Adelaide) ',
        'Australia/Darwin' => 'Australian Central Time (Northern Territory) ',
        'Asia/Tokyo' => 'Tokyo ',
        'Australia/Perth' => 'Australian Western Time ',
        'Asia/Hong_Kong' => 'Hong Kong ',
        'Asia/Bangkok' => 'Thailand (Bangkok) ',
        'Asia/Jakarta' => 'Western Indonesian Time (Jakarta) ',
        'Asia/Dhaka' => 'Bangladesh (Dhaka) ',
        'Asia/Kolkata' => 'India ',
        'Asia/Kabul' => 'Afghanistan (Kabul) ',
        'Asia/Tashkent' => 'Uzbekistan (Tashkent) ',
        'Asia/Dubai' => 'UAE (Dubai) ',
        'Europe/Moscow' => 'Moscow ',
        'Asia/Tehran' => 'Tehran ',
        'Africa/Djibouti' => 'Djibouti ',
        'Europe/Minsk' => 'Minsk ',
        'Cairo' => 'Cairo ',
        'Europe/Paris' => 'Eastern European Time ',
        'Europe/Dublin' => 'Central European Time ',
        'Europe/Lisbon' => 'Western European Time (Lisbon) ',
        'Europe/London' => 'British Time (London) ',
        'GMT' => 'Greenwich Mean Time ',
        'Atlantic/Azores' => 'Azores',
        'America/Sao_Paulo' => 'Eastern Brazil ',
        'Atlantic/South_Georgia' => 'Central Greenland Time ',
        'America/Godthab' => 'Western Greenland Time ',
        'America/St_Johns' => 'Newfoundland Time ',
        'America/Argentina/Buenos_Aires' => 'Buenos Aires ',
        'Atlantic/Bermuda' => 'Atlantic Time (Bermuda) ',
        'America/Port_of_Spain' => 'Atlantic Time ',
        'America/New_York' => 'Eastern Time ',
        'America/Indiana/Indianapolis' => 'Eastern Time (Indiana) ',
        'America/Chicago' => 'Central Time ',
        'America/Mexico_City' => 'Central Time (Mexico City, Monterrey) ',
        'America/Regina' => 'Central Time (Saskatchewan) ',
        'America/Denver' => 'Mountain Daylight Time ',
        'America/Phoenix' => 'Mountain Standard Time ',
        'America/Los_Angeles' => 'Pacific Time ',
        'America/Anchorage' => 'Alaska Time ',
        'Pacific/Honolulu' => 'Hawaiian/Aleutian Time ',
        'Pacific/Midway' => 'Midway Island',
        'Kwajalein' => 'International Date Line West'
    );

    if(!empty($time_zone)){
        if($timezoneList[$time_zone]){
            return $timezoneList[$time_zone];    
        }else{
            return 'Not Available';
        }
    }else{
        return $timezoneList;
    }
}

function blockCompleteDay(array $date_slots=[],$userIds){
    $has_dates = \App\CustomEvent::where('block_complete_day',1)->whereIn('user_id',$userIds);
    if(count($date_slots)){
        $has_dates = $has_dates->whereIn(\DB::raw("DATE(date)"),$date_slots);
    }
    return $has_dates->count() ? array_merge($date_slots,$has_dates->pluck('date')->toArray()) : [];
}

function blockTimeSlots($date,array $time_slots,$interview_slot_gap,$userIds){
    $custom_events_slots = \App\CustomEvent::whereDate('date',$date)->where('block_complete_day',0)->whereIn('user_id',$userIds)->pluck('time_range')->toArray();
    $new_slots = [];
    foreach($custom_events_slots as $slot){
        $slot = explode('-',$slot);
        $time_start = date('Y-m-d H:i', strtotime($slot[0]));
        $time_end = date('Y-m-d H:i', strtotime($slot[1]));
        while ($time_start < $time_end) {
            $temp_time = date('Y-m-d H:i', strtotime("+{$interview_slot_gap} minutes", strtotime($time_start)));
            $time_slot = date('h:i A', strtotime($time_start)) . " - " . date('h:i A', strtotime($temp_time));
            $new_slots[] = $time_slot;
            $time_start = $temp_time;
        }
    }
    return $new_slots;
}

function createSlots($start_time, $end_time, $gap){
    $new_slots = [];
    $time_start = date('Y-m-d H:i', strtotime($start_time));
    $time_end = date('Y-m-d H:i', strtotime($end_time));
    while ($time_start < $time_end) {
        $temp_time = date('Y-m-d H:i', strtotime("+{$gap} minutes", strtotime($time_start)));
        $time_slot = date('h:i A', strtotime($time_start)) . " - " . date('h:i A', strtotime($temp_time));
        $new_slots[] = $time_slot;
        $time_start = $temp_time;
    }
    return $new_slots;
}

function triggerOperators($metric){
    $metric = strtolower($metric);
    switch ($metric) {    
        default:
            return [
                'Is equals to',
                'Does not equals to',
                // 'contains',
                // 'not contains',
                // 'starts with',
                // 'end with',
                'Is greater than',
                'Is less than',
                'Is greater than or equals to',
                'Is less than or equals to',
                // 'not starts with',
                // 'not end with'
            ];
            break;
    }    
}

function getTriggerOperator($operator, $value_one,$value_two){
    $operator = strtolower($operator);
    switch (strtolower($operator)) {
        case 'is equals to':
            return ($value_one == $value_two);
        break;
        case 'does not equals to':
            return ($value_one != $value_two);
        break;
        case 'contains':
            return (strpos($value_one,$value_two) >= 0);
        break;
        case 'not contains':
            return (!strpos($value_one,$value_two));
        break;
        case 'starts with':
            return (strpos($value_one,$value_two)==0);
        break;
        case 'is greater than':
            return ($value_one > $value_two);
        break;
        case 'is less than or equals to':
            return ($value_one < $value_two);
        break;
        case 'is greater than or equals to':
            return ($value_one >= $value_two);
        break;
        case 'is less than':
            return ($value_one <= $value_two);
        break;
        case 'not starts with':
            return (0);
        break;
        case 'not end with':
            return ( strpos($value_one,$value_two) == (strlen($value_one) - strlen($value_two)) );
        break;
    }    
}

function calander_api(){
    
}

function blockHolidays(array $date_slots,$companyIds){
    $holidays = \App\Holiday::whereIn('company_id',$companyIds)->get();
    $block_dates = [];
    foreach ($holidays as $holiday) {
        for($slot = date($holiday->from); $slot <= date($holiday->to); $slot++){
            $block_dates[] = $slot;
        }
    }
    if($holidays->count()){
        return (count($date_slots)>1) ? array_unique(array_merge($date_slots,$block_dates)) : $block_dates;
    }
    else{
        return (count($date_slots)>1) ? $date_slots : [];
    }
}

function upComingHolidays(){ 
    return \App\Holiday::whereDate('from', '>=' ,Carbon::now(auth()->user()->company->timezone)->format('Y-m-d'))
    ->whereIn('company_id',[auth()->user()->company_id])->get();
}

function totalUpComingScheduleRequests($user_id, $company_id){
    return count(\DB::SELECT("SELECT * FROM `job_applications` WHERE `id` IN (SELECT `job_application_id` FROM `interview_schedules` WHERE `id` IN (SELECT `interview_schedule_id` FROM `interview_schedule_employees` WHERE `company_id` = " . $company_id . " AND `user_id` = " . $user_id . " AND `user_accept_status` = 'waiting' AND `interview_schedule_id` NOT IN ( SELECT `interview_schedule_id` FROM `interview_schedule_employees` WHERE `interview_schedule_id` IN (SELECT `interview_schedule_id` FROM `interview_schedule_employees` WHERE `company_id` = " . $company_id . " AND `user_id` = " . $user_id . " AND `user_accept_status` = 'waiting') AND `user_accept_status` != 'waiting' GROUP BY `interview_schedule_id` ))) AND `deleted_at` IS NULL"));
    // \App\InterviewScheduleEmployee::where('user_id', '=', $user_id)->where('user_accept_status', '=', 'waiting')->groupBy('interview_schedule_id')->get()->count();
}

function upComingScheduleRequests(){
    $schedules = \App\InterviewSchedule::with('employees')->where('status', '=', 'pending')
                ->whereDate('schedule_date', '>=' ,Carbon::now(auth()->user()->company->timezone)->format('Y-m-d'))
                ->whereNull('user_accept_status')
                ->orderBy('schedule_date')
                ->get();
    $schedules = $schedules->filter(function($value){
        return !empty($value->jobApplication->id);
    });
    return $schedules;
}

function createGoogleEvent($title, $desc, $start, $end, $user)
{
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = 'http://localhost/recruitment/public/oauth2callback';
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken($user->GC_refresh_token);
    $client->getAccessToken();
    $service = new Google_Service_Calendar($client);
        $event = new Google_Service_Calendar_Event(array(
        'summary' => $title,
        'description' => $desc,
        'start' => array(
            'dateTime' => $start ,
            'timeZone' => $user->company->timezone,
        ),
        'end' => array(
            'dateTime' => $end ,
            'timeZone' => $user->company->timezone,
        ),
        'reminders' => array(
            'useDefault' => FALSE,
            'overrides' => array(
            array('method' => 'email', 'minutes' => 24 * 60),
            array('method' => 'popup', 'minutes' => 10),
            ),
        ),
        ));
        
        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);
        $calander = new \App\calendar_log();
        $calander->company_id = $user->company_id;
        $calander->user_id = $user->id;
        $calendar_id = $event->id;
        $calander->eid = $calendar_id;
        $calander->cid = explode("?eid=", $event->htmlLink)[1];
        $calander->created_at = $event->created;
        $calander->interview_start = $start;
        $calander->interview_end = $end;
        $calander->save();     
        return $event->id;
}

function cancleGC($user, $eid){
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = url('oauth2callback');
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken($user->GC_refresh_token);
    $clint_json = $client->getAccessToken();
    if($clint_json){
        $service = new Google_Service_Calendar($client);
        $check = $service->events->get('primary', $eid);
        if($check->status != 'cancelled'){
            $calendarId = 'primary';
            $event = $service->events->delete($calendarId, $eid);
        }
    }
}

function inactiveEvent($eid){
    $update = \App\GcUpdate::where('eid', "=", $eid)->first();
    if($update){
        $update->state = 'inactive';
        $update->save();
    }
}

function gc_update($data){
    $update = new \App\GcUpdate;
    $update->fill($data);
    $update->save();
    return $update;
}

function hasToAccept($schedule_id,$user_id){
    return \App\InterviewScheduleEmployee::where("interview_schedule_id", $schedule_id)
            ->where("user_id", $user_id)->first()->id ?? 0;
}


function sendReminder($jobApplication, $user, $interviewSchedule, $dateTime){
    $allPending = \App\SmsLog::where('reference_id', $jobApplication->id)->where('status','pending')->get();
    if($allPending->count()){
        foreach($allPending as $pending){
            if(in_array($pending->reference_name, ['applicant-reminder-alert', 'admin-reminder-alert'])){
                $pending->status = 'canceled';
                $pending->save();
            }
        }
    }
    $company_settings = \App\Option::getAll($jobApplication->company_id);
    $interview_url = route('candidate.schedule-inteview') .
                "?c={$jobApplication->company_id}&app={$jobApplication->id}&t={$jobApplication->email_token}";
    $message = "Dear {$jobApplication->full_name}, this is a reminder for your interview with {$user->name} at {$jobApplication->job->company->company_name} on ". $interviewSchedule->schedule_date->format('M d, Y') ." from {$interviewSchedule->slot_timing}";

    $has_message = \App\Option::getOption('sms_reminder_text', $jobApplication->company_id);
    if($jobApplication->job->workflow_id != 0){
        $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$jobApplication->workflow_stage_id)->first();
        if($interview_action->sms_reminder != 0){
            $has_message = \App\SmsTemplate::find($interview_action->sms_reminder)->content;
        }
    }
    if($has_message){
        $message = strtr($has_message, [
            '[applicant_name]'  => $jobApplication->full_name,
            '[applicant_email]' => $jobApplication->email,
            '[employee_name]'   => $user->name,
            '[company_name]' => $jobApplication->job->company->company_name,
            '[interview_url]'   => $interview_url,
            '[schedule_date]' => $interviewSchedule->schedule_date->format('M d, Y'),
            '[schedule_time]' => $interviewSchedule->slot_timing,
            '[job_title]' => $jobApplication->job->title,
        ]);
        if($jobApplication->job->workflow_id != 0){
            $message = strtr($message ?? '', [
                '[interview_session_title]' => $interview_action->session_title,
            ]);
        }else{
            $message = strtr($message ?? '', [
                '[interview_session_title]' => '',
            ]);
        }
    }
    $reminder_hour = \Illuminate\Support\Arr::get($company_settings, 'sms_auto_reminder_hour');
    if($reminder_hour != 'disable' && is_numeric($reminder_hour)) {
        $main_hour = $dateTime->copy();
        $send_at = $main_hour->subHours($reminder_hour);
        \App\SmsLog::reminder($jobApplication->phone, $message, $send_at, $jobApplication->company_id, 'applicant-reminder-alert', $jobApplication->id);
    }

    $reminder_day = \Illuminate\Support\Arr::get($company_settings, 'sms_auto_reminder_day');
    if($reminder_day != 'disable' && is_numeric($reminder_day)) {
        $main_day = $dateTime->copy();
        $send_at = $main_day->subDays($reminder_day);
        if($send_at->lessThan($dateTime)) {
            \App\SmsLog::reminder($jobApplication->phone, $message, $send_at, $jobApplication->company_id, 'applicant-reminder-alert', $jobApplication->id);
        }
    }

    if($user->can_text){
        // Hourly
        if($user->sms_auto_reminder_hour != 'disable'){
            $main_hour = $dateTime->copy();
            $send_at = $main_hour->subHours($user->sms_auto_reminder_hour);
            $log = new \App\SmsLog;
            $log->company_id = $jobApplication->company_id;
            $log->message_type = 'outbound';
            $log->type = 'reminder';
            $log->number = trim($user->calling_code . $user->mobile);
            $log->message = "This is a reminder for your interview with {$jobApplication->full_name} on {$interviewSchedule->schedule_date->format('M d, Y')} from {$interviewSchedule->slot_timing}". ($interviewSchedule->location->location_name ? "\nLocation: {$interviewSchedule->location->location_name}" : "");
            $log->status = 'pending';
            $log->send_at = $send_at->format('Y-m-d h:i:s');
            $log->reference_name = 'admin-reminder-alert';
            $log->reference_id = $jobApplication->id;
            $log->save();
        }
        // Daywise
        if($user->sms_auto_reminder_day != 'disable'){
            $main_day = $dateTime->copy();
            $send_at = $main_day->subDays($user->sms_auto_reminder_day);
            $log = new \App\SmsLog;
            $log->company_id = $jobApplication->company_id;
            $log->message_type = 'outbound';
            $log->type = 'reminder';
            $log->number = trim($user->calling_code . $user->mobile);
            $log->message = "This is a reminder for your interview with {$jobApplication->full_name} on {$interviewSchedule->schedule_date->format('M d, Y')} from {$interviewSchedule->slot_timing}". ($interviewSchedule->location->location_name ? "\nLocation: {$interviewSchedule->location->location_name}" : "");
            $log->status = 'pending';
            $log->send_at = $send_at->format('Y-m-d h:i:s');
            $log->reference_name = 'admin-reminder-alert'; 
            $log->reference_id = $jobApplication->id;              
            if($send_at->lessThan($dateTime)) {
                $log->save();
            }
        }
    }
}
function getGCEvents($user){
    $base_path = public_path('assets/GC_USERS').'/c_' . $user->company_id . '_u_'.$user->id . '.cache';
    if(file_exists($base_path)){
        return json_decode(file_get_contents($base_path),1);
    }else{
        return getExternalGEvents($user);
    }
    return [];
}
function getExternalGEvents($user){
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = url('oauth2callback');
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken($user->GC_refresh_token);
    $clint_json = $client->getAccessToken();
    if($clint_json){
        $service = new Google_Service_Calendar($client);
        $calendarId = 'primary';
        $optParams = array(
        'maxResults' => 250,
        'orderBy' => 'startTime',
        'singleEvents' => true,
        'timeZone' => 'GMT',
        'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();
        $ar = [];
        if (!empty($events)) { 
            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                $ev = $event->getSummary();
                $c = \App\calendar_log::where('eid', '=', $event->id)->first();
                if($event->start->date && $event->end->date){
                    $actual_end_day = Carbon::createFromFormat('Y-m-d', $event->end->date)->subDays('1')->format('Y-m-d');
                    $start = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ',  $event->start->date .'T00:00:00Z')));
                    $end = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ', $actual_end_day .'T23:59:00Z')));

                }else{
                    $start = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ',  $event->start->dateTime)));
                    $end = Carbon::createFromFormat('Y-m-d H:i:s', str_replace("Z", '',str_replace("T", ' ', $event->end->dateTime)));

                }
                $hours = (abs(strtotime($start) - strtotime($end))/(60*60));
                $start->setTimezone($user->company->timezone);
                $end->setTimezone($user->company->timezone);

                $comp_date = str_replace("Z", '',str_replace("T", ' ', $event->start->dateTime));
                $i = \App\InterviewSchedule::where("google_event_id", '=', $event->id)->first();
                if($i){
                    $change = false;
                    $i_date = Carbon::createFromFormat('Y-m-d H:i:s', $i->schedule_date);
                    $i_date->setTimezone($user->company->timezone);
                    $day = explode("T", $i->toArray()["schedule_date"]);
                    $slot = explode(" - ", $i->slot_timing);
                    if(date("Y-m-d H:i:s", strtotime($i_date)) != date("Y-m-d H:i:s", strtotime($comp_date))){
                        $i->schedule_date = date("Y-m-d H:i:s", strtotime($comp_date));
                        $change = true;
                    }

                    if(date("h:i A", strtotime($day[0] . " " . $slot[0])) != date("h:i A", strtotime($start))){
                        $slot[0] =  date("h:i A", strtotime($start));
                        $change = true;
                    }

                    if(date("h:i A", strtotime($day[0] . " " . $slot[1])) != date("h:i A", strtotime($end))){
                        $slot[1] = date("h:i A", strtotime($end));
                        $change = true;
                    }
                    
                    $i->slot_timing = implode(" - ", $slot);
                    if($change){
                        $i->save();
                    }
                
                }
                if($c === null){
                    if(round($hours) >= 24){
                        $start->setTimezone('GMT');
                        $end->setTimezone('GMT');
                    }
                    $ar[] = array('id'=> "google__" . $event->id, 'title'=> 'Google Event', 'start'=> $start->format("Y-m-d h:i A"), 'end'=> $end->format("Y-m-d h:i A"));                            
                }

            }
        }
        return $ar;
    }
    return [];
}