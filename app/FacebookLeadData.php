<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookLeadData extends Model
{
    public $fillable = [
        'company_id', 'job_application_id', 'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function job_application()
    {
        return $this->belongsTo('App\JobApplication');
    }

}
