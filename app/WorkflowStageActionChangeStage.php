<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionChangeStage extends Model
{
    protected $table = 'workflow_stage_action_change_stage';
    protected $fillable = ['stage_id','action_id','to_stage'];

    function changed_to(){
        return $this->belongsTo('\App\WorkflowStage', 'to_stage','id')->withDefault();
    }
    function stage(){
        return $this->belongsTo(\App\WorkflowStage::class, 'stage_id');
    }
}
