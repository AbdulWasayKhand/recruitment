<?php
/**
 * Created by PhpStorm.
 * User: DEXTER
 * Date: 24/05/17
 * Time: 11:29 PM
 */

namespace App\Traits;

use App\EmailSetting;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Facades\Config;
use App\GlobalSetting;
use App\Option;
trait SmtpSettings{

    public function setMailConfigs($name = null, $from = null){
        $smtpSetting = EmailSetting::first();
        $settings = GlobalSetting::first();
        $company = explode(' ', trim($settings->company_name));
        $from_name = $smtpSetting->mail_from_name;
        $from_email = $smtpSetting->mail_from_email;

        if(auth()->check()){
            $from_name = Option::getOption('company_email_sender_name', user()->company_id);
            $from_email = Option::getOption('company_sender_email', user()->company_id);
        }
        if($name != null){
            $from_name = $name;
        }
        if($from != null){
            $from_email = $from;
        }

        if (\config('app.env') !== 'development') {
            Config::set('mail.driver', $smtpSetting->mail_driver);
            Config::set('mail.host', $smtpSetting->mail_host);
            Config::set('mail.port', $smtpSetting->mail_port);
            Config::set('mail.username', $smtpSetting->mail_username);
            Config::set('mail.password', $smtpSetting->mail_password);
            Config::set('mail.encryption', $smtpSetting->mail_encryption);
            Config::set('mail.from.name', $from_name);
            Config::set('mail.from.address', $from_email);
        }
        Config::set('app.name', $company[0]);
        // Config::set('mail.from.name', $company[0]);

        (new MailServiceProvider(app()))->register();
    }

}
