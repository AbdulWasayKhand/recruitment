<?php

namespace App\Exports;

use App\JobApplication;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;

class JobApplicationExport extends DefaultValueBinder implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize, WithMapping, WithCustomValueBinder
{
    use Exportable;

    protected $filters;
    protected $data;

    public function __construct(array $filters, array $data)
    {
        $this->filters = $filters;
        $this->data = $data;
    }

    public function collection()
    {   
        // Fetching All Job Applications
        $jobApplications = JobApplication::select(
                'job_applications.id',
                'job_applications.full_name',
                'job_applications.email',
                'job_applications.phone',
                'jobs.title',
                'application_status.status',
                'job_applications.source',
                'job_applications.job_id',
                'job_applications.created_at as applied_date',
                'job_applications.created_at as applied_time',
                'job_applications.eligibility',
                'jobs.eligibility_percentage',
                'job_applications.company_id'
            )
            ->leftJoin('jobs', 'jobs.id', '=', 'job_applications.job_id')
            ->leftJoin('application_status', 'application_status.id', '=', 'job_applications.status_id');
        
        if($this->filters['status'] != 'all'){
            $jobApplications = $jobApplications->whereIn('job_applications.id', explode(",", $this->filters['status']));
        }
        
        // Filter  By Job
        if ($this->filters['jobs'] != 'all' && $this->filters['jobs'] != '') {
            $jobApplications = $jobApplications->where('job_applications.job_id', $this->filters['jobs']);
        }

        // Filter  By StartDate
        if ($this->filters['startDate'] != null && $this->filters['startDate'] != '' && $this->filters['startDate'] != 0) {
            $jobApplications = $jobApplications->whereDate('job_applications.created_at', '>=', $this->filters['startDate']);
        }
        
        // Filter  By EndDate
        if ($this->filters['endDate'] != null && $this->filters['endDate'] != '' && $this->filters['endDate'] != 0) {
            $jobApplications = $jobApplications->whereDate('job_applications.created_at', '<=', $this->filters['endDate']);
        }

        return $jobApplications->orderBy('job_applications.full_name','desc')->get();
    }

    public function map($row): array
    {
        $qualifiable = \DB::table('job_questions')->leftJoin('custom_field_values', "job_questions.question_id", '=', 'custom_field_values.question_id')->where('job_id','=', $row->job_id)->whereNotNull('question_qualifiable')->count();
        $row->eligibility = ($row->eligibility == null) ? 0 : $row->eligibility;
        $row->applied_date = Carbon::parse($row->applied_date)->setTimezone($row->company->timezone ?? 'GMT')->format('m-d-Y');
        $row->applied_time = Carbon::parse($row->applied_time)->setTimezone($row->company->timezone ?? 'GMT')->format('h:i A');
        if($row->eligibility >= ($row->job->eligibility_percentage ?? $row->eligibility_percentage)){
            $elg = number_format($row->eligibility, 1);
            $row->qualification = 'QUALIFIED (' . ( $elg <= 100 ? $elg : '100.0') . '%)';
            $attributes = ['id','resume_url','job_title','job_id', 'photo_url', 'eligibility_percentage', 'eligibility','applicant_name','job','qualified','company_id','company'];
            $row = $row->makeHidden($attributes);
            return $row->toArray();
        }
        
        if(is_numeric($row->eligibility)){
            $row->qualification = 'UNQUALIFIED (' . (number_format(100-$row->eligibility, 1)) . '%)';
            $attributes = ['id','resume_url','job_title','job_id', 'photo_url', 'eligibility_percentage', 'eligibility','applicant_name','job','qualified','company_id','company'];
            $row = $row->makeHidden($attributes);
            return $row->toArray();
        }
        if($qualifiable == 0){
            $row->qualification = 'N/A';
            $attributes = ['id','resume_url','job_title','job_id', 'photo_url', 'eligibility_percentage', 'eligibility','applicant_name','job','qualified','company_id','company'];
            $row = $row->makeHidden($attributes);
            return $row->toArray();
        }
        $row->qualification = 'N/A';
        $attributes = ['id','resume_url','job_title','job_id', 'photo_url', 'eligibility_percentage', 'eligibility','applicant_name','job','qualified','company_id','company'];
        $row = $row->makeHidden($attributes);
        return $row->toArray();
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function headings(): array
    {
        return ['Name', 'Email', 'Phone Number', 'Job Title', 'Status', 'Source', 'Applied Date', 'Applied Time', 'Qualification' ];
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Job Applications')->setDescription('job-applications file')->setCreator('Recruit')->setCompany($this->data['company']);
            },
            AfterSheet::class => function(AfterSheet $event) {
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                ];
                $event->sheet->getDelegate()->getStyle('A1:I1')->applyFromArray($styleArray);
            },
        ];
    }
}
