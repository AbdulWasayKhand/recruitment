<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFieldValue extends Model
{
    protected $table = 'custom_field_values';

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id')->withDefault();
    }
}
