<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Holiday extends Model
{
    protected $fillable = ['title','from','to','delete_google_event','clone_google_event','description','company_id'];
    
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('holidays.company_id', user()->company_id);
            }
        });
    }
}