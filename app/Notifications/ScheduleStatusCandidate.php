<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Option;
use App\User;

class ScheduleStatusCandidate extends Notification
{
    use Queueable, SmtpSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, InterviewSchedule $interviewSchedule, $isCancel = false)
    {
        $this->jobApplication = $jobApplication;
        $this->interviewSchedule = $interviewSchedule;
        $this->isCancel = $isCancel;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
        if($this->jobApplication->job->workflow_id != 0){
            $this->interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $this->jobApplication->workflow_stage_id)->first();
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $multiEmail = Option::getOption("company_multi_email",$this->jobApplication->company_id);
        if($multiEmail){
            $multiEmail = explode(",", $multiEmail);
        }else{
            $multiEmail = [];
        }
        $applicant_name = $notifiable->full_name ?? $this->jobApplication->full_name;
        $subject = __('email.ScheduleStatusCandidate.subject');
        $status = $this->jobApplication->status_id ? ucfirst($this->jobApplication->status->status) : ucfirst($this->interviewSchedule->status);
        $message1 = __('email.statusfor') . ' ' . __('email.ScheduleStatusCandidate.text') . ' - ' . ucwords($this->jobApplication->job->title);

        $message = __('email.ScheduleStatusCandidate.hasBeen') . ': ' . $status;
        if($this->isCancel) {
            $subject = \App\Option::getOption('canceled_interview_subject', $this->jobApplication->company_id);
            $subject = $subject ? $subject : "Your Phone Interview has been Canceled.";
            $message = \App\Option::getOption('canceled_interview_text', $this->jobApplication->company_id);
            if($this->jobApplication->job->workflow_id != 0){
                if($this->interview_action->email_cancelled != 0){
                    $interview_email_template = \App\EmailTemplate::find($this->interview_action->email_cancelled);
                    $subject = $interview_email_template->subject;
                    $message = $interview_email_template->content;
                }
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
            }
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $status = "canceled";
            
            if($message){
                $message = strtr($message, [
                    '[applicant_name]'  => ucwords($applicant_name),
                    '[applicant_email]' => $this->jobApplication->email,
                    '[company_name]' => $this->jobApplication->company->company_name,
                    '[status_name]' => $status,
                    '[job_title]'       => $this->jobApplication->job->title,
                    '[interview_url]'   => '',
                    '[employee_name]'   => User::frontAllAdmins($this->jobApplication->company_id)[0]->name,
                    '[schedule_date]'   => $this->interviewSchedule->schedule_date->format('M d, Y'),
                    '[schedule_time]'   => $this->interviewSchedule->slot_timing,
                    '[job_location]'    => '',
                    '[job_location_address]'=> '',
                    ]);
                if($this->jobApplication->job->workflow_id != 0){
                    $message = strtr($message ?? '', [
                        '[interview_session_title]' => $this->interview_action->session_title,
                    ]);
                }else{
                    $message = strtr($message ?? '', [
                        '[interview_session_title]' => '',
                    ]);
                }
                return (new MailMessage)
                    ->subject($subject)
                    ->line($firstLine)
                    ->line($message);
            }
            $message = __('email.ScheduleStatusCandidate.hasBeen') . ' - ' . "**$status**";
        }

        /* If Rejected */
        if($this->jobApplication->status->status=='rejected' || $this->jobApplication->hiring_status == 'rejected'){
            return $this->ifRejected($subject);
        }
        /* end If Rejected */
        /* If Hired */
        if($this->jobApplication->status->status=='hired' || $this->jobApplication->hiring_status == 'hired'){
            return $this->ifHired($subject);
        }
        /* end If Hired */

        if($status == 'interview') {
            $message1 = "";
            $message = "Your Interview Request has been accepted by the Hiring Manager";
        }
        
        return (new MailMessage)
            ->subject($subject)
            ->cc($multiEmail)
            ->line($firstLine)
            ->line($message1)
            ->line($message)
            ->line(__('email.thankyouNote'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    public function ifRejected($subject)
    {
        $subject = Option::getOption('rejected_applicant_subject', $this->jobApplication->company_id) ?? $subject;
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $has_message = Option::getOption('rejected_applicant_text', $this->jobApplication->company_id);
        $employees = User::frontAllAdmins(auth()->user()->company_id);
        if($has_message){
            if($this->jobApplication->job->workflow_id != 0){
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => '',
                ]);
            };
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $message = strtr($has_message, [
                '[applicant_name]'=> $this->jobApplication->full_name,
                '[applicant_email]'=> $this->jobApplication->email,
                '[company_name]' => $this->jobApplication->company->company_name,
                '[job_title]'=> $this->jobApplication->job->title,
                '[employee_name]'=> $employees[0]->name
                ]);
            return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
        }
        $message = __('email.ScheduleStatusCandidate.hasBeen') . ' - ' . "**Rejected**";
        return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
    }
        
    public function ifHired($subject)
    {
        $subject = Option::getOption('hired_applicant_subject', $this->jobApplication->company_id) ?? $subject;
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' 
        . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $has_message = Option::getOption('hired_applicant_text', $this->jobApplication->company_id);
        $employees = User::frontAllAdmins(auth()->user()->company_id);
        if($has_message){
            if($this->jobApplication->job->workflow_id != 0){
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => '',
                ]);
            };
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $message = strtr($has_message, [
                '[applicant_name]'=> $this->jobApplication->full_name,
                '[applicant_email]'=> $this->jobApplication->email,
                '[company_name]' => $this->jobApplication->company->company_name,
                '[job_title]'=> $this->jobApplication->job->title,
                '[employee_name]'=> $employees[0]->name
                ]);
            return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
        }
        $message = __('email.ScheduleStatusCandidate.hasBeen') . ' - ' . "**Hired**";
        return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
    }
}