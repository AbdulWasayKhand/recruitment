<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Option;
use App\User;

class CandidateScheduleInterview extends Notification
{
    use Queueable, SmtpSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, InterviewSchedule $interviewSchedule, $reSchedule = false)
    {
        $this->jobApplication = $jobApplication;
        $this->interviewSchedule = $interviewSchedule;
        $this->reSchedule = $reSchedule;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
        if($this->jobApplication->job->workflow_id != 0){
            $this->interview_action = \App\WorkflowStageActionInterview::where('stage_id','=', $jobApplication->workflow_stage_id)->first();
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $applicant_name = $notifiable->full_name ?? $this->jobApplication->full_name;
        $line1 = "Your video Interview has been scheduled on:";
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $application = JobApplication::find($this->jobApplication->id);
        $token = $application ? $application->email_token : '';
        $url = route('candidate.schedule-inteview') . 
                "?c={$this->jobApplication->company_id}&app={$this->jobApplication->id}&t={$token}";
        $timezone = $this->jobApplication->job->company->timezone ?: 'GMT';
        $subject = \App\Option::getOption('interview_schedule_subject', $this->jobApplication->company_id);
        $message = \App\Option::getOption('interview_schedule_text', $this->jobApplication->company_id);



        if($this->jobApplication->job->workflow_id != 0){
            if($this->interview_action->email_schedule != 0){
                $interview_email_template = \App\EmailTemplate::find($this->interview_action->email_schedule);
                $subject = $interview_email_template->subject;
                $message = $interview_email_template->content;
            }
            $subject = strtr($subject ?? '', [
                '[interview_session_title]' => $this->interview_action->session_title,
            ]);
        }else{
            $subject = strtr($subject ?? '', [
                '[interview_session_title]' => '',
            ]);
        }
        $subject = strtr($subject ?? '', [
            '[company_name]' => $this->jobApplication->company->company_name,
        ]);
        $subject = $subject ? $subject : __('email.interviewSchedule.subject');

        $line1 = "Your Phone Interview has been scheduled on:";
        if($this->reSchedule) {
            $subject = $subject ? $subject : "Interview Rescheduled";
            $message = \App\Option::getOption('interview_reschedule_text', $this->jobApplication->company_id);
            $subject = \App\Option::getOption('interview_reschedule_subject', $this->jobApplication->company_id);
            if($this->jobApplication->job->workflow_id != 0){
                if($this->interview_action->email_reschedule != 0){
                    $interview_email_template = \App\EmailTemplate::find($this->interview_action->email_reschedule);
                    $subject = $interview_email_template->subject;
                    $message = $interview_email_template->content;
                }
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
            }
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $line1 = "Your Phone Interview has been rescheduled to:";
            if($message){
                $message = strtr($message, [
                    '[rescheduled_time]' => $this->interviewSchedule->schedule_date->timezone($timezone)->format('l M d, Y') . ' from ' . $this->interviewSchedule->slot_timing.' - '.get_timezone_list($timezone),
                    '[applicant_name]'  => $applicant_name,
                    '[applicant_email]' => $this->jobApplication->email,
                    '[company_name]' => $this->jobApplication->company->company_name,
                    '[job_title]'       => $this->jobApplication->job->title,
                    '[interview_url]'   => $url,
                    '[employee_name]'   => User::frontAllAdmins($this->jobApplication->company_id)[0]->name,
                    '[schedule_date]'   => $this->interviewSchedule->schedule_date->timezone($timezone)->format('M d, Y'),
                    '[schedule_time]'   => $this->interviewSchedule->slot_timing,
                    '[job_location]'    => $this->interviewSchedule->location->location_name,
                    '[job_location_address]' => $this->interviewSchedule->location->location,
                ]);
                if($this->jobApplication->job->workflow_id != 0){
                    $message = strtr($message ?? '', [
                        '[interview_session_title]' => $this->interview_action->session_title,
                    ]);
                }else{
                    $message = strtr($message ?? '', [
                        '[interview_session_title]' => '',
                    ]);
                }
                return (new MailMessage)
                    ->subject($subject)
                    ->line($firstLine)
                    ->line($message);                
            }
            return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($line1)
                ->line($this->interviewSchedule->schedule_date->timezone($timezone)->format('l M d, Y') . ' from ' . $this->interviewSchedule->slot_timing.' - '.get_timezone_list($timezone))
                ->line("Please be ready before the Video Interview.");
        }
        if($message){
            $message = strtr($message, [
                '[applicant_name]'  => $applicant_name,
                '[applicant_email]' => $this->jobApplication->email,
                '[company_name]'    => $this->jobApplication->company->company_name,
                '[job_title]'       => $this->jobApplication->job->title,
                '[job_location]'    => $this->interviewSchedule->location->location_name,
                '[job_location_address]' => $this->interviewSchedule->location->location,
                '[interview_url]'   => $url,
                '[employee_name]'   => User::frontAllAdmins($this->jobApplication->company_id)[0]->name,
                '[schedule_date]'   => $this->interviewSchedule->schedule_date->timezone($timezone)->format('M d, Y'),
                '[schedule_time]'   => $this->interviewSchedule->slot_timing,
            ]);
            if($this->jobApplication->job->workflow_id != 0){
                $message = strtr($message ?? '', [
                    '[interview_session_title]' => $this->interview_action->session_title,
                ]);
            }else{
                $message = strtr($message ?? '', [
                    '[interview_session_title]' => '',
                ]);
            }
            return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
        }
        return (new MailMessage)
            ->subject($subject)
            ->line($firstLine)
            ->line($line1)
            ->line($this->interviewSchedule->schedule_date->timezone($timezone)->format('l M d, Y') . ' from ' . $this->interviewSchedule->slot_timing.' - '.get_timezone_list($timezone))
            ->line("Please be ready before the Video Interview.")
            ->action('Reschedule or Cancel Interview', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

}