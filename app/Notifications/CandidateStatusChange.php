<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Option;
use App\User;
use App\ApplicationSetting;
use App\ApplicationStatus;

class CandidateStatusChange extends Notification
{
    use Queueable, SmtpSettings;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication,$status=null)
    {
        $this->jobApplication = $jobApplication;
        $this->status = $status;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /* If Rejected */
        if($this->status == 'rejected' && Option::getOption('rejected_auto_email_enabled', $this->jobApplication->company_id)){
            return $this->ifRejected();
        }
        /* end If Rejected */
        /* If Hired */
        if($this->status == 'hired' && Option::getOption('hired_auto_email_enabled', $this->jobApplication->company_id)){
            return $this->ifHired();
        }
        /* end If Hired */
        /* If Interview */
        $applicantStatuses = ApplicationStatus::get()->pluck('id','status');
        $mailSetting = ApplicationSetting::first()->mail_setting;
        // dd($this->status == 'interview' && isset($mailSetting[$applicantStatuses['interview']]) && $mailSetting[$applicantStatuses['interview']]['status']);
        if($this->status == 'interview' && isset($mailSetting[$applicantStatuses['interview']]) && $mailSetting[$applicantStatuses['interview']]['status'] ){
            return $this->ifInterview();
        }
        /* end If Interview */
        /* If No Show */
        if($this->status == 'no show' && Option::getOption('no_show_auto_email_enabled', $this->jobApplication->company_id)){
            return $this->ifNoShow();
        }
        /* end If No Show */
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $this->status = $this->status ?? (array_flip($applicantStatuses->toArray())[request()->status_id] ?? '');
        return (new MailMessage)
        ->subject(__('email.candidateStatusUpdate.subject'))
        ->line($firstLine)
        // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
        ->line(__('email.candidateStatusUpdate.text').' - ' . ucwords($this->jobApplication->job->title))
        ->line(__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($this->status))
        ->line(__('email.thankyouNote'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
       //
    }
    
    public function ifRejected()
    {
        $subject = Option::getOption('rejected_applicant_subject', $this->jobApplication->company_id);
        $subject = $subject ? $subject : __('email.candidateStatusUpdate.subject');
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $has_message = Option::getOption('rejected_applicant_text', $this->jobApplication->company_id);
        $employees = User::frontAllAdmins(auth()->user()->company_id);
        if($has_message){
            if($this->jobApplication->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$this->jobApplication->workflow_stage_id)->first();
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $interview_action->session_title,
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => $interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => '',
                ]);
            };
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $message = strtr($has_message, [
                '[applicant_name]'=> $this->jobApplication->full_name,
                '[applicant_email]'=> $this->jobApplication->email,
                '[company_name]'=> $this->jobApplication->company->company_name,
                '[job_title]'=> $this->jobApplication->job->title,
                '[employee_name]'=> $employees[0]->name
                ]);
                return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
            }
            return (new MailMessage)
            ->subject($subject)
            ->line($firstLine)
            // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
            ->line(__('email.candidateStatusUpdate.text').' - ' . ucwords($this->jobApplication->job->title))
            ->line(__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($this->status))
            ->line(__('email.thankyouNote'));
        }
        
        public function ifHired()
        {
            $subject = Option::getOption('hired_applicant_subject', $this->jobApplication->company_id);
            $subject = $subject ? $subject : __('email.candidateStatusUpdate.subject');
            $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
            $has_message = Option::getOption('hired_applicant_text', $this->jobApplication->company_id);
            $employees = User::frontAllAdmins(auth()->user()->company_id);
            if($has_message){
                if($this->jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$this->jobApplication->workflow_stage_id)->first();
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                    $has_message = strtr($has_message, [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                }else{
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => '',
                    ]);
                    $has_message = strtr($has_message, [
                        '[interview_session_title]' => '',
                    ]);
                };
                $subject = strtr($subject ?? '', [
                    '[company_name]' => $this->jobApplication->company->company_name,
                ]);
                $message = strtr($has_message, [
                    '[applicant_name]'=> $this->jobApplication->full_name,
                    '[applicant_email]'=> $this->jobApplication->email,
                    '[company_name]'=> $this->jobApplication->company->company_name,
                    '[job_title]'=> $this->jobApplication->job->title,
                    '[employee_name]'=> $employees[0]->name
                    ]);
                    return (new MailMessage)
                    ->subject($subject)
                    ->line($firstLine)
                    ->line($message);
                }
                return (new MailMessage)
        ->subject($subject)
        ->line($firstLine)
        // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
        ->line(__('email.candidateStatusUpdate.text').' - ' . ucwords($this->jobApplication->job->title))
        ->line(__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($this->status))
        ->line(__('email.thankyouNote'));
    }

    public function ifNoShow()
    {
            $subject = Option::getOption('no_show_applicant_subject', $this->jobApplication->company_id);
            $subject = $subject ? $subject : __('email.candidateStatusUpdate.subject');
            $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
            $has_message = Option::getOption('no_show_applicant_text', $this->jobApplication->company_id);
            $employees = User::frontAllAdmins(auth()->user()->company_id);
            if($has_message){
                if($this->jobApplication->job->workflow_id != 0){
                    $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$this->jobApplication->workflow_stage_id)->first();
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                    $has_message = strtr($has_message, [
                        '[interview_session_title]' => $interview_action->session_title,
                    ]);
                }else{
                    $subject = strtr($subject ?? '', [
                        '[interview_session_title]' => '',
                    ]);
                    $has_message = strtr($has_message, [
                        '[interview_session_title]' => '',
                    ]);
                };
                $subject = strtr($subject ?? '', [
                    '[company_name]' => $this->jobApplication->company->company_name,
                ]);
                $message = strtr($has_message, [
                    '[applicant_name]'=> $this->jobApplication->full_name,
                    '[applicant_email]'=> $this->jobApplication->email,
                    '[company_name]'=> $this->jobApplication->company->company_name,
                    '[job_title]'=> $this->jobApplication->job->title,
                    '[employee_name]'=> $employees[0]->name
                    ]);
                    return (new MailMessage)
                    ->subject($subject)
                    ->line($firstLine)
                    ->line($message);
                }
                return (new MailMessage)
        ->subject($subject)
        ->line($firstLine)
        // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
        ->line(__('email.candidateStatusUpdate.text').' - ' . ucwords($this->jobApplication->job->title))
        ->line(__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($this->status))
        ->line(__('email.thankyouNote'));
    }

    public function ifInterview()
    {
        $subject = Option::getOption('qualified_subject', $this->jobApplication->company_id);
        $subject = $subject ? $subject : __('email.candidateStatusUpdate.subject');
        $subject = $message = strtr($subject, ['[interview_session_title]' => '']);
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        $has_message = Option::getOption('qualified_text', $this->jobApplication->company_id);
        $employees = User::frontAllAdmins(auth()->user()->company_id);
        if($has_message){
            if($this->jobApplication->job->workflow_id != 0){
                $interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$this->jobApplication->workflow_stage_id)->first();
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => $interview_action->session_title,
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => $interview_action->session_title,
                ]);
            }else{
                $subject = strtr($subject ?? '', [
                    '[interview_session_title]' => '',
                ]);
                $has_message = strtr($has_message, [
                    '[interview_session_title]' => '',
                ]);
            };
            $subject = strtr($subject ?? '', [
                '[company_name]' => $this->jobApplication->company->company_name,
            ]);
            $url = route('candidate.schedule-inteview')."?c={$this->jobApplication->company_id}&app={$this->jobApplication->id}&t={$this->jobApplication->email_token}";
            $message = strtr($has_message, [
                '[applicant_name]'=> $this->jobApplication->full_name,
                '[applicant_email]'=> $this->jobApplication->email,
                '[company_name]'=> $this->jobApplication->company->company_name,
                '[employee_name]'=> $employees[0]->name,
                '[interview_url]'   => $url,
            ]);
            return (new MailMessage)
                ->subject($subject)
                ->line($firstLine)
                ->line($message);
        }
        return (new MailMessage)
        ->subject($subject)
        ->line($firstLine)
        // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
        ->line(__('email.candidateStatusUpdate.text').' - ' . ucwords($this->jobApplication->job->title))
        ->line(__('email.ScheduleStatusCandidate.nowStatus').' - ' . ucfirst($this->status))
        ->line(__('email.thankyouNote'));
    }
}