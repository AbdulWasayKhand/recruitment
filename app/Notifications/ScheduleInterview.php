<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\SmsSetting;
use App\Traits\SmsSettings;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use App\Option;
use App\Company;
use App\User;
use App\SmsLog;
class ScheduleInterview extends Notification
{
    use Queueable, SmtpSettings, SmsSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, InterviewSchedule $interviewSchedule, $reSchedule = false,$comment='')
    {
        $this->company = Company::find($jobApplication->company_id);
        $this->jobApplication = $jobApplication;
        $this->interviewSchedule = $interviewSchedule;
        $this->reSchedule = $reSchedule;
        $this->comment = $comment;
        $this->smsSetting = SmsSetting::first();
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        // $name = 'WAZiE Recruit';
        // $from = 'info@engyj.com';
        $this->setMailConfigs($name, $from);
        $this->setSmsConfigs();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail', TwilioChannel::class];

        if ($this->smsSetting->nexmo_status == 'active' && $notifiable->mobile_verified == 1) {
            array_push($via, 'nexmo');
        }

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
        // ->line($firstLine)
            $multiEmail = Option::getOption("company_multi_email",$this->jobApplication->company_id);
            if($multiEmail){
                $multiEmail = explode(",", $multiEmail);
            }else{
                $multiEmail = [];
            }
        
        // $greet = __('email.hello').' ' . ucwords($notifiable->name) . ',';
        $greet = " ";
        $line1 = $firstLine . "Your interview with <strong>" . __($this->jobApplication->full_name)."</strong> for the " . ucwords($this->jobApplication->job->title) . " position has been scheduled on:<br>" .
        $this->interviewSchedule->schedule_date->timezone($this->company->timezone)->format('D, M d, Y') . " at {$this->interviewSchedule->slot_timing}.". ($this->interviewSchedule->location->location_name ? "\nLocation: {$this->interviewSchedule->location->location_name}" : "");
        $url = getDomainSpecificUrl(url('admin/interview-schedule'),$notifiable->company);
        $subject = __('email.interviewSchedule.subject');
        if($this->reSchedule) {
            $subject = "Interview Rescheduled";
            $line1 = $firstLine . "Your interview with <strong>{$this->jobApplication->full_name}</strong> for the {$this->jobApplication->job->title} position has been rescheduled to:<br>".
            $this->interviewSchedule->schedule_date->timezone($this->company->timezone)->format('D, M d, Y') . " at {$this->interviewSchedule->slot_timing}.". ($this->interviewSchedule->location->location_name ? "\nLocation: {$this->interviewSchedule->location->location_name}" : "");
        }
        $min = JobApplication::where('email', $this->jobApplication->email)
                ->select(\DB::raw('MIN(id) as min'))
                ->groupByRaw('job_id')
                ->pluck('min')->toArray(); 
        if(!in_array($this->jobApplication->id, $min)){
            $line1 = $line1.'<br><strong>Note:</strong> <span style="color:red;">This applicant has applied before.</span>';
        }
        if($this->comment){
            $line1 .= '<br>Comment from Applicant : '.$this->comment;
        }
        $reSchedule = $this->reSchedule;
        if($this->jobApplication->job->workflow_id){
            return (new MailMessage)
            ->subject($subject)
            ->from('info@engyj.com', 'WAZiE Recruit')
            ->markdown('email.schedule-interview-received', compact('greet', 'url', 'line1', 'reSchedule'));
        }
        return (new MailMessage)
            ->subject($subject)
            ->from('info@engyj.com', 'WAZiE Recruit')
            ->cc($multiEmail)
            ->markdown('email.schedule-interview-received', compact('greet', 'url', 'line1', 'reSchedule'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    /**
     * Get the Twilio / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TwilioSmsMessage
     */
    public function toTwilio($notifiable)
    {

    }
}