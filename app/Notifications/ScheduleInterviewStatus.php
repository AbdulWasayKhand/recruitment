<?php

namespace App\Notifications;

use App\JobApplication;
use App\SmsSetting;
use App\Traits\SmsSettings;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use App\Option;

class ScheduleInterviewStatus extends Notification
{
    use Queueable, SmtpSettings, SmsSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, $canceled = false)
    {
        $this->jobApplication = $jobApplication;
        $this->smsSetting = SmsSetting::first();
        $this->canceled = $canceled;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        // $name = 'WAZiE Recruit';
        // $from = 'info@engyj.com';
        $this->setMailConfigs($name, $from);
        $this->setSmsConfigs();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail', 'database', TwilioChannel::class];

        if ($this->smsSetting->nexmo_status == 'active' && $notifiable->mobile_verified == 1) {
            array_push($via, 'nexmo');
        }

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        // ->line($firstLine)
        $multiEmail = Option::getOption("company_multi_email",$this->jobApplication->company_id);
        $status = $this->jobApplication->status->status ? ucFirst($this->jobApplication->status->status) : $this->jobApplication->schedule->status;
        $line = $this->jobApplication->full_name.' '.__('email.interviewScheduleStatus.text').' - ' . ucwords($this->jobApplication->job->title) .' ' .__('email.interviewScheduleStatus.statusChangesTo'). '  ' . $status;
        if($this->jobApplication->status->status == 'rejected' || $this->jobApplication->hiring_status == 'rejected'){
            $line = "The applicant ".$this->jobApplication->full_name." has been rejected for the " .ucwords($this->jobApplication->job->title). " position.";
        }
        if($status == 'pass' || $status == 'fail'){
            $line = "{$this->jobApplication->full_name} " .ucFirst($status). "ed the " . session()->get('previous_stage_session_title') . ".";
            $line .= session()->get('has_triggered') ? " They are now in the {$this->jobApplication->workflow_stage->title} Stage." : "";
        }
        if($multiEmail){
            $multiEmail = explode(",", $multiEmail);
        }else{
            $multiEmail = [];
        }
        if($this->canceled) {
            return (new MailMessage)
                    ->subject("Interview has been canceled")
                    ->from('info@engyj.com', 'WAZiE Recruit')
                    ->cc($multiEmail)
                    ->line($firstLine)
                    // ->greeting(__('email.hello') . ' ' . ucwords($notifiable->name) . ',')
                    ->line("Your interview with {$this->jobApplication->full_name} for the job {$this->jobApplication->job->title} has been canceled")
                    ->line(__('email.thankyouNote'));
        }
        
        return (new MailMessage)
            ->subject(__('email.interviewScheduleStatus.subject'))
            ->from('info@engyj.com', 'WAZiE Recruit')
            ->cc($multiEmail)
            ->line($firstLine)
            // ->greeting(__('email.hello').' ' . ucwords($notifiable->name) . ',')
            ->line($line)
            ->action("Log in to WAZiE Recruit for details", getDomainSpecificUrl(route('login'),$notifiable->company));
            // ->line(__('email.thankyouNote'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    public function toTwilio($notifiable)
    {
        // $message = "The applicant {$this->jobApplication->full_name}'s interview for job {$this->jobApplication->job->title} has been canceled";
        // return (new TwilioSmsMessage())
        //     ->content($message);
    }
}
