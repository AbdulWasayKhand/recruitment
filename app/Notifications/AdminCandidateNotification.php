<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\SmsSetting;
use App\Traits\SmsSettings;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use App\Option;
use App\Company;
class AdminCandidateNotification extends Notification
{
    use Queueable, SmtpSettings, SmsSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, $qualified)
    {
        $this->qualified = ucfirst($qualified);
        $this->company = Company::find($jobApplication->company_id);
        $this->jobApplication = $jobApplication;
        $this->smsSetting = SmsSetting::first();
        // $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        // $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $name = 'WAZiE Recruit';
        $from = 'info@engyj.com';
        $this->setMailConfigs($name, $from);
        $this->setSmsConfigs();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail', TwilioChannel::class];

        if ($this->smsSetting->nexmo_status == 'active' && $notifiable->mobile_verified == 1) {
            array_push($via, 'nexmo');
        }

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
            $multiEmail = trim(Option::getOption("company_multi_email",$this->jobApplication->company_id).' ');
            if($multiEmail){
                $multiEmail = explode(",", $multiEmail);
            }else{
                $multiEmail = [];
            }
        $greet = " ";
        $text_color = ($this->qualified=='Qualified') ? 'green' : 'red';
        $line1 = $firstLine . "You have received a new job application from <strong>{$this->jobApplication->full_name}</strong> for the " . ucwords($this->jobApplication->job->title) . " position and this applicant is <strong style='color:{$text_color};'>{$this->qualified}</strong>."; 
        $url = getDomainSpecificUrl(route('admin.job-applications.table'),$notifiable->company);
        $subject = "New {$this->qualified} Applicant";
        $min = JobApplication::where('email', $this->jobApplication->email)
                ->select(\DB::raw('MIN(id) as min'))
                ->groupByRaw('job_id')
                ->pluck('min')->toArray(); 
        if(!in_array($this->jobApplication->id, $min)){
            $line1 = $line1.'<br><strong>Note:</strong> <span style="color:red;">This applicant has applied before.</span>';
        }
        return (new MailMessage)
            ->subject($subject)
            ->cc($multiEmail)
            ->markdown('email.admin-received-application', compact('greet', 'url', 'line1'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    /**
     * Get the Twilio / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TwilioSmsMessage
     */
    public function toTwilio($notifiable)
    {
        $message = "You have received a new job application from {$this->jobApplication->full_name} for the {ucwords($this->jobApplication->job->title)} position.";        
        $twilio_number = Option::getOption('twilio_from_number', $this->jobApplication->company_id) ?? config('twilio-notification-channel.from');
        return (new TwilioSmsMessage())
            ->from($twilio_number)
            ->content($message);
    }
}
