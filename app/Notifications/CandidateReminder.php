<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Option;

class CandidateReminder extends Notification
{
    use Queueable, SmtpSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(InterviewSchedule $schedule)
    {
        $this->schedule = $schedule;
        $name = Option::getOption('company_email_sender_name', $schedule->company_id);
        $from = Option::getOption('company_sender_email', $schedule->company_id);
        $this->setMailConfigs($name, $from);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $notifiable->company->logo_url . '" alt="Recruit" title="Recruit" /></a>';
        
        return (new MailMessage)
            ->subject(__('email.interviewSchedule.interviewReminder'))
            ->line($firstLine)
            // ->greeting(__('email.hello').' ' . ucwords($notifiable->full_name) . '!')
            ->line(__('email.your').' '.__('email.interviewSchedule.text').' - ' . ucwords($notifiable->job->title))
            ->line(__('email.on').' - ' . $this->schedule->schedule_date->format('M d, Y h:i a'))
            ->line(__('email.thankyouNote'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $notifiable->toArray()
        ];
    }
}
