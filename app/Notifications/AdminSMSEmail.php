<?php

namespace App\Notifications;

use App\JobApplication;
use App\SmsSetting;
use App\Traits\SmsSettings;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioSmsMessage;
use App\Option;
use App\Company;
class AdminSMSEmail extends Notification
{
    use Queueable, SmtpSettings, SmsSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, $body)
    {
        $this->company = Company::find($jobApplication->company_id);
        $this->jobApplication = $jobApplication;
        $this->body = $body;
        $name = 'WAZiE Recruit';
        $from = 'info@engyj.com';
        $this->setMailConfigs($name, $from);
        $this->setSmsConfigs();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail'];

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
            $multiEmail = Option::getOption("company_multi_email",$this->jobApplication->company_id);
            if($multiEmail){
                $multiEmail = explode(",", $multiEmail);
            }else{
                $multiEmail = [];
            }
        $greet = " ";
        $line1 = $this->body; 
        // $url = getDomainSpecificUrl(route('login'),$notifiable->company);
        $url = url("admin/text-messages");
        $subject = "Text Message from ".$this->jobApplication->full_name;
        return (new MailMessage)
            ->subject($subject)
            ->cc($multiEmail)
            ->markdown('email.schedule-interview-received', compact('greet', 'url', 'line1'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    /**
     * Get the Twilio / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TwilioSmsMessage
     */
    public function toTwilio($notifiable)
    {

    }
}
