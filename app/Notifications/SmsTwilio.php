<?php

namespace App\Notifications;

use App\Traits\SmsSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class SmsTwilio extends Notification
{
    use Queueable, SmsSettings;

    public $message;
    public $from;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $from = NULL)
    {
        $this->message = $message;
        $this->from = $from;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TwilioChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \NotificationChannels\Twilio\TwilioSmsMessage
     */
    public function toTwilio($notifiable)
    {
        if(!is_null($this->from)) {
            return (new TwilioSmsMessage())
                ->from($this->from)
                ->content($this->message);
        }

        return (new TwilioSmsMessage())
            ->from(config('twilio-notification-channel.from'))
            ->content($this->message);
    }

}
