<?php

namespace App\Notifications;

use App\InterviewSchedule;
use App\JobApplication;
use App\SmsSetting;
use App\Traits\SmsSettings;
use App\Traits\SmtpSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use App\Option;
use App\Company;
use App\User;
use App\SmsLog;
class AcceptInterviewRequest extends Notification
{
    use Queueable, SmtpSettings, SmsSettings;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, InterviewSchedule $interviewSchedule,User $accepter, $auto = false)
    {
        $this->company = Company::find($jobApplication->company_id);
        $this->jobApplication = $jobApplication;
        $this->interviewSchedule = $interviewSchedule;
        $this->accepter = $accepter;
        $this->smsSetting = SmsSetting::first();
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
        $this->setSmsConfigs();
        $this->auto = $auto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['mail', TwilioChannel::class];

        if ($this->smsSetting->nexmo_status == 'active' && $notifiable->mobile_verified == 1) {
            array_push($via, 'nexmo');
        }

        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
        $greet = " ";
        $line1 = $firstLine . "Dear <strong>{$notifiable->name}</strong>,<br>The interview of {$this->jobApplication->full_name} for the position of {$this->jobApplication->job->title} has been ".($this->auto ? "Auto " : "" )."Accepted by <strong>{$this->accepter->name}</strong>";
        $subject = "Interview ".($this->auto ? "Auto " : "" )."Accepted by {$this->accepter->name}";
        return (new MailMessage)
            ->subject($subject)
            ->from('info@engyj.com', 'WAZiE Recruit')
            ->markdown('email.accept-interview-request', compact('greet', 'line1'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->jobApplication->toArray()
        ];
    }

    /**
     * Get the Twilio / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TwilioSmsMessage
     */
    public function toTwilio($notifiable)
    {

    }
}