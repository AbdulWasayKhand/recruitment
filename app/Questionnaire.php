<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Questionnaire extends Model
{
    protected $fillable = ['name','custom_field_value_ids'];
    
    protected $casts = [
        'custom_field_value_ids' => 'array'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('questionnaires.company_id', user()->company_id);
            }
        });
    }

    public function custom_field_values(){
        return $this->belongsTo('App\CustomFieldValue')->withDefault(function () {
        	$ids = $this->custom_field_value_ids ? json_decode($this->custom_field_value_ids) : [];
            return \App\CustomFieldValue::leftjoin('questions','custom_field_values.question_id','questions.id')->orderBy('questions.sort_order')->whereIn('custom_field_values.id',(array)$ids)->select('custom_field_values.*')->get();
    	});
    }

    public function questions()
    {
        $questions = \App\Question::where([]);
        if($this->custom_field_values->count()){
            $questions = $questions->whereIn('id',$this->custom_field_values->pluck('question_id'));
        }
        return $questions->get();
    }
}