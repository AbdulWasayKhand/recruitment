<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowStageActionInterviewQuestionnaire extends Model
{
    protected $fillable = ['action_id','workflow_stage_action_interview_id','questionnaire_id'];
    
    function qna(){
        return $this->belongsTo(Questionnaire::class, 'questionnaire_id')->withDefault();
    }
    function template(){
        return $this->belongsTo(EmailTemplate::class, 'email_template_id');
    }
}
