<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Traits\SmtpSettings;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class NewUserNotification extends Mailable
{
    use Queueable, SerializesModels, SmtpSettings;
    public function __construct($data)
    {
        $this->from_email = $data['from']['name'];
        $this->email_name = $data['from']['email'];
        $this->message = $data['message'];
        $this->subject = $data['subject'];
        $this->to_email = $data['to'];
        $this->setMailConfigs();
    }
    public function build()
    {
        return $this->subject($this->subject)
            ->from($this->email_name, $this->from_email)
            ->to($this->to_email)
            ->markdown('email.newusernotification', ['text' => $this->message]);

    }
}