<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Traits\SmtpSettings;
use App\JobApplication;
use App\Option;
use Illuminate\Support\Facades\Config;

class ReceivedApplication extends Mailable
{
    use Queueable, SerializesModels, SmtpSettings;

    public $jobApplication;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication)
    {   
        $this->jobApplication = $jobApplication;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = \App\Company::find($this->jobApplication->company_id);
        return $this->subject(__('email.applicationReceived.subject'))
            ->from(Option::getOption('company_sender_email', $this->jobApplication->company_id), Option::getOption('company_email_sender_name', $this->jobApplication->company_id))
            ->to($this->jobApplication->email)
            ->markdown('email.received_application',[
                'company' => $company
            ]);
    }
}
