<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Traits\SmtpSettings;
use App\Option;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels, SmtpSettings;

    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->email = $request;
        $this->setMailConfigs();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__('email.contact.subject'))
            ->from('info@engyj.com', 'Recruit')
            ->to('sohail.sabaseo@gmail.com')
            ->markdown('email.contactmail');
    }
}
