<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Traits\SmtpSettings;
use App\JobApplication;
use App\Option;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class MailQualification extends Mailable
{
    use Queueable, SerializesModels, SmtpSettings;

    public $jobApplication;
    public $qualified;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, $qualified, $message = null)
    {
        $this->jobApplication = $jobApplication;
        $this->qualified = $qualified;
        $this->message = $message;
        $name = Option::getOption('company_email_sender_name', $jobApplication->company_id);
        $from = Option::getOption('company_sender_email', $jobApplication->company_id);
        $this->setMailConfigs($name, $from);
        if($this->jobApplication->job->workflow_id != 0){
            $this->interview_action = \App\WorkflowStageActionInterview::where('stage_id','=',$this->jobApplication->workflow_stage_id)->first() ?? new \App\WorkflowStageActionInterview;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->message = $this->message ? $this->message : Option::getOption($this->qualified . '_text', $this->jobApplication->company_id);
        $subject = Option::getOption('qualified_subject', $this->jobApplication->company_id);
        $subject = $subject ? $subject : __('email.applicationQualification.subject');

        if($this->jobApplication->job->workflow_id != 0){
            $subject = strtr($subject ?? '', [
                '[interview_session_title]' => $this->interview_action->session_title,
            ]);
        }else{
            $subject = strtr($subject ?? '', [
                '[interview_session_title]' => '',
            ]);
        };
        if($this->jobApplication->job->workflow_id != 0){
            $this->message = strtr($this->message ?? '', [
                '[interview_session_title]' => $this->interview_action->session_title,
            ]);
        }else{
            $this->message = strtr($this->message ?? '', [
                '[interview_session_title]' => '',
            ]);
        }
        $subject = strtr($subject ?? '', [
            '[company_name]' => $this->jobApplication->company->company_name,
        ]);
        $firstLine = '<a href="#"><img style="max-width: 40%; margin-left:30%;margin-bottom:30px;" src="' . $this->jobApplication->company->logo_url . '" alt="Recruit" title="Recruit" /></a><br>';
        // ->line($firstLine)
        $url = route('candidate.schedule-inteview') . 
                "?c={$this->jobApplication->company_id}&app={$this->jobApplication->id}&t={$this->jobApplication->email_token}";
        $company = \App\Company::find($this->jobApplication->company_id);
        return $this->subject($subject)
            ->from(Config::get('mail.from.address'), Option::getOption('company_email_sender_name', $this->jobApplication->company_id))
            ->to($this->jobApplication->email)
            ->markdown('email.qualification_application', [
                'jobApplication' => $this->jobApplication,
                'company' => $company,
                'text' => $firstLine . $this->message,
                'url' => $url
            ]);
    }
}
