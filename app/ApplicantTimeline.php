<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantTimeline extends Model
{
    protected $table = 'applicant_timeline';
    protected $fillable = ['applicant_id', 'entity_id', 'user_id', 'entity', 'comments'];

    public function applicant()
    {
        return $this->belongsTo(JobApplication::class, 'applicant_id')->withDefault();
    }


    public function getEntity()
    {
        switch ($this->entity) {
            case 'interview_schedule':
                return $this->belongsTo('\App\InterviewSchedule', 'entity_id','id')->withDefault();
            break;
            case 'interview_reschedule':
                return $this->belongsTo('\App\InterviewSchedule', 'entity_id','id')->withDefault();
            break;
            case 'interview_canceled':
                return $this->belongsTo('\App\InterviewSchedule', 'entity_id','id')->withDefault();
            break;
            case 'interview_pass':
                return $this->belongsTo('\App\InterviewSchedule', 'entity_id','id')->withDefault();
            break;
            case 'interview_fail':
                return $this->belongsTo('\App\InterviewSchedule', 'entity_id','id')->withDefault();
            break;
            case 'stage':
                return $this->belongsTo('\App\WorkflowStage', 'entity_id','id')->withDefault();
            break;
            case 'notes':
                return $this->belongsTo('\App\ApplicantNote', 'entity_id','id')->withDefault();
            break;
            case 'questionnaires':
                return $this->belongsTo('\App\Questionnaire', 'entity_id','id')->withDefault();
            break;
            case 'hire':
                return $this->belongsTo('\App\JobApplication', 'entity_id','id')->where('hiring_status','hired')->withDefault();
            break;
            case 'reject':
                return $this->belongsTo('\App\JobApplication', 'entity_id','id')->where('hiring_status','rejected')->withDefault();
            break;
            case 'attachment_photo':
                return $this->belongsTo('\App\JobApplication', 'entity_id','id')->withDefault();
            break;
            case 'attachment_resume':
                return $this->belongsTo('\App\Document', 'entity_id','id')->withDefault();
            break;
            case 'rating':
                return $this->belongsTo('\App\JobApplication', 'entity_id','id')->withDefault();
            break;
            case 'documents':
                return $this->belongsTo('\App\Document', 'entity_id','id')->withDefault();
            break;
        }
    }
}