<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Budget extends Model
{
    protected $fillable = ['company_id','job_id','facebook','indeed','direct','google','linkedin'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check() && !auth()->user()->is_superadmin) {
                $builder->where('budgets.company_id', user()->company_id);
            }
        });
    }
}
