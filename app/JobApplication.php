<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Option;

class JobApplication extends Model
{
    use Notifiable, SoftDeletes;

    public $fillable = ['referer_url','status_id','widget_host'];

    protected $dates = ['dob'];

    protected $casts = [
        'skills' => 'array'
    ];

    protected $appends = ['resume_url', 'photo_url', 'qualified','job_title','applicant_name'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function (Builder $builder) {
            if (auth()->check()) {
                $builder->where('job_applications.company_id', user()->company_id);
            }
        });
    }

    public function labels(){
        return $this->hasMany(LabelTransition::class, 'application_id');
    }

    public function timelines(){
        return $this->hasMany(ApplicantTimeline::class, 'applicant_id')->orderBy('id','desc');
    }

    public function documents()
    {
        return $this->morphMany(Document::class, 'documentable');
    }

    public function attachments()
    {
        return $this->hasMany('\App\Document', 'documentable_id','id');
    }
    
    public function resumeDocument()
    {
        return $this->morphOne(Document::class, 'documentable')->where('name', 'Resume');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id')->withDefault();
    }

    public function onboard()
    {
        return $this->hasOne(Onboard::class, 'job_application_id');
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class)->withDefault();
    }

    public function workflow_stage()
    {
        return $this->belongsTo(WorkflowStage::class, 'workflow_stage_id')->withDefault();
    }

    public function status()
    {
        return $this->belongsTo(ApplicationStatus::class, 'status_id')->withDefault();
    }

    public function schedule()
    {
        return $this->hasOne('App\InterviewSchedule')->orderBy('id','desc')->withDefault();
    }

    public function notes()
    {
        return $this->hasMany(ApplicantNote::class, 'job_application_id')->orderBy('id', 'desc');
    }

    public function facebook_data()
    {
        return $this->hasMany('App\FacebookLeadData', 'job_application_id');
    }

    public function interview()
    {
        return $this->hasOne('App\InterviewSchedule')->orderBy('id','desc')->withDefault();
    }

    public function company()
    {
        return $this->belongsTo('App\Company')->withDefault();
    }

    public function getPhotoUrlAttribute()
    {
        if (is_null($this->photo)) {
            return asset('avatar.png');
        }
        return asset_url('candidate-photos/' . $this->photo);
    }
    public function history(){
        return $this->hasMany('App\ApplicantHistory', 'applicant_id');
    }
    public function lastStatus($status){
        $appStatus = \App\ApplicationStatus::pluck('id', 'status');
        if($this->job->workflow_id){
            return $this->timelines()->where('entity', '=', ($status == 'hired') ? 'hire' : (($status == 'rejected') ? 'reject' : '') )->first() ?? new \App\ApplicantTimeline;
        }
        return $this->history()->where('current_value', '=', $appStatus[$status])->orderBy('id','desc')->first() ?? new \App\ApplicantHistory;
    }
    public function getJobTitleAttribute()
    {
        return $this->job->title;
    }

    public function getApplicantNameAttribute()
    {
        return $this->full_name;
    }

    public function getResumeUrlAttribute()
    {
        if ($this->documents()->where('name', 'Resume')->first()) {
            return asset_url('documents/' . $this->id . '/' . $this->documents()->where('name', 'Resume')->first()->hashname);
        }
        return false;
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

    public function routeNotificationForTwilio()
    {
        return $this->phone;
    }

    public function getQualifiedAttribute()
    {   
        $qualifiable = \DB::table('job_questions')->leftJoin('custom_field_values', "job_questions.question_id", '=', 'custom_field_values.question_id')->where('job_id','=', $this->job->id)->whereNotNull('question_qualifiable')->count();
        $this->eligibility = ($this->eligibility == null) ? 0 : $this->eligibility;
        if($this->eligibility >= ($this->job->eligibility_percentage ?? $this->eligibility_percentage)){
            $elg = number_format($this->eligibility, 1);
            return 'QUALIFIED (' . ( $elg <= 100 ? $elg : '100.0') . '%)';
        }
        if($qualifiable == 0){
            return 'N/A';
        }
        
        if(is_numeric($this->eligibility))
            return 'UNQUALIFIED (' . (number_format(100-$this->eligibility, 1)) . '%)';

        return 'N/A';
    }

    public function getSourceAttribute($value)
    {   
        $url = $this->referer_url != '' || $this->referer_url != null ? $this->referer_url : '#';
        $title = $value;
        $title = strpos($url, '#') !== false ? "Direct" : (($value=='referral') ? 'Referral' : 'Other');
        $title = strpos($url, 'facebook') !== false ? "Facebook" : $title;
        $title = strpos($url, 'google') !== false ? "Google" : $title;
        $title = strpos($url, 'linkedin') !== false ? "Linkedin" : $title;
        $title = strpos($url, 'indeed') !== false ? "Indeed" : $title;
        $title = strpos($url, 'recruit') !== false ? "Direct" : $title;
        if(strpos($url, 'mycna')){
            return 'MyCNA';
        }
        if(strpos($url, 'careerplug')){
            return 'Career Plug';
        }
        if(strpos($url, 'jazzhr')){
            return 'JazzHR';
        }
        if(strpos($url, 'craigslist')){
            return 'Craigslist';
        }
        if(strpos($url, 'apply')){
            return 'Apply';
        }
        return $title;
    }

    public function onStatusChange()
    {
        if($this->isDirty('status_id') && Option::getOption("sent_manual_interview_invite", $this->company_id, null, $this->id)) {
            Option::destroyOption('sent_manual_interview_invite', $this->company_id, $this->id);
        }
    }
    public function conversation()
    {
        return \App\SmsLog::where('number', 'like', '%'.$this->phone.'%')
                    ->where('company_id',  $this->company_id)
                    ->where('status', '!=', 'pending');
    }
    public function sms_logs()
    {
        return $this->hasMany('App\SmsLog', 'reference_id')
                    ->where('message_type', 'inbound')
                    ->where('status', 'sent')
                    ->where('reference_name', 'application');
    }

    public function interviews()
    {
        return $this->hasMany('App\InterviewSchedule');
    }

    public function answered()
    {
        return $this->hasMany('App\JobApplicationAnswer','job_application_id')->where('job_id',$this->job_id);
    }

    public function getQuestinnaires($which)
    {
        $all_stages = \App\WorkflowStageActionQuestionnaire::where('workflow_id',$this->job->workflow_id)->where('questionnaire_type','external')->get()->pluck('questionnaire_id');
        $all_que = $all_stages;
        if($which == 'internal'){
            $all_stages = \App\WorkflowStageActionInterview::where('workflow_id',$this->job->workflow_id)->get()->pluck('id');
            $all_que  = \App\WorkflowStageActionInterviewQuestionnaire::whereIn('workflow_stage_action_interview_id',  $all_stages)->get()->pluck('questionnaire_id');
        }
        $answers = [];
        foreach(\App\Questionnaire::find($all_que) as $key){
            $before = \App\JobApplicationAnswer::where('job_application_id', '=', $this->id)->where('questionnaire_id', '=', $key->id)->get();
            if($before->count()){
                $answers[$key->name] = \App\JobApplicationAnswer::where('job_application_id', '=', $this->id)->where('questionnaire_id', '=', $key->id)->get();
            }   
        }
        return $answers;
    }

    public function getStageQuestionnaires($which)
    {
        $questionnaire_ids = \App\WorkflowStageActionQuestionnaire::where('workflow_id',$this->job->workflow_id)->where('questionnaire_type',$which)->pluck('questionnaire_id');
        $answers = [];
        foreach(\App\Questionnaire::find($questionnaire_ids) as $questionnaire){
            if(($answered = $this->answered()->where('questionnaire_id', $questionnaire->id)->get())->count()){
                $answers[$questionnaire->name] = $answered;
            }
        }
        return $answers;
    }

    public function ExternalQuestinnaires()
    {
        $all_stages = \App\WorkflowStage::where('workflow_id',$this->job->workflow_id)->get()->pluck('id');
        $all_que  = \App\Questionnaire::find(\App\WorkflowStageActionQuestionnaire::whereIn('stage_id',  $all_stages)->where('questionnaire_type','external')->get()->pluck('questionnaire_id'));
        return $this->answered->whereIn('questionnaire_id',$all_que);
    }

    public function getAppliedBeforeAttribute()
    {
        $min = JobApplication::where('email', $this->email)
                ->select(\DB::raw('MIN(id) as min'))
                ->groupByRaw('job_id')
                ->pluck('min')->toArray(); 
        return !in_array($this->id, $min);
    }
}
