<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Attachments</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link href="{{ asset('assets/node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }
            .mt-12 {
                margin-top : 12px;
            }
            .attachment-wrapper {
                background-color: #f4f6f9 !important;
                padding: 50px 0px;
                /* height: 100%; */
            }
            .attachment-container {
                width: 60%;
                background-color: white;
                margin-left: 20%;
                box-shadow: 0 6px 15px rgba(36,37,38,0.08);
                border: 1px solid #ccc;
                border-radius: 10px;
            }
            .attachment-form {
                width: 80%;
                margin: 0px 0px 40px 10%;
            }
            .attach-file {
                border: 1px solid #ccc;
                border-radius: 6px;
                height: 56px;
                display: flex;
                align-items: center;
                padding: 15px;
            }
            .btn-upload {
                background-color: #efefef;
                border: 1px solid #b7b7b7;
                color: #6f6f6f;
                border-radius: 4px;
                height: 30px;
            }
            .btn-upload-wrapper {
                font-family: "Open Sans", sans-serif;
                color: #7b7b7b;
                font-weight: 300;
                font-size: 14px;
            }
            .btn-attachment {
                padding: 10px 50px;
            }
            .template-sub {
                font-size: 26px;
                font-weight: 700;
                color: #484848;
            }
            .temp-container {
                width: 80%;
                margin: 0px 0px 0px 10%;
            }
            @media only screen and (max-width: 600px) {
                .attachment-container {
                    width: 80%;
                    margin-left: 10%
                }
                .attach-file {
                    padding: 10px;
                }
                .btn-attachment {
                    padding: 10px 30px;
                }
            }
        </style>
    </head>
    <body>
    
    
        <div class="attachment-wrapper">
            <div class="attachment-container">
                <div class="temp-container">
                    <div class="row">
                        <h1 class="col-md-12 text-center template-sub">{{$template->subject}}</h1>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12 text-center">{!! $template->content !!}</div>
                    </div>
                    <div class="row" style="padding-bottom:25px;">
                        <div class="col-md-12 text-center mt-12">
                        <span>@lang('modules.jobApplication.document.accept')</span>
                        </div>
                    </div>
                </div>
                <form action="{{ url('email-attachment/store') }}" class="attachment-form" id="submitAttachment" autocomplete="off">
                    @csrf
                    <input type="hidden" name="applicant_id" value="{{ $jobApplication->id }}">
                    <input type="hidden" name="action_id" value="{{ $action->id }}">
                    @php($i=0)
                    @foreach($action->title as $title)
                    @php($attachment = $action->applicantAttachmentByFileName($jobApplication->id,$title))
                    <input type="hidden" name="attachments[{{$i}}][id]" value="{{ $attachment->id }}">
                    <input type="hidden" name="attachments[{{$i}}][name]" value="{{ $title }}">
                    <div class="row mt-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{$title}}</label><br>
                                <div class="attach-file">
                                    <div class="btn-upload-wrapper">
                                        <input class="file_picker" type="file" name="attachments[{{$i}}][file]" accept=".png,.jpg,.jpeg,.pdf" value="{{$attachment->hashname}}" style="display:none;">
                                        <button onclick="loadFile(this)" type="button" class="btn-upload" >Choose File</button> 
                                        <small>{{$attachment->hashname ?? "No File Chosen" }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php($i++)
                    @endforeach
                    <div class="row mt-12">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-success btn-attachment">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>
        <script src="{{ asset('/froiden-helper/helper.js') }}" type="text/javascript"></script>
        <script>
        function loadFile(elm){
            $childrens = elm.parentNode.children;
            $childrens[0].click();
           $($childrens[0]).on('change', function(){
                $path = this.value.split('\\');
                $childrens[2].innerHTML = $path[ ($path.length -1) ]
            });
        }
            $('#submitAttachment').submit(function(e) {
                e.preventDefault();
                const form = $(this);
                var any_empty = 0;
                var $names = [];
                $.each($('#submitAttachment').find('input[type=file]'),function(){
                    if($(this).attr('type')=='file'){
                        $this_value = $(this).val() ? $(this).val() : $(this).attr('value'); 
                        if($this_value==''){
                            $(this).closest('div').css('border','1px solid red');                       
                            any_empty++;
                        }
                        else{
                            $(this).closest('div').css('border','1px solid #ccc');
                            $extension = $this_value.substr( ($this_value.lastIndexOf('.') +1) ); 
                            if($.inArray($extension,['jpg','jpeg','png','gif','pdf'])<0){
                                $.toast({
                                    heading: 'warning',
                                    text: "File must be in above formats.",
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                    
                                });
                                $(this).closest('div').css('border','1px solid red');
                                any_empty++;
                            }
                            else{
                                $(this).closest('div').css('border','1px solid #ccc');
                            }
                        }
                    }
                });
                if(any_empty){
                    return null;
                }
                $('#submitAttachment').find('button[type="submit"]').prop("disabled", true);
                $('#submitAttachment').find('button[type="submit"]').css({
                    "opacity": ".2",
                    "cursor": "progress"
                });
                $.easyAjax({
                    url: form.attr('action'),
                    type: 'POST',
                    container: '#submitAttachment',
                    file: true,
                    success: function (response) {
                        if(response.success){
                            location.href = response.url;
                        }
                    }
                })
            });
        </script>        
    </body>
</html>