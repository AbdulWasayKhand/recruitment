@extends('layouts.front')

@section('header-text')
    <h1 class="hidden-sm-down"> Interview Canceled </h1>
    <h4 class="hidden-sm-up">Interview Canceled </h4>
    <style>

        .btn-outline.btn-custom {
            color: {{ $frontTheme->primary_color }};
            background-color: transparent;
            border-color: {{ $frontTheme->primary_color }};
        }

        .btn-custom:hover {
            -webkit-box-shadow: 0 2px 10px rgba(15,172,243,0.4);
            box-shadow: 0 2px 10px rgba(15,172,243,0.4);
        }
        .btn-custom, .btn-custom:hover {
            background-color: {{ $frontTheme->primary_color }};
            border-color: {{ $frontTheme->primary_color }};
            color: #fff;
        }
        .btn-custom:hover {
            color: #fff;
            background-color: {{ $frontTheme->primary_color }};
            border-color: {{ $frontTheme->primary_color }};
        }
        .btn-custom:active, .btn-custom.active, .show>.btn-custom.dropdown-toggle {
            background-color: {{ $frontTheme->primary_color }};
            border-color: {{ $frontTheme->primary_color }};
            color: #fff;
        }
    </style>
@endsection

@section('content')



    <!--
    |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
    | Working at TheThemeio
    |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
    !-->
    <section class="section bg-gray py-60">
        <div class="container">

            <div class="row gap-y align-items-center">

                <div class="col-12">
                    <h3>Interview Canceled</h3>
                    <p>Your interview has been Canceled</p>
                </div>

            </div>

        </div>
    </section>
@endsection