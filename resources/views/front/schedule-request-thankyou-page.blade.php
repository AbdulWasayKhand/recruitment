@extends('layouts.front')

@section('header-text')
    <h1 class="hidden-sm-down"> {{$pageTitle}}. You are all set. You can <a href="https://recruit.engyj.com/login">login</a> to the Engyj recruit backed for further tasks.</h1>
    <h4 class="hidden-sm-up"> {{$pageTitle}}. You are all set. You can <a href="https://recruit.engyj.com/login"></a>login to the Engyj recruit backed for further tasks.</h4>
@endsection