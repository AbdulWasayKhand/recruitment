@extends('layouts.front')

@section('header-text')
    <h1 class="hidden-sm-down"><i class="icon-ribbon"></i> {{$pageTitle}} </h1>
    <h4 class="hidden-sm-up"><i class="icon-ribbon"></i> {{$pageTitle}} </h4>
@endsection

