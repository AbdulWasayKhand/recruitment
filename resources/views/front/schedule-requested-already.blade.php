@extends('layouts.front')

@section('header-text')
    <h1 class="hidden-sm-down"> {{\Request::get('already_accepted') ?? '[manager_name]'}} has already accepted this interview request. </h1>
    <h4 class="hidden-sm-up"> {{\Request::get('already_accepted') ?? '[manager_name]'}} has already accepted this interview request. </h4>
@endsection