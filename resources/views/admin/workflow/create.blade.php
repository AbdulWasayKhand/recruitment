@extends('layouts.app')
@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
   

    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw==" crossorigin="anonymous" />
    <style>
    .card-header{color: #fff;cursor:pointer;}
    #headingThree{background-color:#27a844;color: #fff;cursor:pointer;}
    #accordion .card-body {background-color: #e0e7f4;}
    .btn-gray{background: #ececec;}
    .add-action-btn {border: 0;background: transparent;cursor:pointer}
    .add-action-btn::after{display:none !important}
    .add-action-btn:focus, .add-action-btn:hover{outline:0}
    .modal-fields select,.modal-fields input {margin-bottom:25px}
    .modal-fields input {height:37.7px}
    .modal-fields h5{margin-bottom:5px; margin-top:20px}
    .modal-header{border-bottom:0}
    .modal-footer{border-top:0}
    #actionModal4, #actionModal3, #actionModal2, #actionModal1, #emailAttachmentModal {z-index: 1111;}
    .template-content p{font-size:12px}
    .template-content label {margin-bottom: 0;font-size: 13px;}
    .template-content:not(:first-child) {display: none;}
    .select2-dropdown {z-index: 1111;}
    .select2-container--default .select2-selection--multiple .select2-selection__choice {background-color: #febd13 !important;border-color: #febd13 !important;color: #000;}
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{color: rgb(0 0 0);}
    @media screen and (min-width:767px){.modal-lg {max-width: 80%;}}
    #workfow_title {
        font-size: 25px; 
        font-weight: bold; 
        outline: none;
        border-color: transparent;
    }
    #workfow_title:focus {border-bottom: 2px solid gray;}
    #cloneable {display: none;}
    .r-stage {position: absolute;right: 10px;}
    .r-stage:hover {opacity: .7;}
    .pointer {
        cursor: 'pointer';
    }
    .modal-scroll{
        max-height:70vh;
        overflow:scroll;
    }
    </style>
@endpush

@section('content')
    <div class="row"> 
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <span class="float-sm-left pb-4">
                            <div class="d-flex align-items-center position-relative workfow_title_container" onmouseover="handlePencilVisible('edit-icon', true)" onmouseout="handlePencilVisible('edit-icon', false)">
                                <i class="fa fa-pencil position-absolute pointer" style="right:0; margin-right: 22px; visibility: hidden;" id="edit-icon" aria-hidden="true" onclick="handleIconClick(this)" ></i>
                                <i class="fa fa-close position-absolute pointer" id="close-icon" style="visibility: hidden; right:8px" aria-hidden="true" onclick="handleCloseClick(this)" ></i>
                                <input type="text" placeholder="@lang('modules.workflow.enterWorkflowNameHere')" style="padding-right: 38px;" disabled id="workfow_title" data-cid="{{$workflow->company_id}}" data-wf-id="{{$workflow->id ?? 0}}" value="{{$workflow->name}}" />
                            </div>
                            <span id='workflow_title_error' class="text-danger"></span>
                            <br />
                            <div id="stages_columns" class="@if(!Request::has('id') && Request::get('id')=='') d-none @endif">
                            <p class="d-inline">@lang('modules.workflow.uCanAddStageByClicking')</p>
                            </div>
                        </span>
                        <span class="float-sm-right">
                            <a href="{{ url('admin/workflow/trigger') }}" class="mr-2"><i class="fa fa-flash"></i> @lang('modules.workflow.manageTriggers')</a>
                            <a href="javascript:addNewStage();" class="btn btn-dark btn-sm m-l-15 mb-2"><i class="fa fa-plus-circle"></i> Add a new stage</a>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div id="accordion">

                        <div class="card" id="cloneable">
                            <div class="card-header stage_heading_"  onclick="toggleHash(this)"  data-toggle="collapse" data-target="#collapse_" aria-expanded="true">
                            <div class="r-stage r_stage_" onclick="deleteStage(this)" ><i class="fas fa-trash-alt"></i></div>
                            <h5 class="mb-0 stage_name_" >
                                
                            </h5>
                            </div>

                            <div  class="collapse show collapse_" aria-labelledby="stage_heading_" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <p class="d-inline-block mb-1"><b class="text-bold">Action</b> Following actions will be performed by the system automatically when a condidiate is moved to this stage (column).</p>
                                        <button class="float-right text-bold dropdown-toggle add-action-btn add_action_btn_"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i> Add Action</button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal1">Email</button>
                                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal2">Text Message</button>
                                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal3">Interview</button>
                                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal4">Questionnaire</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <ul class="list-group list_id_list">
                                    
                                </ul>
                            </div>
                            </div>
                        </div>

                        
                        @foreach($stages as $key => $stage)
                            <div class="card">
                                <div class="card-header" id="stage_heading_{{$stage->id}}" onclick="toggleHash(this)" style="background:{{$stage->color}};"  data-toggle="collapse" data-target="#collapse_{{$stage->id}}" aria-expanded="true">
                                    @if(!in_array($stage->type,['applied','hired','rejected']))
                                    <div class="pull-right mr-3" onmouseover="workflow_id='{{$workflow->id}}'; stage_id='{{$stage->id}}'; stage_name='{{$stage->title}}'; stage_color='{{$stage->color}}'; position='{{$stage->position}}';" data-toggle="modal" data-target="#editStageModal"><i class="fa fa-pencil" style='font-size:20px; padding-left:5px;'></i></div>
                                    <div class="r-stage" onclick="deleteStage(this)" data-stage-id='{{$stage->id}}'><i class="fas fa-trash-alt"></i></div>
                                    <div class="pull-right">
                                        
                                            @if($key != 1)
                                                <i class="fa fa-chevron-up" onclick="moveMe('{{$stage->id}}', 'above')" style='font-size:20px;'></i>
                                            @endif
                                            @if($key != ($stages->count()-3))
                                                <i class="fa fa-chevron-down" onclick="moveMe('{{$stage->id}}', 'down')" style='font-size:20px;'></i>
                                            @endif
                                            <!-- <button class="float-right text-bold dropdown-toggle add-action-btn"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-arrows-alt" style='color:white; padding-right:5px;'></i>
                                            </button> -->
                                        @if(false)
                                            <div class="dropdown-menu dropdown-menu-right">
                                                @if($key != 0 && $key != 1)
                                                    <button class="dropdown-item" type="button" onclick="moveMe('{{$stage->id}}', 'top')" >Move Top</button>
                                                    <button class="dropdown-item" type="button" onclick="moveMe('{{$stage->id}}', 'above')">Move Up</button>
                                                @endif
                                                @if($key != ($stages->count()-1))
                                                    <button class="dropdown-item" type="button" onclick="moveMe('{{$stage->id}}', 'down')">Move Down</button>
                                                    <button class="dropdown-item" type="button" onclick="moveMe('{{$stage->id}}', 'bottom')">Move Bottom</button>
                                                @endif
                                            </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    @endif
                                    <h5 class="mb-0" id="stage_name_{{$stage->id}}" >
                                        {{$stage->title}}
                                    </h5>
                                </div>

                                <div  class="collapse" id="collapse_{{$stage->id}}" aria-labelledby="stage_heading_{{$stage->id}}" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                        <?php 
                                        $email_template = App\WorkflowStageActionEmail::where('stage_id', '=', $stage->id )->where('workflow_id' , '=', $stage->workflow_id )->get();
                                        $sms_template = App\WorkflowStageActionSms::where('stage_id', '=', $stage->id )->where('workflow_id' , '=', $stage->workflow_id )->get();
                                        $questionnaire_template = App\WorkflowStageActionQuestionnaire::where('stage_id', '=', $stage->id )->where('workflow_id' , '=', $stage->workflow_id )->get();
                                        $interview_template = \App\WorkflowStageActionInterview::where('stage_id', $stage->id )->where('workflow_id', $stage->workflow_id )->get();
                                        $triggers = \App\WorkflowStageActionTrigger::where('stage_id', $stage->id )->where('workflow_id', $stage->workflow_id )->get();
                                        ?>
                                        @if($email_template->count() || $sms_template->count() || $questionnaire_template->count() ||  $interview_template->count() )
                                            <p class="mb-1 action-column-line d-inline-block"><b class="text-bold">Action</b> Following actions will be performed by the system automatically when a condidiate is moved to this stage (column).</p>
                                        @endif    
                                            <button class="float-right text-bold dropdown-toggle add-action-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onmouseover="list_id = 'list_id_{{$stage->id}}'; main_action_id = 0; main_questionnaire_id = 0;"><i class="fa fa-plus"></i> Add Action</button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal1">Send an email</button>
                                                <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal2">Send a text message</button>
                                                @if($stage->workflow_actions('interview')->count()==0 && !in_array($stage->type,['applied','hired','rejected'])) <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal3">Schedule an interview</button>@endif
                                                <button class="dropdown-item" type="button" data-toggle="modal" data-target="#actionModal4">Questionnaire</button>
                                                @if($key == 0)                                             
                                                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#changeStageModal" data-stage_id="{{$stage->id}}">@lang('modules.workflow.changeStage')</button>
                                                @endif
                                                <button class="dropdown-item" type="button" data-toggle="modal" data-target="#emailAttachmentModal" data-stage_id="{{$stage->id}}" onclick="create_doc.value = 'Create'">@lang('modules.workflow.requestDocs')</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <ul class="list-group" id="list_id_{{$stage->id}}">

                                        @foreach($email_template as $action)
                                            <li class="list-group-item" id="email_{{$action->id}}">
                                                <div class="row">
                                                    <div class="col-md-3 text-bold pt-2">Send Email</div>
                                                    <div class="col-md-6 pt-2">{{ $action->template->name }} <mark><b>From:</b></mark> {{ $from[$action->from] }} <mark><b>To:</b></mark> {{ $to[$action->to] }} </div>
                                                    <div class="col-md-3 text-right">
                                                        <a href="#" class="btn btn-sm btn-gray" data-toggle="modal" data-target="#actionModal1" data-template_id="{{$action->email_template_id}}" data-from="{{$action->from}}" data-to="{{$action->to}}" data-custom_emails="{{$action->custom_emails}}" onmouseover="main_action_id = '{{$action->id}}'; list_id = 'list_id_{{$stage->id}}'"><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:deleteAction({{$action->id}}, 'email')" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach

                                        @foreach($sms_template as $action)
                                            <li class="list-group-item" id="sms_{{$action->id}}">
                                                <div class="row">
                                                    <div class="col-md-3 text-bold pt-2">Send Text Message</div>
                                                    <div class="col-md-6 pt-2">{{ $action->template->name }} <mark><b>To:</b></mark> {{ $to[$action->to] }} </div>
                                                    <div class="col-md-3 text-right">
                                                        <a href="#" class="btn btn-sm btn-gray" data-toggle="modal" data-target="#actionModal2" data-template_id="{{$action->sms_template_id}}" data-to="{{$action->to}}"  onmouseover="main_action_id = '{{$action->id}}' ; list_id = 'list_id_{{$stage->id}}'"><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:deleteAction({{$action->id}}, 'sms')" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach

                                        @foreach($questionnaire_template as $action)
                                            <li class="list-group-item" id="questionnaire_{{$action->id}}">
                                                <div class="row">
                                                    <div class="col-md-3 text-bold pt-2">Send Questionnaire</div>
                                                    <div class="col-md-6 pt-2"> {{$action->questionnaire->name ?? ""}} @if($action->questionnaire_type=='external')<mark><b>With</b></mark> {{$action->template->name ?? ""}} @endif</div>
                                                    <div class="col-md-3 text-right">
                                                        <a href="#" class="btn btn-sm btn-gray" data-toggle="modal" data-target="#actionModal4" data-template_id="{{$action->email_template_id}}" data-questionnaire_id="{{$action->questionnaire_id}}" data-type="{{$action->questionnaire_type}}"  onmouseover="main_questionnaire_id = '{{$action->id}}' ; list_id = 'list_id_{{$stage->id}}'"><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:deleteQuestionnaire({{$action->id}})" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        @foreach($interview_template as $action)
                                            @php($questionnaire_ids = \App\WorkflowStageActionInterviewQuestionnaire::where('workflow_stage_action_interview_id',$action->id)->pluck('questionnaire_id'))
                                            @php($int_questionnaires = \App\Questionnaire::whereIn('id',$questionnaire_ids)->pluck('name'))
                                            @php($int_questionnaires = $int_questionnaires->count() ? implode(', ',$int_questionnaires->toArray()) : '')
                                            <li class="list-group-item" id="interview_{{$action->id}}">
                                                <div class="row">
                                                    <div class="col-md-3 text-bold pt-2">Send Interview</div>
                                                    <div class="col-md-6 pt-2"> Send Interview Invite to Candidate </div>
                                                    <div class="col-md-3 text-right">
                                                        <a href="#" 
                                                            class="btn btn-sm btn-gray" 
                                                            data-toggle="modal" 
                                                            data-target="#actionModal3" 
                                                            data-available_days="{{$action->available_days}}" 
                                                            data-interview_invite="{{$action->interview_invite}}" 
                                                            data-employees="<?= str_replace('"', '' , str_replace('[', '', str_replace(']', '',  $action->employees))) ?>" 
                                                            data-timeslot="{{$action->timeslot}}"  
                                                            data-session_title="{{$action->session_title}}" 
                                                            data-show_locations="{{$action->show_location}}" 
                                                            data-same_day_schedule="{{$action->same_day_schedule}}" 
                                                            data-email_invite="{{$action->email_invite}}"
                                                            data-email_schedule="{{$action->email_schedule}}"
                                                            data-email_reschedule="{{$action->email_reschedule}}"
                                                            data-email_cancelled="{{$action->email_cancelled}}"
                                                            data-email_reminder="{{$action->email_reminder}}"
                                                            data-sms_invite="{{$action->sms_invite}}"
                                                            data-sms_schedule="{{$action->sms_schedule}}" 
                                                            data-sms_reschedule="{{$action->sms_reschedule}}" 
                                                            data-sms_cancelled="{{$action->sms_cancelled}}" 
                                                            data-sms_reminder="{{$action->sms_reminder}}"
                                                            data-if_pass="{{$action->if_pass}}"
                                                            data-if_fail="{{$action->if_fail}}"
                                                            data-if_canceled="{{$action->if_canceled}}"
                                                            questionnaires="{{count($questionnaire_ids) ? implode(',',$questionnaire_ids->toArray()) : ','}}" 
                                                            onmouseover="main_interview_id = '{{$action->id}}' ; list_id = 'list_id_{{$stage->id}}'"
                                                            ><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:deleteInterview({{$action->id}})" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        @foreach($triggers as $trigger)
                                        <li class="list-group-item" id="change_stage_{{$trigger->id}}">
                                            <div class="row">
                                                <div class="col-md-3 text-bold pt-2"> @lang('modules.workflow.changeStage')</div>
                                                <div class="col-md-6 pt-2"><span class="fa fa-flash"></span> @lang('modules.workflow.trigger.trigger') : If "{{ $trigger->trigger->name }}" <mark>To:</mark> {{ $trigger->stage->title }} </div>
                                                <div class="col-md-3 text-right">
                                                <a href="#" class="btn btn-sm btn-gray" data-toggle="modal" data-target="#changeStageModal" data-stage_id="{{$stage->id}}" data-to_stage="{{ $trigger->stage->id }}" data-trigger_id="{{ $trigger->trigger->id }}" data-workflow_stage_action_id="{{$trigger->id}}" data-action_id="{{$action->id ?? ''}}"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:deleteAction({{$trigger->id}}, 'change_stage')" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                        @foreach($stage->email_attachments as $email_attachment)
                                        <li class="list-group-item" id="change_stage_{{$email_attachment->id}}">
                                            <div class="row">
                                                <div class="col-md-3 text-bold pt-2"> @lang('modules.workflow.requestDocs')</div>
                                                <div class="col-md-6 pt-2">{{ $email_attachment->template->name }} <mark><b>From:</b></mark> {{ $from[$email_attachment->from] }} <mark></mark></div>
                                                <div class="col-md-3 text-right">
                                                <a href="#" class="btn btn-sm btn-gray" data-toggle="modal" data-target="#emailAttachmentModal" data-stage_id="{{$stage->id}}" data-email_template_id="{{ $email_attachment->email_template_id }}" data-doc-titles="{{ implode(',',$email_attachment->title) }}" data-from="{{ $email_attachment->from }}" data-workflow_stage_action_email_attachment_id="{{$email_attachment->id}}" data-action_id="{{$action->id ?? ''}}" onmouseover="create_doc.value = 'Update'"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:deleteAction({{$email_attachment->id}}, 'email_attachment')" class="btn btn-sm btn-gray"><i class="fas fa-trash-alt"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 1-->
    <div class="modal fade" id="actionModal1" tabindex="-1" role="dialog" aria-labelledby="actionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="modal-header">
                            <h4 class="modal-title text-bold" id="actionModalLabel">@lang('modules.workflow.newActionSendAnEmail')</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body py-0">
                            <div class="row modal-fields">
                                <div class="col-md-12">
                                    <h5 class="d-inline-block">Select an email template</h5><a target="_blank" class="d-inline-block pull-right mt-3" href="{{url('admin/template')}}">@lang('modules.workflow.manageEmailTemplates')</a>
                                    <select id="email_template_id" name="email_template_id" onchange="$('.email-template').hide();$('#ET_' + this.value ).show();" class="form-control">
                                        @foreach($email_templates as $template)
                                            <option value="{{$template->id}}">{{$template->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <h5>Email From</h5>
                                    <select name="email_from" id="email_from" class="form-control">
                                        <option value="hiring_manager">Admin</option>
                                        <option value="company">Company</option>
                                        <option value="engyj">Engyj</option>    
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <h5>Email To</h5>
                                    <select name="email_to" id="email_to" onchange="
                                        if(this.value == 'custom'){
                                            $('#custom_emails').show();
                                        }else{
                                            $('#custom_emails').hide();
                                        }
                                    " class="form-control">
                                        <option value="hiring_manager">Admin</option>
                                        <option value="candidate">Candidate</option>
                                        <option value="hiring_team">Hiring Team</option>
                                        <option value="custom">Custom</option>
                                    </select>
                                </div>
                                <div class="col-md-12" id="custom_emails" style="display: none;">
                                    <h5>Custom Emails</h5>
                                    <input type="text" class="form-control" id='custom_email' placeholder="Comma-Separated Emails"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="insertAction(list_id, 'email')">Insert</button>
                        </div>
                    </div>
                    <div class="col-md-5">
                        @foreach($email_templates as $template)
                            <div class="template-content email-template" id="ET_{{$template->id}}">
                                <div class="modal-header pl-md-0">
                                    <h5 class="modal-title text-bold" id="name">{{$template->name}}</h5>
                                </div>
                                <div class="modal-body pl-md-0 pt-0">
                                    <label>SUBJECT</label>
                                    <p id="subject">{{$template->subject}}</p>
                                    <label>MESSAGE</label>
                                    <p id="message">
                                        {!! $template->content !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Email Attachment Modal-->
    <div class="modal fade" id="emailAttachmentModal" tabindex="-1" role="dialog" aria-labelledby="actionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form method="post" action="{{ route('admin.workflow.email-attachment') }}" id="email_attachment_form">
            @csrf
            <input type="hidden" name="workflow_id" value="{{$workflow->id ?? 0}}">
            <input type="hidden" name="stage_id">
            <input type="hidden" name="workflow_stage_action_email_attachment_id">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="modal-header">
                            <h4 class="modal-title text-bold" id="actionModalLabel">@lang('modules.workflow.newActionSendAnEmailAttach')</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body py-0">
                            <div class="row modal-fields modal-scroll">
                                <div class="col-md-12">
                                    <h5 class="d-inline-block">Select an email template</h5><a target="_blank" class="d-inline-block pull-right mt-3" href="{{url('admin/template')}}">@lang('modules.workflow.manageEmailTemplates')</a>
                                    <select name="email_template_id" onchange="$('.email-template').hide();$('#EAT_' + this.value ).show();" class="form-control">
                                        @foreach($email_templates as $template)
                                            <option value="{{$template->id}}">{{$template->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <h5>Email From</h5>
                                    <select name="from" id="email_from" class="form-control" style="margin-bottom: 0;">
                                        <option value="hiring_manager">Admin</option>
                                        <option value="company">Company</option>
                                        <option value="engyj">Engyj</option>    
                                    </select>
                                </div>
                                <div class="col-md-12" id="attachments_title"></div>
                            </div>
                        </div>
                        <div class="col-md-12 pb-2 pl-3"><strong>Note</strong>: @lang('modules.workflow.addDocsNote')</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="add_doc_title"><i class="fa fa-plus pr-1"></i> Document Titles</button>
                            <input type="submit" class="btn btn-success" id="create_doc" value="Create" >
                        </div>
                    </div>
                    <div class="col-md-5">
                        @foreach($email_templates as $template)
                            <div class="template-content email-template" id="EAT_{{$template->id}}">
                                <div class="modal-header pl-md-0">
                                    <h5 class="modal-title text-bold" id="name">{{$template->name}}</h5>
                                </div>
                                <div class="modal-body pl-md-0 pt-0">
                                    <label>SUBJECT</label>
                                    <p id="subject">{{$template->subject}}</p>
                                    <label>MESSAGE</label>
                                    <p id="message">
                                        {!! $template->content !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!-- Modal 2-->
    <div class="modal fade" id="actionModal2" tabindex="-1" role="dialog" aria-labelledby="actionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="modal-header">
                            <h4 class="modal-title text-bold" id="actionModalLabel">@lang('modules.workflow.newActionSendAnText')</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body py-0">
                            <div class="row modal-fields">
                                <div class="col-md-12">
                                    <h5 class="d-inline-block">Select a text message template</h5><a target="_blank" href="{{url('admin/template?text_message=1')}}" class="d-inline-block pull-right mt-3">@lang('modules.workflow.manageTextTemplates')</a>
                                    <select id="sms_template_id" class="form-control" onchange="$('.sms-template').hide();$('#ST_' + this.value ).show();">
                                        @foreach($sms_templates as $template)
                                            <option value="{{$template->id}}">{{$template->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <h5>Text Message To</h5>
                                    <select id="sms_to" class="form-control">
                                        <option value="candidate">Candidate</option>
                                        <option value="hiring_manager">Admin</option>
                                        <option value="hiring_team">Hiring Team</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="insertAction(list_id, 'sms')">Insert</button>
                        </div>
                    </div>
                    <div class="col-md-5">
                        @foreach($sms_templates as $template)
                            <div class="template-content sms-template" id="ST_{{$template->id}}">
                                <div class="modal-header pl-md-0">
                                    <h5 class="modal-title text-bold">{{$template->name}}</h5>
                                </div>
                                <div class="modal-body pl-md-0 pt-0">
                                    <label>MESSAGE</label>
                                    <p>
                                        {{$template->content}}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 3-->
    <div class="modal fade" id="actionModal3" role="dialog" aria-labelledby="actionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="row">
                <div class="col-md-6">
                        <div class="modal-header">
                            <h4 class="modal-title text-bold" id="actionModalLabel">@lang('modules.workflow.newActionScheduleAnInterview')</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body py-0 modal-scroll">
                            <small>@lang('modules.workflow.scheduleAnInterviewNote')</small>
                            <div class="row">
                                <div class="col-md-12 pt-4">
                                    <h5>Interview Session Title</h5>
                                    <input type="text" name="session_title" class="form-control">
                                    <label class="mt-1">
                                        <input type="checkbox" name="show_locations" class="form-control" >
                                        Show Locations
                                    </label>
                                    <label class="mt-1 pl-2">
                                        <input type="checkbox" name="same_day_schedule" class="form-control" >
                                        Same Day Schedule
                                    </label>
                                </div>
                                <div class="col-md-12 pt-4">
                                    <h5>@lang('modules.workflow.selectIntrnalQuestionnaire')</h5>
                                    <small>@lang('modules.workflow.intrnalQuestionnaireNote')</small>
                                    <select class="select2 m-b-10 select2-multiple" id="internal-questionnaire" style="width: 100%; " multiple="multiple"
                                            name="internal_questionnaire[]">
                                        @foreach($questionnaires as $questionnaire)
                                            <option value="{{$questionnaire->id}}">{{$questionnaire->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-12 pt-4">
                                    <h5>@lang('modules.workflow.interviewingManagers')</h5>
                                    <small>@lang('modules.workflow.interviewingManagersNote')</small>
                                    <select class="select2 m-b-10 select2-multiple" id="employees" style="width: 100%; " multiple="multiple"
                                            name="employees[]">
                                        @foreach(\App\User::where('company_id', '=', $user->company_id)->get() as $emp)
                                            <option value="{{$emp->id}}">{{$emp->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="mt-1">
                                        <label>
                                            <input type="checkbox" name="interview_invite" id="interview_invite">
                                            Auto accept if only one Hiring Manager is available
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 pt-3" >
                                <input type="hidden" name="if_pass" />
                                <input type="hidden" name="if_fail" />
                                <input type="hidden" name="if_canceled" />
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>If Pass Move To</label>
                                            <div id="if_pass_cloner"></div>
                                            <select id="if_pass" class="form-control" style="display: none;">
                                                <option value="0">None</option>
                                                @foreach($stages as $stage)
                                                    <option value="{{$stage->id}}" id="pass_{{$stage->id}}" class="status_opt">{{ucwords($stage->title)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>If Fail Move To</label>
                                            <div id="if_fail_cloner"></div>
                                            <select id="if_fail" class="form-control" style="display: none;">
                                                <option value="0">None</option>
                                                @foreach($stages as $stage)
                                                    <option value="{{$stage->id}}" id="fail_{{$stage->id}}" class="status_opt">{{ucwords($stage->title)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>If Canceled Move To</label>
                                            <div id="if_canceled_cloner"></div>
                                            <select id="if_canceled" class="form-control" style="display: none;">
                                                <option value="0">None</option>
                                                @foreach($stages as $stage)
                                                    <option value="{{$stage->id}}" id="canceled_{{$stage->id}}" class="status_opt">{{ucwords($stage->title)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-12 pt-3" >
                                        <h5>@lang('modules.workflow.calendarLimit')</h5>
                                        <small>@lang('modules.workflow.calendarLimitNote')</small>
                                        <select id="available_days" name="available_days" class="form-control">
                                            <option value='7'>Next 7 Days</option>
                                            <option value='14'>Next 14 Days</option>
                                        </select>

                                    </div>
                                    <div class="col-md-12 pt-3">
                                        <h5>Interview Time-Slot</h5>
                                        <select id="timeslot" name="timeslot" class="form-control">
                                            <option selected="" value="15">15 Minutes</option>
                                            <option selected="" value="30">30 Minutes</option>
                                            <option value="45">45 Minutes</option>
                                            <option value="60">1 Hour</option>
                                            <option value="90">1 Hour and 30 Minutes</option>
                                            <option value="120">2 Hour</option>
                                            <option value="180">3 Hour</option> 
                                        </select>
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="addInterview(list_id)">Insert</button>
                        </div>
                    </div>
                    <?php 
                        $titles = [
                                'invite' => 'Interview Invite', 
                                'schedule' => 'Interview Scheduled', 
                                'reschedule' => 'Interview Re-scheduled', 
                                'cancelled' => 'Interview Canceled', 
                                'reminder' => 'Interview Reminder'
                            ];
                        ?>
                    <div class="col-md-6">
                        <div style="padding: 20px;">
                            @foreach(['email'=>'Email Template', 'sms'=>'Text Message Template'] as $hed_val=>$heading)
                                <h3>{{$heading}}</h3>
                                <table>
                                    @foreach($titles as $key=>$title)
                                        @if($hed_val == 'email' && $key == 'reminder') @else
                                        <tr>
                                            <th>{{$title}}</th>
                                            <td style="padding-left: 10px;">
                                                <select class="form-control" id="{{$hed_val}}_{{$key}}">
                                                    <option value="0">Default</option>
                                                        @foreach(($hed_val == 'email' ? $email_templates : $sms_templates) as $template)
                                                            <option value="{{$template->id}}">{{$template->name}}</option>
                                                        @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </table>
                            @endforeach
                        </div>
                        <input type="hidden" name='alerts' />
                    </div>
                </div>

                
            </div>
        </div>
    </div>
    <!-- Modal 4-->
    <div class="modal fade" id="actionModal4" tabindex="-1" role="dialog" aria-labelledby="actionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="modal-header">
                            <h4 class="modal-title text-bold" id="actionModalLabel">@lang('modules.workflow.newActionQuestionnaire')</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body py-0">
                            <div class="row modal-fields">
                            <div class="col-md-12">
                                    <h5>Select Type</h5>
                                    <select id="q_type" class="form-control">
                                        <option value="internal">Internal</option>
                                        <option value="external">External</option>
                                    </select>
                                </div>
                                <div class="col-md-12" id="questionnaire_email_template" style="display:none;">
                                    <h5>Select an email template</h5>
                                    <select id="q_email_template_id" onchange="$('.email-template').hide();$('#QET_' + this.value ).show();" class="form-control">
                                        @foreach($email_templates as $template)
                                            <option value="{{$template->id}}">{{$template->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <h5>Select a Questionnaire</h5>
                                    <select name="questionnaire_id"  id="questionnaire_id" class="form-control">
                                        @foreach($questionnaires as $questionnaire)
                                            <option value="{{$questionnaire->id}}">{{$questionnaire->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="addQuestionnaire(list_id)">Insert</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    @foreach($email_templates as $template)
                        <div class="template-content email-template" id="QET_{{$template->id}}">
                            <div class="modal-header pl-md-0">
                                <h5 class="modal-title text-bold">{{$template->name}}</h5>
                            </div>
                            <div class="modal-body pl-md-0 pt-0">
                                <label>SUBJECT</label>
                                <p>{{$template->subject}}</p>
                                <label>MESSAGE</label>
                                <p>
                                    {!! $template->content !!}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
               
    </div>

    <div class="modal fade bs-modal-md in" id="scheduleDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button  type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="editStageModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <form method="post" action="{{ route('admin.workflow.save-stage') }}" id="stage_edit_form">
            <input type="hidden" name="workflow_id" id="workflow_id">
            <div class="modal-content">
            <link rel="stylesheet" href="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
            <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="icon-pencil"></i> Edit Stage 
                    </h4>
                    <button type="button" id="close_modal" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label class="d-block control-label">@lang('modules.jobApplication.statusName')</label>
                                        <input type="text" id="stage_name" name="stage_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label class="d-block control-label">@lang('modules.jobApplication.statusColor')</label>
                                        <span class="dot" id="stageColor1" style="background-color: #000000;" onclick="handleSelectedColor('stageColor1', '#000000')">
                                            <input type="radio" value="#000000" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <span class="dot" id="stageColor2" style="background-color: #ea4335;" onclick="handleSelectedColor('stageColor2', '#ea4335')">
                                            <input type="radio" value="#ea4335" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <span class="dot" id="stageColor3" style="background-color: #Fbbc05;" onclick="handleSelectedColor('stageColor3', '#Fbbc05')">
                                            <input type="radio" value="#Fbbc05" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <span class="dot" id="stageColor4" style="background-color: #34a853;" onclick="handleSelectedColor('stageColor4', '#34a853')">
                                            <input type="radio" value="#34a853" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <span class="dot" id="stageColor5" style="background-color: #2c97df;" onclick="handleSelectedColor('stageColor5', '#2c97df')">
                                            <input type="radio" value="#2c97df" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <span class="dot" id="stageColor6" style="background-color: #9c56b8;" onclick="handleSelectedColor('stageColor6', '#9c56b8')">
                                            <input type="radio" value="#9c56b8" name="stage_color" class="dot" style="opacity: 0;">
                                        </span>
                                        <input type="text" name="stage_color" id="stage_color" value="#ea4335" style="visibility: hidden" />
                                    </div>
                                </div>
                            </div>
                            @php($firstStage = $workflow->stages->where('position',1)->first() ?? new \App\WorkflowStage )
                            {{--<div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label class="d-block control-label">@lang('modules.workflow.afterStage')</label>
                                        <select class="form-control" id="position" name="position">
                                            @foreach($stages as $stage)
                                            <option value="{{$stage->id}}">{{'After '.ucwords($stage->title)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="stage_id" id="stage_id">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- CHANGE STAGE MODAL --}}
    <div class="modal fade bs-modal-md in" id="changeStageModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <form method="post" action="{{ route('admin.workflow.change-stage') }}" id="stage_change_form">
            <input type="hidden" name="workflow_id" value="{{$workflow->id ?? 0}}">
            <input type="hidden" name="stage_id">
            <input type="hidden" name="workflow_stage_action_id">
            <div class="modal-content">
            <link rel="stylesheet" href="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
            <div class="modal-header">
                    <h4 class="modal-title">
                        @lang('modules.workflow.changeStage')
                    </h4>
                    <button type="button" id="close_modal" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label class="d-block control-label">@lang('modules.workflow.toStage')</label>
                                        <select class="form-control" name="to_stage">
                                            @foreach($stages as $key=>$stage)
                                                <option id="stg_{{$stage->id}}" value="{{$stage->id}}">{{ucwords($stage->title)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <label for="trigger_id" class="d-block control-label">@lang('modules.workflow.trigger.trigger')</label>
                                        <select class="form-control" id="trigger_id" name="trigger_id">
                                            @foreach(\App\WorkflowTrigger::where('company_id', '=', $user->company_id)->get() as $trigger)
                                            <option value="{{$trigger->id}}">{{ucwords($trigger->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">@lang('app.close')</button>
                    <input type="submit" id="trg" class="btn btn-success" name="action" value="Change" >
                </div>
            </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
<script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    
    <script>

        function handlePencilVisible(id, isVisible) {
            let el = document.getElementById(id);
            isVisible ? el.style.visibility = 'visible' : el.style.visibility = 'hidden';
        }

        // custom colorpicker logic
        function handleSelectedColor(id, color) {
            document.getElementById('stage_color').value = color;
            let element = document.getElementById(id);    
            let allSelectedElements = document.getElementsByClassName("selected-dot");
            for(let i = 0; i < allSelectedElements.length; i++){
                allSelectedElements[i].classList.remove('selected-dot');
            }
            if(element.className.includes('selected-dot')) element.classList.remove('selected-dot');
            else element.classList.add('selected-dot');
        }
        // custom colorpicker logic

        $(function() {
            $('#cp2').colorpicker();
        });
        $(document).ready(function () {
            $('#actionModal1').on('show.bs.modal', function(e) {
                $(this).find('.modal-dialog').removeClass('modal-lg');
                $(this).find('.modal-dialog').css("max-width","900px");
                var $to = $(e.relatedTarget).data('to'); 
                $(this).find('select[name="email_to"]').val($to);
                $(this).find('select[name="email_from"]').val($(e.relatedTarget).data('from'));
                $(this).find('#email_template_id').val($(e.relatedTarget).data('template_id'));
                // If custom emails
                ($to=='custom') ? $(this).find('#custom_emails').show() : $(this).find('#custom_emails').hide();
                $(this).find('#custom_email').val($(e.relatedTarget).data('custom_emails'));
                $('.email-template').hide();
                $(this).find('#ET_'+$(e.relatedTarget).data('template_id')).show();
                if($(e.relatedTarget).data('template_id')){
                    var btn_txt = 'Update';
                }
                else{
                    var btn_txt = 'Insert';
                }
                $(this).find('.modal-footer button').text(btn_txt);
            });
            $('#actionModal2').on('show.bs.modal', function(e) {
                $(this).find('.modal-dialog').removeClass('modal-lg');
                $(this).find('.modal-dialog').css("max-width","900px");
                $(this).find('#sms_template_id').val($(e.relatedTarget).data('template_id'));
                $(".template-content").hide();
                $(this).find('#sms_to').val($(e.relatedTarget).data('to'));
                $(this).find('#ST_'+$(e.relatedTarget).data('template_id')).show();
                if($(e.relatedTarget).data('to')){
                    var btn_txt = 'Update';
                }
                else{
                    var btn_txt = 'Insert';
                }
                $(this).find('.modal-footer button').text(btn_txt);
            });
            $('#actionModal4').on('show.bs.modal', function(e) { 
                $(this).find('#q_email_template_id').val($(e.relatedTarget).data('template_id'));
                $(this).find('#questionnaire_id').val($(e.relatedTarget).data('questionnaire_id'));
                $(this).find('#q_type').val($(e.relatedTarget).data('type'));
                $(this).find('#q_type').change();
                $(".template-content").hide();
                $(this).find('#QET_'+$(e.relatedTarget).data('template_id')).show();
                if($(e.relatedTarget).data('template_id')){
                    var btn_txt = 'Update';
                }
                else{
                    var btn_txt = 'Insert';
                }
                $(this).find('.modal-footer button').text(btn_txt);
            });
            // After Shown
            $('#actionModal3').on('shown.bs.modal', function(e) {
                ($(e.relatedTarget).attr('data-available_days')) ? $("#available_days").val($(e.relatedTarget).attr('data-available_days')) : $("#available_days").val(7);
                ($(e.relatedTarget).attr('data-timeslot')) ? $("#timeslot").val($(e.relatedTarget).attr('data-timeslot')) : $("#timeslot").val(15);
            });
            // Before Show
            $('#actionModal3').on('show.bs.modal', function(e) {
                $('.status_opt').removeAttr("disabled");
                document.querySelector('input[name=session_title]').style.borderColor = 'lightgray';
                alerts = [];
                ['email', 'sms'].forEach(function(a){
                    ['invite', 'schedule', 'reschedule', 'cancelled', 'reminder'].forEach(function(b){
                        $key = a + '_' + b;
                        $('#' + $key).val($(e.relatedTarget).data($key) ? $(e.relatedTarget).data($key) : 0);
                    });
                });
                $(this).find('input[name="session_title"]').val($(e.relatedTarget).data('session_title'));
                ['fail', 'pass', 'canceled'].forEach(function(e){
                    $cloner = document.getElementById('if_' + e + '_cloner');
                    $cloner.innerHTML = '';
                    $cln = document.getElementById('if_' + e).cloneNode(true);
                    $cln.style.display = 'block';
                    $cln.classList.add('if_' + e);
                    $cloner.appendChild($cln);
                    $remove = document.getElementById(e + '_' + list_id.split('_')[2]);
                    $remove.remove();
                });
                document.getElementsByClassName('if_pass')[0].value = ($(e.relatedTarget).data('if_pass') ? $(e.relatedTarget).data('if_pass') : 0);
                document.getElementsByClassName('if_fail')[0].value = ($(e.relatedTarget).data('if_fail') ? $(e.relatedTarget).data('if_fail') : 0);
                document.getElementsByClassName('if_canceled')[0].value = ($(e.relatedTarget).data('if_canceled') ? $(e.relatedTarget).data('if_canceled') : 0);
                if($(e.relatedTarget).data('show_locations')){
                    $('input[name="show_locations"]').iCheck('check');
                }else{
                    $('input[name="show_locations"]').iCheck('uncheck');
                }
                $(e.relatedTarget).data('same_day_schedule') ?
                    $('input[name="same_day_schedule"]').iCheck('check') :
                    $('input[name="same_day_schedule"]').iCheck('uncheck');
                if($(e.relatedTarget).data('interview_invite')){
                    $('input[name="interview_invite"]').iCheck('check');
                }else{
                    $('input[name="interview_invite"]').iCheck('uncheck');
                }
                $emp = $(e.relatedTarget).attr('data-employees') ? $(e.relatedTarget).attr('data-employees').split(",") : null;
                $this_quest = $(this).find('#employees');
                $this_quest.val($emp).trigger("change");

                if($(e.relatedTarget).attr('data-available_days') != 0){
                    available_days.value = $(e.relatedTarget).attr('data-available_days');
                }else{
                    available_days.value = 7;
                }
                if($(e.relatedTarget).attr('data-timeslot') != 0){
                    timeslot.value = $(e.relatedTarget).attr('data-timeslot');
                }else{
                    timeslot.value = 30;
                }
                if($(e.relatedTarget).attr('questionnaires')){
                    $questionnaires = $(e.relatedTarget).attr('questionnaires').indexOf(',') ? $(e.relatedTarget).attr('questionnaires').split(',') : $(e.relatedTarget).attr('questionnaires');
                    var $this_quest = $(this).find('#internal-questionnaire');
                    $this_quest.val($questionnaires).trigger("change");
                }

                if($(e.relatedTarget).data('session_title')){
                    var btn_txt = 'Update';
                }
                else{
                    var btn_txt = 'Insert';
                }
                $(this).find('.modal-footer button').text(btn_txt);
            });
            $('#editStageModal').on('show.bs.modal', function(e) {
                $('#cp2').colorpicker();
                if(stage_color && stage_id && stage_name){
                    $(this).find('#workflow_id').val(workflow_id);
                    $(this).find('#stage_id').val(stage_id);
                    $(this).find('#stage_name').val(stage_name);
                    $(this).find('#stage_color').val(stage_color);
                    $position_opts = '<option>@lang('modules.jobApplication.noChange')</option>';
                    if(position > 1){
                        $position_opts = $position_opts+'<option value="before_first">Before {{ucwords($firstStage->title)}}</option>';
                    }
                    $(this).find('#position').html($position_opts+$(this).find('#position').html());
                    $(this).find('#position').val(position);
                    $(this).find('.input-group-append .colorpicker-input-addon i').css('background',stage_color);
                    // custom colorpicker logic
                    let allColorElements = document.getElementsByClassName("dot");
                    for(let i = 0; i < allColorElements.length; i++){
                        let child = allColorElements[i].querySelector('input[name="stage_color"]');
                        if(child && child.value === stage_color) allColorElements[i].classList.add('selected-dot');
                        else if (allColorElements[i].className.includes('selected-dot')) allColorElements[i].classList.remove('selected-dot');
                    }
                    // custom colorpicker logic
                }
            });
            $('#changeStageModal').on('show.bs.modal', function(e) {
                $stg = document.getElementById('stg_' + $(e.relatedTarget).data('stage_id'));
                if($stg){
                    $stg.remove();
                }
                if(typeof $(e.relatedTarget).data('workflow_stage_action_id') === 'undefined'){
                    trg.value = 'Create';
                }else{
                    trg.value = 'Update';
                }
                $(this).find('input[name="stage_id"]').val($(e.relatedTarget).data('stage_id'));
                $(this).find('input[name="action_id"]').val($(e.relatedTarget).data('action_id'));
                $(this).find('select[name="to_stage"]').val($(e.relatedTarget).data('to_stage'));
                $(this).find('select[name="trigger_id"]').val($(e.relatedTarget).data('trigger_id'));
                $(this).find('input[name="workflow_stage_action_id"]').val($(e.relatedTarget).data('workflow_stage_action_id'));
            });
            // When Email Attachment Modal Show
            $('#emailAttachmentModal').on('show.bs.modal', function(e) {
                $(this).find('.modal-dialog').removeClass('modal-lg');
                $(this).find('.modal-dialog').css("max-width","900px");
                // if(typeof $(e.relatedTarget).data('workflow_stage_action_id') === 'undefined'){
                //     $(this).find('input[type="submit"]').val('Create');
                // }else{
                //     $(this).find('input[type="submit"]').val('Update');
                // }
                $(this).find('input[name="stage_id"]').val($(e.relatedTarget).data('stage_id'));
                $(this).find('input[name="action_id"]').val($(e.relatedTarget).data('action_id'));
                $(this).find('select[name="email_template_id"]').val($(e.relatedTarget).data('email_template_id'));
                // titles fields
                $titles = $(e.relatedTarget).data('doc-titles') ? $(e.relatedTarget).data('doc-titles').split(',') : [];
                $fields = '<div class="row">';
                var $i = 0;
                $.each($titles,function(k,v){
                    $i++;
                    $fields += '<div class="col-md-6">';
                    $fields += `
                    <h5>Document ${$i}</h5>
                        <input type="text" name="doc_titles[]" value="${v}" class="form-control col-md-11 d-inline-block">
                        <span class="fa fa-trash text-danger delete-attachment-title" style="font-size: 18px;"></span>
                    </div>`;
                    if($i%2==0){ 
                        $titles += `</div><div class="row">`;
                    }
                });
                $("#attachments_title").html($fields+'</div>');
                $(e.relatedTarget).data('doc-titles')
                $(this).find('select[name="from"]').val($(e.relatedTarget).data('from'));
                $(this).find('input[name="workflow_stage_action_email_attachment_id"]').val($(e.relatedTarget).data('workflow_stage_action_email_attachment_id'));
                $(this).find('.email-template').hide();
                $(this).find('#EAT_'+$(e.relatedTarget).data('email_template_id')).show();
            });
            $('body').on('submit','#stage_edit_form', function(e) {
                if($(this).find("#stage_name").val() == "" || $(this).find("#stage_name").val() == ""){
                    return false;
                }
                $("#editStageModal").find('button.btn').prop("disabled", true);
                $("#editStageModal").find('button.btn').css({
                    "opacity": ".2",
                    "cursor": "progress"
                });
            });
            // When Change Stage Form submit
            $('body').on('submit','#stage_change_form', function(e) {
                $("#changeStageModal").find('button.btn').prop("disabled", true);
                $("#changeStageModal").find('button.btn').css({
                    "opacity": ".2",
                    "cursor": "progress"
                });
            });
            // When Email Attachment Form submit
            $('body').on('submit','#email_attachment_form', function(e) {
                if($(this).find('input[name="doc_titles[]"]').length == 0){
                    alert('Please add atleast one document');
                    e.preventDefault();
                    return;
                }else{
                    if($(this).find('input[name="doc_titles[]"]').val()==''){
                        $(this).find('input[name="doc_titles[]"]').css('border','1px solid red');
                        e.preventDefault();
                        return false;
                    }
                }
                
                $(this).find('input[name="doc_titles[]"]').css('border','1px solid #ced4da');
                $(this).find('input[type="submit"]').prop("disabled", true);
                $(this).find('input[type="submit"]').css({
                    "opacity": ".2",
                    "cursor": "progress"
                });
            });

            var counter = 0;
            // delete documents title fields
            $("body").on('click','.delete-attachment-title',function(){
                counter = $("#emailAttachmentModal").find('#attachments_title input[type=text]').length;
                $(this).closest('div').remove();
                counter--;
            });
            
            $("body").on('click','#add_doc_title',function(){
                counter = $("#emailAttachmentModal").find('#attachments_title input[type=text]').length;
                counter++;
                if(counter%2==0){
                        $("#attachments_title").find('.row').last().find('.col-md-12').removeClass('col-md-12');
                        $("#attachments_title").find('.row').last().find('div').addClass('col-md-6');
                        $("#attachments_title").find('.row').last().append(`<div class="col-md-6">
                            <h5>Document ${counter}</h5>
                            <input type="text" name="doc_titles[]" class="form-control col-md-11 d-inline-block">
                            <span class="fa fa-trash text-danger delete-attachment-title" style="font-size: 18px;"></span>
                        </div>`);
                }
                else{
                    $("#attachments_title").append(`<div class="row">
                        <div class="col-md-12">
                            <h5>Document ${counter}</h5>
                            <input type="text" name="doc_titles[]" class="form-control col-md-11 d-inline-block">
                            <span class="fa fa-trash text-danger delete-attachment-title" style="font-size: 18px;"></span>
                        </div>
                    </div>`);
                }
            }); 
        });
        var workflow_name = '';
        var list_id = '';
        var main_action_id = 0;
        var main_questionnaire_id = 0;
        var main_interview_id = 0;
        function deleteStage(elm){
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('admin.workflow.delete-stage') }}",
                        type: 'POST',
                        data: {
                            sid: elm.getAttribute('data-stage-id'),
                            '_token': '{{ csrf_token() }}',
                        },
                        success: function (response) {
                            window.location.hash = '';
                            window.location.reload();
                        }
                    })
                }
            });
        }
        
        if(window.location.hash != ''){
            document.querySelector('#collapse_' + window.location.hash.split('_')[2]).classList.add('show');
        }else{
            if(document.querySelectorAll('.collapse')[1]){
                document.querySelectorAll('.collapse')[1].classList.add('show');
            }
        }

        function toggleHash(elm) {
            if(window.location.hash == "#" + elm.id){
                window.location.hash = '';
            }else{
                window.location.hash = elm.id;
            }
        }

        function deleteQuestionnaire(aid){


            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteActionWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var myurl = "{{ route('admin.workflow.delete-questionnaire') }}";
                    mydata = {
                        action_id: aid,
                        '_token': '{{ csrf_token() }}',
                    }
                    $.ajax({
                        url: myurl,
                        type: 'POST',
                        data: mydata,
                        success: function (response) {
                            // $("#questionnaire_" + aid).remove();
                            location.reload();
                        }
                    })
                }
            });


        }

        function deleteInterview(interview_id){
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteActionWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('admin.workflow.action.interview.delete') }}",
                        type: 'POST',
                        data: { interview_id: interview_id, '_token': '{{ csrf_token() }}' },
                        success: function (response) {
                            location.reload();
                            // $("#interview_" + interview_id).remove();
                        }
                    })
                }
            });


        }

        function deleteAction(aid, type){


            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteActionWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var myurl = "{{ route('admin.workflow.delete-email-action') }}";
                        if(type == 'sms'){
                            myurl = "{{ route('admin.workflow.delete-sms-action') }}"
                        }
                        if(type == 'change_stage'){
                            myurl = "{{ route('admin.workflow.delete-change-stage-action') }}"
                        }
                        if(type == 'email_attachment'){
                            myurl = "{{ route('admin.workflow.delete-email-attachment') }}"
                        }
                        mydata = {
                            action_id: aid,
                            '_token': '{{ csrf_token() }}',
                        }
                        $.ajax({
                            url: myurl,
                            type: 'POST',
                            data: mydata,
                            success: function (response) {
                                $("#" + type + "_" + aid).remove();
                                if(type == 'change_stage' || type == 'email_attachment'){
                                    location.reload();
                                }
                            }
                        })
                }
            });

        }

        function insertAction(lid, type){
            $("#actionModal1").find('button.btn').prop("disabled", true);
            $("#actionModal1").find('button.btn').css({
                "opacity": ".2",
                "cursor": "progress"
            });
            var myurl = "{{ route('admin.workflow.workflow-stage-action-emails') }}";
            if(type == 'sms'){
                myurl = "{{ route('admin.workflow.workflow-stage-action-sms') }}"
            }
            $("#actionModal1").find('select').each(function(){
                ($(this).val()==null) ? $(this).addClass('border border-danger') : $(this).removeClass('border border-danger');
            });
            var mydata = {
                action_id : main_action_id,
                template_id: document.getElementById(type + '_template_id').value,
                from:  document.getElementById(type + '_from') ? document.getElementById(type + '_from').value : '',
                to: document.getElementById(type + '_to').value,
                custom_email: custom_email.value,
                workflow_id: workfow_title.getAttribute('data-wf-id'),
                stage_id: lid.split('_')[2],
                '_token': '{{ csrf_token() }}',
            }
            $.ajax({
                url: myurl,
                type: 'POST',
                data: mydata,
                success: function (response) {
                    window.location.reload();
                }
            })
        }


        function addQuestionnaire(lid){
            var myurl = "{{ route('admin.workflow.add-workflow-stage-action-questionnaires') }}";
            var mydata = {
                action_id : main_questionnaire_id,
                type : q_type.value,
                template_id: q_email_template_id.value,
                questionnaire: questionnaire_id.value,
                workflow_id: workfow_title.getAttribute('data-wf-id'),
                stage_id: lid.split('_')[2],
                '_token': '{{ csrf_token() }}',
            }
            $.ajax({
                url: myurl,
                type: 'POST',
                data: mydata,
                success: function (response) {
                    window.location.reload();
                }
            })
        }
        function addInterview(lid){

            var title = document.querySelector('input[name=session_title]');
            title.style.borderColor = 'lightgray';
            if(title.value == ''){
                title.style.borderColor = 'red';
                return;
            }
            myurl = "{{ route('admin.workflow.action.interview.save') }}";
            var alerts = {};
            ['email', 'sms'].forEach(function(a){
                ['invite', 'schedule', 'reschedule', 'cancelled', 'reminder'].forEach(function(b){
                    $key = a + '_' + b;
                    if($key != 'email_reminder'){
                        alerts[$key] = document.getElementById($key).value;
                    }
                });
            });
            $('#actionModal3').append($('<input type="hidden" name="_token"/>').val('{{ csrf_token() }}'));
            $('#actionModal3').append($('<input type="hidden" name="id"/>').val(main_interview_id));
            $('#actionModal3').append($('<input type="hidden" name="workflow_id"/>').val(workfow_title.getAttribute('data-wf-id')));
            $('#actionModal3').append($('<input type="hidden" name="stage_id"/>').val(lid.split('_')[2]));
            $('#actionModal3').append($('<input type="hidden" name="timeslot"/>').val(timeslot.value));
            $('#actionModal3').append($('<input type="hidden" name="available_days"/>').val(available_days.value));
            $('#actionModal3').append($('<input type="hidden" name="alerts"/>').val(JSON.stringify(alerts)));
            $('#actionModal3').append($('<input type="hidden" name="if_pass"/>').val(document.getElementsByClassName('if_pass')[0].value));
            $('#actionModal3').append($('<input type="hidden" name="if_fail"/>').val(document.getElementsByClassName('if_fail')[0].value));
            $('#actionModal3').append($('<input type="hidden" name="if_canceled"/>').val(document.getElementsByClassName('if_canceled')[0].value));
            // $('#actionModal3').append($('<input type="hidden" name="employees"/>').val(employees.value));
            $.ajax({
                url: myurl,
                type: 'POST',
                data: $('#actionModal3').find('input,select').serialize(),
                success: function (response) {
                    // console.log(response)
                    window.location.reload();
                }
            })
        }
        function addNewStage(){
            var url = "{{ route('admin.workflow.create-stage') }}";
            
            $('#modelHeading').html('Application Status');
            $.ajaxModal('#scheduleDetailModal', url);
        }
        $(".select2").select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
        });

        //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
        // $('#reservationdatetime').datetimepicker();

        function handleCloseClick(el) {
            el.style.visibility = 'hidden';
            let editIcon = document.getElementById('edit-icon');
            editIcon.classList.remove('fa-check');
            editIcon.classList.add('fa-pencil');
            workfow_title.disabled = true;
            workfow_title.value = workflow_name 
        }

        function handleIconClick(el) {
            let editMode = el.className.includes('fa-pencil') ? true : false;
            let closeIcon = document.getElementById('close-icon');
            if(editMode) {
                closeIcon.style.visibility = 'visible';
                el.classList.remove('fa-pencil');
                el.classList.add('fa-check');
                workfow_title.disabled = false;
                workfow_title.focus();
                var url = "{{ route('admin.workflow.create-stage') }}";
                workflow_name = workfow_title.value;
            }
            else {
                if(!workfow_title.value || workfow_title.value.length === 0 || /^\s*$/.test(workfow_title.value)) return null;
                closeIcon.style.visibility = 'hidden';
                let inputEl =  document.getElementById('workfow_title');
                var endpoint = "{{ route('admin.workflow.update-workflow') }}";
                var new_value = workfow_title.value;
                if($(workfow_title).val()==''){
                    $("#workfow_title_error").html('This field is required.');
                    $("#stages_columns").addClass('d-none');
                }
                else{
                    $("#stages_columns").removeClass('d-none');
                    $("#workfow_title_error").html('');
                }
                var wrf_id = workfow_title.getAttribute('data-wf-id');
                var cidd = workfow_title.getAttribute('data-cid');
                if(workflow_name != new_value){}
                $.ajax({
                    url: endpoint,
                    type: 'POST',
                    data: {
                        old : workflow_name,
                        new : new_value,
                        wf_id : wrf_id,
                        cid : cidd,
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (response) {
                        workfow_title.value = response.name;
                        window.location.replace('{{url("admin/workflow/workflow?id=")}}'+response.id);
                    }
                })
                el.classList.remove('fa-check');
                el.classList.add('fa-pencil');
                workfow_title.disabled = true;
            }
        }
        $("body").on('change','#q_type',function () {
            if($(this).val()){
                ($(this).val()=='internal') ? $("#questionnaire_email_template").hide() : $("#questionnaire_email_template").show();
            }
        }).change();

        function moveMe(id, action){
            $.ajax({
                    url: "{{ route('admin.workflow.change-stage-order') }}",
                    type: 'POST',
                    data: {
                        sid : id,
                        do : action,
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (response) {
                        console.log(response)
                        window.location.reload();
                    }
                })
        }
    </script>
@endpush