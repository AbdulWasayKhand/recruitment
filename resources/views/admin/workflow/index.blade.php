@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
   
@endpush
@section('create-button')
    <a href="{{ url('admin/workflow/trigger') }}" class="mr-2"><i class="fa fa-flash"></i> @lang('modules.workflow.manageTriggers')</a>
    <form class="d-none" action="{{ route('admin.workflow.update-workflow') }}" method="POST" id="create-workflow">{{csrf_field()}}</form>
    <a href="javascript:void(0)" onclick="document.getElementById('create-workflow').submit();" class="btn btn-dark btn-sm m-l-15"><i class="fa fa-plus-circle"></i> @lang('app.createNew')</a>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    
                    <!-- <span class="float-sm-right">
                        <a href="#" class="btn btn-dark btn-sm m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>
                    </span> -->
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>@lang('app.name')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($workflows as $workflow) 
                                <tr role="row" class="odd">
                                    <td>{{$workflow->name}}</td>
                                    <td>
                                        <a href="{{ url('admin/workflow/workflow') }}?id={{$workflow->id}}" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:deleteWorkflow('{{$workflow->id}}');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-row-id="6" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>


    <script>
        // $(".select2").select2();
        //Flat red color scheme for iCheck
        // $('input[type="checkbox"]').iCheck({
        //     checkboxClass: 'icheckbox_flat-blue',
        // });

        //Date and time picker
    // $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
        // $('#reservationdatetime').datetimepicker();
        // var table = $('#myTable').dataTable({
        //     responsive: true,
        // });

        // new $.fn.dataTable.FixedHeader( table );

        function deleteWorkflow(wrf_id) {
            swal({
                title: "Are You Sure?",
                text: "You want to delete this workflow",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#f44336",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('admin.workflow.delete-workflow') }}",
                        type: 'POST',
                        data: {
                            wf_id : wrf_id,
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function (response) {
                            window.location.reload();
                        }
                    })
                }
            });
        }
    </script>
@endpush