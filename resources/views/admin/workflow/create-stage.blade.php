<style>
    .msg {
        display: none;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
<div class="modal-header">
    <h4 class="modal-title">
        <i class="icon-plus"></i> @lang('modules.workflow.addANewStage')
    </h4>
    <button type="button" id="close_modal" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="fa fa-times"></i>
    </button>
</div>
<div class="modal-body">
    <form id="createStatus" class="ajax-form" method="post">
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label class="d-block control-label">@lang('modules.jobApplication.statusName')</label>
                        <input type="text" id="stage_name" name="stage_name" class="form-control">
                        <span for="stage_name" class="msg"><i style="color:red;">Field is empty</i></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="form-group">
                        <label class="d-block control-label">@lang('modules.jobApplication.statusColor')</label>
                        <span class="dot" id="stageColor1" style="background-color: #000000;" onclick="handleSelectedColor('stageColor1', '#000000')">
                            <input type="radio" value="#000000" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <span class="dot selected-dot" id="stageColor2" style="background-color: #ea4335;" onclick="handleSelectedColor('stageColor2', '#ea4335')">
                            <input type="radio" value="#ea4335" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <span class="dot" id="stageColor3" style="background-color: #Fbbc05;" onclick="handleSelectedColor('stageColor3', '#Fbbc05')">
                            <input type="radio" value="#Fbbc05" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <span class="dot" id="stageColor4" style="background-color: #34a853;" onclick="handleSelectedColor('stageColor4', '#34a853')">
                            <input type="radio" value="#34a853" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <span class="dot" id="stageColor5" style="background-color: #2c97df;" onclick="handleSelectedColor('stageColor5', '#2c97df')">
                            <input type="radio" value="#2c97df" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <span class="dot" id="stageColor6" style="background-color: #9c56b8;" onclick="handleSelectedColor('stageColor6', '#9c56b8')">
                            <input type="radio" value="#9c56b8" name="stage_color" class="dot" style="opacity: 0;">
                        </span>
                        <input type="text" id="stage_color" value="#ea4335" style="visibility: hidden" />
                    </div>
                </div>
            </div>
                <!-- <div class="col-xs-12 col-md-6">
                    <label class="d-block control-label">@lang('modules.jobApplication.statusPosition')</label>
                    <select name="status_position" id="stage_position" class="select2 form-control">

                    </select>
                </div> -->
        </div>
    </form>

</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
    <button type="button" class="btn btn-success" onclick="javascript:saveStage();">@lang('app.submit')</button>
</div>
<script src="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script>
    function handleSelectedColor(id, color) {
        document.getElementById('stage_color').value = color
        let element = document.getElementById(id);    
        let allSelectedElements = document.getElementsByClassName("selected-dot");
        for(let i = 0; i < allSelectedElements.length; i++){
            allSelectedElements[i].classList.remove('selected-dot');
        }
        if(element.className.includes('selected-dot')) element.classList.remove('selected-dot');
        else element.classList.add('selected-dot');
    }

    $(function() {
        $('#cp2').colorpicker();
    });
    function saveStage(event){
        document.querySelectorAll('.msg').forEach(function (e) {
            if(document.getElementById(e.getAttribute('for')).value == ""){
                e.style.display = 'block';
                event.preventDefault();
            }else{
                e.style.display = 'none';
            }
        });
        if($("#createStatus").find('#stage_name') == "" || $("#createStatus").find('#stage_color') == ""){
            return null;
        }
        $("#createStatus").closest('.modal-content').find('button.btn').prop("disabled", true);
        $("#createStatus").closest('.modal-content').find('button.btn').css({
            "opacity": ".2",
            "cursor": "progress"
        });
        var endpoint = "{{ route('admin.workflow.save-stage') }}";
        $.ajax({
            url: endpoint,
            type: 'POST',
            data: {
                stage_name: $("#createStatus").find('#stage_name').val(),
                stage_color: $("#createStatus").find('#stage_color').val(),
                // stage_position: stage_position.value,
                workflow_id:workfow_title.getAttribute('data-wf-id'),
                '_token': '{{ csrf_token() }}'
            },
            success: function (response) {
                location.reload();
                var cln = cloneable.cloneNode(true);
                cln.style.display = 'block';
                cln.removeAttribute('id');
                accordion.appendChild(cln);
                var heading = document.querySelectorAll('.stage_heading_')[1];
                var stage_name = document.querySelectorAll('.stage_name_')[1];
                var stage_collapse = document.querySelectorAll('.collapse_')[1];
                var list_id_list = document.querySelectorAll('.list_id_list')[1];
                var add_action_btn = document.querySelectorAll('.add_action_btn_')[1];
                var r_stage = document.querySelectorAll('.r_stage_')[1];
                add_action_btn.addEventListener('mouseover', function(){
                    list_id = 'list_id_' + response.stage_id;
                });
                add_action_btn.classList.remove('add_action_btn_');

                att_1 = document.createAttribute("id")
                att_1.value = 'stage_heading_' + response.stage_id;
                heading.setAttributeNode(att_1);
                heading.style.background = response.color;
                heading.setAttribute('data-target', '#collapse_' + response.stage_id);
                heading.classList.remove('stage_heading_');

                att_2 = document.createAttribute("id")
                att_2.value = 'stage_name_' + response.stage_id;
                stage_name.setAttributeNode(att_2);
                stage_name.innerHTML = response.title;
                stage_name.classList.remove('stage_name_');

                att_3 = document.createAttribute("id")
                att_3.value = 'collapse_' + response.stage_id;
                stage_collapse.setAttributeNode(att_3);
                stage_collapse.classList.remove('collapse_');

                att_4 = document.createAttribute("id");
                att_4.value = 'list_id_' + response.stage_id;
                list_id_list.setAttributeNode(att_4);
                list_id_list.classList.remove('list_id_');

                r_stage.setAttribute('data-stage-id', response.stage_id);
                document.querySelectorAll('.show')[1].classList.remove('show');
                window.location.hash = 'stage_heading_' + response.stage_id;

                
            }
        });
    }
</script>
