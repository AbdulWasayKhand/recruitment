@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <style>
    @media screen and (min-width:767px){.modal-lg {max-width: 80%;}}
    .trigger_name {font-size: 25px; font-weight: bold;cursor: pointer; margin-bottom: 10px;outline: none;border-color: transparent;}
    .trigger_name:focus {border-bottom: 2px solid gray;}
    </style>
@endpush

@section('content')
    <div class="row pb-2 pl-1">
        <div class="col-6 text-muted" style="font-size:95%;">
            @lang('modules.workflow.trigger.triggersHelpYou')
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">@lang('modules.workflow.trigger.newTrigger')</h4>
                    <form class="ajax-form" method="POST" id="createForm">
                        @csrf
                        <input type="hidden" name="id" value="{{$id}}" />
                        <div class="row">
                            <div class="d-flex align-items-center position-relative workfow_title_container" onmouseover="handlePencilVisible('edit-icon', true)" onmouseout="handlePencilVisible('edit-icon', false)">
                                <i class="fa fa-pencil position-absolute pointer" style="right:0; margin-right: 22px; visibility: hidden;" id="edit-icon" aria-hidden="true" onclick="handleIconClick(this)" ></i>
                                <i class="fa fa-close position-absolute pointer" id="close-icon" style="visibility: hidden; right:8px" aria-hidden="true" onclick="handleCloseClick(this)" ></i>
                                <input type="text" name="name" disabled value="{{$workflow_trigger->name}}" class="trigger_name" id="trigger_name" placeholder="@lang('modules.workflow.trigger.enterTriggerNameHere')">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-9">
                                <div class="form-group">
                                    Activate this trigger when 
                                    <select class="form-control col-md-2 d-inline" name="scope">
                                        <option value="any" {{$workflow_trigger->scope=='any' ? 'selected' : '' }}>Any</option>
                                        <option value="all" {{$workflow_trigger->scope=='all' ? 'selected' : '' }}>All</option>
                                    </select> of the following conditions match
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="pull-left">CONDITIONS</label><hr class="w-50 pull-left m-2"><span id="add_condition"><i class="fa fa-plus-circle" style="font-size:20px;"></i></span>
                                </div>
                            </div>
                        </div>
                        <div id="conditions-container">
                            @php($i=0)
                            @foreach($workflow_trigger->conditions as $condition)
                            
                            <div class="row mt-3">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <b>If</b>
                                        <select class="form-control col-md-2 d-inline ml-2" name="conditions[{{$i}}][metric]">
                                            <option value="qualification" {{$condition->metric=='qualification' ? 'selected' : ''}}>Qualification Score</option>
                                        </select>
                                        <select class="form-control col-md-2 d-inline ml-2" name="conditions[{{$i}}][operator]">
                                            @foreach(triggerOperators('qualification') as $operator)
                                            <option value="{{$operator}}" {{$condition->operator==$operator ? 'selected' : '' }}>{{$operator}}</option>
                                            @endforeach
                                        </select>
                                        <input type="number" class="form-control col-md-2 d-inline ml-2" placeholder="@lang('modules.workflow.trigger.conditionValue')" name="conditions[{{$i}}][value]" value="{{$condition->value}}" style="height:39px;" />
                                        <span id="delete_condition" data-id="{{$condition->id}}" class="fa fa-trash ml-2 text-danger" style="font-size:18px;"></span>
                                        <input type="hidden" counter="{{$i}}" name="conditions[{{$i}}][id]" value="{{$condition->id}}" />
                                    </div>
                                </div>
                            </div>
                            @php($i++)
                            @endforeach
                        </div>
                        <hr>
                        <button type="button" id="save-form" class="btn btn-success mt-3"><i class="fa fa-check"></i> @lang('app.save')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        function handlePencilVisible(id, isVisible) {
            let el = document.getElementById(id);
            isVisible ? el.style.visibility = 'visible' : el.style.visibility = 'hidden';
        }
        function handleCloseClick(el) {
            el.style.visibility = 'hidden';
            let editIcon = document.getElementById('edit-icon');
            editIcon.classList.remove('fa-check');
            editIcon.classList.add('fa-pencil');
            trigger_name.disabled = true;
            trigger_name.value = workflow_name 
        }

        function handleIconClick(el) {
            let editMode = el.className.includes('fa-pencil') ? true : false;
            let closeIcon = document.getElementById('close-icon');
            if(editMode) {
                closeIcon.style.visibility = 'visible';
                el.classList.remove('fa-pencil');
                el.classList.add('fa-check');
                trigger_name.disabled = false;
                trigger_name.focus();
                var url = "{{ route('admin.workflow.create-stage') }}";
                workflow_name = trigger_name.value;
            }
            else {
                if(!trigger_name.value || trigger_name.value.length === 0 || /^\s*$/.test(trigger_name.value)) return null;
                closeIcon.style.visibility = 'hidden';
                let inputEl =  document.getElementById('trigger_name');
                var endpoint = "{{ route('admin.workflow.update-workflow') }}";
                var new_value = trigger_name.value;
                if($(trigger_name).val()==''){
                    $("#workfow_title_error").html('This field is required.');
                    $("#stages_columns").addClass('d-none');
                }
                else{
                    $("#stages_columns").removeClass('d-none');
                    $("#workfow_title_error").html('');
                }
                var wrf_id = trigger_name.getAttribute('data-wf-id');
                var cidd = trigger_name.getAttribute('data-cid');

                el.classList.remove('fa-check');
                el.classList.add('fa-pencil');
                trigger_name.disabled = true;
            }
        }
    $(function(){
        $('#save-form').click(function () {
            trigger_name.disabled = false;
            var data       = $('#createForm').serializeArray();
            $.easyAjax({
                url: "{{url('admin/workflow/trigger/store')}}",
                container: '#createForm',
                type: "POST",
                data: data,
                error: function (response) {
                    handleFails(response);
                },
                success: function(){
                    trigger_name.disabled = true;
                }
            });

        });
        // Add Conditions
        var $triggerOperators = JSON.parse('{!! json_encode(triggerOperators("qualification")) !!}');
        var counter = $("#conditions-container").find('input[type=hidden]').length;
        $("body").on('click','#add_condition',function(){
            counter++;
            $html = `
                
                <div class="row mt-3">
                    <div class="col-md-9">
                        <div class="form-group">
                            <b>If</b>
                            <select class="form-control col-md-2 d-inline ml-2" name="conditions[${counter}][metric]">
                                <option value="qualification">Qualification Score</option>
                            </select>
                            <select class="form-control col-md-2 d-inline ml-2" name="conditions[${counter}][operator]">`
                                $.each($triggerOperators,function(k,operator){
                                    $html += `<option value="${operator}">${operator}</option>`;
                                });
                            $html += `
                            </select>
                            <input type="number" class="form-control col-md-2 d-inline ml-2" placeholder="@lang('modules.workflow.trigger.conditionValue')" name="conditions[${counter}][value]" style="height:39px;" />
                            <span id="delete_condition" class="fa fa-trash ml-2 text-danger" style="font-size:18px;"></span>
                            <input type="hidden" counter="${counter}" name="conditions[${counter}][id]" />
                        </div>
                    </div>
                </div>
            `;
            $("#conditions-container").append($html);
        });
        // Delete Conditions
        $("body").on('click','#delete_condition',function(){
            var $this = $(this);
            if($(this).data('id')){
                $.ajax({
                    url : '{{url("admin/workflow/trigger/condition/delete")}}',
                    type : 'POST',
                    data : {'_token' : '{{ csrf_token() }}','id':$(this).data('id')},
                    success : function(){
                        $this.closest('div').parent().parent().remove();
                        location.reload();
                    }
                });
            }else{
                this.parentNode.parentNode.parentNode.remove();
            }
        });
    });
        function handleFails(response) {

            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                console.log(keys);
                $('#createForm').find(".has-error").find(".help-block").remove();
                $('#createForm').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#createForm').find("[name='" + key + "']");

                    var grp = ele.closest(".form-group");
                    // grp = grp.length ? grp : ;
                    console.log(grp);
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".wysihtml5-toolbar").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="help-block">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({scrollTop: element.offset().top - 150}, 200);
                    }
                }
            }
        }
    </script>
@endpush