@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
   
@endpush
@section('create-button')
    <a href="{{ url('admin/workflow/trigger/create') }}" class="btn btn-dark btn-sm m-l-15"><i class="fa fa-plus-circle"></i> @lang('app.createNew')</a>
@endsection

@section('content')
    <div class="row pb-2 pl-1">
        <div class="col-6 text-muted" style="font-size:95%;">
            @lang('modules.workflow.trigger.triggersHelpYou')
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>@lang('app.name')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($workflow_triggers as $trigger)
                                <tr role="row" class="odd">
                                    <td>{{$trigger->name}}</td>
                                    <td>
                                        <a href="{{ url('admin/workflow/trigger/edit/'.$trigger->id) }}" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:deleteWorkflow('{{$trigger->id}}');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-row-id="6" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>


    <script>
        // $(".select2").select2();
        //Flat red color scheme for iCheck
        // $('input[type="checkbox"]').iCheck({
        //     checkboxClass: 'icheckbox_flat-blue',
        // });

        //Date and time picker
    // $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
        // $('#reservationdatetime').datetimepicker();
        // var table = $('#myTable').dataTable({
        //     responsive: true,
        // });

        // new $.fn.dataTable.FixedHeader( table );

        function deleteWorkflow(wrf_trigger_id) {
            swal({
                title: "Are You Sure?",
                text: "You want to delete this workflow trigger",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#f44336",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.easyAjax({
                        url: "{{ url('admin/workflow/trigger') }}/"+wrf_trigger_id+"/delete",
                        type: 'POST',
                        data: {
                            wf_id : wrf_trigger_id,
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function (response) {
                            window.location.reload();
                        }
                    })
                }
            });
        }
    </script>
@endpush