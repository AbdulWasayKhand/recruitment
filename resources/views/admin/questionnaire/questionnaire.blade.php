<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/froiden-helper/helper.js') }}" type="text/javascript"></script>

<style>

.modal-body * {
    /* text-align: center; */
}
.modal-body form {
    width: 50%;
    margin: auto;
}
/* .icheckbox_flat-blue {
    display: initial !important;
} */
</style>

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <h4 class="card-title form-sub-heading mb-4" style="text-align: center;">
            {{ucFirst($questionnaire->name)}} @lang('app.for') {{ucFirst($jobApplication->applicant_name)}}
            </h4>
            <form class="ajax-form" method="POST" id="createForm">
                @csrf
                <div class="row" style="overflow-y: auto;height: 70vh;">
                    <div class="col-md-12 mt-3">
                        @foreach($custom_fields as $field)
                            @if($field->question_type == 'checkbox' || $field->question_type == 'radio' || $field->question_type == 'dropdown')
                                @php
                                    $question_value = json_decode($field->question_value);
                                @endphp
                                @if($field->question_value=='null')
                                    @continue
                                @endif
                            @endif
                            @php($answered = isset($answers[$field->question->id]) ? $answers[$field->question->id] : '')
                            @switch($field->question_type)
                                @case('dropdown')
                                    <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label>
                                        <select class="form-control form-control-md" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" placeholder="@lang('modules.front.yourAnswer')">
                                            <option value="">@lang('app.choose')</option>
                                        @if(!empty($question_value))    
                                        @foreach($question_value as $value)
                                            @if($value)
                                            <option value="{{ $value }}" {{($answered==$value) ? 'selected' : ''}}>{{ $value }}</option>
                                            @endif
                                        @endforeach
                                        @endif
                                        </select>
                                    </div>
                                    @break

                                @case('checkbox')
                                    @php($answered = $answered ? explode(',',$answered) : '')
                                    <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        @if(!empty($question_value))
                                        @foreach($question_value as $key=>$value)
                                            @if($value)
                                            <label class="" style="display: inline-flex;">
                                                    <input class="flat-red" style="opacity: initial;display:block;margin:4px;" type="checkbox" value="{{$value}}" name="answer[{{$field->question->id}}][{{$key}}]" id="answer[{{ $field->question->id}}][{{$key}}]" {{(is_array($answered) && (in_array($value,$answered) || in_array(' '.$value,$answered))) ? 'checked' : ''}} />
                                                {{ ucfirst($value)}} @if($field->question->required == 'yes')(@lang('app.required'))@endif
                                            </label><br>
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                    @break
                                @case('radio')
                                     <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        @if(!empty($question_value))
                                        @foreach($question_value as $value)
                                            @if($value)
                                                <label class="checkbox-inline"><input id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" type="radio" value="{{$value}}" {{($answered==$value) ? 'checked' : ''}}>&nbsp;&nbsp;{{ucFirst($value)}}</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                    @break
                                @case('textarea')
                                     <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        <textarea class="form-control form-control-md" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" value="{{$answered}}" placeholder="@lang('modules.front.yourAnswer')" rows="3"></textarea>
                                    </div>
                                    @break
                                @default
                                @if($field->question->question)
                                <div class="form-group">
                                    <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label>
                                    <input class="form-control form-control-md" type="text" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" value="{{$answered}}" placeholder="@lang('modules.front.yourAnswer')">
                                </div>
                                @endif
                            @endswitch
                            <hr />
                        @endforeach
                        <input type="hidden" name="job_application_id" value="{{$jobApplication->id}}">
                        <input type="hidden" name="job_id" value="{{$jobApplication->job_id}}">
                        <input type="hidden" name="questionnaire_id" value="{{$questionnaire_id}}">
                    </div>
                </div>
                <button type="button" id="save-form" class="btn btn-success mt-3" style=""><i class="fa fa-check"></i> Submit</button>
            </form>
        </div>
    </div>
</div>

    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

    <script>

        //Flat red color scheme for iCheck
        // $('input[type="checkbox"].flat-red').iCheck({
        //     checkboxClass: 'icheckbox_flat-blue',
        // })
        $('document').ready(function(){
            $('body').on('click','#save-form',function () {
                var data       = $('#createForm').serializeArray();
                $.easyAjax({
                    url: "{{url('/saveApplicantAnswer')}}?user=applicant",
                    container: '#createForm',
                    type: "POST",
                    redirect: true,
                    data: data,
                    success: function (response) {
                        location.href = '{{$thanks_page}}';
                    }
                });
            });
        });
    </script>
