<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <h4 class="card-title form-sub-heading mb-4">
            {{ucFirst($questionnaire->name)}} @lang('app.for') {{ucFirst($jobApplication->applicant_name)}}
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span aria-hidden="true">×</span></button>
            </h4>
            <form class="ajax-form" method="POST" id="createForm">
                @csrf
                <?php //if($answers == []){ ?>
                    <!-- <div class="row" style="overflow-y: auto;height: 70vh;">
                    <div class="col-md-12 mt-3">
                        <i>Not Answered yet.</i>
                    </div>
                    </div> -->
                <?php //exit; } ?>
                <div class="row" style="overflow-y: auto;height: 70vh;">
                    <div class="col-md-12 mt-3">
                        @foreach($custom_fields as $field)
                            @if($field->question_type == 'checkbox' || $field->question_type == 'radio' || $field->question_type == 'dropdown')
                                @php
                                    $question_value = json_decode($field->question_value);
                                @endphp
                                @if($field->question_value=='null')
                                    @continue
                                @endif
                            @endif
                            @php($answered = isset($answers[$field->question->id]) ? $answers[$field->question->id] : '')
                            @switch($field->question_type)
                                @case('dropdown')
                                    <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label>
                                        <select class="form-control form-control-md" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" placeholder="@lang('modules.front.yourAnswer')" @if($editable) disabled @endif >
                                            <option value="">@lang('app.choose')</option>
                                        @if(!empty($question_value))    
                                        @foreach($question_value as $value)
                                            @if($value)
                                            <option value="{{ $value }}" {{($answered==$value) ? 'selected' : ''}}>{{ $value }}</option>
                                            @endif
                                        @endforeach
                                        @endif
                                        </select>
                                    </div>
                                    @break

                                @case('checkbox')
                                    @php($answered = $answered ? explode(',',$answered) : '')
                                    <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        @if(!empty($question_value))
                                        @foreach($question_value as $key=>$value)
                                            @if($value)
                                            <label class="">
                                                    <input class="flat-red" type="checkbox" value="{{$value}}" name="answer[{{$field->question->id}}][{{$key}}]" id="answer[{{ $field->question->id}}][{{$key}}]" {{(is_array($answered) && (in_array($value,$answered) || in_array(' '.$value,$answered))) ? 'checked' : ''}}  @if($editable) disabled @endif />
                                                {{ ucfirst($value)}} @if($field->question->required == 'yes')(@lang('app.required'))@endif
                                            </label><br>
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                    @break
                                @case('radio')
                                     <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        @if(!empty($question_value))
                                        @foreach($question_value as $value)
                                            @if($value)
                                                <label class="checkbox-inline"><input id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" type="radio" value="{{$value}}" {{($answered==$value) ? 'checked' : ''}} @if($editable) disabled @endif >&nbsp;&nbsp;{{ucFirst($value)}}</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                    @break
                                @case('textarea')
                                     <div class="form-group">
                                        <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label></br>
                                        <textarea class="form-control form-control-md" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" value="{{$answered}}" placeholder="@lang('modules.front.yourAnswer')" rows="3"  @if($editable) disabled @endif ></textarea>
                                    </div>
                                    @break
                                @default
                                @if($field->question->question)
                                <div class="form-group">
                                    <label class="control-label" for="answer[{{ $field->question->id}}]">{{ $field->question->question }}</label>
                                    <input class="form-control form-control-md" type="text" id="answer[{{ $field->question->id}}]" name="answer[{{ $field->question->id}}]" value="{{$answered}}" placeholder="@lang('modules.front.yourAnswer')"   @if($editable) disabled @endif >
                                </div>
                                @endif
                            @endswitch
                        @endforeach
                        <input type="hidden" name="job_application_id" value="{{$jobApplication->id}}">
                        <input type="hidden" name="job_id" value="{{$jobApplication->job_id}}">
                        <input type="hidden" name="questionnaire_id" value="{{$questionnaire_id}}">
                    </div>
                </div>
                <button type="button" id="save-form" class="btn btn-success mt-3" style="@if($editable) display:none; @endif "><i
                            class="fa fa-check"></i> @lang('app.save')</button>
                <button type="button" class="btn default mt-3 pull-right" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>

    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
        })
        $('document').ready(function(){
            $('body').on('click','#save-form',function () {
                $('#createForm').find('button.btn').prop("disabled", true);
                $('#createForm').find('button.btn').css({
                    "opacity": ".2",
                    "cursor": "progress"
                });
                var data       = $('#createForm').serializeArray();
                $.easyAjax({
                    url: '{{route('admin.questionnaire.save_applicant_answers')}}',
                    container: '#createForm',
                    type: "POST",
                    redirect: true,
                    data: data,
                    error: function (response) {
                        handleFails(response);
                        $('#createForm').find('button.btn').prop("disabled", false);
                        $('#createForm').find('button.btn').css({
                            "opacity": "unset",
                            "cursor": "unset"
                        });
                    },
                    success: function (response) {
                        var qHtml = '';
                            $.each(response.printData,function (q,value) {
                                qHtml = '<div class="col-sm-12">'+
                                            '<strong>'+value["question"]+'</strong><br>'+
                                            '<p class="text-muted" id="a-'+q+'">'+value["answer"]+'</p>'+
                                        '</div>'+qHtml;
                            });
                            $("#questionnaire_print").find('#questionnaire_answers').html(qHtml);
                            $("#prev_questionnaire_button_"+$("#createForm").find('input[name="questionnaire_id"]').val()).text('Edit');
                            $("#questionnaireModal").find('.close:eq(0)').click();
                    }
                });
            });
        });
        function handleFails(response) {

            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#createForm').find(".has-error").find(".help-block").remove();
                $('#createForm').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#createForm').find("[name='" + key + "']");

                    var grp = ele.closest(".form-group");
                    // grp = grp.length ? grp : ;
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".wysihtml5-toolbar").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="help-block">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({scrollTop: element.offset().top - 150}, 200);
                    }
                }
            }
        }
    </script>
