@extends('layouts.app')

@push('head-script')
<style>
    .list-group-item{
        background-color: #f6f7fb;
        margin: 5px;
        border-radius: 0 !important;
        border: 0;
        padding: 5px;
        padding-left: 30px;
    }

    .list-group-item p {
        font-size: 12px;
        padding-left: 20px;
    }

    .list-group-item .btn{
        font-size: 13px;
        display: inline-block;
        padding: 0 5px;
        color: var(--main-color);
    }
    .box-dashboard {
        background: #fff;
        padding: 30px 15px 8px;
        border: 1px solid #d2d6de;
        margin-bottom: 30px;
        box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.15);
    }
    .day-main{height: 393px;overflow-y: auto;overflow-x: hidden;}
    .day-wrapper{margin-bottom: 22px;padding-bottom: 22px}
    div.day-wrapper:last-child{margin-bottom: 0;}
    .day-wrapper:not(:last-child){border-bottom: 1px solid #e2e2e2;}
    .headertitle{margin-bottom: 20px;color: #fff;display: flex;justify-content: space-between;align-items: center;flex-wrap: wrap;border-radius: 5px 5px 0 0;}
    .headertitle h5{color: #fff;padding: 13px 20px 11px;line-height: 1;font-size: 16px;margin: 0;font-weight:600}
    .applicant-details p{font-size:13px;font-weight:600;color:#868686}
    .applicant-details .name{font-weight:700;color:#3f51b5;font-size: 14px;}
    .time-slot p {font-size:13px}
    .circle {width: 48px;margin-top: 5px;margin-right: 7px;}
    .circle span {background: #cecece;width: 8px;height: 8px;float: left;border-radius: 10px;}
    span.qualified-circle{background-color:#FF9800;}
    .day-wrapper > .row {margin-bottom: 8px;}
    
    .text-main{width:100%;height: 393px;overflow-y: auto;overflow-x: hidden;}
    .text-main .col-9 > .row > div{border-left: 3px solid #bfbebe;}
    .text-main .applicant-details{transition:all 0.25s linear}
    .text-main .applicant-details > div{width:100%}
    .text-main div.applicant-details:hover{background-color:#e9e9f5 !important}
    .text-main div.applicant-details:hover div.action{display:block !important}

    .chart-main{width: 50%;height: 300px;overflow-y: auto;overflow-x: hidden;}
    .chart-main-full{width: 100%;height: 300px;overflow-y: auto;overflow-x: hidden;}
    .todo-box2 {background-color: #fff;border-radius: 5px;border: 1px solid #d2d6de;box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.15);}

    .open-jobs {overflow-x: auto;}
    .open-jobs .job {width: 24%;flex-shrink: 0;min-height:370px;background: #e9e9f5;color:#040074;transition:all 0.25s linear}
    .open-jobs .job > .flex-column{height:100%}
    .open-jobs .job > .flex-column h6{font-weight:500;}
    .open-jobs .job > .flex-column p{font-weight:500;}
    div.job:hover{background-color: #4e6dfc;color:#fff;}
    div.job:first-child{margin-left:0 !important;}

    .dashboard-simple-box .col {height: 165px;background: #fff7ea;border-radius: 10px;color:#505050;margin-left: 15px;transition:all 0.25s linear}
    .dashboard-simple-box .col .flex-column{height:100%}
    .dashboard-simple-box .col .flex-column h1{font-size:55px;font-weight:800;}
    .dashboard-simple-box .col .flex-column p{font-weight:600}
    .dashboard-simple-box .col .flex-column i{font-size: 20px;}
    .dashboard-simple-box div.col:first-child{margin-left:0}
    .dashboard-simple-box div.col:last-child{margin-right:0}
    .dashboard-simple-box div.col:nth-child(odd){background:#f8f9fe}
    .dashboard-simple-box .col:hover{background-color: #4e6dfc !important;color: #fff;}
    .main-date .btn-success{background-color: #4f5c9d; border-color: #4f5c9d;}
    /* .main-date .fa-calendar:before {content: "\f073" !important;} */
    .main-date .caret {display: inline-block; width: 0; height: 0; margin-left: 2px; vertical-align: middle; border-top: 4px dashed; border-top: 4px solid\9; border-right: 4px solid transparent; border-left: 4px solid transparent;}

    @media screen and (max-width:767px){
        .dashboard-simple-box{margin-left: -15px !important;}
        .dashboard-simple-box div.col:first-child{margin-left:15px}
        .dashboard-simple-box .col{width: 30%;flex: 30%;margin-top: 15px;}
        .main-date{padding:0}
        #reportrange i{font-size:11px;}
        #reportrange span{font-size:10px;}
        .open-jobs{overflow-x: hidden;}
        .open-jobs .job {width: 100%;}
    }
    .indicator {
        /* font-size: 24px; */
        color: green;
    }
    .indicator > a, .indicator > b {
        font-style: none;
        font-weight: 900;
    }
</style>
    
@endpush

@section('content')
    <?php $pendingInterviewCount = totalUpComingScheduleRequests($user->id, $user->company_id); ?>
    @if($pendingInterviewCount)
        <div class="indicator alert alert-info">You have got <b>{{$pendingInterviewCount}}</b> Interview Request{{$pendingInterviewCount > 1 ? 's' : ''}}. please click <a href="{{url('/admin/interview-schedule')}}">here</a> to accept them</div>
        <div style="padding:5px;"></div>
        @endif
    <div class="row">
        <div class="col-md-12 main-date">
            <div id="reportrange"  class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                <i class="fas fa-calendar-alt"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
            <br /><br />
            <input type="hidden" id="daterange_picker_start" value="">
            <input type="hidden" id="daterange_picker_end" value="">
        </div>     
    </div>

    <div class="row">
        <style>.anc{color:#505050;cursor: pointer;}.anc:hover, .open-jobs > .anc:hover{color:ivory;background: #1579d0;} .open-jobs > .anc {color: #4e5d9e;}</style>
        <!--Start New-->
        <div class="col">
            <div class="row">
                <div class="col-md-12 p-0">
                    <div class="row m-0 mb-3 dashboard-simple-box">
                        <div class="col">
                            <a href="{{url('admin/jobs?active=1')}}" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <!-- <h1 class="mb-0" id="totalOpenings">0</h1> -->
                                    <h1 class="mb-0">{{$totalOpenings}}</h1>
                                    <p class="mb-1">Active Jobs</p>
                                    <p class="mb-0"><i class="fa fa-briefcase"></i></p>
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="job-applications/table-view" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <h1 class="mb-0" id="totalApplications">0</h1>
                                    <p class="mb-1">Applicants</p>
                                    <p class="mb-0"><i class="fa fa-users"></i></p>
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="job-applications/table-view?qualified=1" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <h1 class="mb-0" id="totalQualified">0</h1>
                                    <p class="mb-1">Qualified <small id="total-qualified-percentage"></small></p>
                                    <p class="mb-0"><i class="fas fa-clipboard-check"></i></p>
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="interview-schedule" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <h1 class="mb-0" id="totalInterviewed">0</h1>
                                    <p class="mb-1">Interviews <small id="total-interview-percentage"></small></p>
                                    <p class="mb-0"><i class="fas fa-calendar-check"></i></p>
                                </div>
                            </a>
                        </div>

                        <div class="col">
                            <a href="{{url('admin/job-applications/table-view?status='.$appStatuses['hired'])}}" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <h1 class="mb-0" id="totalHired">0</h1>
                                    <p class="mb-1">Hired <small id="total-hired-percentage"></small></p>
                                    <p class="mb-0"><i class="fas fa-id-card-alt"></i></p>
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="job-applications/table-view?rejected=1" class="anc">
                                <div class="d-flex flex-column text-center justify-content-center">
                                    <h1 class="mb-0" id="totalOffered">0</h1>
                                    <p class="mb-1">Rejected <small id="total-rejected-percentage"></small></p>
                                    <p class="mb-0"><i class="fas fa-envelope-open-text"></i></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 p-0">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div id="lower-box" class="todo-box2">
                                <div class="headertitle" style="background-color: #dc3545;">
                                    <h5>Applicant Sources</h5>
                                </div>
                                <nav class="nav custom-nav-pill nav-pills justify-content-center">
                                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#ap-sources-bar-chart" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-chart-bar"></i></a>
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#ap-sources-donut-chart" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-chart-pie"></i></a>
                                </nav>
                                <div class="tab-content mt-4" id="myTabContent">
                                    <div class="tab-pane fade" id="ap-sources-donut-chart" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                            <div class="chart-main bar-ch-donut">
                                                <canvas id="donutChart" style="height:150px; min-height:150px"></canvas>
                                            </div>                                 
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="ap-sources-bar-chart" role="tabpanel" aria-labelledby="profile-tab">
                                        <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                            <div class="col-12 bar-source-chart">
                                                <canvas id="applicationSourceBarChart" style="height:300px;min-height:300px"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div id="lower-box" style="height: 439px;" class="todo-box2">
                                <div class="headertitle" style="background-color: #28a745;">
                                    <h5>Applicants by day</h5>
                                </div>
                                <div class="d-flex flex-row m-0 pb-3 px-2 open-jobs">
                                    <div class="chart-main-full bar-ch" style="margin-top:62px;">
                                        <canvas id="stackedBarChart_1" style="height:230px; min-height:230px"></canvas>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-4 mb-3">
                            <div id="lower-box" class="todo-box2" style="height: 439px;">
                                <div class="headertitle" style="background-color: #007bff;">
                                    <h5>Qualified vs Unqualified</h5>
                                </div>
                                <nav class="nav custom-nav-pill nav-pills justify-content-center">
                                    <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#ap-sources-donut-chart" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-chart-pie"></i></a>
                                    <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#ap-sources-bar-chart" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-chart-bar"></i></a>
                                </nav>
                                <div class="tab-content mt-4" id="myTabContent">
                                    <div class="tab-pane fade" id="ap-sources-donut-chart" role="tabpanel" aria-labelledby="home-tab2">
                                    <div class="d-flex flex-row m-0 pb-3 justify-content-center">A
                                        <div class="chart-main bar-ch-donut-two" style="margin-top:12px;">
                                            <canvas id="donutChart2" style="height:150px; min-height:150px"></canvas>
                                        </div>                                 
                                    </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="ap-sources-bar-chart" role="tabpanel" aria-labelledby="profile-tab2">
                                        <div class="d-flex flex-row m-0 pb-3 justify-content-center">B
                                            <div class="col-12 bar-source-chart">
                                                <canvas id="applicationSourceBarChart2" style="height:300px;min-height:300px"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                               
                            </div>
                        </div> -->
                        <div class="col-md-4 mb-3">
                            <div id="lower-box" class="todo-box2">
                                <div class="headertitle" style="background-color:  #007bff;">
                                    <h5>Qualified vs Unqualified</h5>
                                </div>
                                <nav class="nav custom-nav-pill nav-pills justify-content-center">
                                    <a class="nav-link active" id="profile-tab-2" data-toggle="tab" href="#ap-sources-bar-chart-2" role="tab" aria-controls="profile" aria-selected="false">By Day</i></a>
                                    <a class="nav-link" id="home-tab-2" data-toggle="tab" href="#ap-sources-donut-chart-2" role="tab" aria-controls="home" aria-selected="true">By Total</i></a>
                                </nav>
                                <div class="tab-content mt-4" id="myTabContent">
                                    <div class="tab-pane fade" id="ap-sources-donut-chart-2" role="tabpanel" aria-labelledby="home-tab-2">
                                        <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                            <div class="chart-main bar-ch-donut-two" >
                                                <canvas id="donutChart2" style="height:150px; min-height:150px"></canvas>
                                            </div>                                 
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="ap-sources-bar-chart-2" role="tabpanel" aria-labelledby="profile-tab-2">
                                        <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                            <div class="chart-main-full bar-ch-2" >
                                                <canvas id="stackedBarChart2" style="height:230px; min-height:230px"></canvas>
                                            </div> 
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 box-dashboard">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="headertitle" style="background-color: #FF9800;">
                                <h5>My Upcoming Interviews</h5>
                            </div>
                            <div class="day-main">
                                @foreach($upComingSchedules as $day => $interviews) 
                                    <div class="day-wrapper">
                                        <div class="row">
                                            <div class="col-3 pt-3">
                                                <h5><strong>@if(date($day) == date("Y-m-d")) Today @else @if(date($day) == (date('Y-m-d', strtotime('+1 day', strtotime(date("Y-m-d")))))) Tomorrow @else {{ date_format(date_create($day), "l") }} @endif @endif</strong></h5>
                                                <p class="mb-0" style="font-size:14px"><?= date_format(date_create($day), "D, d M") ?></p>
                                            </div>
                                            <div class="col-9">
                                                @foreach($interviews as $interview)
                                                    <div class="row m-0">
                                                        <div class="col-lg-2 p-0 pt-3 pb-3 time-slot">
                                                            <p class="mb-0"><?= explode(" - ",$interview["slot_timing"])[0] ?></p>
                                                            <p class="mb-0"><?= explode(" - ",$interview["slot_timing"])[1] ?></p>
                                                        </div>
                                                        <div class="col-lg-10 p-0" style="margin-bottom: 5px;">
                                                            <div class="row p-3 m-0 justify-content-between applicant-details" style="background: #f6f7fb;">
                                                                <div>
                                                                    <p class="mb-0 name">{{ $interview["full_name"] }}</p>
                                                                    <p class="mb-0 d-flex">
                                                                        <span class="circle d-flex d-flex-row justify-content-between">
                                                                            <?php for($i = 1 ; $i <= 5 ; $i++){ ?>
                                                                                <span class="<?= $i <= intval($interview["qualified"] / 20 ) ? 'qualified-circle' : '' ?>"></span>
                                                                            <?php } ?>
                                                                        </span>    
                                                                        <span>@if($interview["qualified"] == NULL) 0 @else {{ $interview["qualified"] }} @endif% Qualified</span>
                                                                    </p>
                                                                </div>
                                                                <div>
                                                                    <p class="text-right mb-0">{{ $interview["title"] }}</p>
                                                                <p class="text-right mb-0">Applied <?php $days = (int)date_diff(date_create(date("Y-m-d", strtotime($interview["applied"]))), date_create(date("Y-m-d")))->format("%R%a"); echo $days; echo $days != 1 ? " days" : " day"; ?> ago</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div id="lower-box" class="todo-box">
                                <div class="headertitle" style="background-color: #81007f;">
                                    <h5>Recent Texts</h5>
                                </div>
                                <div class="d-flex flex-row m-0 pb-3 open-jobs">
                                    <div class="text-main">
                                        @foreach ($messages as $message)
                                            <div class="day-wrapper">
                                                @php
                                                $date_sent = Arr::get($message, 'date_created');
                                                $date_sent->timezone($global->timezone);
                                                @endphp
                                                <div class="row">
                                                    <div class="col-3 pt-3">
                                                        <h5 style="font-size: 11px;"><strong>{{ ($date_sent)->diffForHumans() }}</strong></h5>
                                                        <p class="mb-0" style="font-size:14px">{{ $date_sent->format('D, d M') }}</p>
                                                    </div>
                                                    <div class="col-9">
                                                        <div class="row m-0">
                                                            <div class="col-lg-12 p-0" style="margin-bottom: 5px;">
                                                                <div class="row p-3 m-0 justify-content-between applicant-details" style="background: #f6f7fb;">
                                                                    <div>
                                                                        <p class="mb-0 name d-inline-block">{{ Arr::get($message, 'full_name') }}</p>
                                                                        <div class="d-inline-block float-right">
                                                                            <p class="text-right mb-0">{{ $date_sent->format('h:i A') }}</p>
                                                                        </div>
                                                                        <p class="mb-0 d-flex">
                                                                        <span style="word-break: break-all;">{{ Arr::get($message, 'body') }}</span>
                                                                        </p>
                                                                        <div class="action" style="display:none">
                                                                            {{-- <a href="#" class="mr-1"><small><i class="fas fa-reply mr-1"></i>Reply</small></a> --}}
                                                                            {{-- <a href="#" class="text-danger"><small><i class="fas fa-trash-alt mr-1"></i>Delete</small></a> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 box-dashboard">
                    <div class="row">
                        <div class="col-12">
                            <div id="lower-box" class="todo-box">
                                <div class="headertitle" style="background-color: #20c997;">
                                    <h5>Open Jobs</h5>
                                </div>
                                <div class="d-flex flex-row m-0 pb-3 open-jobs">
                                    @foreach($jobs as $job)
                                        <a href="job-applications/table-view/?f_jobs={{$job['id']}}" class="anc job px-4 py-5 ml-1">
                                            <div class="d-flex flex-column justify-content-between">
                                                <div class="job-detail-top">
                                                    <h6><?php echo date("M d", strtotime($job["start_date"])) . " - "  . date("M d", strtotime($job["end_date"])) ?></h6>
                                                    <h3 class="text-bold">{{$job["title"]}}</h3>
                                                    {{--<p class="mb-1">Source: {{$job["company_name"]}}</p>--}}
                                                    <p class="mb-1">{{$job["point"]}}</p>
                                                </div>
                                                <div class="job-detail-bottom">
                                                    <p class="mb-1">{{ $job->getApplicants($job["start_date"],$job["end_date"],'unqualified', $job->id) }} Unqualified</p>
                                                    <p class="mb-1">{{ $job->getApplicants($job["start_date"],$job["end_date"],'qualified', $job->id) }} Qualified</p>
                                                    <p class="mb-1">{{ $job->inteviewed }} Interviewed</p>
                                                    {{-- <p class="mb-1">{{ $job->inteviewed }} Offered</p> --}}
                                                    <p class="mb-1">{{ $job->hired }} Hired</p>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php /* ?>

        <!--End New-->
        <!--<div class="col-md-12">
            @if(isset($lastVersion))

                <div class="alert alert-info col-md-12">
                    <div class="col-md-10"><i class="ti-gift"></i> @lang('modules.update.newUpdate') <label class="label label-success">{{ $lastVersion }}</label>
                    </div>
                </div>
            @endif
            @if (!$user->mobile_verified && $smsSettings->nexmo_status == 'active')
                <div id="verify-mobile-info">
                    <div class="alert alert-info col-md-12" role="alert">
                        <div class="row">
                            <div class="col-md-10 d-flex align-items-center">
                                <i class="fa fa-info fa-3x mr-2"></i>
                                @lang('messages.info.verifyAlert')
                            </div>
                            <div class="col-md-2 d-flex align-items-center justify-content-end">
                                <a href="{{ route('admin.profile.index') }}" class="btn btn-warning">
                                    @lang('menu.profile')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>-->

        <!-- Column -->
        {{-- <div class="col-md-6 col-lg-4 col-xlg-2">
            <div class="card">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white">{{ $totalCompanies }}</h1>
                    <h6 class="text-white">@lang('modules.dashboard.totalCompanies')</h6>
                </div>
            </div>
        </div> --}}
        <!--<div class="row">
            <div class="col-12 box-dashboard">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('admin.jobs.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-info text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $totalOpenings }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.totalOpenings')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.job-applications.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-primary text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $totalApplications }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.totalApplications')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.job-applications.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-success text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $totalHired }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.totalHired')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.job-applications.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-dark text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $totalRejected }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.totalRejected')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.job-applications.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-danger text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $newApplications }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.newApplications')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.job-applications.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-warning text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $shortlisted }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.shortlistedCandidates')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.interview-schedule.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-primary text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $interviews_count }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.totalInterview')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('admin.interview-schedule.index') }}" target="_blank">
                            <div class="card">
                                <div class="box bg-primary text-center rounded py-4">
                                    <h1 class="font-light text-white">{{ $totalTodayInterview }}</h1>
                                    <h6 class="text-white">@lang('modules.dashboard.todayInterview')</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            {{-- <div class="col">
                <div class="row">
                    <div class="col-md-12 box-dashboard" id="todo-items-list">
    
                    </div>
                </div>
            </div> --}}
            <div class="w-100"></div>
            <div class="col-12 box-dashboard">
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-header d-flex p-0 ui-sortable-handle">
                                <h3 class="card-title p-3">
                                    <i class="fa fa-file"></i> @lang('modules.interviewSchedule.interviewSchedule')
                                </h3>
                            </div>
                            <div id="upcoming-schedules" class="card-body">
                                @include('admin.interview-schedule.upcoming-schedule', ['upComingSchedules' => $upComingSchedules])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <?php */ ?>
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha512-mf78KukU/a8rjr7aBRvCa2Vwg/q0tUjJhLtcK53PHEbFwCEqQ5durlzvVTgQgKpv+fyNMT6ZQT1Aq6tpNqf1mg==" crossorigin="anonymous"></script>
    <script>
        var updated = true;

        function showNewTodoForm() {
            let url = "{{ route('admin.todo-items.create') }}"

            $.ajaxModal('#application-md-modal', url);
        }

        function initSortable() {
            let updates = {'pending-tasks': false, 'completed-tasks': false};
            let completedFirstPosition = $('#completed-tasks').find('li.draggable').first().data('position');
            let pendingFirstPosition = $('#pending-tasks').find('li.draggable').first().data('position');

            $('#pending-tasks').sortable({
                connectWith: '#completed-tasks',
                cursor: 'move',
                handle: '.handle',
                stop: function (event, ui) {
                    const id = ui.item.data('id');
                    const oldPosition = ui.item.data('position');

                    if (updates['pending-tasks']===true && updates['completed-tasks']===true)
                    {
                        const inverseIndex =  completedFirstPosition > 0 ? completedFirstPosition - ui.item.index() + 1 : 1;
                        const newPosition = inverseIndex;

                        updateTodoItem(id, position={oldPosition, newPosition}, status='completed');

                    }
                    else if(updates['pending-tasks']===true && updates['completed-tasks']===false)
                    {
                        const newPosition = pendingFirstPosition - ui.item.index();

                        updateTodoItem(id, position={oldPosition, newPosition});
                    }

                    //finally, clear out the updates object
                    updates['pending-tasks']=false;
                    updates['completed-tasks']=false;
                },
                update: function (event, ui) {
                    updates[$(this).attr('id')] = true;
                }
            }).disableSelection();

            $('#completed-tasks').sortable({
                connectWith: '#pending-tasks',
                cursor: 'move',
                handle: '.handle',
                stop: function (event, ui) {
                    const id = ui.item.data('id');
                    const oldPosition = ui.item.data('position');

                    if (updates['pending-tasks']===true && updates['completed-tasks']===true)
                    {
                        const inverseIndex =  pendingFirstPosition > 0 ? pendingFirstPosition - ui.item.index() + 1 : 1;
                        const newPosition = inverseIndex;

                        updateTodoItem(id, position={oldPosition, newPosition}, status='pending');
                    }
                    else if(updates['pending-tasks']===false && updates['completed-tasks']===true)
                    {
                        const newPosition = completedFirstPosition - ui.item.index();

                        updateTodoItem(id, position={oldPosition, newPosition});
                    }

                    //finally, clear out the updates object
                    updates['pending-tasks']=false;
                    updates['completed-tasks']=false;
                },
                update: function (event, ui) {
                    updates[$(this).attr('id')] = true;
                }
            }).disableSelection();
        }
        function updateTodoItem(id, pos, status=null) {
            let data = {
                _token: '{{ csrf_token() }}',
                id: id,
                position: pos,
            };

            if (status) {
                data = {...data, status: status}
            }

            $.easyAjax({
                url: "{{ route('admin.todo-items.updateTodoItem') }}",
                type: 'POST',
                data: data,
                container: '#todo-items-list',
                success: function (response) {
                    $('#todo-items-list').html(response.view);
                    initSortable();
                }
            });
        }

        function showUpdateTodoForm(id) {
            let url = "{{ route('admin.todo-items.edit', ':id') }}"
            url = url.replace(':id', id);

            $.ajaxModal('#application-md-modal', url);
        }

        function deleteTodoItem(id) {
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    let url = "{{ route('admin.todo-items.destroy', ':id') }}";
                    url = url.replace(':id', id);

                    let data = {
                        _token: '{{ csrf_token() }}',
                        _method: 'DELETE'
                    }

                    $.easyAjax({
                        url,
                        data,
                        type: 'POST',
                        container: '#roleMemberTable',
                        success: function (response) {
                            if (response.status == 'success') {
                                $('#todo-items-list').html(response.view);
                                initSortable();
                            }
                        }
                    })
                }
            });
        }

        @if ($user->roles->count() > 0)
            $('#todo-items-list').html(`{!! $todoItemsView !!}`);
        @endif

        initSortable();

        $('body').on('click', '#create-todo-item', function () {
            $.easyAjax({
                url: "{{route('admin.todo-items.store')}}",
                container: '#createTodoItem',
                type: "POST",
                data: $('#createTodoItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        $('#todo-items-list').html(response.view);
                        initSortable();

                        $('#application-md-modal').modal('hide');
                    }
                }
            })
        });

        $('body').on('click', '#update-todo-item', function () {
            const id = $(this).data('id');
            let url = "{{route('admin.todo-items.update', ':id')}}"
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                container: '#editTodoItem',
                type: "POST",
                data: $('#editTodoItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        $('#todo-items-list').html(response.view);
                        initSortable();

                        $('#application-md-modal').modal('hide');
                    }
                }
            })
        });

        $('body').on('change', '#todo-items-list input[name="status"]', function () {
            const id = $(this).data('id');
            let status = 'pending';

            if ($(this).is(':checked')) {
                status = 'completed';
            }

            updateTodoItem(id, null, status);
        });

        //--------------------------------
        //- Application Source BAR CHART -
        //--------------------------------
        function applicationSourceBarChart($labels, $colors, $data) {
            $('#applicationSourceBarChart').remove();
            $('.bar-source-chart').html('').append($('<canvas />').attr('id','applicationSourceBarChart').attr('style','height:300px; min-height:300px'));
            var colorScheme = $colors
            var barChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Count',
                    backgroundColor     :  colorScheme,
                    borderColor         :  colorScheme,
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data
                    }
                ]
            }

            var stackedBarChartCanvas = $('#applicationSourceBarChart').get(0).getContext('2d')
            var stackedBarChartData = jQuery.extend(true, {}, barChartData)

            var stackedBarChartOptions = {
                legend: false,
                responsive              : true,
                    maintainAspectRatio     : false,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                }],
                                yAxes: [{
                                stacked: true
                            }]
                        }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar', 
                data: stackedBarChartData,
                options: stackedBarChartOptions
            })
        }

        //--------------------------------
        //- Application Source Donut CHART -
        //--------------------------------
        function sources_chart(facebook_total, facebook_per, google_total, google_per, indeed_total, indeed_per, linkedin_total, linkedin_per, mycna_total, mycna_per, others_total, others_per, direct_total, direct_per){
            $('#donutChart').remove();
            $('.bar-ch-donut').html('').append($('<canvas />').attr('id','donutChart').attr('style','height:300px; min-height:300px'));
            // console.log($('#donutChart').get(0));return;

            var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
            var donutData        = {
                labels: [
                    'Facebook '+facebook_per+'% ('+facebook_total+')', 
                    'Google '+google_per+'% ('+google_total+')',
                    'Indeed '+indeed_per+'% ('+indeed_total+')',
                    'linkedin '+linkedin_per+'% ('+linkedin_total+')',
                    'MyCNA '+mycna_per+'% ('+mycna_total+')',
                    'others '+others_per+'% ('+others_total+')',
                    'Direct '+direct_per+'% ('+direct_total+')',
                ],
                datasets: [
                    {
                        data: [facebook_total, google_total, indeed_total, linkedin_total, mycna_total, others_total, direct_total],
                        backgroundColor : ['#2777f2','#ea4f35', '#073a9b', '#3178b5', '#4db1f8', '#00a65a', 'rgb(117 117 117)'],
                    }
                ]
            }
            var donutOptions     = {
                maintainAspectRatio : false,
                responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var donutChart = new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions      
            });
        }
        function get_engyj_data2(start_date = '', end_date = ''){
            if(start_date == '' && end_date == ''){
                var start_date = "<?= date('Y-m-d', strtotime('-90 days'));?>";
                var end_date   = "<?= date('Y-m-d');?>";
            }
            var token = '{{ csrf_token() }}';
            $.ajax({
                type:"POST",
                url: "{{ url('admin/reports/get-engyj-data-for-dashboard') }}",
                data:{'_token': token, start_date:start_date, end_date:end_date},
                dataType: "json",
                success:function(data){
                    // $('#total-qualified-percentage').html('(' + data[1]['qual_per'] + '%)');
                    sources_chart(data[2]['facebook_total'], data[2]['facebook_per'], data[2]['google_total'], data[2]['google_per'], data[2]['indeed_total'], data[2]['indeed_per'], data[2]['linkedin_total'], data[2]['linkedin_per'], data[2]['mycna_total'], data[2]['mycna_per'], data[2]['others_total'], data[2]['others_per'], data[2]['direct_total'], data[2]['direct_per']);
                    applicationSourceBarChart(data[9]['labels'],data[9]['colors'],data[9]['data']);
                }});
        }
        // function sources_chart(facebook_total, facebook_per, widget_total, widget_per, indeed_total, indeed_per){
        //     $('#donutChart').remove();
        //     $('.bar-ch-donut').html('').append($('<canvas />').attr('id','donutChart').attr('style','height:150px; min-height:150px'));

        //     var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        //     var donutData        = {
        //         labels: [
        //             'Facebook '+facebook_per+'% ('+facebook_total+')', 
        //             'Widget '+widget_per+'% ('+widget_total+')',
        //             'Indeed '+indeed_per+'% ('+indeed_total+')',
        //         ],
        //         datasets: [
        //             {
        //                 data: [facebook_total, widget_total, indeed_total],
        //                 backgroundColor : ['#f56954', '#00a65a', '#3c8dbc'],
        //             }
        //         ]
        //     }
        //     var donutOptions     = {
        //         maintainAspectRatio : false,
        //         responsive : true,
        //     }
        //     //Create pie or douhnut chart
        //     // You can switch between pie and douhnut using the method below.
        //     var donutChart = new Chart(donutChartCanvas, {
        //         type: 'doughnut',
        //         data: donutData,
        //         options: donutOptions      
        //     });
        // } //sources_chart

        function qualified_unqualified_chart(qualified, qual_per, unqualified, unqual_per){
            $('#donutChart2').remove();
            $('.bar-ch-donut-two').html('').append($('<canvas />').attr('id','donutChart2').attr('style','height:150px; min-height:150px'));

            var donutChartCanvas = $('#donutChart2').get(0).getContext('2d')
            var donutData        = {
                labels: [
                    'Qualified '+qualified+' ('+qual_per+'%)', 
                    'Unqualified '+unqualified+' ('+unqual_per+'%)',
                ],
                datasets: [
                    {
                    data: [qualified, unqualified],
                    backgroundColor : ['#00a65a', '#f56954'],
                    }
                ]
            }
            var donutOptions     = {
                maintainAspectRatio : false,
                responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var donutChart = new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions      
            });
        } //qualified_unqualified_chart

        function bar_chart(days, days_val){
            $('#stackedBarChart_1').remove();
            $('.bar-ch').html('').append($('<canvas />').attr('id','stackedBarChart_1').attr('style','height:230px; min-height:230px'));
            
            var barChartData = {
                labels  : days,
                datasets: [
                    {
                    label               : 'Applicants',
                    backgroundColor     : 'rgba(60,141,188,0.9)',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : days_val
                    }
                ]
            }
            var stackedBarChartCanvas = $('#stackedBarChart_1').get(0).getContext('2d');
            var stackedBarChartData = jQuery.extend(true, {}, barChartData);
            var stackedBarChartOptions = {
                legend: false,
                responsive              : true,
                maintainAspectRatio     : false,
                scales: {
                    xAxes: [{
                    stacked: true,
                    }],
                    yAxes: [{
                    stacked: true
                    }]
                }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar', 
                data: stackedBarChartData,
                options: stackedBarChartOptions
            });
        }

        function bar_chart_qualified(days_val){
            $('#stackedBarChart2').remove();
            $('.bar-ch-2').html('').append($('<canvas />').attr('id','stackedBarChart2').attr('style','height:230px; min-height:230px'));
            
            var barChartData = {
                labels  : ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                datasets: [
                    {
                    label               : 'Qualified',
                    backgroundColor     : '#00a65a',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : days_val[0]
                    },
                    {
                    label               : 'Unqualified',
                    backgroundColor     : '#f56954',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : days_val[1]
                    },
                ]
            }
            var stackedBarChartCanvas = $('#stackedBarChart2').get(0).getContext('2d');
            var stackedBarChartData = jQuery.extend(true, {}, barChartData);
            var temp0 = stackedBarChartData.datasets[0]
            var temp1 = stackedBarChartData.datasets[1]
            stackedBarChartData.datasets[0] = temp1
            stackedBarChartData.datasets[1] = temp0

            var stackedBarChartOptions = {
                legend                  : false,
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar', 
                data: stackedBarChartData,
                options: stackedBarChartOptions
            });
        }
    </script>
    

    <script>
        function get_engyj_data(start_date = '', end_date = ''){
            if(start_date == '' && end_date == ''){
                start_date = "<?= date('Y-m-d', strtotime('-90 days'));?>";
                end_date   = "<?= date('Y-m-d');?>";
            }
            var token = '{{ csrf_token() }}';
            $.ajax({
                type:"POST",
                url: "{{ url('admin/dashboard/get-engyj-data') }}",
                data:{'_token': token, start_date:start_date, end_date:end_date},
                dataType: "json",
                success:function(data){
                    bar_chart(data[0]['days'], data[0]['days_val']);
                    qualified_unqualified_chart(data[1]['qualified'], data[1]['qual_per'], data[1]['unqualified'], data[1]['unqual_per']);
                    // console.log();
                    bar_chart_qualified([data[4]['data']['qualified'],data[4]['data']['unqualified']]);
                    // sources_chart(data[2]['facebook_total'], data[2]['facebook_per'], data[2]['widget_total'], data[2]['widget_per'], data[2]['indeed_total'], data[2]['indeed_per']);
                    
                    stats = data[3];
                    // $('#totalOpenings').html(stats['total_jobs_openings']);
                    $('#totalApplications').html(stats['total_applications']);
                    $('#totalQualified').html(stats['total_qualified']);
                    $('#total-qualified-percentage').html(stats['total-qualified-percentage']);
                    $('#totalInterviewed').html(stats['total_interviewed']);
                    $('#total-interview-percentage').html(stats['total-interview-percentage']);
                    $('#totalOffered').html(stats['total_offered']);
                    $('#total-rejected-percentage').html(stats['total-rejected-percentage']);
                    $('#totalHired').html(stats['total_hired']);
                    $('#total-hired-percentage').html(stats['total-hired-percentage']);
                    
                }	
            });
        }
    </script>
    
    
    <!-- Date Picker Start -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />                    
    <!-- Date Picker END -->


    <script>
        $(function() {
            var start = moment().subtract(90, 'days');
            var end   = moment();
            
            function cb(start, end) {
                var start_end  = start+'_all_'+end;
                var start_date = '';
                var end_date   = '';

                if(start_end == 'NaN_all_NaN'){
                    $('#reportrange span').html('All Time');
                    start_date = '1970-01-01 00:00:00'; 
                    end_date   = '2050-12-31 23:59:59';
                }else{
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    start_date = start.format('YYYY-MM-DD')+' 00:00:00'; 
                    end_date   = end.format('YYYY-MM-DD')+' 23:59:59';
                }
                
                $('#daterange_picker_start').val(start_date);
                $('#daterange_picker_end').val(end_date);
                
                get_engyj_data(start_date, end_date);
                get_engyj_data2(start_date, end_date);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                'All Time'  : ['all', 'all']
                }
            }, cb);
            cb(start, end);
        });
    </script>

@endpush