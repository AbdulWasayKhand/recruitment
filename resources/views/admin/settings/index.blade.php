@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/dropify/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <style> 
    .w-700 { width:700px; }
    .w-317 { width:317px; }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="editSettings" class="ajax-form">
                        @csrf
                        @method('PUT')

                        <div class="form-group w-700">
                            <label for="company_name">@lang('modules.accountSettings.businessName')</label>
                            <input type="text" class="form-control" id="company_name" name="company_name"
                                   value="{{ $global->company_name }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="company_name">@lang('modules.accountSettings.token')</label>
                            <input type="text" class="form-control" id="company_token" name="engyj_api_token" value="{{ $engyj_api_token }}" readonly>
                        </div>
                        <div class="form-group w-700">
                            <label for="company_name">@lang('modules.accountSettings.timings')</label>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="time" class="checkins form-control pull-left" id="checkin" name="company_timings[checkin]" placeholder="Checkin Time" value="{{ Arr::get($company_timings, 'checkin') }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="time" class="checkins form-control pull-right" id="checkout" name="company_timings[checkout]" placeholder="Checkout Time" value="{{ Arr::get($company_timings, 'checkout') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label class="">
                                    <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                        <input
                                            type="checkbox"
                                            <?= isset($company_timings['saturday']) ? 'checked' : "" ?>
                                            value="yes"
                                            name="company_timings[saturday]"
                                            class="flat-red columnCheck"
                                            style="position: absolute; opacity: 0;"
                                        >
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                    </div>
                                    Saturday is a working day
                                </label>
                                <label class="">
                                    <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                        <input
                                            type="checkbox"
                                            <?= isset($company_timings['sunday']) ? 'checked' : "" ?>
                                            value="yes"
                                            name="company_timings[sunday]"
                                            class="flat-red columnCheck"
                                            style="position: absolute; opacity: 0;"
                                        >
                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                    </div>
                                    Sunday  is a working day
                                </label>
                            </div>
                        </div>
                        <div class="form-group w-700">
                            <label for="company_name">@lang('modules.accountSettings.urlLink')</label>
                            <input type="text" class="form-control" id="company_slug" name="company_slug" value="{{ $global->career_page_link }}" required>
                            <input type="hidden" id="slug" value="{{ $global->career_page_link }}" name="slug">
                            <span class="text-success">@lang('modules.accountSettings.companyUrlLinkWillBe') </span> : <span class="font-weight-bold " style="border: none" id="slugValue"><a href="{{ route('jobs.jobOpenings', $global->career_page_link) }}" target="_blank">{{ route('jobs.jobOpenings', $global->career_page_link) }}</a></span>
                        </div>
                        <div class="form-group w-700">
                            <label for="company_email">@lang('modules.accountSettings.email')</label>
                            <input type="email" class="form-control" id="company_email" name="company_email"
                                   value="{{ $global->company_email }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="company_phone">@lang('modules.accountSettings.phone')</label>
                            <input type="tel" class="form-control" id="company_phone" name="company_phone"
                                   value="{{ $global->company_phone }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="exampleInputPassword1">@lang('modules.accountSettings.website')</label>
                            <input type="text" class="form-control" id="website" name="website"
                                   value="{{ $global->website }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="exampleInputPassword1">Twilio Number</label>
                            <input type="text" class="form-control" id="twilio_from_number" name="twilio_from_number"
                                   value="{{ $twilio_from_number }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="exampleInputPassword1">Redirect Number</label>
                            <input type="text" class="form-control" id="call_forward_number" name="call_forward_number"
                                   value="{{ $call_forward_number }}">
                        </div>
                        <div class="form-group w-700">
                            <label for="exampleInputPassword1">@lang('modules.accountSettings.logo')</label>
                             <div class="card">
                                    <div class="card-body">
                                        <input type="file" id="input-file-now" name="logo" class="dropify"
                                            data-default-file="{{ $global->logo_url }}"
                                        />
                                    </div>
                                </div>
                        </div>

                        @if(module_enabled('Subdomain'))
                            <div class="form-group w-700">
                                <label><?php echo app('translator')->get('modules.frontCms.loginBackroundImage'); ?></label>
                                <div class="card">
                                    <div class="card-body">
                                        <input type="file" id="input-file-now" name="login_background" class="dropify"
                                               data-default-file="{{ $global->login_background_image_url }}"
                                        />
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-group w-700">
                            <label for="address">@lang('modules.accountSettings.address')</label>
                            <textarea class="form-control" id="address" rows="5"
                                      name="address">{{ $global->address }}</textarea>
                        </div>

                        <div class="form-group w-700">
                            <label for="address">@lang('modules.accountSettings.defaultTimezone')</label>
                            <select name="timezone" id="timezone" class="form-control select2 custom-select">
                                @foreach(get_timezone_list() as $k=>$tz)
                                    <option  value="{{$k}}" @if($global->timezone == $k) selected @endif>{{ $tz }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group w-700">
                            <label for="address">@lang('modules.accountSettings.changeLanguage')</label>

                            <select class="form-control" name="locale">
                                @foreach($languageSettings as $language)
                                    <option value="{{ $language->language_code }}" @if($global->locale == $language->language_code) selected @endif  data-content='<span class="flag-icon flag-icon-{{ $language->language_code }}"></span> {{ $language->language_name }}'>{{ $language->language_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <hr>


                        <div class="form-group w-700">
                            <label for="exampleInputPassword1">@lang('modules.accountSettings.jobOpeningHeading')</label>
                            <input type="text" class="form-control" id="job_opening_title" name="job_opening_title"
                                   value="{{ $global->job_opening_title }}">
                        </div>

                        <div class="form-group w-700">
                            <label for="address">@lang('modules.accountSettings.jobOpeningText')</label>
                            <textarea class="form-control" id="job_opening_text" rows="5"
                                      name="job_opening_text">{{ $global->job_opening_text }}</textarea>
                        </div>

                        <div class="form-group w-700">
                            <label for="address">@lang('modules.applicationSetting.termsConditions')</label>
                            <textarea class="form-control" id="legal_term" name="legal_term" rows="15" placeholder="Enter text ...">{!! $appSetting->legal_term !!}</textarea>
                        </div>

                        <hr>

                        <button type="button" id="save-form"
                                class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/dropify/dist/js/dropify.min.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/speakingurl@14.0.1/speakingurl.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/slugify/src/slugify.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
    // document.querySelectorAll(".checkins").forEach(function(e){
    //     e.addEventListener('focus', function(){
    //         e.type = 'time';
    //     });
    //     e.addEventListener('blur', function(){
    //         e.type = 'text';
    //     })
    // });
    var drEvent = $('.dropify').dropify();

    drEvent.on('dropify.beforeClear', function(event, element){
        // if(confirm("Do you really want to delete this file ?")){

            $.easyAjax({
            url: "{{ url('deleteLogo') }}",
                data: {
                    '_token': '{{ csrf_token() }}',
                    'company_id': "{{$global->id}}"
                },
                type: "POST",
                redirect: false,
                file: false
            });
        // }
    });
    $('input[type="checkbox"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
    })
//        jQuery(function($) {
            $.slugify("Ätschi Bätschi"); // "aetschi-baetschi"
            $('#slug').slugify('#company_slug'); // Type as you slug

//            $("#slug-target").slugify("#slug-source", {
//                separator: '_' // If you want to change separator from hyphen (-) to underscore (_).
//            });
//        });

        $('#company_slug').on('keyup change', function() {
            if ($('#slug').val() !== '' && $('#slug').val() !== undefined) {
                $('#slugValue').html($('#slug').val());
            } else {

            }
        });
        // For select 2
        $(".select2").select2();

        $('.dropify').dropify({
            messages: {
                default: '@lang("app.dragDrop")',
                replace: '@lang("app.dragDropReplace")',
                remove: '@lang("app.remove")',
                error: '@lang('app.largeFile')'
            }
        });



        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.settings.update', $global->id)}}',
                container: '#editSettings',
                type: "POST",
                redirect: true,
                file: true
            });
        });
    </script>
@endpush
