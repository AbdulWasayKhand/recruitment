@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/node_modules/dropify/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <style>
        .timings {
            display: none;
        }
        .selected-day {
            background: #f6f7fd;
            border: 1px solid lightgray;
            border-radius: 5px;
            padding: 5px;
        }
        .selected-day-checked {
            background: #439dfd;
            border: 1px solid #439dfd;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            border-bottom: none;
            color: white;
            padding: 5px;
        }
        .selected-day-time {
            border: 1px solid lightgray;
            padding: 20px;
            border-color: #439dfd;
            width: 100%!important;
        }
        .sheld2{
            z-index:4444;
            position: fixed;
            width: 100%;
            height: 100%;
            color: white;
            font-size: 30px;
            text-align: center;
            padding-top: 20%;
            top: 0;
            left:0;
            display:none;
        }
        .sheld2{
            /* display:none; */
            background-color: rgba(0,0,0,.3);
        }
        @media screen and (max-width:767px){
            .sheld2 > div > img {
                margin-top: 70%!important;
            }
            
        }
    </style>
@endpush

@section('content')
        <div class="sheld2" id="sheld2">
            <div id="fliper">
                <img style="width:50px;" src="{{ url('/assets/')}}/loading.gif" />
                <br />
                <small><i>Please wait....</i></small>
            </div> 
        </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                        @if($optionEmp == '')
                        <a href="{{ url('oauth2callback') }}" class="btn pull-right p-0">
                            <img src="{{ asset('assets/images/link-with-google.png') }}">
                        </a>
                        @else 
                        <a onclick="syncGC()" @if($_SERVER['SERVER_NAME'] == 'localhost') style="color:white;" @else  style="color:white;display:none;"  @endif class="btn btn-success btn-sm pull-right">
                            Sync Google Calander Now
                        </a>
                        <a class="btn btn-primary btn-sm mr-1 pull-right" style="color:white;" onclick="unlinkGC()">
                            Unlink with Google Calendar
                        </a>
                        @endif
                    <div id="verify-mobile">
                        @include('sections.admin_verify_phone')
                    </div>
                    <form id="editSettings" class="ajax-form" style="margin-top:30px;">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name">@lang('app.name')</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   value="{{ ucwords($user->name) }}">
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('app.email')</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="company_phone">@lang('app.password')</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <span class="help-block"> @lang('messages.passwordNote')</span>
                        </div>

                        @if ($smsSettings->nexmo_status == 'deactive')
                            <!-- text input -->
                            <div class="form-group">
                                <label>@lang('app.mobile')</label>
                                <div class="form-row">
                                    <div class="col-sm-3">
                                        <select name="calling_code" id="calling_code" class="form-control selectpicker" data-live-search="true" data-width="100%" >
                                            @foreach ($calling_codes as $code => $value)
                                                <option value="{{ $value['dial_code'] }}"
                                                @if ($user->calling_code)
                                                    {{ $user->calling_code == $value['dial_code'] ? 'selected' : '' }}
                                                @endif>{{ $value['dial_code'] . ' - ' . $value['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="mobile" value="{{ $user->mobile }}">
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="" onclick="bussness_hours.style.display = document.querySelector('input[name=can_text]').checked ? 'none' : 'block'; ">
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if($user->can_text == 1) checked @endif
                                        value="1"
                                        name="can_text"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                @lang('app.smsAdminOptionOnOffTitle')
                            </label>
                        </div>
                        <div class="form-group" id='bussness_hours' style='display:  {{ ( $user->can_text == 1 ) ? "block" : "none" }}; '>
                            <label class="">
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if($user->bussness_hours_only == 1) checked @endif
                                        value="1"
                                        name="bussness_hours_only"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                Alerts During Business Hours Only.
                            </label>
                        </div>
                            <div class="form-group">
                                <label>@lang('modules.applicationSetting.smsAutoReminderHourQualifyHiringManager')</label>
                                    <select class="form-control col-md-6 col-sm-12" name="sms_auto_reminder_hour">
                                        @foreach(['disable', '1', '2', '3', ] as $timing)
                                            <option value="{{ $timing }}" {{ $user->sms_auto_reminder_hour == $timing ? 'selected' : '' }}>{{ $timing }}</option>                                            
                                        @endforeach
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>@lang('modules.applicationSetting.smsAutoReminderDayQualifyHiringManager')</label>
                                <select class="form-control col-md-6 col-sm-12" name="sms_auto_reminder_day">
                                    @foreach(['disable', '1', '2', '3', ] as $timing)
                                        <option value="{{ $timing }}" {{ $user->sms_auto_reminder_day == $timing ? 'selected' : '' }}>{{ $timing }}</option>                                            
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Change Stage: </label><br/>
                                <small>This is individual account setting, you can change stage/status on single and collective level in all Applicants.</small>
                                <select class="form-control col-md-6 col-sm-12" name="job_wise">
                                    <option value="1" @if($user->job_wise == 1) selected @endif >Job wise</option>
                                    <option value="0" @if($user->job_wise == 0) selected @endif >Workflow/Status wise</option>
                                </select>
                            </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">@lang('app.image')</label>
                            <div class="card">
                                <div class="card-body">
                                    <input type="file" id="input-file-now" name="image" accept=".png,.jpg,.jpeg" class="dropify"
                                           data-default-file="{{ $user->profile_image_url  }}"
                                    />
                                </div>
                            </div>
                        </div>
                        <hr />
                        @if($optionEmp != '')
                        <div class="form-group">
                                <h2><b>@lang('modules.googleCalendarSettings.googleCalendarSettings')</b></h2>
                                <label>
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if($user->block_all_day_event == 1) checked @endif
                                        value="1"
                                        name="block_all_day_event"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                @lang('modules.googleCalendarSettings.enableAllDayEvents')
                            </label>
                        </div>
                        @endif


                        <div class="form-row">
                            <div id="schedule_view" class="row" style="padding:10px;" >
                                <div class="col-md-12 @if($profile_online_schedule == 0) timings @endif w-100" id='overall'  >
                                    <div class="clearfix"><hr /></div>
                                    <div class="row w-100">
                                        <div class="col-md-12" >
                                            <h2><b>Interview Scheduling</b></h2>
                                        </div>

                                        <div class="col-md-12">
                                            <h4 class="mb-0">Interview Schedule Calendar</h4>
                                            <small>Select the number of days that will be available for the applicant to schedule their interview.</small>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <select name="schedule[global][available_weaks]" class="form-control" style="margin-top: 10px;">
                                                <option @if(Arr::get($profile_schedule, 'global.available_weaks') == 1) selected @endif value="1">Next 7 days</option>
                                                <option @if(Arr::get($profile_schedule, 'global.available_weaks') == 2 || Arr::get($profile_schedule, 'global.available_weaks') != 1) selected @endif value="2">Next 14 days</option>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <h4 class="mb-0">Interview Time-Slot Duration</h4>
                                            <small>Select the average duration of an interview you conduct.</small>
                                        </div>

                                        <div class="col-md-4">
                                            <select name="schedule[global][interview_slot]" class="form-control" style="margin-top: 10px;">
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 30) selected @endif value="15">15 Minutes</option>
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 30) selected @endif value="30">30 Minutes</option>
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 45) selected @endif value="45">45 Minutes</option>
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 60) selected @endif value="60">1 Hour</option>
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 90) selected @endif value="90">1 Hour and 30 Minutes</option>
                                                <option @if(Arr::get($profile_schedule, 'global.interview_slot') == 120) selected @endif value="120">2 Hour</option>
                                            </select>
                                        </div>
                                        
                                        {{-- <div class="col-md-12">
                                            <label style="cursor:pointer; margin-top: 20px;">
                                            <h4 class="mb-0">
                                                <input type="checkbox" @if(Arr::get($profile_schedule, 'global.exclude_lunch')) checked @endif name="schedule[global][exclude_lunch]" style="position:initial" value="1" flip="lunch_break"/>
                                                Exclude lunch break from interview schedule
                                            </h4>
                                            <small style="margin-left: 20px;">If selected, candidates will not be able to schedule interviews during your lunch break.</small>
                                            </label>
                                        </div> --}}
                                    </div>
                                    {{-- <div style="margin-left: 20px;" class="row @if(!Arr::get($profile_schedule, 'global.exclude_lunch')) timings @endif lunch_break" id="">
                                        <div  class="row w-100" >
                                            <div class="form-group col-md-4">
                                                <input  type="{{ Arr::get($profile_schedule, 'global.break_start') ? 'time' : 'text'}}" value="{{Arr::get($profile_schedule, 'global.break_start')}}" name="schedule[global][break_start]" placeholder="Start (eg: 12:00 PM)" class="form-control d_type" />
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="{{ Arr::get($profile_schedule, 'global.break_end') ? 'time' : 'text'}}" value="{{Arr::get($profile_schedule, 'global.break_end')}}" name="schedule[global][break_end]" placeholder="End (eg: 01:00 PM)" class="form-control d_type" />
                                            </div>
                                        </div>
                                    </div> --}}

                                    <div class="col-md-12" style="margin-top:20px;">
                                        <h4 class="mb-0"><b>Availability</b></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <label style="cursor:pointer; margin-top: 20px;">
                                        <h4 class="mb-0">
                                            <input type="radio" @if(Arr::get($profile_schedule, 'global.available_anytime') == 1) checked @endif name="schedule[global][available_anytime]" style="position:initial" value="1" flip="available_anytime" class="toggle_timings"/>
                                            I am available to conduct interviews anytime during my shift.
                                        </h4>
                                        <small style="margin-left: 20px;">
                                            Candidates can schedule their interviews anytime during your work schedule.
                                        </small>
                                        </label>
                                    </div>

                                    <div style="margin-left: 25px;" class="row @if(!Arr::get($profile_schedule, 'global.available_anytime') == 1) timings @endif toggle_me available_anytime" id="available_anytime">
                                        <div  class="row w-100" >
                                            <div class="form-group col-md-4">
                                                <input  type="{{ Arr::get($profile_schedule, 'global.time_in') ? 'time' : 'text'}}" value="{{Arr::get($profile_schedule, 'global.time_in')}}" name="schedule[global][time_in]" placeholder="Time In (eg: 09:00 AM)" class="form-control d_type" />
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="{{ Arr::get($profile_schedule, 'global.time_out') ? 'time' : 'text'}}" value="{{Arr::get($profile_schedule, 'global.time_out')}}" name="schedule[global][time_out]" placeholder="Time out (eg: 09:00 PM)" class="form-control d_type" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label style="cursor:pointer; margin-top: 20px;">
                                        <h4 class="mb-0">
                                            <input type="radio" @if(Arr::get($profile_schedule, 'global.available_anytime') == 0) checked @endif name="schedule[global][available_anytime]" style="position:initial" value="0" flip="weekly_timings" class="toggle_timings"/>
                                            I would like to schedule my availability to conduct interviews for each day of the week.
                                        </h4>
                                        <small style="margin-left: 20px;">
                                            Candidates can schedule their interviews only during your specified hours.
                                        </small>
                                        </label>
                                    </div>
                                <div style="margin-left: 75px;" class="@if(!Arr::get($profile_schedule, 'global.available_anytime') == 0) timings @endif toggle_me weekly_timings" id="weekly_timings">
                                    @foreach(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',  'Sunday'] as $day)
                                    @php
                                    $active = Arr::get($profile_schedule, "days.".strtolower($day).".active");
                                    $time_in = Arr::get($profile_schedule, "days.".strtolower($day).".time_in");
                                    $time_out = Arr::get($profile_schedule, "days.".strtolower($day).".time_out");
                                    $break_start = Arr::get($profile_schedule, "days.".strtolower($day).".break_start");
                                    $break_end = Arr::get($profile_schedule, "days.".strtolower($day).".break_end");
                                    $active_break = Arr::get($profile_schedule, "days.".strtolower($day).".active_break");
                                    @endphp
                                        <div class="row">
                                            <div class="col-md-9 @if(Arr::get($profile_schedule, "days.".strtolower($day).".active") == 1) selected-day-checked @else selected-day @endif" >
                                                <label style="padding:0; margin:0"><input type="checkbox" @if(Arr::get($profile_schedule, "days.".strtolower($day).".active")) checked @endif name="schedule[days][{{strtolower($day)}}][active]" flip='{{$day}}'  style="position:initial" value="1" class="fliper" /> {{$day}}</label>
                                            </div>
                                            <div id="{{$day}}" style="margin-left: 0;" class="row col-md-9 selected-day-time  @if(Arr::get($profile_schedule, "days.".strtolower($day).".active") != 1) timings @endif {{$day}}" >
                                                <div class="col-md-12"><h4>{{$day}}'s Shift Timings</h4></div>
                                                <div class="col-md-6" >
                                                    <input  type="{{ $time_in ? 'time' : 'text' }}" value="{{$time_in}}" name="schedule[days][{{strtolower($day)}}][time_in]" placeholder="@lang('modules.profileSchedule.timeIn')" class="form-control d_type" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="{{ $time_out ? 'time' : 'text' }}" value="{{$time_out}}" name="schedule[days][{{strtolower($day)}}][time_out]" placeholder="@lang('modules.profileSchedule.timmeOut')" class="form-control d_type" />
                                                </div>
                                                <div class="col-md-12 " style="margin-top:5px;">
                                                    <label>
                                                        <input type="checkbox" @if($active_break) checked @endif name="schedule[days][{{strtolower($day)}}][active_break]" style="position:initial" value="1" flip="ab_{{$day}}" />
                                                        <small>Include Break Timings</small>
                                                    </label>
                                                </div>
                                                <div class="col-md-6 @if(!$active_break) timings @endif ab_{{$day}}"  id="">
                                                    <input  type="{{ $break_start ? 'time' : 'text' }}" value="{{$break_start}}" name="schedule[days][{{strtolower($day)}}][break_start]" placeholder="Start (eg: 12:00 PM)" class="form-control d_type" />
                                                </div>
                                                <div class="col-md-6 @if(!$active_break) timings @endif ab_{{$day}}"  id="">
                                                    <input type="{{ $break_end ? 'time' : 'text' }}" value="{{$break_end}}" name="schedule[days][{{strtolower($day)}}][break_end]" placeholder="End (eg: 01:00 PM)" class="form-control d_type" />
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="save-form"
                                class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        <button type="reset"
                                class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/dropify/dist/js/dropify.min.js') }}" type="text/javascript"></script>
    <script>
        function unlinkGC(){
        // href="{{ url('unlinkGC') }}"

        swal({
                title: "@lang('errors.areYouSure')",
                text: "If you unlink your account with Google Calendar it may cause a malfunction on past events",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Unlink Anyways",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    location.href = "{{ url('unlinkGC') }}";
                }
            });
    }
    $('input[type="checkbox"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
    })
        function createSchedule() {
            var url = "{{ route('admin.schedule-profile') }}";
            $.ajaxModal('#schedule_view', url);
        }

        $(".toggle_timings").on("click", function () {
            $(".toggle_me").hide();
        });

        $(".d_type").on('focus', function() {
            if(this.value == '')
                this.type = 'time';
        });

        $(".d_type").on('blur', function(){
            if(this.value == '')
                this.type = 'text';
        });

        $('input[flip]').on('click', function(){
            var check = this.checked ? 1 : 0;
            if($(this).hasClass('fliper')){
                $(this).parent().parent().removeClass('selected-day-checked');
                $(this).parent().parent().removeClass('selected-day');
                if(check == 1){
                    $(this).parent().parent().addClass('selected-day-checked');
                }else{
                    $(this).parent().parent().addClass('selected-day');
                }
            }
            if(check == 1){
                $("." + this.getAttribute('flip')).show();
                if($("." + this.getAttribute('flip')).hasClass("row")){
                    $("." + this.getAttribute('flip')).css('display', 'flex');
                }
                // $('div[invert]').hide();
            }else{
                $("." + this.getAttribute('flip')).hide();
                // $('div[invert]').css('display', 'flex');
            }
        });

        $('.dropify').dropify({
            messages: {
                default: '@lang("app.dragDrop")',
                replace: '@lang("app.dragDropReplace")',
                remove: '@lang("app.remove")',
                error: '@lang('app.largeFile')'
            }
        });

        $('body').on('click', '#change-mobile', function () {
            $.easyAjax({
                url: "{{ route('changeMobile') }}",
                container: '#verify-mobile',
                type: "GET",
                success: function (response) {
                    $('#verify-mobile').html(response.view);
                    $('.selectpicker').selectpicker({
                        style: 'btn-info',
                        size: 4
                    });
                }
            })
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.profile.update', $user->id)}}',
                container: '#editSettings',
                type: "POST",
                redirect: true,
                file: true
            })
        });
    </script>

    <script>
        var x = '';
        
        function clearLocalStorage() {
            localStorage.removeItem('otp_expiry');
            localStorage.removeItem('otp_attempts');
        }

        function checkSessionAndRemove() {
            $.easyAjax({
                url: '{{ route('removeSession') }}',
                type: 'GET',
                data: {'sessions': ['verify:request_id']}
            })
        }

        function startCounter(deadline) {
            x = setInterval(function() {
                var now = new Date().getTime();
                var t = deadline - now;

                var days = Math.floor(t / (1000 * 60 * 60 * 24));
                var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60));
                var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((t % (1000 * 60)) / 1000);

                $('#demo').html('Time Left: '+minutes + ":" + seconds);
                $('.attempts_left').html(`${localStorage.getItem('otp_attempts')} attempts left`);

                if (t <= 0) {
                    clearInterval(x);
                    clearLocalStorage();
                    checkSessionAndRemove();
                    location.href = '{{ route('admin.profile.index') }}'
                }
            }, 1000);
        }

        if (localStorage.getItem('otp_expiry') !== null) {
            let localExpiryTime = localStorage.getItem('otp_expiry');
            let now = new Date().getTime();

            if (localExpiryTime - now < 0) {
                clearLocalStorage();
                checkSessionAndRemove();
            }
            else {
                $('#otp').focus().select();
                startCounter(localStorage.getItem('otp_expiry'));
            }
        }

        function sendOTPRequest() {
            $.easyAjax({
                url: '{{ route('sendOtpCode.account') }}',
                type: 'POST',
                container: '#request-otp-form',
                messagePosition: 'inline',
                data: $('#request-otp-form').serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        localStorage.setItem('otp_attempts', 3);

                        $('#verify-mobile').html(response.view);
                        $('.attempts_left').html(`3 attempts left`);

                        let html = `<div class="alert alert-info mb-0" role="alert">
                            @lang('messages.info.verifyAlert')
                            <a href="{{ route('admin.profile.index') }}" class="btn btn-warning">
                                @lang('menu.profile')
                            </a>
                        </div>`;

                        $('#verify-mobile-info').html(html);
                        $('#otp').focus();

                        var now = new Date().getTime();
                        var deadline = new Date(now + parseInt('{{ config('nexmo.settings.pin_expiry') }}')*1000).getTime();

                        localStorage.setItem('otp_expiry', deadline);
                        // intialize countdown
                        startCounter(deadline);
                    }
                    if (response.status == 'fail') {
                        $('#mobile').focus();
                    }
                }
            });
        }

        function sendVerifyRequest() {
            $.easyAjax({
                url: '{{ route('verifyOtpCode.account') }}',
                type: 'POST',
                container: '#verify-otp-form',
                messagePosition: 'inline',
                data: $('#verify-otp-form').serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        clearLocalStorage();

                        $('#verify-mobile').html(response.view);
                        $('#verify-mobile-info').html('');

                        // select2 reinitialize
                        $('.selectpicker').selectpicker({
                            style: 'btn-info',
                            size: 4
                        });
                    }
                    if (response.status == 'fail') {
                        // show number of attempts left
                        let currentAttempts = localStorage.getItem('otp_attempts');

                        if (currentAttempts == 1) {
                            clearLocalStorage();
                        }
                        else {
                            currentAttempts -= 1;

                            $('.attempts_left').html(`${currentAttempts} attempts left`);
                            $('#otp').focus().select();
                            localStorage.setItem('otp_attempts', currentAttempts);
                        }

                        if (Object.keys(response.data).length > 0) {
                            $('#verify-mobile').html(response.data.view);

                            // select2 reinitialize
                            $('.selectpicker').selectpicker({
                                style: 'btn-info',
                                size: 4
                            });

                            clearInterval(x);
                        }
                    }
                }
            });
        }

        $('body').on('submit', '#request-otp-form', function (e) {
            e.preventDefault();
            sendOTPRequest();
        })

        $('body').on('click', '#request-otp', function () {
            sendOTPRequest();
        })

        $('body').on('submit', '#verify-otp-form', function (e) {
            e.preventDefault();
            sendVerifyRequest();
        })

        $('body').on('click', '#verify-otp', function() {
            sendVerifyRequest();
        })
        function syncGC(){
            $('.sheld2').show();
            $.get("{{ url('SyncGC') }}?user_id={{$user->id}}", function(data, status){
                if(data.status == 'success'){
                    $('.sheld2').hide();
                    location.reload();
                }else{
                    $('.sheld2').hide();
                    alert(data.message);
                }
            });
        }
    </script>
@endpush