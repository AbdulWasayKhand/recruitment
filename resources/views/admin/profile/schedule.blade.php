<style>
    .timings {
        display: none;
    }
</style>
<div class="modal-header">
    <h4 class="modal-title"><i class="icon-plus"></i> @lang('modules.interviewSchedule.scheduleInterview')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<form class="ajax-schedule-form" id="ajax_form" method="POST">
<div class="modal-body">
    <h3>@lang('modules.profileSchedule.title')</h3>
    <div class="row">
        <div class="col-md-3">
            <label>@lang('modules.profileSchedule.useOnlineSchedule')</label>
            <label><input type="radio" name="profile_online_schedule" value="1" @if($profile_online_schedule == 1) checked @endif flip='overall'>@lang('modules.profileSchedule.yes')</label>
            <label><input type="radio" name="profile_online_schedule" value="0" @if($profile_online_schedule == 0) checked @endif  flip='overall' >@lang('modules.profileSchedule.no')</label>
        </div>
    </div>
    <div class="row">
        <label>Your Timezone is: {{ $timezone }}</label>
    </div>
    <div class="@if($profile_online_schedule == 0) timings @endif w-100" id='overall'  >
        <div class="clearfix"><hr /></div>
        <div class="row border-bottom">
            <div class="col-md-2">
                <h4>Lunch Break</h4>
            </div>
            <div class="col-md-6">
                    <label><input type="checkbox" @if(Arr::get($profile_schedule, 'lunch_break_stay_offline')) checked @endif name="lunch_break_stay_offline"  onclick=" lunch_break.style.display = this.checked ? 'flex' : 'none'; " style="position:initial"/>@lang('modules.profileSchedule.stayOffInLunch')</label>
                </div>
            <div id="lunch_break" class="row w-100" style="@if(Arr::get($profile_schedule, 'lunch_break_stay_offline')) display:flex @else display:none @endif ;">
                <div class="form-group col-md-6">
                    <input  type="text" value="@if(Arr::get($profile_schedule, 'lunch_break_start') != '') {{Arr::get($profile_schedule, 'lunch_break_start')}} @endif" name="lunch_break_start" placeholder="Start (eg: 12:00 PM)" class="form-control d_type" />
                </div>
                <div class="form-group col-md-6">
                    <input type="text" value="@if(Arr::get($profile_schedule, 'lunch_break_end') != '') {{Arr::get($profile_schedule, 'lunch_break_end')}} @endif" name="lunch_break_end" placeholder="End (eg: 01:00 PM)" class="form-control d_type" />
                </div>
            </div>
        </div>
        @foreach(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',  'Sunday'] as $day)
            <div class="clearfix"><br /></div>
            <div class="row @if($day != 'Sunday') border-bottom @endif">
                <div class="col-md-2">
                <h4>{{$day}} <?php $in = $day."_time_in" ; $out = $day."_time_out"; $stay = $day."_stay_offline"; ?></h4>
                </div>
                <div class="col-md-6">
                        <label><input type="radio" @if(Arr::get($profile_schedule, $stay) == 1) checked @endif name="{{$day}}_stay_offline" value="1" flip='{{$day}}' >@lang('modules.profileSchedule.useSchedule')</label>
                        <label><input type="radio" @if(Arr::get($profile_schedule, $stay) == 0) checked @endif name="{{$day}}_stay_offline" value="0" flip='{{$day}}' >@lang('modules.profileSchedule.stayOffline')</label>
                </div>
                <div id="{{$day}}" class="row w-100 @if(Arr::get($profile_schedule, $stay) == 0) timings @endif" >
                    <div class="form-group col-md-6" >
                        <input  type="text" value="@if(Arr::get($profile_schedule, $in) != '') {{Arr::get($profile_schedule, $in)}} @endif" name="{{$day}}_time_in" placeholder="@lang('modules.profileSchedule.timeIn')" class="form-control d_type" />
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" value="@if(Arr::get($profile_schedule, $out) != '') {{Arr::get($profile_schedule, $out)}} @endif" name="{{$day}}_time_out" placeholder="@lang('modules.profileSchedule.timmeOut')" class="form-control d_type" />
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
    <button type="button" class="btn btn-success save-schedule" onclick="saveProfileSchedule()">@lang('app.submit')</button>
</div>
</form>

<script>
    function saveProfileSchedule(){
        $.easyAjax({
            url: "{{ route('admin.schedule-profile-save') }}",
            type: 'POST',
            data: $(ajax_form).serialize() + '&_token={{csrf_token()}}',
            success: function (response) {
                if(response.status == 'success') {
                    
                    // $(this).prop('disabled', false);
                }
            }
        })
    }
    document.querySelectorAll('.d_type').forEach(function(e) {
        e.addEventListener('focus', function(){
            e.type = 'time';
        });
        e.addEventListener('blur', function(){
            e.type = 'text';
        });
    });
    document.querySelectorAll('input[flip]').forEach(function(e) {
        e.addEventListener('click', function(){
            if(e.value == 1){
                if(e.getAttribute('flip') != 'overall'){
                    document.getElementById(e.getAttribute('flip')).style.display = 'flex';
                }else{
                    document.getElementById(e.getAttribute('flip')).style.display = 'block';
                }
            }else{
                document.getElementById(e.getAttribute('flip')).style.display = 'none';
            }
        });
    });

</script>