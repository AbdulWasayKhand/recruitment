@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">@lang('app.createNew')</h4>

                    <form class="ajax-form" method="POST" id="createForm">
                        @csrf

                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="address">Select Question Type</label>
                                    <select name="question_type" id="question_type" class="form-control">
                                        <option value="text">Text</option>
                                        <option value="dropdown">Dropdown</option>
                                        <option value="radio">Radio</option>
                                        <option value="checkbox">Checkbox</option>
                                        {{-- <option value="textarea">Paragraph</option> --}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="address">@lang('menu.question')</label>
                                    <input type="text" name="question" class="form-control" placeholder="@lang('menu.question')">
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="address">@lang('app.requiredForApplicant')</label>
                                    <select name="required" class="form-control">
                                        <option value="yes">@lang('app.yes')</option>
                                        <option value="no">@lang('app.no')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="address">@lang('app.category')</label>
                                    <select name="category_id" class="form-control" onchange="on_category_change(this)">
                                        <option></option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                        <option value="add">@lang('app.add')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="address">@lang('menu.sort') @lang('menu.order')</label>
                                    <input type="number" name="sort_order" class="form-control" placeholder="@lang('menu.sort') @lang('menu.order')" value="0" min="0">
                                </div>
                            </div>
                            <div class="col-md-9" id="qualify_on" style="display:none">
                                <div class="form-group">
                                    <label for="address">@lang('app.isQualifyQuestion')</label>
                                    <select name="qualify_on" name="required" class="form-control select-is-qualify" id="qualify_option">
                                        <option value="no">@lang('app.no')</option>
                                        <option value="yes">@lang('app.yes')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8 px-0">
                                <div id="field-content"> </div>
                                <div id="btn-content" class="px-2 mb-3">
                                 <a href="javascript:;" id="addFields" style="text-decoration: underline;display:none">Add More Option</a>
                                <div><span id="e_msg" style="color:red;"></span></div>
                                </div>
                            </div>
                            
                        </div>

                        <button type="button" id="save-form" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-md in" id="addCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <form class="modal-content" method="post" action="{{route('admin.question.add-category')}}" onsubmit="return false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body row">
                        <div class="form-group col-md-12">
                            <label>@lang('app.name')</label>
                            <input id="c_name" placeholder="Category Name" type="text" name="name" class="form-control" >
                        </div>
                </div>
                <div class="modal-footer">
                    @csrf
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Add Category</button>
                </div>

            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
<script>
    
    $("#qualify_option").on('change', function(){
        if(this.value == 'no'){
            $(".input-group-append").css('width', '0');
        }
    });
    $('#save-form').click(function () {
        e_msg.innerHTML = ''
        var num = 0;
        var flag = false;
        document.querySelectorAll("#field-content input[type=text]").forEach(function (e){
            if(e.value ==  ""){
                e.style.borderColor = '#ced4da';
                
            } 
        });
        if(['dropdown', 'radio','checkbox'].includes(question_type.value)){
            if(option_nos < 2){
                e_msg.innerHTML = 'Please add atlest 2 options.';
                flag = true;
            }else{
                document.querySelectorAll("#field-content input[type=text]").forEach(function (e){
                    if(e.value ==  ""){
                       e.style.borderColor = 'red';
                       
                    }else{
                        num++;
                    } 
                });
                if(num < 2){
                    e_msg.innerHTML = 'Please fill in altest 2 fields.'
                    flag = true;
                }
            }
            
        }
        if(!flag){
            $.easyAjax({
                url: '{{route('admin.questions.store')}}',
                container: '#createForm',
                type: "POST",
                redirect: true,
                data: $('#createForm').serialize()
            })
        }
        
    });

    function on_category_change(select) {
        if(select.value == 'add') {
            $(select).val(0)
            c_name.value = null;
            $('#addCategoryModal').modal('show');
        }
    }

    $('#addCategoryModal form').on('submit', function (e) {
        e.preventDefault();

        $.easyAjax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $('#addCategoryModal form').serialize(),
            success: function(response) {
                if(response.name) {
                    $('[name=category_id] option:last').remove();
                    $('[name=category_id]').append(`<option value="${response.id}">${response.name}</option>`);
                    $('[name=category_id]').append(`<option value="add">Add</option>`);

                    $('[name=category_id]').val(response.id);

                    $('#addCategoryModal').modal('hide');
                }
            }
        });
        return false;
    })

    $('.select-is-qualify').on('change', function(e) {
        e.preventDefault();
        var question_type = $('[name=question_type]').val();
        if(question_type !== 'text') {
            
        }
    });
</script>

<script>
var html_qualify = function(type) {
                return `<div class="input-group">
                    <input type="text" name="${type}[]" class="form-control">
                </div>`;
            }
var html_dropdown = `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add Dropdown Value</label>
                            <input type="text" name="dropdown[]" class="form-control">
                        </div>
                    </div>`;
var html_radio =  `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add radio Value</label>
                            <input type="text" name="radio[]" class="form-control">
                        </div>
                    </div>`;
var html_checkbox = `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add Checkbox Value</label>
                            <div class="input-group">
                                <input type="text" name="checkbox[]" class="form-control">
                                
                            </div>
                        </div>
                    </div>`;
var html_text = function(type, i) {
    return `
        <div class="col-md-12">
            <div class="form-group">
                <label>Add ${type[0].toUpperCase() + type.slice(1)} Value</label>
                <div class="input-group">
                    <input type="text" name="${type}[]" class="form-control">
                    ${
                        $('.select-is-qualify').val() == 'yes' ? `
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <input style="margin-right: 10px;" id="cb_${i}" type="checkbox" name="qualifiable[]" value="${i}">
                                    <label style="margin-bottom: 0;" for="cb_${i}">This is qualifying answer?</label>
                                </div>
                            </div>
                        ` : ``
                    }
                </div>
            </div>
        </div>
    `;
}
var html;
$('#question_type,.select-is-qualify').on('change', function() {
    option_nos = 0;
    var val_rec = $('#question_type').val();
    if(val_rec == "text"){
        $('#field-content').empty();
        $('#addFields').hide();
        $('#qualify_on').hide();
    }
    if(val_rec == "textarea"){
        $('#field-content').empty();
        $('#addFields').hide();
        $('#qualify_on').hide();
    }
    if(val_rec == "dropdown"){
        $('#field-content').empty();
        $('#addFields').show();
        html = html_dropdown;
        $('#qualify_on').show();
    }
    if(val_rec == "radio"){
        $('#field-content').empty();
        $('#addFields').show();
        html = html_radio;
        $('#qualify_on').show();
    }
    if(val_rec == "checkbox"){
        $('#field-content').empty();
        $('#addFields').show();
        html = html_checkbox;
        $('#qualify_on').show();
    }
});
var option_nos = 0;
$('#addFields').on('click', function() {
    e_msg.innerHTML = '';
    document.querySelectorAll("#field-content input[type=text]").forEach(function (e){
            if(e.value ==  ""){
                e.style.borderColor = '#ced4da';
                
            } 
        });
    var val_rec = $('#question_type').val();
    $('#field-content').append(html_text(val_rec, option_nos));
    option_nos++;
});

</script>
@endpush