@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">@lang('app.edit')</h4>

                    <form class="ajax-form" method="POST" id="createForm">
                        @csrf

                        <input name="_method" type="hidden" value="PUT">

                    <div id="education_fields"></div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="address">Select Question Type</label>
                                <select name="question_type" id="question_type" class="form-control">
                                    <option value="text">Text</option>
                                    <option value="dropdown">Dropdown</option>
                                    <option value="radio">Radio</option>
                                    <option value="checkbox">Checkbox</option>
                                    {{-- <option value="textarea">Paragraph</option> --}}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-9 nopadding">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="question" value="{{ $question->question }}" class="form-control" placeholder="Enter Your Question">

                                </div>
                            </div>
                        </div>                        
                        <div class="col-md-9">
                            <div class="form-group">
                            <label for="address">@lang('app.requiredForApplicant')</label>
                                <select name="required" class="form-control">
                                    <option @if($question->required == 'yes') selected @endif value="yes">@lang('app.yes')</option>
                                    <option @if($question->required == 'no') selected @endif  value="no">@lang('app.no')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="address">@lang('app.category')</label>
                                <select name="category_id" class="form-control" onchange="on_category_change(this)">
                                    <option></option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{ $question->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                                    <option value="add">@lang('app.add')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="address">@lang('menu.sort') @lang('menu.order')</label>
                                <input type="number" name="sort_order" class="form-control" placeholder="@lang('menu.sort') @lang('menu.order')" value="{{$question->sort_order}}" min="0">
                            </div>
                        </div>
                        <div class="col-md-9" id="qualify_on">
                            <div class="form-group">
                                <label for="address">@lang('app.isQualifyQuestion')</label>
                                <select name="qualify_on" class="form-control select-is-qualify" id="qualification">
                                    <option value="yes">@lang('app.yes')</option>
                                    <option value="no" @if(isset($question->custom_fields[0]) && ($question->custom_fields[0]->question_qualifiable == null || $question->custom_fields[0]->question_qualifiable == '')) selected @endif>@lang('app.no')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8 px-0">
                            <div id="field-content"> </div>
                            <div id="btn-content" class="px-2 mb-3">
                                <a href="javascript:;" id="addFields" style="text-decoration: underline;display:none">Add More Option</a>
                                <div><span id="e_msg" style="color:red;"></span></div>
                            </div>
                        </div>
                    </div>

                    <button type="button" id="save-form" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-md in" id="addCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <form class="modal-content" method="post" action="{{route('admin.question.add-category')}}" onsubmit="return false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body row">
                        <div class="form-group col-md-12">
                            <label>@lang('app.name')</label>
                            <input id="c_name" placeholder="Category Name"  type="text" name="name" class="form-control">
                        </div>
                </div>
                <div class="modal-footer">
                    @csrf
                    <button type="button" class="btn default" data-dismiss="modal" onclick="c_name.value = ''">Close</button>
                    <button type="submit" class="btn blue" >Add Category</button>
                </div>

            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
<script>
    // Update Question
    $('#save-form').click(function () {
        e_msg.innerHTML = ''
        var num = 0;
        var flag = false;
        document.querySelectorAll("#field-content input[type=text]").forEach(function (e){
            if(e.value ==  ""){
                e.style.borderColor = '#ced4da';
                
            } 
        });
        if(['dropdown', 'radio','checkbox'].includes(question_type.value)){
            var opt = document.querySelectorAll("#field-content input[type=text]");      
            if(option_nos < 2){
                if(opt.length < 2){
                    e_msg.innerHTML = 'Please add atlest 2 options.';
                    flag = true;
                }else{
                    opt.forEach(function (e){
                    if(e.value ==  ""){
                        e.style.borderColor = 'red';
                        
                        }else{
                            num++;
                        } 
                    });
                    if(num < 2){
                        e_msg.innerHTML = 'Please fill in altest 2 fields.'
                        flag = true;
                    } 
                }        
            }else{
                opt.forEach(function (e){
                    if(e.value ==  ""){
                       e.style.borderColor = 'red';
                       
                    }else{
                        num++;
                    } 
                });
                if(num < 2){
                    e_msg.innerHTML = 'Please fill in altest 2 fields.'
                    flag = true;
                }
            }
            
        }
        if(!flag){
            $added_fields = $("#field-content").find('input');
            if($added_fields.length && ($("#question_type").val()!='text' && $("#question_type").val()!='textarea')){
                $added_fields.each(function () {
                    if($(this).val()==''){
                        $(this).closest('div').parent().remove();
                    }
                });
            }
                
            $.easyAjax({
                url: '{{route('admin.questions.update', $question->id)}}',
                container: '#createForm',
                type: "POST",
                redirect: true,
                data: $('#createForm').serialize()
            })
        }
    });

    function on_category_change(select) {
        if(select.value == 'add') {
            $(select).val(0)
            c_name.value = null;
            $('#addCategoryModal').modal('show');
        }
    }

    $('#addCategoryModal form').on('submit', function (e) {
        e.preventDefault();

        $.easyAjax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $('#addCategoryModal form').serialize(),
            success: function(response) {
                if(response.name) {
                    $('[name=category_id] option:last').remove();
                    $('[name=category_id]').append(`<option value="${response.id}">${response.name}</option>`);
                    $('[name=category_id]').append(`<option value="add">Add</option>`);

                    $('[name=category_id]').val(response.id);

                    $('#addCategoryModal').modal('hide');
                }
            }
        });
        return false;
    })
</script>

<script>
var custom_fields = @json($question->custom_fields)

var html_dropdown = `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add Dropdown Value</label>
                            <input type="text" name="dropdown[]" class="form-control">
                        </div>
                    </div>`;
var html_radio =  `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add radio Value</label>
                            <input type="text" name="radio[]" class="form-control">
                        </div>
                    </div>`;
var html_checkbox = `<div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Add Checkbox Value</label>
                            <input type="text" name="checkbox[]" class="form-control">
                        </div>
                    </div>`;
var html_text = function(type, i, val = '', qenabled = false, checked = false) {
    if(qenabled) {
        option_nos++;
    }
    return `
        <div class="col-md-12">
            <div class="form-group">
                <label>Add ${type[0].toUpperCase() + type.slice(1)} Value</label>
                <div class="input-group">
                    <input type="text" name="${type}[]" class="form-control" value="${val}">
                    ${
                        qenabled ? `
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <input style="margin-right: 10px;" id="cb_${i}" type="checkbox" name="qualifiable[]" value="${i}" ${checked ? 'checked' : ''}>
                                    <label style="margin-bottom: 0;" for="cb_${i}">This is qualifying answer?</label>
                                </div>
                            </div>
                        ` : ``
                    }
                </div>
            </div>
        </div>
    `;
}
    var option_nos = 0;
    var html = '';
    $('#field-content').empty();
    custom_fields.map(field => {
        var val_rec = field.question_type;
        var values = JSON.parse(field.question_value);
        var qualify = JSON.parse(field.question_qualifiable || '[]');
        $('#question_type').val(val_rec)
        if(val_rec == "text"){
            $('#field-content').empty();
            $('#addFields').hide();
        }
        if(val_rec == "dropdown"){
            $('#field-content').empty();
            $('#addFields').show();
            html = '';
            if(values){
                values.map((v, i) => html += html_text('dropdown', option_nos, v, field.question_qualifiable, qualify.indexOf(i.toString()) > -1)) 
            }
        }
        if(val_rec == "radio"){
            $('#field-content').empty();
            $('#addFields').show();
            if(values){
                values.map((v, i) => html += html_text('radio', option_nos, v, field.question_qualifiable, qualify.indexOf(i.toString()) > -1)) 
            }
        }
        if(val_rec == "checkbox"){
            $('#field-content').empty();
            $('#addFields').show();
            html_checkbox = $(html_checkbox);
            html = '';
            if(values){
                values.map((v, i) => html += html_text('checkbox', option_nos, v, field.question_qualifiable, qualify.indexOf(i.toString()) > -1))
            }
        }
        $('#field-content').append(html);
    });

    if($('#question_type').val() == 'text'){
        $("#qualification").val('no');
        $("#qualify_on").hide();
    }

    var html;
    $('#question_type, .select-is-qualify').on('change', function() {
        option_nos = 0;
        var val_rec = $('#question_type').val();
        if(val_rec == "text"){
            $('#field-content').empty();
            $('#addFields').hide();
            $('#qualify_on').hide();
        }
        if(val_rec == "dropdown"){
            $('#field-content').empty();
            $('#addFields').show();
            html = html_dropdown;
            $('#qualify_on').show();
        }
        if(val_rec == "radio"){
            $('#field-content').empty();
            $('#addFields').show();
            html = html_radio;
            $('#qualify_on').show();
        }
        if(val_rec == "checkbox"){
            $('#field-content').empty();
            $('#addFields').show();
            html = html_checkbox;
            $('#qualify_on').show();
        }
    });

    $('#addFields').on('click', function() {
        e_msg.innerHTML = '';
        document.querySelectorAll("#field-content input[type=text]").forEach(function (e){
                if(e.value ==  ""){
                    e.style.borderColor = '#ced4da';
                    
                } 
            });
        var val_rec = $('#question_type').val();
        $('#field-content').append(html_text(val_rec, option_nos, '', $('.select-is-qualify').val() == 'yes'));
        option_nos++;
    });

</script>
@endpush