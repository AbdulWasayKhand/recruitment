@extends('layouts.app')

@push('head-script')
    <style>
        .btn-primary:disabled{
            background-color: #4e5d9e !important;
            border-color: #4e5d9e !important;
        }
        .card_2 {
            background: #fff;

        }
        .temp-title {
            outline: none !important;
            border-color: transparent !important;
            font-size: 20px;
            font-weight: bold;
        }
        .modal-body input {
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;;
        }
        .modal-body mark {
            background: #6c757d;
            color: white;
            font-weight: bold;
            border-radius: 2px;
            margin-right: 3px;
            margin-bottom: 3px;
            display: inline-block;
            cursor: pointer;
        }
        .tooltiptext {
            position: fixed;
            color: #fff;
            background-color: #5aa9ff;
            top: 20px;
            width: 20%;
            left: 40%;
            display: flex;
            justify-content: center;
            border-radius: 5px;
            height: 40px;
            align-items: center;
            font-weight: 500;
            transition: all 0.3s ease-out;
            z-index: 9999;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

@endpush
@section('create-button')
    
@endsection

@section('content')

<div class="tooltiptext" style="visibility:hidden" id="tagTooltip">Copy to clipboard!</div>

<div class="modal fade bs-modal-md in" id="emailTemplateDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
    <div class="modal-dialog modal-lg" id="template_view">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('modules.emailTextTemplate.createNewTemplate')</h4>
                <button  type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                <input id="name_email" type="text" placeholder="@lang('modules.emailTextTemplate.nameOfTemplate')" oninput="this.value != '' ? email_save.removeAttribute('disabled') : email_save.setAttribute('disabled', 'disabled')" />
                <input id="subject_email" type="text" placeholder="Subject" />
                <div class="form-group">
                    <textarea class="form-control" id="content_email" name="content_email" rows="15" placeholder="Enter text ..."></textarea>
                </div>
                <div>
                    <h4>Available Tags</h4>
                    <p>Please use following tags in the email content or email subject for injecting the dynamic content.</p>
                    <mark id="tag1" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[applicant_name]</mark>
                    <mark id="tag2" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[company_name]</mark>
                    <mark id="tag3" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[job_title]</mark>
                    <mark id="tag4" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[interview_url]</mark>
                    <mark id="tag5" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[schedule_date]</mark>
                    <mark id="tag6" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[schedule_time]</mark>
                    <mark id="tag7" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[employee_name]</mark>
                    <mark id="tag8" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[qualification]</mark>
                    <mark id="tag9" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applied_note]</mark>
                    <mark id="tag11" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location]</mark>
                    <mark id="tag12" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location_address]</mark>
                    <mark id="tag40" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_session_title]</mark>
                    <mark id="tag44" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applicant_email]</mark>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default btn-sm" data-dismiss="modal">Cancel</button>
                <button type="button" id="email_save" class="btn btn-primary btn-sm"  onclick="saveTemplate('email')" disabled>Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-md in" id="smsTemplateDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
    <div class="modal-dialog modal-lg" id="template_view">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('modules.emailTextTemplate.createNewTemplate')</h4>
                <button  type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input id="name_sms" class="form-control" placeholder="@lang('modules.emailTextTemplate.nameOfTemplate')" type="text" value="" oninput="this.value != '' ? sms_save.removeAttribute('disabled') : sms_save.setAttribute('disabled', 'disabled')" oninput="this.value != '' ? email_save.removeAttribute('disabled') : email_save.setAttribute('disabled', 'disabled')" />
                </div>
                <div class="form-group">
                    <textarea id="content_sms" class="form-control" rows="15" placeholder="Enter text ..."></textarea>
                </div>
                <div>
                    <h4>Available Tags</h4>
                    <p>Please use following tags for injecting the dynamic content.</p>
                    <mark id="tag8" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[applicant_name]</mark>
                    <mark id="tag9" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[company_name]</mark>
                    <mark id="tag10" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_title]</mark>
                    <mark id="tag11" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_url]</mark>
                    <mark id="tag12" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[schedule_date]</mark>
                    <mark id="tag13" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[schedule_time]</mark>
                    <mark id="tag14" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[employee_name]</mark>
                    <mark id="tag15" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[qualification]</mark>
                    <mark id="tag16" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applied_note]</mark>
                    <mark id="tag18" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location]</mark>
                    <mark id="tag19" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location_address]</mark>
                    <mark id="tag40" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_session_title]</mark>
                    <mark id="tag41" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applicant_email]</mark>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default btn-sm " data-dismiss="modal">Cancel</button>
                <button type="button" id="sms_save" class="btn btn-primary btn-sm" onclick="saveTemplate('sms')" disabled>Save</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-5 col-sm-3" style="padding-right:0;">
    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link @if(!Request::has('text_message')) active @endif" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">Emails</a>
        <a class="nav-link @if(Request::has('text_message')) active @endif" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Text Messages</a>
    </div>
    </div>
    <div class="col-7 col-sm-9">
    <div class="tab-content" id="vert-tabs-tabContent">
        <div class="tab-pane text-left fade show @if(!Request::has('text_message')) active @endif" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
        <div class="row"> 
                <div class="col-12" style="padding-left:0;">
                    <div class="card"  >
                        <div style="min-height: 500px; margin:10px;">
                            <h3 style="display: contents;">Email Templates</h3>
                            <span class="float-sm-right">
                                <button class="btn btn-dark btn-sm m-l-15" type="button" data-toggle="modal" data-target="#emailTemplateDetailModal"><i class="fa fa-plus-circle"></i> Create New</button>
                            </span>
                            <div style="padding-top: 10px;"></div>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>@lang('app.name')</th>
                                        <th width='100'>@lang('app.action')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($all_emails as $template)
                                        <tr role="row" class="odd">
                                            <td>{{$template->name}}</td>
                                            <td>
                                                <a href="#" onclick="loadData('{{$template->id}}','{{$template->name}}', '{{$template->subject}}', '{!! urlencode($template->content) !!}', 'email')" data-toggle="modal" data-target="#editEmailTemplateDetailModal" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                                                <a href="javascript:deleteTemp('email',{{$template->id}});" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-row-id="6" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="tab-pane fade @if(Request::has('text_message')) show active @endif" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">
            <div class="row"> 
                <div class="col-12" style="padding-left:0;">
                    <div class="card">
                        <div style="min-height: 500px; margin:10px;">
                            <h3 style="display: contents;">Text Message Templates</h3>
                            <span class="float-sm-right">
                            <button class="btn btn-dark btn-sm m-l-15" type="button" data-toggle="modal" data-target="#smsTemplateDetailModal"><i class="fa fa-plus-circle"></i> Create New</button>
                            </span>
                            <div style="padding-top: 10px;"></div>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>@lang('app.name')</th>
                                        <th width='100'>@lang('app.action')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($all_sms as $template)
                                        <tr role="row">
                                            <td>{{$template->name}}</td>
                                            <td>
                                                <a href="#" onclick="loadData('{{$template->id}}', '{{$template->name}}', null, '{{ urlencode($template->content) }}', 'sms')" data-toggle="modal" data-target="#editSmsTemplateDetailModal" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                                                <a href="javascript:deleteTemp('sms', {{$template->id}});" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-row-id="6" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    </div>
</div>

 

<div class="modal fade bs-modal-md in" id="editEmailTemplateDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
    <div class="modal-dialog modal-lg" id="template_view">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('modules.emailTextTemplate.updateTemplate')</h4>
                <button  type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input id="edit_name_email" class="form-control" type="text" placeholder="Enter template title here" value="" />
                </div>
                <div class="form-group">
                    <input id="edit_subject_email" class="form-control" type="text" placeholder="Subject" />
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="edit_content_email" name="edit_content_email" rows="15" placeholder="Enter text ..."></textarea>
                </div>
                <div>
                    <h4>Available Tags</h4>
                    <p>Please use following tags in the email content or email subject for injecting the dynamic content.</p>

                    <mark id="tag15" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applicant_name]</mark>
                    <mark id="tag16" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[company_name]</mark>
                    <mark id="tag17" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_title]</mark>
                    <mark id="tag18" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_url]</mark>
                    <mark id="tag19" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[schedule_date]</mark>
                    <mark id="tag20" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[schedule_time]</mark>
                    <mark id="tag21" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[employee_name]</mark>
                    <mark id="tag22" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[qualification]</mark>
                    <mark id="tag23" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applied_note]</mark>
                    <mark id="tag25" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location]</mark>
                    <mark id="tag26" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location_address]</mark>
                    <mark id="tag40" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_session_title]</mark>
                    <mark id="tag42" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applicant_email]</mark>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default btn-sm" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-sm"  onclick="editTemplate('email')">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-md in" id="editSmsTemplateDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
    <div class="modal-dialog modal-lg" id="template_view">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('modules.emailTextTemplate.updateTemplate')</h4>
                <button  type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input id="edit_name_sms" class="form-control" type="text" value="Untitled-Text-Message-Template" />
                </div>
                <div class="form-group">
                    <textarea id="edit_content_sms" class="form-control" rows="15" placeholder="Enter text ..."></textarea>
                </div>
                <div>
                    <h4>Available Tags</h4>
                    <p>Please use following tags for injecting the dynamic content.</p>
                    <mark id="tag22" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[applicant_name]</mark>
                    <mark id="tag23" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[company_name]</mark>
                    <mark id="tag24" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[job_title]</mark>
                    <mark id="tag25" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[interview_url]</mark>
                    <mark id="tag26" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[schedule_date]</mark>
                    <mark id="tag27" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[schedule_time]</mark>
                    <mark id="tag28" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()" >[employee_name]</mark>
                    <mark id="tag29" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[qualification]</mark>
                    <mark id="tag30" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applied_note]</mark>
                    <mark id="tag32" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location]</mark>
                    <mark id="tag33" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[job_location_address]</mark>
                    <mark id="tag40" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[interview_session_title]</mark>
                    <mark id="tag43" onclick="handleSelectedTag(this)" onmouseover="tagOver()" onmouseout="tagOut()">[applicant_email]</mark>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default btn-sm " data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-sm" onclick="editTemplate('sms')">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer-script')
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>



    <script>
        var tempId;
        var tempEdit;
        function loadData(id, name, subject, content, type){
            tempId = id;
            document.getElementById('edit_name_'+type).value = name;
            if(document.getElementById('edit_subject_'+type)){
                document.getElementById('edit_subject_'+type).value = subject;
            }
            if(type == 'email' && $("#editEmailTemplateDetailModal").find('iframe').length==0){
                $('#edit_content_email').wysihtml5({
                    "font-styles": true, 
                    "emphasis": true,
                    "lists": true, 
                    "html": true, 
                    "link": true, 
                    "image": true, 
                    "color": true, 
                    stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"],
                });
            }
            if(document.getElementById('edit_content_'+type)){
                content = decodeURI(decodeURIComponent(content))
                while(content.includes("+")){
                    content = content.replace("+", ' ');
                }
                document.getElementById('edit_content_'+type).innerHTML = content;
                $('#edit_content_email').data("wysihtml5").editor.setValue(content);
            }
        }
        function editTemplate(temp){
            var myurl = "{{Route('admin.template.edit-email')}}";
            selected_modal = $("#editEmailTemplateDetailModal");
            if(temp == 'sms'){
                myurl = "{{Route('admin.template.edit-sms')}}"
                selected_modal = $("#editSmsTemplateDetailModal");
            }
            selected_modal.find('button.btn').prop("disabled", true);
            selected_modal.find('button.btn').css({
                "opacity": ".2",
                "cursor": "progress"
            });
            mydata = {
                title: document.getElementById("edit_name_"+temp).value,
                subject: document.getElementById("edit_subject_"+temp) ? document.getElementById("edit_subject_"+temp).value : null,
                content: document.getElementById("edit_content_"+temp).value,
                cid: "{{ $user->company_id }}",
                tid: tempId,
                _token: '{{ csrf_token() }}'
            }
            $.ajax({
                type:"POST",
                url: myurl,
                data: mydata,
                success: function (response) {
                    if(temp == 'sms'){
                        window.location.href = '{{url("admin/template?text_message=1")}}';
                    }
                    else{
                        window.location.href = '{{url("admin/template")}}';
                    }
                }
            });
        }
        function deleteTemp(temp, tid){
            swal({
                title: "Are You Sure?",
                text: "You want to delete this Template",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#f44336",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var myurl = "{{Route('admin.template.delete-email')}}";
                    if(temp == 'sms'){
                        myurl = "{{Route('admin.template.delete-sms')}}"
                    }
                    mydata = {
                        id: tid,
                        _token: '{{ csrf_token() }}'
                    }
                    $.ajax({
                        type:"POST",
                        url: myurl,
                        data: mydata,
                        success: function (response) {
                            if(temp == 'sms'){
                                window.location.href = '{{url("admin/template?text_message=1")}}';
                            }
                            else{
                                window.location.href = '{{url("admin/template")}}';
                            }
                        }
                    });
                }
            });
        
        }
        function saveTemplate(temp){
            var myurl = "{{Route('admin.template.save-email')}}";

            selected_modal = $("#subject_"+temp).closest('.modal-content');
            if(temp == 'sms'){
                myurl = "{{Route('admin.template.save-sms')}}"
                selected_modal = $("#smsTemplateDetailModal");
            }
            selected_modal.find('button.btn').prop("disabled", true);
            selected_modal.find('button.btn').css({
                "opacity": ".2",
                "cursor": "progress"
            });
            mydata = {
                title: document.getElementById("name_"+temp).value,
                subject: document.getElementById("subject_"+temp) ? document.getElementById("subject_"+temp).value : null,
                content: document.getElementById("content_"+temp).value,
                cid: "{{ $user->company_id }}",
                _token: '{{ csrf_token() }}'
            }
            $.ajax({
                type:"POST",
                url: myurl,
                data: mydata,
                success: function (response) {
                    if(temp == 'sms'){
                        window.location.href = '{{url("admin/template?text_message=1")}}';
                    }
                    else{
                        window.location.href = '{{url("admin/template")}}';
                    }
                }
            });

        }
        var email_body = $('#content_email').wysihtml5({
            "font-styles": true, 
            "emphasis": true,
            "lists": true, 
            "html": true, 
            "link": true, 
            "image": true, 
            "color": true, 
            stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"],
        });

        function tagOver() {
            var tooltip = document.getElementById("tagTooltip");
            tooltip.innerHTML = "Copy to clipboard!";
            tooltip.style.visibility = "visible";
        }

        function tagOut() {
            var tooltip = document.getElementById("tagTooltip");
            tooltip.style.visibility = "hidden";
        }

        function handleSelectedTag(el) {
            console.log(el)
            var text = el.textContent;
            var elem = document.createElement("textarea");
            document.body.appendChild(elem);
            elem.value = text;
            elem.select();
            document.execCommand("copy");
            document.body.removeChild(elem);
            var tooltip = document.getElementById("tagTooltip");
            tooltip.innerHTML = "Copied: " + elem.value;
        }
    </script>
@endpush