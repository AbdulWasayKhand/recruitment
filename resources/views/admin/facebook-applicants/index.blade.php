@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
@endpush

@section('content')

    <form class="row" method="POST" action="{{route('admin.facebook.lead.save')}}">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>@lang('app.name')</th>
                                <th>@lang('app.date')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(is_array($leads) && count($leads) > 0)
                                    @foreach($leads as $key => $lead)
                                        <tr>
                                            <td>{{$lead['name']}}</td>
                                            <td>
                                                <a href="javascript:;" class="show_date" data-leadId="{{$lead['id']}}">Show Date</a>
                                            </td>
                                            <td>
                                                <input type="hidden" name="leads[{{intval($key)}}][lead_id]" value="{{$lead['id']}}">
                                                <input type="hidden" name="leads[{{intval($key)}}][name]" value="{{$lead['name']}}">
                                                <div class="form-group">
                                                <label class="">
                                                    <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                        <input type="checkbox" value="1" name="leads[{{intval($key)}}][enabled]" class="flat-red"  style="position: absolute; opacity: 0;" {{isset($lead['enabled']) && $lead['enabled'] ? 'checked' : ''}}>
                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                    </div>
                                                    Enabled
                                                </label></div>
                                                <!-- <a href="{{route('admin.facebook-applicants.show', $lead['id'])}}" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="@lang('app.show')"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3" class="text-center">No Leads Found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mb-4 text-right">
            @csrf
            {{-- <button type="button" class="btn btn-primary btn-lg" id="sync_facebook_data">Sync Facebook Data</button> --}}
            <button type="submit" class="btn btn-success btn-lg">@lang('app.save')</button>
        </div>
    </form>
@endsection

@push('footer-script')
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script>
$('input[type="checkbox"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
});
$('[data-toggle=tooltip]').tooltip();
$('#myTable').dataTable({"order": [[ 1, "asc" ]]})
$('#sync_facebook_data').on('click', function() {
    $.easyAjax({
        type: 'POST',
        url: "{{route('admin.facebook.lead.sync')}}",
        data: $('form').serialize(),
        success: function (response) {
            
        }
    })
});
window.popovers = [];
$('.show_date').on('click', function() {
    var lead_id = $(this).data('leadid');
    var _token = '{{ csrf_token() }}';
    var e=$(this);
    window.popovers.push(e);
    e.off('click');
    $.easyAjax({
        type: 'POST',
        url: "{{route('admin.facebook.lead.show_date')}}",
        data: { lead_id, _token },
        success: function (response) {
            if(response.date)
                e.popover({content: response.date,placement: 'top'})
                .popover('show');
        }
    })
});

$('body').on('click', function() {
    window.popovers.map(e => {
        e.popover('hide');
    });
});

</script>
@endpush
