@extends('layouts.app')

@section('content')
<?php
$data = [];
foreach (\Illuminate\Support\Arr::get($applicants, 'data', []) as $applicant) {
    $_data = [];
    // $_data['id'] = $applicant['id'];
    foreach (\Illuminate\Support\Arr::get($applicant, 'field_data', []) as $field_data) {
        $_data[$field_data['name']] = implode(', ',$field_data['values']);
    }
    $data[] = $_data;
}
?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-2">
                        <a href="{{route('admin.facebook-applicants.index')}}" class="btn btn-primary">
                            <i class="icon-arrow-left"></i>
                            @lang('app.back')
                        </a>
                    </div>
                    @if(count($data) > 0)
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                @foreach(array_keys($data[0]) as $field)
                                <th data-toggle="tooltip" data-title="{{ ucwords(str_replace('_', ' ', $field)) }}">{{ Illuminate\Support\Str::limit(ucwords(str_replace('_', ' ', $field)), 26, '...') }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                <tr>
                                    @foreach(array_keys($data[0]) as $field)
                                    <td>{{str_replace('_', ' ', $value[$field])}}</td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="alert alert-info">
                        No data found for this lead
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@push('footer-script')
<script>
$('[data-toggle=tooltip]').tooltip();
var docTable = $('#myTable').dataTable();
</script>
@endpush