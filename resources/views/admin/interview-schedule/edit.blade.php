<link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
<div class="modal-header">
<h4 class="modal-title"><i class="icon-plus"></i> @lang('modules.interviewSchedule.editInterview')</h4>
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body">
    <form id="updateSchedule" class="ajax-form" method="put">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="reschedule" value="1">
        <div class="form-body">
            <div class="row">
                <div class="col-md-6  col-xs-12">
                    <div class="form-group">
                        <label class="d-block">@lang('modules.interviewSchedule.candidate')</label>
                        <select disabled class="select2 m-b-10 form-control"
                                data-placeholder="@lang('modules.interviewSchedule.chooseCandidate')">
                            @foreach($candidates as $candidate)
                                <option @if($schedule->job_application_id == $candidate->id) selected @endif value="{{ $candidate->id }}">{{ ucwords($candidate->full_name) }} {{$candidate->phone}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="candidate_id" value="{{$schedule->job_application_id}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label class="d-block">@lang('modules.interviewSchedule.employee')</label>
                        <select class="select2 m-b-10 form-control select2-multiple " multiple="multiple"
                                data-placeholder="@lang('modules.interviewSchedule.chooseEmployee')" name="employee[]" id="employee" disabled>
                            @foreach($users as $emp)
                                <option  value="{{ $emp->id }}">{{ ucwords($emp->name) }} @if($emp->id == $user->id)
                                        (@lang('app.you')) @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 col-md-4 ">
                    <div class="form-group">
                        <label>@lang('modules.interviewSchedule.scheduleDate')</label>
                        <input type="text" name="scheduleDate" id="scheduleDate" value="{{$schedule->schedule_date->timezone($schedule->jobApplication->company->timezone)->format('m-d-Y')}}" class="form-control">
                    </div>
                </div>

                <!-- <div class="col-xs-5 col-md-4">
                    <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                        <label>@lang('modules.interviewSchedule.scheduleTime')</label>
                        <input type="text" name="scheduleTime" id="scheduleTime" value="{{$schedule->schedule_date->format('H:i')}}" class="form-control">
                    </div>
                </div> -->

                <div class="col-xs-5 col-md-6">
                    <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                        <label>@lang('modules.interviewSchedule.scheduleTime')</label>
                        <select class="form-control" id="scheduleTimeSlot" name="slot_timing">
                            <option value="{{$schedule->slot_timing}}">{{$schedule->slot_timing}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <?php 
                $show_location = true;
                $locations = explode(',', str_replace('"', '',  str_replace(']', '', str_replace('[', '', $schedule->jobApplication->job->location_id)))); 
                if($schedule->jobApplication->job->workflow_id != 0){
                    $show_location = \App\WorkflowStageActionInterview::where('stage_id', '=', $schedule->jobApplication->workflow_stage_id)->first()->show_location;
                }
            ?>
            <div class="row">
                <div class="col-xs-11 col-md-10">
                @if(sizeOf($locations) > 1 && $show_location)
                    <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                        <label>@lang('modules.interviewSchedule.jobLocation')</label>
                        <select class="form-control" id="scheduleLocationId" name="location_id" >
                            @foreach(\App\JobLocation::whereIn('id', $locations )->get() as $location)
                                <option value="{{$location->id}}" @if($schedule->location_id == $location->id) selected @endif>{{$location->location_name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group">
                        <label>@lang('modules.interviewSchedule.applicantComments')</label><br>
                        {{ $comment->comment?? '' }}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-success update-schedule">@lang('app.update')</button>
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
</div>

<script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>

<script>
    // Select 2 Init
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('#employee').val({{$employeeList}}).change();

    // Datepicker Set
    $('#scheduleDate').bootstrapMaterialDatePicker
    ({
        time: false,
        format: 'MM-DD-YYYY',
        minDate : new Date()
    });
    $('#scheduleDate').on('change',function(){
        getSlots();
    });

    // Timepicker Set
    $('#scheduleTime').bootstrapMaterialDatePicker
    ({
        date: false,
        shortTime: true,   // look it
        format: 'HH:mm',
        switchOnClick: true
    });

    // Update Schedule
    $('.update-schedule').click(function () {
        employee.removeAttribute('disabled');
        if(typeof scheduleLocationId !== 'undefined'){
            scheduleLocationId.style.borderColor = 'black';
            if(scheduleLocationId.value == 0){
                scheduleLocationId.style.borderColor = 'red';
            return;
            }
        }
        var data = $('#updateSchedule').serializeArray();
        var schedule_start = $("#scheduleDate").val().split('-');
        data.forEach(function(item) {
            if(item.name == 'scheduleDate'){
                item.value = schedule_start[2]+'-'+schedule_start[0]+'-'+schedule_start[1];
            }
        });
        $.easyAjax({
            url: '{{route('admin.interview-schedule.update', $schedule->id)}}',
            container: '#updateSchedule',
            type: "PUT",
            data: data,
            success: function (response) {
                $("select[name='employee[]']").prop('disabled',true);
                if(response.status == 'success'){
                    window.location.reload();
                }
            },
            error: function () {
                $("select[name='employee[]']").prop('disabled',true);
            }
        })
    })

    $('[name="employee[]"], [name="scheduleDate"], [name="candidates[]"]',).on('change', function (e) {
        e.preventDefault();
        getSlots();
    });

    getSlots();
    function getSlots(){
        let schedule_date = $('#scheduleDate').val().split('-');
        schedule_date = schedule_date[2]+'-'+schedule_date[0]+'-'+schedule_date[1];
        var data = { date: schedule_date, employees: [$('[name="employee[]"]').val()], candidates: ['{{$schedule->job_application_id}}'], _token: '{{ csrf_token() }}', update: 1 };
        
        var currentTiming = '{{$schedule->slot_timing}}';
        $.easyAjax({
            url: '{{route('admin.interview-schedule.get-timing')}}',
            type: "POST",
            data: data,
            success: function (response) {
                $('#scheduleTimeSlot').html(``);
                if(response.data) {
                    response.data.forEach(timings => {
                        $('#scheduleTimeSlot').append(`<option value="${timings}" ${currentTiming == timings ? 'selected' : ''}>${timings}</option>`);
                    });
                }
            }
        })
    }    
</script>
