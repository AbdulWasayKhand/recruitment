<style>
    .scheduleul li {
        /* border-bottom: 1px solid lightgray; */
    }
</style>
@php
$company = App\Company::select('id', 'timezone')->find(Auth::user()->company_id);
@endphp
@forelse($upComingSchedules as $key => $upComingSchedule)
    <div>
        @php
            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $key);
        @endphp
        

        <ul class="scheduleul" id="ul_{{$key}}">
            @forelse($upComingSchedule as $key => $dtData)
                @if($key == 0)
                    <li style="list-style: none;"><h4>{{ $date->format('M d, Y') }}</h4><hr></li>
                @endif
                @if(in_array($user->id , $dtData->employee->pluck('user_id')->toArray()))
                <li class="deco" id="schedule-{{$dtData->id}}" onclick="getScheduleDetail(event, {{$dtData->id}}) "
                    style="list-style: none;">
                    <h5 class="text-muted"
                        style="float: left">{{ ucfirst($dtData->jobApplication->job->title) }} </h5>
                    <div class="pull-right">
                        @if(in_array($user->id , $dtData->employee->pluck('user_id')->toArray()))
                            <span style="">
                                <button onclick="editUpcomingSchedule(event, '{{ $dtData->id }}')"
                                        class="btn btn-sm btn-info notify-button editSchedule"
                                        title="Edit"> <i class="fa fa-pencil"></i></button>
                            </span>
                        @endif

                    </div>
                    <div class="clearfix"></div>
                    <div class="direct-chat-name"
                            style="font-size: 13px">{{ ucfirst($dtData->jobApplication->full_name) }}</div>
                    <span class="direct-chat-timestamp"
                            style="font-size: 13px">{{  explode(" - ", $dtData->slot_timing)[0] }}</span>

                    @if(in_array($user->id, $dtData->employee->pluck('user_id')->toArray()))
                        @php
                            $empData = $dtData->employeeData($user->id);
                        @endphp

                        @if($empData->user_accept_status == 'accept')
                            <label class="badge badge-success float-right">@lang('app.accepted')</label>
                        @elseif($empData->user_accept_status == 'refuse')
                            <label class="badge badge-danger float-right">@lang('app.refused')</label>
                        @else
                            <span class="float-right">
                                <button onclick="employeeResponse({{$empData->id}}, 'accept')"
                                        class="btn btn-sm btn-success notify-button responseButton">@lang('app.accept')</button>
                                {{--<button onclick="employeeResponse({{$empData->id}}, 'refuse')"
                                        class="btn btn-sm btn-danger notify-button responseButton">@lang('app.refuse')</button>--}}
                            </span>
                        @endif
                    @endif
                    <hr>
                </li>
                @endif
            @empty

            @endforelse
        </ul>

    </div>
@empty
    <div>
        <p>@lang('messages.noUpcomingScheduleFund')</p>
    </div>
@endforelse

<script>
    document.querySelectorAll('ul.scheduleul').forEach(function(e){
        if(e.children.length == 1){
            e.remove();
        }
    });
</script>