<style>
    .notify-button-show{
        /*width: 9em;*/
        height: 1.5em;
        font-size: 0.730rem !important;
        line-height: 0.5 !important;
    }
    .select2-dropdown {z-index: 1111;}
    .select2-container--default .select2-selection--multiple .select2-selection__choice {background-color: #febd13 !important;border-color: #febd13 !important;color: #000;}
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{color: rgb(0 0 0);}

</style>
<link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">

<div class="modal-header">
    <h4 class="modal-title">@lang('modules.interviewSchedule.interviewDetails')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="portlet-body">
            <div class="row font-12">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-5">
                            <h4>@lang('modules.interviewSchedule.scheduleEditDetail')</h4>
                        </div>
                    </div>
                    <?php
                        $show_location = true;
                        if($schedule->jobApplication->job->workflow_id != 0){
                            $show_location = \App\WorkflowStageActionInterview::where('stage_id', '=', $schedule->jobApplication->workflow_stage_id)->first()->show_location;
                        }
                    ?>
                    <div class="col-sm-12">
                        <b><strong>@lang('modules.interviewSchedule.job')</strong></b><br>
                        <p class="text-muted">{{ ucwords($schedule->jobApplication->job->title) }}</p>
                        @if($schedule->location_id != 0 && $show_location)
                        <b><strong>Location</strong></b>
                        <p>
                            {{ \App\JobLocation::find($schedule->location_id)->location_name }}
                            <br />
                            <i>{{ \App\JobLocation::find($schedule->location_id)->location }}</i>
                        </p>
                        @endif
                        <b><strong>Date</strong></b>
                        <p>{{ $schedule->schedule_date->setTimezone(company()->timezone)->format('M d, Y') }} ({{  $schedule->slot_timing }}) </p>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"><hr></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <strong>@lang('modules.interviewSchedule.assignedEmployee')</strong><br>
                        </div>
                        <div class="col-sm-6">
                            <strong>@lang('modules.interviewSchedule.employeeResponse')</strong><br>
                        </div>
                       @forelse($schedule->employee as $key => $emp )
                        <div class="col-sm-6">
                            <p class="text-muted">{{ $emp->user_accept_status == 'waiting' ? implode(', ', \App\User::find($schedule->employee->pluck('user_id'))->pluck('name')->toArray()) : ucwords($emp->user->name) }}</p>
                        </div>

                        <div class="col-sm-6">
                            @if($emp->user_accept_status == 'accept')
                                <label class="badge badge-success">{{ ucwords($emp->user_accept_status) }}ed</label>
                            @elseif($emp->user_accept_status == 'refuse')
                                <label class="badge badge-danger">{{ ucwords($emp->user_accept_status) }}</label>
                            @else
                                @if($scheduleEmpId = hasToAccept($emp->interview_schedule_id,$user->id))
                                    <button onclick="employeeResponse({{$scheduleEmpId}}, 'accept')"
                                        class="btn btn-sm btn-success notify-button responseButton">@lang('app.accept')</button>
                                @else 
                                    <label class="badge badge-warning">{{ ucwords($emp->user_accept_status) }}</label>
                                @endif
                            @endif
                        </div>
                        @break
                        @empty
                            <div class="col-sm-6 pt-2 pb-2 text-muted">
                                <strong>@lang('modules.interviewSchedule.noEmployeeAssigned')</strong><br>
                            </div>
                        @endforelse
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>@lang('modules.interviewSchedule.applicantDetail')</h4>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <strong>@lang('app.name')</strong><br>
                        <p class="text-muted">
                            <a href="{{ route('admin.job-applications.table') . '?applicant=' . $schedule->jobApplication->id }}" target="_blank">{{ ucwords($schedule->jobApplication->full_name) }}</a>
                        </p>
                    </div>


                    <div class="col-sm-12">
                        <strong>@lang('app.email')</strong><br>
                        <p class="text-muted">{{ $schedule->jobApplication->email }}</p>
                    </div>

                    <div class="col-sm-12">
                        <strong>@lang('app.phone')</strong><br>
                        <p class="text-muted">{{ $schedule->jobApplication->phone }}</p>
                    </div>

                    <div class="col-sm-12">
                        <p class="text-muted">
                        @if ($schedule->jobApplication->resume_url)
                            <a target="_blank" href="{{ $schedule->jobApplication->resume_url }}" class="btn btn-sm btn-primary">@lang('app.view') @lang('modules.jobApplication.resume')</a>
                        @endif
                        </p>
                    </div>
                    <div class="col-md-12">
                        @if($schedule->jobApplication->schedule->comments->count())
                        <h5>@lang('modules.interviewSchedule.applicantComments')</h5>
                            @forelse($schedule->jobApplication->schedule->comments as $key => $comment )
                                <!-- <div class="col-sm-12"> -->
                                    <p class="text-muted">{{ $comment->comment }}</p>
                                <!-- </div> -->
                            @empty
                            @endforelse
                        @endif
                    </div>
                    <div class="col-md-12">
                        @foreach($questionnaires as $questionnaire)
                            @if(isset($questionnaire))
                                @if($schedule->employee->count() && $emp->user->id == $user->id && $schedule->user_accept_status == 'accept')
                                <a href="javascript:addEditQuestionnaire({{$questionnaire->id}})" class="btn btn-sm btn-info p-hide btn-block line-break" title="{{$questionnaire->name}}">
                                    {{$questionnaire->name}}
                                </a>
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
                @if($schedule->employee->count() && $emp->user->id == $user->id)
                <div class="col-6">
                    <div class="col-sm-12">
                        <div class="form-group">
                            @if($schedule->jobApplication->job->workflow_id != 0  && $schedule->user_accept_status == 'accept')
                            <label for="pass">
                                <div class="iradio_minimal-blue" aria-checked=""
                                    aria-disabled="false" style="position: relative;font-size: .7rem">
                                    <input id="pass" type="radio" @if($schedule->status == 'pass') checked @endif name="r1"
                                    class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                        style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                Pass
                            </label>
                            <label for="fail">
                                <div class="iradio_minimal-blue" aria-checked=""
                                    aria-disabled="false" style="position: relative;margin-left: 10px;font-size: .7rem">
                                    <input id="fail" type="radio" @if($schedule->status == 'fail') checked @endif name="r1"
                                    class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                        style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0; "></ins>
                                </div>
                                Fail
                            </label>
                            @endif
                            @if($schedule->user_accept_status == 'accept')
                            <label for="pending">
                                <div class="iradio_minimal-blue" aria-checked=""
                                    aria-disabled="false" style="position: relative;margin-left: 10px;font-size: .7rem">
                                    <input id="pending" type="radio" @if($schedule->status == 'pending') checked @endif name="r1"
                                    class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                        style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                @lang('app.pending')
                            </label>
                            <label for="canceled">
                                <div class="iradio_minimal-blue" aria-checked=""
                                    aria-disabled="false" style="position: relative;margin-left: 10px;font-size: .7rem">
                                    <input id="canceled" type="radio" @if($schedule->status == 'canceled') checked @endif name="r1"
                                    class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                        style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0; "></ins>
                                </div>
                                @lang('app.canceled')
                            </label>
                            @endif
                            @if($schedule->jobApplication->job->workflow_id == 0 && $schedule->user_accept_status == 'accept')
                            <label for="rejected">
                                <div class="iradio_minimal-blue" aria-checked=""
                                        aria-disabled="false" style="position: relative;font-size: .7rem">
                                    <input id="rejected" type="radio" @if($schedule->status == 'rejected') checked @endif name="r1"
                                        class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                            style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div>
                                Rejected
                            </label>
                            <label for="hired">
                                <div class="iradio_minimal-blue" aria-checked=""
                                        aria-disabled="false" style="position: relative;margin-left: 10px;font-size: .7rem">
                                    <input id="hired" type="radio" @if($schedule->status == 'rejected') checked @endif name="r1"
                                        class="minimal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"
                                            style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0; "></ins>
                                </div>
                                Hired
                            </label>
                            @endif
                    </div>
                </div>
            </div>
            @endif
    </div>
</div>
<div class="modal-footer">

    @if(!isset($_REQUEST['profile']) && $schedule->employee->count() && $emp->user->id == $user->id && $schedule->user_accept_status == 'accept')
        <button onclick="editSchedule()" class="btn btn-sm btn-info" title="Edit" > <i class="fa fa-pencil"></i> @lang('app.change') @lang('menu.interviewDate') @lang('menu.and') @lang('menu.time') </button>
        <a target="_blank" href="{{ route('admin.job-applications.table') . '?applicant=' . $schedule->jobApplication->id }}" class="btn btn-sm btn-primary" @if(Str::contains(url()->previous(), 'job-applications')) data-dismiss="modal" @endif>@lang('app.view') @lang('menu.applicant')</a>
    @endif
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
</div>
<script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>


<script>
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    })

    $('input[type="radio"].minimal').on('ifChecked', function(e) {
        statusChange($(this).prop('id'));
    })

    function addEditQuestionnaire(questionnaire_id) {
        if(questionnaire_id){
            var url = "{{ url('admin/questionnaire/get_answers') }}/"+questionnaire_id+'?applicant_id={{$schedule->jobApplication->id}}';
            $.ajaxModal('#questionnaireModal', url);
        }
    }

    // Employee Response on schedule
    function statusChange(status) {
        var msg;
        if(status == 'pass' || status == 'fail' || status == 'pending'){
            statusChangeConfirm(status , 'no');
            return;
        }
        swal({
            title: "@lang('errors.askForCandidateEmail2')",
            text: "@lang('errors.settingMessageWillBeSent')",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#0c19dd",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true,
            closeOnEsc: false,
            closeOnClickOutside: true
        }, function (isConfirm) {
            if (isConfirm) {
                statusChangeConfirm(status , 'yes')
            }
            else{
                statusChangeConfirm(status , 'no')
            }

        });

        if($("#close_btn").length){
            $("#close_btn").remove();
        }
        $($(".showSweetAlert").children()[2]).before('<a href="javascript:void(0)" class="close" id="close_btn" onClick="swal.close()">×</a>');
    }

    // change Schedule schedule
    function statusChangeConfirm(status, mailToCandidate) {
        var token = "{{ csrf_token() }}";
        var id = "{{$schedule->id}}";
        $.easyAjax({
            url: "{{route('admin.interview-schedule.change-status')}}",
            container: '.modal-body',
            type: "POST",
            data: {'_token': token,'status': status,'id': id,'mailToCandidate': mailToCandidate},
            success: function (response) {
                @if($tableData)
                    table._fnDraw();
                @else
                    @if(Str::contains(url()->previous(), 'job-applications'))
                    location.href = "{{ route('admin.job-applications.table') . '?applicant=' . $schedule->jobApplication->id }}";
                    @endif
                    reloadSchedule();
                @endif
                $('#scheduleDetailModal').modal('hide');
                location.reload();
            }
        })
    }
    function editSchedule() {
        var url = "{{ route('admin.interview-schedule.edit', $schedule->id) }}";
        $('#modelHeading').html('Schedule');
        $('#scheduleEditModal').modal('hide');
        $.ajaxModal('#scheduleEditModal', url);
    }
    $(".select2").select2();
</script>

