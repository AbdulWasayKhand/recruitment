@php
$prev_date = '';
@endphp
@foreach(upComingScheduleRequests() as $upComing)
    <div>
        <ul class="requestul" style="padding:0">
            @if(in_array($user->id , $upComing->employee->pluck('user_id')->toArray()))
                @if($prev_date != $upComing->schedule_date->format('Y-m-d'))
                    <li style="list-style: none;"><h4>{{ $upComing->schedule_date->format('M d, Y') }}</h4><hr></li>
                @endif
                <li class="deco" id="schedule-{{$upComing->id}}" onclick="getScheduleDetail(event, {{$upComing->id}}) "
                    style="list-style: none;">
                    <h5 class="text-muted"
                        style="float: left">{{ ucfirst($upComing->jobApplication->job->title) }} </h5>
                    <div class="pull-right">
                        @if(in_array($user->id , $upComing->employee->pluck('user_id')->toArray()))
                            {{--<span style="">
                                <button onclick="editUpcomingSchedule(event, '{{ $upComing->id }}')"
                                        class="btn btn-sm btn-info notify-button editSchedule"
                                        title="Edit"> <i class="fa fa-pencil"></i></button>
                            </span>--}}
                        @endif

                    </div>
                    <div class="clearfix"></div>
                    <div class="direct-chat-name"
                            style="font-size: 13px">{{ ucfirst($upComing->jobApplication->full_name) }}</div>
                    <span class="direct-chat-timestamp"
                            style="font-size: 13px">{{  explode(" - ", $upComing->slot_timing)[0] }}</span>

                    @if(in_array($user->id, $upComing->employee->pluck('user_id')->toArray()))
                        @php
                            $empData = $upComing->employeeData($user->id);
                        @endphp
                        @if($empData->user_accept_status == 'waiting' || empty($empData->user_accept_status))
                            <span class="float-right">
                                <button onclick="employeeResponse({{$empData->id}}, 'accept')"
                                        class="btn btn-sm btn-success notify-button responseButton">@lang('app.accept')</button>
                            </span>
                        @endif
                    @endif
                    <hr>
                </li>
                @endif
        </ul>
    </div>
    @php($prev_date = $upComing->schedule_date->format('Y-m-d'))
@endforeach