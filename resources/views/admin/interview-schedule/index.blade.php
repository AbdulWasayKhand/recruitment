@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/plugins/calendar/dist/fullcalendar.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">

    <link rel="stylesheet"
          href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

    <style>
        .cancel {
            background-color: #f44336 !important;
        }
        .mb-20 {
            margin-bottom: 20px
        }

        .datepicker {
            z-index: 9999 !important;
        }

        .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
            width: 100%;
        }

        .select2-search {
            width: 100%;
        }

        .select2.select2-container {
            width: 100% !important;
        }
        /* ->message_body
            padding: .375rem .75rem !important;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        } */

        .d-block {
            display: block;
        }

        .upcomingdata {
            height: 37.5rem;
            overflow-x: scroll;
        }

        .scheduleul {
            padding: 0 15px 0 11px;
        }
        #message_body , #sms_body, #personal_message{width:100%;height: 200px;border-color: lightgray;outline: none;}

        .upcomming-scroll {
            max-height : 258px;
            overflow : scroll;    
        }
        .sheld2{
            z-index:4444;
            position: fixed;
            width: 100%;
            height: 100%;
            color: white;
            font-size: 30px;
            text-align: center;
            padding-top: 20%;
            top: 0;
            left:0;
            display: none;
        }
        .sheld2{
            /* display:none; */
            background-color: rgba(0,0,0,.3);
        }
        @media screen and (max-width:767px){
            .sheld2 > div > img {
                margin-top: 70%!important;
            }
            
        }
    </style>
@endpush

@section('content')
        <div class="sheld2" id="sheld2">
            <div id="fliper">
                <img style="width:50px;" src="{{ url('/assets/')}}/loading.gif" />
                <br />
                <small><i>Please wait....</i></small>
            </div> 
        </div>
    <div class="row">
        <div class="col-md-12 mb-2">
                

            <a href="#googleEvent" data-toggle="modal" hidden id="g_event"> 
                google
            </a>
                @if(in_array("view_schedule", $userPermissions))
                {{-- <a href="{{ route('admin.interview-schedule.table-view') }}"
                   class="btn btn-sm btn-primary">@lang('app.tableView') <i class="fa fa-table"></i></a> --}}
                @endif
                
                {{-- <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-info waves-effect waves-light">
                    @lang('modules.jobApplication.sendInteviewInvite')
                </a> --}}
                @if(in_array("add_schedule", $userPermissions))
                <a href="#" data-toggle="modal" onclick="createSchedule(`{{$schedule_date}}`,1)"
                   class="btn btn-sm btn-success waves-effect waves-light" style="padding:8px;">
                    <i class="ti-plus"></i> @lang('modules.interviewSchedule.ScheduleInterview')
                </a>
                @endif
                {{--<a href="javascript:openInModal('{{route('admin.custom-event.create')}}')" class="btn btn-danger btn-sm">
                    Create an Event
                </a>--}}
                @if($optionEmp == '')
                    <a href="{{ url('oauth2callback') }}" class="btn btn-sm pull-right p-0">
                        <img src="{{ asset('assets/images/link-with-google.png') }}">
                    </a>
                @else 
                    <a class="btn btn-primary btn-sm pull-right" style="color:white; padding:8px;" onclick="unlinkGC()">
                        Unlink with Google Calendar
                    </a>
                    <a onclick="syncGC()" @if($_SERVER['SERVER_NAME'] == 'localhost') style="color:white;" @else  style="color:white;display:none;"  @endif class="btn btn-success btn-sm">
                        Sync Google Calander Now
                    </a>
                @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div id="calendar"></div>
                </div><!-- /.card-body -->
            </div>

        </div>
        <div class="col-md-4">
            @if(upComingScheduleRequests()->count() && $is_user_accepter)
            <div class="card">
                <div class="card-header d-flex p-0 ui-sortable-handle">
                    <h3 class="card-title p-3">
                        <i class="fa fa-file"></i> @lang('modules.interviewSchedule.ScheduleRequests')
                    </h3>
                </div><!-- /.card-header -->
                <div id="upcoming-schedule-requests" class="card-body upcomming-scroll">
                    @include('admin.interview-schedule.upcoming-schedule-requests', ['timezone' => $timezone])
                </div><!-- /.card-body -->
            </div>
            @endif
            <div class="card">
                <div class="card-header d-flex p-0 ui-sortable-handle">
                    <h3 class="card-title p-3">
                        <i class="fa fa-file"></i> @lang('modules.interviewSchedule.interviewsRequest')
                    </h3>
                </div><!-- /.card-header -->
                <div id="upcoming-schedules" class="card-body upcomming-scroll">
                    @include('admin.interview-schedule.upcoming-schedule', ['upComingSchedules' => $upComingSchedules, 'timezone' => $timezone])
                </div><!-- /.card-body -->
            </div>
            <div class="card">
                <div class="card-header d-flex p-0 ui-sortable-handle">
                    <h3 class="card-title p-3">
                        <i class="fa fa-file"></i> @lang('modules.interviewSchedule.upcomingHolidays')
                    </h3>
                </div><!-- /.card-header -->
                <div id="upcoming-holidays" class="card-body upcomming-scroll">
                    @include('admin.interview-schedule.upcoming-holidays', ['timezone' => $timezone])
                </div><!-- /.card-body -->
            </div>
        </div>
    </div>

    {{--Ajax Modal Start for--}}
    <div class="modal fade bs-modal-md in" id="scheduleDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="row">
                    <div class="col-md-3 pl-4 pb-2">
                        <button type="button" class="btn default pull-right" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-md-9 pr-4 pb-2">
                        <button type="button" class="btn blue">Save changes</button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal Start for--}}
    <div class="modal fade bs-modal-md in" id="scheduleEditModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    <div class="modal fade bs-modal-md in" id="questionnaireModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">@lang('menu.questionnaire')</span>
               </div>
               <div class="modal-body">
                   Loading...
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn default" data-dismiss="modal">Close</button>
                   <button type="button" class="btn blue">Save changes</button>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="googleEvent" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
   <div class="modal-dialog" id="modal-data-application">
       <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-google"></i> Google Event</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
           <div class="modal-body">
               <h4 id="g_summary">Title</h4>
               <p>
                   <label>Date :</label> <span id="g_date">NULL</span><br/>
                   <label>From :</label> <span id="g_from">Null</span><br/>
                    <label>To : </label> <span id="g_to">Null</span>

               </p>
           </div>
           <div class="modal-footer">
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
           </div>
       </div>
       <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
   
<div class="modal fade bs-modal-md in" id="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">@lang('modules.jobApplication.sendInteviewInvite')</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label>Applicants</label>
            <select class="select2 m-b-10 form-control" name="applicant_id">
                @foreach($applicants as $applicant)
                <option value="{{$applicant->id}}">{{$applicant->full_name}}</option>
                @endforeach
            </select>
            <hr />
            <label>Email Message</label>
            <textarea class="wysihtml5"  id="message_body" placeholder="Email  Message"></textarea>
            <hr />
            <label>Text Message</label>
            <textarea id="sms_body" placeholder="Text Message" ></textarea>
        </div>
        <div class="modal-footer">
                <a href="javascript:beforeSend()" class="btn btn-success">
                    @lang('modules.jobApplication.sendInteviewInvite')
                </a>
                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal" id="close_message_modal">Close</button>
                <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
        </div>
        </div>
    </div>
</div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/calendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/calendar/dist/jquery.fullcalendar.js') }}"></script>
    <script src="{{ asset('assets/plugins/calendar/dist/locale-all.js') }}"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>

    <script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        },
        width: '100%'
    });
    $('.wysihtml5').wysihtml5();
    function unlinkGC(){
        // href="{{ url('unlinkGC') }}"

        swal({
                title: "@lang('errors.areYouSure')",
                text: "If you unlink your account with Google Calendar it may cause a malfunction on past events",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Unlink Anyways",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    location.href = "{{ url('unlinkGC') }}";
                }
            });
    }      
        var companyTimeZone = '{{ $timezone }}';
        var todayTime = '{{ $today }}';
        userCanAdd = false;
        userCanEdit = false;
        @if($user->cans('add_schedule'))
            userCanAdd = true;
        @endif
        @if($user->cans('edit_schedule'))
            userCanEdit = true;
        @endif
        taskEvents = [
            @foreach($schedules as $schedule)
            @if($schedule->jobApplication->status->status!='hired' && $schedule->jobApplication->status->status!='rejected' && $schedule->jobApplication->id)
            {
                id: '{{ ucfirst($schedule->id) }}',
                // title: '{{ $schedule->jobApplication->job->title }} on {{ $schedule->jobApplication->full_name }}',
                title: 'Interview {{ $schedule->jobApplication->full_name }}',
                start: '{{ $schedule->schedule_date->timezone($timezone)->format("c") }}',
                end: '{{ $schedule->schedule_date->timezone($timezone)->format("c") }}',
                is_passed: '{{ strtotime($schedule->schedule_date->timezone($timezone)->toString()) >= strtotime(now()->timezone($timezone)->toString()) }}'
            },
            @endif
            @endforeach

            @foreach($googleSchedules as $schedule)
            @if($schedule['id'] == 0)
            {
                id: "<?= $schedule['id'] ?>",
                title: "<?= $schedule['title'] ?>",
                start: "<?= $schedule['start'] ?>",
                end: "<?= $schedule['end'] ?>",
            },
            @endif
            @endforeach
            {{--@foreach($custom_events as $event)--}}
            // {
            //     id: "{{-- $event->id --}}",
            //     title: '{{-- $event->title --}}',
            //     start: '{{--$event->date--}}',
            //     end: '{{--$event->date--}}',
            //     flag: 'custom',
            //     complete_day: '{{--$event->block_complete_day--}}',
            //     action: '{{--url("admin/custom-event/") . "/" . $event->id--}}',
            // },
            {{--@endforeach--}}
            @foreach($holidays as $holiday)
            {
                id: "{{ $holiday->id }}",
                title: '{{ $holiday->title }}',
                start: "{{$holiday->from}}T00:00:00",
                end: "{{$holiday->to}}T24:00:00",
                flag: 'holiday',
                action: '{{url("admin/holiday/show/".$holiday->id)}}',
            },
            @endforeach
        ];
        var scheduleLocale = '{{ $global->locale }}';
    </script>
    <script src="{{ asset('js/schedule-calendar.js') }}"></script>

    <script>
        // Schedule create modal view

        @if($user->cans('edit_schedule'))
        // Schedule create modal view
        function editUpcomingSchedule(event, id) {
            if (!$(event.target).closest('.editSchedule').length) {
                return false;
            }
            var url = "{{ route('admin.interview-schedule.edit',':id') }}";
            url = url.replace(':id', id);
            $('#modelHeading').html('Schedule');
            $('#scheduleEditModal').modal('hide');
            $.ajaxModal('#scheduleEditModal', url);
        }
        @endif


        // Update Schedule
        function reloadSchedule() {
            $.easyAjax({
                url: '{{route('admin.interview-schedule.index')}}',
                container: '#updateSchedule',
                type: "GET",
                success: function (response) {
                    $('#upcoming-schedules').html(response.data);
                    taskEvents = [];
                    console.log(response)
                    response.scheduleData.forEach(function(schedule){
                        const taskEvent = {
                            id: schedule.id,
                            title: schedule.job_application.job.title +' on '+  schedule.job_application.full_name ,
                            start: schedule.schedule_date ,
                            end: schedule.schedule_date,
                        };
                        taskEvents.push(taskEvent);
                    });
                }
            })
        }
        @if($user->cans('delete_schedule'))
        $('body').on('click', '.deleteSchedule', function (event) {
            var id = $(this).data('schedule-id');
            if (!$(event.target).closest('.deleteSchedule').length) {
                return false;
            }
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.interview-schedule.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#schedule-'+id).remove();
                                // Schedule create modal view
                                reloadSchedule();
                            }
                        }
                    });
                }
            });
        });
        @endif
        // Employee Response on schedule
        function employeeResponse(id, type) {
            var msg;
            // $('.cancel').css('background', '#ff5722');
            if (type == 'accept') {
                msg = "@lang('errors.acceptSchedule')";
            } else {
                msg = "@lang('errors.refuseSchedule')";
            }
            swal({
                title: "@lang('errors.areYouSure')",
                text: msg,
                type: "warning",
                showCancelButton: true,
                cancelButtonColor: "red",
                confirmButtonColor: "#4caf50",
                confirmButtonText: "@lang('app.yes')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var url = "{{ route('admin.interview-schedule.response',[':id',':type']) }}";
                    url = url.replace(':id', id);
                    url = url.replace(':type', type);

                    // update values for all tasks
                    $.easyAjax({
                        url: url,
                        type: 'GET',
                        success: function (response) {
                            if (response.status == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        }

        // schedule detail
        var months = {1:"Jan ", 2:"Feb ",3:"Mar ",4:"Apr ",5:"May ",6:"Jun ",7:"Jul ",8:"Aug ",9:"Sep ",10:"Oct ",11:"Nov ",12:"Dec "};
        var getScheduleDetail = function (event, id) {
            if(id.indexOf("google__") == 0){
                taskEvents.forEach(function (e){
                    if(e.id == id){
                        g_summary.innerHTML = e.title;
                        var std = e.start.split(' ');
                        var etd = e.end.split(' ');
                        var isSameDate = e.start.split(" ")[0] != e.end.split(" ")[0];
                        g_date.innerHTML = months[parseInt(std[0].split("-")[1])] + std[0].split("-")[2] + ", " + std[0].split("-")[0];
                        g_from.innerHTML = (isSameDate ? months[parseInt(std[0].split("-")[1])] + std[0].split("-")[2] + ", " + std[0].split("-")[0] + ", " : "") + new Date(e.start).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                        g_to.innerHTML = (isSameDate ? months[parseInt(etd[0].split("-")[1])] + etd[0].split("-")[2] + ", " + etd[0].split("-")[0] + ", " : "") + new Date(e.end).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                        g_event.click();
                    }
                })
                return;
            }

            if ($(event.target).closest('.editSchedule, .deleteSchedule, .responseButton').length) {
                return false;
            }

            var url = '{{ route('admin.interview-schedule.show', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('Schedule');
            $.ajaxModal('#scheduleDetailModal', url);
        }
        @if($user->cans('add_schedule'))

        // Schedule create modal view
        function createSchedule(scheduleDate,byButton='') {
            if (typeof scheduleDate === "undefined") {
                scheduleDate = '';
            }
            var url = '{{ route('admin.interview-schedule.create')}}?date=' + scheduleDate;
            url = byButton ? url+'&is_clicked_by_button=1' : url;
            $('#modelHeading').html('Schedule');
            $.ajaxModal('#scheduleDetailModal', url);
        }
        @endif

        @if($user->cans('add_schedule'))
            function addScheduleModal(start, end, allDay) {
            var scheduleDate;
            if (start) {
                var sd = new Date(start);
                var curr_date = sd.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = sd.getMonth();
                curr_month = curr_month + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = sd.getFullYear();
                scheduleDate = curr_year + '-' + curr_month + '-' + curr_date;
            }

            createSchedule(scheduleDate);
        }
        @endif
        function openInModal(url) {
            $.ajaxModal('#questionnaireModal', url);
        }
        function beforeSend(){
            var application_id = $("select[name='applicant_id']").val();
            if(message_body.value != "" &&  sms_body.value != ""){
                swal({
                title: "@lang('errors.areYouSure')",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "@lang('app.yes')",
                cancelButtonText: "@lang('app.no')",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.job-application.interviewInvitation') }}";
                        var token = '{{ csrf_token() }}';

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token':token, 'applicationId' : application_id , 'emailMessage' : message_body.value != '' ? encodeURI(encodeURIComponent(message_body.value)) : 'N_A' , 'smsMassage' : sms_body.value != '' ? encodeURI(encodeURIComponent(sms_body.value)) : 'N_A'},
                            success: function (response) {
                                $("body").removeClass("control-sidebar-slide-open");
                                if (response.status === 'success') {
                                    $("#exampleModal").find('.close').click();
                                }
                            }
                        });
                    }
                });
            }else{
                alert("Empty fields are not allowed")
            }
        }
        function syncGC(){
            $('.sheld2').show();
            $.get("{{ url('SyncGC') }}?user_id={{$user->id}}", function(data, status){
                if(data.status == 'success'){
                    $('.sheld2').hide();
                    location.reload();
                }else{
                    $('.sheld2').hide();
                    alert(data.message);
                }
            });
        }
    </script>
@endpush