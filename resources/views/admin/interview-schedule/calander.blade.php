@foreach($all_days as $k=>$d)
    <?php $date = \Carbon\Carbon::parse($d['date']); $occupied = $d['occupied']; $format='Y-m-d'; $schedule_date =  $application->schedule->schedule_date != '' ? $application->schedule->schedule_date->format('Y-m-d') : ''; ?>
    <li class="col-7-1 p-0" >
        <div class="row m-0 fix-height align-content-center" style="width: 70px;">
            <input id="date_{{$k}}" type="radio" name="date" value="" @if($occupied || $schedule_date == $d['date'] && $application->schedule->status == 'pending') disabled @endif>
            <label for="date_{{$k}}" class="content mx-auto @if($schedule_date == $d['date'] && $application->schedule->status == 'pending') current-date @endif @if($occupied) occupied @endif" @if(!$occupied && $schedule_date == $d['date'] && $application->schedule->status == 'pending') onclick="cancleOrReschedule('{{$date->format($format)}}')" @else  @if(!$occupied) onclick="handleModalShow('{{$date->format($format)}}')" @endif @endif>
                <div class="col-12">
                    <div class="month @if($date->format('d') == 1 || $k==0) d-block @endif">{{$date->format('M')}}</div>
                    <div class="date">{{$date->format('d')}}</div>
                </div>
                <div class="col-12">
                    <div class="day">{{$date->format('D')}}</div>
                    @if($occupied)
                    <i data-toggle="tooltip" data-placement="bottom" title="{{ $d['holiday'] ?? 'Occupied'}}" class="fas fa-ban icon"></i>
                    @endif
                </div>
            </label>
        </div>
    </li>
@endforeach