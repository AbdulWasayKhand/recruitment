            
            <div class="col-md-12 mt-4 text-center">
                <ul class="d-inline-flex ovweflow-x-scroll p-4" id="calander_Ui">
                    @if(!$forCompany)
                        @foreach($all_days as $k=>$d)
                            <?php $date = \Carbon\Carbon::parse($d['date']); $occupied = $d['occupied']; $format='Y-m-d'; $schedule_date =  $application->schedule->schedule_date != '' ? $application->schedule->schedule_date->format('Y-m-d') : ''; ?>
                            <li class="col-7-1 p-0" >
                                <div class="row m-0 fix-height align-content-center" style="width: 70px;">
                                    <input id="date_{{$k}}" type="radio" name="date" value="" @if($occupied || $schedule_date == $d['date'] && $application->schedule->status == 'pending') disabled @endif>
                                    <label for="date_{{$k}}" class="content mx-auto @if($schedule_date == $d['date'] && $application->schedule->status == 'pending') current-date @endif @if($occupied) occupied @endif" @if(!$occupied && $schedule_date == $d['date'] && $application->schedule->status == 'pending') onclick="cancleOrReschedule('{{$date->format($format)}}')" @else  @if(!$occupied) onclick="handleModalShow('{{$date->format($format)}}')" @endif @endif>
                                        <div class="col-12">
                                            <div class="month @if($date->format('d') == 1 || $k==0) d-block @endif">{{$date->format('M')}}</div>
                                            <div class="date">{{$date->format('d')}}</div>
                                        </div>
                                        <div class="col-12">
                                            <div class="day">{{$date->format('D')}}</div>
                                            @if($occupied)
                                            <i data-toggle="tooltip" data-placement="bottom" title="{{ $d['holiday'] ?? 'Occupied'}}" class="fas fa-ban icon"></i>
                                            @endif
                                        </div>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
                <ul class="controller">
                    <div class="prev"><i class="fas fa-arrow-left"></i></div>
                    <div class="next"><i class="fas fa-arrow-right"></i></div>
                </ul>
            </div>

            {{--Ajax Modal Start for--}}
            <div class="modal fade bs-modal-md in" id="scheduleEditModal" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-md" id="modal-data-application">
                    <div class="modal-content" style="text-align:left">
                        <div class="modal-header pb-0" style="border-bottom:initial;padding-top:4px;">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex p-3 justify-content-start flex-column">
                                <input type="text" id="scheduleDatestring" style="font-size: 14px;" value="" class="form-control border-0 bg-transparent p-0" disabled>
                                <h4 class="modal-title mb-1"><i class="icon-plus"></i>{{ $application->schedule->status == 'pending' ? "Reschedule" : "Schedule"}} Interview</h4>
                                <div class="saperater"></div>
                            </div>
                            <div class="modal-body">
                                <form id="createSchedule" class="ajax-form" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="c" value="{{ request('c') }}">
                                    @if($application->schedule->status != 'pending')
                                    <input type="hidden" name="employees[]" value="{{ $employee->id }}">
                                    <input type="hidden" name="candidates[]" value="{{ $application->id }}">
                                    @else
                                    <input type="hidden" name="employee[]" value="{{ $employee->id }}">
                                    <input type="hidden" name="candidate_id" value="{{ $application->id }}">
                                    @endif
                                    <input type="hidden" name="scheduleDate" id="schedule_date" value="">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                                                    <label>@lang('modules.interviewSchedule.scheduleTime')</label>
                                                    <select class="form-control" id="scheduleTimeSlot" name="slot_timing" {{ ($application->status == 'pending') ? 'disabled' : '' }}>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $locations = explode(',', str_replace('"', '',  str_replace(']', '', str_replace('[', '', $application->job->location_id)))); ?>
                                        @if(sizeOf($locations) > 1 && $show_location)                                    
                                        <div class="row" id="forCompanyLocation">
                                            <div class="col">
                                                <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                                                    <label>@lang('modules.interviewSchedule.jobLocation')</label>
                                                    <select class="form-control" id="scheduleLocationId" name="location_id" {{ ($application->status == 'pending') ? 'disabled' : '' }}>
                                                        @foreach(\App\JobLocation::whereIn('id', $locations )->get() as $location)
                                                            <option value="{{$location->id}}" @if($application->schedule->location_id == $location->id) selected @endif >{{$location->location}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if($application->status != 'pending')
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <label>@lang('modules.interviewSchedule.remark')</label>
                                                    <textarea type="text" name="comment" id="comment" placeholder="@lang('modules.interviewSchedule.commentRemark')" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                        @if($application->status != 'pending')
                        <div class="modal-footer">
                            <button type="button" class="btn bg-transparent" data-dismiss="modal"><i class="fa fa-arrow-left pr-1"></i>@lang('modules.interviewSchedule.modalClosetext')</button>
                            <button type="button" class="btn btn-success save-schedule">@lang('app.submit')</button>
                        </div>
                        @endif
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            {{--Ajax Modal Ends--}}


            {{--Ajax Modal Start for--}}
            <div class="modal fade bs-modal-md in" id="locationModal" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="false" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-md">
                    <div class="modal-content" style="text-align:left">
                        <div class="modal-body">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                                            <label>@lang('modules.interviewSchedule.jobLocation')</label>
                                            <select class="form-control" id="scheduleLocationIdForCompany" name="location_id" {{ ($application->status == 'pending') ? 'disabled' : '' }}>
                                            <option value="0" >Select Location</option>
                                                @foreach(\App\JobLocation::whereIn('id', $locations )->get() as $location)
                                                    <option value="{{$location->id}}" @if($application->schedule->location_id == $location->id && $forCompany_reschedule) selected @endif >{{$location->location}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            {{--Ajax Modal Ends--}}

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{{ asset('froiden-helper/helper.js') }}"></script>
        <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
        <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>

        {{--<script src="{{ asset('assets/plugins/calendar/dist/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/calendar/dist/jquery.fullcalendar.js') }}"></script>
        <script src="{{ asset('assets/plugins/calendar/dist/locale-all.js') }}"></script>--}}

        <script src="{{ asset('assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>
        <script>
            $('.sheld2').hide();
            @if($forCompany)
                const selectLocationForCompany = document.getElementById('scheduleLocationIdForCompany');
                @if($forCompany_reschedule)
                    $('.sheld2').show();
                    $.get(window.location + '&locationIdForCompany='+selectLocationForCompany.value, function(response){
                        document.getElementById('calander_Ui').innerHTML = response;
                        $('.sheld2').hide();
                    });
                @else
                    $('#locationModal').modal('show');
                    selectLocationForCompany.addEventListener('change', function(){
                        $('#locationModal').modal('hide');
                        $('.sheld2').show();
                        document.getElementById('scheduleLocationId').value = selectLocationForCompany.value;
                        $.get(window.location + '&locationIdForCompany='+selectLocationForCompany.value, function(response){
                            document.getElementById('calander_Ui').innerHTML = response;
                            $('.sheld2').hide();
                        });
                    });
                @endif
                document.getElementById('forCompanyLocation').style.display = 'none';
            @endif
            mySwal = function() {
                swal(arguments[0]);
                if (arguments[0].showCloseButton) {
                    closeButton = document.createElement('button');
                    closeButton.className = 'swal2-close';
                    closeButton.onclick = function() { swal.close(); };
                    closeButton.textContent = '×';
                    modal = document.querySelector('.swal-modal');
                    modal.appendChild(closeButton);
                }
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                  $('.next').click(function(){
                    $('.ovweflow-x-scroll').animate({
                        scrollLeft: "+=200px"
                    }, "fast");
                })

                $('.prev').on('click', function(){
                    $('.ovweflow-x-scroll').animate({
                        scrollLeft: "-=200px"
                    }, "fast");
                })
            });
            function handleModalShow($date) {
                @if($application->schedule->status == 'pending')
                    swal({
                        title: "Are you sure",
                        text: "You want to reschedule your interview.",
                        icon: "warning",
                        buttons: {
                            cancel: "Close",
                            reschedule: {
                                text: "Reschedule",
                                value: "reschedule",
                            },
                        },
                        focusConfirm: false,
                        showCloseButton: true,
                        })
                        .then((value) => {
                        switch (value) {
                            case "reschedule":
                                reschedule($date);
                            break;
                        
                            default:
                        }
                    });
                    closeBtn();
                @else
                    reschedule($date);
                @endif
            }

            function reschedule($date){
                $('#scheduleEditModal').modal('show');
                $('#scheduleDatestring').val(moment($date).format('ddd, MMM D'));
                schedule_date.value = $date;
                $('.sheld2').show();
                $.easyAjax({
                    url: "{{route('candidate.interview-schedule.get-timing')}}",
                    type: "POST",
                    data: { date: $date, employees: ['{{$employee->id}}'], _token: '{{ csrf_token() }}', c: "{{request('c')}}", app: "{{request('app')}}" },
                        success: function (response) {
                        $('#scheduleTimeSlot').html('');
                        if(response.data) {
                            var selected = "{{ $application->schedule->slot_timing ?? '' }}";
                            // var comment = "$application->schedule->comments[0]->comment ?? ''";
                            var comment = "";
                            @if($application->schedule->status != 'pending')
                                document.getElementById('comment').value = comment; 
                            @endif
                            response.data.forEach(timings => {
                                $('#scheduleTimeSlot').append(`<option value="${timings}" ${selected == timings ? 'selected' : ''}>${timings}</option>`);
                            });
                            schedule_date.value = $date;
                            $('.sheld2').hide();
                        }
                    }
                });
            }
            $('.save-schedule').click(function () {
                console.log($('#createSchedule').serialize());
                $(this).prop('disabled', true);
                @if($application->schedule->status == 'pending')
                    var endpoint = "{{ route('candidate.interview-schedule.update', $application->schedule->id) }}?from=api";
                @else
                    var endpoint = "{{ route('candidate.interview-schedule.store') }}?from=api";
                @endif
                $.easyAjax({
                    url: endpoint,
                    container: '#createSchedule',
                    type: "POST",
                    data: $('#createSchedule').serialize(),
                    success: function (response) {
                        window.location.replace(response.url);
                    }
                });
            });
            function cancelInterview() {
                swal({
                    title: "Are you sure",
                    text: "You won't  be able to schedule your interview again.",
                    icon: "warning",
                    buttons: {
                        cancel: "Close",
                        cancelInterview: {
                            text: "Cancel Interview",
                            value: "cancelInterview",
                        },
                    },
                    focusConfirm: false,
                    showCloseButton: true,
                    })
                    .then((value) => {
                    switch (value) {
                        case "cancelInterview":
                                swal({
                                    title: "Interview Canceled",
                                    text: "",
                                    icon: "error",
                                    buttons: {
                                        okay: {
                                            text: "Okay",
                                            value: "okay",
                                        },
                                    },
                                    focusConfirm: false,
                                    showCloseButton: true,
                                    })
                                    .then((value) => {
                                    switch (value) {
                                        case "okay":
                                            cancelIt();   
                                        break;
                                    
                                        default:
                                            cancelIt();
                                    }
                                });
                                closeBtn();
                        break;
                        default:
                    }
                });
                closeBtn();
            }

            function cancleOrReschedule($date){
                swal({
                        title: "Please Confirm",
                        text: "",
                        icon: "warning",
                        showCloseButton: true,
                        buttons: {
                            cancel: "Close",
                            cancelInterview: {
                                text: "Cancel Interview",
                                value: "cancelInterview",
                            },
                            reschedule: {
                                text: "Reschedule",
                                value: "reschedule",
                            },
                            
                        },
                    })
                    .then((value) => {
                    switch (value) {
                        case "cancelInterview":
                            cancelInterview();
                        break;
                    
                        case "reschedule":
                            reschedule($date);
                        break;
                    
                        default:
                    }
                });
                closeBtn();
            }

            function cancelIt(){
                var endpoint = "{{route('candidate.interview-schedule.change-status')}}";
                $.easyAjax({
                    url: endpoint,
                    type: "POST",
                    data: "_token={{csrf_token()}}&from=api&c={{request('c')}}&status=canceled&mailToCandidate=yes&id={{ $application->interview->id }}",
                    success: function (response) {
                        if(response.status == 'success') {
                            location.href = '{{ $career_link }}';
                        }
                    }
                })
            }

            function closeBtn(){
                closeButton = document.createElement('button');
                closeButton.className = 'swal2-close';
                closeButton.onclick = function() { swal.close(); };
                closeButton.textContent = '×';
                modal = document.querySelector('.swal-modal');
                modal.appendChild(closeButton);
            }
        </script>