@php
$company = App\Company::select('id', 'timezone')->find(Auth::user()->company_id);
@endphp
@forelse(upComingHolidays() as $upComingHoliday)
    <div>
        <h4>{{ ucFirst($upComingHoliday->title) }}</h4>


        <ul class="holidayul">
                <li class="deco" id="holiday-{{$upComingHoliday->id}}" onclick=""
                    style="list-style: none;">
                    <h5 class="text-muted"
                        style="float: left">{{ \Carbon\Carbon::parse($upComingHoliday->from)->format('M d, Y') }} - {{ \Carbon\Carbon::parse($upComingHoliday->to)->format('M d, Y') }}</h5>
                    <div class="clearfix"></div>
                    <div class="direct-chat-name" style="font-size: 13px">{{ $upComingHoliday->description }}</div>
                </li>
        </ul>
    </div>
    <hr>
@empty
    <div>
        <p>@lang('messages.noUpcomingScheduleFund')</p>
    </div>
@endforelse