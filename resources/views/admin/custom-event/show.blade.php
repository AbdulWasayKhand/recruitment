<div class="modal-header">
    <h4 class="modal-title"></i> @lang('modules.customEvent.blockDayTime')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body pt-2 pb-0">
    <div class="form-body">
            <div class="row">
                <div class="col-md-12  col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">@lang('modules.customEvent.title')</label><br>
                        {{ucFirst($event->title)}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-6 ">
                    <div class="form-group">
                        <label class="control-label" for="date">@lang('modules.customEvent.date')</label><br>
                        {{Carbon\Carbon::parse($event->date)->format('M d, Y') }} @if(!$event->block_complete_day) ({{  $event->time_range }}) @endif
                    </div>
                </div>
            </div>
            @if($event->description)
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group">
                        <label class="control-label" for="description">@lang('modules.customEvent.desc')</label><br>
                        {{$event->description}}
                    </div>
                </div>
            </div>
            @endif
        </div>
</div>