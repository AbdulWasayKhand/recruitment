<link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
<div class="modal-header">
    <h4 class="modal-title"></i> @lang('modules.customEvent.blockDayTime')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body pt-2 pb-0">
    <form class="ajax-form" method="POST" id="createForm">
    @csrf
    <input type="hidden" name="id"/>
    <div class="form-body">
            <div class="row">
                <div class="col-md-12  col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">@lang('modules.customEvent.title')</label>
                        <input class="form-control form-control-md required" type="text" id="title" name="title" value="" placeholder="@lang('modules.customEvent.title')">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-6 ">
                    <div class="form-group">
                        <label class="control-label" for="date">@lang('modules.customEvent.date')</label>
                        <input type="text" name="date" id="date" class="form-control required" placeholder="@lang('modules.customEvent.date')">
                    </div>
                </div>
                <div class="col-xs-6 col-md-6">
                    <div class="form-group chooseCandidate bootstrap-timepicker timepicker">
                        <label class="control-label" for="">@lang('modules.customEvent.timeRange')</label><br>
                        <input class="form-control col-md-5 time_range d-inline" type="text" id="time_from" name="time_from" placeholder="@lang('modules.customEvent.time')">&nbsp;&nbsp; - &nbsp;&nbsp;
                        <input class="form-control col-md-5 time_range d-inline" type="text" id="time_to" name="time_to" placeholder="@lang('modules.customEvent.to')">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group">
                        <label class="control-label" for="description">@lang('modules.customEvent.desc')</label></br>
                        <textarea class="form-control form-control-md required" id="description" name="description" placeholder="@lang('modules.customEvent.desc')" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group mb-0">
                        <label class="" style="display: inline-flex;">
                            <input class="flat-red" style="margin:4px; position:initial; left:0;" type="checkbox" value="1" name="block_complete_day" id="block_complete_day" />
                            @lang('modules.customEvent.blockCompleteDay')
                        </label><br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group mb-0">
                        <label class="" style="display: inline-flex;">
                            <input class="flat-red" style="margin:4px; position:initial; left:0;" type="checkbox" value="1" name="clone_google_event" />
                            @lang('modules.customEvent.cloneGoogle')
                        </label><br>
                    </div>
                </div>
            </div>
            <div class="row d-none" id="delete-google">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group mb-0">
                        <label class="" style="display: inline-flex;">
                            <input class="flat-red" style="margin:4px; position:initial; left:0;" type="checkbox" value="1" name="delete_google_event" />
                            @lang('modules.customEvent.deleteGoogle')
                        </label><br>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
    <button type="button" class="btn btn-success" id="save-form">@lang('app.submit')</button>
</div>

<script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('/froiden-helper/helper.js') }}" type="text/javascript"></script>

<script>
    // Datepicker set
    $('#date').bootstrapMaterialDatePicker
    ({
        time: false,
        format: 'MM-DD-YYYY',
        minDate : new Date()
    });

    // Timepicker Set
    $('.time_range').bootstrapMaterialDatePicker
    ({
        date: false,
        time: true,
        shortTime: true,
        format: 'hh:mm A',
        use24hours: false,
        switchOnClick: true
    });
    $('document').ready(function(){
        $("body").on('change','input[name="clone_google_event"]',function () {
            $(this).is(':checked') ? $('#delete-google').removeClass('d-none') : $('#delete-google').addClass('d-none');
        });
        $('body').on('change','#block_complete_day',function () {
            if($(this).is(':checked')){
                $(".time_range").closest('div').parent().addClass('d-none');
                $(".time_range").closest('div').parent().prev().addClass('col-md-12');
            }
            else{
                $(".time_range").closest('div').parent().removeClass('d-none');
                $(".time_range").closest('div').parent().prev().removeClass('col-md-12');
            }
        });
        $('body').on('click','#save-form',function () {
            var block = false;
            [date, title, description].forEach(function (e) {
                if($.trim($(e).val())==''){
                    $(e).css('border','1px solid #ea4335');
                    block = true;
                }
                else{
                    $(e).css('border','1px solid #ced4da');
                }
            });
            if(block){ return null; }
            $(this).prop("disabled", true);
            $(this).css({
                "opacity": ".2",
                "cursor": "progress"
            });
            var data = $('#createForm').serializeArray();
            var $action = '{{route("admin.custom-event.store")}}';
            $.easyAjax({
                url: $action,
                container: '#createForm',
                type: "POST",
                data: data,
                redirect: true,
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });
    });
</script>