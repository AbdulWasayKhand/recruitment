@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
@endpush

@section('create-button')
    <a href="javascript:openInModal('{{ route('admin.custom-event.create') }}')" class="btn btn-dark btn-sm m-l-15"><i class="fa fa-plus-circle"></i> @lang('app.createANewCustomEvent')</a>
    {{--<a href="javascript:linkGC()" id="GC_event" class="btn btn-dark btn-sm m-l-15" style="color:white;background-color:#4e5d9e;">@if($user->clone_google_events) Unlink With Google's Events @else Link With Google's Events  @endif</a>--}}
    <a class="ml-2 multiple-action" onclick="" style="display:none"><button class="btn btn-sm btn-danger multi-delete-button" type="button"><i class="fa fa-trash"></i> @lang('app.deleteMultiple')</button></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive mt-2">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox form-check">
                                        <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                                        <label for="example-select-all"></label>
                                    </div>
                                </th>
                                <th>@lang('modules.customEvent.title')</th>
                                <th>@lang('modules.customEvent.date')</th>
                                <th>@lang('modules.customEvent.time')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-md in" id="eventModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">@lang('menu.questionnaire')</span>
               </div>
               <div class="modal-body">
                   Loading...
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn default" data-dismiss="modal">Close</button>
                   <button type="button" class="btn blue">Save changes</button>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

    <script>

    function linkGC(){
        $.easyAjax({
            type: 'POST',
            url: "{{url('linkCM_GC')}}",
            data: {user_id:'{{$user->id}}', "_token": "{{ csrf_token() }}"},
            redirect:false,
            success: function (response) {
                console.log(response);
                GC_event.innerHTML = response.data;
            }
        });
    }
        var table = $('#myTable').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            pageLength: 25,
            stateSave: true,
            order: [1, "desc"],
            ajax: '{!! route('admin.custom-event.data') !!}',
            language: languageOptions(),
            "fnDrawCallback": function( oSettings ) {
                $("th.text-center").removeClass('sorting_asc');
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'selectColumn', name: 'selectColumn', width: '4%', class: 'text-center', orderable: false, searchable: false},
                { data: 'title', name: 'title' },
                { data: 'date', name: 'date' },
                { data: 'time_range', name: 'time_range' },
                { data: 'action', name: 'action', width: '20%',sortable:false }
            ]
        });

        new $.fn.dataTable.FixedHeader( table );

        // Select Checkbox
        $('#myTable').on('change', 'input[type="checkbox"]', function () {
            if(this.id == 'example-select-all') {
                $('.multi-check').prop('checked', $(this).prop('checked'));
            }

            if($('input[type="checkbox"]:checked').length > 0) {
                $('.multiple-action').show();
            } else {
                $('.multiple-action').hide();
            }
        });

        $('.multi-delete-button').on('click', function () {
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var data = $('#myTable input[type=checkbox]:not(#example-select-all)').serialize();
                    var token = "{{ csrf_token() }}";
                    data += "&_token="+token+"&_method=DELETE";
                    $.easyAjax({
                        type: 'POST',
                        url: '{{route("admin.custom-event.multiDelete")}}',
                        data: data,
                        success: function (response) {
                            if (response.status == "success") {
                                location.reload();
                            }
                        }
                    });
                }
            });
        })

        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('row-id');
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.custom-event.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });
        function openInModal(url) {
            $.ajaxModal('#eventModal', url);
        }
    </script>
@endpush