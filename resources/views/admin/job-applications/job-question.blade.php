@if(count($jobQuestion) > 0)
    @forelse($jobQuestion as $question)
        @php
            $answered = $question->applicantAnswer($applicationId)->answer;
        @endphp
        <div class="form-group">
        @if(count($question->custom_fields) > 0)
                            @if($question->custom_fields[0]->question_type == 'checkbox' || $question->custom_fields[0]->question_type == 'radio' || $question->custom_fields[0]->question_type == 'dropdown')
                                @php
                                    $question_value1 = json_decode($question->custom_fields[0]->question_value);
                                    $answered = explode(',',$question->applicantAnswer($applicationId)->answer);
                                @endphp
                            @endif
                            @if($question->custom_fields[0]->question_type == 'dropdown')
                                <div class="form-group">
                                    <label class="control-label" for="answer[{{ $question->id}}]">{{ $question->question }}</label>
                                    <select class="form-control form-control-lg" id="answer[{{ $question->id}}]" name="answer[{{ $question->id}}]" placeholder="@lang('modules.front.yourAnswer')">
                                @foreach($question_value1 as $value)
                                    <option value="{{ $value }}" {{($answered==$value) ? 'selected' : ''}}>{{ $value }}</option>
                                @endforeach
                                    </select>
                                </div>
                            @elseif ($question->custom_fields[0]->question_type == 'checkbox')
                                <div class="form-group">
                                    <label class="control-label" for="answer[{{ $question->id}}]">{{ $question->question }}</label></br>
                                @foreach($question_value1 as $value)
                                    <label class="checkbox-inline"><input id="answer[{{ $question->id}}]" name="answer[{{ $question->id}}][]" type="checkbox" value="{{ $value }}" {{(is_array($answered) && in_array($value,$answered)) ? 'checked' : ''}}>&nbsp;&nbsp;{{ $value }}</label>&nbsp;&nbsp;
                                @endforeach
                                @if($question->required == 'yes')
                                <input type="hidden" name="checkbox[{{ $question->id}}]">
                                @endif
                                </div>
                             @elseif ($question->custom_fields[0]->question_type == 'radio')
                                 <div class="form-group">
                                    <label class="control-label" for="answer[{{ $question->id}}]">{{ $question->question }}</label></br>
                                @foreach($question_value1 as $value)
                                    <label class="checkbox-inline"><input id="answer[{{ $question->id}}]" name="answer[{{ $question->id}}]" type="radio" value="{{ $value }}" {{(is_array($answered) && in_array($value,$answered)) ? 'checked' : ''}}>{{ $value }}</label>&nbsp;&nbsp;
                                @endforeach
                                @if($question->required == 'yes')
                                <input type="hidden" name="radio[{{ $question->id}}]">
                                @endif
                                </div>
                            @else
                                <div class="form-group">
                                    <label class="control-label" for="answer[{{ $question->id}}]">{{ $question->question }}</label>
                                    <input class="form-control form-control-lg" type="text" id="answer[{{ $question->id}}]" name="answer[{{ $question->id}}]" placeholder="@lang('modules.front.yourAnswer')" value="{{$answered}}">
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label class="control-label" for="answer[{{ $question->id}}]">{{ $question->question }}</label>
                                <input class="form-control form-control-lg" type="text" id="answer[{{ $question->id}}]" name="answer[{{ $question->id}}]" placeholder="@lang('modules.front.yourAnswer')" value="{{$answered}}">
                            </div>
                        @endif
        </div>
    @empty
    @endforelse
@endif