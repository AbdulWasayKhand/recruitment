<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-bar-rating-master/dist/themes/fontawesome-stars.css') }}">
<style>

    .right-panel-box{
        /* overflow-x: scroll; */
        max-height: 100vh;
    }
    .cancel {
        background-color: #f44336 !important;
    }
    .resume-button{
        text-align: center; margin-top: 7px;
    }
    .bold-text {
        font-size: 24px;
        font-weight: 500;
    }
    .rounded-circle-text{
        background-color: #93c954;
        height: 60px;
        width: 60px;
        padding-top: 10px;
        color: #fff;
        margin: 0 auto;
        font-size: 2em;
        font-weight: bold;      
    }
    .line-break{
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .label {
        min-width: 100px;
        white-space: nowrap;
        /* max-width: 200px; */
        /* overflow: hidden; */
        /* text-overflow: ellipsis; */
        padding: 5px;
        font-size: 15px;
        font-weight: bold;
        color: white;
        display: inline-block;
        border-radius: 3px;
        text-align: center;
        cursor: default;
        margin: 0 3px;
        margin-top: 10px;
    }
    #addLabels {
        overflow: hidden;
        text-overflow: ellipsis;
        font-size: 15px;
        font-weight: bold;
        width: 50px;
        border-radius: 3px;
        text-align: center;
        cursor: pointer;
        background: #eceaea;
        display: inline-block;
        padding: 5px;
        margin-top: 10px;
        /* position: absolute; */
    }
    .lbl {
        margin: 5px 0;
        border-radius: 3px;
        width: 100%;
        display: flex;
        cursor: pointer;
        border-radius: 3px;
    }
    .lbl_txt {
        white-space: nowrap !important;
        overflow: hidden !important;
        text-overflow: ellipsis !important;
        color: #fff;
        width: 100%;
        display: block;
        padding: 5px 5px 5px 0;
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
    }
    .lbl_txt:hover, .lbl_pointer:hover ~ * {
        opacity: .6;
    }

    .lbl_pointer {
        padding: 6px;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    #all_labels {
        overflow: scroll;
        max-height: 350px;
        background: #f9f9f9;
        width: 310px;
        white-space: nowrap;
        position: absolute;
        z-index: 4444;
        display: none;
        padding: 5px;
        left: 0px;
        top: 22px;
        border: 1px solid lightgrey;
    }
    @media screen and (max-width: 540px) {
        #all_labels {
            width: 180px;
        }
    }
    #new_label {
        width: 100%;
        border-top: 1px solid #D3D3D3;
    }
    .lbl_btn {
        width: 93%;
        background: #e2e2e2;
        display: inline-block;
        text-align: center;
        margin: 5px 0;
        padding: 10px 0;
        font-size: 15px;
        margin-left: 3.33%;
        cursor: pointer;
        border-radius: 3px;
    }
    #pasted_labels {
        width: 93%;
        margin: 3.33%;

    }
    .tick {
        position: absolute;
        margin: 10px;
        color: #f3f0f0;
        right: 20px;
    }
    .color_code {
        padding: 20px;
        margin:5px;
        cursor: pointer;
        border-radius: 50%;
    }
    @media screen and (max-width: 540px) {
        .color_code {
            padding: 10px;
        }
    }
    #pic_color  {
        display: inline-flex;
        margin: auto;
    }
    #color_box {
        border-top: 1px solid #D3D3D3;
        text-align: center
    }
    #clonable, #clonable_label {
        display: none !important;
    }
    #add_new_labels > div{
        width: 92%;
        position: relative;
        padding: 5px 10px;
        border-radius: 5px;
        color:ivory;
        display: inline-grid;
        margin: 0 0 5px 4%;
    }
    #add_new_labels > div > span {
        position: absolute;
        right: 5px;
        display: none;
    }
    .ok, .nok {
        cursor: pointer;
        margin-left: 5px;
    }
    #add_new_labels > div:hover span {
        display: block;
    }
    #add_new_labels > div > div {
        outline: none;
        max-width: 100%;
    }
    #add_new_labels {
        display: grid;
    }
    #priint {
        color: white;
        margin-right: 10px;;
    }
    .section-name {
        font-weight: 500;
        font-size: 13px;
    }
    .txt-age {
        font-size: 14px;
        font-weight: 500;
    }

    p#innerPara {
        padding:20px ;
    }
    legend{
        width: 110px;
        margin-left: 10px;
        padding-left: 10px;
        color: #6c757d;
        font-size: 14px;
        font-weight: 500;
    }
    .btn-attachment {
        padding: 10px;
        background-color: #fff;
        border: 1px solid #c5c5c5;
        cursor: pointer;
        border-radius: 4px;
     }
     .btn-upload {
        padding: 6px;
        background-color: #fff;
        border: 2px solid #17a2b8;
        cursor: pointer;
        border-radius: 4px;
        color: #17a2b8;
        font-weight: 500;
    }
    .text-header {
        font-weight: bold;
        color: #6c757d;
    }
    .badge-interview{
        float: right;
        background-color: #28a745;
        color: #fff;
        padding: 4px 10px 4px 10px;
        font-size: 12px;
        border-radius: 20px;
        font-weight: bold;
    }
    .job-card-header {
        /* background-color: #f1fbf3;
        border-top: 4px solid #28a745; */
    }
    .prev-int-card-header {
        /* background-color: #f1fbf3; */
        border-top: 4px solid #727a82;
    }
    .fw-500 {
        font-weight: 500;
    }
    .table-header {
        background-color: #f7f7f7;
    }
    .list-group-flush {
        font-size: 12px;
    }
    .pass-badge {
        background-color: green;
        color: white!important;
        padding: 5px 15px 5px 15px;
        font-size: 12px;
        border-radius: 20px;
        font-weight: 500;
        text-align: center;
    }

    .fail-badge {
        background-color: red;
        color: white!important;
        padding: 5px 15px 5px 15px;
        font-size: 12px;
        border-radius: 20px;
        font-weight: 500;
        text-align: center;
    }
    .canceled-badge {
        background-color: gray;
        color: white!important;
        padding: 4px 10px 4px 10px;
        font-size: 10px;
        border-radius: 20px;
        font-weight: 500;
        text-align: center;
    }
    .text-internal-ques {
        /* text-decoration: underline; */
        cursor: pointer;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .prev-questionnaire {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 80%;
    }
    .timeline-container {
        width: 100%;
        border-left: 2px solid #a5a5a5;
        position: relative;
    }
    .timeline-icon {
        position: absolute;
        left: -19px;
        top: -8px;
        border: 10px solid white;
        background: #fff;
        border-radius: 60%;
        z-index: 1000;
        font-size: 20px;
    }
    .br-wrapper {
        display: inline-block;
    }
    .timeline-item {
        border: none !important;
        line-height: 1.5;
    }
    .font-15 {
        font-size: 15px;
    }
    </style>

<div class="rpanel-title"> <div class="d-inline right-side-toggle" style="cursor:pointer;">Applicants</div>
        <button hidden onclick="printIt()" accesskey="p"></button>
    <span class="right-side-toggle"><i class="fa fa-close" aria-hidden="true"></i></span> 
    <a href="javascript:;" onclick="printIt()" id="priint" class="p-hide pull-right" title="Shortcut: Alt + P"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
</div>
<div class="r-panel-body p-3" id="print_container">
    <style>
        @media print {
            .p-hide {
                display: none !important;
            }
            .form-control, .select2-container--default .select2-selection--multiple, .select2-selection__choice {
                border-color: transparent !important;
                background: white !important;
                color: black !important;
            }
            .select2-selection__choice__remove {
                color: transparent !important;
                display: block !important;
            }
            .print_rapper{
                width: 21cm;
                min-height: 29.7cm;
                padding: 1cm;
                margin: 1cm auto;
                margin-top: 2.5cm;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }

            @page {
            size: A4;
            margin: 0;
            }
            .navv {
                cursor: pointer !important;
            }
        }
    </style>
    <div class="print_rapper">
        <div class="row font-12" style="overflow: scroll;">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12 p-0 section-name">
                        <div class="col-sm-12 mt-4">
                            <p class="text-muted bold-text m-0">
                                {{ ucwords($application->full_name) }}
                                <?php 
                                $age = 'DOB';
                                if($application->dob ?? 0){
                                    $dob  =  date('Y', strtotime($application->dob)); 
                                    $thisYear = date('Y');
                                    $age = ($thisYear - $dob) . ' Years';
                                }
                                // Carbon\Carbon::parse($application->dob)->diffForHumans();
                                ?>
                                {{--<small class="txt-age">(<b><i>{{ $age }}</i></b>)</small>--}}
                            </p>
                        </div>
                        <div class="col-sm-12 pt-1">
                            <p class="text-muted m-0" id="phone-{{ $application->id }}" style="margin-bottom: 0;">{{ $application->phone }} <a href="javascript:void(0);" class="text-copier" onmouseover="tagOver()" onmouseout="tagOut()"><i class='fa fa-copy'></i><span class="d-none">{{ $application->phone }}</span></a></p>
                            @if((\App\SmsLog::where('number', '=', $application->phone )->orderBy('id', 'desc')->first()->status ?? '') == 'bounce')
                            <span style="color:red; font-size:10px;">
                                Phone Not Accepting Text Message<br />
                                (Could be Service or a Landline Phone)
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-12 pt-1">
                            <a class="text-muted" style="text-decoration: underline;color: #007bff !important;" id="email-{{ $application->id }}" href="mailto:{{ $application->email }}">{{ $application->email }}</a> <a href="javascript:void(0);" class="text-copier" onmouseover="tagOver()" onmouseout="tagOut()"><i class='fa fa-copy'></i><span class="d-none">{{ $application->email }}</span></a>
                        </div>
                        {{--<div class="col-sm-12">
                            <p class="text-muted" >{{$application->city ?? 'City' }}, {{$application->state ?? 'State'}}, {{$application->country ?? 'Country'}}.</p>
                        </div>--}}
                        @if($user->cans('edit_job_applications'))
                        <div class="col-sm-12 pt-1">                            
                            <select id="example-fontawesome" name="rating" autocomplete="off">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <i class="fas fa-trash-alt out-of-print" id="trash_ico" style="color:#dc3545; margin: 3px 0 0 5px; cursor:pointer; {{$application->rating ? '' : 'display:none;'}}" onclick="removeStars()"></i>
                            @if($application->rating)
                            <p class="text-muted d-none inside-print" id="rating-to-print">
                                <b>Rating</b> : {{$application->rating}}
                            </p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
                @if($application->deleted_at == null)
                    <div class="text-muted resume-button">
                        <label class="p-hide" style="position:relative"> </label>
                        <a href="javascript:"  onclick="sms_number = '{{$application->phone}}'; app_id = '{{$application->id}}'"  data-toggle="modal" data-target="#exampleModalTwo" class="btn btn-sm btn-success p-hide btn-block" style="background-color: deepskyblue;">
                            Send Personal Text Message
                        </a>                        
                    </div>
                    @if($application->company_id != 50 && $application->company_id != 51 && $interviewAction->count() && \App\InterviewSchedule::where('job_application_id', $application->id)->where('stage_id', $interviewAction[0]->stage_id)->where('status', 'pending')->get()->count() == 0)
                    <div class="text-muted resume-button">
                        <label class="p-hide" style="position:relative"><input type="checkbox" id="edit_message" style="position:initial"> @lang('app.edit') @lang('menu.messages') @lang('menu.before') @lang('menu.sending')</label>
                        <a href="javascript:sendInterviewInvitation({{ $application->id }})" class="btn btn-sm btn-success p-hide btn-block">
                            @if($application->workflow_stage->interview_action->id) @lang('modules.jobApplication.resendInteviewInvite')  @else @lang('modules.jobApplication.sendInteviewInvite') @endif
                        </a>
                        <input type="hidden" data-toggle="modal" data-target="#exampleModal" id="open_edit_modal">
                        {{-- @if($has_interview_invited)<span class="text-success">Invited</span>@endif --}}
                    </div>
                    @endif
                @endif
                <div class="row mt-5 out-of-print">
                    <div class="col-lg-12">
                        <fieldset class="border pb-3 pl-3">
                            <legend>Attachments</legend>

                            <div class="col-lg-12">
                                <div class="row">
                                    <?php $show_none = true; ?>
                                    @if(strpos($application->photo_url,'avatar.png') < 1)
                                        @php($show_none = false)
                                        <a href="{{$application->photo_url}}" class="btn-attachment" style="margin-right: 5px; color:black; text-decoration: none;" target="_blank">
                                            <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            <span class="text-muted">Photo</span>
                                        </a>
                                    @endif
                                    @foreach($application->attachments as $attachment)
                                            @php($show_none = false)
                                            <a href="{{ $attachment->path }}" class="btn-attachment" style="margin-right: 5px; color:black; text-decoration: none;" target="_blank">
                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                <span class="text-muted">{{$attachment->name}}</span>
                                            </a>
                                    @endforeach
                                    @if($show_none)
                                        <i>None</i>
                                    @endif
                                    <!-- <button class="btn-attachment">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        <span class="text-muted">Resume</span>
                                    </button>
                                    <button class="btn-attachment ml-2 mt-1">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        <span class="text-muted">CPA Certificate</span>
                                    </button>
                                    <button class="btn-attachment mt-3">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        <span class="text-muted">Experience Letter Acme Inc.</span>
                                    </button> -->
                                    <button class="btn-upload w-100 mt-3" style="display: none;">
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                        <span>Upload</span>
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-md-8 right-panel-box" style="overflow: scroll;">
                <div class="row" style="padding-bottom:100px;">
                    @if($answers->count())
                        <div class="col-sm-12 d-none inside-print" style="margin-top:10px;">
                            <h3>Applicant's Answers ({{\Carbon\Carbon::parse($application->created_at)->timezone($application->company->timezone)->format('M d, Y h:i A')}})</h3>
                            @foreach($answers as $answer)
                                <div class="{{ $answer->question->is_qualifiable() ? ($answer->qualified() ? 'text-success' : 'text-danger') : '' }}">
                                    <strong>
                                        {{$answer->question->question}}
                                    </strong><br>
                                    <p class="text-muted">{{ ucfirst($answer->answer)}}</p>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-sm-12">
                        <div class="" style="margin-top: 20px;">
                            <h4>Labels: </h4>
                            <div style="display:inline-flex;" style="margin:10px 0;">
                                <div id="clonable" class="label"></div>
                                <div id="label_container">
                                    @foreach($application->labels as $label)  
                                        <?php $app = \App\ApplicationLabel::find($label->label_id); ?>    
                                        <div id="lbl_{{$label->label_id}}" class="label" style="background:{{$app->label_color}};" title="{{$app->label_value}}">{{$app->label_value}}</div>
                                    @endforeach
                                    <br/>
                                    <div class="position-relative d-inline p-hide" style="margin-left: 3px;">
                                        <div id="addLabels" onclick="all_labels.style.display = all_labels.style.display == 'block' ? 'none' : 'block';">+</div>
                                        
                                        <div id="all_labels" style="z-index:44444">
                                            <h5>AVAILABLE LABELS</h5>
                                            <div id="pasted_labels">
                                                    <?php $lids = \App\LabelTransition::where('application_id','=',$application->id)->pluck('label_id')->toArray(); ?>
                                                    
                                                @foreach($labels as $label)
                                                    <span class="lbl" data-applicant_id="{{$application->id}}"  onclick="addLabel(this, {{$application->id}}, {{$application->company_id}})" data-id='{{$label->id}}' data-value='{{$label->label_value}}' data-color='{{$label->label_color}}' ><span class="lbl_pointer" style="background: {{$label->label_color}};"></span><span class="lbl_txt" style="background: {{$label->label_color}};">{{$label->label_value}}</span><i class="fas fa-check tick" style="display: {{ in_array($label->id, $lids) ? 'block' : 'none' }};"></i></span>
                                                @endforeach
                                            </div>
                                            <div id="add_new_labels">
                                                <h5>NEW LABELS</h5>
                                                <div id="clonable_label"><div contenteditable="true" onkeydown="pasteLabel(this.parentNode, {{$application->company_id}}, {{$application->id}})"></div><span><i class="fas fa-check ok" onclick="addMe(this.parentNode.parentNode, {{$application->company_id}}, {{$application->id}})"></i><i class="fas fa-close nok" onclick="this.parentNode.parentNode.remove()"></i></span></div>
                                            </div>
                                            <div id="color_box">
                                                <div id="pic_color">
                                                    <div class="color_code" data-color='#4caf50'></div>
                                                    <div class="color_code" data-color='#ffc107'></div>
                                                    <div class="color_code" data-color='#ffeb3b'></div>
                                                    <div class="color_code" data-color='#f44336'></div>
                                                    <div class="color_code" data-color='#00bcd4'></div>
                                                </div>
                                            </div>
                                            <div id="new_label">
                                                <span class="lbl_btn" onclick="all_labels.style.display = 'none'; $('.clonable_label').remove();">Cancel</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12 mt-4">
                        <div class="card fw-500" style="width: 100%;">
                            <div class="card-header job-card-header" style="background-color:rgba({{implode(',',sscanf($application->workflow_stage->color ?? '#28A745', '#%02x%02x%02x'))}}, 0.3); border-top: 4px solid {{$application->workflow_stage->color}}; ">
                                <span class="text-header">{{$application->job->title}}</span>
                                <span class="badge-interview" style="background-color:{{$application->workflow_stage->color}};">{{ucFirst($application->workflow_stage->title)}}</span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="text-muted" >Applied</p>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-muted" >{{ Carbon\Carbon::parse($application->created_at)->timezone($application->company->timezone)->format('M d, Y') }} ({{ Carbon\Carbon::parse($application->created_at)->timezone($application->company->timezone)->diffForHumans() }})</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="text-muted pt-2" >Qualification Score</p>
                                        </div>
                                        <div class="col-md-4">
                                        <p class="pt-2 @if(str_contains($application->qualified,'UNQUALIFIED')) text-danger @elseif(str_contains($application->qualified,'QUALIFIED')) text-success @else 'text-muted' @endif">
                                            <?= $application->qualified; ?>
                                        </p>
                                        </div>
                                        <div class="col-md-4 out-of-print">
                                            <a href="javascript:" onclick="loadQna('{{$application->id}}')" data-toggle="modal" data-target="#addDepartmentModal" class="btn btn-outline-secondary" style="font-size: 12px;float: right;">Answers</a>
                                        </div>
                                    </div>
                                </li>
                                <!-- <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="text-muted" >Applied</p>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-muted" >12/25/2020 (7 days ago)</p>
                                        </div>
                                    </div>
                                </li> -->
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <!-- @forelse($interviews as $interivew) -->
                                                        <!-- @if($interivew->status == 'pending') -->
                                                            <!-- @if($application->workflow_stage->interview_actions->count()) -->
                                                            <?php $is_id = \App\InterviewScheduleEmployee::where("interview_schedule_id", "=" , $application->interview->id)->where("user_id", "=", $user->id)->where('user_accept_status', '=', 'waiting')->first()->id ?? 0 ?>
                                                            @if($is_id)
                                                                <button onclick="employeeResponse({{$is_id}})" class="btn btn-sm btn-success notify-button responseButton">Accept Interview</button>
                                                            @endif
                                                            @php($has_accept = $interivew->hasAccepted(auth()->user()->id))
                                                            @if($has_accept->id) 
                                                                <a href="javascript:void(0)" class="btn btn-primary out-of-print" style="color: #fff;background-color: #007bff;border-color: #007bff; font-size: 12px;" onClick="getScheduleDetail({{$interivew->id}})">Start Interview</a>
                                                            @endif
                                                                <span class="font-weight-light">
                                                                    Interview Scheduled: 
                                                                    <i>{{ Carbon\Carbon::parse($interivew->schedule_date)->timezone($application->company->timezone)->format('M d, Y')}} ({{$interivew->slot_timing}})</i>
                                                                </span><br>
                                                                @if(!$is_id)
                                                                <span class="font-weight-light">
                                                                    (<b>{{\App\WorkflowStageActionInterview::where('stage_id', '=', $interivew->jobApplication->workflow_stage_id)->first()->session_title ?? ''}}</b>)@if(empty($has_accept->id)) Accepted by {{ucFirst($interivew->accepter()->name)}}@endif
                                                                </span><br>
                                                                @endif
                                                            <!-- @endif -->
                                                        <!-- @endif -->
                                                        <!-- <a href="javascript:moveNextStage()" class="btn btn-primary" style="color: #fff;background-color: #007bff;border-color: #007bff; font-size: 12px;">@lang('modules.jobApplication.advance')</a> -->
                                                    <!-- @empty
                                                        @if($application->workflow_stage->interview_actions->count())
                                                        <p class="font-weight-light mb-0 mt-2">@lang('modules.jobApplication.waiting4ApplicantInterview')</p>
                                                        @else
                                                        <a href="javascript:moveNextStage()" class="btn btn-primary" style="color: #fff;background-color: #007bff;border-color: #007bff; font-size: 12px;">@lang('modules.jobApplication.advance')</a>
                                                        @endif
                                                    @endforelse -->
                                                </div>
                                                
                                                @if($application->hiring_status=='none')
                                                    @if($application->schedule->status != 'pending')
                                                        <div class="col-md-3 text-right out-of-print">   
                                                            <button type="button" class="btn btn-success change_hiring_status" style="color: #fff;border-color: #28a745; font-size: 12px;" stage_id="{{$application->workflow_stage_id}}" hiring_status="hired">Hire</button>
                                                            <button type="button" class="btn btn-danger change_hiring_status" style="color: #fff;border-color: #dc3545; font-size: 12px;" stage_id="{{$application->workflow_stage_id}}" hiring_status="rejected">Reject</button>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-4">
                        <div class="card fw-500" style="width: 100%;">
                            <div class="card-header prev-int-card-header">
                                <span class="text-header">Previous Interviews</span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item pb-0 table-header out-of-print">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="text-muted" >Session Name</p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="text-muted" >Date</p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="text-muted" >Status</p>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="text-muted text-internal-ques">Internal Questionnaires</p>
                                        </div>
                                    </div>
                                </li>
                                @foreach($interviews as $interivew)
                                    @if($interivew->status != 'pending')

                                        <li class="list-group-item pb-0">
                                            <div class="row p-hide">
                                                <div class="col-md-4">
                                                    <p class="text-muted" >{{\App\WorkflowStageActionInterview::where('stage_id', '=', $interivew->stage_id)->first()->session_title ?? ''}}</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p class="text-muted" >{{ Carbon\Carbon::parse($interivew->created_at)->timezone($application->company->timezone)->format('M d, Y, h:i A')}}</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <span class="text-muted  @if($interivew->status == 'fail') fail-badge @endif @if($interivew->status == 'pass') pass-badge @endif @if($interivew->status == 'canceled') canceled-badge @endif">{{ucfirst($interivew->status)}}</span>
                                                </div>
                                                <div class="col-md-4 out-of-print">
                                                    <!-- <p class="text-muted text-internal-ques" > -->
                                                        @foreach(\App\WorkflowStageActionInterviewQuestionnaire::where('action_id', '=',  \App\WorkflowStageActionInterview::where('stage_id', '=', $interivew->stage_id)->first()->action_id ?? 0)->get() as $questionnaire)
                                                            <?php $questionnaire_ = \App\Questionnaire::find($questionnaire->questionnaire_id); ?>  
                                                            @if(isset($questionnaire_->id))
                                                                <a href='javascript:addEditQuestionnaire({{$questionnaire_->id}})' title="{{$questionnaire_->name}}">{{$questionnaire_->name}}</a>,
                                                            @endif
                                                        @endforeach
                                                    <!-- </p> -->
                                                </div>
                                            </div>
                                            <div class="col-sm-12 inside-print d-none">
                                                {{\App\WorkflowStageActionInterview::where('stage_id', '=', $interivew->stage_id)->first()->session_title ?? ''}} 
                                                {{ Carbon\Carbon::parse($interivew->created_at)->timezone($application->company->timezone)->format('M d, Y, h:i A')}}
                                                ({{ucfirst($interivew->status)}})
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                   @if(count($application->getQuestinnaires('internal')))
                    <div class="col-sm-12 inside-print d-none" style="margin-top:30px;">
                        <h3>Internal Questionnaires</h3>
                        @foreach($application->getQuestinnaires('internal') as $key => $answers)
                            <h4><u>{{$key}}</u> ({{ Carbon\Carbon::parse($answers[0]->created_at)->timezone($application->company->timezone)->format('M d, Y, h:i A')}}) </h4>
                            @foreach($answers as $answered)
                                <div class="{{ $answered->question->is_qualifiable() ? ($answered->qualified() ? 'text-success' : 'text-danger') : '' }}">
                                    <strong>
                                        {{$answered->question->question}}
                                    </strong><br>
                                    <p class="text-muted">{{ ucfirst($answered->answer)}}</p>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    @endif
                   @if(count($application->getQuestinnaires('external')))
                    <div class="col-sm-12 inside-print d-none" style="margin-top:30px;">
                        <h3>External Questionnaires</h3>
                        @foreach($application->getQuestinnaires('external') as $key => $answers)
                            <h4><u>{{$key}}</u> ({{ Carbon\Carbon::parse($answers[0]->created_at)->timezone($application->company->timezone)->format('M d, Y, h:i A')}}) </h4>
                            @foreach($answers as $answered)
                                <div class="{{ $answered->question->is_qualifiable() ? ($answered->qualified() ? 'text-success' : 'text-danger') : '' }}">
                                    <strong>
                                        {{$answered->question->question}}
                                    </strong><br>
                                    <p class="text-muted">{{ ucfirst($answered->answer)}}</p>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    @endif
                    <div class="col-sm-12 mt-4 p-hide">
                        <div class="card fw-500" style="width: 100%;">
                            <div class="card-header prev-int-card-header">
                                <span class="text-header">Questionnaires</span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item pb-0 table-header out-of-print">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="text-muted" >Questionnaires</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p class="text-muted" >Date</p>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <p class="text-muted text-internal-ques">Internal Questionnaires</p> -->
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item pb-0">
                                    {{--<div class="row out-of-print">
                                        <div class="col-md-6">
                                            <p class="text-muted pt-1 prev-questionnaire" >Job Questions</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p class="text-muted pt-1" >{{\Carbon\Carbon::parse($application->created_at)->timezone($application->company->timezone)->format('M d, Y h:i A')}}</p>
                                        </div>
                                        <div class="col-md-3 out-of-print">
                                            <a href="javascript:" onclick="loadQna('{{$application->id}}')" data-toggle="modal" data-target="#addDepartmentModal" class="btn btn-outline-secondary" style="font-size: 12px;float: right;">Answers</a>
                                        </div>
                                    </div>--}}
                                            
                                </li>
                                @foreach($previous_questionnaire as $qna)
                                    @if($qna['name'] != '')
                                        <li class="list-group-item pb-0 p-hide">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted pt-1 prev-questionnaire" >{{ $qna['name'] }}</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p class="text-muted pt-1" >{{ $qna['created_at'] }}</p>
                                                </div>
                                                <div class="col-md-3 out-of-print">
                                                    <a id="prev_questionnaire_button_{{$qna['id']}}" href="javascript:addEditQuestionnaire({{$qna['id']}}, {{$qna['is_external']}})" class="btn btn-outline-secondary" style="font-size: 12px;float: right;">{{ ($qna['is_external']) ? 'Answers' : ($qna['is_filled'] ? 'Edit' : 'Fill')}}</a>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-4 out-of-print">
                        <div class="card fw-500" style="width: 100%;">
                            <div class="card-header prev-int-card-header" style="border-top: 4px solid #007bff;">
                                <span class="text-header">Internal Note</span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item pb-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" id="add_update_note_value" rows="3" placeholder="Enter your note here..."></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="button" id="add_update_note" class="btn btn-sm btn-outline-primary p-hide">@lang('modules.jobApplication.addNote')</button>
                                                @if($application->notes->count()) <a href="javascript:;" class="btn btn-sm btn-outline-primary p-hide" data-toggle="modal" applicant_id="{{$application->id}}" data-target="#NotesModal">@lang('app.viewNotes')</a> @endif
                                            </div>
                                            <div class="form-group" id="move_stage_dropdown">
                                                @if($application->job->workflow_id==0)
                                                <label for="exampleFormControlSelect1">Move to stage</label>
                                                <select class="select2 form-control applicant_status_id" onchange="onChangeApplicantStatus(this)" data-applicant="{{$application->id}}">
                                                    @foreach($statuses as $status)
                                                        <option value="{{ $status->id }}" @if($application->status_id == $status->id) selected @endif>{{ ucwords($status->status) }}</option>
                                                    @endforeach
                                                </select>
                                                @else
                                                <label for="exampleFormControlSelect1">Move to stage</label>
                                                <select class="form-control" onchange="onChangeApplicantStage(this)">
                                                    @foreach($stages as $stage)
                                                        <option value="{{$stage->id}}" @if($application->workflow_stage_id == $stage->id) selected @endif >{{$stage->title}}</option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-4">
                        <div class="fw-500 timeline-container" style="width: 100%;">
                            <h5 class="text-muted ml-3 mb-4">TIMELINE<h5>
                            <ul class="list-group list-group-flush">
                                @foreach($application->timelines as $timeline)
                                    <li class="list-group-item timeline-item pb-0 @if($timeline->entity == 'notes') notes-li @endif">
                                        <div class="pb-2">
                                            <i id="i-{{$application->id}}" class="
                                            @if($timeline->entity == 'status')
                                                far fa-bell 
                                            @endif

                                            @if($timeline->entity == 'interview_canceled')
                                                fas fa-user-times  
                                            @endif

                                            @if(in_array($timeline->entity, ['interview_schedule','interview_reschedule']))
                                                fas fa-walking 
                                            @endif

                                            @if($timeline->entity == 'questionnaires')
                                                fa fa-pencil-square-o 
                                            @endif

                                            @if($timeline->entity == 'notes')
                                                fa fa-sticky-note-o notes-timeline
                                            @endif

                                            @if($timeline->entity == 'documents')
                                                fa fa-sticky-note-o
                                            @endif

                                            @if($timeline->entity == 'interview_fail')
                                                fa fa-times 
                                            @endif

                                            @if($timeline->entity == 'interview_pass')
                                                fas fa-check 
                                            @endif

                                            @if($timeline->entity == 'stage')
                                                fas fa-arrow-up 
                                            @endif

                                            @if(in_array($timeline->entity, ['attachment_resume', 'attachment_photo'] ))
                                                fa fa-paperclip
                                            @endif

                                            @if($timeline->entity == 'hire')
                                                fas fa-user-check 
                                            @endif

                                            @if($timeline->entity == 'reject')
                                                fas fa-user-times 
                                            @endif

                                            @if($timeline->entity == 'rating')
                                                fa fa-star                                     
                                            @endif

                                            @if($timeline->entity == 'label')
                                                fas fa-tags font-15                                     
                                            @endif
                                            

                                             timeline-icon" 
                                             aria-hidden="true"
                                             ></i>
                                            <p class="text-muted font-weight-light" style="position:relative; z-index: 44444; display: inline-block; @if($timeline->entity == 'hire') color:green!important; @endif @if($timeline->entity == 'reject') color:red!important; @endif">{{\Carbon\Carbon::parse($timeline->created_at)->timezone($application->company->timezone)->format('m/d/Y h:i A. l')}}</p><br/>
                                            <p class="text-muted fw-500 pb-1" style="@if($timeline->entity == 'hire') color:green!important; @endif @if($timeline->entity == 'reject') color:red!important; @endif">
                                                {!!$timeline->comments!!} 
                                            </p>
                                            @if(in_array($timeline->entity,["interview_schedule","interview_reschedule","interview_canceled","interview_pass","interview_fail"]))
                                                <span class="badge-interview" style="float: none;">{{ ucFirst($timeline->getEntity->actions->session_title) }}</span>
                                            @endif
                                            @if($timeline->entity == 'notes')
                                                <span style="line-height:normal;">{{$timeline->getEntity->note_text}}</span>
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@if($user->cans('edit_job_applications'))
<script src="{{ asset('assets/plugins/jquery-bar-rating-master/dist/jquery.barrating.min.js') }}" type="text/javascript"></script>
<script>
    function employeeResponse(id, type = 'accept') {
        var msg;

        if (type == 'accept') {
            msg = "@lang('errors.acceptSchedule')";
        } else {
            msg = "@lang('errors.refuseSchedule')";
        }
        swal({
            title: "@lang('errors.areYouSure')",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.cancel')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                var url = "{{ route('admin.interview-schedule.response',[':id',':type']) }}";
                url = url.replace(':id', id);
                url = url.replace(':type', type);
                // update values for all tasks
                $.easyAjax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        if (response.status == 'success') {
                            // window.location.reload();
                            location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                        }
                    }
                });
            }
        });
    }
    function loadQna(id){
       
        $.ajax({
            type: 'Post',
            url: "{{ url('/admin/job-applications/get-qna') }}",
            data: {
                id: id,
                '_token': '{{ csrf_token() }}' 
            },
            success: function (response) 
            {
                qna_view.innerHTML = response;
            }
        });
    }
    
    function load(id){
        $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('admin.job-applications.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    }
    function pasteLabel(elm, cid, aid){
        // if (event.keyCode === 13) {
        //     addMe(elm, cid, aid)
        // }
    }
    function addMe(elm, cid, aid){
        if(elm.firstChild.innerHTM != ""){
            $.easyAjax({
                type: 'Post',
                url: "{{ url('/labels/addLabel') }}",
                data: {
                    aid: aid,
                    cid: cid,
                    color: elm.style.background,
                    value: elm.firstChild.innerHTML,
                    '_token': '{{ csrf_token() }}' 
                },
                success: function (response) 
                {
                    if(response.length != 0){
                        pasted_labels.innerHTML += response[0];
                        elm.remove();
                    }
                }
            });
        }
    }
    document.querySelectorAll(".color_code").forEach(function(e){
        e.style.background = e.getAttribute('data-color');
        e.addEventListener('click', function(){
            var color = e.getAttribute('data-color');
            var clone = clonable_label.cloneNode(true);
            clone.removeAttribute('id');
            clone.classList.add('clonable_label');
            clone.style.background = color;
            add_new_labels.appendChild(clone);
        });
    });
    function addLabel(elm, app_id, comp_id){
        var token = '{{ csrf_token() }}';
        var lid = elm.getAttribute('data-id');
        var value = elm.getAttribute('data-value');
        var color = elm.getAttribute('data-color');
        var required_data = {
                    'label_id' :lid, 
                    'label_color' : color,
                    'label_value' : value,
                    'application_id' : app_id,
                    'company_id' : comp_id,
                    '_token':token,
                    'user_id':'{{auth()->user()->id}}',
                    'stage_name': '{{$application->workflow_stage->title}}',
                    };
        if(document.getElementById('lbl_' + elm.getAttribute('data-id'))){
            document.getElementById('lbl_' + elm.getAttribute('data-id')).remove();
            $.easyAjax({
                type: 'Post',
                url: "{{url('/applicant/delete_label')}}",
                data: required_data,
                success: function (response) {
                    elm.lastChild.style.display = 'none';
                    // location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                }
            });
        }else{
            var clone = clonable.cloneNode(true);
            clone.removeAttribute('id');
            clone.id = 'lbl_' + elm.getAttribute('data-id');
            clone.style.background = elm.getAttribute('data-color');
            clone.innerHTML = elm.getAttribute('data-value');
            clone.title = elm.getAttribute('data-value');
            clone.style.margin = '0 3px;'
            label_container.insertBefore(clone, label_container.childNodes[0])                
            $.easyAjax({
                type: 'Post',
                url: "{{url('/applicant/paste_label')}}",
                data: required_data,
                success: function (response) {
                    elm.lastChild.style.display = 'block';
                    // location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                    
                    if(response.id){
                        $(".timeline-container").find('ul li:eq(0)').before(`
                            <li class="list-group-item timeline-item pb-0">
                                <div><i class="fas fa-tags timeline-icon font-15" aria-hidden="true"></i></div>
                                <p class="text-muted font-weight-light" style="position:relative; z-index: 44444; display: inline-block;  ">${response.dateTime}</p><br>
                                <p class="text-muted fw-500 pb-1">${response.comments}</p>
                            </li>
                        `);
                    }
                }
            });

        }
        all_labels.style.display = 'none';
    }
    function addEditQuestionnaire(questionnaire_id, editable = false) {
        if(questionnaire_id){
            var url = "{{ url('admin/questionnaire/get_answers') }}/"+questionnaire_id+'?applicant_id={{$application->id}}' + (editable ? '&editable=0' : '');
            $.ajaxModal('#questionnaireModal', url);
        }
    }
    function printIt(){
        /* Print Preview */
        // remove scroll
        ($(".print_rapper").find('.row').length) ? $(".print_rapper").find('.row').css('overflow','initial') : '';
        // hide elements
        ($(".out-of-print").length) ? $(".out-of-print").addClass('d-none') : '';
        // show elements
        ($(".inside-print").length) ? $(".inside-print").removeClass('d-none') : '';
        var printContents = print_container.innerHTML;
		var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        /* End Print Preview */
        /* Without Print Preview */
        document.body.innerHTML = originalContents;
        ($(".out-of-print").length) ? $(".out-of-print").removeClass('d-none') : '';
        ($(".inside-print").length) ? $(".inside-print").addClass('d-none') : '';
        /* End Without Print Preview */
        location.href = "{{url('admin/job-applications/table-view')}}/?applicant={{$application->id}}";
    }
    $('#example-fontawesome').barrating({
        theme: 'fontawesome-stars',
        showSelectedRating: false,
        onSelect:function(value, text, event){
            if(event !== undefined && value !== ''){
                var url = "{{ route('admin.job-applications.rating-save',':id') }}";
                url = url.replace(':id', {{$application->id}});
                var token = '{{ csrf_token() }}';
                var id = {{$application->id}};
                $.easyAjax({
                    type: 'Post',
                    url: url,
                    container: '#example-fontawesome',
                    data: {'rating':value, '_token':token},
                    success: function (response) {
                        trash_ico.style.display = 'block';
                        $('#example-fontawesome_'+id).barrating('set', value);
                        $('#rating-to-print').html('<b>Rating<b> : '+value);
                        location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                    }
                });
            }

        }
    });
    function removeStars(){

        var url = "{{ route('admin.job-applications.rating-save',':id') }}";
        url = url.replace(':id', {{$application->id}});
        var token = '{{ csrf_token() }}';
        var id = {{$application->id}};
        $.easyAjax({
            type: 'Post',
            url: url,
            container: '#example-fontawesome',
            data: {'rating':0, '_token':token},
            success: function (response) {
                $('#example-fontawesome_'+id).barrating('set', 0);
                $('.br-selected').removeClass('br-selected');
                trash_ico.style.display = 'none';
                location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
            }
        });
 
    }
    @if($application->rating !== null)
        $('#example-fontawesome').barrating('set', {{$application->rating}});       
    @endif

    $('#view-application-details').click(function () {
        var url = "{{ route('admin.job-applications.viewDetails') }}";
        var id = {{$application->id}};
        var token = '{{ csrf_token() }}';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token':token, 'id':id},
            success: function (response) {
                if(response.status == 'success') {
                    $('#email-'+id).html(response.email);
                    $('#phone-'+id).html(response.phone);
                    $('#gender-'+id).html(response.gender);
                    $('#dob-'+id).html(response.dob);
                    $('#country-'+id).html(response.country);
                    $('#state-'+id).html(response.state);
                    $('#city-'+id).html(response.city);
                    if (response.resume !== '') {
                        $('#resume-'+id).html('<a target="_blank" href="'+response.resume+'" class="btn btn-sm btn-primary">@lang('app.view') @lang('modules.jobApplication.resume')</a>');
                    }
                    else {
                        $('#resume-'+id).html('');
                    }
                }
            }
        });
    })

    $('.select2#skills').select2();

    function addSkills(applicationId) {
        let url = "{{ route('admin.job-applications.addSkills', ':id') }}";
        url = url.replace(':id', applicationId);

        $.easyAjax({
            url: url,
            type: 'POST',
            container: '#skills-container',
            data: {
                _token: '{{ csrf_token() }}',
                skills: $('#skills').val()
            },
            success: function (response) {
                if (response.status === 'success') {
                    $("body").removeClass("control-sidebar-slide-open");
                    if (typeof table !== 'undefined') {
                        table._fnDraw();
                    }
                    else {
                        loadData();
                    }
                }
            }
        })

    }

    function sendInterviewInvitation(applicationIdd) {
        if(edit_message.checked){
            open_edit_modal.click();
            application_id = applicationIdd;
            return;
        }
        if(message_body.value != '' && sms_body.value != ''){
            swal({
            title: "@lang('errors.areYouSure')",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.job-application.interviewInvitation') }}";
                    var token = '{{ csrf_token() }}';
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token':token, 'applicationId' : applicationIdd , 'emailMessage' : message_body.value != '' ? encodeURI(encodeURIComponent(message_body.value)) : 'N_A' , 'smsMassage' : sms_body.value != '' ? encodeURI(encodeURIComponent(sms_body.value)) : 'N_A'},
                        success: function (response) {
                            $("body").removeClass("control-sidebar-slide-open");
                            if (response.status === 'success') {
                                if (typeof table !== 'undefined') {
                                    table._fnDraw();
                                }
                                else {
                                    loadData();
                                }
                            }
                        }
                    });
                }
            });
        }else{
            alert("Empty fields are not allowed please edit before send or set default values in Apllicantions options under preference");
        }
    }

    function deleteApplication(applicationId) {
        swal({
            title: "@lang('errors.areYouSure')",
            text: "@lang('errors.deleteWarning')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "@lang('app.delete')",
            cancelButtonText: "@lang('app.cancel')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.job-applications.destroy', ':id') }}";
                url = url.replace(':id', applicationId);

                var token = '{{ csrf_token() }}';

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token':token, '_method': 'DELETE'},
                    success: function (response) {
                        $("body").removeClass("control-sidebar-slide-open");
                        if (response.status === 'success') {
                            if (typeof table !== 'undefined') {
                                table._fnDraw();
                            }
                            else {
                                loadData();
                            }
                        }
                    }
                });
            }
        });
    }

    function archiveApplication(applicationId) {
        swal({
            title: "@lang('errors.areYouSure')",
            text: "@lang('errors.archiveWarning')",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.job-applications.archiveJobApplication', ':id') }}";
                url = url.replace(':id', applicationId);

                var token = '{{ csrf_token() }}';

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token':token},
                    success: function (response) {
                        $("body").removeClass("control-sidebar-slide-open");
                        if (response.status === 'success') {
                            if (typeof table !== 'undefined') {
                                table._fnDraw();
                            }
                            else {
                                loadData();
                            }
                        }
                    }
                });
            }
        });
    }

    // Add Note on Blur
    $('body').on('click','#add_update_note',function () {
        var $this = $(this);
        if($.trim($("#add_update_note_value").val())){
            $("#add_update_note_value").css('border', '1px solid #ced4da');
        }
        else{
            $("#add_update_note_value").css('border', '1px solid #e46a76');
            return;
        }
        $this.prop('disabled', true);
        if($.trim($("#add_update_note_value").val()))
        var url = "{{ route('admin.applicant-note.store') }}";
        var id = {{$application->id}};
        var note = $("#add_update_note_value").val();
        var token = '{{ csrf_token() }}';
        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token':token, 'id':id, 'note': note},
            success: function (response) {
                if(response.status == 'success') {
                    // $('#applicant-notes').html(response.view);
                    location.href = "{{ route('admin.job-applications.table') . '?applicant=' . $application->id }}";
                }
            }
        });
    });

    function onChangeApplicantStatus(element) {
        var status_id = $(element).val();
        var applicant_id = $(element).data('applicant');

        $.easyAjax({
            type: 'POST',
            url: '{{ route("admin.job-application.updateStatus") }}',
            data: {'_token':'{{ csrf_token() }}', 'applicant_id':applicant_id, 'status_id': status_id},
            success: function (response) {
                if(response.status == 'success') {
                    var box = $(`[data-application-id=${applicant_id}]`).fadeOut(500);
                    setTimeout(() => {
                        $(`[data-column-id=${status_id}]`).find('.lobipanel').parent().prepend(box);
                        box.fadeIn();
                        // location.reload();
                        location.href = "{{url('public/admin/job-applications/table-view')}}?applicant={{$application->id}}";
                    }, 550);
                }
            }
        });
    }

    function onChangeApplicantStage(element) {
        var workflow_stage_id = $(element).val();
        var applicant_id = '{{$application->id}}';

        $.easyAjax({
            type: 'POST',
            url: '{{ route("admin.job-application.updateStage") }}',
            data: {'_token':'{{ csrf_token() }}', 'applicant_id':applicant_id, 'workflow_stage_id': workflow_stage_id},
            success: function (response) {
                if(response.status == 'success') {
                    var box = $(`[data-application-id=${applicant_id}]`).fadeOut(500);
                    setTimeout(() => {
                        $(`[data-column-id=${workflow_stage_id}]`).find('.lobipanel').parent().prepend(box);
                        box.fadeIn();
                            if(response.message == "Applicant's workflow stage updated!"){
                                // location.reload();
                                location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                            }
                    }, 550);
                }else{
                    setTimeout(function(){
                        location.href = "{{url('admin/job-applications/table-view')}}?applicant={{$application->id}}";
                    }, 2000);
                }
            },
            error: function (response) {
                
            }
        });
    }

    $('body').on('click','.change_hiring_status',function(){
        var $this = $(this);
        var applicant_id = '{{$application->id}}';
        var status = $this.attr('hiring_status');
        var workflow_stage_id = $this.attr('stage_id');
        // $this.prop("disabled", true);
        // $this.css({
        //     "opacity": ".2",
        //     "cursor": "progress"
        // });
        var mailToCandidate = 0;
        swal({
            title: `Do you want to really ${(status=='hired') ? 'Hire' : 'Reject'} this Applicant?`,
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#0c19dd",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true,
            closeOnEsc: false,
            closeOnClickOutside: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.easyAjax({
                    type: 'POST',
                    url: '{{ route("admin.job-application.updateHiringStatus") }}',
                    data: {'_token':'{{ csrf_token() }}', 'applicant_id':applicant_id, 'hiring_status': status, 'mailToCandidate': mailToCandidate},
                    success: function (response) {
                        if(response.status == 'success') {
                            var box = $(`[data-application-id=${applicant_id}]`).fadeOut(500);
                            setTimeout(() => {
                                $(`[data-column-id=${workflow_stage_id}]`).find('.lobipanel').parent().prepend(box);
                                box.fadeIn();
                                location.reload();
                            }, 550);
                        }
                    }
                });
            }
        });
    });

    // schedule detail
        var getScheduleDetail = function (id) {
            var url = "{{ route('admin.interview-schedule.show', ':id')}}?profile=1";
            url = url.replace(':id', id);
            $('#modelHeading').html('Schedule');
            $.ajaxModal('#scheduleDetailModal', url);
        }
        function moveNextStage() {
            $("#move_stage_dropdown").find('select').val($("#move_stage_dropdown").find('select').find(':selected').next().attr('value'));
            onChangeApplicantStage($("#move_stage_dropdown").find('select'));
        }
    //Hide Move to Stage
    if($('a[href="javascript:moveNextStage()"]').length){
        $("#move_stage_dropdown").addClass('d-none');
    }
    else{
        $("#move_stage_dropdown").removeClass('d-none');
    }

</script>
@endif
@if(!is_null($application->skype_id))
    <script src="https://swc.cdn.skype.com/sdk/v1/sdk.min.js"></script>
@endif
