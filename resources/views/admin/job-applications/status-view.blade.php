<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-bar-rating-master/dist/themes/fontawesome-stars.css') }}">
<style>

    .right-panel-box{
        overflow-x: scroll;
        max-height: 100vh;
    }
    .resume-button{
        text-align: center; margin-top: 7px;
    }
    .bold-text {
        font-size: 23px;
        font-weight: bold;
    }
    .rounded-circle-text{
        background-color: #93c954;
        height: 60px;
        width: 60px;
        padding-top: 10px;
        color: #fff;
        margin: 0 auto;
        font-size: 2em;
        font-weight: bold;      
    }
    .line-break{
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .label {
        min-width: 100px;
        white-space: nowrap;
        max-width: 200px;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 5px;
        font-size: 15px;
        font-weight: bold;
        color: white;
        display: inline-block;
        border-radius: 3px;
        text-align: center;
        cursor: default;
        margin: 0 3px;
    }
    #addLabels {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        font-size: 15px;
        font-weight: bold;
        width: 50px;
        border-radius: 3px;
        text-align: center;
        cursor: pointer;
        background: #eceaea;
        display: inline-block;
        padding: 5px;
    }
    .lbl {
        margin: 5px 0;
        border-radius: 3px;
        width: 100%;
        display: flex;
        cursor: pointer;
        border-radius: 3px;
    }
    .lbl_txt {
        white-space: nowrap !important;
        overflow: hidden !important;
        text-overflow: ellipsis !important;
        color: #fff;
        width: 100%;
        display: block;
        padding: 5px 5px 5px 0;
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
    }
    .lbl_txt:hover, .lbl_pointer:hover ~ * {
        opacity: .6;
    }

    .lbl_pointer {
        padding: 6px;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    #all_labels {
        background: #f9f9f9;
        width: 120%;
        white-space: nowrap;
        position: absolute;
        z-index: 4444;
        display: none;
        padding: 5px;
        right: -120%;
        border: 1px solid lightgrey;
    }
    #new_label {
        width: 100%;
        border-top: 1px solid #D3D3D3;
    }
    .lbl_btn {
        width: 93%;
        background: #e2e2e2;
        display: inline-block;
        text-align: center;
        margin: 5px 0;
        padding: 10px 0;
        font-size: 15px;
        margin-left: 3.33%;
        cursor: pointer;
        border-radius: 3px;
    }
    #pasted_labels {
        width: 93%;
        margin: 3.33%;

    }
    .tick {
        position: absolute;
        margin: 10px;
        color: #f3f0f0;
        right: 20px;
    }
    .color_code {
        padding: 20px;
        margin:5px;
        cursor: pointer;
        border-radius: 50%;
    }
    #pic_color  {
        display: inline-flex;
        margin: auto;
    }
    #color_box {
        border-top: 1px solid #D3D3D3;
        text-align: center
    }
    #clonable, #clonable_label {
        display: none !important;
    }
    #add_new_labels > div{
        width: 92%;
        position: relative;
        padding: 5px 10px;
        border-radius: 5px;
        color:ivory;
        display: inline-grid;
        margin: 0 0 5px 4%;
    }
    #add_new_labels > div > span {
        position: absolute;
        right: 5px;
        display: none;
    }
    .ok, .nok {
        cursor: pointer;
        margin-left: 5px;
    }
    #add_new_labels > div:hover span {
        display: block;
    }
    #add_new_labels > div > div {
        outline: none;
        max-width: 100%;
    }
    #add_new_labels {
        display: grid;
    }
    #priint {
        color: white;
        margin-right: 10px;;
    }
    .qs::-webkit-scrollbar {
        display: none;
    }
    .qs {
        -ms-overflow-style: none;  /* IE and Edge */
        scrollbar-width: none;  /* Firefox */
    }
</style>

<div class="rpanel-title"> <div class="d-inline right-side-toggle" style="cursor:pointer;"><i class="ti-angle-left"></i>@lang('modules.jobApplication.backToAllApplicants')</div>
        <button hidden onclick="printIt()" accesskey="p"></button>
    <span class="right-side-toggle">CLOSE</span> 
    <a href="javascript:;" onclick="printIt()" id="priint" class="p-hide pull-right" title="Shortcut: Alt + P"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
</div>
<div class="r-panel-body p-3" id="print_container">
    <style>
        @media print {
            .p-hide {
                display: none !important;
            }
            .form-control, .select2-container--default .select2-selection--multiple, .select2-selection__choice {
                border-color: transparent !important;
                background: white !important;
                color: black !important;
            }
            .select2-selection__choice__remove {
                color: transparent !important;
                display: block !important;
            }
            .print_rapper{
                width: 21cm;
                min-height: 29.7cm;
                padding: 1cm;
                margin: 1cm auto;
                margin-top: 2.5cm;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            @page {
            size: A4;
            margin: 0;
            }
        }
    </style>
    
    <div class="print_rapper">
        <div class="row font-12 pb-3">
            @if(isset($before))
            <div class="col-sm-6 out-of-print">
                <div class="navv" onclick="load('{{ $before->id  }}')">
                    <i class="fa fa-arrow-left"></i>
                    <span class="text-muted" style="cursor:pointer;"><b>Previous:</b> {{ $before->full_name  }}</span>
                </div>
            </div>
            @endif
            @if(isset($after))
            <div class="col-sm-6 text-right out-of-print" >
                <div class="navv" onclick="load('{{ $after->id  }}')">
                    <span class="text-muted" style="cursor:pointer;"><b>Next:</b> {{ $after->full_name  }}</span>
                    <i class="fa fa-arrow-right"></i>
                </div>
            </div>
            @endif
        </div>
    <div class="row font-12" style="overflow-x: scroll; max-height: 90vh;">
        <div class="col-md-4 text-center">
            <div class="rounded-circle rounded-circle-text p-hide">{{ucwords($application->full_name ? $application->full_name[0] : '')}}</div>
            <div class="col-sm-12">
                <p class="text-muted bold-text">{{ ucwords($application->full_name) }}</p>
            </div>
            <div class="col-sm-12">
                <a href="mailto:{{ $application->email }}" class="text-muted" id="email-{{ $application->id }}">{{ $application->email }} </a><a href="javascript:void(0);" class="text-copier" onmouseover="tagOver()" onmouseout="tagOut()"><i class='fa fa-copy'></i><span class="d-none">{{ $application->email }}</span></a>
            </div>

            <div class="col-sm-12">
                <p class="text-muted" id="phone-{{ $application->id }}" style="margin-bottom: 0;">{{ $application->phone }} <a href="javascript:void(0);" class="text-copier" onmouseover="tagOver()" onmouseout="tagOut()"><i class='fa fa-copy'></i><span class="d-none">{{ $application->phone }}</span></a></p>
                @if((\App\SmsLog::where('number', '=', $application->phone )->orderBy('id', 'desc')->first()->status ?? '') == 'bounce')
                <span style="color:red;font-size:10px;">
                    Phone Not Accepting Text Message<br />
                    (Could be Service or a Landline Phone)
                </span>
                @endif
            </div>
            <p class="text-muted font-weight-bold mt-3">
            {{$application->qualified}}
            </p>
            {{--<div class="col-sm-6">--}}
                <p class="text-muted resume-button" id="resume-{{ $application->id }}">
                    @if ($application->resume_url)
                        <a target="_blank" href="{{ $application->resume_url }}" class="btn btn-sm btn-primary p-hide" >@lang('app.view') @lang('modules.jobApplication.resume')</a>
                    @endif
                </p>
            {{--</div>--}}
            @if($application->deleted_at == null)
            <div class="text-muted resume-button">
                <label class="p-hide" style="position:relative"> </label>
                <a href="javascript:" onclick="sms_number = '{{$application->phone}}'; app_id = '{{$application->id}}'" data-toggle="modal" data-target="#exampleModalTwo" class="btn btn-sm btn-success p-hide btn-block" style="background-color: deepskyblue;">
                Send Personal Text Message
                </a>                        
            </div>
            @endif
            @if($user->cans('edit_job_applications'))
                <h4 class="p-hide">Rate Applicant</h4>
                <div class="stars stars-example-fontawesome text-center" style="display: inline-flex">
                    
                    <select id="example-fontawesome" name="rating" autocomplete="off">
                        <option value=""></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <i class="fas fa-trash-alt out-of-print" id="trash_ico" style="color:#dc3545; margin: 3px 0 0 5px; cursor:pointer; {{$application->rating ? '' : 'display:none;'}}" onclick="removeStars()"></i>  
                    <p class="text-muted d-none inside-print" id="rating-to-print">
                        <b>Rating</b> : {{$application->rating ?? 0}}
                    </p>    
                </div>
            @endif

            @if($application->status->status == 'hired' && is_null($application->onboard) && false)
                <p class="text-muted resume-button">
                    <a href="{{ route('admin.job-onboard.create') }}?id={{$application->id}}"
                        class="btn btn-sm btn-success p-hide">@lang('app.startOnboard')</a>
                </p>
            @endif
            @php
            $interviewStatus = $statuses->filter(function($s) {
                return $s->status == 'interview';
            })->values()->first();
            @endphp

            @if ($user->cans('delete_job_applications'))
            @if($application->deleted_at == null)
                @if($interviewStatus != null)
                    @if($interviewStatus->id > $application->status_id)
                    <div class="text-muted resume-button">
                        <label class="p-hide" style="position:relative"><input type="checkbox" id="edit_message" style="position:initial"> @lang('app.edit') @lang('menu.messages') @lang('menu.before') @lang('menu.sending')</label>
                        <a href="javascript:sendInterviewInvitation({{ $application->id }})" class="btn btn-sm btn-success p-hide btn-block">
                            @if($has_interview_invited || $application->is_auto_informed) @lang('modules.jobApplication.resendInteviewInvite')  @else @lang('modules.jobApplication.sendInteviewInvite') @endif 
                        </a>
                        <input type="hidden" data-toggle="modal" data-target="#exampleModal" id="open_edit_modal">
                        {{-- @if($has_interview_invited)<span class="text-success">Invited</span>@endif --}}
                    </div>
                    @endif
                @endif
            @endif
                {{-- <div class="text-muted resume-button">
                    <a href="javascript:archiveApplication({{ $application->id }})" class="btn btn-sm btn-info p-hide">
                        @lang('modules.jobApplication.archiveApplication')
                    </a>
                </div> --}}
                @if($questionnaire->name)
                <div class="text-muted resume-button">
                    <a href="javascript:addEditQuestionnaire({{$questionnaire->id}})" class="btn btn-sm btn-info p-hide btn-block line-break" title="{{ucFirst($questionnaire->name)}}">
                        {{ucFirst($questionnaire->name)}}
                    </a>
                </div>
                @endif
                @if($application->deleted_at == null)
                <div class="text-muted resume-button">
                    <a href="javascript:deleteApplication({{ $application->id }})" class="btn btn-sm btn-danger p-hide btn-block">
                        Archive Application
                    </a>
                </div>
                @endif
            @endif
            <div class="col-10 mx-auto out-of-print">
                <div class="form-group mt-2">
                    <select class="select2 form-control applicant_status_id" onchange="onChangeApplicantStatus(this)" data-applicant="{{$application->id}}">
                        @foreach($statuses as $status)
                            <option value="{{ $status->id }}" @if($application->status_id == $status->id) selected @endif>{{ ucwords($status->status) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div >
                <h4>Labels: </h4>
                <div style="display:inline-flex;" style="margin:10px 0;">
                    <div id="clonable" class="label"></div>
                    <div id="label_container">
                        @foreach($application->labels as $label)  
                            <?php $app = \App\ApplicationLabel::find($label->label_id); ?>    
                            <div id="lbl_{{$label->label_id}}" class="label" style="background:{{$app->label_color}};" title="{{$app->label_value}}">{{$app->label_value}}</div>
                        @endforeach
                        <div id="addLabels" class="p-hide" onclick="all_labels.style.display = all_labels.style.display == 'block' ? 'none' : 'block';">+</div>
                    </div>
                    <div id="all_labels" class="p-hide" style="z-index:44444">
                        <h5>AVAILABLE LABELS</h5>
                        <div id="pasted_labels">
                                <?php $lids = \App\LabelTransition::where('application_id','=',$application->id)->pluck('label_id')->toArray(); ?>
                                
                            @foreach($labels as $label)
                                <span class="lbl" data-applicant_id="{{$application->id}}"  onclick="addLabel(this, {{$application->id}}, {{$application->company_id}})" data-id='{{$label->id}}' data-value='{{$label->label_value}}' data-color='{{$label->label_color}}' ><span class="lbl_pointer" style="background: {{$label->label_color}};"></span><span class="lbl_txt" style="background: {{$label->label_color}};">{{$label->label_value}}</span><i class="fas fa-check tick" style="display: {{ in_array($label->id, $lids) ? 'block' : 'none' }};"></i></span>
                            @endforeach
                        </div>
                        <div id="add_new_labels">
                            <h5>NEW LABELS</h5>
                            <div id="clonable_label"><div contenteditable="true" onkeydown="pasteLabel(this.parentNode, {{$application->company_id}}, {{$application->id}})"></div><span><i class="fas fa-check ok" onclick="addMe(this.parentNode.parentNode, {{$application->company_id}}, {{$application->id}})"></i><i class="fas fa-close nok" onclick="this.parentNode.parentNode.remove()"></i></span></div>
                        </div>
                        <div id="color_box">
                            <div id="pic_color">
                                <div class="color_code" data-color='#4caf50'></div>
                                <div class="color_code" data-color='#ffc107'></div>
                                <div class="color_code" data-color='#ffeb3b'></div>
                                <div class="color_code" data-color='#f44336'></div>
                                <div class="color_code" data-color='#00bcd4'></div>
                            </div>
                        </div>
                        <div id="new_label">
                            <span class="lbl_btn" onclick="all_labels.style.display = 'none'; $('.clonable_label').remove();">Cancel</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <strong>@lang('modules.jobApplication.appliedAt')</strong><br>
                <p class="text-muted">
                    @if ($timezone === '')
                        {{ $application->created_at->timezone('GMT')->format('M d,  Y - h:i A') }}
                    @else
                        {{ $application->created_at->timezone($timezone)->format('M d,  Y - h:i A') }}
                    @endif
                </p>
            </div>
            @if(!is_null($application->schedule) && $application->schedule->status != 'canceled')
            <hr>
            @if($application->schedule->status == 'pending')
                <h5>@lang('modules.interviewSchedule.scheduleDetail')</h5>
                <div class="col-sm-12">
                    <strong>@lang('modules.interviewSchedule.scheduleDate')</strong><br>
                    <p class="text-muted">
                            
                            @if ($timezone === '')
                                {{ $application->schedule->schedule_date->timezone('GMT')->format('M d') }}
                            @else
                                {{ \Carbon\Carbon::parse($application->schedule->schedule_date)->setTimezone($timezone)->format('M d, Y') ?? '' }}
                            @endif
                            {{explode(" - ",$application->schedule->slot_timing)[0]}}
                    </p>
                </div>
                <div class="row p-hide">
                    <div class="col-sm-6">
                        <strong>@lang('modules.interviewSchedule.assignedEmployee')</strong><br>
                    </div>
                    <div class="col-sm-6">
                        <strong>@lang('modules.interviewSchedule.employeeResponse')</strong><br>
                    </div>
                    @forelse($application->schedule->employee as $key => $emp )
                        <div class="col-sm-6">
                            <p class="text-muted">{{ ucwords($emp->user->name) }}</p>
                        </div>

                        <div class="col-sm-6">
                            @if($emp->user_accept_status == 'accept')
                                <label class="badge badge-success">{{ ucwords($emp->user_accept_status) }}ed</label>
                            @elseif($emp->user_accept_status == 'refuse')
                                <label class="badge badge-danger">{{ ucwords($emp->user_accept_status) }}</label>
                            @else
                                <label class="badge badge-warning">{{ ucwords($emp->user_accept_status) }}</label>
                            @endif
                        </div>
                    @break
                    @empty
                    @endforelse
                </div>
                @endif
            @endif
        </div>

        <div class="col-md-8 right-panel-box qs">

            @if($application->job->location)
            <div class="col-sm-12">
                <strong>@lang('modules.jobApplication.appliedFor')</strong><br>
                <p class="text-muted">{{ ucwords($application->job->title) }}</p>
            </div>
            @endif
            


            <div class="col-sm-12">
                <div class="row">
                    @if (!is_null($application->gender))
                        <div class="col-sm-12 col-md-4">
                            <strong>@lang('app.gender')</strong><br>
                            <p class="text-muted" id="gender-{{ $application->id }}">{{ ucfirst($application->gender) }}</p>
                        </div>
                    @endif
                    @if (!is_null($application->dob))
                        <div class="col-sm-12 col-md-4">
                            <strong>@lang('app.dob')</strong><br>
                            <p class="text-muted" id="dob-{{ $application->id }}">{{ $application->dob->format('M d, Y') }}</p>
                        </div>
                    @endif
                </div>
            </div>

            @if (!is_null($application->country))
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col">
                            <strong>@lang('app.country')</strong><br>
                            <p class="text-muted" id="country-{{ $application->id }}">{{ $application->country }}</p>
                        </div>
                        <div class="col">
                            <strong>@lang('app.state')</strong><br>
                            <p class="text-muted" id="state-{{ $application->id }}">{{ $application->state }}</p>
                        </div>
                        <div class="col">
                            <strong>@lang('app.city')</strong><br>
                            <p class="text-muted" id="city-{{ $application->id }}">{{ $application->city }}</p>
                        </div>
                    </div>
                </div>
            @endif


            <div class="col-sm-12">
                <h4>Questions And Answers</h4>
                @forelse($answers as $answer)
                <div class="{{ $answer->question->is_qualifiable() ? ($answer->qualified() ? 'text-success' : 'text-danger') : '' }}">
                        <strong>
                            {{$answer->question->question}}
                        </strong><br>
                        <p class="text-muted">{{ ucfirst($answer->answer)}}</p>
                    </div>
                @empty
                @endforelse
            </div>
            @if($answers->count() == 0)
            <div class="col-sm-12">
                @if($application->facebook_data->count() > 0)
                    @foreach($application->facebook_data as $fb_data)
                        @foreach($fb_data->data as $_question => $_answer)
                            <strong>
                            {{ ucwords(str_replace('_', ' ', $_question)) }}
                            </strong><br>
                            <p class="text-muted">{{ str_replace('_', ' ', $_answer) }}</p>
                        @endforeach
                    @endforeach
                @endif
            </div>
            @endif
            

            @if(!is_null($application->schedule) && $application->schedule->status == 'canceled')
                <div class="col-sm-6">
                    <label class="badge badge-danger">User Canceled</label>
                </div>
            @endif

            {{--@if(isset($application->schedule->comments) == 'interview' && count($application->schedule->comments) > 0)
                <hr>

                <h5>@lang('modules.interviewSchedule.comments')</h5>
                @forelse($application->schedule->comments as $key => $comment )

                    <div class="col-sm-12">
                        <p class="text-muted">{{ $comment->comment }}</p>
                    </div>
                    @empty
                @endforelse

            @endif--}}
            <div class="col-sm-12">
                <p class="text-muted">
                    @if(!is_null($application->skype_id))
                        <span class="skype-button rounded" data-contact-id="live:{{$application->skype_id}}" data-text="Call"></span>
                    @endif
                </p>
            </div>
            <div class="row">
                @if($user->cans('add_schedule') && $application->status->status == 'interview' && (is_null($application->schedule) || $application->schedule->status == 'canceled'))
                    <div class="col-sm-6">
                        <p class="text-muted">
                            <a onclick="createSchedule('{{$application->id}}')" href="javascript:;" class="btn btn-sm btn-info p-hide">@lang('modules.interviewSchedule.scheduleInterview')</a>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @if ($user->cans('edit_job_applications'))
            <div class="col-12" id="skills-container out-of-print">
                <hr>
                <div class="col-sm-12 mb-3 out-of-print">
                    <h5>@lang('modules.jobApplication.skills')</h5>
                </div>
                <div class="form-group mb-2">
                    <select name="skills[]" id="skills" class="form-control select2 custom-select" multiple>
                        @forelse ($skills as $skill)
                            <option @if (!is_null($application->skills) && in_array($skill->id, $application->skills)) selected @endif value="{{ $skill->id }}">{{ $skill->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <a href="javascript:addSkills({{ $application->id}});" id="add-skills" class="btn btn-sm btn-outline-success p-hide">
                    @if (!is_null($application->skills) && sizeof($application->skills) > 0)
                        @lang('modules.jobApplication.updateSkills')
                    @else
                        @lang('modules.jobApplication.saveSkills')
                    @endif
                </a>
            </div>
        @endif
        @if($questionnaire->name && $questionnaire->custom_field_values->count())
        <div class="col-12 d-none" id="questionnaire_print">
            <div class="row">
            <hr>
            <div class="col-sm-12 mb-3">
                <h3>{{ucFirst($questionnaire->name)}}</h3>
            </div>
            <div id="questionnaire_answers">
            @foreach($questionnaire->custom_field_values as $field)
            <div class="col-sm-12">
                <strong>{{  $field->question->question }}</strong><br>
                <p class="text-muted" id="a-{{$field->question_id}}">{{ $field->question->applicantAnswer($application->id)->answer }}</p>
            </div>
            @endforeach
            </div>
            </div>
        </div>
        @endif                
        <div class="col-12">
        <div class="row">
            <hr>
            <div class="col-sm-12 mb-3" style="padding-top: 10px;">
                <h5>@lang('modules.jobApplication.internalNotes')</h5>
            </div>

            <div id="applicant-notes" class="col-sm-12">
                <ul class="list-unstyled">
                @foreach($application->notes as $key => $notes )
                    <li class="media mb-3" id="note-{{ $notes->id }}">
                        <div class="media-body">
                            <h6 class="mt-0 mb-1">{{ ucwords($notes->user->name) }} 
                                <span class="pull-right font-italic font-weight-light p-hide"><small> {{ $notes->created_at->diffForHumans() }} </small>
                                    @if($user->cans('edit_job_applications'))
                                        <a href="javascript:;" class="edit-note" data-note-id="{{ $notes->id }}"><i class="fa fa-edit ml-2"></i></a>
                                        <a href="javascript:;" class="delete-note" data-note-id="{{ $notes->id }}"><i class="fa fa-trash ml-1 text-danger"></i></a>
                                    @endif
                                </span>
                            </h6>
                            <small class="note-text">{{ ucfirst($notes->note_text) }}</small>
                            <div class="note-textarea"></div>
                        </div>
                    </li>
                @endforeach
                </ul>
            </div>

            @if($user->cans('edit_job_applications'))
                <div class="col-sm-12">
                    <div class="form-group mb-2">
                        <textarea name="note" id="note_text" cols="30" rows="2" class="form-control"></textarea>
                    </div>
                    <a href="javascript:;" id="add-note" class="btn btn-sm btn-outline-primary p-hide">@lang('modules.jobApplication.addNote')</a>
                </div>
            @endif
            <div class="p-hide container" style="margin: 20px; 0;">
                <h2>Status History</h2>
                <table class="table">
                <tr>
                    <th>Status</th>
                    <th>Time</th>
                    <th>Date</th>
                </tr>
                @foreach($application->history as $history)
                    @if(isset(\App\ApplicationStatus::find($history->current_value)->status))
                    <tr>
                        <td>{{$history->current_value ? ucfirst(\App\ApplicationStatus::find($history->current_value)->status ?? '-') : '-' }}</td>
                        <td>{{ \Carbon\Carbon::parse($history->updated_at)->setTimeZone($history->company->timezone ?? 'GMT')->format('h:i:s A') }}</td>
                        <td>{{ \Carbon\Carbon::parse($history->updated_at)->setTimeZone($history->company->timezone ?? 'GMT')->format('M d, Y') }}</td>
                    </tr>
                    @endif
                @endforeach
                </table>
            </div>

        </div>
    </div>
    </div>
</div>
@if($user->cans('edit_job_applications'))
<script src="{{ asset('assets/plugins/jquery-bar-rating-master/dist/jquery.barrating.min.js') }}" type="text/javascript"></script>
<script>
    function pasteLabel(elm, cid, aid){
        // if (event.keyCode === 13) {
        //     addMe(elm, cid, aid)
        // }
    }
    function addMe(elm, cid, aid){
        if(elm.firstChild.innerHTM != ""){
            $.easyAjax({
                type: 'Post',
                url: "{{ url('/labels/addLabel') }}",
                data: {
                    aid: aid,
                    cid: cid,
                    color: elm.style.background,
                    value: elm.firstChild.innerHTML,
                    '_token': '{{ csrf_token() }}' 
                },
                success: function (response) 
                {
                    if(response.length != 0){
                        pasted_labels.innerHTML += response[0];
                        elm.remove();
                    }
                }
            });
        }
    }
    document.querySelectorAll(".color_code").forEach(function(e){
        e.style.background = e.getAttribute('data-color');
        e.addEventListener('click', function(){
            var color = e.getAttribute('data-color');
            var clone = clonable_label.cloneNode(true);
            clone.removeAttribute('id');
            clone.classList.add('clonable_label');
            clone.style.background = color;
            add_new_labels.appendChild(clone);
        });
    });
    function addLabel(elm, app_id, comp_id){
        var token = '{{ csrf_token() }}';
        var lid = elm.getAttribute('data-id');
        var value = elm.getAttribute('data-value');
        var color = elm.getAttribute('data-color');
        var required_data = {
                    'label_id' :lid, 
                    'label_color' : color,
                    'label_value' : value,
                    'application_id' : app_id,
                    'company_id' : comp_id,
                    '_token':token,
                    };
        if(document.getElementById('lbl_' + elm.getAttribute('data-id'))){
            document.getElementById('lbl_' + elm.getAttribute('data-id')).remove();
            $.easyAjax({
                type: 'Post',
                url: "{{url('/applicant/delete_label')}}",
                data: required_data,
                success: function (response) {
                    elm.lastChild.style.display = 'none';
                    console.log(response);
                }
            });
        }else{
            var clone = clonable.cloneNode(true);
            clone.removeAttribute('id');
            clone.id = 'lbl_' + elm.getAttribute('data-id');
            clone.style.background = elm.getAttribute('data-color');
            clone.innerHTML = elm.getAttribute('data-value');
            clone.title = elm.getAttribute('data-value');
            clone.style.margin = '0 3px;'
            label_container.insertBefore(clone, label_container.childNodes[0])                
            $.easyAjax({
                type: 'Post',
                url: "{{url('/applicant/paste_label')}}",
                data: required_data,
                success: function (response) {
                    elm.lastChild.style.display = 'block';
                    console.log(response);
                }
            });

        }
        all_labels.style.display = 'none';
    }
    function addEditQuestionnaire(questionnaire_id) {
        if(questionnaire_id){
            var url = "{{ url('admin/questionnaire/get_answers') }}/"+questionnaire_id+'?applicant_id={{$application->id}}';
            $.ajaxModal('#questionnaireModal', url);
        }
    }
    function printIt(){
        /* Print Preview */
        // remove scroll
        ($(".print_rapper").find('.row:eq(1)').length) ? $(".print_rapper").find('.row:eq(1)').css('overflow','initial') : '';
        // hide elements
        ($(".out-of-print").length) ? $(".out-of-print").addClass('d-none') : '';
        // show elements
        ($("#questionnaire_print").length) ? $("#questionnaire_print").removeClass('d-none') : '';
        ($(".inside-print").length) ? $(".inside-print").removeClass('d-none') : '';
        var printContents = print_container.innerHTML;
		var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        /* End Print Preview */
        /* Without Print Preview */
        document.body.innerHTML = originalContents;
        ($(".out-of-print").length) ? $(".out-of-print").removeClass('d-none') : '';
        ($("#questionnaire_print").length) ? $("#questionnaire_print").addClass('d-none') : '';
        ($(".inside-print").length) ? $(".inside-print").addClass('d-none') : '';
        /* End Without Print Preview */
        location.reload();
    }
    $('#example-fontawesome').barrating({
        theme: 'fontawesome-stars',
        showSelectedRating: false,
        onSelect:function(value, text, event){
            if(event !== undefined && value !== ''){
                var url = "{{ route('admin.job-applications.rating-save',':id') }}";
                url = url.replace(':id', {{$application->id}});
                var token = '{{ csrf_token() }}';
                var id = {{$application->id}};
                $.easyAjax({
                    type: 'Post',
                    url: url,
                    container: '#example-fontawesome',
                    data: {'rating':value, '_token':token},
                    success: function (response) {
                        trash_ico.style.display = 'block';
                        $('#example-fontawesome_'+id).barrating('set', value);
                        $('#rating-to-print').html('<b>Rating<b> : '+value);
                    }
                });
            }

        }
    });
    function removeStars(){

        var url = "{{ route('admin.job-applications.rating-save',':id') }}";
        url = url.replace(':id', {{$application->id}});
        var token = '{{ csrf_token() }}';
        var id = {{$application->id}};
        $.easyAjax({
            type: 'Post',
            url: url,
            container: '#example-fontawesome',
            data: {'rating':0, '_token':token},
            success: function (response) {
                $('#example-fontawesome_'+id).barrating('set', 0);
                $('.br-selected').removeClass('br-selected');
                trash_ico.style.display = 'none';
            }
        });
 
    }
    @if($application->rating !== null)
        $('#example-fontawesome').barrating('set', {{$application->rating}});       
    @endif

    $('#view-application-details').click(function () {
        var url = "{{ route('admin.job-applications.viewDetails') }}";
        var id = {{$application->id}};
        var token = '{{ csrf_token() }}';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token':token, 'id':id},
            success: function (response) {
                if(response.status == 'success') {
                    $('#email-'+id).html(response.email);
                    $('#phone-'+id).html(response.phone);
                    $('#gender-'+id).html(response.gender);
                    $('#dob-'+id).html(response.dob);
                    $('#country-'+id).html(response.country);
                    $('#state-'+id).html(response.state);
                    $('#city-'+id).html(response.city);
                    if (response.resume !== '') {
                        $('#resume-'+id).html('<a target="_blank" href="'+response.resume+'" class="btn btn-sm btn-primary">@lang('app.view') @lang('modules.jobApplication.resume')</a>');
                    }
                    else {
                        $('#resume-'+id).html('');
                    }
                }
            }
        });
    })

    $('.select2#skills').select2();

    function addSkills(applicationId) {
        let url = "{{ route('admin.job-applications.addSkills', ':id') }}";
        url = url.replace(':id', applicationId);

        $.easyAjax({
            url: url,
            type: 'POST',
            container: '#skills-container',
            data: {
                _token: '{{ csrf_token() }}',
                skills: $('#skills').val()
            },
            success: function (response) {
                if (response.status === 'success') {
                    $("body").removeClass("control-sidebar-slide-open");
                    if (typeof table !== 'undefined') {
                        table._fnDraw();
                    }
                    else {
                        loadData();
                    }
                }
            }
        })

    }

    function sendInterviewInvitation(applicationIdd) {
        if(edit_message.checked){
            open_edit_modal.click();
            application_id = applicationIdd;
            return;
        }
        if(message_body.value != '' && sms_body.value != ''){
            swal({
            title: "@lang('errors.areYouSure')",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.job-application.interviewInvitation') }}";
                    var token = '{{ csrf_token() }}';

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token':token, 'applicationId' : applicationIdd , 'emailMessage' : message_body.value != '' ? encodeURI(encodeURIComponent(message_body.value)) : 'N_A' , 'smsMassage' : sms_body.value != '' ? encodeURI(encodeURIComponent(sms_body.value)) : 'N_A'},
                        success: function (response) {
                            $("body").removeClass("control-sidebar-slide-open");
                            if (response.status === 'success') {
                                if (typeof table !== 'undefined') {
                                    table._fnDraw();
                                }
                                else {
                                    loadData();
                                }
                            }
                        }
                    });
                }
            });
        }else{
            alert("Empty fields are not allowed please edit before send or set default values in Apllicantions options under preference");
        }
    }

    function deleteApplication(applicationId) {
        swal({
            title: "@lang('errors.areYouSure')",
            text: "Do you want to archive?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Archive",
            cancelButtonText: "@lang('app.cancel')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.job-applications.destroy', ':id') }}";
                url = url.replace(':id', applicationId);

                var token = '{{ csrf_token() }}';

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token':token, '_method': 'DELETE'},
                    success: function (response) {
                        $("body").removeClass("control-sidebar-slide-open");
                        if (response.status === 'success') {
                            if (typeof table !== 'undefined') {
                                table._fnDraw();
                            }
                            else {
                                loadData();
                            }
                        }
                    }
                });
            }
        });
    }

    function archiveApplication(applicationId) {
        swal({
            title: "@lang('errors.areYouSure')",
            text: "@lang('errors.archiveWarning')",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.no')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.job-applications.archiveJobApplication', ':id') }}";
                url = url.replace(':id', applicationId);

                var token = '{{ csrf_token() }}';

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token':token},
                    success: function (response) {
                        $("body").removeClass("control-sidebar-slide-open");
                        if (response.status === 'success') {
                            if (typeof table !== 'undefined') {
                                table._fnDraw();
                            }
                            else {
                                loadData();
                            }
                        }
                    }
                });
            }
        });
    }

    $('#add-note').click(function () {
        var url = "{{ route('admin.applicant-note.store') }}";
        var id = {{$application->id}};
        var note = $('#note_text').val();
        var token = '{{ csrf_token() }}';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token':token, 'id':id, 'note': note},
            success: function (response) {
                if(response.status == 'success') {
                    $('#applicant-notes').html(response.view);
                    $('#note_text').val('');
                }
            }
        });
    });

    $('body').on('click', '.edit-note', function() {
        $(this).hide();
        let noteId = $(this).data('note-id');
        $('body').find('#note-'+noteId+' .note-text').hide();

        let noteText = $('body').find('#note-'+noteId+' .note-text').html();
        let textArea = '<textarea id="edit-note-text-'+noteId+'" class="form-control" row="4">'+noteText+'</textarea><a class="update-note" data-note-id="'+noteId+'" href="javascript:;"><i class="fa fa-check"></i> @lang("app.save")</a>';
        $('body').find('#note-'+noteId+' .note-textarea').html(textArea);
    });

    $('body').on('click', '.update-note', function () {
        let noteId = $(this).data('note-id');

        var url = "{{ route('admin.applicant-note.update', ':id') }}";
        url = url.replace(':id', noteId);

        var note = $('#edit-note-text-'+noteId).val();
        var token = '{{ csrf_token() }}';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token':token, 'noteId':noteId, 'note': note, '_method': 'PUT'},
            success: function (response) {
                if(response.status == 'success') {
                    $('#applicant-notes').html(response.view);
                }
            }
        });
    });

    $('body').on('click', '.delete-note', function(){
        let noteId = $(this).data('note-id');
        swal({
            title: "@lang('errors.areYouSure')",
            text: "@lang('errors.deleteWarning')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "@lang('app.delete')",
            cancelButtonText: "@lang('app.cancel')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.applicant-note.destroy', ':id') }}";
                url = url.replace(':id', noteId);

                var token = '{{ csrf_token() }}';

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token':token, '_method': 'DELETE'},
                    success: function (response) {
                        if(response.status == 'success') {
                            $('#applicant-notes').html(response.view);
                        }
                    }
                });
            }
        });
    });

    function onChangeApplicantStatus(element) {
        var status_id = $(element).val();
        var applicant_id = $(element).data('applicant');

        $.easyAjax({
            type: 'POST',
            url: '{{ route("admin.job-application.updateStatus") }}',
            data: {'_token':'{{ csrf_token() }}', 'applicant_id':applicant_id, 'status_id': status_id},
            success: function (response) {
                if(response.status == 'success') {
                    var box = $(`[data-application-id=${applicant_id}]`).fadeOut(500);
                    setTimeout(() => {
                        $(`[data-column-id=${status_id}]`).find('.lobipanel').parent().prepend(box);
                        box.fadeIn();
                        location.reload();
                    }, 550);
                }
            }
        });
    }
    function load(id){
        $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('admin.job-applications.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    }
</script>
@endif
@if(!is_null($application->skype_id))
    <script src="https://swc.cdn.skype.com/sdk/v1/sdk.min.js"></script>
@endif
