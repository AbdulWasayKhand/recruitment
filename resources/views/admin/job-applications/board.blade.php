<style>
.scroll-modified{margin-right: 1px !important;margin-left: 1px !important;}
.scroll-modified .card-title{font-size: 1rem;}
.scroll-modified .badge-primary{background-color:#6a6a6a}
.scroll-modified .board-column .badge-primary {padding: .3em .8em;font-size: 14px;}
.scroll-modified  div.board-column:first-child{margin-left:0}
.scroll-modified::-webkit-scrollbar-thumb, .custom-panel::-webkit-scrollbar-thumb {border-radius: 4px;background-color: rgb(185 185 185 / 60%);-webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);}
.scroll-modified::-webkit-scrollbar-track, .custom-panel::-webkit-scrollbar-track {border-radius: 4px;background-color: rgb(185 185 185 / 35%);-webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);}
.custom-panel{padding-right:5px;}
.custom-panel h6{font-weight:500}
.custom-panel h5{color:#636363}
.custom-panel .font-13{font-size:13px;font-weight:500;color:#636363}
.custom-panel button{font-weight: 500;padding-bottom: 2px;}
.custom-panel .lobipanel-sortable {border-left: none;background: #dedede;border-radius:8px;}
.custom-panel .circle span {background: #b5b5b5;width: 10px;height: 10px;border-radius: 10px;margin-left: 3px;}
.custom-panel .circle span:first-child{margin-left: 0;}
#reportrange{width:100%;border: 1px solid #d2d6de !important;border-radius: 4px;}
</style>
@extends('layouts.app')

@if(in_array("add_job_applications", $userPermissions))
@section('create-button')
    <a href="{{ route('admin.job-applications.create') }}" class="btn btn-dark btn-sm m-l-15"><i
                class="fa fa-plus-circle"></i> @lang('app.createNew')</a>
@endsection
@endif

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/lobipanel/dist/css/lobipanel.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-bar-rating-master/dist/themes/fontawesome-stars.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    
    <style>
        .board-column{
            /* max-width: 21%; */
        }

        .board-column .card{
            box-shadow: none;
        }
        .notify-button{
            /*width: 9em;*/
            height: 1.5em;
            font-size: 0.730rem !important;
            line-height: 0.5 !important;
        }
        .panel-scroll{
            height: calc(100vh - 330px); overflow-y: scroll
        }
        .mb-20{
            margin-bottom: 20px
        }
        .datepicker{
            z-index: 9999 !important;
        }
        .d-block{
            display: block;
        }
        .upcomingdata {
            height: 37.5rem;
            overflow-x: scroll;
        }
        .notify-button{
            height: 1.5em;
            font-size: 0.730rem !important;
            line-height: 0.5 !important;
        }
        .scheduleul
        {
            padding: 0 15px 0 11px;
        }
        .searchInput
        {
            width: 50%; display: inline
        }
        .searchButton
        {
            margin-bottom: 4px;margin-left: 3px;
        }
        #ticket-filters {
            display:none;
        }
        #create_new {
            display: none;
        }
        #message_body , #sms_body, #personal_message{width:100%;height: 200px;border-color: lightgray;outline: none;}
    </style>
@endpush

@section('content')
    <div class="row mb-2">
        <div class="col-sm-9">
            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-success btn-sm toggle-filter">
                <i class="fa fa-sliders"></i> @lang('app.filterResults')
            </a>
            <a href="{{ route('admin.application-setting.index').'#mail-setting' }}" class="btn btn-sm btn-info">
                <i class="fa fa-envelope-o"></i>
                @lang('modules.applicationSetting.mailSettings')
            </a>
            <a href="javascript:createApplicationStatus();" class="btn btn-sm btn-success d-none">
                <i class="fa fa-bookmark-o"></i>
                @lang('modules.jobApplication.newStatus')
            </a>

            <a href="{{ route('admin.job-applications.table') }}" class="btn btn-primary btn-sm"><i class="fa fa-sliders"></i> @lang('modules.jobApplication.switchToTableView')</a>
        </div>
        @if(session()->get('workflow_dropdown_job_id') === null)
            @php(session()->put('workflow_dropdown_job_id', $default_workflow_view_job))
        @endif
        <div class="col-sm-3">
            <select class="form-control" id="job_select">
                @foreach($jobs as $job)
                <option value="{{$job->id}}" @if(session()->get('workflow_dropdown_job_id')==$job->id ) selected @endif>{{$job->title}}</option>
                @endforeach
            </select>
        </div>
        {{--<div class="col-sm-4">
            <div id="search-container" class="form-group pull-right">
                <input id="search" class="form-control" type="text" name="search" placeholder="@lang('modules.jobApplication.enterName')">
                <a href="javascript:;" class="d-none">
                    <i class="fa fa-times-circle-o"></i>
                </a>
            </div>
        </div>--}}
    </div>

    <div class="container-scroll">
        <div class="card" id="ticket-filters" style="display:none">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right mt-2 mr-2 toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
                    </div>
                    <div class="col-md-4 main-date">
                        <div id="reportrange"  class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                            <i class="fas fa-calendar-alt"></i>&nbsp;
                            <span></span> <b class="caret"></b>
                        </div>
                        <br /><br />
                        <input type="hidden" id="daterange_picker_start" value="">
                        <input type="hidden" id="daterange_picker_end" value="">
                    </div> 
                    <!--<div class="col-md-4">
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control" id="date-start" value="<?= date('m-d-Y', strtotime($startDate)) ?>" name="start_date">
                            <span class="input-group-addon bg-info b-0 text-white p-1">@lang('app.to')</span>
                            <input type="text" class="form-control" id="date-end" name="end_date" value="<?= date('m-d-Y', strtotime($endDate)) ?>">
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <select class="select2" name="jobs" id="jobs" data-style="form-control">
                                <option value="all">@lang('modules.jobApplication.allJobs')</option>
                                @forelse($jobs as $job)
                                    <option title="{{ucfirst($job->title)}}" value="{{$job->id}}">{{ucfirst($job->title)}}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" id="apply-filters" class="btn btn-success btn-sm"><i class="fa fa-check"></i> @lang('app.apply')</button>
                            <button type="button" id="reset-filters" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row container-row scroll-modified">
        </div>
    </div>

    {{--Ajax Modal Start for--}}
    <div class="modal fade bs-modal-md in" id="scheduleDetailModal" style="z-index: 1042;" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal Start for--}}
    <div class="modal fade bs-modal-md in" id="addDepartmentModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="z-index: 1041;">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Question And Answers</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body" id="qna_view">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
{{--Send Text Modal Start--}}
    <div class="modal fade bs-modal-md in" id="exampleModalTwo" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 99999;">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Personal Text Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label>Text message</label>
            <textarea  id="personal_message" placeholder="Enter Your Text message"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_message_modal_two">Close</button>
            <a href="javascript:sendSMS()" class="btn btn-success">
                Send
            </a>
        </div>
        </div>
    </div>
</div>
{{--Send Text Modal Ends--}}
<div class="modal fade bs-modal-md in" id="NotesModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                    <h4 class="card-title form-sub-heading mb-4">
                    Notes
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body" style="overflow-y: auto;max-height: 45vh;">
                    <div class="row">
                        <div class="col-12">
                            <ul class="list-group list-group-flush ml-4" id="view-notes-modal"></ul>
                        </div>
                    </div>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{ asset('assets/lobipanel/dist/js/lobipanel.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-bar-rating-master/dist/jquery.barrating.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>

    <script>
        $(".select2").select2({
            width: '100%'
        });
        loadData();

        $('#apply-filters').click(function () {
            loadData();
        });

        $('#reset-filters').click(function () {
            $('#date-start').val('{{ $startDate }}');
            $('#date-end').val('{{ $endDate }}');
            $('#jobs').val('all').trigger('change');

            loadData();
        })
        $('#applySearch').click(function () {
            var search = $('#search').val();
            if(search !== undefined && search !== null && search !== ""){
                loadData();
            }
        })

        $('#date-end').bootstrapMaterialDatePicker({ weekStart : 0, time: false, format: 'MM-DD-YYYY' });
        $('#date-start').bootstrapMaterialDatePicker({ weekStart : 0, time: false, format: 'MM-DD-YYYY' }).on('change', function(e, date)
        {
            $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
        });
        
        // Schedule create modal view
        function createSchedule (id) {
            var url = "{{ route('admin.job-applications.create-schedule',':id') }}";
            url = url.replace(':id', id);
            $('#modelHeading').html('Schedule');
            $.ajaxModal('#scheduleDetailModal', url);
        }

        // Create application status modal view
        function createApplicationStatus () {
            var url = "{{ route('admin.application-status.create') }}";

            $('#modelHeading').html('Application Status');
            $.ajaxModal('#scheduleDetailModal', url);
        }

        function deleteStatus(id) {
            const panels = $('.board-column[data-column-id="' + id + '"').find('.lobipanel.show-detail');
            let applicationIds = [];
            panels.each((ind, element) => {
                applicationIds.push($(element).data('application-id'));
            });

            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.deleteStatusWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    let url = "{{ route('admin.application-status.destroy', ':id') }}";
                    url = url.replace(':id', id);

                    let data = {
                        _token: '{{ csrf_token() }}',
                        _method: 'DELETE',
                        applicationIds: applicationIds
                    }

                    $.easyAjax({
                        url,
                        data,
                        type: 'POST',
                        container: '.container-row',
                        success: function (response) {
                            if (response.status == 'success') {
                                loadData();
                            }
                        }
                    })
                }
            });
        }

        function editStatus(id) {
            var url = "{{ route('admin.application-status.edit', ':id') }}";
            url = url.replace(':id', id);

            $('#modelHeading').html('Application Status');
            $.ajaxModal('#scheduleDetailModal', url);
        }

        function saveStatus() {
            $.easyAjax({
                url: "{{ route('admin.application-status.store') }}",
                container: '#createStatus',
                type: "POST",
                data: $('#createStatus').serialize(),
                success: function (response) {
                    $('#scheduleDetailModal').modal('hide');
                    loadData();
                }
            });
        }

        function updateStatus(id) {
            let url = "{{ route('admin.application-status.update', ':id') }}";
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                container: '#updateStatus',
                type: "POST",
                data: $('#updateStatus').serialize(),
                success: function (response) {
                    $('#scheduleDetailModal').modal('hide');
                    loadData();
                }
            });
        }

        function loadData () {
            // var startDate = $('#date-start').val();

            var   startDate = $('#daterange_picker_start').val();
            var   endDate = $('#daterange_picker_end').val();

            var jobs = $('#jobs').val();
            var search = $('#search').val();

            // if (startDate == '') {
            //     startDate = null;
            // // }

            // var endDate = $('#date-end').val();

            // if (endDate == '') {
            //     endDate = null;
            // }

            if(startDate == '' && endDate == ''){
                startDate = "<?= date('Y-m-d', strtotime('-90 days'));?>";
                endDate   = "<?= date('Y-m-d', strtotime('now'));?>";
            }

            var url = '{{route('admin.job-applications.index')}}?startDate=' + startDate + '&endDate=' + endDate + '&jobs=' + jobs + '&search=' + search;

            $.easyAjax({
                url: url,
                container: '.container-row',
                type: "GET",
                success: function (response) {
                    $('.container-row').html(response.view);
                }

            })
        }

        search($('#search'), 500, 'data');

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        });
    </script>

     <!-- Date Picker Start -->
     <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />                    
    <!-- Date Picker END -->


    <script>
        $(function() {
            var start = moment().subtract(90, 'days');
            var end   = moment();
            
            function cb(start, end) {
                var start_end  = start+'_all_'+end;
                var startDate = '';
                var endDate   = '';

                if(start_end == 'NaN_all_NaN'){
                    $('#reportrange span').html('All Time');
                    // startDate = '1970-01-01 00:00:00'; 
                    // endDate   = '2050-12-31 23:59:59';
                    startDate = '1970-01-01'; 
                    endDate   = '2050-12-31';
                }else{
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    // startDate = start.format('YYYY-MM-DD')+' 00:00:00'; 
                    // endDate   = end.format('YYYY-MM-DD')+' 23:59:59';
                    startDate = start.format('YYYY-MM-DD'); 
                    endDate   = end.format('YYYY-MM-DD');
                }
                
                $('#daterange_picker_start').val(startDate);
                $('#daterange_picker_end').val(endDate);

                // exportJobApplication(startDate, endDate);
                // beforeSend(startDate, endDate);
                // tableLoad(startDate, endDate);
                console.log(startDate, endDate);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                'All Time'  : ['all', 'all']
                }
            }, cb);
            cb(start, end);
        });
        var sms_number = 0;
        var app_id = 0;
        function sendSMS(){
            personal_message.style.borderColor = 'lightgray';
            var pm = personal_message.value;
            if(!pm){
                personal_message.style.borderColor = 'red';
                return;
            }
            $.easyAjax({
                type: 'POST',
                url: "{{ url('sendSMS') }}",
                data: {'_token': '{{ csrf_token() }}', to: sms_number, message: pm , company_id: '{{company()->id}}', applicant_id: app_id},
                success: function (response) {
                    console.log(response)
                    personal_message.value = '';
                    sms_number = 0;
                    app_id = 0;
                    close_message_modal_two.click();
                }
            });
        }

        function beforeSend(){
            if(message_body.value != "" &&  sms_body.value != ""){
                swal({
                title: "@lang('errors.areYouSure')",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "@lang('app.yes')",
                cancelButtonText: "@lang('app.no')",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.job-application.interviewInvitation') }}";
                        var token = '{{ csrf_token() }}';

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token':token, 'applicationId' : application_id , 'emailMessage' : message_body.value != '' ? encodeURI(encodeURIComponent(message_body.value)) : 'N_A' , 'smsMassage' : sms_body.value != '' ? encodeURI(encodeURIComponent(sms_body.value)) : 'N_A'},
                            success: function (response) {
                                $("body").removeClass("control-sidebar-slide-open");
                                if (response.status === 'success') {
                                    if (typeof table !== 'undefined') {
                                        table._fnDraw();
                                    }
                                    else {
                                        loadData();
                                    }
                                }
                                application_id = 0;
                                location.reload();


                            }
                        });
                    }
                });
            }else{
                alert("Empty fields are not allowed")
            }
        }
        $('#NotesModal').on('show.bs.modal', function(e) {
            $html = '';
            $("#i-"+$(e.relatedTarget).attr('applicant_id')).closest('div').parent().parent().find('.notes-li').each(function () {
                $html += $(this).prop('outerHTML');
            });
            ($(".notes-timeline").length) ?
            $("#view-notes-modal").html($html) :
            $("#view-notes-modal").html('<span>Notes not found.</span>');
        });
    </script>
@endpush