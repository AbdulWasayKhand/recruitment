@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    
    <style>
        .mb-20 {margin-bottom: 20px}
        .datepicker {z-index: 9999 !important;}
        #message_body , #sms_body, #personal_message{width:100%;height: 200px;border-color: lightgray;outline: none;}
        .table-danger2 td:nth-child(10) { background-color: #dd4b44 !important; color:#fff; font-weight: 500;}
        .table-danger3 td:nth-child(9) { background-color: #bdbbbb !important; color:#fff; font-weight: 500;}
        .even td:nth-child(10), .odd td:nth-child(10) { background-color: #45DC76;color:#fff;font-weight: 500;}
        #reportrange{width:100%;border: 1px solid #d2d6de !important;border-radius: 4px;}
        .control-sidebar-slide-open .control-sidebar{position:fixed}
        .toggle-filter {
            color: #28a745 !important;
        }
        .toggle-filter:hover {
            background-color: #28a745 !important;
            color: #fff !important;
        }
        .settings-email {
            color: #17a2b8 !important;
        }
        .settings-email:hover {
            background-color: #17a2b8 !important;
            color: #fff !important;
        }
        .switch-workflow {
            color: #4e5d9e !important;
        }
        .switch-workflow:hover {
            background-color: #4e5d9e !important;
            color: #fff !important;
        }
        .stages {
            display:none;
        }
    </style>
    <script>
    @if(request('qualified'))
        var f_qualified = true; 
    @else 
        var f_qualified = false;
    @endif
    @if(request('rejected'))
        var f_rejected = true; 
    @else 
        var f_rejected = false;
    @endif
    @if(request('f_jobs'))
        var f_jobs = true; 
        var filt_jobs = "<?= request('f_jobs') ?>";
    @else 
        var f_jobs = false;
        var filt_jobs = '';
    @endif
    </script>
@endpush

@if(in_array("add_job_applications", $userPermissions))
@section('create-button')
    <a href="{{ route('admin.job-applications.create') }}" class="btn btn-dark btn-sm m-l-15"><i class="fa fa-plus-circle"></i> @lang('app.createNew')</a>
@endsection
@endif

@section('content')
<div class="modal fade bs-modal-md in" id="exampleModalTwo" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 99999;">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Personal Text Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label>Text message</label>
            <textarea  id="personal_message" placeholder="Enter Your Text message"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_message_modal_two">Close</button>
            <a href="javascript:sendSMS()" class="btn btn-success">
                Send
            </a>
        </div>
        </div>
    </div>
</div>
<div class="modal fade bs-modal-md in" id="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">@lang('modules.jobApplication.editEmail&Text')</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label>Email Message</label>
            <textarea class="wysihtml5"  id="message_body" placeholder="Email  Message"></textarea>
            <hr />
            <label>Text Message</label>
            <textarea id="sms_body" class="form-control" placeholder="Text Message" ></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_message_modal">Close</button>
            <a href="javascript:beforeSend()" class="btn btn-success">
                @lang('modules.jobApplication.sendInteviewInvite')
            </a>
        </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12 mb-2">
            <a class="pull-right pull-none-xs" onclick="exportJobApplication()" >
                <button class="btn btn-sm btn-primary" type="button">
                    <i class="fa fa-upload"></i>  @lang('menu.export')
                </button>
            </a>
            <a @if($_SERVER['SERVER_NAME'] == 'localhost') href="javascript:;" data-toggle="modal" data-title="add-applicant" data-target="#responsive-modal2" @else href="{{url('admin/job-applications/create')}}" @endif class="pull-right pull-none-xs pr-2">
                <button class="btn btn-sm btn-success" type="button">
                    <i class="nav-icon fa fa-plus" style="width: auto;"></i> @lang('menu.addApplicant')
                </button>
            </a>
        </div>
            <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row clearfix">
                        <div class="col-md-12 mb-20">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-sm toggle-filter"><i class="fa fa-sliders"></i> @lang('app.filterResults')</a>
                            <a href="{{ route('admin.application-setting.index').'#mail-setting' }}" class="btn btn-sm settings-email">
                                <i class="fa fa-envelope-o"></i>
                                @lang('modules.applicationSetting.mailSettings')
                            </a>
                            <a href="{{ route('admin.job-applications.index').'#mail-setting' }}" class="btn btn-sm switch-workflow">
                                <i class="fa fa-list"></i>
                                @lang('modules.jobApplication.switchToWorkflowView')
                            </a>
                            <a href="{{ route('admin.applications-archive.index') }}" class="btn btn-sm text-danger">
                                <i class="fa fa-archive"></i>
                                @lang('modules.jobApplication.archived')
                            </a>
                            <div style="display:inline-block" class="pull-right mr-2">
                                @foreach(\App\Workflow::where('company_id', '=', $user->company_id)->get() as $workflows)
                                    <select class="stages form-control" id="workflow_{{$workflows->id}}">
                                        <option value="0">Change Stage</option>
                                        @foreach(\App\WorkflowStage::where('workflow_id','=', $workflows->id)->get() as $stages)
                                            <option value="{{$stages->id}}">{{$stages->title}}</option>
                                        @endforeach
                                    </select>
                                @endforeach
                                <select class="stages form-control" id="workflow_0">
                                    <option value="0">Change Status</option>
                                    @foreach($boardColumns as $status)
                                        <option value="{{$status->id}}">{{ucfirst($status->status)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <a class="pull-right mr-2 multiple-action" onclick="" style="display:none"><button class="btn btn-sm btn-danger multi-delete-button" style="padding:7px;" type="button"><i class="fa fa-trash"></i>@lang('modules.jobApplication.ArchiveSeleted')
                            </button></a>
                        </div>
                    </div>
                    <div class="row b-b b-t mb-3" style="display: none; background: #fbfbfb;" id="ticket-filters">
                        <div class="col-md-12">
                            <h4 class="mt-2">@lang('app.filterBy') <a href="javascript:;" class="pull-right mt-2 mr-2 toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
                        </div>
                        <div class="col-md-12">
                            <form action="" id="filter-form" class="row" >
                                <div class="col-md-4 main-date">
                                    <div id="reportrange"  class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                                        <i class="fas fa-calendar-alt"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                    <br /><br />
                                    <input type="hidden" id="daterange_picker_start" value="">
                                    <input type="hidden" id="daterange_picker_end" value="">
                                </div> 
                                <!--<div class="col-md-4">
                                    <div class="example">
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" class="form-control" id="start-date" placeholder="Show Results From" value="" />
                                            <span class="input-group-addon bg-info b-0 text-white p-1">@lang('app.to')</span>
                                            <input type="text" class="form-control" id="end-date" placeholder="Show Results To" value="" />
                                        </div>
                                    </div>
                                </div>-->
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="select2" name="status" id="status" data-style="form-control">
                                            <option value="all">@lang('modules.jobApplication.allStatus')</option>
                                            @forelse($boardColumns as $status)
                                                <option value="{{$status->id}}">{{ucfirst($status->status)}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="select2" data-style="form-control" name="jobs" id="jobs" >
                                            <option value="all">@lang('modules.jobApplication.allJobs')</option>
                                            @forelse($jobs as $job)
                                                <option title="{{ucfirst($job->title)}}" value="{{$job->id}}">{{ucfirst($job->title)}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="hidden" name="criteria" value="{{$job->eligibility_percentage ?? '0'}}">
                                        <select class="select2" name="qualification" id="qualification" data-style="form-control">
                                            <option value="all">@lang('modules.jobApplication.allQualifications')</option>
                                            <option value="qualified">Qualified</option>
                                            <option value="unqualified">UnQualified</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select class="select2" name="skill[]" data-placeholder="Select Skills" multiple="multiple" id="skill" data-style="form-control">
                                            @forelse($skills as $skill)
                                                <option value="{{$skill->id}}">{{ucfirst($skill->name)}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" id="apply-filters" class="btn btn-sm btn-success"><i class="fa fa-check"></i> @lang('app.apply')</button>
                                        <button type="button" id="reset-filters" class="btn btn-sm btn-dark "><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox form-check">
                                        <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                                        <label for="example-select-all"></label>
                                    </div>
                                </th>
                                <th>Date Applied</th>
                                <th id="full_name" class="sorting">@lang('modules.jobApplication.applicantName')</th>
                                <th id="applicant_hidden" class="d-none"></th>
                                <th>@lang('menu.jobs')</th>
                                <th>@lang('menu.locations')</th>
                                <th>@lang('app.stage')</th>
                                <th>@lang('app.source')</th>
                                <th>Host</th>
                                {{-- <th>@lang('modules.jobApplication.refererUrl')</th> --}}
                                <th>@lang('app.qualification')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        {{--Ajax Modal Start for--}}
        <div class="modal fade bs-modal-md in" id="scheduleDetailModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-lg" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
               </div>
               <div class="modal-body">
                   Loading...
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn default" data-dismiss="modal">Close</button>
                   <button type="button" class="btn blue">Save changes</button>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
   </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal Start for fill Questionnaire--}}
    <div class="modal fade bs-modal-md in" id="questionnaireModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">@lang('menu.questionnaire')</span>
               </div>
               <div class="modal-body">
                   Loading...
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn default" data-dismiss="modal">Close</button>
                   <button type="button" class="btn blue">Save changes</button>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="NotesModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                    <h4 class="card-title form-sub-heading mb-4">
                    Notes
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body" style="overflow-y: auto;max-height: 45vh;">
                    <div class="row">
                        <div class="col-12">
                            <ul class="list-group list-group-flush ml-4" id="view-notes-modal"></ul>
                        </div>
                    </div>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
   <style>#create_new{display:none !important;}</style>
   {{--Ajax Modal Start for--}}
    <div class="modal fade bs-modal-md in" id="addDepartmentModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="z-index: 1041;">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Question And Answers</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body" id="qna_view">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')

    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
    <script>

    

        var sms_number = 0;
        var app_id = 0;
        function sendSMS(){
            personal_message.style.borderColor = 'lightgray';
            var pm = personal_message.value;
            if(!pm){
                personal_message.style.borderColor = 'red';
                return;
            }
            $.easyAjax({
                type: 'POST',
                url: "{{ url('sendSMS') }}",
                data: {'_token': '{{ csrf_token() }}', to: sms_number, message: pm , company_id: '{{company()->id}}', applicant_id: app_id},
                success: function (response) {
                    console.log(response)
                    personal_message.value = '';
                    sms_number = 0;
                    app_id = 0;
                    close_message_modal_two.click();
                }
            });
        }
        var jobDescription = $('#legal_term').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": true, //Button to change color of font
            stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)
        });

        $('.wysihtml5').each(function(index, node) {
            $(node).wysihtml5({
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": true, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": true, //Button to change color of font
                stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)
            });
        })

        var filtered_records = [];
        if("{{\Request::has('status')}}" && "{{\Request::get('status')}}"){
            $('#status').val("{{Request::get('status')}}");
            $('#apply-filters').click();
        }
        if(f_qualified){
            $('#qualification').val('qualified');
            tableLoad('filter');
        }
        if(f_jobs){
            $('#jobs').val(filt_jobs);
            tableLoad('filter');
        }
        if(f_rejected){
            // $('#status').find('');
            $val = 0;
            Array.from(document.getElementById('status').children).forEach(function (e){
                if(e.innerHTML == 'Rejected'){
                    $val = e.value;
                }
            });
            $('#status').val($val);
            tableLoad('filter');
        }
        $('#start-date').datepicker({
            format: 'mm-dd-yyyy'
        })
        $('#end-date').datepicker({
            format: 'mm-dd-yyyy'
        })

        var table;
        tableLoad('load');

        // For select 2
        $(".select2").select2({
            width: '100%'
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#filter-form').find('select').val('all').trigger('change');
            tableLoad('load');
        });

        $('#apply-filters').click(function () {
            tableLoad('filter');
            var table = $('#myTable').dataTable();
            table.fnPageChange(1,true);
        });


        function createSchedule (id) {
            var url = "{{ route('admin.job-applications.create-schedule',':id') }}";
            url = url.replace(':id', id);
            $('#modelHeading').html('Schedule');
            $.ajaxModal('#scheduleDetailModal', url);
        }

        function tableLoad(type) {
            var status = $('#status').val();
            var jobs = $('#jobs').val();
            var location = 'all';
            var qualification = $('#qualification').val();
            // var   startDate = $('#start-date').val();
            // var   endDate = $('#end-date').val();
            var   startDate = $('#daterange_picker_start').val();
            var   endDate = $('#daterange_picker_end').val();
            var   skill = $('#skill').val();
            filtered_records = [];
            if(startDate == '' && endDate == ''){
                startDate = "<?= date('Y-m-d', strtotime('-90 days'));?>";
                endDate   = "<?= date('Y-m-d', strtotime('now'));?>";
            }
            console.log('table loading...', startDate, endDate);
            var v= 0;
            table = $('#myTable').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                pageLength: 25,
                stateSave: true,
                order: [1, "desc"],
                ajax: '{!! route('admin.job-applications.data') !!}?qualification='+qualification+'&status='+status+'&location='+location+'&startDate='+startDate+'&endDate='+endDate+'&jobs='+jobs+'&skill='+skill+'&job_id={{$job_id}}',
                language: languageOptions(),
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'selectColumn', name: 'selectColumn', width: '4%', class: 'text-center', orderable: false, searchable: false },
                    {data: 'date', name: 'created_at', width: '18%'},
                    {data: 'full_name', name: 'full_name', width: '17%',sorting:false,orderable:false},
                    {data: 'applicant_name', name: 'applicant_name',target:3,class:"d-none"},
                    {data: 'title', name: 'job_title', width: '30%'},
                    {data: 'location', name: 'location'},
                    {data: 'workflow_stage', name: 'workflow_stage'},
                    {data: 'source', name: 'source'},
                    {data: 'widget_host', name: 'wdidget_host'},
                    {data: 'qualification', name: 'eligibility', searchable: false},
                    {data: 'action', name: 'action', width: '17%', searchable : false,sorting:false,orderable:false},
                ],
                'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    // if(aData.qualification != 'N/A' && aData.eligibility < aData.job.eligibility_percentage) {
                    //     nRow.classList.add('table-danger2');
                    // }
                    if(aData.qualification.includes("UNQUALIFIED")){
                        nRow.classList.add('table-danger2');
                    }
                    if(aData.qualification.includes("N/A")){
                        nRow.classList.add('table-danger3');
                    }
                    if(aData.selectColumn){
                        nRow.firstChild.firstChild.setAttribute('data-id', v++);
                    }
                    filtered_records.push(aData.id);
                }
            });
            new $.fn.dataTable.FixedHeader(table);
        }
            $('#myTable').on("click","#full_name",function () {
                // var table = $('#myTable').DataTable();
                 
                // Sort by columns 1 and 2 and redraw
                if($("#applicant_hidden").hasClass('sorting_asc')){
                    $('#myTable').DataTable()
                    .order( [ 3, 'desc' ] )
                    .draw();                    
                }
                else if($("#applicant_hidden").hasClass('sorting_desc')){
                    $('#myTable').DataTable()
                    .order( [ 3, 'asc' ] )
                    .draw();                                
                }
                else{
                    $('#myTable').DataTable()
                    .order( [ 3, 'asc' ] )
                    .draw();                    
                }
                });
        $check = {};
        $job_check = {};
        $uids = {};
        $('.stages').on('change', function(){
            $uids_array = [];
            Object.keys($uids).forEach(($key) => {
                $uids_array.push($uids[$key]);
            });
            $.easyAjax({
                type: 'POST',
                url: '{{ route("admin.job-application.multiUpdateStage") }}',
                data: {'_token':'{{ csrf_token() }}', 'applicant_ids':$uids_array, 'workflow_stage_id': this.value},
                success: function (response) {
                    setTimeout(() => {
                        // location.reload();
                    }, 550);
                },
                error: function (response) {
                    setTimeout(() => {
                        // location.reload();
                    }, 550);
                },
            });

        });
        $('#myTable').on('change', 'input[type="checkbox"]', function () {
            $('.stages').css('display', 'none');
            if(this.id == 'example-select-all') {
                $('.multi-check').prop('checked', $(this).prop('checked'));
            }

            if($('input[type="checkbox"]:checked').length > 0) {
                $('.multiple-action').show();
            } else {
                $('.multiple-action').hide();
            }
            var data_id = "__" + this.parentNode.getAttribute('data-id');
            var job_data_id = "__" + this.getAttribute('data-job');
            var uid = '__' + this.id;
            if(this.checked){
                $check[data_id] = this.getAttribute('data-workflow');
                if(job_data_id != '__0'){
                    $job_check[data_id] = job_data_id;
                }
                $uids[uid] = this.id;
            }else{
                delete $check[data_id];
                delete $uids[uid];
                if(job_data_id != '__0'){
                    delete $job_check[data_id];
                }
            }

            if(Object.keys(checkKeys($check)).length == 1){
                if(job_data_id != '__0'){
                    if(Object.keys(checkKeys($job_check)).length == 1){
                        enableDropdown($check, this);
                    }
                }else{
                    enableDropdown($check, this);
                }
            }
        });
        function enableDropdown($check, $this){
            $wid = checkKeys($check);
            Object.keys($wid).forEach(($key) => {
                var elm = document.getElementById("workflow" + $key);
                if(elm && $this.id != 'example-select-all'){
                    elm.style.display = 'block';
                }else{
                    $('.stages').css('display', 'none');
                }
            });
        }
        function checkKeys($obj){
            $check_ = {};
            Object.keys($obj).forEach(($key) => {
                $check_["_" + $obj[$key]] = true;
            });
            return $check_;
        }
        $('.multi-delete-button').on('click', function () {
            swal({
                title: "@lang('errors.areYouSure')",
                text: "@lang('errors.doYouArchiveWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('modules.jobApplication.archive')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var data = $('#myTable input[type=checkbox]:not(#example-select-all)').serialize();
                    var token = "{{ csrf_token() }}";
                    data += "&_token="+token+"&_method=DELETE";
                    $.easyAjax({
                        type: 'POST',
                        url: '{{route("admin.job-application.multiArchive")}}',
                        data: data,
                        success: function (response) {
                            if (response.status == "success") {
                                location.reload();
                            }
                        }
                    });
                }
            });
        })
        
        $('body').on('click', '.sa-params,.delete-document', function(){
            var id = $(this).data('row-id');
            const deleteDocClassPresent = $(this).hasClass('delete-document');
            const saParamsClassPresent = $(this).hasClass('sa-params');

            swal({
                title: "@lang('errors.areYouSure')",
                text: deleteDocClassPresent ? "@lang('errors.deleteWarning')" : "@lang('errors.doYouArchiveWarning')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: deleteDocClassPresent ? "@lang('app.delete')" : "@lang('modules.jobApplication.archive')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    let url = '';

                    if (deleteDocClassPresent) {
                        url = "{{ route('admin.documents.destroy',':id') }}";
                    }
                    if (saParamsClassPresent) {
                        url = "{{ route('admin.job-applications.archiveJobApplication',':id') }}";
                    }

                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': deleteDocClassPresent ? 'DELETE' : 'POST'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                if (deleteDocClassPresent) {
                                    docTable._fnDraw();
                                }
                                if (saParamsClassPresent) {
                                    table._fnDraw();
                                }
                            }
                        }
                    });
                }
            });
        });
        @if(request('applicant'))
            setTimeout(() => {
                $("<a></a>")
                    .attr('href', 'javascript:;')
                    .attr('class', 'show-detail')
                    .attr('data-widget', 'control-sidebar')
                    .attr('data-slide', 'true')
                    .attr('data-row-id', "{{ request('applicant') }}")
                    .prependTo(table)
                    .click();
            }, 1000);
        @endif
        table.on('click', '.show-detail', function () {
            $(".right-sidebar").slideDown(50).addClass("shw-rside");

            var id = $(this).data('row-id');
            var url = "{{ route('admin.job-applications.show',':id') }}";
            url = url.replace(':id', id);

            $.easyAjax({
                type: 'GET',
                url: url,
                success: function (response) {
                    if (response.status == "success") {
                        $('#right-sidebar-content').html(response.view);
                        window.history.pushState({}, '', '{{route("admin.job-applications.table")}}');
                    }
                }
            });
        });

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        });

        $('body').on('click', '.show-document', function() {
            const type = $(this).data('modal-name');
            const id = $(this).data('row-id');

            const url = "{{ route('admin.documents.index') }}?documentable_type="+type+"&documentable_id="+id;

            $.ajaxModal('#application-lg-modal', url);
        })

        function exportJobApplication(){
            var all = false;
            var startDate = $('#daterange_picker_start').val();
            var endDate = $('#daterange_picker_end').val();
            var status = $('#status').val();
            var jobs = $('#jobs').val();
            var location = $('#location').val();
            var qualification = $("#qualification").val();
            var skill = $("#skill").val();
            //  startDate = $('#start-date').val();
            //  endDate = $('#end-date').val();

            // if(startDate == '' || startDate == null){
            //     startDate = 0;
            // }

            if(endDate == '' || endDate == null){
                endDate = 0;
            }
            if(status == 'all' && (startDate == 0 || startDate == undefined) && endDate == 0 && jobs == 'all' && qualifications == 'all' && skill == ''){
                all = true;
            }
            var url = '{{ route('admin.job-applications.export', [':status', ':startDate', ':endDate', ':jobs', ':qualification']) }}';
            url = url.replace(':status', all ? 'all' : filtered_records);
            url = url.replace(':jobs', jobs);
            url = url.replace(':startDate', startDate);
            url = url.replace(':endDate', endDate);
            url = url.replace(':qualification', qualification);
            window.location.href = url;
        }
        var application_id = 0;
        function beforeSend(){
            if(message_body.value != "" &&  sms_body.value != ""){
                swal({
                title: "@lang('errors.areYouSure')",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "@lang('app.yes')",
                cancelButtonText: "@lang('app.no')",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.job-application.interviewInvitation') }}";
                        var token = '{{ csrf_token() }}';

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token':token, 'applicationId' : application_id , 'emailMessage' : message_body.value != '' ? encodeURI(encodeURIComponent(message_body.value)) : 'N_A' , 'smsMassage' : sms_body.value != '' ? encodeURI(encodeURIComponent(sms_body.value)) : 'N_A'},
                            success: function (response) {
                                $("body").removeClass("control-sidebar-slide-open");
                                if (response.status === 'success') {
                                    if (typeof table !== 'undefined') {
                                        table._fnDraw();
                                    }
                                    else {
                                        loadData();
                                    }
                                }
                                application_id = 0;
                                location.reload();


                            }
                        });
                    }
                });
            }else{
                alert("Empty fields are not allowed")
            }
        }
    </script>

    <!-- Date Picker Start -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />                    
    <!-- Date Picker END -->


    <script>
        $(function() {
            var start = moment().subtract(90, 'days');
            var end   = moment();
            
            function cb(start, end) {
                var start_end  = start+'_all_'+end;
                var startDate = '';
                var endDate   = '';

                if(start_end == 'NaN_all_NaN'){
                    $('#reportrange span').html('All Time');
                    // startDate = '1970-01-01 00:00:00'; 
                    // endDate   = '2050-12-31 23:59:59';
                    startDate = '1970-01-01'; 
                    endDate   = '2050-12-31';
                }else{
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    // startDate = start.format('YYYY-MM-DD')+' 00:00:00'; 
                    // endDate   = end.format('YYYY-MM-DD')+' 23:59:59';
                    startDate = start.format('YYYY-MM-DD'); 
                    endDate   = end.format('YYYY-MM-DD');
                }
                
                $('#daterange_picker_start').val(startDate);
                $('#daterange_picker_end').val(endDate);

            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                'All Time'  : ['all', 'all']
                }
            }, cb);
            cb(start, end);
        });
        $('#NotesModal').on('show.bs.modal', function(e) {
            $html = '';
            $("#i-"+$(e.relatedTarget).attr('applicant_id')).closest('div').parent().parent().find('.notes-li').each(function () {
                $html += $(this).prop('outerHTML');
            });
            ($(".notes-timeline").length) ?
            $("#view-notes-modal").html($html) :
            $("#view-notes-modal").html('<span>Notes not found.</span>');
        });

    </script>
@endpush