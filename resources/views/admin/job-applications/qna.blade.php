@forelse($answers as $answer)
    <div class="{{ $answer->question->is_qualifiable() ? ($answer->qualified() ? 'text-success' : 'text-danger') : '' }}">
        <strong>
            {{$answer->question->question}}
        </strong><br>
        <p class="text-muted">{{ ucfirst($answer->answer)}}</p>
    </div>
@empty
@endforelse