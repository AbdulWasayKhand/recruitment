<?php $temp_message = $message_body = \Illuminate\Support\Arr::get(\App\Option::getAll(company()->id), 'qualified_text'); ?>
<?php $temp_sms = $sms_body = \Illuminate\Support\Arr::get(\App\Option::getAll(company()->id), 'sms_qualified_text'); ?>
@if($application->job->workflow_id==0)
    @include('admin.job-applications.status-view')
@else
    <?php
    $interviewAction = $application->workflow_stage->workflow_actions('interview');
    if($interviewAction->count()){
        if($interviewAction[0]->sms_invite == 0){
            $sms_body = $temp_sms;
        }else{
            $sms_body = $interviewAction[0]->sms_invite_template->content ?? $temp_sms;
        }
        if($interviewAction[0]->email_invite == 0){
            $message_body = $temp_message;
        }else{
            $message_body = $interviewAction[0]->email_invite_template->content ?? $temp_message;
        }
    }
    ?>
    @include('admin.job-applications.workflow-view')
@endif
<div style="display=none" id="sms_body_div">{!! $sms_body !!}</div>
<script>
    var current_message_body = '{!! $message_body !!}';
    var current_sms_body = sms_body_div.innerHTML;
    $('#message_body').data("wysihtml5").editor.setValue(current_message_body);
    $('#sms_body').val(current_sms_body);
</script>