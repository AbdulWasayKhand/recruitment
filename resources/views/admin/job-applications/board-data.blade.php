<style>
    .cancel {
        background-color: #f44336 !important;
    }
    .sweet-alert .sa-icon.sa-warning{
        border-color: #4e5d9e !important;
    }
    .sweet-alert .sa-icon.sa-warning .sa-body{
        background-color: #4e5d9e !important;
    }
    .sweet-alert .sa-icon.sa-warning .sa-dot{
        background-color: #4e5d9e !important;
    }
    .sweet-alert p{
        font-weight: bold !important;
        font-size: 24px !important;
    }
</style>
<script>var pass_fail = false;</script>
<div class="modal fade bs-modal-md in" id="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">@lang('modules.jobApplication.editEmail&Text')</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label>Email Message</label>
            <textarea class="wysihtml5"  id="message_body" placeholder="Email  Message"></textarea>
            <hr />
            <label>Text Message</label>
            <textarea id="sms_body" class="form-control" placeholder="Text Message" ></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_message_modal">Close</button>
            <a href="javascript:beforeSend()" class="btn btn-success">
                @lang('modules.jobApplication.sendInteviewInvite')
            </a>
        </div>
        </div>
    </div>
</div>
@php($first_job = (session()->has('workflow_dropdown_job_id') && session()->get('workflow_dropdown_job_id') != 0) ? $jobs->find(session()->get('workflow_dropdown_job_id')) : $jobs->first())
@if($first_job->workflow_id==0)
<?php $columnName = $boardColumns->pluck('status', 'id'); ?>
@foreach($boardColumns as $key=>$column)
    <style>.qualified-circle{background:orange !important;}</style>
    <div class="board-column p-0" data-column-id="{{ $column->id }}" data-position="{{ $column->position }}">
        <div class="card" style="margin-bottom:0 !important;">
            <div class="card-body p-3">
                <h6 class="card-title pt-1 pb-1">
                    {{ ucwords($column->status) }}
                    <span class="badge badge-pill badge-primary text-white ml-auto" style="background: {{$column->color}};" id="columnCount_{{$column->id}}"> 
                        {{ count($column->applications) }}
                    </span>
                    <span class="pull-right">
                        <a data-toggle="dropdown" href="#" class="d-none">
                            <i class="fa fa-ellipsis-h" style="font-size: 18px;"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="javascript:editStatus({{$column->id}});">
                                <i class="fa fa-edit"></i> @lang('app.edit')
                            </a>
                            @if ($column->id > 5)
                            <a class="dropdown-item" href="javascript:deleteStatus({{$column->id}});">
                                <i class="fa fa-trash-o"></i> @lang('app.delete')
                            </a>
                            @endif
                        </div>
                    </span>
                </h6>
                <div class="card-text">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="custom-column panel-scroll custom-panel">
                                
                                <?php for($i = $column->jobApplications->count() ; $i > 0 ; $i--){ ?>
                                    <?php $application = $column->jobApplications[($i - 1)] ?> 
                                    <div class="panel panel-default lobipanel show-detail"
                                         data-widget="control-sidebar" data-slide="true"
                                         data-row-id="{{ $application->id }}" application_name="{{ $application->full_name }}"
                                         data-application-id="{{ $application->id }}"
                                         data-sortable="true"
                                         style="border-color: {{$column->color}};">
                                        <div class="panel-body">
                                            <h6 class="text-muted mb-0">{{ ucwords($application->job->title) }}</h6>
                                            <h5 class="text-bold mb-1">
                                                {{ ucwords($application->full_name) }}</h5>
                                            <div class="stars stars-example-fontawesome">
                                            </div>
                                            <p class="mt-2 mb-0 font-13">{{$application->qualified}}</p>
                                            <div class="font-13">
                                                <span>
                                                    @if(!is_null($application->schedule)  && $column->status == 'interview')
                                                        {{ Carbon\Carbon::parse($application->schedule->schedule_date)->format('M d, Y') }}
                                                    @else
                                                        {{ $application->created_at->format('M d, Y') }}
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="row mt-2" id="workflow-board-labels{{$application->id}}">
                                            @foreach($application->labels as $label)
                                                @if($label->label)
                                                <div id="w-label-{{$application->id}}-{{$label->label_id}}" class="d-inline-block badge ml-2" style="background:{{$label->label->label_color}}; color: #fff; font-weight: 700 !important;" title="{{$label->label->label_value}}">{{$label->label->label_value}}</div>
                                                @endif
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="panel panel-default lobipanel" data-sortable="true"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
@else
@php($stages = \App\WorkflowStage::where('workflow_id',$first_job->workflow->id)->where(function($q){
            $q->where('position','>',-1)->orWhereNull('position');
        })->orderBy('position','asc')->get()
)
@php($stages = $stages->merge( \App\WorkflowStage::where('workflow_id',$first_job->workflow->id)
                        ->where('position','<',0)->get() )
)
<?php $columnName = $stages->pluck('title', 'id'); ?>
@foreach($stages as $stage)
    <style>.qualified-circle{background:orange !important;}</style>
    <div class="board-column p-0" data-column-id="{{ $stage->id }}" data-position="">
        <div class="card" style="margin-bottom:0 !important;">
            <div class="card-body p-3">
                <h6 class="card-title pt-1 pb-1">
                    {{ ucwords($stage->title) }}
                    <span class="badge badge-pill badge-primary text-white ml-auto" style="background: ;" id="columnCount_{{$stage->id}}"> 
                        {{$stage->applications->where('job_id',$first_job->id)->count()}}
                    </span>
                    <span class="pull-right">
                        <a data-toggle="dropdown" href="#" class="d-none">
                            <i class="fa fa-ellipsis-h" style="font-size: 18px;"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="javascript:editStatus({{$stage->id}});">
                                <i class="fa fa-edit"></i> @lang('app.edit')
                            </a>
                            @if ($stage->id > 5)
                            <a class="dropdown-item" href="javascript:deleteStatus({{$stage->id}});">
                                <i class="fa fa-trash-o"></i> @lang('app.delete')
                            </a>
                            @endif
                        </div>
                    </span>
                </h6>
                <div class="card-text">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="custom-column panel-scroll custom-panel">
                                @foreach($first_job->workflowapplications->where('workflow_stage_id',$stage->id) as $application)
                                    <div class="panel panel-default lobipanel show-detail"
                                         data-widget="control-sidebar" data-slide="true"
                                         data-row-id="{{ $application->id }}"
                                         data-application-id="{{ $application->id }}"
                                         data-sortable="true"
                                         style="border-color: ;" application_name="{{ $application->full_name }}">
                                        <div class="panel-body">
                                            <h6 class="text-muted mb-0">{{ ucwords($application->job->title) }}</h6>
                                            <h5 class="text-bold mb-1">
                                                {{ ucwords($application->full_name) }}</h5>
                                            <div class="stars stars-example-fontawesome">
                                                <?php $is_id = \App\InterviewScheduleEmployee::where("interview_schedule_id", "=" , $application->interview->id)->where("user_id", "=", $user->id)->first()->id ?? 0 ?>
                                                {!! $application->interview->schedule_date && $application->workflow_stage_id == $application->interview->stage_id ? ucwords($application->workflow_stage->title) . ( $application->interview->user_accept_status == 'done' ?  "<br><i class='fa fa-calendar'></i>&nbsp;<span style='font-size:12px;'>". \Carbon\Carbon::parse($application->interview->schedule_date)->format('M d, Y')."</span><br><small " . ($application->interview->status == 'pass' ? "style='color:green'" : ($application->interview->status == 'fail' ? "style='color:red'" : '' )) . "><i>".ucwords($application->interview->status).'</i></small>' : ("<br>" .  ($application->interview->user_accept_status ? "<span style='font-size:12px;" .($application->schedule->employees->first()->id == $user->id ? "" : "display:none;" ) . "'><a class='btn btn-success btn-sm' style='color:white; font-size:10px;' onclick='passHim(" . $application->interview->id . ", " . $application->id . ")' onmouseover='pass_fail = true' onmouseout='pass_fail = false' >Pass</a> <a class='btn btn-danger btn-sm' style='color:white; font-size:10px;' onclick='failHim(" . $application->interview->id . ", " . $application->id . ")' onmouseover='pass_fail = true' onmouseout='pass_fail = false'>Fail</a></span>" : "<i><small style='color:#ffc107;" . ($is_id == 0 ? "display:none;" : "") . "'><button style='font-size:12px;' onclick='employeeResponse(". $is_id .")' onmouseover='pass_fail = true' onmouseout='pass_fail = false' class='btn btn-sm btn-success '>Accept</button></small></i>"))) : ucwords($application->workflow_stage->title) . "<br>" . ($application->workflow_stage->interview_action->id ? "<i><small style='color:#4caf50;'>Invited</small></i>" : '') !!}
                                            </div>
                                            <p class="mt-2 mb-0 font-13">{{$application->qualified}}</p>
                                            <div class="font-13">
                                                <span>
                                                    @if(!is_null($application->schedule))
                                                        {{ Carbon\Carbon::parse($application->schedule->schedule_date)->format('M d, Y') }}
                                                    @else
                                                        {{ $application->created_at->format('M d, Y') }}
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="row mt-2" id="workflow-board-labels{{$application->id}}">
                                            @foreach($application->labels as $label)
                                                @if($label->label)
                                                <div id="w-label-{{$application->id}}-{{$label->label_id}}" class="d-inline-block badge ml-2" style="background:{{$label->label->label_color}}; color: #fff; font-weight: 700 !important;" title="{{$label->label->label_value}}">{{$label->label->label_value}}</div>
                                                @endif
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="panel panel-default lobipanel" data-sortable="true"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
@endif
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
{{--Ajax Modal Start for fill Questionnaire--}}
    <div class="modal fade bs-modal-md in" id="questionnaireModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">@lang('menu.questionnaire')</span>
               </div>
               <div class="modal-body">
                   Loading...
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn default" data-dismiss="modal">Close</button>
                   <button type="button" class="btn blue">Save changes</button>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

<script>
        $('.wysihtml5').each(function(index, node) {
            $(node).wysihtml5({
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": true, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": true, //Button to change color of font
                stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)
            });
        })
        <?php
        $temp = [];
            foreach($columnName as $key => $value){
                $temp[$key] = ucwords($value);
            } 
        $columnName = json_encode($temp);
        ?>
        var columnNames = JSON.parse(@json($columnName));
        function employeeResponse(id, type = 'accept') {
            var msg;

            if (type == 'accept') {
                msg = "@lang('errors.acceptSchedule')";
            } else {
                msg = "@lang('errors.refuseSchedule')";
            }
            swal({
                title: "@lang('errors.areYouSure')",
                text: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "@lang('app.yes')",
                cancelButtonText: "@lang('app.cancel')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var url = "{{ route('admin.interview-schedule.response',[':id',':type']) }}";
                    url = url.replace(':id', id);
                    url = url.replace(':type', type);
                    // update values for all tasks
                    $.easyAjax({
                        url: url,
                        type: 'GET',
                        success: function (response) {
                            if (response.status == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        }
    function passHim(sid, aid){
        changeStatus('pass', sid, aid);
    }
    function failHim(sid, aid){
        changeStatus('fail', sid, aid);
    }
    function changeStatus(status, sid, aid){
        statusChangeConfirm(status, sid, aid , 'yes')
        // swal({
        //     title: "@lang('errors.askForCandidateEmail2')",
        //     text: "@lang('errors.settingMessageWillBeSent')",
        //     type: "info",
        //     showCancelButton: true,
        //     confirmButtonColor: "#0c19dd",
        //     confirmButtonText: "@lang('app.yes')",
        //     cancelButtonText: "@lang('app.no')",
        //     closeOnConfirm: true,
        //     closeOnCancel: true,
        //     closeOnEsc: false,
        //     closeOnClickOutside: true
        // }, function (isConfirm) {
        //     if (isConfirm) {
        //         statusChangeConfirm(status, sid, aid , 'yes')
        //     }
        //     else{
        //         statusChangeConfirm(status, sid, aid , 'no')
        //     }
        // });
    }


    function statusChangeConfirm(status, sid, aid, mailToCandidate) {
        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: "{{route('admin.interview-schedule.change-status')}}",
            container: '.modal-body',
            type: "POST",
            data: {'_token': token,'status': status,'id': sid,'mailToCandidate': mailToCandidate, 'applicant': aid},
            success: function (response) {
                location.reload();
            }
        })
    }


    var appId = 0;
    document.querySelectorAll('.lobipanel').forEach(function(e){
        e.addEventListener('click', function(){
            appId = e.getAttribute('data-application-id');
        });
    });
    $('.example-fontawesome').barrating({
        theme: 'fontawesome-stars',
        showSelectedRating: false,
        readonly:true,

    });

    $(function() {
        $('.bar-rating').each(function(){
            const val = $(this).data('value');

            $(this).barrating('set', val ? val : '');
        });
    });

    $('.example-fontawesome').barrating('set', '');

    // Send Reminder and notification to Candidate
    function sendReminder(id, type, status){
        var msg;

        if(type == 'notify'){
            if (status == 'hired') {
                msg = "@lang('errors.sendHiredNotification')";
            } else {
                msg = "@lang('errors.sendRejectedNotification')";
            }
        }
        else{
            msg = "@lang('errors.sendInterviewReminder')";
        }
        swal({
            title: "@lang('errors.areYouSure')",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "@lang('app.yes')",
            cancelButtonText: "@lang('app.cancel')",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.interview-schedule.notify',[':id',':type']) }}";
                url = url.replace(':id', id);
                url = url.replace(':type', type);

                // update values for all tasks
                $.easyAjax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                    }
                });
            }
        });
    }

    $(function () {
        $("body").on('click','.lbl',function () {
            var applicant_id = $(this).data('applicant_id');
            var label_id = $(this).data('id');
            var label = $(this).data('value');
            var label_color = $(this).data('color');
            var $w_label = $("#workflow-board-labels"+applicant_id).find('#w-label-'+applicant_id+'-'+label_id);
            if($w_label.length){
                $w_label.remove();
            }
            else{
                $("#workflow-board-labels"+applicant_id).append('<div id="w-label-'+applicant_id+'-'+label_id+'" class="d-inline-block badge ml-2" style="background:'+label_color+'; color: #fff; font-weight: 700 !important;" title="'+label+'">'+label+'</div>');
            }
        });
        // Getting Data of all colomn and applications
        boardStracture = JSON.parse('{!! $boardStracture !!}');

        var oldParentId, currentParentId, oldElementIds = [], i = 1;

        let draggingTaskId = 0;
        let draggedTaskId = 0;
        let missingElementId = 0;
        let currentApplicationId = 0;

        $('.lobipanel').on('dragged.lobiPanel', function (e, lobiPanel) {
                    var $parent = $(this).parent(),
                    $children = $parent.children('.show-detail');
                    var pr = $(this).closest('.board-column');

                    if (draggingTaskId !== 0) {
                        oldParentId = pr.data('column-id');
                    }
                    currentParentId = pr.data('column-id');

                    var boardColumnIds = [];
                    var applicationIds = [];
                    var prioritys = [];

                    $children.each(function (ind, el) {
                        boardColumnIds.push($(el).closest('.board-column').data('column-id'));
                        applicationIds.push($(el).data('application-id'));
                        prioritys.push($(el).index());
                    });

                    if (draggingTaskId !== 0) {
                        boardStracture[oldParentId] = [ ...applicationIds, currentApplicationId ];
                    } 
                    else {
                        const result = boardStracture[oldParentId].filter(el => el !== currentApplicationId);
                        boardStracture[oldParentId] = result;
                        boardStracture[currentParentId] = applicationIds;
                    }

                    if (oldParentId == 3 && currentParentId == 4) {
                        $('#buttonBox' + oldParentId + currentApplicationId).show();
                        var button = '';
                        $('#buttonBox' + oldParentId + currentApplicationId).html(button);
                        $('#buttonBox' + oldParentId + currentApplicationId).attr('id', 'buttonBox' + currentParentId + currentApplicationId);

                    } else if (oldParentId == 4 && currentParentId == 3) {
                        var timeStamp = $('#buttonBox' + oldParentId + currentApplicationId).data('timestamp');
                        var currentDate = "{{$currentDate}}";
                        if (currentDate < timeStamp) {
                            $('#buttonBox' + oldParentId + currentApplicationId).show();
                            var button = '';
                            $('#buttonBox' + oldParentId + currentApplicationId).html(button);
                        }
                        $('#buttonBox' + oldParentId + currentApplicationId).attr('id', 'buttonBox' + currentParentId + currentApplicationId);
                    } else {
                        $('#buttonBox' + oldParentId + currentApplicationId).hide();
                        $('#buttonBox' + oldParentId + currentApplicationId).attr('id', 'buttonBox' + currentParentId + currentApplicationId);
                    }

                    var startDate = $('#date-start').val();
                    var jobs = $('#jobs').val();
                    var search = $('#search').val();

                    if (startDate == '') {
                        startDate = null;
                    }

                    var endDate = $('#date-end').val();

                    if (endDate == '') {
                        endDate = null;
                    }
                    swal({
                        title: "",
                        text: 'Are you sure you want to move ' +$($("div[data-application-id='" +currentApplicationId+ "']")[0]).attr('application_name')+ ' to ' + columnNames[currentParentId] +'?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#4caf50",
                        confirmButtonText: "@lang('app.yes')",
                        cancelButtonText: "@lang('app.cancel')",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        
                        if(isConfirm){
                                $.easyAjax({
                                url: '{{ route("admin.job-applications.updateIndex") }}',
                                type: 'POST',
                                container: '.container-row',
                                data: {
                                    boardColumnIds: boardColumnIds,
                                    applicationIds: applicationIds,
                                    prioritys: prioritys,
                                    startDate: startDate,
                                    jobs: jobs,
                                    search: search,
                                    endDate: endDate,
                                    draggingTaskId: draggingTaskId,
                                    draggedTaskId: draggedTaskId,
                                    '_token': '{{ csrf_token() }}'
                                },
                                success: function (response) {
                                    if (draggedTaskId !== 0) {
                                        $.each( response.columnCountByIds, function( key, value ) {
                                            $('#columnCount_' + value.id).html((value.status_count));
                                            $('#columnCount_' + value.id).parents('.board-column').find('.lobipanel').css('border-color', value.color);
                                        });
                                    }
                                    setTimeout(function(){
                                        window.location.reload();
                                    }, 2000);
                                },
                                error: function (response) {
                                    setTimeout(function(){
                                        window.location.reload();
                                    }, 2000);
                                }
                            });
                            if(oldParentId != currentParentId){
                                $.easyAjax({
                                    url: "{{URL::to('/update-status')}}",
                                    type: 'POST',
                                    container: '.container-row',
                                    data: {
                                        previous_value: oldParentId,
                                        current_value: currentParentId,
                                        application_id: appId,
                                        '_token': '{{ csrf_token() }}' 
                                    },
                                    success: function (response) {
                                        appId = 0;
                                    }
                                });
                            }
                        }else{
                            window.location.reload();
                        }
                    });

                    if (draggingTaskId !== 0) {
                        draggedTaskId = draggingTaskId;
                        draggingTaskId = 0;
                    }



        }).lobiPanel({
            sortable: true,
            reload: false,
            editTitle: false,
            close: false,
            minimize: false,
            unpin: false,
            expand: false,
        });

        var isDragging = 0;
        $('.lobipanel-parent-sortable').on('sortactivate', function(){
            $('.board-column > .panel-body').css('overflow-y', 'unset');
            isDragging = 1;
        });
        $('.lobipanel-parent-sortable').on('sortstop', function(e){
            $('.board-column > .panel-body').css('overflow-y', 'auto');
            isDragging = 0;
        });
        $('.show-detail').click(function (event) {
            if ($(event.target).hasClass('notify-button') || pass_fail) {
                return false;
            }
            var id = $(this).data('application-id');
            draggingTaskId = currentApplicationId = id;

            if (isDragging == 0) {
                $(".right-sidebar").slideDown(50).addClass("shw-rside");

                var url = "{{ route('admin.job-applications.show',':id') }}";
                url = url.replace(':id', id);

                $.easyAjax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#right-sidebar-content').html(response.view);
                        }
                    }
                });
            }
        })
    });
    $(function () {
    $("body").on('change',"#job_select",function () { 
        $.get('{{url("admin/job_workflow")}}/'+$(this).val(),function(response){
                window.location.reload();
       });
    });
});
</script>