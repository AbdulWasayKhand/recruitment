@extends('layouts.app')

@push('head-script')
<style>
.todo-box2 {background-color: #fff;border-radius: 5px;border: 1px solid #d2d6de;box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.15);}
.saperator {border-bottom: 1px solid #d6d6d6;margin-bottom: 15px;}
.f-16{font-size:16px}
.main-score-board h1{color: #505050;}
.main-date .btn-success{background-color: #4f5c9d; border-color: #4f5c9d;}
.main-date .caret {display: inline-block; width: 0; height: 0; margin-left: 2px; vertical-align: middle; border-top: 4px dashed; border-top: 4px solid\9; border-right: 4px solid transparent; border-left: 4px solid transparent;}
.headertitle{margin-bottom: 20px;color: #fff;display: flex;justify-content: space-between;align-items: center;flex-wrap: wrap;border-radius: 5px 5px 0 0;}
.headertitle h5{color: #fff;padding: 13px 20px 11px;line-height: 1;font-size: 16px;margin: 0;font-weight:600}
.custom-nav-pill a{padding: 0.5rem 0.8rem;}
.custom-nav-pill .nav-link.active, .custom-nav-pill .show>.nav-link{background-color: rgb(0 0 0 / 8%);color: #6c757d;}
.custom-nav-pill .nav-link {background-color: rgb(244 246 249);}
.custom-nav-pill .nav-link:not(.active):hover{background-color: rgb(0 0 0 / 8%);color: #6c757d;}
#budgetTable > tbody > tr:last-child > td{color: White;font-weight: bold;background: #26a743;}

.funnelBar-container{position:relative;}
.funnelBar-container > div[class^="col"] {position: relative;z-index: 2;margin: 0 auto;border-top: 51px solid;border-left: 50px solid transparent;border-right: 50px solid transparent;text-align: center;box-sizing: border-box;}
.funnelBar-container >  div[class^="col"] > div{    margin-bottom: -34px;
    position: relative;
    z-index: 1;
    transform: translateY(-45px);
    font-size: 12px;
    font-weight: 500;
    line-height: 13px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

}
    /* .funnelBar-container >  div[class^="col"] > div > div:nth-child(1){font-size:10px;opacity: 0.6;} */
    .l-13{left:-13%}
.break{width: 94px;margin: 0 auto -32px !important;}
.data-column{position: absolute;top: 0;right: 0;left: 0;}
.data-column .row{height: 56px;position: relative;}
.data-column .row::before{    content: "";
    position: absolute;
    left: 0;
    right: 50%;
    height: 100%;
    background: #fff;
    z-index: 1;}
.data-contain {position: absolute;right: -14%;top: 13px;margin-bottom: 0;width: 116px;line-height: 14px;}
.data-contain span{font-size: 12px;}
.alice-blue-1{border-top-color: aliceblue !important;}
.alice-blue-2{border-top-color: #e2f0fd !important;}
.alice-blue-3{border-top-color: #cde5fb !important;}
.alice-blue-4{border-top-color: #aed7fd !important;}
.alice-blue-5{border-top-color: #8fc8fd !important;}
.red-filled{border-top-color: #dc3545 !important;}
.red-filled > div{color:#fff !important;}
.indicator{position: absolute;right: 103%;width: 1000%;top: 21px;height: 2px;background: #8fc8fd;}
.indicator.red{background: #dc3545 !important;}
.set-single-txt{position: relative;top: 13px;}
.small-text{font-size: 10px !important;opacity: 0.6;}
.calendar-wrapper {
    display: flex;
    align-items: center;
    justify-content: flex-end;
}
.calendar-container {
    background: #fff;
    cursor: pointer;
    border: 1px solid #ccc;
    padding: 1px;
}
.sheld2{
    z-index:4444;
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.3);
    color: white;
    font-size: 30px;
    text-align: center;
    padding-top: 20%;
    top:0;
    left:0;
}
</style>

@endpush

@section('content')
        <div class="sheld2">
            <div id="fliper">
                <img style="width:50px;" src="{{ url('/assets/')}}/loading.gif" />
                <br />
                <small><i>Please wait....</i></small>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-12 main-date">                
                <div class="mx-0 calendar-wrapper">
                    <div class="mr-2">
                        <select name="select_job" id="select_job" onchange="loadReports(this)">
                            @foreach($jobs_dropdown as $id => $title)
                                <option value="{{$id}}">{{$title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <div id="reportrange"  class="pull-left calendar-container">
                            <i class="fas fa-calendar-alt pr-1"></i>
                            <span></span> <b class="caret"></b>
                        </div>
                        
                        <input type="hidden" id="daterange_picker_start" value="">
                        <input type="hidden" id="daterange_picker_end" value="">
                    </div>
                </div> 
            </div>    
        </div>

        <div class="row mb-4">
            <div class="col">
                <div id="lower-box" class="todo-box2">
                    <div class="row mx-0 main-score-board">
                        <!-- <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold">{{$jobs}}</h1>
                                <p class="mb-1">Jobs</p>
                                <p class="mb-0"><i class="fa fa-briefcase"></i></p>
                            </div>
                        </div> -->
                        <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="total_applications">0</h1>
                                <p class="mb-1" id="total_applicant_stats">Applicants</p>
                                <p class="mb-0 f-16"><i class="fa fa-users"></i></p>
                            </div>
                        </div>
                        <!-- <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold">{{$openJobs}}</h1>
                                <p class="mb-1">Open Jobs</p>
                                <p class="mb-0 f-16"><i class="fa fa-briefcase"></i></p>
                            </div>
                        </div> -->
                        <!-- <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="jobsAdded">0</h1>
                                <p class="mb-1">Jobs Added</p>
                                <p class="mb-0 f-16"><i class="fas fa-clipboard-check"></i></p>
                            </div>
                        </div> -->
                        <!-- <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="jobsClosed">0</h1>
                                <p class="mb-1">Jobs Closed</p>
                                <p class="mb-0 f-16"><i class="fas fa-calendar-check"></i></p>
                            </div>
                        </div> -->
                        <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="totalQualified">0</h1>
                                <p class="mb-1"  id="total_qualified_stats">Qualified  <small id="total-qualified-percentage"></small></p>
                                <p class="mb-0 f-16"><i class="fas fa-clipboard-check"></i></p>
                            </div>
                        </div>
                        <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="totalUnQualified">0</h1>
                                <p class="mb-1"  id="total_unqualified_stats">Unqualified <small id="total-unqualified-percentage"></small></p>
                                <p class="mb-0 f-16"><i class="fa fa-briefcase"></i></p>
                            </div>
                        </div>
                        <!-- <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="avgScore">0</h1>
                                <p class="mb-1">Avg. Score</p>
                                <p class="mb-0 f-16"><i class="fa fa-users"></i></p>
                            </div>
                        </div> -->
                        <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="totalHired">0</h1>
                                <p class="mb-1"  id="total_hired_stats">Hired <small id="total-hired-percentage"></small></p>
                                <p class="mb-0 f-16"><i class="fas fa-id-card-alt"></i></p>
                            </div>
                        </div>
                        <div class="col p-3">
                            <div class="d-flex flex-column text-center justify-content-center">
                                <h1 class="mb-0 text-bold" id="totalRejected">0</h1>
                                <p class="mb-1"  id="total_rejected_stats">Rejected <small id="total-rejected-percentage"></small></p>
                                <p class="mb-0 f-16"><i class="fas fa-envelope-open-text"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #dc3545;">
                        <h5>Applicant Sources</h5>
                    </div>
                    <nav class="nav custom-nav-pill nav-pills justify-content-center">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-chart-pie"></i></a>
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-chart-bar"></i></a>
                    </nav>
                    <div class="tab-content mt-4" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                <div class="col-5 bar-ch-donut">
                                    <canvas id="donutChart" style="height:300px; min-height:300px"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                <div class="col-12 bar-source-chart">
                                    <canvas id="applicationSourceBarChart" style="height:300px;min-height:300px"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive m-t-4">
                        <table id="myTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>@lang('app.source')</th>
                                <th>@lang('modules.jobApplicationStatus.applicants')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pb-5">
            <div class="col-md-12">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #26a743;">
                        <h5>Status Report</h5>
                    </div>
                    <center><span>Showing number of applicants on different stage at this time</span></center>
                    <nav class="nav custom-nav-pill nav-pills justify-content-center">
                        <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home2" aria-selected="true"><i class="fas fa-chart-pie"></i></a>
                        <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile2" aria-selected="false"><i class="fas fa-chart-bar"></i></a>
                        {{--<a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false"><i class="fas fa-bars"></i></a>--}}
                    </nav>
                    <div class="tab-content mt-4" id="myTabContent2">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                <div class="col-12 bar-ch-donut-2">
                                    <canvas id="donutChart2" style="height:240px; min-height:240px"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                <div class="col-12 stacked-Bar-Chart-2">
                                    <canvas id="stackedBarChart2" style="height:240px; min-height:240px"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                            <div class="row m-0 pt-5 p-3 justify-content-center">
                                <div class="col-md-6 mb-4">
                                    <div class="progress-group">
                                        Applied
                                        <span class="float-right"><b>160</b>/200</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-primary" style="width: 80%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>
                                
                                <div class="col-md-6 mb-4">
                                    <div class="progress-group">
                                        Shortlisted
                                        <span class="float-right"><b>310</b>/400</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-danger" style="width: 75%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>

                                <div class="col-md-6 mb-4">
                                    <div class="progress-group">
                                        <span class="progress-text">Interviewed</span>
                                        <span class="float-right"><b>480</b>/800</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-success" style="width: 60%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>

                                <div class="col-md-6 mb-4">
                                    <div class="progress-group">
                                        Offered
                                        <span class="float-right"><b>250</b>/500</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-warning" style="width: 50%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>

                                <div class="col-md-12 mb-4">
                                    <div class="progress-group">
                                        Hired
                                        <span class="float-right"><b>250</b>/500</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-info" style="width: 50%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <center><span>Workflow statuses</span></center>
                    <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                        <div class="col-12 p-0">
                        <div class="table-responsive m-t-4">
                                <table id="myTable7" class="table table-striped">
                                    <!-- <thead>
                                    <tr>
                                        <th>@lang('app.source')</th>
                                        <th>@lang('modules.jobApplicationStatus.applied')</th>
                                        <th>@lang('modules.jobApplicationStatus.phoneScreen')</th>
                                        <th>@lang('app.interviewed')</th>
                                        <th>@lang('app.hired')</th>
                                        <th>@lang('modules.jobApplicationStatus.rejected')</th>
                                    </tr>
                                    </thead> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pb-5">    
            <div class="col-md-12">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #007aff;">
                        <h5>@lang('app.qualifiedVsUnqualified')</h5>
                    </div>
                    <nav class="nav custom-nav-pill nav-pills justify-content-center">
                        <a class="nav-link active" id="sources-tab" data-toggle="tab" href="#sourcesTab" role="tab" aria-controls="sourcesTab" aria-selected="true">Sources</a>
                        <a class="nav-link" id="weekdays-tab" data-toggle="tab" href="#weekdaysTab" role="tab" aria-controls="weekdaysTab" aria-selected="false">Days</a>
                    </nav>
                    <div class="tab-content mt-4" id="qUtabContent2">
                    <div class="tab-pane fade show active" id="sourcesTab" role="tabpanel" aria-labelledby="sources-tab">
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 bar-Chart">
                                        <canvas id="barChart" style="height: 300px;min-height: 300px;"></canvas>
                                    </div>
                                </div>
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 p-0">
                                        <div class="table-responsive m-t-4">
                                            <table id="myTable5" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>@lang('app.source')</th>
                                                    <th>@lang('app.qualified')</th>
                                                    <th>@lang('app.unqualified')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="weekdaysTab" role="tabpanel" aria-labelledby="weekdays-tab">
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 bar-Chart-daywise">
                                    <canvas id="barChartDaywise" style="height: 300px;min-height: 300px;"></canvas>
                                    </div>
                                </div>
                                    <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                        <div class="col-12 p-0">
                                            <div class="table-responsive m-t-4">
                                                <table id="daywiseTable" class="table table-striped w-100">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('app.day')</th>
                                                        <th>@lang('app.qualified')</th>
                                                        <th>@lang('app.unqualified')</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pb-5">    
            <div class="col-md-12">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #dc3545 !important;">
                        <h5>Hired Vs Rejected</h5>
                    </div>
                    <nav class="nav custom-nav-pill nav-pills justify-content-center">
                        <a class="nav-link active" id="sources-tab" data-toggle="tab" href="#sourcesTabHRS" role="tab" aria-controls="sourcesTabHRS" aria-selected="true">Sources</a>
                        <a class="nav-link" id="weekdays-tab" data-toggle="tab" href="#weekdaysTabHRS" role="tab" aria-controls="weekdaysTabHRS" aria-selected="false">Days</a>
                    </nav>
                    <div class="tab-content mt-4" id="qUtabContent2">
                    <div class="tab-pane fade show active" id="sourcesTabHRS" role="tabpanel" aria-labelledby="sources-tab">
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 bar-Chart-HRS">
                                        <canvas id="barChart_HRS" style="height: 300px;min-height: 300px;"></canvas>
                                    </div>
                                </div>
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 p-0">
                                        <div class="table-responsive m-t-4">
                                            <table id="myTable5_HRS" class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>@lang('app.source')</th>
                                                    <th>Hired</th>
                                                    <th>Rejected</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="weekdaysTabHRS" role="tabpanel" aria-labelledby="weekdays-tab">
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 bar-Chart-daywise-HRS">
                                    <canvas id="barChartDaywise_HRS" style="height: 300px;min-height: 300px;"></canvas>
                                    </div>
                                </div>
                                    <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                        <div class="col-12 p-0">
                                            <div class="table-responsive m-t-4">
                                                <table id="daywiseTable_HRS" class="table table-striped w-100">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('app.day')</th>
                                                        <th>Hired</th>
                                                        <th>Rejected</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pb-5">
   
           
            <div class="col-md-6" id="budget_view">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #26a743;">
                        <h5>Budget Report</h5>
                    </div>
                    <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                        <div class="col-12 p-0">
                        <div class="table-responsive m-t-4">
                                <table id="budgetTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>@lang('app.source')</th>
                                        <th>Daily Budget</th>
                                        <th>Cost<span style="color:transparent;">_</span>Occurred</th>
                                        <th>Applicants</th>
                                        <th>Cost per Applicant</th>
                                        <th>Hired</th>
                                        <th>Cost per Person<span style="color:transparent;">_</span>Hired</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #dc3545 !important;">
                        <h5><span id='stage_or_funnel'></span> Report</h5>
                    </div>
                    <div class="row">
                    <div class="col-12">
                        <h5 class="text-center" id='stage_or_funnel_desc'> - </h5>
                        </div>
                    </div>
                    <div class="d-flex flex-row m-0 pb-3 justify-content-center" style="overflow:hidden">
                        <div class="col-11 p-0 l-13">
                            <div class="d-flex flex-column justify-content-center funnelBar-container m-0" id="stage_bar">
                                <div class="data-column" id="stage_info"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6" id="funnel_report_for_workflow">
                <div id="lower-box" class="todo-box2">
                    <div class="headertitle" style="background-color: #dc3545 !important;">
                        <h5>Funnel Report</h5>
                    </div>
                    <div class="row">
                    <div class="col-12">
                        <h5 class="text-center">Drop Offs at different stages of the hiring process</h5>
                        </div>
                    </div>
                    <div class="d-flex flex-row m-0 pb-3 justify-content-center" style="overflow:hidden">
                        <div class="col-11 p-0 l-13">
                            <div class="d-flex flex-column justify-content-center funnelBar-container m-0" id="stage_bar_2">
                                <div class="data-column" id="stage_info_2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div  class="row pb-5">


        <div class="col-md-6" id="interview_box">
                <div  class="todo-box2">
                    <div class="headertitle" style="background-color: #007aff !important;">
                        <h5>Interview Report</h5>
                    </div>
                    <div class="tab-content mt-4" id="qUtabContent3">
                    <div class="tab-pane fade show active" id="sourcesTabInterview" role="tabpanel" aria-labelledby="sources-tab">
                                <div class="d-flex flex-row m-0 pb-3 justify-content-center">
                                    <div class="col-12 bar-Chart-Interview">
                                        <canvas id="barChart_interview" style="height: 300px;min-height: 300px;"></canvas>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha512-mf78KukU/a8rjr7aBRvCa2Vwg/q0tUjJhLtcK53PHEbFwCEqQ5durlzvVTgQgKpv+fyNMT6ZQT1Aq6tpNqf1mg==" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
    function funnel_filter(job_id, start_date, end_date){

        $.ajax({
            'url' : "{!! url('admin/reports/funnel-data') !!}",
            'type' : 'POST',
            'data' : {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id},
            success : function(response){
                if(response.type == 'Stage'){
                    funnel_report_for_workflow.style.display = 'block';
                }else{
                    funnel_report_for_workflow.style.display = 'none';
                }
                
                stage_or_funnel.innerHTML = response.type;
                stage_or_funnel_desc.innerHTML = response.desc;
                $width = 100 / (response.data.length + 3);
                $realwidth = 100;
                $co = 200;
                $b = $co;
                $r = $co;
                $g = $co;
                stage_bar.innerHTML = '';
                var d0 = document.createElement("DIV");
                stage_bar.appendChild(d0);
                var total = 0;
                response.data.forEach(function(e, i){
                    if(i == 0){
                        total = e.total;
                    }
                    var d1 = document.createElement("DIV");
                    var d2 = document.createElement("DIV");
                    var d3 = document.createElement("DIV");
                    var d5 = document.createElement("DIV");
                    var d6 = document.createElement('DIV');
                    d5.innerHTML = 'Dropped Off as';
                    d6.innerHTML = '&nbsp;'
                    var sp = document.createElement("SPAN");
                    var bld = document.createElement("B");

                    var d4 = document.createElement("DIV");
                    var para = document.createElement("P");
                    var sp1 = document.createElement("SPAN");
                    var sp2 = document.createElement("SPAN");
                    var sp3 = document.createElement("SPAN");
                    var sp4 = document.createElement("SPAN");

                    d0.classList.add('data-column');
                    d1.classList.add('col-9');
                    d1.style.width = $realwidth + '%';
                    d1.style.borderTopColor = 'rgba('+$r+', '+$g+', '+$b+', 1)';
                    d1.style.marginTop = '5px';   
                    d2.style.top = '8px';
                    d3.innerHTML = e.title;
                    

                    d4.classList.add('row');
                    bld.innerHTML = e.total;
                    if(e.extra){
                        bld.innerHTML = e.total + ' (' + ((e.total / total * 100) ? (e.total / total * 100 ).toFixed(2) : '0.00') + '%)';
                        para.classList.add('data-contain');
                        sp1.classList.add('d-block');
                        sp1.classList.add('small-text');
                        sp1.innerHTML = 'Selected as';
                        para.appendChild(sp1);

                        sp2.innerHTML = e.title;
                        para.appendChild(sp2);

                        sp3.classList.add('indicator');
                        sp3.style.background = 'rgba(' + $r +' , ' + $g + ', ' + $b + ', 1)';
                        para.appendChild(sp3);

                        sp4.classList.add('d-block');
                        sp4.innerHTML = '<b>' + e.previous + '</b> (<b>' + ((e.previous / total * 100) ? (e.previous / total * 100 ).toFixed(2) : '0.00') + '</b>%)';
                        if(i == 1){
                            sp2.innerHTML = 'Qualified';
                            sp4.innerHTML = '<b>' + response.qualified + '</b> (<b>' + ((response.qualified / total * 100) ? (response.qualified / total * 100 ).toFixed(2) : '0.00' )+ '</b>%)';
                        }
                        para.appendChild(sp4);

                    }

                    d2.appendChild(d3);
                    sp.appendChild(bld);
                    d2.appendChild(sp);
                    d1.appendChild(d2);
                    d4.appendChild(para);
                    d0.appendChild(d4);


                    stage_bar.appendChild(d1);
                    $r -= 8;
                    $g -+ 3;
                    $realwidth -= $width
                });
            }
        });
    }


    function funnel_filter_2(job_id, start_date, end_date){

        $.ajax({
            'url' : "{!! url('admin/reports/funnel-data-two') !!}",
            'type' : 'POST',
            'data' : {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id},
            success : function(response){
                $width = 100 / (response.data.length + 3);
                $realwidth = 100;
                $co = 200;
                $b = $co;
                $r = $co;
                $g = $co;
                stage_bar_2.innerHTML = '';
                var d0 = document.createElement("DIV");
                stage_bar_2.appendChild(d0);
                var total = 0;
                response.data.forEach(function(e, i){
                    if(i == 0){
                        total = e.total;
                    }
                    var d1 = document.createElement("DIV");
                    var d2 = document.createElement("DIV");
                    var d3 = document.createElement("DIV");
                    var d5 = document.createElement("DIV");
                    var d6 = document.createElement('DIV');
                    d5.innerHTML = 'Dropped Off as';
                    d6.innerHTML = '&nbsp;'
                    var sp = document.createElement("SPAN");
                    var bld = document.createElement("B");

                    var d4 = document.createElement("DIV");
                    var para = document.createElement("P");
                    var sp1 = document.createElement("SPAN");
                    var sp2 = document.createElement("SPAN");
                    var sp3 = document.createElement("SPAN");
                    var sp4 = document.createElement("SPAN");

                    d0.classList.add('data-column');
                    d1.classList.add('col-9');
                    d1.style.width = $realwidth + '%';
                    d1.style.borderTopColor = 'rgba('+$r+', '+$g+', '+$b+', 1)';
                    d1.style.marginTop = '5px';   
                    d2.style.top = '8px';
                    d3.innerHTML = e.title;

                    d4.classList.add('row');
                    bld.innerHTML = e.total;
                    if(e.extra){
                        bld.innerHTML = e.total + ' (' + ((e.total / total * 100) ? (e.total / total * 100 ).toFixed(2) : '0.00') + '%)';
                        para.classList.add('data-contain');
                        sp1.classList.add('d-block');
                        sp1.classList.add('small-text');
                        sp1.innerHTML = 'Selected as';
                        para.appendChild(sp1);

                        sp2.innerHTML = e.title;
                        para.appendChild(sp2);

                        sp3.classList.add('indicator');
                        sp3.style.background = 'rgba(' + $r +' , ' + $g + ', ' + $b + ', 1)';
                        para.appendChild(sp3);

                        sp4.classList.add('d-block');
                        sp4.innerHTML = '<b>' + e.previous + '</b> (<b>' + ((e.previous / total * 100) ? (e.previous / total * 100 ).toFixed(2) : '0.00') + '</b>%)';
                        if(i == 1){
                            sp2.innerHTML = 'Qualified';
                            sp4.innerHTML = '<b>' + response.qualified + '</b> (<b>' + ((response.qualified / total * 100) ? (response.qualified / total * 100 ).toFixed(2) : '0.00' )+ '</b>%)';
                        }
                        para.appendChild(sp4);

                    }

                    d2.appendChild(d3);
                    sp.appendChild(bld);
                    d2.appendChild(sp);
                    d1.appendChild(d2);
                    d4.appendChild(para);
                    d0.appendChild(d4);

                    stage_bar_2.appendChild(d1);
                    $r -= 8;
                    $g -+ 3;
                    $realwidth -= $width
                });
            }
        });
    }

    $('#myTable2').dataTable({
        order: [1, "desc"],
        searching: false,
        paging: false,
        // scrollY: 200,
        "bInfo" : false
    });
    $('#myTable4').dataTable({
        order: [1, "desc"],
        searching: false,
        paging: false,
        // scrollY: 200,
        "bInfo" : false
    });
    </script>
    <script>
        // For select 2
        $(".select2").select2({ width: '100%' });
        
        //-------------
        //- APPLICATION SOURCE DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        function sources_chart(facebook_total, facebook_per, google_total, google_per, indeed_total, indeed_per, linkedin_total, linkedin_per, others_total, others_per, direct_total, direct_per){
            $('#donutChart').remove();
            $('.bar-ch-donut').html('').append($('<canvas />').attr('id','donutChart').attr('style','height:300px; min-height:300px'));

            var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
            var donutData        = {
                labels: [
                    'Facebook '+facebook_per+'% ('+facebook_total+')', 
                    'Google '+google_per+'% ('+google_total+')',
                    'Indeed '+indeed_per+'% ('+indeed_total+')',
                    'linkedin '+linkedin_per+'% ('+linkedin_total+')',
                    'others '+others_per+'% ('+others_total+')',
                    'Direct '+direct_per+'% ('+direct_total+')',
                ],
                datasets: [
                    {
                        data: [facebook_total, google_total, indeed_total, linkedin_total, others_total, direct_total],
                        backgroundColor : ['#2777f2','#ea4f35', '#073a9b', '#3178b5', '#00a65a', 'rgb(117 117 117)'],
                    }
                ]
            }
            var donutOptions     = {
                maintainAspectRatio : false,
                responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var donutChart = new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions      
            });
        }


        //----------------------------------
        //- APPLICATION SOURCE  STATUSWISE DONUT CHART 2-
        //----------------------------------
        // Get context with jQuery - using jQuery's .get() method.
        function applicationSourceStatusWiseChart($labels, $colors, $data){
            $('#donutChart2').remove();
            $('.bar-ch-donut-2').html('').append($('<canvas />').attr('id','donutChart2').attr('style','height:240px; min-height:240px'));
            var donutChartCanvas = $('#donutChart2').get(0).getContext('2d');
            var donutData        = {
            labels: $labels,
            datasets: [
                {
                data: $data,
                // backgroundColor : ['#007bff', '#3c8dbc', '#28a745', '#17a2b8', '#ffc107', '#dc3545'],
                backgroundColor : $colors,
                }
            ]
            }
            var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
                legend : {
                    position: 'top'
                },
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions      
            })
        }

        //--------------------------------
        //- Application Source BAR CHART -
        //--------------------------------
        function applicationSourceBarChart($labels, $colors, $data) {
            $('#applicationSourceBarChart').remove();
            $('.bar-source-chart').html('').append($('<canvas />').attr('id','applicationSourceBarChart').attr('style','height:300px; min-height:300px'));
            var colorScheme = $colors
            var barChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Count',
                    backgroundColor     :  colorScheme,
                    borderColor         :  colorScheme,
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data
                    }
                ]
            }

            var stackedBarChartCanvas = $('#applicationSourceBarChart').get(0).getContext('2d')
            var stackedBarChartData = jQuery.extend(true, {}, barChartData)

            var stackedBarChartOptions = {
                legend: false,
                responsive              : true,
                    maintainAspectRatio     : false,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                }],
                                yAxes: [{
                                stacked: true
                            }]
                        }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar', 
                data: stackedBarChartData,
                options: stackedBarChartOptions
            })
        }

        //------------------------------------------------------
        //- Application Source Qualified Unqualified BAR CHART -
        //------------------------------------------------------
        function applicationSourceQualifiedUnqualifiedBarChart($labels,$data) {
            $('#barChart').remove();
            $('.bar-Chart').html('').append($('<canvas />').attr('id','barChart').attr('style','height: 300px; min-height: 300px;'));
            var areaChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Qualified',
                    backgroundColor     : '#45DC76',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data['qualified']
                    },
                    {
                    label               : 'Unqualified',
                    backgroundColor     : '#dd4b44',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : $data['unqualified']
                    },
                ]
            }

            var barChartCanvas = $('#barChart').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
            legend: false,
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: areaChartData,
            options: barChartOptions
            })
        }
//------------------------------------------------------
        //- Application Source Qualified Unqualified BAR CHART -
        //------------------------------------------------------
        function applicationInterviewBarChart($data) {
            $('#barChart_interview').remove();
            $('.bar-Chart-Interview').html('').append($('<canvas />').attr('id','barChart_interview').attr('style','height: 300px; min-height: 300px;'));
            var areaChartData = {
                labels  : ['Passed', 'Failed', 'Canceled'],
                datasets: [
                    {
                    backgroundColor     : ['#45DC76', '#dc3545', '#6c757d'],
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data
                    },
                    
                ]
            }

            var barChartCanvas = $('#barChart_interview').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            barChartData.datasets[1] = temp0

            var barChartOptions = {
            legend: false,
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false, 
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
                }
            }

            var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: areaChartData,
            options: barChartOptions
            })
        }


        //------------------------------------------------------
        //- Application Source Qualified Unqualified BAR CHART -
        //------------------------------------------------------
        function applicationSourceHRSBarChart($labels,$data) {
            $('#barChart_HRS').remove();
            $('.bar-Chart-HRS').html('').append($('<canvas />').attr('id','barChart_HRS').attr('style','height: 300px; min-height: 300px;'));
            var areaChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Hired',
                    backgroundColor     : '#45DC76',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data['hired']
                    },
                    {
                    label               : 'Rejected',
                    backgroundColor     : '#dd4b44',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : $data['rejected']
                    },
                ]
            }

            var barChartCanvas = $('#barChart_HRS').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
            legend: false,
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: areaChartData,
            options: barChartOptions
            })
        }



        //------------------------------------------------------
        //- Application Daywise Qualified Unqualified BAR CHART -
        //------------------------------------------------------
        function applicationDaywiseQualifiedUnqualifiedBarChart($labels,$data) {
            $('#barChartDaywise').remove();
            $('.bar-Chart-daywise').html('').append($('<canvas />').attr('id','barChartDaywise').attr('style','height: 300px; min-height: 300px;'));
            var areaChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Qualified',
                    backgroundColor     : '#45DC76',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data['qualified']
                    },
                    {
                    label               : 'Unqualified',
                    backgroundColor     : '#dd4b44',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : $data['unqualified']
                    },
                ]
            }

            var barChartCanvas = $('#barChartDaywise').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
            legend: false,
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: areaChartData,
            options: barChartOptions
            })
        }



                //------------------------------------------------------
        //- Application Daywise Qualified Unqualified BAR CHART -
        //------------------------------------------------------
        function applicationDaywiseHRSBarChart($labels,$data) {
            $('#barChartDaywise_HRS').remove();
            $('.bar-Chart-daywise-HRS').html('').append($('<canvas />').attr('id','barChartDaywise_HRS').attr('style','height: 300px; min-height: 300px;'));
            var areaChartData = {
                labels  : $labels,
                datasets: [
                    {
                    label               : 'Hired',
                    backgroundColor     : '#45DC76',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data['hired']
                    },
                    {
                    label               : 'Rejected',
                    backgroundColor     : '#dd4b44',
                    borderColor         : 'rgba(210, 214, 222, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(210, 214, 222, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data                : $data['rejected']
                    },
                ]
            }

            var barChartCanvas = $('#barChartDaywise_HRS').get(0).getContext('2d')
            var barChartData = $.extend(true, {}, areaChartData)
            var temp0 = areaChartData.datasets[0]
            var temp1 = areaChartData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0

            var barChartOptions = {
            legend: false,
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : false
            }

            var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: areaChartData,
            options: barChartOptions
            })
        }


        //-----------------------------------------------------
        //- APPLICATION SOURCE  STATUSWISE STACKED BAR CHART 2-
        //-----------------------------------------------------
        function applicationSourceStatusWiseBarChart($labels, $color, $data = []) {
            $('#stackedBarChart2').remove();
            $('.stacked-Bar-Chart-2').html('').append($('<canvas />').attr('id','stackedBarChart2').attr('style','height:240px; min-height:240px'));
            var barChartData2 = {
                labels  : $labels,
                datasets: [
                    {
                    // label               : 'Status Report',
                    backgroundColor     :  $color,
                    borderColor         :  $color,
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : $data
                    }
                ]
            }
                
            var stackedBarChartCanvas2 = $('#stackedBarChart2').get(0).getContext('2d')
            var stackedBarChartData2 = jQuery.extend(true, {}, barChartData2)

            var stackedBarChartOptions2 = {            
                legend: false,
                responsive              : true,
                    maintainAspectRatio     : false,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                }],
                                yAxes: [{
                                stacked: true
                            }]
                        }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas2, {
                type: 'bar', 
                data: stackedBarChartData2,
                options: stackedBarChartOptions2
            })
        }
    </script>

    <script>
        var is_first_visit = 0;
        function get_engyj_data(start_date = '', end_date = '', job_id){
            if(start_date == '' && end_date == ''){
                var start_date = "<?= date('Y-m-d', strtotime('-90 days'));?>";
                var end_date   = "<?= date('Y-m-d');?>";
            }
            var token = '{{ csrf_token() }}';
            $.ajax({
                type:"POST",
                url: "{{ url('admin/reports/get-engyj-data') }}",
                data:{'_token': token, start_date:start_date, end_date:end_date, 'job_id': job_id},
                dataType: "json",
                success:function(data){
                    funnel_filter(job_id, start_date, end_date);
                    funnel_filter_2(job_id, start_date, end_date);
                    // bar_chart(data[0]['days'], data[0]['days_val']);
                    // qualified_unqualified_chart(data[1]['qualified'], data[1]['qual_per'], data[1]['unqualified'], data[1]['unqual_per']);
                    sources_chart(data[2]['facebook_total'], data[2]['facebook_per'], data[2]['google_total'], data[2]['google_per'], data[2]['indeed_total'], data[2]['indeed_per'], data[2]['linkedin_total'], data[2]['linkedin_per'], data[2]['others_total'], data[2]['others_per'], data[2]['direct_total'], data[2]['direct_per']);
                    $col = ['#2777f2','#ea4f35', '#073a9b', '#3178b5', '#00a65a', 'rgb(117 117 117)'];
                    applicationSourceBarChart(data[0]['labels'],$col,data[0]['data']);
                    applicationSourceQualifiedUnqualifiedBarChart(data[3]['labels'],data[3]['data']);
                    applicationSourceHRSBarChart(data[3]['labels'],data[9]['data']);
                    applicationInterviewBarChart(data[11]);
                    // console.log(data[9]['data']);
                    // applicationSourceStatusWiseChart(data[4]['labels'],data[4]['applied_total'], data[4]['applied_per'], data[4]['phonescreen_total'], data[4]['phonescreen_per'], data[4]['interviewed_total'], data[4]['interviewed_per'], data[4]['hired_total'], data[4]['hired_per'], data[4]['rejected_total'], data[4]['rejected_per'],data[4]['data']);
                    // applicationSourceStatusWiseBarChart(data[4]['labels'],data[4]['data']);
                    stats = data[6];
                    jobs_options = data[7]['jobs_dropdown'];
                    applicationDaywiseQualifiedUnqualifiedBarChart(data[8]['labels'],data[8]['data']);
                    applicationDaywiseHRSBarChart(data[8]['labels'],data[10]['data']);
                    // $('#totalOpenings').html(stats['total_jobs_openings']);
                    $('#total_applications').html(stats['total_applications']);
                    $('#jobsAdded').html(stats['jobs_added']);
                    $('#jobsClosed').html(stats['jobs_closed']);
                    $('#totalQualified').html(stats['total_qualified']);
                    $('#totalUnQualified').html(stats['total_unqualified']);
                    $('#totalHired').html(stats['total_hired']);
                    $('#totalRejected').html(stats['total_rejected']);
                    $('#avgScore').html(stats['qualified_avg']);
                    $('#total-qualified-percentage').html(stats['total-qualified-percentage']);
                    $('#total-unqualified-percentage').html(stats['total-unqualified-percentage']);
                    $('#total-hired-percentage').html(stats['total-hired-percentage']);
                    $('#total-rejected-percentage').html(stats['total-rejected-percentage']);

                    $('#myTable7').html('');
                    if(stats['workflow_id'] == 0){
                        budget_view.style.display = 'block';
                        interview_box.style.display = 'none';
                        
                    }else{
                        budget_view.style.display = 'none';
                        interview_box.style.display = 'block';
                    }
                    $.ajax({
                        'url' : "{!! url('admin/reports/stage-data') !!}",
                        'type' : 'POST',
                        'data' : {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id},
                        success : function(response){
                            var html = '<thead><tr><th>Source</th>'; 
                            response.stages.forEach(function(e,k){
                                html += (stats['workflow_id']==0 && k!=0) ? '<th>At ' + e + '</th>' : '<th>' + e + '</th>';
                            });
                            html += '</tr></thead>';
                            html += '<tbody>'
                            response.sources.forEach(function(s,k){
                                html += '<tr class="'+(k%2 == 0 ? 'even': 'odd')+'">'; 
                                html += '<td><b>' + s['source'] + '</b></td>';
                                response.stages.forEach(function(e){
                                    html += '<td >' + s[e] + '</td>';
                                })
                                html += '</tr>';
                            });
                            html += "</tbody>"
                            $('#myTable7').html(html);
                            applicationSourceStatusWiseChart(response.stages, response.colors, response.data);
                            applicationSourceStatusWiseBarChart(response.stages, response.colors, response.data);
                        }
                    });

                    var my_table = $('#myTable').DataTable();
                    var my_table3 = $('#myTable3').DataTable();
                    var my_table5 = $('#myTable5').DataTable();
                    var daywiseTable = $('#daywiseTable').DataTable();
                    var my_table5_HRS = $('#myTable5_HRS').DataTable();
                    var daywiseTable_HRS = $('#daywiseTable_HRS').DataTable();
                    my_table.destroy(); my_table3.destroy(); my_table5.destroy(); daywiseTable.destroy(); my_table5_HRS.destroy(); daywiseTable_HRS.destroy();
                    

                    // Hiring Rejected status Daywise
                    $('#myTable5_HRS').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        // scrollY: 200,
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/HRS-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id},
                            error: function(error){
                                // console.log(error);
                            }
                        },
                        columns: [
                            { data: 'source', name: 'source' },
                            { data: 'hired', name: 'hired' },
                            { data: 'rejected', name: 'rejected' }
                        ]
                    });

                    // Hiring Rejected status Daywise
                    $('#daywiseTable_HRS').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/HRS-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date,'daywise':true, 'job_id': job_id }
                        },
                        columns: [
                            { data: 'day', name: 'day' },
                            { data: 'hired', name: 'hired' },
                            { data: 'rejected', name: 'rejected' }
                        ]
                    });

                    $('#myTable').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        // scrollY: 400,
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/sources-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id':job_id}
                        },
                        columns: [
                            { data: 'source', name: 'source' },
                            { data: 'applied', name: 'applied' }
                        ]
                    });
                    //new $.fn.dataTable.tables(myTable);

                    //Application Sorce Status Wise DataTable
                    $('#myTable3').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        scrollX: "200px",
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/status-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id}
                        },
                        columns: [
                            { data: 'source', name: 'source' },
                            { data: 'applied', name: 'applied' },
                            { data: 'phone_screen', name: 'phone_screen' },
                            { data: 'interviewed', name: 'interviewed' },
                            { data: 'hired', name: 'hired' },
                            { data: 'rejected', name: 'rejected' },
                            { data: 'total_applicants', name: 'total_applicants' }
                        ]
                    });

                    // Application Source Qualified UnQualified DataTable
                    $('#myTable5').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        // scrollY: 200,
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/qualified-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date, 'job_id': job_id}
                        },
                        columns: [
                            { data: 'source', name: 'source' },
                            { data: 'qualified', name: 'qualified' },
                            { data: 'unqualified', name: 'unqualified' }
                        ]
                    });

                    // Application Daywise Qualified UnQualified DataTable
                    $('#daywiseTable').dataTable({
                        order: [1, "desc"],
                        searching: false,
                        paging: false,
                        // scrollY: 200,
                        "bInfo" : false,
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/qualified-data') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date,'daywise':true, 'job_id': job_id }
                        },
                        columns: [
                            { data: 'day', name: 'day' },
                            { data: 'qualified', name: 'qualified' },
                            { data: 'unqualified', name: 'unqualified' }
                        ]
                    });

                    $('#budgetTable').DataTable().destroy();
                    // Budget DataTable
                    // start_date = (is_first_visit>1) ? start_date : '2020-04-01';
                    // end_date = (is_first_visit>1) ? end_date : '2020-05-01';
                    var selected_job = $("#get_job").find(':selected').val();
                    $('#budgetTable').dataTable({
                        searching: false,
                        paging: false,
                        // scrollY: 200,
                        "bInfo" : false,
                        scrollX: "200px",
                        serverSide: true,
                        "ajax": {
                            "url": "{!! url('admin/reports/budget-table') !!}",
                            "type": "POST",
                            "data": {'_token':'{{ csrf_token() }}','start_date':start_date,'end_date':end_date,'job_id':job_id},
                            "statusCode" : { 
                                    200: function(respose){
                                        $('.sheld2').hide();
                                }
                            }
                        },
                        columns: [
                            { data: 'source', name: 'source' },
                            { data: 'monthly_budget', name: 'monthly_budget' },
                            { data: 'total_days', name: 'total_days' },
                            { data: 'total_applicants', name: 'total_applicants' },
                            { data: 'cost_as_per_applicant', name: 'cost_as_per_applicant' },
                            { data: 'total_hred', name: 'total_hired' },
                            { data: 'cost_as_per_hired', name: 'cost_as_per_hired' }
                        ]
                    });
                    /*append dropdown options*/
                    $('#get_job').html('');
                    $.each(jobs_options,function(id,job) {
                        checked = (selected_job==id) ? 'selected' : '';
                        $('#get_job').append('<option value="'+id+'" '+checked+'>'+job+'</option>');
                    });
                    /*end append dropdown options*/
                }   
            });
        is_first_visit++;
        }
    </script>
    <!-- Date Picker Start -->
     <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
     <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />                    
    <!-- Date Picker END -->

    <script>
        // Date Picker
        // Date Picker
        function getRejectedData(start, end){
            $.ajax({
                type:"POST",
                url: "{{ url('admin/reports/getRejectedData') }}",
                data:{'_token': '{{ csrf_token() }}', start_date:start, end_date:end},
                dataType: "json",
                success:function(data){
                    data.forEach(function(e){
                        if(document.getElementById("r_t_" + e.key)){
                            var per = ((e.value/e.total) * 100).toFixed(2);
                            document.getElementById("r_t_" + e.key).innerHTML = e.value;
                            $("#r_p_"+e.key).html(e.percentage);
                        }
                        if(document.getElementById("ur_t_" + e.key2)){
                            var per = ((e.value2/e.total) * 100).toFixed(2);
                            document.getElementById("ur_t_" + e.key2).innerHTML = e.value2;
                            $("#ur_p_"+e.key2).html(e.percentage2);
                        }                        
                        // document.getElementById("p_t_" + e.key).innerHTML =  per != 'NaN' ? per : '0.00';
                    });
                }
            });
        }
        var start = moment().subtract(90, 'days');
        var end   = moment();
        function loadReports(elm){
            cb(start, end);
        }
        // $(function() {
            function cb(start, end, job_id) {
                $('.sheld2').show();
                var start_end  = start+'_all_'+end;
                var start_date = '';
                var end_date   = '';

                if(start_end == 'NaN_all_NaN'){
                    $('#reportrange span').html('All Time');
                    start_date = '1970-01-01 00:00:00'; 
                    end_date   = '2050-12-31 23:59:59';
                }else{
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    start_date = start.format('YYYY-MM-DD')+' 00:00:00'; 
                    end_date   = end.format('YYYY-MM-DD')+' 23:59:59';
                }
                
                $('#daterange_picker_start').val(start_date);
                $('#daterange_picker_end').val(end_date);
                
                get_engyj_data(start_date, end_date, select_job.value);
                getRejectedData(start_date, end_date,  select_job.value);
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                'All Time'  : ['all', 'all']
                }
            }, cb);
            // $("body").on('change',"select#get_job",function () {
            //     if($(this).val()){
            //         cb(start, end);
            //     }
            // });
            cb(start, end);
        // });
    </script>
@endpush