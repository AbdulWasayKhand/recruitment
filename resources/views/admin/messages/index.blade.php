@extends('layouts.app')

@push('head-script')
<style>
    .search-input .form-control-lg{padding: 0.8rem 1rem;font-size: 0.9rem;background: #e4e7ee;border: 0;border-radius: 10px;}
    .search-input .form-control-lg::placeholder{color: #929292;}
    .search-input .input-group-text{background-color: #e4e7ee;border: 0;border-radius: 10px;color: #bdbdbd;font-size: 1.2rem;}
    .main-chatbot .bb-1{border-bottom:1px solid #e4e7ee}
    .saperator{border-bottom:1px solid #e4e7ee}
    .chatter{background: #fff;padding: 20px 15px;border-radius: 10px;box-shadow: 1px 1px 3px rgb(0 0 0 / 13%);cursor: pointer;}
    .chatter-old{background: transparent;box-shadow:none}
    .chatter-name {background: antiquewhite;text-align: center;height: 35px;width: 35px;border-radius: 30px;font-size: 10px;padding-top: 9.8px;}
    .chatter-content .msg{font-size: 12px;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;color: #868686;font-weight: 400;}
    .chatter-content   .chatter-full-name {font-weight: 500;}
    .chatter-right{text-align: center;color: #868686;}
    .chatter-right .time{}
    .chatter-right .rate i{color: #d4d4d4;}
    .chatter-right .badge-primary{background-color: #4e5d9e;font-weight: 700 !important;}
    .customer-msg {border: 1px solid rgb(0 0 0 / 8%);border-radius: 5px;padding: 10px;font-size: 12px;font-weight: 400;color: #464646;line-height: 20px;word-wrap: break-word;}
    .customer-msg .msg-time {margin-bottom: 0;text-align: right;color: rgb(0 0 0 / 33%);}
    .agent-msg{width: calc( 100% - 40px );border-radius: 5px;padding: 10px;font-size: 12px;font-weight: 400;color: #464646;align-self: flex-end;line-height: 20px;background-color: #f5f6fa;word-break: break-word;}
    .agent-msg .msg-time {margin-bottom: 0;text-align: right;color: rgb(0 0 0 / 33%);}
    .write-msg-area{position:relative}
    .write-msg-area textarea{resize:none;height:100px;padding-right: 89px;}
    .write-msg-area button{position: absolute;bottom: 10px;right: 10px;}
    .write-msg-area button.btn-info{background-color: #4e5d9e;border-color: #4e5d9e;}
    .msg-content{height: calc(100vh - 365px);overflow-y:auto;overflow-x:hidden;}
    .agent-image{width: 35px;height: 35px;border-radius: 100px;margin-left: 5px;} 
    .border-3{border-width: 3px !important;}
    .chatter-info h3 {font-size: 16px;}
    .chatter-info h3 span {font-size: 9px;color: #868686;}
    .chatter-info p.phone {font-size: 12px;}
    .chatter-info p.email {font-size: 11px;color:blue;text-decoration:underline}
    .chatter-info p.address {font-size: 11px;}
    .table-applicant-info{box-shadow: 0px 2px 3px rgb(0 0 0 / 12%);}
    .applcation-status h3{font-size:11px;text-transform:uppercase;color: rgb(0 0 0 / 62%);}
    .table-applicant-info th{font-size:11px;font-weight:700}
    .table-applicant-info th span{font-size: 10px;font-weight: 700;background: green;color: #fff;border-radius: 23px;padding: 1px 7px;}
    .table-applicant-info td{font-size:11px;}
    .content-wrapper > .col-md-5 > .main-heading { display: none; }
    div::-webkit-scrollbar-thumb {height: 20%;}
    div::-webkit-scrollbar {display: block;width: 10px;}
    .hideMe{
        display: none;
    }
</style>
@endpush

@section('content')
<div>
    @if($twilio_number != '')
        <h3>@lang('modules.textMessages.urCustomNumber') : {{ $twilio_number }}</h3>
    @endif
</div>
@if($call_forward_number)
<div class="row">
    <div class="col-md-12">
        <label>@lang('modules.textMessages.callsToCustomNumber')</label> : <label>{{$call_forward_number}}</label> 
        {{--<input type="text" class="form-control" placeholder="Enter Custom Number" id="call_forward" value="{{$call_forward_number}}">--}}
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12">
        <label>Please Contact us if you would like your Custom Number to redirect to your office number</label> 
    </div>
</div>
@endif
<!--Chat Bot UI Start-->
<div class="col-12 px-0 main-chatbot" style="background:#fff">
    <div class="row mx-0">
        <div style="display:grid;" class="col-md-4">
            <input type="text" placeholder="Search" onkeyup="searchMe()" id="search_here" style="max-height: 40px;"/>
        <div class="col-md-4 pr-4"  style="min-width:100%;background-color:#f5f6fa;height: calc(100vh - 210px); display: inline-block; overflow: auto;">
            @foreach ($applicants as $applicant)
            @if($applicant->full_name)
            <div class="chatter @if(!$loop->first) chatter-old @endif my-3 hasToHide " id="A_{{$applicant->id}}" data-id="{{ $applicant->id }}" >
                <div class="row">
                    <div class="col-2">
                        <div class="chatter-name">{{strtoupper($applicant->full_name)[0]}}</div>
                    </div>
                    <div class="col-8">
                        <div class="chatter-content">
                            <div class="chatter-full-name">
                                {{ $applicant->full_name }}
                                <br><small>{{$applicant->phone}}</small>
                                @if($applicant->conversation()->count()) 
                                @else
                                    <br><small><i>No Conversation Yet.</i></small> 
                                @endif
                            </div>
                            <div class="msg"></div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="chatter-right">
                            {{-- <div class="time"></div> --}}
                            {{-- <div class="msg-alert badge badge-primary">1</div> --}}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
        </div>
        <div class="col-md-8">
            <div class="row" id="message-single-thread">
                
            </div>
        </div>
        
    </div>
</div>
<!--Chat Bot UI End-->
@endsection

@push('footer-script')
<script>
    var all_app = [];
    @foreach ($applicants as $applicant)
        all_app.push({"id": "A_{{$applicant->id}}", "name":"{{$applicant->full_name}}"});
    @endforeach
    function searchMe(){
        $(".hasToHide").removeClass('hideMe');
        if(search_here.value != ''){
            // $(".hasToHide").addClass('hideMe');
            all_app.forEach(function (e){
                if(!e.name.includes(search_here.value)){
                    $elm = document.getElementById(e.id);
                    if($elm){
                        $($elm).addClass('hideMe');
                    }
                }
            });
        }
    }

    $(document).ready(function() {
        $('.main-chatbot').on('click', '.chatter', function(e) {
            e.preventDefault();
            $(this).parent().children()
                        .not('.saperator')
                        .not('.chatter-old')
                        .addClass('chatter-old');
            $(this).removeClass('chatter-old');
            
            var applicant_id = $(this).data('id');
            var _token = '{{ csrf_token() }}';

            $.easyAjax({
                type: 'POST',
                url: '{{ route("admin.messages.fetch") }}',
                data: {'applicant_id':applicant_id, '_token':_token},
                success: function (response) {
                    $('#message-single-thread').html(response.data);
                }
            });
        });

        $('.chatter').first().click();
    });
    $('body').on('click','.copy_applicant_phone',function(){
        var copyPhone = $.trim($(this).closest('p').text());
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -1000px; top: -1000px";
        tempInput.value = copyPhone;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        console.log("Copied the text:", tempInput.value);
        document.body.removeChild(tempInput);
    });

</script>
@endpush