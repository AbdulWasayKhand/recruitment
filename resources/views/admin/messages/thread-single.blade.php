<style>
    .no_converssion {
        background-color: #6c757d;
        position: absolute;
        margin-left: 20%;
        margin-top: 10px;
        padding: 5px;
        border-radius: 3px;
        color: white;
        font-size: 10px;
    }
</style>
@if(!$messages->count())
<div class="no_converssion"><i>No Conversation Yet.</i></div>
@endif
<div class="col-md-7 px-0 border-right">
    <div class="row mx-0 p-4">
        <div class="col-12 px-0 my-2 msg-content" id="msg_content">
            <div class="row flex-column align-items-start mx-0" id="messages-container">
                @if($messages->count())
                    @foreach($messages as $message)
                         @if($message->message_type == 'outbound')
                        <div class="col-9 my-2 agent-msg">
                            {{ $message->message }} 
                            <p class="msg-time">{{ $message->updated_at->timezone($user->company->timezone)->format('M d, Y (h:i A)') }} <i class='fas {{ $message->status == "bounce" ? "fa-check" : "fa-check-double"}}'></i></p>
                        </div>
                        @else
                        <div class="col-9 my-2 customer-msg">
                            {{ $message->message }} 
                            <p class="msg-time">{{ $message->updated_at->timezone($user->company->timezone)->format('M d, Y (h:i A)') }}</p>
                        </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-12 px-0 my-2 write-msg-area">
            <textarea name="write_msg" id="write_msg" class="form-control" placeholder="Write a reply"></textarea>
            <button class="msg-btn btn btn-info btn-sm" data-phone="{{ $applicant->phone }}" data-id="{{ $applicant->id }}"><i class="fa fa-send"></i> Send</button>
        </div>
    </div>
</div>

<div class="col-md-5 p-3">
    <div class="chatter-info mb-3">
        <h3 class="mb-0">{{ $applicant->full_name }} </h3> {{-- <span>30 year</span> --}}
        <p class="phone mb-0">{{ $applicant->phone }} <a href="javascript:void(0);" class="copy_applicant_phone"><i class='fa fa-copy'></i></a></p>
        <p class="email mb-0"><a href="mailto:{{ $applicant->email }}">{{ $applicant->email }}</a></p>
        {{--<p class="start-rating mb-1">
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
        </p>--}}
        <p class="resume-btn mb-0">
            @if ($applicant->resume_url)
                <a target="_blank" href="{{ $applicant->resume_url }}" class="btn btn-sm btn-info p-hide">@lang('app.view') @lang('modules.jobApplication.resume')</a>
            @endif
        </p>
    </div>
    <div class="applcation-status">
        <h3>Active Applications</h3>
        <table class="table table-responsive table-applicant-info table-sm">
            <tr>
                <th colspan="2" class="border-top border-3 border-success">In-{{$applicant->job->title}} 

                </th>
            </tr>
            <tr>
                <td>Applied</td>
                <td>{{ $applicant->created_at->format('m/d/Y') }} ({{ $applicant->created_at->diffForHumans() }})</td>
            </tr>
            <?php
                $q = explode(' (', $applicant->qualified)[0];
                $color = ($q == 'N/A' ? 'black' : ($q == 'UNQUALIFIED' ? '#dd4b44' : '#28a745' )); 
            ?>
            <tr style="color:{{$color}};">
                <td>Qualification</td>
                <td>{{$applicant->qualified}}</td>
            </tr>
            <tr>
                <td>Source</td>
                <td>{{ucfirst($applicant->source)}}</td>
            </tr>
        </table>
    </div>
</div>
<script>
    // var objDiv = document.getElementById("msg_content");
    msg_content.scrollTop = msg_content.scrollHeight;
// window.addEventListener('load', function() {
    $('.write-msg-area .msg-btn').on('click', function (e) {
        var applicant_id = $(this).data('id');
        var phone = $(this).data('phone');
        var message = $('.write-msg-area [name=write_msg]').val();
        
        var _token = '{{ csrf_token() }}';
        $.easyAjax({
            type: 'POST',
            url: '{{ route("admin.messages.send") }}',
            data: {applicant_id, phone, message, '_token':_token},
            success: function (response) {
                if(response.status == 'success') {
                    var _html = `<div class="col-9 align-self-end my-2">
                                    <div class="row m-0">
                                        <div class="agent-msg">
                                            ${message}
                                            <p class="msg-time">${response.data.time}</p>    
                                        </div>
                                    </div>
                                </div>`;
                    $('.write-msg-area [name=write_msg]').val('');
                    $('#messages-container').append(_html);
                }
            }
        });
    })
// });
</script>
