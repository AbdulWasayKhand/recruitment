@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/switchery/dist/switchery.min.css') }}">
@endpush


@section('content')
    @php
        $requiredColumns = [
            'gender' => __('modules.front.gender'),
            'dob' => __('modules.front.dob'),
            'country' => __('modules.front.country')
        ];
        $sectionVisibility = [
            'profile_image' => __('modules.jobs.profileImage'),
            'resume' => __('modules.jobs.resume'),
            'cover_letter' => __('modules.jobs.coverLetter'),
            'terms_and_conditions' => __('modules.jobs.termsAndConditions')
        ];
    @endphp
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title form-sub-heading mb-4">@lang('app.edit')</h4>

                    <form class="ajax-form" method="POST" id="createForm">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            
                            <div class="col-md-8 mt-3">

                                <div class="form-group">
                                    <label class="d-block">@lang('menu.jobCategories')</label>
                                    <select name="category_id" id="category_id"
                                            class="form-control">
                                            <option value="">@lang('app.choose') @lang('menu.jobCategories')</option>
                                        @foreach($categories as $category)
                                            <option
                                                    @if($category->id == $job->category_id) selected @endif
                                            value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="col-md-8 mt-3">

                                <div class="form-group">
                                    <label class="d-block">@lang('modules.jobs.jobTitle')</label>
                                    <input type="text" style="height:44px;" class="form-control form-control-lg" name="title" value="{{ $job->title }}">
                                </div>

                            </div>
                            <div class="col-md-8 mt-3">

                                <div class="form-group">
                                    <label class="d-block">@lang('modules.jobs.jobDescription')</label>
                                    <textarea class="form-control" id="job_description" name="job_description" rows="15" placeholder="Enter text ...">{!! $job->job_description !!}</textarea>
                                </div>

                            </div>
                            <div class="col-md-8 mt-3">

                                <div class="form-group">
                                    <label class="d-block">@lang('modules.jobs.jobRequirement')</label>
                                    <textarea class="form-control" id="job_requirement" name="job_requirement" rows="15" placeholder="Enter text ...">{!! $job->job_requirement !!}</textarea>
                                </div>

                            </div>
                            @if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == 'stage.engyj.com' || $_SERVER['SERVER_NAME'] == 'local.recruit.com' || in_array(company()->id, [15, 16, 43, 44, 48, 22, 47, 50, 21, 51, 12, 53, 46]))
                            <div class="col-md-12 mt-3" style="display: none;">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="d-block">@lang('app.workflow')</label>
                                            <select name="workflow_id" id="workflow" class="form-control">
                                                <option>Default</option>
                                                @foreach($workflows as $workflow)
                                                <option value="{{$workflow->id}}" @if($workflow->id==$job->workflow_id) selected @endif>{{$workflow->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6 d-none">
                                                <div class="form-group">
                                                    <label class="d-block">@lang('modules.jobs.totalPositions')</label>
                                                    <input type="number" class="form-control" name="total_positions" id="total_positions" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 mt-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('menu.skills')</label> <a style="padding: 0px 4px 0px 4px;" href="javascript:;" data-target="#create-skill-modal" data-toggle="modal" class="btn btn-sm btn-primary">Add</a>
                                            <select class="select2 m-b-10 select2-multiple" id="job_skills" style="width: 100%; " multiple="multiple"
                                                    data-placeholder="@lang('app.add') @lang('menu.skills')" name="skill_id[]">
                                                @foreach($skills as $skill)
                                                    <option
                                                            @foreach($job->skills as $jskill)
                                                                @if($skill->id == $jskill->skill_id)
                                                                    selected
                                                                @endif
                                                            @endforeach
                                                            value="{{ $skill->id }}">{{ ucwords($skill->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="d-block">@lang('menu.locations')</label>
                                            <select class="select2 m-b-10 select2-multiple" id="job_locations" style="width: 100%; " multiple="multiple"
                                                    data-placeholder="@lang('app.add') @lang('menu.locations')" name="location_id[]">
                                            @foreach($locations as $location)
                                                <option
                                                        @if(in_array($location->id, explode(',', str_replace('"', '', str_replace(']', '', str_replace('[', '', $job->location_id)))))) selected @endif
                                                        value="{{ $location->id }}">{{ ucfirst($location->location_name). ' ('.$location->country->country_code.')' }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="d-block">@lang('app.startDate')</label>
                                                    <input type="text" class="form-control" id="date-start" value="{{ $job->start_date->format('m-d-Y') }}" name="start_date">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="d-block">@lang('app.endDate')</label>
                                                    <input type="text" class="form-control" id="date-end" name="end_date" value="{{ $job->end_date->format('m-d-Y') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="d-block">@lang('app.status')</label>
                                            <select name="status" id="status" class="form-control">
                                                <option
                                                        @if($job->status == 'active') selected @endif
                                                        value="active">@lang('app.active')</option>
                                                <option
                                                        @if($job->status == 'inactive') selected @endif
                                                value="inactive">@lang('app.inactive')</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="d-block" for="meta-title">@lang('modules.jobs.metaTitle')</label>
                                            <input type="text" id="meta-title" class="form-control" name="meta_title" value="{{ $job->meta_details['title'] }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="d-block" for="meta-description">@lang('modules.jobs.metaDescription')</label>
                                            <textarea id="meta-description" class="form-control" name="meta_description" rows="3">{{ $job->meta_details['description'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h2 class="form-sub-heading">@lang("modules.jobs.qualificationTitle")</h2>
                                        <div class="form-group">
                                            <label class="d-block" for="eligibility_percentage">What percentage will make the Candidate Qualify?</label>
                                            <input type="number" name="eligibility_percentage" class="form-control" value="{{ $job->eligibility_percentage }}" min="0" max="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span style="font-size:20px; font-weight:bold;">@lang('menu.setBudget')</span>
                                            <br/>
                                            <span>Please enter your <b style="font-weight:bold">daily</b> advertisement budget.</span>
                                            <br/><br/>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="form-group position-relative">
                                                        <label for="address">@lang('modules.budget.direct')</label>
                                                        <span class="input-addon-left"><i class="fa fa-dollar-sign"></i></span>
                                                        <input type="text" value="{{$budget->direct}}" name="direct" class="form-control input-budget">
                                                        <span class="input-addon-right">.00</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group position-relative">
                                                        <label for="address">@lang('modules.budget.indeed')</label>
                                                        <span class="input-addon-left"><i class="fa fa-dollar-sign"></i></span>
                                                        <input type="text" value="{{$budget->indeed}}" name="indeed" class="form-control input-budget">
                                                        <span class="input-addon-right">.00</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group position-relative">
                                                        <label for="address">@lang('modules.budget.facebook')</label>
                                                        <span class="input-addon-left"><i class="fa fa-dollar-sign"></i></span>
                                                        <input type="text" value="{{$budget->facebook}}" name="facebook" class="form-control input-budget">
                                                        <span class="input-addon-right">.00</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group position-relative">
                                                        <label for="address">@lang('modules.budget.google')</label>
                                                        <span class="input-addon-left"><i class="fa fa-dollar-sign"></i></span>
                                                        <input type="text" value="{{$budget->google}}" name="google" class="form-control input-budget">
                                                        <span class="input-addon-right">.00</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group position-relative">
                                                        <label for="address">@lang('modules.budget.linkedin')</label>
                                                        <span class="input-addon-left"><i class="fa fa-dollar-sign"></i></span>
                                                        <input type="text" value="{{$budget->linkedin}}" name="linkedin" class="form-control input-budget">
                                                        <span class="input-addon-right">.00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 mt-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4 class="m-b-0 m-l-10 box-title form-sub-heading">@lang('menu.internal_questionnaires')</h4>
                                            <p class="mb-2">@lang('modules.questionnaire.jobDescp')</p>
                                            <select id="select_questionnaire"
                                                    class="form-control select2 custom-select" name="questionnaire_id">
                                                <option value="">@lang('app.choose')</option>
                                                @foreach($questionnaires as $questionnaire)
                                                <option {{$job->questionnaire_id==$questionnaire->id ? 'selected' : ''}} value="{{$questionnaire->id}}">{{ucFirst($questionnaire->name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 mt-3">
                                @if(count($questions) > 0)
                                <h4 class="m-b-0 m-l-10 box-title form-sub-heading">@lang('modules.front.bot_questionnaire')</h4>
                                <p class="mb-2" style="">@lang('modules.jobs.qualificationDiscp2')</p>
                                @endif
                                @forelse($questions as $question)
                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <label class="">
                                            <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                <input  @if(in_array($question->id, $jobQuestion)) checked @endif type="checkbox" value="{{$question->id}}" name="question[]" class="flat-red"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            {{ ucfirst($question->question)}} @if($question->required == 'yes')(@lang('app.required'))@endif
                                        </label>
                                    </div>
                                </div>
                                @empty
                                @endforelse
                            </div>

                            <div class="col-md-8 mt-3">
                                <div id="columns">
                                    <label class="d-block" style="font-weight:700 !important">@lang('app.askApplicantsFor')</label>
                                    <div class="form-group form-group-inline">
                                        @foreach ($job->required_columns as $key => $value)
                                            <label class="mr-4">
                                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                    <input  @if($value) checked @endif type="checkbox" value="{{ $key }}" name="{{ $key }}" class="flat-red"  style="position: absolute; opacity: 0;">
                                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                </div>
                                                {{ ucfirst(__($requiredColumns[$key])) }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>        
                            </div>

                            <div class="col-md-8 mt-3">
                                <div id="columns">
                                    <label class="d-block" style="font-weight:700 !important">@lang('modules.jobs.sectionVisibility')</label>
                                    <div class="form-group form-group-inline">
                                        @foreach ($job->section_visibility as $key => $value)
                                        <label class="mr-4">
                                            <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false"
                                                style="position: relative;">
                                                <input @if($value == 'yes') checked @endif type="checkbox" value="yes"
                                                    name="{{ $key }}" class="flat-red"
                                                    style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper"
                                                    style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            {{ ucfirst(__($sectionVisibility[$key])) }}
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 mt-3">
                                <div id="columns">
                                    <div class="form-group form-group-inline">
                                        <label class="mr-4">
                                            <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;">
                                                <input  @if($job->default_workflow_view_job) checked @endif type="checkbox" name="default_workflow_view_job" value="{{$job->id}}" class="flat-red"  style="position: absolute; opacity: 0;">
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            @lang('modules.jobs.setDefaultWorkflowView')
                                        </label>
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-12 mt-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="d-block">@lang('menu.facebookLead')</label>
                                            <select id="template_search"
                                                    class="form-control select2 custom-select" name="fb_lead_id">
                                                    <option value="">@lang('app.choose')</option>
                                                    @foreach($leads as $template)
                                                        <option value="{{$template->lead_id}}" @if($job->fb_lead_id == $template->lead_id) selected @endif>{{$template->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <button type="button" id="save-form" class="btn btn-success mt-3"><i
                                    class="fa fa-check"></i> @lang('app.save')</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-lg in" id="create-skill-modal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <form id="createSkillForm" class="row">
                        <div class="form-group col-md-12">
                            <label>@lang('menu.jobCategories')</label>
                            <select name="category_id" id="category_id"
                                    class="form-control custom-select">
                                    <option value="">@lang('menu.unCategorized')</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div id="education_fields" class="form-group col-md-9 nopadding">
                            <div class="input-group">
                                <input type="text" name="name[]" class="form-control"
                                    placeholder="@lang('menu.skills') @lang('app.name')" value="">
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="button" id="add-more"><i
                                            class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="from_admin" value="1">
                        @csrf
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue" id="save-skill-form">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
        })

        // For select 2
        $(".select2").select2();

        $('#date-end').bootstrapMaterialDatePicker({ weekStart : 0, time: false, format: 'MM-DD-YYYY' });
        $('#date-start').bootstrapMaterialDatePicker({ weekStart : 0, time: false, format: 'MM-DD-YYYY' }).on('change', function(e, date)
        {
            $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
        });

        var jobDescription = $('#job_description').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": true, //Button to change color of font
            stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)

        });

        $('#job_requirement').wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": true, //Button to change color of font
            stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)

        });

        $('#category_id').change(function () {

            var id = $(this).val();

            var url = "{{route('admin.job-categories.getSkills', ':id')}}";
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                success: function (response) {
                    $('#job_skills').html(response.data);
                    // $(".select2").select2();
                }
            })
        });


        $('#save-form').click(function () {
            var data = $('#createForm').serializeArray();
            var date_start = $("#date-start").val().split('-');
            var date_end   = $("#date-end").val().split('-');

            data.forEach(function(item) {
                if(item.name == 'start_date'){
                    item.value = date_start[2]+'-'+date_start[0]+'-'+date_start[1];
                }

                if(item.name == 'end_date'){
                    item.value = date_end[2]+'-'+date_end[0]+'-'+date_end[1];
                }    
            });
            
            $.easyAjax({
                url: '{{route('admin.jobs.update', $job->id)}}',
                container: '#createForm',
                type: "POST",
                redirect: true,
                data: data
            })
        });

        var room = 1;
        $('#add-more').click(function () {
            room++;

            var divtest = `
                <div class="mt-3 removeclass${room}">
                    <div class="input-group">
                        <input type="text" name="name[]" class="form-control" placeholder="@lang('menu.skills') @lang('app.name')">
                        <div class="input-group-append"> 
                            <button class="btn btn-danger" type="button" onclick="remove_education_fields(${room});">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                </div>`;
                
            $('#education_fields').append(divtest);
            $(`.removeclass${room} input`).focus();
        })

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }

        $('#save-skill-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.skills.store')}}',
                container: '#createSkillForm',
                type: "POST",
                redirect: true,
                data: $('#createSkillForm').serialize(),
                success: function(response) {
                    if(response.skills) {
                        response.skills.map(skill => {
                            $('#job_skills').append(`<option value="${skill.id}">${skill.name}</option>`);
                        });

                        setTimeout(() => {
                            $('#create-skill-modal').modal('hide')
                        }, 500);
                    }
                }
            })
        });

    </script>
@endpush