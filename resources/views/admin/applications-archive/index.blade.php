@extends('layouts.app')

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <style>
        .switch-workflow {
            color: #4e5d9e !important;
        }
        .switch-workflow:hover {
            background-color: #4e5d9e !important;
            color: #fff !important;
        }
        .ml-20 {
            margin-left : 20%;
        }
    </style>
@endpush
@section('content')

    <div class="row">
        <div class="col-md-12 mb-2">
            <a class="pull-right pull-none-xs ml-20" onclick="exportJobApplication()" >
                <button class="btn btn-sm btn-primary" type="button">
                    <i class="fa fa-upload"></i>  @lang('menu.export')
                </button>
            </a>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row clearfix">
                        <div class="col-md-12 mb-4 d-flex justify-content-between">
                            <a href="{{ route('admin.job-applications.table') }}" class="btn btn-sm switch-workflow">
                                <i class="fa fa-list"></i>
                                @lang('app.switchToApplicants')
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('modules.jobApplication.applicantName')</th>
                                <th>@lang('menu.jobs')</th>
                                <th>@lang('menu.locations')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-modal-md in" id="NotesModal" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
       <div class="modal-dialog modal-md" id="modal-data-application">
           <div class="modal-content">
               <div class="modal-header">
                    <h4 class="card-title form-sub-heading mb-4">
                    Notes
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body" style="overflow-y: auto;max-height: 45vh;">
                    <div class="row">
                        <div class="col-12">
                            <ul class="list-group list-group-flush ml-4" id="view-notes-modal"></ul>
                        </div>
                    </div>
               </div>
           </div>
           <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

    <script>
        var table;
        loadTable();

        function redrawTable() {
            table._fnDraw();
        }

        function loadTable() {
            table = $('#myTable').dataTable({
                responsive: true,
                // processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: {
                    url: "{!! route('admin.applications-archive.data') !!}",
                    data: function ( d ) {
                        return $.extend( {}, d, {
                            skill: $('#skill').val()
                        } );
                    }
                },
                language: languageOptions(),
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_Row_Index'},
                    { data: 'full_name', name: 'full_name' },
                    { data: 'title', name: 'title', width: '17%'},
                    { data: 'location', name: 'location'},
                ]
            });
            new $.fn.dataTable.FixedHeader( table );
        }

        function exportJobApplication(){
            var skillVal = $('#skill').val();

            if (skillVal == '') {
                skillVal = undefined
            }

            var url = '{{ route('admin.applications-archive.export', ':skill') }}';
            url = url.replace(':skill', skillVal);

            window.location.href = url;
        }

        search($('#skill'), 500, 'table');

        table.on('click', '.show-detail', function () {
            $(".right-sidebar").slideDown(50).addClass("shw-rside");

            var id = $(this).data('row-id');
            var url = ("{{auth()->user()->role->role->name=='admin'}}") ? "{{ route('admin.applications-archive.admin-show',':id') }}" : "{{ route('admin.applications-archive.show',':id') }}";
            url = url.replace(':id', id);

            $.easyAjax({
                type: 'GET',
                url: url,
                success: function (response) {
                    if (response.status == "success") {
                        $('#right-sidebar-content').html(response.view);
                    }
                }
            });
        });
        $('#NotesModal').on('show.bs.modal', function(e) {
            $html = '';
            $("#i-"+$(e.relatedTarget).attr('applicant_id')).closest('div').parent().parent().find('.notes-li').each(function () {
                $html += $(this).prop('outerHTML');
            });
            ($(".notes-timeline").length) ?
            $("#view-notes-modal").html($html) :
            $("#view-notes-modal").html('<span>Notes not found.</span>');
        });
    </script>
@endpush