<link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
<link rel="stylesheet" href="{{ asset('assets/node_modules/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
<style>.h-33{ height:33px; }</style>
<div class="modal-header">
    <h4 class="modal-title"><i class="fa fa-pencil"></i> @lang('modules.holiday.editHoliday')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body pt-2 pb-0">
    <form class="ajax-form" method="POST" id="createForm">
    @csrf
    <input type="hidden" name="id" value="{{$holiday->id}}"/>
    <div class="form-body">
            <div class="row">
                <div class="col-md-12  col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">@lang('modules.holiday.title')</label>
                        <input class="form-control form-control-md required" value="{{$holiday->title}}" type="text" id="title" name="title" value="" placeholder="@lang('modules.holiday.title')">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="row">
                        <label class="control-label col-md-6">@lang('modules.holiday.from')</label>
                        <label class="control-label col-md-5 ml-3">@lang('modules.holiday.to')</label>
                    </div>
                    <div class="input-group form-group">
                        <input type="text" class="date form-control form-control-md required h-33" value="{{\Carbon\Carbon::parse($holiday->from)->format('m-d-Y')}}" name="from" placeholder="@lang('modules.holiday.from')">
                        <div class="input-group-prepend h-33">
                            <span class="input-group-text">To</span>
                        </div>
                        <input type="text" class="date form-control form-control-md h-33" name="to" value="{{\Carbon\Carbon::parse($holiday->to)->format('m-d-Y')}}" placeholder="@lang('modules.holiday.to')">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group">
                        <label class="control-label" for="description">@lang('modules.holiday.desc')</label></br>
                        <textarea class="form-control form-control-md" id="description" name="description" placeholder="@lang('modules.holiday.desc')" rows="3">{{$holiday->description}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group mb-0">
                        <label class="" style="display: inline-flex;">
                            <input class="flat-red" style="margin:4px; position:initial; left:0;" type="checkbox" value="1" name="clone_google_event" {{$holiday->clone_google_event ? 'checked' : ''}}/>
                            @lang('modules.customEvent.cloneGoogle')
                        </label><br>
                    </div>
                </div>
            </div>
            <div class="row @if($holiday->clone_google_event==0) d-none @endif" id="delete-google">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group mb-0">
                        <label class="" style="display: inline-flex;">
                            <input class="flat-red" style="margin:4px; position:initial; left:0;" type="checkbox" value="1" name="delete_google_event" {{$holiday->delete_google_event ? 'checked' : ''}}/>
                            @lang('modules.customEvent.deleteGoogle')
                        </label><br>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('app.close')</button>
    <button type="button" class="btn btn-success" id="save-form">@lang('app.submit')</button>
</div>

<script src="{{ asset('assets/node_modules/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('/froiden-helper/helper.js') }}" type="text/javascript"></script>

<script>
    // Datepicker set
    $('.date').bootstrapMaterialDatePicker ({
        time: false,
        format: 'MM-DD-YYYY',
        minDate : new Date(),
    });
    $("body").on('change','input[name="clone_google_event"]',function () {
        $(this).is(':checked') ? $('#delete-google').removeClass('d-none') : $('#delete-google').addClass('d-none');
    });
    $("body").on('change','input[name="from"]',function () {
        $('input[name="to"]').remove();
        $(this).closest('div').append('<input type="text" class="form-control form-control-md h-33" name="to" placeholder="To">');
        $('input[name="to"]').bootstrapMaterialDatePicker({
            time: false,
            format: 'MM-DD-YYYY',
            minDate : new Date($(this).val())
        });
        $('input[name="to"]').val($(this).val());
    });

    $('document').ready(function(){
        $('body').on('click','#save-form',function () {
            var $i=0;
            $('#createForm').find('.required').each(function () {
                if($.trim($(this).val())==''){
                    $(this).css('border','1px solid #ea4335');
                    $i++;
                }
                else{
                    $(this).css('border','1px solid #ced4da');
                }
            });
            if($i>0){ return null; }
            $(this).prop("disabled", true);
            $(this).css({
                "opacity": ".2",
                "cursor": "progress"
            });
            var data = $('#createForm').serializeArray();
            var $action = '{{route("admin.holiday.store")}}';
            $.easyAjax({
                url: $action,
                container: '#createForm',
                type: "POST",
                data: data,
                redirect: true,
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });
    });
</script>