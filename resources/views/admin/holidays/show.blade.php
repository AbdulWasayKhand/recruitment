<div class="modal-header">
    <h4 class="modal-title"></i> @lang('modules.holiday.holidays')</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
</div>
<div class="modal-body pt-2 pb-0">
    <div class="form-body">
            <div class="row">
                <div class="col-md-12  col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">@lang('modules.holiday.title')</label><br>
                        {{ucFirst($holiday->title)}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-6 ">
                    <div class="form-group">
                        <label class="control-label" for="date">@lang('modules.customEvent.date')</label><br>
                        {{Carbon\Carbon::parse($holiday->from)->format('M d, Y') }} - {{Carbon\Carbon::parse($holiday->to)->format('M d, Y') }}
                    </div>
                </div>
            </div>
            @if($holiday->description)
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="form-group">
                        <label class="control-label" for="description">@lang('modules.customEvent.desc')</label><br>
                        {{$holiday->description}}
                    </div>
                </div>
            </div>
            @endif
        </div>
</div>