@extends('layouts.app')

@push('head-script')
<link rel="stylesheet" href="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/all.css') }}">
<style type="text/css">
    .font-10px{
        font-size: 10px;
    }
    #l_color  {
        display: inline-flex;
    }
    .color_code {
        padding: 20px;
        margin:5px;
        cursor: pointer;
        border-radius: 50%;
    }
    #all_labels > div{
        min-width: 100px;
        position: relative;
        padding: 10px;
        border-radius: 5px;
        color:ivory;
        display: inline-grid;
        margin: 5px;
    }
    #all_labels > div {

    }
    #all_labels > div > div {
        outline: none;
    }
    #all_labels > div > span {
        position: absolute;
        right: 5px;
        cursor: pointer;
        display: none;
    }
    #all_labels > div:hover > span {
        display: block;
    }
    #clonable {
        display: none !important;
    }
    .lbl {
        white-space: nowrap;
        max-width: 300px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="editSettings" class="ajax-form">
                        @csrf
                        @method('PUT')
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary" style="font-size:25px; font-weight:bold;">@lang('modules.applicationSetting.mailSettings')</h4>
                            <!-- <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.termsConditions')</h4> -->
                        </div>

                        <hr>

                        <!-- <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary" style="font-size:25px; font-weight:bold;">@lang('modules.applicationSetting.mailSettings')</h4>
                        </div> -->

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('modules.applicationSetting.fromEmailAddress')</label>
                                <input type="text" class="form-control" id="company_sender_email" name="company_settings[company_sender_email]" placeholder="Default Email to display" value="{{ Arr::get($company_settings, 'company_sender_email') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('modules.applicationSetting.fromSenderName')</label>
                                <input type="text" class="form-control" id="company_email_sender_name" name="company_settings[company_email_sender_name]" placeholder="Default Name to dispaly" value="{{ Arr::get($company_settings, 'company_email_sender_name') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label >@lang('modules.applicationSetting.hiringMangerEmail')</label>
                                <br />
                                <small>@lang('modules.applicationSetting.hiringMangerNote')</small>
                                <input type="text" class="form-control" id="company_multi_email" name="company_settings[company_multi_email]" placeholder="Multiple Emails for Admins" value="{{ Arr::get($company_settings, 'company_multi_email') }}">
                            </div>
                        </div>
                        <div id="mail-setting" class="row ml-0">
                            <label style="margin-left: 10px">Send mail if candidate move to </label>
                            @forelse($statuses as $mailSetting)
                                @if($mailSetting->status != 'applied')
                                <div class="form-group" style="margin-left: 20px">
                                    <label class="">
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($setting->mail_setting, "{$mailSetting->id}.status")) checked @endif
                                                value="{{$mailSetting->id}}"
                                                name="mail_setting[{{$mailSetting->status}}]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                        {{ ucwords($mailSetting->status) }}
                                    </label>
                                </div>
                                @endif
                            @empty
                            @endforelse
                            
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.newScheduleRequest')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('modules.applicationSetting.sendEmail') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'schedule_request_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[schedule_request_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="schedule_request_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [interview_session_title] [company_name]
                                </label>
                                <input type="text" class="form-control" id="schedule_request_subject" name="company_settings[schedule_request_subject]" placeholder="Default New Schedule Request Subject" value="{{ Arr::get($company_settings, 'schedule_request_subject') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('app.text')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [employee_name]
                                [applicant_name]
                                [company_name]
                                [schedule_date]
                                [schedule_time]
                                [job_title]
                                [accept_url]
                                [interview_session_title]
                                </label>
                                <textarea class="form-control wysihtml5" id="company_settings[schedule_request_text]" name="company_settings[schedule_request_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'schedule_request_text') !!}</textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">Job Application Received</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                            Send automated email if candidate applies for Job &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                @foreach($statuses as $mailSetting)
                                @if($mailSetting->status == 'applied')
                                    <input
                                    type="checkbox"
                                    @if (Arr::get($setting->mail_setting, "{$mailSetting->id}.status")) checked @endif
                                    value="{{$mailSetting->id}}"
                                    name="mail_setting[{{$mailSetting->status}}]"
                                    class="flat-red columnCheck"
                                    style="position: absolute; opacity: 0;"
                                    >
                                @endif
                                @endforeach
                                </div>
                            </label>
                        </div>
                        

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="applicant_recieved_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [company_name]
                                </label>
                                <input type="text" class="form-control" id="applicant_recieved_subject" name="company_settings[applicant_recieved_subject]" placeholder="Default Applicant Recieved Subject" value="{{ Arr::get($company_settings, 'applicant_recieved_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="interview_schedule_text">@lang('app.text')</label><br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [employee_name]
                                </label>
                                <textarea class="form-control wysihtml5" id="applicant_recieved_text" name="company_settings[applicant_recieved_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'applicant_recieved_text') !!}</textarea>
                            </div>

                        </div>





                        
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantInvite')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('modules.applicationSetting.mailAutoTextQualify') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'qualify_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[qualify_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="qualified_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [interview_session_title] [company_name]
                                </label>
                                <input type="text" class="form-control" id="qualified_subject" name="company_settings[qualified_subject]" placeholder="Default Qualified Subject" value="{{ Arr::get($company_settings, 'qualified_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="address">@lang('app.text')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [employee_name]
                                [interview_url]
                                [interview_session_title]
                                </label>
                                <textarea class="form-control wysihtml5" id="company_settings[qualified_text]" name="company_settings[qualified_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'qualified_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewScheduled')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'schedule_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[schedule_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="qualified_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [interview_session_title] [company_name]
                                </label>
                                <input type="text" class="form-control" id="qualified_subject" name="company_settings[interview_schedule_subject]" placeholder="Default Schedule Interview Subject" value="{{ Arr::get($company_settings, 'interview_schedule_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="interview_schedule_text">@lang('app.text')</label><br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [job_location]
                                [job_location_address]
                                [interview_url]
                                [schedule_date]
                                [schedule_time]
                                [employee_name]
                                [interview_session_title]
                                </label>
                                <textarea class="form-control wysihtml5" id="interview_schedule_text" name="company_settings[interview_schedule_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'interview_schedule_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewRescheduled')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'reschedule_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[reschedule_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="interview_reschedule_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [interview_session_title] [company_name]
                                </label>
                                <input type="text" class="form-control" id="interview_reschedule_subject" name="company_settings[interview_reschedule_subject]" placeholder="Default Interview Reschedule Subject" value="{{ Arr::get($company_settings, 'interview_reschedule_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="interview_reschedule_text">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [job_location]
                                [job_location_address]
                                [interview_url]
                                [schedule_date]
                                [schedule_time]
                                [employee_name]
                                [interview_session_title]

                                </label>
                                <textarea class="form-control wysihtml5" id="interview_reschedule_text" name="company_settings[interview_reschedule_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'interview_reschedule_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewCanceled')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'cancel_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[cancel_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="canceled_interview_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [interview_session_title] [company_name]
                                </label>
                                <input type="text" class="form-control" id="canceled_interview_subject" name="company_settings[canceled_interview_subject]" placeholder="Default Canceled Interview Subject" value="{{ Arr::get($company_settings, 'canceled_interview_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="canceled_interview_text">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [employee_name]
                                [interview_session_title]
                                </label>
                                <textarea class="form-control wysihtml5" id="canceled_interview_text" name="company_settings[canceled_interview_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'canceled_interview_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantRejected')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'rejected_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[rejected_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="rejected_applicant_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [company_name]
                                </label>
                                <input type="text" class="form-control" id="rejected_applicant_subject" name="company_settings[rejected_applicant_subject]" placeholder="Default Rejected Applicant Subject" value="{{ Arr::get($company_settings, 'rejected_applicant_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="rejected_applicant_text">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [employee_name]
                                </label>
                                <textarea class="form-control wysihtml5" id="rejected_applicant_text" name="company_settings[rejected_applicant_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'rejected_applicant_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantHired')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'hired_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[hired_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="hired_applicant_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [company_name]
                                </label>
                                <input type="text" class="form-control" id="hired_applicant_subject" name="company_settings[hired_applicant_subject]" placeholder="Default Rejected Applicant Subject" value="{{ Arr::get($company_settings, 'hired_applicant_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="hired_applicant_text">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [employee_name]
                                </label>
                                <textarea class="form-control wysihtml5" id="hired_applicant_text" name="company_settings[hired_applicant_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'hired_applicant_text') !!}</textarea>
                            </div>

                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantnoShow')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'no_show_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[no_show_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="no_show_applicant_subject">@lang('app.subject')</label>
                                <br>
                                <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [company_name]
                                </label>
                                <input type="text" class="form-control" id="no_show_applicant_subject" name="company_settings[no_show_applicant_subject]" placeholder="Default No Show Applicant Subject" value="{{ Arr::get($company_settings, 'no_show_applicant_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="hired_applicant_text">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [job_title]
                                [employee_name]
                                </label>
                                <textarea class="form-control wysihtml5" id="no_show_applicant_text" name="company_settings[no_show_applicant_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'no_show_applicant_text') !!}</textarea>
                            </div>

                        </div>
                        {{--<hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('app.text') @lang('app.for') @lang('app.status') @lang('app.change') @lang('app.email')</h4>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                @lang('app.send') @lang('app.email') &nbsp;
                                <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                    <input
                                        type="checkbox"
                                        @if (Arr::get($company_settings, 'status_change_auto_email_enabled')) checked @endif
                                        value="enable"
                                        name="company_settings[status_change_auto_email_enabled]"
                                        class="flat-red columnCheck"
                                        style="position: absolute; opacity: 0;"
                                    >
                                </div>
                            </label>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="status_change_subject">@lang('app.status') @lang('app.change') @lang('app.subject')</label>
                                <input type="text" class="form-control" id="status_change_subject" name="company_settings[status_change_subject]" placeholder="Default Status Change Subject" value="{{ Arr::get($company_settings, 'status_change_subject') }}">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="status_change_text">@lang('app.status') @lang('app.change') @lang('app.text')</label>
                                <textarea class="form-control wysihtml5" id="company_settings[status_change_text]" name="company_settings[status_change_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'status_change_text') !!}</textarea>
                            </div>

                        </div>--}}

                        {{--<div class="col-md-12">

                            <div class="form-group">
                                <label for="address">@lang('modules.applicationSetting.unQualifiedText')</label>
                                <textarea class="form-control wysihtml5" id="company_settings[unqualified_text]" name="company_settings[unqualified_text]" rows="15" placeholder="Enter text ...">{!! Arr::get($company_settings, 'unqualified_text') !!}</textarea>
                            </div>

                        </div>--}}
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary" style="font-size:25px; font-weight:bold;">@lang('modules.applicationSetting.textSettings')</h4>
                        </div>

                        <div class="col-md-12">
                            <div id="mail-setting" class="row">
                                <label style="margin-left: 10px">@lang('modules.applicationSetting.smsSendText')</label>
                                @forelse($statuses as $mailSetting)
                                @if($mailSetting->status != 'applied')
                                    <div class="form-group" style="margin-left: 20px">
                                        <label class="">
                                            <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                                <input
                                                    type="checkbox"
                                                    @if (Arr::get($setting->sms_setting, "{$mailSetting->id}.status")) checked @endif
                                                    value="{{$mailSetting->id}}"
                                                    name="sms_setting[{{$mailSetting->status}}]"
                                                    class="flat-red columnCheck"
                                                    style="position: absolute; opacity: 0;"
                                                >
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                            </div>
                                            {{ ucwords($mailSetting->status) }}
                                        </label>
                                    </div>
                                @endif
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.newScheduleRequest')</h4>
                                <div class="form-group">
                                    <label>
                                    @lang('modules.applicationSetting.sendText') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'schedule_request_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[schedule_request_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [employee_name]
                                    [applicant_name]
                                    [company_name]
                                    [schedule_date]
                                    [schedule_time]
                                    [job_title]
                                    [accept_url]
                                    [interview_session_title]
                                    </label>
                                </div>
                                <textarea class="form-control" id="sms_schedule_request_text" name="company_settings[sms_schedule_request_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_schedule_request_text') }}</textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">Job Application Received</h4>
                                <div class="form-group">
                                    <label>
                                        Send automated text message if candidate applies for Job &nbsp;
                                        @foreach($statuses as $mailSetting)
                                        @if($mailSetting->status == 'applied')
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                        <input
                                                    type="checkbox"
                                                    @if (Arr::get($setting->sms_setting, "{$mailSetting->id}.status")) checked @endif
                                                    value="{{$mailSetting->id}}"
                                                    name="sms_setting[{{$mailSetting->status}}]"
                                                    class="flat-red columnCheck"
                                                    style="position: absolute; opacity: 0;"
                                                >
                                        </div>
                                        @endif
                                        @endforeach
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [employee_name]
                                    </label>
                                </div>
                                <textarea class="form-control" id="applicant_recieved_sms" name="company_settings[applicant_recieved_sms]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'applicant_recieved_sms') }}</textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.qualificationTextsSMS')</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>
                                        @lang('modules.applicationSetting.smsAutoTextQualify') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'qualify_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[qualify_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('app.text')</label>
                                <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                [applicant_name]
                                [company_name]
                                [employee_name]
                                [interview_url]
                                [interview_session_title]
                                </label>
                                <textarea class="form-control" id="qualified_text" name="company_settings[sms_qualified_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_qualified_text') }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewScheduled')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'schedule_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[schedule_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [job_location]
                                    [job_location_address]
                                    [interview_url]
                                    [schedule_date]
                                    [schedule_time]
                                    [employee_name]
                                    [interview_session_title]
                                    </label>
                                </div>
                                <textarea class="form-control" id="schedule_interview_text" name="company_settings[sms_schedule_interview_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_schedule_interview_text') }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewRescheduled')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'reschedule_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[reschedule_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [job_location]
                                    [job_location_address]
                                    [interview_url]
                                    [schedule_date]
                                    [schedule_time]
                                    [employee_name]
                                    [interview_session_title]
                                    </label>
                                </div>
                                <textarea class="form-control" id="reschedule_interview_text" name="company_settings[sms_reschedule_interview_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_reschedule_interview_text') }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.interviewCanceled')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'cancel_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[cancel_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [employee_name]
                                    [interview_session_title]
                                    [status_name]
                                    </label>
                                </div>
                                <textarea class="form-control" id="cancel_interview_text" name="company_settings[sms_cancel_interview_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_cancel_interview_text') }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantRejected')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'rejected_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[rejected_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [employee_name]
                                    </label>
                                </div>
                                <textarea class="form-control" id="rejected_applicant_text" name="company_settings[sms_rejected_applicant_text]" rows="5" placeholder="Enter text ...">{!! Arr::get($company_settings, 'sms_rejected_applicant_text') !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantHired')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'hired_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[hired_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [employee_name]
                                    </label>
                                </div>
                                <textarea class="form-control" id="hired_applicant_text" name="company_settings[sms_hired_applicant_text]" rows="5" placeholder="Enter text ...">{!! Arr::get($company_settings, 'sms_hired_applicant_text') !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.applicantnoShow')</h4>
                                <div class="form-group">
                                    <label>
                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                            <input
                                                type="checkbox"
                                                @if (Arr::get($company_settings, 'no_show_auto_sms_enabled')) checked @endif
                                                value="enable"
                                                name="company_settings[no_show_auto_sms_enabled]"
                                                class="flat-red columnCheck"
                                                style="position: absolute; opacity: 0;"
                                            >
                                        </div>
                                    </label>
                                    <br><label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [job_title]
                                    [employee_name]
                                    </label>
                                </div>
                                <textarea class="form-control" id="sms_no_show_applicant_text" name="company_settings[sms_no_show_applicant_text]" rows="5" placeholder="Enter text ...">{!! Arr::get($company_settings, 'sms_no_show_applicant_text') !!}</textarea>
                            </div>
                        </div>
                        {{--<div class="col-md-12">
                                <div class="form-group">
                                    <label for="sms_status_change_text">@lang('app.status') @lang('app.change') @lang('app.text') @lang('app.message')</label>
                                            <div class="form-group">
                                        <label>
                                                                                        @lang('app.send') @lang('app.text') @lang('app.message') &nbsp;
                                                                                        <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative; margin-right: 5px">
                                                                                            <input
                                                                                                type="checkbox"
                                                                                                @if (Arr::get($company_settings, 'status_change_auto_sms_enabled')) checked @endif
                                                                                                value="enable"
                                                                                                name="company_settings[status_change_auto_sms_enabled]"
                                                                                                class="flat-red columnCheck"
                                                                                                style="position: absolute; opacity: 0;"
                                                                                            >
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <textarea class="form-control" id="sms_status_change_text" name="company_settings[sms_status_change_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_status_change_text') }}</textarea>
                                                                            </div>
                                                                        </div>--}}

                        {{-- <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('modules.applicationSetting.unQualifiedText')</label>
                                <textarea class="form-control" id="unqualified_text" name="company_settings[sms_unqualified_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_unqualified_text') }}</textarea>
                            </div>
                        </div> --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('modules.applicationSetting.smsAutoReminderHourQualify')</label>
                                    <select class="form-control col-md-6 col-sm-12" name="company_settings[sms_auto_reminder_hour]">
                                        @foreach(['disable', '1', '2', '3', ] as $timing)
                                            <option value="{{ $timing }}" {{ Arr::get($company_settings, 'sms_auto_reminder_hour') == $timing ? 'selected' : '' }}>{{ $timing }}</option>                                            
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('modules.applicationSetting.smsAutoReminderDayQualify')</label>
                                <select class="form-control col-md-6 col-sm-12" name="company_settings[sms_auto_reminder_day]">
                                    @foreach(['disable', '1', '2', '3', ] as $timing)
                                        <option value="{{ $timing }}" {{ Arr::get($company_settings, 'sms_auto_reminder_day') == $timing ? 'selected' : '' }}>{{ $timing }}</option>                                            
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top:30px;">
                            <div class="form-group">
                                <h4 class="card-title mb-4 text-primary">@lang('modules.applicationSetting.reminder') @lang('app.text') @lang('app.message')</h4>
                                <div class="form-group">
                                    <label class="font-10px">@lang('modules.applicationSetting.note')<br>
                                    [applicant_name]
                                    [company_name]
                                    [schedule_date]
                                    [schedule_time]
                                    [employee_name]
                                    [interview_url]
                                    [job_title]
                                    [interview_session_title]
                                    </label>
                                </div>
                                <textarea class="form-control" id="sms_reminder_text" name="company_settings[sms_reminder_text]" rows="5" placeholder="Enter text ...">{{ Arr::get($company_settings, 'sms_reminder_text') }}</textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('modules.applicationSetting.SMStimeDelayText')</label>
                                <input type="number" class="form-control" id="sms_time_delay" name="company_settings[sms_time_delay]" placeholder="Enter Time Delay (Minutes Only)" value="{{ Arr::get($company_settings, 'sms_time_delay') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">@lang('modules.interviewSchedule.calendar') @lang('modules.applicationSetting.thanksPageUrl')</label>
                                <input type="text" class="form-control" id="thanks_page_url" name="company_settings[thanks_page_url]" value="{{ Arr::get($company_settings, 'thanks_page_url') ?: route('jobs.jobOpenings', $company->career_page_link) }}">
                            </div>
                        </div>
                        <hr />
                            <h4 class="card-title mb-4 text-primary" style="font-size:25px; font-weight:bold;">Labels</h4>
                            <small>Click to Add a new Label</small>
                            <div id="labels_container">
                                <div id="l_color">
                                    <div class="color_code" data-color='#4caf50'></div>
                                    <div class="color_code" data-color='#ffc107'></div>
                                    <div class="color_code" data-color='#ffeb3b'></div>
                                    <div class="color_code" data-color='#f44336'></div>
                                    <div class="color_code" data-color='#00bcd4'></div>
                                </div>
                                <hr />
                                <div id="clonable"><div contenteditable="true" ></div><span onclick="removeMe(this.parentNode, 0)">x</span></div>
                            <div id="all_labels">@foreach($labels as $label) <div style="background:{{$label->label_color}};" title="{{$label->label_value}}"><div contenteditable="true" class="editable_label lbl" data-lid='{{$label->id}}' data-cid='{{$label->company_id}}' data-color='{{$label->label_color}}'>{{$label->label_value}}</div><span onclick="removeMe(this.parentNode, {{$label->id}})" onmouseover="this.parentNode.firstChild.blur()">x</span></div>@endforeach </div>
                            </div>
                        
                            <hr />
                        <button type="button" id="save-form"
                                class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        <button type="reset"
                                class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
<script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/node_modules/html5-editor/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/node_modules/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

<script>
    var oldValue = '';
    document.querySelectorAll(".color_code").forEach(function(e){
        e.style.background = e.getAttribute('data-color');
        e.addEventListener('click', function(){
            var color = e.getAttribute('data-color');
            var clone = clonable.cloneNode(true);
            clone.removeAttribute('id');
            clone.style.background = color;
            all_labels.appendChild(clone);
        });
    });
    document.querySelectorAll('.editable_label').forEach(function (e){
        e.addEventListener('mouseenter', function(){
            oldValue = this.innerHTML;
        });
        e.addEventListener('blur', function(){
            if(oldValue != this.innerHTML){
                $.easyAjax({
                url: "{{ url('/labels/update') }}",
                    data: {
                        lid: e.getAttribute('data-lid'),
                        cid: e.getAttribute('data-cid'),
                        color: e.getAttribute('data-color'),
                        value: e.innerHTML,
                        '_token': '{{ csrf_token() }}' 
                    },
                    type: "POST",
                    redirect: false,
                    file: false
                })
            }            
        })
        oldValue = '';
    });
    function removeMe(elm, id){
        if(id != 0){
            $.easyAjax({
            url: "{{ url('/labels/delete') }}",
                data: {
                    lid: id,
                    '_token': '{{ csrf_token() }}' 
                },
                type: "POST",
                redirect: false,
                file: false
            })
        }
        elm.remove();
    }
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
    })

    var jobDescription = $('#legal_term').wysihtml5({
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": true, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": true, //Button to change color of font
        stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)
    });

    $('.wysihtml5').each(function(index, node) {
        $(node).wysihtml5({
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": true, //Button to change color of font
            stylesheets: ["{{ asset('assets/node_modules/html5-editor/wysiwyg-color.css') }}"], // (path_to_project/lib/css/wysiwyg-color.css)
        });
    })

    $('#save-form').click(function () {
        label = [];
        Array.from(all_labels.childNodes).forEach(function (e){
            if(e.tagName){
                if(e.firstChild.innerHTML != ''){
                    label.push({'value': e.firstChild.innerHTML,'color':e.style.background});
                }
            }
        });
        $.easyAjax({
            url: '{{route('admin.application-setting.update', $global->id)}}',
            container: '#editSettings',
            data: {
                labels: JSON.stringify( label ),
                '_token': '{{ csrf_token() }}' 
            },
            type: "POST",
            redirect: true,
            file: true
        })
    });
</script>

  
@endpush