<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <!--<a href="{{ route('admin.dashboard') }}" class="brand-link">
        <img src="{{ $global->logo_url }}"
             alt="AdminLTE Logo"
             class="brand-image img-fluid">
    </a>-->
     <a class="image-container nav-link waves-effect waves-light" style="width: 230px;"
        @if(!$user->is_superadmin)
            href="{{ route('admin.profile.index') }}"
        @else
            href="{{ route('superadmin.profile.index') }}"
        @endif
        >
            <div class="image" style="margin-right:15px">
                <img src="{{ $user->profile_image_url  }}" alt="User Image">
            </div>
            <span style="color:#fff">{{ ucwords($user->name) }}</span>
     </a>
    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" id="sidebarnav" role="menu"
                data-accordion="false">

            @if(!is_null($activePackage))
                <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{ route('admin.dashboard') }}" class="nav-link {{ request()->is('admin/dashboard*') ? 'active' : '' }}">
                            <i class="nav-icon icon-speedometer"></i>
                            <p>
                                @lang('menu.dashboard')
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview @if(\Request()->is('admin/job-applications*') ) active menu-open @endif">
                            <li class="nav-item has-treeview @if(\Request()->is('admin/job-applications*')) active menu-open @endif">
                                <a href="{{ route('admin.job-applications.table') }}" class="nav-link">
                                    <i class="nav-icon icon-user"></i>
                                    <p>
                                        @lang('menu.jobApplications')
                                    </p>
                                </a>
                            </li>
                            @if(in_array("view_schedule", $userPermissions))
                            <li class="nav-item">
                                <a href="{{ route('admin.interview-schedule.index') }}" class="nav-link {{ request()->is('admin/interview-schedule*') ? 'active' : '' }}">
                                    <i class="nav-icon icon-calendar"></i>
                                    <p>
                                        @lang('menu.interviewCalendar')
                                    </p>
                                </a>
                            </li>
                            @endif
                            {{--<li class="nav-item">
                                <a href="{{ route('admin.custom-event.index') }}" class="nav-link {{ request()->is('admin/interview-schedule*') ? 'active' : '' }}">
                                    <i class="nav-icon icon-calendar"></i>
                                    <p>
                                        @lang('menu.customEvents')
                                    </p>
                                </a>
                            </li>--}}
                            @if($user->id == \App\User::where('company_id','=',$user->company_id)->first()->id)
                            <li class="nav-item">
                                <a href="{{ url('admin/holiday') }}" class="nav-link {{ request()->is('admin/holiday*') ? 'active' : '' }}">
                                    <i class="nav-icon icon-calendar"></i>
                                    <p>@lang('menu.holidays')</p>
                                </a>
                            </li>
                            @endif
                            <li class="nav-item" style="border-bottom: 0.1px solid gray;">
                                <a href="{{ route('admin.messages') }}" class="nav-link {{ request()->is('admin/text-messages*') ? 'active' : '' }}">
                                    <i class="nav-icon icon-envelope"></i>
                                    <p>
                                        @lang('menu.smsMessages')
                                    </p>
                                </a>
                            </li>
                            @if(in_array("view_jobs", $userPermissions))
                            <li class="nav-item has-treeview 
                                    @if(\Request()->is('admin/jobs*') ||
                                        request()->is('admin/locations*') ||
                                        request()->is('admin/questions*') || request()->is('admin/questionnaire*') ||
                                        request()->is('admin/job-templates*') ||
                                        request()->is('admin/job-categories*') || 
                                        request()->is('admin/skills*')
                                        )
                                        active menu-open 
                                    @endif">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon-badge"></i>
                                    <p>
                                        @lang('menu.jobs')
                                        <i class="right fa fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('admin.jobs.create') }}"
                                        class="looking-btn">
                                            <i class="nav-icon fa fa-plus" style="width: auto;"></i>
                                            <p>@lang('menu.createnewjob')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('admin.jobs.index') }}" class="nav-link {{ request()->is('admin/jobs*') ? 'active' : '' }}">
                                            <i class="nav-icon icon-badge"></i>
                                            <p>
                                                @lang('menu.allJobs')
                                            </p>
                                        </a>
                                    </li>
                                    @if(in_array("view_category", $userPermissions))
                                        <li class="nav-item">
                                            <a href="{{ route('admin.job-categories.index') }}" class="nav-link {{ request()->is('admin/job-categories*') ? 'active' : '' }}">
                                                <i class="icon-badge nav-icon"></i>
                                                <p>
                                                    @lang('menu.jobCategories')
                                                </p>
                                            </a>
                                        </li>
                                    @endif
                                    
                                    @if(in_array("view_skills", $userPermissions))
                                        <li class="nav-item">
                                            <a href="{{ route('admin.skills.index') }}" class="nav-link {{ request()->is('admin/skills*') ? 'active' : '' }}">
                                                <i class="icon-badge nav-icon"></i>
                                                <p>
                                                    @lang('menu.skills')
                                                </p>
                                            </a>
                                        </li>
                                    @endif
                                    @if(in_array("view_locations", $userPermissions))
                                        <li class="nav-item">
                                            <a href="{{ route('admin.locations.index') }}" class="nav-link {{ request()->is('admin/locations*') ? 'active' : '' }}">
                                                <i class="nav-icon icon-location-pin"></i>
                                                <p>
                                                    @lang('menu.locations')
                                                </p>
                                            </a>
                                        </li>
                                    @endif
                                    <li class="nav-item">
                                        <a href="{{ route('jobs.jobOpenings',$global->career_page_link) }}" target="_blank"
                                        class="nav-link">
                                            <i class="nav-icon fa fa-external-link" style="width: auto;"></i>
                                            <p>@lang('menu.landingPage')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.questions.index')}}"
                                        class="nav-link {{ request()->is('admin/questions*') ? 'active' : '' }}">
                                            <i class="nav-icon fa fa-question" style="width: auto;margin-right: 9px;"></i>
                                            <p>@lang('menu.customQuestions')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.questionnaire.index')}}"
                                        class="nav-link {{ request()->is('admin/questionnaire*') ? 'active' : '' }}">
                                            <i class="nav-icon fa fa-check-square-o" style="width: auto;margin-right: 9px;"></i>
                                            <p>@lang('menu.questionnaires')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('admin.job-templates.index') }}"
                                        class="nav-link {{ request()->is('admin/job-templates*') ? 'active' : '' }}">
                                            <i class="nav-icon icon-badge" style="width: auto;"></i>
                                            <p>@lang('menu.templates')</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                    </li>

                    <li class="nav-item has-treeview 
                        @if(\Request()->is('admin/job-onboard*') || 
                            \Request()->is('admin/team*') || 
                            request()->is('admin/settings/role-permission')
                            )
                            active menu-open 
                            @endif">
                        <a href="#" class="nav-link">
                            <i class="nav-icon icon-drawer"></i>
                            <p>
                                @lang('menu.employees')
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.job-onboard.index') }}" class="nav-link {{ request()->is('admin/job-onboard*') ? 'active' : '' }}">
                                    <i class="nav-icon icon-user"></i>
                                    <p>
                                        @lang('app.onBoard')
                                    </p>
                                </a>
                            </li>
                            @if(in_array("manage_settings", $userPermissions))
                                <li class="nav-item">
                                    <a href="{{ route('admin.role-permission.index') }}" class="nav-link {{ request()->is('admin/settings/role-permission') ? 'active' : '' }}">
                                        <i class="nav-icon icon-people"></i>
                                        <p>@lang('menu.rolesPermission')</p>
                                    </a>
                                </li>   
                            @endif

                            @if(in_array("view_team", $userPermissions))
                                <li class="nav-item">
                                    <a href="{{ route('admin.team.index') }}" class="nav-link {{ request()->is('admin/team*') ? 'active' : '' }}">
                                        <i class="nav-icon icon-people"></i>
                                        <p>
                                            @lang('menu.team')
                                        </p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>

                    <li class="nav-item has-treeview
                        @if(\Request()->is('admin/todo-items*') ||
                            \Request()->is('admin/settings/application-setting*') ||
                            \Request()->is('admin/subscribe*') ||
                            \Request()->is('admin/profile*') ||
                            \Request()->is('admin/settings/settings') ||
                            \Request()->is('admin/settings/theme-settings') ||
                            \Request()->is('admin/facebook-applicants*')
                            )
                            active menu-open 
                            @endif">
                        <a href="#" class="nav-link">
                            <i class="nav-icon icon-settings"></i>
                            <p>
                                @lang('menu.preferences')
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">    
                            <li class="nav-item">
                                <a href="{{ route('admin.application-setting.index') }}" class="nav-link {{ request()->is('admin/settings/application-setting') ? 'active' : '' }}">
                                    <i class="nav-icon fa fa-envelope-o"></i>
                                    <p>@lang('menu.applicationFormSettings')</p>
                                </a>
                            </li>
                            @if(!is_null(App\Company::find(Auth::user()->company_id)->facebook_id))
                                <li class="nav-item">
                                    <a href="{{ route('admin.facebook-applicants.index') }}" class="nav-link {{ request()->is('admin/facebook-applicants*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>
                                            @lang('menu.integrations')
                                        </p>
                                    </a>
                                </li>
                            @endif

                            <li class="nav-item">
                                <a href="{{ route('admin.profile.index') }}" class="nav-link {{ request()->is('admin/profile*') ? 'active' : '' }}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p> @lang('menu.account')</p>
                                </a>
                            </li>
                            @if(in_array("manage_settings", $userPermissions))
                                <li class="nav-item">
                                    <a href="{{ route('admin.settings.index') }}" class="nav-link {{ request()->is('admin/settings/settings') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>@lang('menu.businessSettings')</p>
                                    </a>
                                </li>
                                @if($linkedinGlobal->status == 'enable')
                                    <li class="nav-item">
                                        <a href="{{ route('admin.linkedin-settings.index') }}" class="nav-link {{ request()->is('admin/settings/linkedin-settings') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>@lang('menu.linkedInSettings')</p>
                                        </a>
                                    </li>
                                @endif
                            @endif

                            @if($user->roles->count() > 0)
                                <li class="nav-item">
                                    <a href="{{ route('admin.todo-items.index') }}" class="nav-link {{ request()->is('admin/todo-items*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>
                                            @lang('menu.todoList')
                                        </p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>

                    @if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == 'stage.engyj.com' || $_SERVER['SERVER_NAME'] == 'local.recruit.com' || in_array(company()->id, [15, 16, 43, 44, 48, 22, 47, 50, 21, 51, 12, 53, 46]))
                    <li class="nav-item has-treeview
                        @if(\Request()->is('admin/workflow/workflow*') || 
                            \Request()->is('admin/workflow/index*') ||
                            \Request()->is('admin/workflow/trigger*') ||
                            \Request()->is('admin/template*') 
                            )
                            active menu-open 
                            @endif">
                        <a href="#" class="nav-link">
                            <i class="nav-icon icon-settings"></i>
                            <p>
                                Workflows
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">    
                                <li class="nav-item">
                                    <form action="{{ route('admin.workflow.update-workflow') }}" method="POST" id="create-workflow">{{csrf_field()}}</form>
                                    <a href="javascript:void(0)" onclick="document.getElementById('create-workflow').submit();"
                                    class="looking-btn">
                                        <i class="nav-icon fa fa-plus" style="width: auto;"></i>
                                        <p style="font-size: 12px;">Create a new Workflow</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ url('admin/workflow/index') }}" class="nav-link {{ request()->is('admin/workflow/index*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p> All Workflows </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.template.index') }}" class="nav-link {{ request()->is('admin/template*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p> Templates </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('admin/workflow/trigger') }}" class="nav-link {{ request()->is('admin/workflow/trigger*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p> Triggers </p>
                                    </a>
                                </li>
                                @if($linkedinGlobal->status == 'enable')
                                    <li class="nav-item">
                                        <a href="{{ route('admin.linkedin-settings.index') }}" class="nav-link {{ request()->is('admin/settings/linkedin-settings') ? 'active' : '' }}">
                                            <i class="fa fa-circle-o nav-icon"></i>
                                            <p>@lang('menu.linkedInSettings')</p>
                                        </a>
                                    </li>
                                @endif
                        </ul>
                    </li>
                    @endif

                    @if(auth()->check())
                    {{-- @if(auth()->check() && auth()->user()->company_id==15) --}}
                    <li class="nav-item">
                        <a href="{{ route('admin.reports') }}" class="nav-link {{ request()->is('admin/reports*') ? 'active' : '' }}">
                        <i class="nav-icon icon-book-open"></i>
                        <p>@lang('menu.reports')</p>
                        </a>
                    </li>
                    {{--<li class="nav-item has-treeview @if(\Request()->is('admin/reports*')) active menu-open @endif">
                        <a href="#" class="nav-link">
                            <i class="nav-icon icon-book-open"></i>
                            <p>
                                <i class="right fa fa-angle-left"></i>
                                @lang('menu.reports')
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('admin/reports') }}" class="nav-link {{ request()->is('admin/reports') ? 'active' : '' }}">
                                    <i class="nav-icon icon-book-open"></i>
                                    <p>@lang('menu.reports')</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('admin/reports/budget') }}" class="nav-link {{ request()->is('admin/reports/budget.*') ? 'active' : '' }}">
                                    <i class="fa fa-money-bill"></i>
                                    <p>&nbsp;@lang('menu.setBudget')</p>
                                </a>
                            </li>
                        </ul>
                    </li>--}}
                    @endif

                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
