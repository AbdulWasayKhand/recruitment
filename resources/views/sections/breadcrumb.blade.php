<!-- Content Header (Page header) -->
<style>
#header-div > #create_new {display: none;}
#create_new {margin-bottom: 5px;}
.main-heading{font-size: 22px;font-weight: 700;margin-left: 24px;}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="header-div">
                <span class="float-sm-right" id="create_new">@yield('create-button')</span>
                <ol class="breadcrumb float-sm-left mr-2 mt-xs-2">
                    <li class="breadcrumb-item"><a
                                href="{{ $user->is_superadmin?route('superadmin.dashboard.index'): route('admin.dashboard')}}"><i
                                    class="icon-home"></i></a></li>
                    <li class="breadcrumb-item active">{{ __($pageTitle) }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<div class="col-md-5">
    <h1 class="mb-xs-2 main-heading">{{ __($pageTitle) }}</h1>
</div>
@push('footer-script')
    <script>
        $(".card-body").prepend(create_new)
    </script>
@endpush