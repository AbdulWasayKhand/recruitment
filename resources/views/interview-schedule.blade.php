<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="{{ asset('engyj.png') }}" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="600" />
        <title>Schedule Interview</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
        
        <link href="{{ asset('front/assets/css/core.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/plugins/calendar/dist/fullcalendar.css') }}">
        <link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
        <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
        <link rel="stylesheet"
            href="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

        <link href="{{ asset('assets/node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/node_modules/sweetalert/sweetalert.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>
            body{font-family: 'Poppins', sans-serif;}
            .mb-20 {
                margin-bottom: 20px
            }

            .datepicker {
                z-index: 9999 !important;
            }

            .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
                width: 100%;
            }

            .select2-search {
                width: 100%;
            }

            .select2.select2-container {
                width: 100% !important;
            }

            .select2-search__field {
                width: 100% !important;
                display: block;
                padding: .375rem .75rem !important;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            }

            .d-block {
                display: block;
            }

            .upcomingdata {
                height: 37.5rem;
                overflow-x: scroll;
            }

            .scheduleul {
                padding: 0 15px 0 11px;
            }
            .center-para{max-width: 343px;width: 100%;margin: 0 auto;font-size: 14px;}
            .empty-space{height:20px}
            .legend-c label {padding: 0 10px;color:#a0a0a0}
            .legend-c label span{display: inline-block;width: 20px;height: 20px;border-radius: 100px;position: relative;top: 4px;left: -5px;}
            .lightgreen{background-color: lightgreen;}
            .lightgray{background-color: #eee;}
            .lightred{ background-color: #f90200;}
            .text-danger{color:#d23e40 !important}
            .btn, button{cursor:pointer}
            .bg-transparent {background: transparent !important;}
            .saperater{border-bottom:1px solid #ccc}
            .modal-title{font-size: 24px;}
            .modal-footer{justify-content: space-between}
            .modal-footer i{position: relative;top: -1px;}
            .modal-body label{font-weight: 500;font-size: 15px;}
            .fc-right{display:none !important; }
            .fc-center h2{font-size: 20px !important; }
            .full-height-calendar .fc-scroller {height: auto !important;}
            .fc-today-button {display:none;}
            #calendar .fc-left { display: none; }

            .ovweflow-x-scroll::-webkit-scrollbar {
            display: none;
            }
            .ovweflow-x-scroll {
                width: 50%;
                height: 165px;
                margin-bottom: 0;
            background-color:#f9f9f9;
            list-style:none;
            padding:0;
             overflow-x: auto;
            -ms-overflow-style: none; 
            scrollbar-width: none; 
            }
            .fix-height{height:120px;}
            .ovweflow-x-scroll .date{font-size: 25px;}
            .align-content-middle{align-content:center}
            .current-date{background: #fff;
            border-radius: 25px;
            padding: 30px 0;
            box-shadow: 0 0 10px rgb(0 0 0 / 21%);}
            .icon{position: absolute;
            left: calc( 50% - 8px );
            bottom: -20px;
            }
            .occupied * {color:red}
            .month {
            position: absolute;
            top: -20px;
            left: 0;
            right: 0;
            display: none;
            }
            .fa-check-circle {
                position: absolute; left: 0;
                right: 0;
                bottom: -20px;
                display:none;
            }
            .content.active {
                background: #4e6dfc;
                color: #fff;
                padding: 30px 0;
                border-radius: 8px;
            }
            .content{cursor:pointer}
            .content.occupied{cursor:not-allowed}
            ul.controller {
                position: relative;
                width: 50%;
                display: block;
                height: 30px;
                padding: 0;
                margin: 0 auto;
            }
            .prev {
                position: absolute;
                border-radius: 52px;
                background-color: #fff;
                padding: 14px 18px;
                box-shadow: 0 0 5px rgb(0 0 0 / 17%);
                left: -27px;
                top: -101px;
                cursor:pointer;
            }
            .next {
                position: absolute;
                border-radius: 52px;
                background-color: #fff;
                padding: 14px 18px;
                box-shadow: 0 0 5px rgb(0 0 0 / 17%);
                right: -27px;
                top: -101px;
                cursor:pointer;
            }
            .swal-button--cancelInterview {
                background-color: #DD6B55 !important;
            }
            li.col-7-1:last-child{border-right: 22px solid transparent;}
            [name="date"]{display:none}
            input[type="radio"]:checked + label{background: #4e6dfc;color: #fff;padding: 30px 0;border-radius: 8px;}
            .active .month, .active .fa-check-circle{display:block}
            input[type="radio"]:checked + label .month, input[type="radio"]:checked + label .fa-check-circle{display:block}
            @media screen and (max-width:767px){
                .ovweflow-x-scroll{width:100%}
                ul.controller {width:100%}
                #locationModal, .sheld > div > img, .sheld2 > div > img {
                    margin-top: 70%!important;
                }
                
            }
            .swal-button--confirm {
                background-color: #dd6b55; 
            }

            .swal-modal .swal2-close {
                position: absolute;
                top: 0;
                right: 0;
                width: 1.2em;
                height: 1.2em;
                transition: color 0.1s ease-out;
                border: none;
                background: transparent;
                color: #cccccc;
                font-family: serif;
                font-size: 40px;
                cursor: pointer;
            }
            .sheld, .sheld2{
                z-index:4444;
                position: fixed;
                width: 100%;
                height: 100%;
                color: white;
                font-size: 30px;
                text-align: center;
                padding-top: 20%;
            }
            .sheld2{
                /* display:none; */
                background-color: rgba(0,0,0,.3);
            }
            #locationModal {
                margin-top: 15%;
            }
            .swal-modal .swal2-close:hover {
            color: #f27474;
            }
        </style>
        <script src="{{ asset('front/assets/js/core.min.js') }}"></script>
    </head>
    <body>

        <div class="sheld2" id="sheld2">
            <div id="fliper">
                <img style="width:50px;" src="{{ url('/assets/')}}/loading.gif" />
                <br />
                <small><i>Please wait....</i></small>
            </div> 
        </div>
        <div class="flex-center position-ref full-height">
                @if(\App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $application->schedule->id)->where('user_accept_status', '=', 'waiting')->get()->count() != 0)
                    <div class="sheld">
                    </div>
                @endif
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mt-5 text-center">
                            <img style="max-width:100px;" src="{{ $application->company->logo_url }}"  alt="engyj" class="logo">
                        </div>
                        {{-- Heading --}}
                        <div class="col-md-12 mt-5 mb-3 text-center">
                            <h2>{{ $application->schedule->status == 'pending' ? "Reschedule" : "Schedule"}} Your @if($application->job->workflow_id) {{ucWords($application->workflow_stage->interview_action->session_title)}} @else Interview @endif</h2>
                            <p class="center-para">@if($application->schedule->status == 'pending') If you need to reschedule the interview for a better date or time, please click available day from following calendar and then select the time slot according to timezone: {{ get_timezone_list($timezone) }} @else Please click an available day from following calendar and then select the time slot according to timezone: {{ get_timezone_list($timezone) }} @endif</p>
                        </div>
                        <div class="empty-space"></div>
                        <div class="col-md-12 mb-3 text-center">
                        
                            @if(\App\InterviewScheduleEmployee::where('interview_schedule_id', '=', $application->schedule->id)->where('user_accept_status', '=', 'waiting')->get()->count() == 0)
                            @if($application->schedule->status == 'pending')
                                <a href="javascript:;" style='margin-bottom:10px;' onclick="cancelInterview()" class="btn btn-md btn-danger btn-outline-danger">Cancel Interview</a>
                            @endif
                            @php
                            $begin = new DateTime( now()->modify('+1 day')->format("Y-m-d") );
                            $end   = new DateTime( now()->modify('+8 day')->format("Y-m-d") );
                            $monthName = $begin->format('F');
                            for($i = $begin; $i <= $end; $i->modify('+1 day')){
                                if($i->format("F") != $monthName){
                                    $monthName = $monthName.'/'.$i->format("F");
                                    break;
                                }
                            }
                            @endphp
                            <h2>{{$monthName}}</h2>
                        </div>
                        <div id="main_view" class="col-md-12 mb-3 text-center" ></div>
                        @else
                            <i style="font-size: 40px; color:goldenrod;" id='waitingForAcceptance'>Waiting for acceptance from admin.</i>
                        @endif
                        @if($application->schedule->status == 'pending')
                            <div class="col-md-12 mb-3 text-center">you have already scheduled your interview on <br><i><b>{{$application->schedule->schedule_date->format('M d, Y l')}}</b></i><br>at {{$application->schedule->slot_timing}}</div>
                        @endif
                    
                    </div>
                </div>
            </div>
        </div>
        <script>
            if(document.getElementById('waitingForAcceptance')){
                document.getElementById('fliper').style.display = 'none';
                document.getElementById('sheld2').style.background =  'transparent';
            }else{
                document.getElementById('fliper').style.display = 'block';
                document.getElementById('sheld2').style.background =  'rgba(0,0,0,.3)';
            }
            $.ajax({
                type:'GET',
                url: window.location + '&loadMainView',
                dataType: "html",
                async: true,
                cache: false,
                success: function(response)
                {
                    $('#main_view').html(response);
                }
            });

        </script>
    </body>
</html>
