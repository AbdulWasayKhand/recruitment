@extends('layouts.app')
@push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<style>
        .hide_id_sorting:before,.hide_id_sorting:after{
            opacity: 0 !important;
        }
        .hide_id_sorting{
            cursor: default !important;
        }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-5">
        <h1 class="mb-xs-2 main-heading">Stats</h1>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered mt-4" id="myTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('app.name')</th>
                            <th>@lang('app.email')</th>
                            <th>@lang('app.message')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer-script')
<script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>



        $(function() {
            var table = $('#myTable').dataTable({
                responsive: true,
                // processing: true,
                serverSide: true,
                ajax: '{!! url("super-admin/contact_form/data") !!}',
                language: languageOptions(),
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_Row_Index',sortable:false},
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'message', name: 'message', width: '30%' },
                    { data: 'action', name: 'action', width: '20%' }
                ]
            });

            new $.fn.dataTable.FixedHeader( table );

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('row-id');
                swal({
                    title: "@lang('errors.areYouSure')",
                    text: "@lang('errors.deleteWarning')",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "@lang('app.delete')",
                    cancelButtonText: "@lang('app.cancel')",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = '{{ url("super-admin/contact_form/delete/:id") }}';
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>

@endpush