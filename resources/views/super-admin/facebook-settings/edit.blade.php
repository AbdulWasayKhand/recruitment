@extends('layouts.app') @push('head-script')
    <link rel="stylesheet" href="{{ asset('assets/node_modules/dropify/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/switchery/dist/switchery.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">@lang('app.edit') @lang('menu.facebookSettings')</h4>
                    <form action="{{ route('superadmin.facebook-settings.update', 1) }}" method="POST">
                        <div class="form-group">
                            <label for="access_token">Current Access Token</label>
                            <input type="text" name="data[access_token]" value="{{ $fbdata['access_token'] ?? '' }}" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label for="client_id">Client ID</label>
                            <input type="text" name="data[client_id]" value="{{ $fbdata['client_token'] ?? '863559434414844' }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="client_secret">Client Secret</label>
                            <input type="text" name="data[client_secret]" value="{{ $fbdata['client_secret'] ?? '3fb80d15a1ff00d53be171d694174cf5' }}" class="form-control">
                        </div>

                        <div class="form-group">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="data[expires_in]" value="{{ $fbdata['expires_in'] ?? '' }}">
                            <input type="hidden" name="data[token_type]" value="{{ $fbdata['token_type'] ?? '' }}">
                            <input type="hidden" name="data[user_id]" value="{{ $fbdata['user_id'] ?? '' }}">
                            <button type="button" class="btn btn-primary" id="facebook_login_button">Get Facebook Token</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="card-title">@lang('app.facebookPage')</h4>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{route('superadmin.updateFacebookPages')}}" id="get_facebook_pages" class="btn btn-primary" {{isset($fbdata['access_token']) ?: 'disabled'}}>Get Facebook Pages</a>
                        </div>
                        <div class="w-100"></div>
                        
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped ">
                                <thead>
                                    <tr>
                                        <th>Page ID</th>
                                        <th>Page Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pages as $page)
                                        <tr>
                                            <td>{{ $page->facebook_id }}</td>
                                            <td>{{ $page->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/dropify/dist/js/dropify.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/node_modules/switchery/dist/switchery.min.js') }}"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '863559434414844',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v8.0'
            });
        };
    </script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>
    <script>

        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function () {
            new Switchery($(this)[0], $(this).data());

        });

    </script>
    <script>
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route("superadmin.facebook-settings.update", 1)}}',
                container: '#editSettings',
                type: "POST",
                redirect: true,
                file: true
            })
        });

        $('#facebook_login_button').on('click', function() {
            $(this).prop('disabled', 1);

            FB.login(function(response) {
                if (response.authResponse) {
                    console.log('auth', response)
                    $('[name="data[user_id]"]').val(response.authResponse.userID);
                    $.get(`https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${$('[name="data[client_id]"]').val()}&client_secret=${$('[name="data[client_secret]"]').val()}&fb_exchange_token=${response.authResponse.accessToken}`, function(response) {
                        console.log('long_auth', response)
                        $('[name="data[access_token]"]').val(response.access_token);
                        $('[name="data[expires_in]"]').val(response.expires_in);
                        $('[name="data[token_type]"]').val(response.token_type);
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'public_profile,pages_show_list,leads_retrieval,pages_manage_ads'});
        });

        $('#get_facebook_pages').on('click', function () {
            
        })
    </script>
@endpush