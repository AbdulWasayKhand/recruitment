@component('mail::layout')
@slot('header')
	@component('mail::header', ['url' => config('app.url'),'company'=>$company])
        WAZiE
    @endcomponent
@endslot

{!! $text !!}

@component('mail::button', ['url' => $url])
    Schedule Your Interview
@endcomponent

@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} {{ config('app.name') }}.
    @endcomponent
@endslot
@endcomponent