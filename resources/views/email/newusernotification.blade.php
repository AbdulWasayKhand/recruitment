@component('mail::layout')
@slot('header')
	@component('mail::header', ['url' => config('app.url')])
        WAZiE
    @endcomponent
@endslot

{!! $text !!}


@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} {{ config('app.name') }}.
    @endcomponent
@endslot
@endcomponent