@extends('layouts.saas-front')

@push('meta_details')
    <title>{{ $headerData->meta_details['title'] ?: 'Recruit' }}</title>

    <meta name="title" content="{{ $headerData->meta_details['title'] ?: '' }}">
    <meta name="description" content="{{ $headerData->meta_details['description'] ?: '' }}">
    <meta name="keywords" content="{{ $headerData->meta_details['keywords'] ?: '' }}">
@endpush

@push('header_css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    @if($firstHeaderData->header_background_color != '')
        <style>
            .background-color {
                border: none;
                background: {{ $firstHeaderData->header_background_color }}
            }

            #header-section {
                background: #44549B;
            }
            .swiper-container-horizontal>.swiper-pagination-bullets, .swiper-pagination-custom, .swiper-pagination-fraction {
                bottom: 0;
                left: 0;
                width: 100%;
            }

        </style>
    @endif

    @if($headerData->header_background_image != '')
        <style>
            #header-section {
                background: url("{{ $headerData->header_backround_image_url }}");
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            #header-section:before {
                content: "";
                position: absolute;
                left: 0; right: 0;
                top: 0; bottom: 0;
                background: rgba(0,0,0,.3);
            }
        </style>
    @endif

    @if($firstHeaderData->custom_css != '')
        <style>
            {!! $firstHeaderData->custom_css !!}
        </style>
    @endif
@endpush

@section('content')
    <div id="space" style="background-color: #44549B; padding-top: 200px;"></div>

    <div id="testimonials" style="background: #fff; padding-top: 100px;">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                <h2 class="section-heading text-burgundy text-center font-weight-bold" style="color: #000;">WAZiE Recruit Reviews</h2>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center" style="color: #000;">
                        <!-- Swiper -->
                        <div class="swiper-container" style="min-height: 200px;">
                            <div class="swiper-wrapper mb-5">
                                <div class="swiper-slide">
                                    <p class="font-italic slider-text">"Engyj is great, it’s user friendly and the team helped us customise it as per our organisation's needs and automate the recruitment process in the right pockets."</p>
                                    <img class="rounded-circle profile-img mt-4" src="meenal-profile.png"
                                        data-holder-rendered="true">
                                    <h4 style="color: #000;" class="m-0 slider-info"> <span class="font-weight-bold">Meenal</span> </br> Hiring Manager </br> Home Care Assistance Calgary</h4>
                                </div>
                                <div class="swiper-slide">
                                    <p class="font-italic slider-text">"We’ve used the WAZiE instant chat feature for recruiting for the last couple of weeks…we’ve received good feedback from candidates that have used the tool. It’s a great option for candidates wanting to jumpstart the recruiting process. Looking forward to using this tool in our recruiting efforts."</p>
                                    <img class="rounded-circle profile-img mt-4" src="ted-profile.png"
                                        data-holder-rendered="true">
                                    <h4 style="color: #000;" class="m-0 slider-info"> <span class="font-weight-bold">Ted Holmgren</span> </br> Owner </br> Home Care Assistance </br> Scottsdale and Chandler</h4>
                                </div>
                                <div class="swiper-slide">
                                    <p class="font-italic slider-text">"Features throughout the website are efficient and easy to use. Applicants have vocalized how easy it is to apply. As a recruiter I love how you can customize how applicants reach me, or vice versa. Kevin and the team have made the process of recruiting much smoother. It’s nice that WAZiE allows you to cater to your specified applicant."</p>
                                    <img class="rounded-circle profile-img mt-4" src="ruyter-profile.jpg"
                                        data-holder-rendered="true">
                                    <h4 style="color: #000;" class="m-0 slider-info"> <span class="font-weight-bold">Clint De Ruyter </span></br> Hiring Assistant </br> Home Care Assistance </br> Scottsdale and Chandler</h4>
                                </div>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                            
                        </div>
                    </div>
                </div>
                <!-- end of row -->
            </div>
            <!-- end of container -->
            </div>
            <!-- Ends Testimonials -->
    <!-- <section class="switchable switchable--switch bg--primary" id="header-section">
        <div class="container">
            <div class="row justify-content-between">
            <!-- Testimonials 
            
            </div>
        </div>
    </section> -->
@endsection
@push('footer-scripts')
<script>
/* Initialize Testimonials Swiper */
var swiper = new Swiper('.swiper-container', {
     slidesPerView: 1,
     spaceBetween: 30,
     loop: true,
     pagination: {
       el: '.swiper-pagination',
       clickable: true,
     },
     navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
     },
});      
   
   
</script>


@endpush