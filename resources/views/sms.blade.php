@extends('layouts.app')

@push('head-script')
<style>
    .search-input .form-control-lg{padding: 0.8rem 1rem;font-size: 0.9rem;background: #e4e7ee;border: 0;border-radius: 10px;}
    .search-input .form-control-lg::placeholder{color: #929292;}
    .search-input .input-group-text{background-color: #e4e7ee;border: 0;border-radius: 10px;color: #bdbdbd;font-size: 1.2rem;}
    .main-chatbot .bb-1{border-bottom:1px solid #e4e7ee}
    .saperator{border-bottom:1px solid #e4e7ee}
    .chatter{background: #fff;padding: 20px 15px;border-radius: 10px;box-shadow: 1px 1px 3px rgb(0 0 0 / 13%);}
    .chatter-old{background: transparent;box-shadow:none}
    .chatter-name {background: antiquewhite;text-align: center;height: 35px;width: 35px;border-radius: 30px;font-size: 10px;padding-top: 9.8px;}
    .chatter-content .msg{font-size: 12px;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;color: #868686;font-weight: 400;}
    .chatter-content   .chatter-full-name {font-weight: 500;}
    .chatter-right{text-align: center;color: #868686;}
    .chatter-right .time{}
    .chatter-right .rate i{color: #d4d4d4;}
    .chatter-right .badge-primary{background-color: #4e5d9e;font-weight: 700 !important;}
    .customer-msg {border: 1px solid rgb(0 0 0 / 8%);border-radius: 5px;padding: 10px;font-size: 12px;font-weight: 400;color: #464646;line-height: 20px;}
    .customer-msg .msg-time {margin-bottom: 0;text-align: right;color: rgb(0 0 0 / 33%);}
    .agent-msg{width: calc( 100% - 40px );border-radius: 5px;padding: 10px;font-size: 12px;font-weight: 400;color: #464646;align-self: flex-end;line-height: 20px;background-color: #f5f6fa;}
    .agent-msg .msg-time {margin-bottom: 0;text-align: right;color: rgb(0 0 0 / 33%);}
    .write-msg-area{position:relative}
    .write-msg-area textarea{resize:none;height:100px;padding-right: 89px;}
    .write-msg-area button{position: absolute;bottom: 10px;right: 10px;}
    .write-msg-area button.btn-info{background-color: #4e5d9e;border-color: #4e5d9e;}
    .msg-content{height: calc(100vh - 365px);overflow-y:auto}
    .agent-image{width: 35px;height: 35px;border-radius: 100px;margin-left: 5px;} 
    .border-3{border-width: 3px !important;}
    .chatter-info h3 {font-size: 16px;}
    .chatter-info h3 span {font-size: 9px;color: #868686;}
    .chatter-info p.phone {font-size: 12px;}
    .chatter-info p.email {font-size: 11px;color:blue;text-decoration:underline}
    .chatter-info p.address {font-size: 11px;}
    .table-applicant-info{box-shadow: 0px 2px 3px rgb(0 0 0 / 12%);}
    .applcation-status h3{font-size: 14px;text-transform: uppercase;font-weight: 300;}
    .table-applicant-info th{font-size:11px;font-weight:700}
    .table-applicant-info th span{white-space: nowrap;font-size: 10px;font-weight: 700;background: green;color: #fff;border-radius: 23px;padding: 1px 7px;}
    .table-applicant-info td{font-size:11px;}
    .timeline.timeline-main:before{left: 0px;background: #b7b7b7;}
    .information .icon{padding: 5px 0;position: relative;z-index: 1;background: #fff;left: -7px;text-align: center;display: inline-block;}
    .information .icon + span{font-size: 11px;margin-left: -6px;position: relative;top: -1px;}
    .information .icon span{position: relative;top: -4px;font-size: 11px;}
    .information .icon i{font-size: 14px;background: #888;color: #fff;width: 20px;height: 20px;border-radius: 100px;text-align: center;padding-top: 3px;}
    .information .next-status {padding-left: 17px;margin-top: -6px;}
    .information .next-status strong{font-size: 11px;}
    .timeline-main h2{font-size: 14px;padding-left: 17px;text-transform: uppercase;font-weight: 300;position:relative}
    .timeline-main h2::before{content:"";width:10px;height:10px;background:#888;border-radius:100px;position:absolute;left: -2.9px;}
    .timeline-main .alert-warning{border-color: #fffabb!important;background-color: #fffabb!important;font-size: 11px;padding: 8px;}
    .blue-icon {background-color:#00c8d2 !important}
    .resume-btn .btn{font-size: 12px;padding: 3px 6px;}
    .start-rating span{color:#d0d0d0}
    .color-gold{color: gold !important;}
    
</style>
@endpush

@section('content')
<!--Chat Bot UI Start-->
<div class="col-12 px-0 main-chatbot" style="background:#fff">
    <div class="row mx-0">
        <div class="col-md-4 pl-0 pr-4" style="background-color:#f5f6fa">
            <div class="input-group mb-3 search-input">
                <input type="text" class="form-control form-control-lg" placeholder="Search">
                <div class="input-group-append">
                <button class="input-group-text"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <div class="saperator"></div>
            <div class="chatter my-3">
                <div class="row">
                    <div class="col-2">
                        <div class="chatter-name">AK</div>
                    </div>
                    <div class="col-8">
                        <div class="chatter-content">
                            <div class="chatter-full-name">Jessica Koel</div>
                            <div class="msg">Hey Jontray, do you remember remember remember remember </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="chatter-right">
                            <div class="time">11:26</div>
                            <div class="msg-alert badge badge-primary">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chatter chatter-old my-3">
                <div class="row">
                    <div class="col-2">
                        <div class="chatter-name">AK</div>
                    </div>
                    <div class="col-8">
                        <div class="chatter-content">
                            <div class="chatter-full-name">Jessica Koel</div>
                            <div class="msg">Hey Jontray, do you remember remember remember remember </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="chatter-right">
                            <div class="time">11:26</div>
                            <div class="rate"><i class="fa fa-star"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chatter chatter-old my-3">
                <div class="row">
                    <div class="col-2">
                        <div class="chatter-name">AK</div>
                    </div>
                    <div class="col-8">
                        <div class="chatter-content">
                            <div class="chatter-full-name">Jessica Koel</div>
                            <div class="msg">Hey Jontray, do you remember remember remember remember </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="chatter-right">
                            <div class="time">11:26</div>
                            <div class="rate"><i class="fa fa-star"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 px-0 border-right">
            <div class="row mx-0 p-4">
                <div class="col-12 px-0 my-2 msg-content">
                    <div class="row flex-column align-items-start mx-0">
                        <div class="col-9 my-2 customer-msg">
                            Hello, I bought something yesterday but haven't gotten any confirmation. Did the order go through?
                            <p class="msg-time">12:32</p>
                        </div>
                        <div class="col-9 align-self-end my-2">
                            <div class="row m-0">
                                <div class="agent-msg">
                                Hello, I bought something yesterday but haven't gotten any confirmation. Did the order go through?
                                <p class="msg-time">12:32</p>    
                            </div>
                                <div class="p-0 text-right">
                                    <img src="https://www.jvmpolytechnic.in/assets/upload/grid_img/12576_1.jpg" alt="" class="agent-image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-0 my-2 write-msg-area">
                    <textarea name="write_msg" id="write_msg" class="form-control" placeholder="Write a reply"></textarea>
                    <button class="msg-btn btn btn-info btn-sm"><i class="fa fa-send"></i> Send</button>
                </div>
            </div>
        </div>
        <div class="col-md-3 p-3">
            <div class="chatter-info mb-5">
                <h3 class="mb-0">Jassica Koel <span>30 year</span></h3>
                <p class="phone mb-0">+1 846 234-4383 <i class='fa fa-copy'></i></p>
                <p class="email mb-0">jassica@gmail.com</p>
                <p class="address mb-0">Downtown, San Diego, 92101 CA.</p>
                <p class="start-rating mb-1">
                    <span class="fa fa-star color-gold"></span>
                    <span class="fa fa-star color-gold"></span>
                    <span class="fa fa-star color-gold"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </p>
                <p class="resume-btn mb-0">
                    <a href="" class="btn btn-sm btn-sm btn-primary">View Resume</a>
                </p>
            </div>
            <div class="applcation-status mb-5">
                <h3>Active Applications</h3>
                <table class="table table-responsive table-applicant-info table-sm">
                    <tr>
                        <th colspan="2" class="border-top border-3 border-success">In-Home caregiver job, Dec 2020 (XY) <span>Interview Scheduled</span></th>
                    </tr>
                    <tr>
                        <td>Applied</td>
                        <td>12/25/2020 (7 days ago)</td>
                    </tr>
                    <tr>
                        <td>Qualification</td>
                        <td>89%</td>
                    </tr>
                    <tr>
                        <td>Source</td>
                        <td>Facebook</td>
                    </tr>
                </table>
            </div>
            <div class="timeline timeline-main">
                <h2>Timeline</h2>
                <div class="information">
                    <span class="icon"> <i class="fa fa-user"></i> </span>
                    <span>31/12/2020 am, Thursday</span>
                    <div class="next-status">
                        <p class="badge badge-success mb-0">Interview</p>
                    </div>
                </div>
                <div class="information">
                    <span class="icon"> <i class="fa fa-plus"></i></span>
                    <span>31/12/2020 am, Thursday</span>
                    <div class="next-status">
                        <strong>Note added by julian King</strong>
                        <p class="alert alert-warning mb-0" role="alert">Called speaks Spanish... has hospital experience</p>
                    </div>
                </div>
                <div class="information">
                    <span class="icon"> <i class="fa fa-plus"></i> </span>
                    <span>31/12/2020 am, Thursday</span>
                    <div class="next-status">
                        <strong>Note added by julian King</strong>
                    </div>
                </div>
                <div class="information">
                    <span class="icon"> <i class="fas fa-file-alt blue-icon"></i></span>
                    <span>31/12/2020 am, Thursday</span>
                    <div class="next-status">
                        <strong>Applied to job In-Home caregiver job, Dec 2020 (XY)</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Chat Bot UI End-->
@endsection

@push('footer-script')

@endpush