<?php

return array (
  'dashboard' => 'Dashboard',
  'jobCategories' => 'Job Categories',
  'settings' => 'Settings',
  'companySettings' => 'Company Settings',
  'rolesPermission' => 'Roles & Permissions',
  'skills' => 'Skills',
  'locations' => 'Locations',
  'jobs' => 'Jobs',
  'jobApplications' => 'Applicants',
  'allApplications' => 'All Applicants',
  'myProfile' => 'My Profile',
  'team' => 'Team',
  'themeSettings' => 'Theme',
  'updateApplication' => 'Update Application',
  'mailSetting' => 'Email Settings',
  'smtpSetting' => 'Smtp Setting',
  'question' => 'Enter your Question',
  'createAQuestion' => 'Create a Question',
  'customQuestions' => 'Custom Questions',
  'sort' => 'Sort',
  'order' => 'Order',
  'add' => 'Add',
  'export' => 'Export To Excel',
  'interviewSchedule' => 'Interviews',
  'interviewCalendar' => 'Interview Calendar',
  'interviewDate' => 'Interview Date',
  'status' => 'Status',
  'companies' => 'Companies',
  'businessSettings' => 'Company',
  'frontCms' => 'Front CMS',
  'cmsGeneral' => 'CMS General',
  'cmsFeatures' => 'Image Features',
  'iconFeatures' => 'Icon Features',
  'clientFeedbacks' => 'Client Feedbacks',
  'packages' => 'Packages',
  'subscription' => 'Subscription',
  'paymentSettings' => 'Payment Settings',
  'subscriptionInvoice' => 'Subscription Invoice',
  'subscriptionDetails' => 'Subscription Details',
  'home' => 'Home',
  'features' => 'Features',
  'reviews' => 'Reviews',
  'pricing' => 'Pricing',
  'contact' => 'Contact',
  'linkedInSettings' => 'Linkedin Settings',
  'facebookSettings' => 'Facebook Settings',
  'history' => 'Payment History',
  'smsSettings' => 'SMS Settings',
  'profile' => 'Profile',
  'sendJobEmails' => 'Send New Job Emails',
  'todoList' => 'ToDos',
  'archivedApplications' => 'Archived Applicants',
  'candidateDatabase' => 'Candidate Database',
  'applicationFormSettings' => 'Email and Text Alerts',
  'customModules' => 'Custom Modules',
  'footerSettings' => 'Footer Settings',

  'jobTemplates' => 'Job Templates',
  'unCategorized' => 'UnCategorized',
  'templates' => 'Templates',
  'customTemplates' => 'Custom Templates',
  'facebookApplicants' => 'Facebook Applicants',
  'facebookLead' => 'Facebook Lead',

  'allJobs' => 'All Jobs',
  'candidates' => 'Candidates',
  'employees' => 'Employees',
  'preferences' => 'Preferences',
  'customWorkflows' => 'Custom Workflows',
  'integrations' => 'Integrations',
  'plans' => 'Plans',
  'accounts' => 'My Accounts',
  'account' => 'My Account',
  'createnewjob' => 'Create a new Job',
  'landingPage' => 'Landing Page',
  'customQuestion' => 'Custom Question',
  'questions' => 'Questions',
  'questionnaire' => 'Questionnaire',
  'questionnaires' => 'Questionnaires',
  'internal_questionnaires' => 'Internal Questionnaires',
  'templates' => 'Templates',
  'smsMessages' => 'Text Messages',
  'and' => 'and',
  'time' => 'Time',
  'applicant' => 'Applicant',
  'messages' => 'Messages',
  'before' => 'Before',
  'sending' => 'Sending',
  'reports' => 'Reports',
  'budget' => 'Budget',
  'setBudget' => 'Set Budget',
  'contactForms' => 'Contact Forms',
  'workflows'     => 'Workflows',
  'customEvents' => 'Custom Events',
  'triggers'     => 'Triggers',
  'holidays' => 'Holidays',
  'manageTriggers'     => 'Manage Triggers',
  'addApplicant' => 'Add Applicant',
  'tasks' => 'Tasks',
);
