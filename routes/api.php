<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function () {
    Route::get('/job/{id}', 'Front\FrontJobsController@apiGetJob');

    Route::post('/career-apply', 'Front\FrontJobsController@jobApplyCareer');
    Route::post('/career-caregiver-apply', 'Front\FrontJobsController@jobApplyCaregiver');
    
    Route::get("/applicants", "Api\ApiController@getApplicants");
    Route::get("/jobs", "Api\ApiController@getJobList");
    Route::get("/events", "Api\ApiController@getEvents");
    Route::get('/validateEmail', 'Api\ApiController@validateEmail');
    Route::post('pushNotification',function (Request $request){
        if($request->header('X-Goog-Channel-ID')){
            $user = \App\User::where("GC_channel_id", $request->header('X-Goog-Channel-ID'))->first();
            if($user){
                $base_path = public_path('assets/GC_USERS')."/";
                $eventData = json_encode(getExternalGEvents($user));
                $file_base_path = $base_path.'c_' . $user->company_id . '_u_'.$user->id . '.cache';
                if($eventData == '[]'){
                    if(file_exists($base_path.'c_' . $user->company_id . '_u_'.$user->id)){
                        unlink($file_base_path);
                    }
                }else{
                    file_put_contents($file_base_path ,$eventData);
                }
            }
        }
    });
});
