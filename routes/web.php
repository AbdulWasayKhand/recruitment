<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\InterviewSchedule;
use Illuminate\Http\Request;
use App\JobApplication;
use App\ApplicantHistory;
use App\Option;
use App\ApplicationLabel;
use App\LabelTransition;


Route::get('SyncGC', function(Request $request){
    if(!file_exists(public_path('assets/GC_USERS'))){
        mkdir(public_path('assets/GC_USERS'), 0777, true);
    }
    try{
        if($request->has('user_id')){
            $user = \App\User::find($request->user_id);
            if($user->GC_refresh_token != ''){
                $base_path = public_path('assets/GC_USERS')."/";
                $eventData = json_encode(getExternalGEvents($user));
                $file_base_path = $base_path.'c_' . $user->company_id . '_u_'.$user->id . '.cache';
                if($eventData == '[]'){
                    if(file_exists($base_path.'c_' . $user->company_id . '_u_'.$user->id)){
                        unlink($file_base_path);
                    }
                }else{
                    file_put_contents($file_base_path ,$eventData);
                }
                $mesg = ['status'=>'success', 'message' => 'Google calander sync successfully','users' => [$user->id]];
            }else{
                $mesg = ['status'=>'fail', 'message' => 'Calander not linked or No event retrived', 'users' => [$user->id]];
            }
        }
    }catch(Exception $e){
        $mesg = ['status'=>'fail', 'message' => 'Enternet connection lost', 'users' => []];
    }
    return $mesg;
});
Route::post('deleteLogo', function(Request $request){
    $company = \App\Company::find($request->company_id);
    $company->logo = '';
    $company->save();
});
Route::post("linkCM_GC", function(Request $request){
    $user = \App\User::find($request->user_id);
    if($user->clone_google_events){
        $user->clone_google_events = 0;
        $user->save();
        return ["data"=>"Link With Google's Events"];
    }else{
        $user->clone_google_events = 1;
        $user->save();
        return ["data"=>"Unlink With Google's Events"];
    }
});

Route::post('sendSMS', function(Request $request){
    $log = new \App\SmsLog();
    $log->company_id = $request->company_id;
    $log->message_type = 'outbound';
    $log->type = 'normal';
    $log->number = str_replace(' ', '',  $request->to)  ;
    $log->message = $request->message;
    $log->status = 'pending';
    $log->send_at = date('Y-m-d h:i:s');
    $log->reference_id = $request->applicant_id;
    $log->reference_name = 'personal-sms';
    $log->save();  
    return $request;
});
Route::get('app', function(Request $request){

    $all_applicant = \App\JobApplication::where('job_id','=',$request->job_id)->get();
    $workflow_id = $all_applicant->first()->job->workflow_id;

    $stage_id = \App\WorkflowStage::where('workflow_id', '=', $workflow_id)->pluck('id', 'title');
    $app = [];
    foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
        foreach($stage_id as $stage=>$sid){
            foreach($all_applicant as $applicant){
                if($applicant->workflow_stage_id == $sid){
                    if(gettype(strpos($applicant->referer_url, $keywords)) == 'integer'){
                        $app[$key][$applicant->id] = $applicant;
                    }else if($applicant->referer_url == null){
                        $app['Direct'][$applicant->id] = $applicant;
                    }else if(gettype(strpos($applicant->referer_url, 'facebook')) != 'integer' && gettype(strpos($applicant->referer_url, 'google')) != 'integer' && gettype(strpos($applicant->referer_url, 'indeed')) != 'integer' && gettype(strpos($applicant->referer_url, 'linkedin')) != 'integer'){
                        $app['Others'][$applicant->id] = $applicant;
                    }
                }
            }
        }  
    }
    foreach($app['Others'] as $k=>$v){
        foreach($app['Direct'] as $k2=>$v2){
            if($k == $k2){
                unset($app['Others'][$k]);
            }
        }
    }
    $sources = [];
    foreach(['Facebook'=>'facebook', 'Google'=>'google', 'Indeed'=>'indeed', 'Linkedin'=>'linkedin', 'Direct'=>'recruit', 'Others'=>'others'] as $key=>$keywords){
        $arr_1 = ['source' => $key];
        $arr_2 = [];
        foreach($stage_id as $stage=>$id){
            $arr_1 =  array_merge($arr_1, [str_replace(' ', '_', $stage)  => 0]);
            if(isset($app[$key])){
                foreach($app[$key] as $appli){
                    if($appli->workflow_stage_id == $id){
                        $arr_1[str_replace(' ', '_', $stage)] += 1;
                    }
                }
            }
        }
        $sources[] = $arr_1;
    }
    echo "<pre>";
    // $app['Facebook'][2688]->workflow_stage_id
    print_r($sources) ;
});
Route::post('/saveApplicantAnswer', function(Request $request){
    if($request->has('questionnaire_id') && $request->questionnaire_id){
        $questionnaire = \App\Questionnaire::find($request->questionnaire_id);
        $questions = $questionnaire->questions();
        $fields = $questionnaire->custom_field_values->first();
        $job = \App\Job::find($request->job_id);
        $jobApplication = \App\JobApplication::find($request->job_application_id);
        $user_qualified = 'qualified';
        $printData = [];
        if ($request->has('answer') && count($request->answer)) {
            $totalScore = 0;
            $totalCount = 0;
            foreach ($request->answer as $key => $answer_value) {
                $value = implode(', ', (array)$answer_value);
                $answer = \App\JobApplicationAnswer::firstOrNew([
                    'job_application_id'=>$request->job_application_id,
                    'job_id'=>$request->job_id,
                    'questionnaire_id'=> $request->questionnaire_id,
                    'question_id'=>$key
                    ]);
                    $answer->job_application_id = $request->job_application_id;
                    $answer->job_id = $request->job_id;
                    $answer->question_id = $key;
                    $answer->company_id = $job->company_id;
                    $answer->answer = $value;
                    // if(!$answer->id){
                        $answer->questionnaire_id = $request->questionnaire_id;
                    // }
                    $question = \App\Question::with('custom_fields')->find($key);
                    if($question && $question->custom_fields->count() > 0) {
                        $custom_question = $question->custom_fields[0];
                        $question_value  = json_decode($custom_question->question_value, 1);
                        $question_qualifiable  = json_decode($custom_question->question_qualifiable, 1);
                        if(is_array($question_qualifiable)) {
                            // $answer->score = 0;
                            $_total = count($question_qualifiable);
                            $_total = (($custom_question->question_type=='checkbox') && $_total>0) ? 1 : $_total;
                            $totalCount += 1;
                            foreach($question_qualifiable as $q) {
                                if(isset($question_value[$q]) && in_array($question_value[$q], (array)$answer_value)) {
                                    $totalScore += (1 / $_total);
                                }
                            }
                        }
                    }
                    $answer->save();
                    $printData[$answer->question_id]['question'] = $answer->question->question;
                    $printData[$answer->question_id]['answer'] = $answer->answer;
            }
            $jobApplication->save();
            if($jobApplication->eligibility < $job->eligibility_percentage) {
                $user_qualified = 'unqualified';
            }
        }
        $data = [];
        foreach ($questions as $question) {
            $data[$question->id]['question'] = $question->question;
            $data[$question->id]['answer'] = array_key_exists($question->id,$printData) ? $printData[$question->id]['answer'] : '';
        }
            // Save Timeline
    $name = $jobApplication->full_name;
    saveTimeline([
        'applicant_id' =>   $jobApplication->id,
        'entity'       =>   'questionnaires',
        'entity_id'    =>   $request->questionnaire_id,
        'comments'     =>   "<b>External Questionnaire:</b> <i>(" . $questionnaire->name . ")</i> filled by {$name}"
    ]);
        // return Reply::dataOnly(['status' => 'success', 'msg' => __('modules.front.applySuccessMsg'),'printData'=>$data]);
        return $request;
    }
});
Route::get('interview-schedule/response/{id}/{type}', 'Admin\InterviewScheduleController@employeeResponse')->name('interview-schedule.request-accept');
Route::get('/-/{flag}', function ($flag){
    $flag = explode('.', $flag);
    $applicant_id = $flag[0];
    $qid = $flag[1];
    $data['questions'] = \App\Question::all();
    $questionnaire = \App\Questionnaire::find($qid);
    $data['custom_fields'] = $questionnaire->custom_field_values;
    $data['questionnaire_id'] = $qid;
    $data['questionnaire'] = $questionnaire;
    $data['jobApplication'] = \App\JobApplication::findOrNew($applicant_id);
    $answers = \App\JobApplicationAnswer::where([
        'job_application_id'=> $applicant_id,
        'job_id'=> $data['jobApplication']->job_id,
        'questionnaire_id'=>$qid,
    ]);
    $data['thanks_page'] = $data['jobApplication']->id ? 
                    \App\Option::getOption('thanks_page_url', $data['jobApplication']->company_id) :
                    \App\Option::getOption('thanks_page_url', \App\JobApplication::onlyTrashed()->find($applicant_id)->company_id);
    if($data['jobApplication']->id && $answers->count() == 0){
        return view('admin.questionnaire.questionnaire', $data);
    }
    return redirect($data['thanks_page']);
// $data['answers'] = $answers->count() ? $answers->pluck('answer','question_id')->toArray() : [];
    
});


Route::post('update-status', function(Request $request){
    $application = JobApplication::find($request->application_id);
    $applicantHistory = new ApplicantHistory();
    $applicantHistory->current_value = $request->current_value;
    $applicantHistory->previous_value = $request->previous_value;
    $applicantHistory->status_type = 'application';
    $applicantHistory->company_id = $application->company_id;
    $applicantHistory->applicant_id = $request->application_id;
    $applicantHistory->save();
});
Route::post('labels/delete', function (Request $request){
    LabelTransition::where('label_id', '=', $request->lid)->delete();
    ApplicationLabel::find($request->lid)->delete();
});
Route::post('labels/update', function (Request $request){
    $value = str_replace("&nbsp;","",preg_replace('#<[^>]+>#', '',$request->value));

    $checkLabel = ApplicationLabel::where('label_value', '=', $value)
                                    ->where('label_color', '=', $request->color)
                                    ->where('company_id', '=', $request->cid)
                                    ->count();
    if($checkLabel == 0){
        $label = ApplicationLabel::find($request->lid);
        $label->label_value = preg_replace('#<[^>]+>#', '',$value);
        $label->save();
    }
});
Route::post('labels/addLabel', function (Request $request){
    $value = str_replace("&nbsp;","",preg_replace('#<[^>]+>#', '',$request->value));
    $checkLabel = ApplicationLabel::where('label_value', '=', $value)
                                    ->where('label_color', '=', $request->color)
                                    ->where('company_id', '=', $request->cid)
                                    ->count();
    if($checkLabel == 0 && $request->value != ''){
        $newlabel = new ApplicationLabel();
        $newlabel->label_value = $value;
        $newlabel->label_color = $request->color;
        $newlabel->company_id = $request->cid;
        $newlabel->save();
        return ['<span class="lbl"  onclick="addLabel(this, ' . $request->aid . ', ' .$request->cid . ')" data-id="' . $newlabel->id . '" data-value="' . $value . '" data-color="' . $request->color . '" ><span class="lbl_pointer" style="background: ' . $request->color . ';"></span><span class="lbl_txt" style="background: ' . $request->color . ';">' . $request->value . '</span><i class="fas fa-check tick" style="display:none;"></i></span>'];
    }else{
        return [];
    }
});

Route::post('applicant/paste_label', function (Request $request){
    $lt = new LabelTransition();
    $lt->label_id = $request->label_id;
    $lt->application_id = $request->application_id;
    $lt->save();
    $user = \App\User::find($request->user_id ?? 0);
    // Save Timeline
    if($user){
        $timeline = saveTimeline([
            'applicant_id' =>   $request->application_id,
            'entity'       =>   'label',
            'entity_id'    =>   $request->label_id,
            'user_id'      =>   $user->id ?? 0,
            'comments'     =>  "'" . $lt->label->label_value . "' added by " .$user->name. " on " . $request->stage_name
        ]);
        $timeline->dateTime = \Carbon\Carbon::parse($timeline->created_at)->timezone($timeline->applicant->company->timezone)->format('m/d/Y h:i A. l');
        return $timeline;
    }
    
    return $lt;
});

Route::post('applicant/delete_label', function (Request $request){
    LabelTransition::where('application_id', '=', $request->application_id)
                    ->where('label_id', '=', $request->label_id)
                    ->delete();
    return $request;
});

Route::get("GCCRON", function(){
    Artisan::call("gc:notification");
});
Route::get("ScheduleUploads", function(){
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = 'http://localhost/recruitment/public/oauth2callback';
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken('1//03HKXy7jRkcq5CgYIARAAGAMSNwF-L9IrrKAg-Lx3vRkyjdkOUCM7D8sx5FTj2sOgUgOULOmTTmTjEfdjnTzcer-4nVk5tM5tdR8');
    $client->getAccessToken();
    $service = new Google_Service_Calendar($client);
            $event = new Google_Service_Calendar_Event(array(
            'summary' => 'Interview with Shafi Muhammad',
            'location' => "USA",
            'description' => "Test",
            'start' => array(
              'dateTime' => "2021-01-27T17:00:00",
              'timeZone' => 'GMT',
            ),
            'end' => array(
              'dateTime' => "2021-01-27T17:00:00",
              'timeZone' => 'GMT',
            ),
            // 'recurrence' => array(
            //   'RRULE:FREQ=DAILY;COUNT=2'
            // ),
            'attendees' => array(
              array('email' => 'khand.shafi@gmail.com'),
            ),
            'reminders' => array(
              'useDefault' => FALSE,
              'overrides' => array(
                array('method' => 'email', 'minutes' => 24 * 60),
                array('method' => 'popup', 'minutes' => 10),
              ),
            ),
          ));
          
          $calendarId = 'primary';
          $event = $service->events->insert($calendarId, $event);
          return $event->id;

});

Route::get("ScheduleDelete", function(){
    $user = user();
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = url('oauth2callback');
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken(\App\User::find($user->id)->GC_refresh_token);
    $client->getAccessToken();
    $service = new Google_Service_Calendar($client);
    $calendarId = 'primary';
    $check = $service->events->get('primary', $_REQUEST['eid']);
    // $event = $service->events->delete($calendarId, $_REQUEST['eid']);
    print_r($check->status);
});
Route::get('ScheduleDownload', function(){
    $user = user();
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = url('oauth2callback');
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);    
    $client->refreshToken(\App\User::find($user->id)->GC_refresh_token);
    $client->getAccessToken();
    $service = new Google_Service_Calendar($client);
    $calendarId = 'primary';
    $optParams = array(
    'maxResults' => 10,
    'orderBy' => 'startTime',
    'singleEvents' => true,
    'timeMin' => date('c'),
    );
    $results = $service->events->listEvents($calendarId, $optParams);
    $events = $results->getItems();
    $html = "";
    $ar = [];
    if (!empty($events)) { 
        foreach ($events as $event) {
            // $start = $event->start->dateTime != "" ? $event->start->dateTime : $event->start->date . "T00:00:00";
            // $end = $event->end->dateTime != "" ? $event->end->dateTime : $event->start->date . "T00:00:00";
            // $date = $event->start->date == null ? explode("T", $event->start->dateTime)[0] : $event->start->date;
            // $UTC_start = explode("-", explode("+", explode("T", $start)[1])[0])[0];
            // $GMT_start = new DateTime(date("Y-m-d h:i A", strtotime($date . "T" . $UTC_start)), new DateTimeZone("GMT"));
            // $GMT_start = $GMT_start->format("h:i A");
            // $UTC_end = explode("-",explode("+", explode("T", $end)[1])[0])[0];
            // $GMT_end = new DateTime(date("Y-m-d h:i A", strtotime($date . "T" . $UTC_end)), new DateTimeZone("GMT"));
            // $GMT_end = $GMT_end->format("h:i A");
            // $i = InterviewSchedule::where("google_event_id", '=', $event->id)->first();
            // if($i){
            //     // $change = false;
            //     // $day = explode("T", $i->toArray()["schedule_date"]);
            //     // $slot = explode(" - ", $i->slot_timing);

            //     // if(date("Y-m-d", strtotime($i->schedule_date)) != date("Y-m-d", strtotime($date))){
            //     //     $i->schedule_date = date("Y-m-d", strtotime($date));
            //     //     $change = true;
            //     // }
            //     // if(date("h:i A", strtotime($day[0] . " " . $slot[0])) != $GMT_start){
            //     //     $slot[0] =  $GMT_start;
            //     //     $change = true;
            //     // }
            //     // if(date("H:i A", strtotime($day[0] . " " . $slot[1])) != $GMT_end){
            //     //     $slot[1] = $GMT_end;
            //     //     $change = true;
            //     // }
            //     // $i->slot_timing = implode(" - ", $slot);
            //     // if($change){
            //     //     $i->save();
            //     // }
            
            // }
            // echo $date ." " . $GMT_start;
            // echo "<br>";
            // echo date("Y-m-d H:i:s", strtotime($date ." " . $GMT_start));
            // break;
            print_r($event);
            echo "<br><br>---------------------------<br><br>";

        }
    }

});

// Email Attachment
Route::group(['prefix' => 'email-attachment'], function () {
    Route::get('/{data}', 'EmailAttachmentController@index');
    Route::post('store', 'EmailAttachmentController@store');
});

Route::get('unlinkGC', function(){
    $user = \App\User::find(user()->id);
    if($user){
        $user->GC_refresh_token = null;
        $user->GC_channel_id = null;
        $user->save();
        $base_path = public_path('assets/GC_USERS').'/c_' . $user->company_id . '_u_'.$user->id . '.cache';
        if(file_exists($base_path)){
            unlink($base_path);
        }
    }
    return redirect("admin/interview-schedule");
});
Route::get('oauth2callback', function(){
    $clientID = '296451324102-r204vuqu8oloectp4uvmgifmgesos5qd.apps.googleusercontent.com';
    $clientSecret = '85NqH8WJnmwOL-L86xEGYLuF'; 
    $redirectUri = url('oauth2callback');
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);
    $client->setApprovalPrompt('force');
    $client->setAccessType('offline');
    $client->addScope("https://www.googleapis.com/auth/calendar");
    $client->addScope("https://www.googleapis.com/auth/calendar.events"); 
    $client->addScope("https://www.googleapis.com/auth/calendar.events.readonly"); 
    $client->addScope("https://www.googleapis.com/auth/calendar.events"); 
    if (isset($_GET['code'])) { 
        $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $client->fetchAccessTokenWithRefreshToken($token);
        $calendar = new Google_Service_Calendar($client);
        $channel =  new Google_Service_Calendar_Channel($calendar);
        $channelId = Illuminate\Support\Str::random(42);
        $channel->setId($channelId);
        $channel->setType('web_hook');
        $channel->setToken($token["refresh_token"]);
        $channel->setAddress(url('api/pushNotification'));
        $timetoExpire = time()+3600000;
        $optParams = array('ttl' => $timetoExpire);
        $channel->setParams($optParams);
        $watchEvent = $calendar->events->watch('primary', $channel);
        
        $user = \App\User::find(user()->id);
        $user->GC_channel_id = $channelId;
        $user->GC_refresh_token = $token["refresh_token"];
        $user->save();
        $base_path = public_path('assets/GC_USERS')."/";
        $eventData = json_encode(getExternalGEvents($user));
        $file_base_path = $base_path.'c_' . $user->company_id . '_u_'.$user->id . '.cache';
        if($eventData == '[]'){
            if(file_exists($base_path.'c_' . $user->company_id . '_u_'.$user->id)){
                unlink($file_base_path);
            }
        }else{
            file_put_contents($file_base_path ,$eventData);
        }
        // dd(["RESPONSE_URL_FOR_CALANDER"=>url('api/pushNotification'),"CALANDER_OF_LOGGENIN_USER"=> $calendar,"SETUP_CHANNAL"=>$channel, "EVENTS_WATCH_RESPONSE_OF_LOGGENGIN_USER"=>$watchEvent, "REQUEST" => $_REQUEST]);
        return redirect("admin/interview-schedule");
    } else {
        return redirect()->away($client->createAuthUrl());
    }  
});

Route::get('/test', function () {
    $t = request()->ver;

    if($t == 1) {
        $message = \App\Option::getOption('qualified_text', user()->company_id ?? 4);

        $message = strtr($message, [
            '[candidate_name]' => 'Hasan',
        ]);
        
        return (new \App\Mail\MailQualification(\App\JobApplication::latest()->first(), 'qualified', $message));
    }
    if($t == 2)
    return (new \App\Notifications\CandidateScheduleInterview(\App\JobApplication::first(), \App\InterviewSchedule::latest()->first(), 0))->toMail(\App\User::first());
    
    if($t == 4)
        return (new \App\Notifications\ScheduleStatusCandidate(\App\JobApplication::latest()->first(), \App\InterviewSchedule::latest()->first()))->toMail(\App\User::latest()->first());

    if($t == 5) {
        $app = \App\JobApplication::latest()->first();
        $message = \App\Option::getOption('qualified_text', $app->company_id);

        $message = strtr($message, [
            '[candidate_name]' => 'Hadi',
        ]);
        return (new \App\Mail\MailQualification($app, 'qualified', $message));

    }
});

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    
    return [];
});

//SAAS Front routes start
Route::group(
    ['namespace' => 'SaasFront'],
    function () {
        Route::get('/', 'SaasFrontController@index')->name('index');
        Route::post('contact_form_submit', 'SaasFrontController@submitContact')->name('contact_form_submit');
        Route::post('/company-register', 'SaasFrontController@companyRegister')->name('company-register');
        Route::get('/email-verification/{code}', 'SaasFrontController@getEmailVerification')->name('get-email-verification');
        Route::post('change-language/{code}', 'SaasFrontController@changeLanguage')->name('changeLanguage');
        Route::get('page/{slug?}', 'SaasFrontController@page')->name('page');
        Route::get('/reviews', 'SaasFrontController@reviews')->name('reviews');
    }
);

//Front routes end

//Front routes start
Route::group(
    ['namespace' => 'Front', 'as' => 'jobs.'],
    function () {
        Route::get('/careers/{slug}', 'FrontJobsController@jobOpenings')->name('jobOpenings');
        Route::get('/schedule-request-thankyou-page/{slug}', 'FrontJobsController@scheduleRequestThankyou')->name('schedule-request-thankyou-page');
        Route::get('/schedule-not-found/{slug}', 'FrontJobsController@scheduleNotFound')->name('schedule-not-found');
        Route::get('/job/{slug}', 'FrontJobsController@jobDetail')->name('jobDetail');
        Route::get('/job-offer/{slug?}', 'FrontJobOfferController@index')->name('job-offer');
        Route::post('/save-offer', 'FrontJobOfferController@saveOffer')->name('save-offer');
        Route::get('/job/{slug}/apply', 'FrontJobsController@jobApply')->name('jobApply');
        Route::post('/job/saveApplication', 'FrontJobsController@saveApplication')->name('saveApplication');
        Route::post('/job/fetch-country-state', 'FrontJobsController@fetchCountryState')->name('fetchCountryState');
        /*Route::get('/redirect/linkedin', ['uses' => 'FrontJobsController@redirectToProvider', 'as' => 'social.login']);
        Route::get('/linkedin/callback', ['uses' => 'FrontJobsController@callback']);*/
        Route::get('auth/callback/{provider}', 'FrontJobsController@callback')->name('linkedinCallback');
        Route::get('auth/redirect/{provider}', 'FrontJobsController@redirect')->name('linkedinRedirect');


        Route::post('/job-apply-career', 'FrontJobsController@jobApplyCareer');
    }
);

Route::post('/twilio-sms-reply', 'HomeController@twilioSmsReply')->middleware('cors');
Route::get('/twilio-sms-reply', 'HomeController@twilioSmsReply')->middleware('cors');
//Front routes end
// Paypal IPN Confirm
Route::post('verify-billing-ipn', array('as' => 'verify-billing-ipn','uses' => 'PaypalIPNController@verifyBillingIPN'));
Route::post('/save-invoices', ['as' => 'save_webhook', 'uses' => 'StripeWebhookController@saveInvoices']);
Route::post('/save-razorpay-invoices', ['as' => 'save_razorpay-webhook', 'uses' => 'RazorpayWebhookController@saveInvoices']);
Route::get('/check-razorpay-invoices', ['as' => 'check_razorpay-webhook', 'uses' => 'RazorpayWebhookController@checkInvoices']);

//Inteview Schedule
Route::get('/schedule-interview', 'Admin\InterviewScheduleController@candidateScheduleInterview')->name('candidate.schedule-inteview');
Route::post('/schedule-interview/create', 'Admin\InterviewScheduleController@store')->name('candidate.interview-schedule.store');
Route::post('/schedule-interview/update/{application_id}', 'Admin\InterviewScheduleController@update')->name('candidate.interview-schedule.update');
Route::post('/interview-schedule/change-status', 'Admin\InterviewScheduleController@changeStatus')->name('candidate.interview-schedule.change-status');
Route::post('/schedule-interview/get-timings', 'Admin\InterviewScheduleController@getTiming')->name('candidate.interview-schedule.get-timing');
Route::get('/interview-canceled-page/{application_id}', 'Admin\InterviewScheduleController@interviewCanceled')->name('candidate.interview-canceled');
Route::post('/auto-login', 'Auth\LoginController@apiAutoLogin');

Auth::routes();
// Admin routes
Route::group(['middleware' => 'auth'], function () {

    Route::post('mark-notification-read', ['uses' => 'NotificationController@markAllRead'])->name('mark-notification-read');

    // Admin routes
    Route::group(
        ['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'],
        function () {
            Route::group(['prefix' => 'workflow', 'as' => 'workflow.'],function ()
            {
                Route::get('index', 'AdminWorkFlowController@index');
                Route::get('workflow', 'AdminWorkFlowController@workflow');
                Route::get('/data', 'AdminJobCategoryController@data');
                Route::get('/create-stage', 'AdminWorkFlowController@createStage')->name('create-stage');
                Route::post('/change-stage', 'AdminWorkFlowController@changeStage')->name('change-stage');
                Route::post('/change-stage-order', 'AdminWorkFlowController@changeStageOrder')->name('change-stage-order');
                Route::post('/save-stage', 'AdminWorkFlowController@saveStage')->name('save-stage');
                Route::post('/delete-stage', 'AdminWorkFlowController@deleteStage')->name('delete-stage');
                Route::post('/update-workflow','AdminWorkFlowController@updateWorkflowName')->name('update-workflow');
                Route::post('/delete-workflow','AdminWorkFlowController@deleteWorkflow')->name('delete-workflow');
                Route::post('/workflow-stage-action-emails', 'AdminWorkFlowController@emailAction')->name('workflow-stage-action-emails');
                Route::post('/workflow-stage-action-emails', 'AdminWorkFlowController@emailAction')->name('workflow-stage-action-emails');
                Route::post('/workflow-stage-action-sms', 'AdminWorkFlowController@smsAction')->name('workflow-stage-action-sms');
                Route::post('/delete-email-action', 'AdminWorkFlowController@deleteEmailAction')->name('delete-email-action');
                Route::post('/delete-sms-action', 'AdminWorkFlowController@deleteSmsAction')->name('delete-sms-action');
                Route::post('/add-workflow-stage-action-questionnaires', 'AdminWorkFlowController@addQuestionnaire')->name('add-workflow-stage-action-questionnaires');
                Route::post('/delete-questionnaire', 'AdminWorkFlowController@deleteQuestionnaire')->name('delete-questionnaire');
                Route::post('/delete-change-stage-action', 'AdminWorkFlowController@deleteChangeStageAction')->name('delete-change-stage-action');
                Route::post('/email-attachment', 'AdminWorkFlowController@emailAttachment')->name('email-attachment');
                Route::post('/delete-email-attachment', 'AdminWorkFlowController@deleteEmailAttachmentAction')->name('delete-email-attachment');
                // Interview Action
                Route::group(['prefix' => 'action', 'as' => 'action.'],function ()
                {
                    Route::group(['prefix' => 'interview', 'as' => 'interview.'],function ()
                    {
                        Route::post('/save', 'AdminWorkFlowController@saveInterview')->name('save');
                        Route::post('/delete', 'AdminWorkFlowController@deleteInterview')->name('delete');
                    });
                });

                // Workflow Triggers
                Route::group(['prefix' => 'trigger', 'as' => 'trigger'],function ()
                {
                    Route::get('/', 'AdminWorkFlowTriggerController@index');
                    Route::get('create', 'AdminWorkFlowTriggerController@create');
                    Route::get('edit/{id}', 'AdminWorkFlowTriggerController@edit');
                    Route::post('store', 'AdminWorkFlowTriggerController@store');
                    Route::group(['prefix' => 'condition', 'as' => '.condition'],function ()
                    {
                        Route::post('/delete', 'AdminWorkFlowTriggerController@deleteCondition');
                    });
                    Route::post('{id}/delete', 'AdminWorkFlowTriggerController@destroy');
                });                
            });

            Route::get('/migrate', function() {
                Artisan::call('migrate');
                
                return [];
            });

            Route::get('send-sms', function () {
                $from = request('from', '2258008941');
                $msg  = request('message', 'Test Message From Engyj');
                if(request('sid')) {

                    return App\Helper\TwilioHelper::set(NULL, NULL)->fetch(request('sid'));
                }
                $sms  = App\Helper\TwilioHelper::set($from, $msg)->send();
                return $sms['message']->toArray();
            });
            
            Route::group(['middleware' => ['active-package']], function () {
                Route::group(['prefix' => 'reports'],function ()
                {
                    Route::get('/', 'AdminReportController@index')->name('reports');
                    Route::post('get-engyj-data', 'AdminReportController@get_engyj_data');
                    Route::post('get-engyj-data-for-dashboard', 'AdminReportController@get_engyj_data_for_dashboard');
                    Route::post('stage-data', 'AdminReportController@stage_data');
                    Route::post('funnel-data', 'AdminReportController@funnel_data');
                    Route::post('funnel-data-two', 'AdminReportController@funnel_data_two');
                    Route::post('sources-data', 'AdminReportController@sourcesData');
                    Route::post('qualified-data', 'AdminReportController@qualifiedData');
                    Route::post('HRS-data', 'AdminReportController@HRSData');
                    Route::post('status-data', 'AdminReportController@statusData');
                    Route::post('budget-table', 'AdminReportController@budgetTable');
                    Route::post('getRejectedData', 'AdminReportController@getRejectedData');
                    Route::group(['prefix' => 'budget'],function ()
                    {
                        Route::get('/', 'AdminBudgetController@index');
                        Route::post('/', 'AdminBudgetController@save');
                    });

                });
                Route::get('/dashboard', 'AdminDashboardController@index')->name('dashboard');
                Route::get('/schedule-profile', 'AdminProfileController@profileSchedule')->name('schedule-profile');
                Route::post('/schedule-profile/save', 'AdminProfileController@profileScheduleSave')->name('schedule-profile-save');
                Route::get('job-categories/data', 'AdminJobCategoryController@data')->name('job-categories.data');
                Route::get('job-categories/getSkills/{categoryId?}', 'AdminJobCategoryController@getSkills')->name('job-categories.getSkills');
                Route::resource('job-categories', 'AdminJobCategoryController');
                //Questions
                Route::get('questions/data', 'AdminQuestionController@data')->name('questions.data');
                Route::resource('questions', 'AdminQuestionController');
                Route::post('question-categories/add', 'AdminQuestionController@addCategory')->name('question.add-category');
                // questionnaires
                Route::get('questionnaire/data', 'AdminQuestionnaireController@data')->name('questionnaire.data');
                Route::get('questionnaire/get_answers/{questionnaire_id}', 'AdminQuestionnaireController@getAnswers')->name('questionnaire.get_answers');
                // Route::get('questionnaire/-/{flag}', 'AdminQuestionnaireController@getCandidateQuestionnaire')->name('questionnaire.candidate');
                Route::post('questionnaire/save_applicant_answers', 'AdminQuestionnaireController@saveApplicantAnswers')->name('questionnaire.save_applicant_answers');
                Route::resource('questionnaire', 'AdminQuestionnaireController');

                Route::post('facebook-applicants/leads/save', 'AdminFacebookApplicant@saveLead')->name('facebook.lead.save');
                Route::post('facebook-applicants/leads/show_date', 'AdminFacebookApplicant@show_date')->name('facebook.lead.show_date');
                Route::post('facebook-applicants/leads/sync', 'AdminFacebookApplicant@syncFacebookApplicants')->name('facebook.lead.sync');
                Route::resource('facebook-applicants', 'AdminFacebookApplicant');

                Route::post('dashboard/get-engyj-data', 'AdminDashboardController@get_engyj_data');

                Route::get("text-messages", 'AdminSmsController@index')->name("messages");
                Route::post("text-messages/fetch", 'AdminSmsController@getMessages')->name("messages.fetch");
                Route::post("text-messages/send", 'AdminSmsController@sendMessage')->name("messages.send");
                Route::post("call-forward-number/save", 'AdminSmsController@saveCallForwardNumber');

                // Holiday
                Route::group(['prefix'=>'holiday','as'=>'holiday'],function(){
                    Route::get('/','AdminHolidayController@index');
                    Route::get('show/{id}','AdminHolidayController@show');
                    Route::get('data','AdminHolidayController@data')->name('.data');
                    Route::get('create','AdminHolidayController@create')->name('.create');
                    Route::get('edit/{id}','AdminHolidayController@edit')->name('.edit');
                    Route::post('store','AdminHolidayController@store')->name('.store');
                    Route::delete('delete/{id}','AdminHolidayController@delete')->name('.delete');
                    Route::delete('multiDelete','AdminHolidayController@multiDelete')->name('.multiDelete');
                });
                // company settings
                Route::group(
                    ['prefix' => 'settings'],
                    function () {

                        Route::resource('settings', 'CompanySettingsController', ['only' => ['edit', 'update', 'index']]);

                        // Application Form routes
                        Route::resource('application-setting', 'ApplicationSettingsController');


                        // Role permission routes
                        Route::resource('settings', 'CompanySettingsController', ['only' => ['edit', 'update', 'index']]);
                        Route::post('role-permission/assignAllPermission', ['as' => 'role-permission.assignAllPermission', 'uses' => 'ManageRolePermissionController@assignAllPermission']);
                        Route::post('role-permission/removeAllPermission', ['as' => 'role-permission.removeAllPermission', 'uses' => 'ManageRolePermissionController@removeAllPermission']);
                        Route::post('role-permission/assignRole', ['as' => 'role-permission.assignRole', 'uses' => 'ManageRolePermissionController@assignRole']);
                        Route::post('role-permission/detachRole', ['as' => 'role-permission.detachRole', 'uses' => 'ManageRolePermissionController@detachRole']);
                        Route::post('role-permission/storeRole', ['as' => 'role-permission.storeRole', 'uses' => 'ManageRolePermissionController@storeRole']);
                        Route::post('role-permission/deleteRole', ['as' => 'role-permission.deleteRole', 'uses' => 'ManageRolePermissionController@deleteRole']);
                        Route::get('role-permission/showMembers/{id}', ['as' => 'role-permission.showMembers', 'uses' => 'ManageRolePermissionController@showMembers']);
                        Route::resource('role-permission', 'ManageRolePermissionController');

                        Route::resource('theme-settings', 'AdminThemeSettingsController');
                        Route::resource('linkedin-settings', 'AdminLinkedInSettingsController');

                    }
                );


                Route::get('skills/data', 'AdminSkillsController@data')->name('skills.data');
                Route::resource('skills', 'AdminSkillsController');

                Route::get('locations/data', 'AdminLocationsController@data')->name('locations.data');
                Route::resource('locations', 'AdminLocationsController');


                Route::get('jobs/data', 'AdminJobsController@data')->name('jobs.data');
                Route::get('jobs/application-data', 'AdminJobsController@applicationData')->name('jobs.applicationData');
                Route::post('jobs/send-emails', 'AdminJobsController@sendEmails')->name('jobs.sendEmails');
                Route::get('jobs/send-email', 'AdminJobsController@sendEmail')->name('jobs.sendEmail');
                Route::get('job_workflow/{job_id}', 'AdminJobsController@workflow')->name('job.workflow');
                Route::resource('jobs', 'AdminJobsController');
                
                Route::get('job-templates/search', 'AdminJobDescriptionTemplateController@searchTemplates')->name('job-templates.search');
                Route::get('job-templates/data', 'AdminJobDescriptionTemplateController@data')->name('job-templates.data');
                Route::resource('job-templates', 'AdminJobDescriptionTemplateController');

                Route::post('job-applications/rating-save/{id?}', 'AdminJobApplicationController@ratingSave')->name('job-applications.rating-save');
                Route::post('job-applications/viewDetails', 'AdminJobApplicationController@viewDetails')->name('job-applications.viewDetails');
                Route::get('job-applications/create-schedule/{id?}', 'AdminJobApplicationController@createSchedule')->name('job-applications.create-schedule');
                Route::post('job-applications/store-schedule', 'AdminJobApplicationController@storeSchedule')->name('job-applications.store-schedule');
                Route::get('job-applications/question/{jobID?}/{applicationId?}', 'AdminJobApplicationController@jobQuestion')->name('job-applications.question');
                Route::get('job-applications/export/{status}/{startDate}/{endDate}/{jobs}/{qualification}', 'AdminJobApplicationController@export')->name('job-applications.export');
                Route::get('job-applications/data', 'AdminJobApplicationController@data')->name('job-applications.data');
                Route::get('job-applications/table-view/{jobId?}', 'AdminJobApplicationController@table')->name('job-applications.table');
                Route::post('job-applications/get-qna', 'AdminJobApplicationController@getQna');
                Route::post('job-applications/updateIndex', 'AdminJobApplicationController@updateIndex')->name('job-applications.updateIndex');
                Route::post('job-applications/archive-job-application/{application}', 'AdminJobApplicationController@archiveJobApplication')->name('job-applications.archiveJobApplication');
                Route::post('job-applications/unarchive-job-application/{application}', 'AdminJobApplicationController@unarchiveJobApplication')->name('job-applications.unarchiveJobApplication');
                Route::post('job-applications/add-skills/{applicationId}', 'AdminJobApplicationController@addSkills')->name('job-applications.addSkills');    
                Route::post('job-applications/update-status', 'AdminJobApplicationController@updateStatus')->name('job-application.updateStatus');
                Route::post('job-applications/update-stage', 'AdminJobApplicationController@updateStage')->name('job-application.updateStage');
                Route::post('job-applications/multi-update-stage', 'AdminJobApplicationController@multiUpdateStage')->name('job-application.multiUpdateStage');
                Route::post('job-applications/update-hiring-status', 'AdminJobApplicationController@updateHiringStatus')->name('job-application.updateHiringStatus');
                Route::post('job-applications/send-interview-invite', 'AdminJobApplicationController@sendInterviewInvitation')->name('job-application.interviewInvitation');
                Route::delete('job-applications/multi-delete', 'AdminJobApplicationController@multiDelete')->name('job-application.multiDelete');
                Route::delete('job-applications/multi-archive', 'AdminJobApplicationController@multiArchive')->name('job-application.multiArchive');
                Route::resource('job-applications', 'AdminJobApplicationController');

                Route::get('applications-archive/data', 'AdminApplicationArchiveController@data')->name('applications-archive.data');
                Route::get('applications-archive/export/{skill}', 'AdminApplicationArchiveController@export')->name('applications-archive.export');
                Route::resource('applications-archive', 'AdminApplicationArchiveController');
                Route::get('applications-archive/admin-show/{id}', 'AdminApplicationArchiveController@adminShow')->name('applications-archive.admin-show');
                
                Route::get('candidate-database/data', 'AdminCandidateDatabaseController@data')->name('candidate-database.data');
                Route::get('candidate-database/export/{skill}', 'AdminCandidateDatabaseController@export')->name('candidate-database.export');
                Route::resource('candidate-database', 'AdminCandidateDatabaseController');

                Route::get('job-onboard/data', 'AdminJobOnboardController@data')->name('job-onboard.data');
                Route::get('job-onboard/send-offer/{id?}', 'AdminJobOnboardController@sendOffer')->name('job-onboard.send-offer');
                Route::get('job-onboard/update-status/{id?}', 'AdminJobOnboardController@updateStatus')->name('job-onboard.update-status');
                Route::resource('job-onboard', 'AdminJobOnboardController');

                Route::resource('profile', 'AdminProfileController');
                Route::resource('application-status', 'AdminApplicationStatusController');

                Route::group(['prefix' => 'template', 'as' => 'template.'],function (){
                    Route::get("/", 'TemplateController@index')->name('index');
                    Route::post("/email-template", 'TemplateController@saveEmail')->name('save-email');
                    Route::post("/sms-template", 'TemplateController@saveSms')->name('save-sms');
                    Route::post("/email-template-delete", 'TemplateController@deleteEmail')->name('delete-email');
                    Route::post("/sms-template-delete", 'TemplateController@deleteSms')->name('delete-sms');
                    Route::post("/email-template-edit", 'TemplateController@editEmail')->name('edit-email');
                    Route::post("/sms-template-edit", 'TemplateController@editSms')->name('edit-sms');
                }); 

                Route::get('interview-schedule/data', 'InterviewScheduleController@data')->name('interview-schedule.data');
                Route::get('interview-schedule/table-view', 'InterviewScheduleController@table')->name('interview-schedule.table-view');
                Route::post('interview-schedule/change-status', 'InterviewScheduleController@changeStatus')->name('interview-schedule.change-status');
                Route::post('interview-schedule/change-status-multiple', 'InterviewScheduleController@changeStatusMultiple')->name('interview-schedule.change-status-multiple');
                Route::get('interview-schedule/notify/{id}/{type}', 'InterviewScheduleController@notify')->name('interview-schedule.notify');
                Route::get('interview-schedule/response/{id}/{type}', 'InterviewScheduleController@employeeResponse')->name('interview-schedule.response');
                Route::post('interview-schedule/get-timing', 'InterviewScheduleController@getTiming')->name('interview-schedule.get-timing');
                Route::resource('interview-schedule', 'InterviewScheduleController');
                // Custom Events
                Route::resource('custom-event', 'AdminCustomEventController');
                Route::group(['prefix', 'custom-event','as'=>'custom-event.'],function(){
                    Route::get('/data', 'AdminCustomEventController@data')->name('data');
                    Route::delete('/multi-delete', 'AdminCustomEventController@multiDelete')->name('multiDelete');
                });
                
                Route::get('team/data', 'AdminTeamController@data')->name('team.data');
                Route::post('team/change-role', 'AdminTeamController@changeRole')->name('team.changeRole');
                Route::resource('team', 'AdminTeamController');

                Route::resource('applicant-note', 'ApplicantNoteController');
                Route::resource('sticky-note', 'AdminStickyNotesController');

                Route::resource('departments', 'AdminDepartmentController');

                Route::resource('designations', 'AdminDesignationController');

                Route::get('documents/data', 'AdminDocumentController@data')->name('documents.data');
                Route::get('documents/download-document/{document}', 'AdminDocumentController@downloadDoc')->name('documents.downloadDoc');
                Route::resource('documents', 'AdminDocumentController');
            });

            Route::get('paypal-recurring', array('as' => 'paypal-recurring','uses' => 'AdminPaypalController@payWithPaypalRecurrring',));
            Route::get('paypal-invoice-download/{id}', array('as' => 'paypal.invoice-download','uses' => 'AdminPaypalController@paypalInvoiceDownload',));

            // route for view/blade file
            Route::get('paywithpaypal', array('as' => 'paywithpaypal','uses' => 'AdminPaypalController@payWithPaypal',));
            // route for post request
            Route::get('paypal/{packageId}/{type}', array('as' => 'paypal','uses' => 'AdminPaypalController@paymentWithpaypal',));
            Route::get('paypal/cancel-subscription', array('as' => 'paypal.cancel-subscription','uses' => 'AdminPaypalController@cancelSubscription',));
            Route::get('paypal/cancel-agreement', array('as' => 'paypal.cancel-agreement','uses' => 'AdminPaypalController@cancelAgreement',));

            // route for check status responce
            Route::get('paypal', array('as' => 'status','uses' => 'AdminPaypalController@getPaymentStatus',));
            Route::get('subscribe/invoice', 'ManageSubscriptionController@invoice')->name('subscribe.invoice');
            Route::get('subscribe/data', 'ManageSubscriptionController@data')->name('subscribe.data');
            Route::get('subscribe/history-data', 'ManageSubscriptionController@historyData')->name('subscribe.history-data');
            Route::get('subscribe/cancel-subscription/{type?}', 'ManageSubscriptionController@cancelSubscription')->name('subscribe.cancel-subscription');
            Route::post('subscribe/payment-stripe', 'ManageSubscriptionController@payment')->name('payments.stripe');
            Route::get('subscribe/select-package/{packageID}',  'ManageSubscriptionController@selectPackage')->name('subscribe.select-package');
            Route::get('subscribe/invoice-download/{invoice}', 'ManageSubscriptionController@download')->name('subscribe.invoice-download');
            Route::get('subscribe/default-invoice-download/{invoice}', 'ManageSubscriptionController@invoiceDownload')->name('subscribe.default-invoice-download');
            Route::get('subscribe/paypal-invoice-download/{id}', array('as' => 'subscribe.paypal-invoice-download','uses' => 'ManageSubscriptionController@paypalInvoiceDownload',));
            Route::get('subscribe/history', array('as' => 'subscribe.history','uses' => 'ManageSubscriptionController@history',));
            Route::get('subscribe/razorpay-invoice-download/{id}', 'ManageSubscriptionController@razorpayInvoiceDownload')->name('subscribe.razorpay-invoice-download');
            Route::post('subscribe/razorpay-payment',  'ManageSubscriptionController@razorpayPayment')->name('subscribe.razorpay-payment');
            Route::post('subscribe/razorpay-subscription',  'ManageSubscriptionController@razorpaySubscription')->name('subscribe.razorpay-subscription');
            Route::resource('subscribe', 'ManageSubscriptionController');

            Route::post('todo-items/update-todo-item', 'AdminTodoItemController@updateTodoItem')->name('todo-items.updateTodoItem');
            Route::resource('todo-items', 'AdminTodoItemController');
        }
    );
    
    Route::group(
        ['namespace' => 'SuperAdmin', 'prefix' => 'super-admin', 'as' => 'superadmin.', 'middleware' => ['super-admin']],
        function () {
            Route::resource('profile', 'SuperAdminProfileController');
            Route::resource('dashboard', 'SuperAdminDashboardController');
            Route::get('smtp-settings/sent-test-email', ['uses' => 'SuperAdminSmtpSettingController@sendTestEmail'])->name('smtp-settings.sendTestEmail');
            Route::resource('smtp-settings', 'SuperAdminSmtpSettingController');

            //language settings
            Route::get('language-settings/change-language', ['uses' => 'LanguageSettingsController@changeLanguage'])->name('language-settings.change-language');
            Route::put('language-settings/change-language-status/{id}', 'LanguageSettingsController@changeStatus')->name('language-settings.changeStatus');
            Route::resource('language-settings', 'LanguageSettingsController');

            Route::resource('theme-settings', 'SuperAdminThemeSettingsController')
            ;

            Route::put('footer-settings/update-footer-menu/{id}', 'SuperAdminFooterSettingsController@updateFooterMenu')->name('footer-settings.updateFooterMenu');
            Route::post('footer-settings/store-footer-menu', 'SuperAdminFooterSettingsController@storeFooterMenu')->name('footer-settings.storeFooterMenu');
            Route::get('footer-settings/data', 'SuperAdminFooterSettingsController@data')->name('footer-settings.data');
            Route::resource('footer-settings', 'SuperAdminFooterSettingsController');

            Route::post('update-application/deleteFile', ['uses' => 'UpdateApplicationController@deleteFile'])->name('update-application.deleteFile');
            Route::get('update-application/update', ['as' => 'update-application.updateApp', 'uses' => 'UpdateApplicationController@update']);
            Route::get('update-application/download', ['as' => 'update-application.download', 'uses' => 'UpdateApplicationController@download']);
            Route::get('update-application/downloadPercent', ['as' => 'update-application.downloadPercent', 'uses' => 'UpdateApplicationController@downloadPercent']);
            Route::get('update-application/checkIfFileExtracted', ['as' => 'update-application.checkIfFileExtracted', 'uses' => 'UpdateApplicationController@checkIfFileExtracted']);
            Route::get('update-application/install', ['as' => 'update-application.install', 'uses' => 'UpdateApplicationController@install']);
            Route::resource('update-application', 'UpdateApplicationController');


            Route::post('company/updateCompanyPackage/{id}', 'SuperAdminCompanyController@updateCompanyPackage')->name('company.updateCompanyPackage');
            Route::get('company/changePackage/{id}', 'SuperAdminCompanyController@changePackage')->name('company.changePackage');
            Route::get('company/data', 'SuperAdminCompanyController@data')->name('company.data');
            Route::post('company/{id}/login', 'SuperAdminCompanyController@loginAsCompany')->name('company.loginAsCompany');
            Route::resource('company', 'SuperAdminCompanyController');

            Route::resource('global-settings', 'GlobalSettingController');
            Route::get('linkedin-settings/update-status/{id}', 'LinkedInSettingController@updateStatus')->name('updateStatus');
            Route::resource('linkedin-settings', 'LinkedInSettingController');

            Route::get('facebook-settings/update-status/{id}', 'FacebookSettingController@updateStatus')->name('updateStatus');
            Route::get('facebook-settings/update-facebook-pages', 'FacebookSettingController@updateFacebookPages')->name('updateFacebookPages');
            Route::resource('facebook-settings', 'FacebookSettingController');

            Route::get('front-cms/index', ['as' => 'front-cms.index', 'uses' => 'SuperAdminFrontCmsController@index']);
            Route::get('front-cms/change-form', 'SuperAdminFrontCmsController@changeForm')->name('front-cms.changeForm');
            Route::post('front-cms/update-common-header', ['as' => 'front-cms.updateCommonHeader', 'uses' => 'SuperAdminFrontCmsController@updateCommonHeader']);
            Route::post('front-cms/update-header', ['as' => 'front-cms.updateHeader', 'uses' => 'SuperAdminFrontCmsController@updateHeader']);
            Route::get('front-cms/features', ['as' => 'front-cms.features', 'uses' => 'SuperAdminFrontCmsController@imageFeatures']);
            Route::post('front-cms/features', ['as' => 'front-cms.savefeatures', 'uses' => 'SuperAdminFrontCmsController@saveImageFeatures']);
            Route::post('front-cms/features/{id}', ['as' => 'front-cms.updatefeatures', 'uses' => 'SuperAdminFrontCmsController@updatefeatures']);
            Route::get('front-cms/features/{id}', ['as' => 'front-cms.editfeatures', 'uses' => 'SuperAdminFrontCmsController@editImageFeatures']);
            Route::post('front-cms/deleteFeature/{id}', ['as' => 'front-cms.deleteFeature', 'uses' => 'SuperAdminFrontCmsController@deleteFeature']);

            Route::resource('icon-features', 'FrontIconFeatureController');

            Route::resource('client-feedbacks', 'ClientFeedbackController');

            Route::resource('packages', 'PackageController');
            Route::resource('currency-settings', 'SuperAdminCurrencyController');
            Route::resource('/payment-settings', 'SuperAdminPaymentSettingsController', ['only' => ['index', 'update']]);
            Route::resource('/sms-settings', 'SuperAdminSmsSettingsController', ['only' => ['index', 'update']]);

            Route::get('invoices/invoice-download/{invoice}', 'InvoiceController@download')->name('invoices.invoice-download');
            Route::get('invoices/paypal-invoice-download/{id}', array('as' => 'invoices.paypal-invoice-download','uses' => 'InvoiceController@paypalInvoiceDownload',));
            Route::get('invoices/razorpay-invoice-download/{id}', 'InvoiceController@razorpayInvoiceDownload')->name('invoices.razorpay-invoice-download');
            Route::get('invoices/data', 'InvoiceController@data')->name('invoices.data');

            Route::resource('invoices', 'InvoiceController');

            // Custom Modules
            Route::post('custom-modules/verify-purchase', ['uses' => 'CustomModuleController@verifyingModulePurchase'])->name('custom-modules.verify-purchase');
            Route::resource('custom-modules', 'CustomModuleController');
            
            Route::get('superadmins/data', 'SuperadminController@data')->name('superadmins.data');
            Route::resource('superadmins', 'SuperadminController');

            // Contact forms
            Route::group(['prefix' => 'contact_form'],function ()
            {
                Route::get('/', 'FrontContactFormController@index');
                Route::get('data', 'FrontContactFormController@tableData');
                Route::post('delete/{id}', 'FrontContactFormController@destroy');
            });
    }
    );

    Route::get('change-mobile', 'VerifyMobileController@changeMobile')->name('changeMobile');
    Route::post('/send-otp-code', 'VerifyMobileController@sendVerificationCode')->name('sendOtpCode');
    Route::post('/send-otp-code/account', 'VerifyMobileController@sendVerificationCode')->name('sendOtpCode.account');
    Route::post('/verify-otp-phone', 'VerifyMobileController@verifyOtpCode')->name('verifyOtpCode');
    Route::post('/verify-otp-phone/account', 'VerifyMobileController@verifyOtpCode')->name('verifyOtpCode.account');
    Route::get('/remove-session', 'VerifyMobileController@removeSession')->name('removeSession');
});
